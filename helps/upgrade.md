# Upgrading from Old Ng7Keps

1. Make sure you're using Node 10.9.0+ and Angular CLI 8.3.1+.

2. In the project's `package.json` dependencies, add/update all the Ng7Keps required packages:

```json
{
  "dependencies": {
    "@angular/animations": "~8.2.3",
    "@angular/cdk": "~8.1.4",
    "@angular/common": "~8.2.3",
    "@angular/compiler": "~8.2.3",
    "@angular/core": "~8.2.3",
    "@angular/forms": "~8.2.3",
    "@angular/material": "~8.1.4",
    "@angular/material-moment-adapter": "~8.1.4",
    "@angular/platform-browser": "~8.2.3",
    "@angular/platform-browser-dynamic": "~8.2.3",
    "@angular/router": "~8.2.3",
    "hammerjs": "~2.0.8",
    "lodash": "~4.17.15",
    "moment": "~2.24.0",
    "rxjs": "~6.4.0",
    "tslib": "~1.10.0",
    "zone.js": "~0.9.1"
  },
  "devDependecies": {
    "@angular-devkit/build-angular": "~0.803.1",
    "@angular/cli": "~8.3.1",
    "@angular/compiler-cli": "~8.2.3",
    "@angular/language-service": "~8.2.3",
    "@types/node": "~8.9.4",
    "@types/lodash": "~4.14.138",
    "ts-node": "~7.0.0",
    "tslint": "~5.15.0",
    "typescript": "~3.5.3"
  }
}
```

3. Remove `@angular/http` package from the dependencies list as it has been deprecated.

4. Delete `package-lock.json` and then run `npm install`.

5. Add `allowSyntheticDefaultImports` to `tsconfig.json` inside `compilerOptions`:

```json
{
  ..
  "compilerOptions": {
    ...
    "allowSyntheticDefaultImports": true,
    ...
  }
  ...
}
```

6. Replace all references of `KepsObjs` to `KepsReference` and `KepsObj` to `KepsObject`.

7. Add `{ static: false }` to every `@ViewChild()`. This is a new change from Angular 8. Example:

```typescript
// Angular 7:
@ViewChild(MatDrawer) drawer: MatDrawer;

// Angular 8:
@ViewChild(MatDrawer, { static: false }) drawer: MatDrawer;
```

8. In every places where we're using the `keps-form` component, we have to pass in a `KepsForm` instead of `FormGroup`. Example:

```typescript
// Old Ng7Keps:
import { FormGroup } from '@angular/forms';

form = new FormGroup({});

// New Ng7Keps:
import { KepsForm } from 'ng7keps';

form = new KepsForm({});
```

9. If we're using `KepsForm`, we don't have to wrap `patchValue` or `reset` inside `setTimeout` anymore. Example:

```typescript
// Example from Vicis Admin leagues page.

// Old Ng7Keps:
openEdit(index: number) {
  setTimeout(() => {
    this.editForm.patchValue(this.leagues[index]);
  }, 5); // Wait a bit for form to initialize before setting values.
}

// New Ng7Keps:
openEdit(index: number) {
  this.editForm.patchValue(this.leagues[index]);
}
```

10. In every places where we're using `keps-table`, if we're declaring a custom column using `kepsTableCol`, we'll need to replace `let-index` with `let-index="index"`.

11. Also, if we have `(onRowClick)` on the table, we should replace it with `(rowClick)`. 

Example for 10 & 11:

```html
<!-- Example from Vicis Employee athlete list page -->

<!-- Old Ng7Keps -->
<keps-table
  model="user"
  [data]="athletes"
  [displayedColumns]="['firstName', 'lastName', 'email', 'teams']"
  showPaginator="true"
  savePageSize="athletePageSize"
  showSort="true"
  [search]="search"
  (onRowClick)="goToAthlete($event)"
>
  <ng-template kepsTableCol="teams" let-index>
    {{ athletes[index]?.teams | arrayDisplay:', ':'name' }}
  </ng-template>
</keps-table>

<!-- New Ng7Keps -->
<keps-table
  model="user"
  [data]="athletes"
  [displayedColumns]="['firstName', 'lastName', 'email', 'teams']"
  showPaginator="true"
  savePageSize="athletePageSize"
  showSort="true"
  [search]="search"
  (rowClick)="goToAthlete($event)"
>
  <ng-template kepsTableCol="teams" let-index="index">
    {{ athletes[index]?.teams | arrayDisplay:', ':'name' }}
  </ng-template>
</keps-table>
```

12. In every places where we're using `keps-dynamic-table`, we should remove `modelName` from the component because it has been deprecated. Example:

```html
<!-- Example from Vicis Employee team list page -->

<!-- Old Ng7Keps -->
<keps-dynamic-table
  modelName="teams"
  query="_id,name,level,sportId,rosterCount"
  model="team"
  [displayedColumns]="['name','level','sportName','rosterCount','myTeam']"
  searchField="name"
  [search]="search"
  (onRowClick)="goToTeam($event)"
  [filterQuery]="filterQuery"
  activeSort="name"
  savePageSize="teamPageSize"
>
  ...
</keps-dynamic-table>

<!-- New Ng7Keps -->
<keps-dynamic-table
  model="team"
  query="_id,name,level,sportId,rosterCount"
  [displayedColumns]="['name','level','sportName','rosterCount','myTeam']"
  searchField="name"
  [search]="search"
  (rowClick)="goToTeam($event)"
  [filterQuery]="filterQuery"
  activeSort="name"
  savePageSize="teamPageSize"
>
  ...
</keps-dynamic-table>
```

13. By default, tables are now transparent. So we'll need to add `background: white` on the CSS if we want it to be white.
