# Known Bugs

- On both the table and dynamic table components, if we're using `align` on the table custom column, only the `td` will change according to `align`. The `th` will remain aligned to the left. This is a known bug from Angular Material caused by using `MatSort` on the table header. See: https://github.com/angular/components/issues/5759
