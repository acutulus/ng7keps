# Setting Up Ng7Keps

---
- [Requirements](#requirements)
- [Creating New Angular 8 Project](#creating-new-angular-8-project)
- [Installing Required Packages](#installing-required-packages)
- [Installing Ng7Keps](#installing-ng7keps)
---

## Requirements

- Node 10.9.0+
- Angular CLI 8.3.1+

## Creating New Angular 8 Project

Create a new project using Angular CLI:
```bash
ng new <project_name> --style=scss --routing=true --skipInstall=true --skipTests=true
```

You can copy this project to an existing repository or make the usual Keps folders and files (`controllers`, `models`, etc.) inside it.

Make sure that the `package.json` contains at least these packages:

```json
{
  "dependencies": {
    "@angular/animations": "~8.2.3",
    "@angular/common": "~8.2.3",
    "@angular/compiler": "~8.2.3",
    "@angular/core": "~8.2.3",
    "@angular/forms": "~8.2.3",
    "@angular/platform-browser": "~8.2.3",
    "@angular/platform-browser-dynamic": "~8.2.3",
    "@angular/router": "~8.2.3",
    "rxjs": "~6.4.0",
    "tslib": "~1.10.0",
    "zone.js": "~0.9.1"
  },
  "devDependecies": {
    "@angular-devkit/build-angular": "~0.803.1",
    "@angular/cli": "~8.3.1",
    "@angular/compiler-cli": "~8.2.3",
    "@angular/language-service": "~8.2.3",
    "@types/node": "~8.9.4",
    "ts-node": "~7.0.0",
    "tslint": "~5.15.0",
    "typescript": "~3.5.3"
  }
}
```

### Optional: Removing Test Files

Angular CLI will automatically generate test files. If we don't need it at all, we can remove it by:
1. Delete the `e2e` folder from the project folder.
1. Remove any reference to `e2e` in `angular.json`.
1. Remove these testing suite packages from the generated `package.json`:
  - `@types/jasmine`
  - `@types/jasminwd2`
  - `jasmine-core`
  - `jasmine-spec-reporter`
  - `karma`
  - `karma-chrome-launcher`
  - `karma-coverage-istanbul-reporter`
  - `karma-jasmine`
  - `karma-jasmine-html-reporter`
  - `protractor`

## Installing Required Packages

1. Add these additional packages that Ng7Keps requires to `package.json`:

```json
{
  "dependencies": {
    "@angular/cdk": "~8.1.4",
    "@angular/material": "~8.1.4",
    "@angular/material-moment-adapter": "~8.1.4",
    "hammerjs": "~2.0.8",
    "lodash": "~4.17.15",
    "moment": "~2.24.0"
  },
  "devDependencies": {
    "@types/lodash": "~4.14.138"
  }
}
```

2. Do `npm install`.
1. Follow the [Angular Material instruction](https://material.angular.io/guide/getting-started#step-2-configure-animations) on how to setup the modules, include a theme, and add icons.
1. Create a file named `material.ts` inside your `src` folder.
1. Paste this to `material.ts`:

```typescript
import { NgModule } from '@angular/core';
import {
  MatButtonModule
} from '@angular/material';

const modules = [
  MatButtonModule
];

@NgModule({
  imports: modules,
  exports: modules
})
export class MaterialModule { }
```

> You can add/replace Angular Material modules inside the `import` statement and `modules` array to be used on the project.

6. Import that module to your `app.module.ts`:

```typescript
import { MaterialModule } from '../material';
...
// Inside your NgModule
  imports: [
    ...
    MaterialModule,
    ...
  ]
```

## Installing Ng7Keps

1. Install the package from this repo:

```bash
$ npm install https://bitbucket.org/acutulus/ng7keps.git
```

2. Inside `package.json`, add a script to generate interfaces from Ng7Keps.

```json
{
  ...
  "scripts": {
    "generate:interface": "npm explore ng7keps -- npm run generate -- <server address> <interface folder>"
  },
  ...
}
```

> Replace `<server address>` with the Keps server address (e.g. `http://localhost:61356/`) and `<interface folder>` with the relative path **from the package** of where the interfaces should be generated (e.g. `../../src/models`).

3. Run the Keps server using `npm start`.

4. While the server is running, run `npm run generate:interface` in another terminal. This will convert all the models from the `models` folder to Typescript interfaces.

5. In the project's `tsconfig.json` file, add these lines:

```json
{
  ..
  "compilerOptions": {
    ...
    "allowSyntheticDefaultImports": true,
    ...
  },
  ...
  "paths": {
    "<projectName>": ["<interface folder>"]
  }
}
```

> `allowSyntheticDefaultImports` will allow you to import `lodash` like this `import _ from 'lodash';` instead of `import * as _ from 'lodash';`. This is required because it's used internally inside Ng7Keps.

> Replace `<projectName>` with the name of the project and `<interface folder>` with the path to the folder generated from the previous step **relative to where the `tsconfig.json` is**. You can then import the generated interfaces like this `import { ModelName } from 'projectName';`.

5. Inside the `app.module.ts` file, add these lines on top of the file:

```typescript
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng7KepsModule, AuthService, AuthInterceptor, AuthGuard } from 'ng7keps';
import { MODELS } from 'projectName';

export function init_auth(authService: AuthService) {
  return () => authService.start();
}
```

6. Still in the same file, add these lines to the `NgModule` options:

```typescript
@NgModule({
  ...
  imports: [
    ...
    Ng7KepsModule.forRoot(MODELS)
  ],
  ...
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: init_auth,
      multi: true,
      deps: [AuthService]
    }
  ]
})
export class AppModule { }
```

7. You can now use the Ng7Keps library in the project.
