# To Do

- Create a `keps-button` component with toggleable loading spinner. See Vicis `vic-button-submit` or Roof `rf-button-submit` for example.
- Add support for more Keps schema types such as `type: "image"`.
- Make Keps form to be able to create form for `type: "object"` with `subSchema`.
