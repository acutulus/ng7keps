/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A helper function that checks for a schema's field type.
 * \@param schema A Keps schema.
 * \@param field A field name.
 * \@param type A type name to be checked.
 * @type {?}
 */
export var isType = (/**
 * @param {?} schema
 * @param {?} field
 * @param {?} type
 * @return {?}
 */
function (schema, field, type) {
    /** @type {?} */
    var fieldType = schema[field].type;
    if (type === 'reference') {
        return fieldType[0] === ':';
    }
    if (type === 'array-reference') {
        return fieldType === 'array' && schema[field].subSchema[0] === ':';
    }
    if (type === 'array-string') {
        return fieldType === 'array' && schema[field].subSchema === 'string';
    }
    // This is to check if table should render the object as is with no additional processing.
    if (type === 'table-normal') {
        return !(isType(schema, field, 'reference') ||
            isType(schema, field, 'array-reference') ||
            isType(schema, field, 'array-string') ||
            fieldType === 'address' ||
            fieldType === 'datetime' ||
            fieldType === 'enum' ||
            fieldType === 'number');
    }
    return fieldType === type;
});
/**
 * A helper function to get the display expression of a field in schema.
 * \@param models Models from the project to get the reference from.
 * \@param schema A Keps schema.
 * \@param field A field name.
 * @type {?}
 */
export var getReferenceDisplay = (/**
 * @param {?} models
 * @param {?} schema
 * @param {?} field
 * @return {?}
 */
function (models, schema, field) {
    /** @type {?} */
    var referenceTo = schema[field].type.slice(1);
    if (models[referenceTo]) {
        return models[referenceTo].properties.displayExpression;
    }
    return null;
});
/**
 * A helper function to get the display expression of an array field in schema.
 * \@param models Models from the project to get the reference from.
 * \@param schema A Keps schema.
 * \@param field A field name.
 * @type {?}
 */
export var getReferenceDisplayArray = (/**
 * @param {?} models
 * @param {?} schema
 * @param {?} field
 * @return {?}
 */
function (models, schema, field) {
    if (schema[field].type === 'array' &&
        typeof schema[field].subSchema === 'string' &&
        schema[field].subSchema[0] === ':') {
        /** @type {?} */
        var referenceTo = ((/** @type {?} */ (schema[field].subSchema))).slice(1);
        if (models[referenceTo]) {
            return models[referenceTo].properties.displayExpression;
        }
    }
    return null;
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL3V0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBUUEsTUFBTSxLQUFPLE1BQU07Ozs7OztBQUFHLFVBQUMsTUFBa0IsRUFBRSxLQUFhLEVBQUUsSUFBWTs7UUFDOUQsU0FBUyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJO0lBQ3BDLElBQUksSUFBSSxLQUFLLFdBQVcsRUFBRTtRQUN4QixPQUFPLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUM7S0FDN0I7SUFDRCxJQUFJLElBQUksS0FBSyxpQkFBaUIsRUFBRTtRQUM5QixPQUFPLFNBQVMsS0FBSyxPQUFPLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUM7S0FDcEU7SUFDRCxJQUFJLElBQUksS0FBSyxjQUFjLEVBQUU7UUFDM0IsT0FBTyxTQUFTLEtBQUssT0FBTyxJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEtBQUssUUFBUSxDQUFDO0tBQ3RFO0lBQ0QsMEZBQTBGO0lBQzFGLElBQUksSUFBSSxLQUFLLGNBQWMsRUFBRTtRQUMzQixPQUFPLENBQUMsQ0FDTixNQUFNLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxXQUFXLENBQUM7WUFDbEMsTUFBTSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsaUJBQWlCLENBQUM7WUFDeEMsTUFBTSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsY0FBYyxDQUFDO1lBQ3JDLFNBQVMsS0FBSyxTQUFTO1lBQ3ZCLFNBQVMsS0FBSyxVQUFVO1lBQ3hCLFNBQVMsS0FBSyxNQUFNO1lBQ3BCLFNBQVMsS0FBSyxRQUFRLENBQ3ZCLENBQUM7S0FDSDtJQUVELE9BQU8sU0FBUyxLQUFLLElBQUksQ0FBQztBQUM1QixDQUFDLENBQUE7Ozs7Ozs7O0FBUUQsTUFBTSxLQUFPLG1CQUFtQjs7Ozs7O0FBQUcsVUFBQyxNQUFrQixFQUFFLE1BQWtCLEVBQUUsS0FBYTs7UUFDakYsV0FBVyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUMvQyxJQUFJLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRTtRQUN2QixPQUFPLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUM7S0FDekQ7SUFDRCxPQUFPLElBQUksQ0FBQztBQUNkLENBQUMsQ0FBQTs7Ozs7Ozs7QUFRRCxNQUFNLEtBQU8sd0JBQXdCOzs7Ozs7QUFBRyxVQUFDLE1BQWtCLEVBQUUsTUFBa0IsRUFBRSxLQUFhO0lBQzVGLElBQ0UsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksS0FBSyxPQUFPO1FBQzlCLE9BQU8sTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsS0FBSyxRQUFRO1FBQzNDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUNsQzs7WUFDTSxXQUFXLEdBQUcsQ0FBQyxtQkFBUSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxFQUFBLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzlELElBQUksTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ3ZCLE9BQU8sTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQztTQUN6RDtLQUNGO0lBQ0QsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBLZXBzTW9kZWxzLCBLZXBzU2NoZW1hIH0gZnJvbSAnLi9pbnRlcmZhY2UnO1xyXG5cclxuLyoqXHJcbiAqIEEgaGVscGVyIGZ1bmN0aW9uIHRoYXQgY2hlY2tzIGZvciBhIHNjaGVtYSdzIGZpZWxkIHR5cGUuXHJcbiAqIEBwYXJhbSBzY2hlbWEgQSBLZXBzIHNjaGVtYS5cclxuICogQHBhcmFtIGZpZWxkIEEgZmllbGQgbmFtZS5cclxuICogQHBhcmFtIHR5cGUgQSB0eXBlIG5hbWUgdG8gYmUgY2hlY2tlZC5cclxuICovXHJcbmV4cG9ydCBjb25zdCBpc1R5cGUgPSAoc2NoZW1hOiBLZXBzU2NoZW1hLCBmaWVsZDogc3RyaW5nLCB0eXBlOiBzdHJpbmcpID0+IHtcclxuICBjb25zdCBmaWVsZFR5cGUgPSBzY2hlbWFbZmllbGRdLnR5cGU7XHJcbiAgaWYgKHR5cGUgPT09ICdyZWZlcmVuY2UnKSB7XHJcbiAgICByZXR1cm4gZmllbGRUeXBlWzBdID09PSAnOic7XHJcbiAgfVxyXG4gIGlmICh0eXBlID09PSAnYXJyYXktcmVmZXJlbmNlJykge1xyXG4gICAgcmV0dXJuIGZpZWxkVHlwZSA9PT0gJ2FycmF5JyAmJiBzY2hlbWFbZmllbGRdLnN1YlNjaGVtYVswXSA9PT0gJzonO1xyXG4gIH1cclxuICBpZiAodHlwZSA9PT0gJ2FycmF5LXN0cmluZycpIHtcclxuICAgIHJldHVybiBmaWVsZFR5cGUgPT09ICdhcnJheScgJiYgc2NoZW1hW2ZpZWxkXS5zdWJTY2hlbWEgPT09ICdzdHJpbmcnO1xyXG4gIH1cclxuICAvLyBUaGlzIGlzIHRvIGNoZWNrIGlmIHRhYmxlIHNob3VsZCByZW5kZXIgdGhlIG9iamVjdCBhcyBpcyB3aXRoIG5vIGFkZGl0aW9uYWwgcHJvY2Vzc2luZy5cclxuICBpZiAodHlwZSA9PT0gJ3RhYmxlLW5vcm1hbCcpIHtcclxuICAgIHJldHVybiAhKFxyXG4gICAgICBpc1R5cGUoc2NoZW1hLCBmaWVsZCwgJ3JlZmVyZW5jZScpIHx8XHJcbiAgICAgIGlzVHlwZShzY2hlbWEsIGZpZWxkLCAnYXJyYXktcmVmZXJlbmNlJykgfHxcclxuICAgICAgaXNUeXBlKHNjaGVtYSwgZmllbGQsICdhcnJheS1zdHJpbmcnKSB8fFxyXG4gICAgICBmaWVsZFR5cGUgPT09ICdhZGRyZXNzJyB8fFxyXG4gICAgICBmaWVsZFR5cGUgPT09ICdkYXRldGltZScgfHxcclxuICAgICAgZmllbGRUeXBlID09PSAnZW51bScgfHxcclxuICAgICAgZmllbGRUeXBlID09PSAnbnVtYmVyJ1xyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHJldHVybiBmaWVsZFR5cGUgPT09IHR5cGU7XHJcbn07XHJcblxyXG4vKipcclxuICogQSBoZWxwZXIgZnVuY3Rpb24gdG8gZ2V0IHRoZSBkaXNwbGF5IGV4cHJlc3Npb24gb2YgYSBmaWVsZCBpbiBzY2hlbWEuXHJcbiAqIEBwYXJhbSBtb2RlbHMgTW9kZWxzIGZyb20gdGhlIHByb2plY3QgdG8gZ2V0IHRoZSByZWZlcmVuY2UgZnJvbS5cclxuICogQHBhcmFtIHNjaGVtYSBBIEtlcHMgc2NoZW1hLlxyXG4gKiBAcGFyYW0gZmllbGQgQSBmaWVsZCBuYW1lLlxyXG4gKi9cclxuZXhwb3J0IGNvbnN0IGdldFJlZmVyZW5jZURpc3BsYXkgPSAobW9kZWxzOiBLZXBzTW9kZWxzLCBzY2hlbWE6IEtlcHNTY2hlbWEsIGZpZWxkOiBzdHJpbmcpID0+IHtcclxuICBjb25zdCByZWZlcmVuY2VUbyA9IHNjaGVtYVtmaWVsZF0udHlwZS5zbGljZSgxKTtcclxuICBpZiAobW9kZWxzW3JlZmVyZW5jZVRvXSkge1xyXG4gICAgcmV0dXJuIG1vZGVsc1tyZWZlcmVuY2VUb10ucHJvcGVydGllcy5kaXNwbGF5RXhwcmVzc2lvbjtcclxuICB9XHJcbiAgcmV0dXJuIG51bGw7XHJcbn07XHJcblxyXG4vKipcclxuICogQSBoZWxwZXIgZnVuY3Rpb24gdG8gZ2V0IHRoZSBkaXNwbGF5IGV4cHJlc3Npb24gb2YgYW4gYXJyYXkgZmllbGQgaW4gc2NoZW1hLlxyXG4gKiBAcGFyYW0gbW9kZWxzIE1vZGVscyBmcm9tIHRoZSBwcm9qZWN0IHRvIGdldCB0aGUgcmVmZXJlbmNlIGZyb20uXHJcbiAqIEBwYXJhbSBzY2hlbWEgQSBLZXBzIHNjaGVtYS5cclxuICogQHBhcmFtIGZpZWxkIEEgZmllbGQgbmFtZS5cclxuICovXHJcbmV4cG9ydCBjb25zdCBnZXRSZWZlcmVuY2VEaXNwbGF5QXJyYXkgPSAobW9kZWxzOiBLZXBzTW9kZWxzLCBzY2hlbWE6IEtlcHNTY2hlbWEsIGZpZWxkOiBzdHJpbmcpID0+IHtcclxuICBpZiAoXHJcbiAgICBzY2hlbWFbZmllbGRdLnR5cGUgPT09ICdhcnJheScgJiZcclxuICAgIHR5cGVvZiBzY2hlbWFbZmllbGRdLnN1YlNjaGVtYSA9PT0gJ3N0cmluZycgJiZcclxuICAgIHNjaGVtYVtmaWVsZF0uc3ViU2NoZW1hWzBdID09PSAnOidcclxuICApIHtcclxuICAgIGNvbnN0IHJlZmVyZW5jZVRvID0gKDxzdHJpbmc+c2NoZW1hW2ZpZWxkXS5zdWJTY2hlbWEpLnNsaWNlKDEpO1xyXG4gICAgaWYgKG1vZGVsc1tyZWZlcmVuY2VUb10pIHtcclxuICAgICAgcmV0dXJuIG1vZGVsc1tyZWZlcmVuY2VUb10ucHJvcGVydGllcy5kaXNwbGF5RXhwcmVzc2lvbjtcclxuICAgIH1cclxuICB9XHJcbiAgcmV0dXJuIG51bGw7XHJcbn07XHJcbiJdfQ==