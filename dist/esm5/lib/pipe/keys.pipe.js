/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
/**
 * A pipe that transforms the keys of an object to an array of strings.
 */
var KeysPipe = /** @class */ (function () {
    function KeysPipe() {
    }
    /**
     * @param {?} obj
     * @return {?}
     */
    KeysPipe.prototype.transform = /**
     * @param {?} obj
     * @return {?}
     */
    function (obj) {
        return Object.keys(obj);
    };
    KeysPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'keys'
                },] }
    ];
    return KeysPipe;
}());
export { KeysPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2V5cy5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9waXBlL2tleXMucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7Ozs7QUFLcEQ7SUFBQTtJQVNBLENBQUM7Ozs7O0lBSkMsNEJBQVM7Ozs7SUFBVCxVQUFVLEdBQVc7UUFDbkIsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7O2dCQVBGLElBQUksU0FBQztvQkFDSixJQUFJLEVBQUUsTUFBTTtpQkFDYjs7SUFPRCxlQUFDO0NBQUEsQUFURCxJQVNDO1NBTlksUUFBUSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8qKlxyXG4gKiBBIHBpcGUgdGhhdCB0cmFuc2Zvcm1zIHRoZSBrZXlzIG9mIGFuIG9iamVjdCB0byBhbiBhcnJheSBvZiBzdHJpbmdzLlxyXG4gKi9cclxuQFBpcGUoe1xyXG4gIG5hbWU6ICdrZXlzJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgS2V5c1BpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgdHJhbnNmb3JtKG9iajogb2JqZWN0KTogc3RyaW5nW10ge1xyXG4gICAgcmV0dXJuIE9iamVjdC5rZXlzKG9iaik7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=