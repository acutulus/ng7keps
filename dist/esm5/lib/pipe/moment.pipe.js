/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import moment from 'moment';
/**
 * A pipe that transforms UNIX timestamp to a nice moment-formatted string.
 */
var MomentPipe = /** @class */ (function () {
    function MomentPipe() {
    }
    /**
     * @param {?} timestamp
     * @param {?=} format
     * @return {?}
     */
    MomentPipe.prototype.transform = /**
     * @param {?} timestamp
     * @param {?=} format
     * @return {?}
     */
    function (timestamp, format) {
        if (format === void 0) { format = 'MM/DD/YYYY'; }
        return moment(timestamp).format(format);
    };
    MomentPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'moment'
                },] }
    ];
    return MomentPipe;
}());
export { MomentPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9tZW50LnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL3BpcGUvbW9tZW50LnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ3BELE9BQU8sTUFBTSxNQUFNLFFBQVEsQ0FBQzs7OztBQUs1QjtJQUFBO0lBU0EsQ0FBQzs7Ozs7O0lBSkMsOEJBQVM7Ozs7O0lBQVQsVUFBVSxTQUFpQixFQUFFLE1BQTZCO1FBQTdCLHVCQUFBLEVBQUEscUJBQTZCO1FBQ3hELE9BQU8sTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMxQyxDQUFDOztnQkFQRixJQUFJLFNBQUM7b0JBQ0osSUFBSSxFQUFFLFFBQVE7aUJBQ2Y7O0lBT0QsaUJBQUM7Q0FBQSxBQVRELElBU0M7U0FOWSxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCc7XHJcblxyXG4vKipcclxuICogQSBwaXBlIHRoYXQgdHJhbnNmb3JtcyBVTklYIHRpbWVzdGFtcCB0byBhIG5pY2UgbW9tZW50LWZvcm1hdHRlZCBzdHJpbmcuXHJcbiAqL1xyXG5AUGlwZSh7XHJcbiAgbmFtZTogJ21vbWVudCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE1vbWVudFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgdHJhbnNmb3JtKHRpbWVzdGFtcDogbnVtYmVyLCBmb3JtYXQ6IHN0cmluZyA9ICdNTS9ERC9ZWVlZJyk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gbW9tZW50KHRpbWVzdGFtcCkuZm9ybWF0KGZvcm1hdCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=