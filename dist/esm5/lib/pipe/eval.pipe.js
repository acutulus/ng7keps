/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
/**
 * A pipe to evaluate an expression from supplied context.
 */
var EvalPipe = /** @class */ (function () {
    function EvalPipe() {
    }
    /**
     * @param {?} context
     * @param {?} expression
     * @return {?}
     */
    EvalPipe.prototype.transform = /**
     * @param {?} context
     * @param {?} expression
     * @return {?}
     */
    function (context, expression) {
        var _this = this;
        // If expression is not a string, return the context.
        if (typeof expression !== 'string') {
            return context;
        }
        // Convert expression to tokens.
        /** @type {?} */
        var tokens = this.getTokens(expression);
        // Parse each token based on the context.
        tokens = tokens.map((/**
         * @param {?} token
         * @return {?}
         */
        function (token) { return _this.parseToken(token, context); }));
        // Combine tokens back as a string.
        return this.combineTokens(tokens);
    };
    /**
     * @private
     * @param {?} str
     * @return {?}
     */
    EvalPipe.prototype.getTokens = /**
     * @private
     * @param {?} str
     * @return {?}
     */
    function (str) {
        return str.match(/[^\s"']+|"([^"]*)"|'([^']*)'/gm);
    };
    /**
     * @private
     * @param {?} token
     * @param {?} context
     * @return {?}
     */
    EvalPipe.prototype.parseToken = /**
     * @private
     * @param {?} token
     * @param {?} context
     * @return {?}
     */
    function (token, context) {
        if ((token[0] === '"' && token[token.length - 1] === '"') ||
            (token[0] === "'" && token[token.length - 1] === "'")) {
            // If token is a quoted string, return the text inside the quotes.
            return token.substring(1, token.length - 1);
        }
        else if (token === '+') {
            // Return '+' as it is.
            return token;
        }
        else if (token[0] === '.') {
            // If the first character is a dot, continue parsing after the dot.
            return this.parseToken(token.substring(1), context);
        }
        else {
            // If context is missing, log the error and return null.
            if (context === null) {
                this.parseError('No context supplied.');
                return null;
            }
            // Check for special characters.
            /** @type {?} */
            var specials = token.match(/(\[|\.)/gm);
            if (specials && specials.length > 0) {
                if (specials[0] === '[') {
                    /** @type {?} */
                    var inbetween = token.match(/\[(\S)\]/);
                    if (inbetween && inbetween.length > 0) {
                        /** @type {?} */
                        var num = parseInt(inbetween[1], 10);
                        /** @type {?} */
                        var before = token.substring(0, token.indexOf('['));
                        if (!context[before]) {
                            this.parseError(before + " not found");
                            return null;
                        }
                        /** @type {?} */
                        var after = token.substring(token.indexOf(']') + 1);
                        /** @type {?} */
                        var parsed = void 0;
                        if (isNaN(num)) {
                            parsed = context[before][inbetween[1]];
                        }
                        else {
                            parsed = context[before][num];
                        }
                        if (after.length > 0) {
                            return this.parseToken(after, parsed);
                        }
                        else {
                            return parsed;
                        }
                    }
                    else {
                        this.parseError('No closing bracket.');
                        return null;
                    }
                }
                else {
                    /** @type {?} */
                    var i = token.indexOf('.');
                    return this.parseToken(token.substring(i + 1), context[token.substring(0, i)]);
                }
            }
            else {
                return context[token];
            }
        }
    };
    /**
     * @private
     * @param {?} tokens
     * @return {?}
     */
    EvalPipe.prototype.combineTokens = /**
     * @private
     * @param {?} tokens
     * @return {?}
     */
    function (tokens) {
        /** @type {?} */
        var result;
        for (var i = 0; i < tokens.length; i++) {
            if (i % 2 === 1) {
                if (tokens[i] !== '+') {
                    this.parseError('error');
                    return null;
                }
            }
            else {
                if (!result) {
                    result = tokens[i];
                }
                else {
                    result += tokens[i];
                }
            }
        }
        return result;
    };
    /**
     * @private
     * @param {?} message
     * @return {?}
     */
    EvalPipe.prototype.parseError = /**
     * @private
     * @param {?} message
     * @return {?}
     */
    function (message) {
        console.error('EvalPipeError: ' + message);
    };
    EvalPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'eval'
                },] }
    ];
    return EvalPipe;
}());
export { EvalPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZhbC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9waXBlL2V2YWwucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7Ozs7QUFLcEQ7SUFBQTtJQTBHQSxDQUFDOzs7Ozs7SUFyR0MsNEJBQVM7Ozs7O0lBQVQsVUFBVSxPQUFZLEVBQUUsVUFBa0I7UUFBMUMsaUJBY0M7UUFiQyxxREFBcUQ7UUFDckQsSUFBSSxPQUFPLFVBQVUsS0FBSyxRQUFRLEVBQUU7WUFDbEMsT0FBTyxPQUFPLENBQUM7U0FDaEI7OztZQUdHLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQztRQUV2Qyx5Q0FBeUM7UUFDekMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsRUFBL0IsQ0FBK0IsRUFBQyxDQUFDO1FBRTlELG1DQUFtQztRQUNuQyxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7Ozs7O0lBRU8sNEJBQVM7Ozs7O0lBQWpCLFVBQWtCLEdBQVc7UUFDM0IsT0FBTyxHQUFHLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7OztJQUVPLDZCQUFVOzs7Ozs7SUFBbEIsVUFBbUIsS0FBYSxFQUFFLE9BQWU7UUFDL0MsSUFDRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDO1lBQ3JELENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsRUFDckQ7WUFDQSxrRUFBa0U7WUFDbEUsT0FBTyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQzdDO2FBQU0sSUFBSSxLQUFLLEtBQUssR0FBRyxFQUFFO1lBQ3hCLHVCQUF1QjtZQUN2QixPQUFPLEtBQUssQ0FBQztTQUNkO2FBQU0sSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO1lBQzNCLG1FQUFtRTtZQUNuRSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUNyRDthQUFNO1lBQ0wsd0RBQXdEO1lBQ3hELElBQUksT0FBTyxLQUFLLElBQUksRUFBRTtnQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLElBQUksQ0FBQzthQUNiOzs7Z0JBR0ssUUFBUSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO1lBQ3pDLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNuQyxJQUFJLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7O3dCQUNqQixTQUFTLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUM7b0JBQ3pDLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzs0QkFDL0IsR0FBRyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDOzs0QkFDaEMsTUFBTSxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ3JELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7NEJBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUksTUFBTSxlQUFZLENBQUMsQ0FBQzs0QkFDdkMsT0FBTyxJQUFJLENBQUM7eUJBQ2I7OzRCQUNLLEtBQUssR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDOzs0QkFDakQsTUFBTSxTQUFLO3dCQUNmLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFOzRCQUNkLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ3hDOzZCQUFNOzRCQUNMLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7eUJBQy9CO3dCQUNELElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7NEJBQ3BCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7eUJBQ3ZDOzZCQUFNOzRCQUNMLE9BQU8sTUFBTSxDQUFDO3lCQUNmO3FCQUNGO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxVQUFVLENBQUMscUJBQXFCLENBQUMsQ0FBQzt3QkFDdkMsT0FBTyxJQUFJLENBQUM7cUJBQ2I7aUJBQ0Y7cUJBQU07O3dCQUNDLENBQUMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQztvQkFDNUIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hGO2FBQ0Y7aUJBQU07Z0JBQ0wsT0FBTyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdkI7U0FDRjtJQUNILENBQUM7Ozs7OztJQUVPLGdDQUFhOzs7OztJQUFyQixVQUFzQixNQUFhOztZQUM3QixNQUFXO1FBQ2YsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDdEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDZixJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUU7b0JBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3pCLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDWCxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNwQjtxQkFBTTtvQkFDTCxNQUFNLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNyQjthQUNGO1NBQ0Y7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDOzs7Ozs7SUFFTyw2QkFBVTs7Ozs7SUFBbEIsVUFBbUIsT0FBZTtRQUNoQyxPQUFPLENBQUMsS0FBSyxDQUFDLGlCQUFpQixHQUFHLE9BQU8sQ0FBQyxDQUFDO0lBQzdDLENBQUM7O2dCQXhHRixJQUFJLFNBQUM7b0JBQ0osSUFBSSxFQUFFLE1BQU07aUJBQ2I7O0lBd0dELGVBQUM7Q0FBQSxBQTFHRCxJQTBHQztTQXZHWSxRQUFRIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqIEEgcGlwZSB0byBldmFsdWF0ZSBhbiBleHByZXNzaW9uIGZyb20gc3VwcGxpZWQgY29udGV4dC5cclxuICovXHJcbkBQaXBlKHtcclxuICBuYW1lOiAnZXZhbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEV2YWxQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcblxyXG4gIHRyYW5zZm9ybShjb250ZXh0OiBhbnksIGV4cHJlc3Npb246IHN0cmluZyk6IGFueSB7XHJcbiAgICAvLyBJZiBleHByZXNzaW9uIGlzIG5vdCBhIHN0cmluZywgcmV0dXJuIHRoZSBjb250ZXh0LlxyXG4gICAgaWYgKHR5cGVvZiBleHByZXNzaW9uICE9PSAnc3RyaW5nJykge1xyXG4gICAgICByZXR1cm4gY29udGV4dDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDb252ZXJ0IGV4cHJlc3Npb24gdG8gdG9rZW5zLlxyXG4gICAgbGV0IHRva2VucyA9IHRoaXMuZ2V0VG9rZW5zKGV4cHJlc3Npb24pO1xyXG5cclxuICAgIC8vIFBhcnNlIGVhY2ggdG9rZW4gYmFzZWQgb24gdGhlIGNvbnRleHQuXHJcbiAgICB0b2tlbnMgPSB0b2tlbnMubWFwKHRva2VuID0+IHRoaXMucGFyc2VUb2tlbih0b2tlbiwgY29udGV4dCkpO1xyXG5cclxuICAgIC8vIENvbWJpbmUgdG9rZW5zIGJhY2sgYXMgYSBzdHJpbmcuXHJcbiAgICByZXR1cm4gdGhpcy5jb21iaW5lVG9rZW5zKHRva2Vucyk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdldFRva2VucyhzdHI6IHN0cmluZyk6IHN0cmluZ1tdIHtcclxuICAgIHJldHVybiBzdHIubWF0Y2goL1teXFxzXCInXSt8XCIoW15cIl0qKVwifCcoW14nXSopJy9nbSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHBhcnNlVG9rZW4odG9rZW46IHN0cmluZywgY29udGV4dDogb2JqZWN0KTogYW55IHtcclxuICAgIGlmIChcclxuICAgICAgKHRva2VuWzBdID09PSAnXCInICYmIHRva2VuW3Rva2VuLmxlbmd0aCAtIDFdID09PSAnXCInKSB8fFxyXG4gICAgICAodG9rZW5bMF0gPT09IGAnYCAmJiB0b2tlblt0b2tlbi5sZW5ndGggLSAxXSA9PT0gYCdgKVxyXG4gICAgKSB7XHJcbiAgICAgIC8vIElmIHRva2VuIGlzIGEgcXVvdGVkIHN0cmluZywgcmV0dXJuIHRoZSB0ZXh0IGluc2lkZSB0aGUgcXVvdGVzLlxyXG4gICAgICByZXR1cm4gdG9rZW4uc3Vic3RyaW5nKDEsIHRva2VuLmxlbmd0aCAtIDEpO1xyXG4gICAgfSBlbHNlIGlmICh0b2tlbiA9PT0gJysnKSB7XHJcbiAgICAgIC8vIFJldHVybiAnKycgYXMgaXQgaXMuXHJcbiAgICAgIHJldHVybiB0b2tlbjtcclxuICAgIH0gZWxzZSBpZiAodG9rZW5bMF0gPT09ICcuJykge1xyXG4gICAgICAvLyBJZiB0aGUgZmlyc3QgY2hhcmFjdGVyIGlzIGEgZG90LCBjb250aW51ZSBwYXJzaW5nIGFmdGVyIHRoZSBkb3QuXHJcbiAgICAgIHJldHVybiB0aGlzLnBhcnNlVG9rZW4odG9rZW4uc3Vic3RyaW5nKDEpLCBjb250ZXh0KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIElmIGNvbnRleHQgaXMgbWlzc2luZywgbG9nIHRoZSBlcnJvciBhbmQgcmV0dXJuIG51bGwuXHJcbiAgICAgIGlmIChjb250ZXh0ID09PSBudWxsKSB7XHJcbiAgICAgICAgdGhpcy5wYXJzZUVycm9yKCdObyBjb250ZXh0IHN1cHBsaWVkLicpO1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBDaGVjayBmb3Igc3BlY2lhbCBjaGFyYWN0ZXJzLlxyXG4gICAgICBjb25zdCBzcGVjaWFscyA9IHRva2VuLm1hdGNoKC8oXFxbfFxcLikvZ20pO1xyXG4gICAgICBpZiAoc3BlY2lhbHMgJiYgc3BlY2lhbHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIGlmIChzcGVjaWFsc1swXSA9PT0gJ1snKSB7XHJcbiAgICAgICAgICBjb25zdCBpbmJldHdlZW4gPSB0b2tlbi5tYXRjaCgvXFxbKFxcUylcXF0vKTtcclxuICAgICAgICAgIGlmIChpbmJldHdlZW4gJiYgaW5iZXR3ZWVuLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29uc3QgbnVtID0gcGFyc2VJbnQoaW5iZXR3ZWVuWzFdLCAxMCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGJlZm9yZSA9IHRva2VuLnN1YnN0cmluZygwLCB0b2tlbi5pbmRleE9mKCdbJykpO1xyXG4gICAgICAgICAgICBpZiAoIWNvbnRleHRbYmVmb3JlXSkge1xyXG4gICAgICAgICAgICAgIHRoaXMucGFyc2VFcnJvcihgJHtiZWZvcmV9IG5vdCBmb3VuZGApO1xyXG4gICAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGFmdGVyID0gdG9rZW4uc3Vic3RyaW5nKHRva2VuLmluZGV4T2YoJ10nKSArIDEpO1xyXG4gICAgICAgICAgICBsZXQgcGFyc2VkOiBhbnk7XHJcbiAgICAgICAgICAgIGlmIChpc05hTihudW0pKSB7XHJcbiAgICAgICAgICAgICAgcGFyc2VkID0gY29udGV4dFtiZWZvcmVdW2luYmV0d2VlblsxXV07XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgcGFyc2VkID0gY29udGV4dFtiZWZvcmVdW251bV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGFmdGVyLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICByZXR1cm4gdGhpcy5wYXJzZVRva2VuKGFmdGVyLCBwYXJzZWQpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHJldHVybiBwYXJzZWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFyc2VFcnJvcignTm8gY2xvc2luZyBicmFja2V0LicpO1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgY29uc3QgaSA9IHRva2VuLmluZGV4T2YoJy4nKTtcclxuICAgICAgICAgIHJldHVybiB0aGlzLnBhcnNlVG9rZW4odG9rZW4uc3Vic3RyaW5nKGkgKyAxKSwgY29udGV4dFt0b2tlbi5zdWJzdHJpbmcoMCwgaSldKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIGNvbnRleHRbdG9rZW5dO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNvbWJpbmVUb2tlbnModG9rZW5zOiBhbnlbXSkge1xyXG4gICAgbGV0IHJlc3VsdDogYW55O1xyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0b2tlbnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaWYgKGkgJSAyID09PSAxKSB7XHJcbiAgICAgICAgaWYgKHRva2Vuc1tpXSAhPT0gJysnKSB7XHJcbiAgICAgICAgICB0aGlzLnBhcnNlRXJyb3IoJ2Vycm9yJyk7XHJcbiAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKCFyZXN1bHQpIHtcclxuICAgICAgICAgIHJlc3VsdCA9IHRva2Vuc1tpXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgcmVzdWx0ICs9IHRva2Vuc1tpXTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiByZXN1bHQ7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHBhcnNlRXJyb3IobWVzc2FnZTogc3RyaW5nKSB7XHJcbiAgICBjb25zb2xlLmVycm9yKCdFdmFsUGlwZUVycm9yOiAnICsgbWVzc2FnZSk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=