/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
/**
 * A pipe that transforms a camel-cased string to words separated by space.
 */
var SplitCamelPipe = /** @class */ (function () {
    function SplitCamelPipe() {
    }
    /**
     * @param {?} str
     * @return {?}
     */
    SplitCamelPipe.prototype.transform = /**
     * @param {?} str
     * @return {?}
     */
    function (str) {
        /** @type {?} */
        var re = /([A-Z])([A-Z])([a-z])|([a-z])([A-Z])|([a-z])([0-9])/g;
        return str.replace(re, '$1$4$6 $2$3$5$7');
    };
    SplitCamelPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'splitCamel'
                },] }
    ];
    return SplitCamelPipe;
}());
export { SplitCamelPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3BsaXQtY2FtZWwucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvcGlwZS9zcGxpdC1jYW1lbC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQzs7OztBQUtwRDtJQUFBO0lBVUEsQ0FBQzs7Ozs7SUFMQyxrQ0FBUzs7OztJQUFULFVBQVUsR0FBVzs7WUFDYixFQUFFLEdBQUcsc0RBQXNEO1FBQ2pFLE9BQU8sR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztJQUM1QyxDQUFDOztnQkFSRixJQUFJLFNBQUM7b0JBQ0osSUFBSSxFQUFFLFlBQVk7aUJBQ25COztJQVFELHFCQUFDO0NBQUEsQUFWRCxJQVVDO1NBUFksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8qKlxyXG4gKiBBIHBpcGUgdGhhdCB0cmFuc2Zvcm1zIGEgY2FtZWwtY2FzZWQgc3RyaW5nIHRvIHdvcmRzIHNlcGFyYXRlZCBieSBzcGFjZS5cclxuICovXHJcbkBQaXBlKHtcclxuICBuYW1lOiAnc3BsaXRDYW1lbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNwbGl0Q2FtZWxQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcblxyXG4gIHRyYW5zZm9ybShzdHI6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBjb25zdCByZSA9IC8oW0EtWl0pKFtBLVpdKShbYS16XSl8KFthLXpdKShbQS1aXSl8KFthLXpdKShbMC05XSkvZztcclxuICAgIHJldHVybiBzdHIucmVwbGFjZShyZSwgJyQxJDQkNiAkMiQzJDUkNycpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19