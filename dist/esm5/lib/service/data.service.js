/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { apiRoute } from '../config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "./error.service";
/**
 * A service to send requests to Keps server.
 */
var DataService = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function DataService(http, errorService) {
        this.http = http;
        this.errorService = errorService;
        this.cache = {};
    }
    /**
     * Sends a GET request to a route of Keps server.
     * @param route Route to get from.
     * @param id ID of an object.
     * @param useCache Whether we should use cache or not.
     */
    /**
     * Sends a GET request to a route of Keps server.
     * @template T
     * @param {?} route Route to get from.
     * @param {?=} id ID of an object.
     * @param {?=} useCache Whether we should use cache or not.
     * @return {?}
     */
    DataService.prototype.get = /**
     * Sends a GET request to a route of Keps server.
     * @template T
     * @param {?} route Route to get from.
     * @param {?=} id ID of an object.
     * @param {?=} useCache Whether we should use cache or not.
     * @return {?}
     */
    function (route, id, useCache) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var getFromCache, getFromKeps;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                try {
                    if (id) {
                        route += '/' + id;
                    }
                    getFromCache = new Promise((/**
                     * @param {?} resolve
                     * @param {?} reject
                     * @return {?}
                     */
                    function (resolve, reject) {
                        if (useCache && _this.cache[route]) {
                            setTimeout((/**
                             * @return {?}
                             */
                            function () {
                                resolve(_this.cache[route]);
                            }), 3000);
                        }
                    }));
                    getFromKeps = this.http.get(apiRoute + route).toPromise()
                        .then((/**
                     * @param {?} result
                     * @return {?}
                     */
                    function (result) {
                        _this.cache[route] = result;
                        return result;
                    }));
                    return [2 /*return*/, Promise.race([getFromCache, getFromKeps])];
                }
                catch (err) {
                    throw this.errorService.normalizeError(err);
                }
                return [2 /*return*/];
            });
        });
    };
    /**
     * Sends a POST request to a route of Keps server.
     * @param route Route to post to.
     * @param data Data to be passed on to the route.
     */
    /**
     * Sends a POST request to a route of Keps server.
     * @template T
     * @param {?} route Route to post to.
     * @param {?} data Data to be passed on to the route.
     * @return {?}
     */
    DataService.prototype.post = /**
     * Sends a POST request to a route of Keps server.
     * @template T
     * @param {?} route Route to post to.
     * @param {?} data Data to be passed on to the route.
     * @return {?}
     */
    function (route, data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                try {
                    data = this.normalizeData(data);
                    return [2 /*return*/, this.http.post(apiRoute + route, data).toPromise()];
                }
                catch (err) {
                    throw this.errorService.normalizeError(err);
                }
                return [2 /*return*/];
            });
        });
    };
    /**
     * Sends a PUT request to a route of Keps server.
     * @param route Route to put to.
     * @param data Data to be passed on to the route.
     */
    /**
     * Sends a PUT request to a route of Keps server.
     * @template T
     * @param {?} route Route to put to.
     * @param {?} data Data to be passed on to the route.
     * @return {?}
     */
    DataService.prototype.put = /**
     * Sends a PUT request to a route of Keps server.
     * @template T
     * @param {?} route Route to put to.
     * @param {?} data Data to be passed on to the route.
     * @return {?}
     */
    function (route, data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                try {
                    data = this.normalizeData(data);
                    return [2 /*return*/, this.http.put(apiRoute + route, data).toPromise()];
                }
                catch (err) {
                    throw this.errorService.normalizeError(err);
                }
                return [2 /*return*/];
            });
        });
    };
    /**
     * Sends a DELETE request to a route of Keps server.
     * @param type Type of the object to delete (can also be just a route).
     * @param data An ID string or a Keps object to be deleted.
     */
    /**
     * Sends a DELETE request to a route of Keps server.
     * @template T
     * @param {?} type Type of the object to delete (can also be just a route).
     * @param {?} data An ID string or a Keps object to be deleted.
     * @return {?}
     */
    DataService.prototype.delete = /**
     * Sends a DELETE request to a route of Keps server.
     * @template T
     * @param {?} type Type of the object to delete (can also be just a route).
     * @param {?} data An ID string or a Keps object to be deleted.
     * @return {?}
     */
    function (type, data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var route;
            return tslib_1.__generator(this, function (_a) {
                try {
                    route = apiRoute;
                    if (typeof data === 'string') {
                        route += type + '/' + data;
                    }
                    else if (typeof data === 'object' && data._id) {
                        route += type + '/' + data._id;
                    }
                    else {
                        route += type;
                    }
                    return [2 /*return*/, this.http.delete(route).toPromise()];
                }
                catch (err) {
                    throw this.errorService.normalizeError(err);
                }
                return [2 /*return*/];
            });
        });
    };
    /**
     * Sends a request depending on the command to Keps server.
     * @param command A Keps command (e.g. 'put.users.update').
     * @param data Data to be passed on to the server.
     */
    /**
     * Sends a request depending on the command to Keps server.
     * @template T
     * @param {?} command A Keps command (e.g. 'put.users.update').
     * @param {?} data Data to be passed on to the server.
     * @return {?}
     */
    DataService.prototype.call = /**
     * Sends a request depending on the command to Keps server.
     * @template T
     * @param {?} command A Keps command (e.g. 'put.users.update').
     * @param {?} data Data to be passed on to the server.
     * @return {?}
     */
    function (command, data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _a, method, type, route, url, id, typeData;
            return tslib_1.__generator(this, function (_b) {
                try {
                    // Parse the command.
                    _a = tslib_1.__read(command.split('.'), 3), method = _a[0], type = _a[1], route = _a[2];
                    if (!method || !type || !route) {
                        throw new Error('Invalid Keps command.');
                    }
                    // Build URL based on data.
                    url = apiRoute + type + 's/';
                    if (data instanceof FormData) {
                        id = data.get(type);
                        if (id) {
                            data.delete(type);
                            url += id + '/' + route;
                        }
                        else {
                            url += route;
                        }
                    }
                    else if (typeof data === 'object') {
                        if (data[type]) {
                            typeData = data[type];
                            if (typeof typeData === 'object') {
                                url += typeData._id + '/' + route;
                                delete data[type];
                            }
                            else if (typeof typeData === 'string') {
                                url += typeData + '/' + route;
                                delete data[type];
                            }
                            else {
                                url += route;
                            }
                        }
                        else {
                            url += route;
                        }
                    }
                    else {
                        url += route;
                    }
                    // Truncate URL for specific routes.
                    if (['query', 'read', 'delete', 'update', 'create'].includes(route)) {
                        url = url.substr(0, url.length - route.length - 1);
                    }
                    // Send out requests.
                    if (method === 'get' || method === 'delete') {
                        url += '?' + this.serializeData(data);
                        return [2 /*return*/, this.http.request(method.toUpperCase(), url).toPromise()];
                    }
                    else if (method === 'post' || method === 'put') {
                        return [2 /*return*/, this.http.request(method.toUpperCase(), url, { body: data }).toPromise()];
                    }
                    else {
                        return [2 /*return*/, this.http.get(url).toPromise()];
                    }
                }
                catch (err) {
                    throw this.errorService.normalizeError(err);
                }
                return [2 /*return*/];
            });
        });
    };
    /**
     * Sends a GraphQL query request to the Keps server and returns the result back.
     *
     * Will throw an error if the result contains any error.
     * @param query A GraphQL query.
     */
    /**
     * Sends a GraphQL query request to the Keps server and returns the result back.
     *
     * Will throw an error if the result contains any error.
     * @param {?} query A GraphQL query.
     * @return {?}
     */
    DataService.prototype.graphql = /**
     * Sends a GraphQL query request to the Keps server and returns the result back.
     *
     * Will throw an error if the result contains any error.
     * @param {?} query A GraphQL query.
     * @return {?}
     */
    function (query) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var cleanQuery, result, err_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        // Clean up whitespaces in query.
                        cleanQuery = query.replace(/([^"]+)|("[^"]+")/g, (/**
                         * @param {?} $0
                         * @param {?} $1
                         * @param {?} $2
                         * @return {?}
                         */
                        function ($0, $1, $2) {
                            if ($1) {
                                return $1.replace(/\s/g, '');
                            }
                            else {
                                return $2;
                            }
                        }));
                        // Send a GraphQL request and throw error if any.
                        return [4 /*yield*/, this.http.get(apiRoute + ("graphqls?q={" + cleanQuery + "}")).toPromise()];
                    case 1:
                        result = _a.sent();
                        if (result.errors) {
                            throw new Error(result.errors[0]);
                        }
                        return [2 /*return*/, result];
                    case 2:
                        err_1 = _a.sent();
                        throw this.errorService.normalizeError(err_1);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Clears all stored cache.
     */
    /**
     * Clears all stored cache.
     * @return {?}
     */
    DataService.prototype.clearCache = /**
     * Clears all stored cache.
     * @return {?}
     */
    function () {
        this.cache = {};
    };
    /**
     * Converts data to FormData if it contains a File or Blob.
     * @param data Data to be normalized.
     */
    /**
     * Converts data to FormData if it contains a File or Blob.
     * @private
     * @param {?} data Data to be normalized.
     * @return {?}
     */
    DataService.prototype.normalizeData = /**
     * Converts data to FormData if it contains a File or Blob.
     * @private
     * @param {?} data Data to be normalized.
     * @return {?}
     */
    function (data) {
        var e_1, _a;
        if (!(data instanceof FormData)) {
            // Check if data contains a File or Blob.
            if (Object.values(data).some((/**
             * @param {?} value
             * @return {?}
             */
            function (value) { return value instanceof File || value instanceof Blob; }))) {
                // Convert to FormData and return it.
                /** @type {?} */
                var formData = new FormData();
                try {
                    for (var _b = tslib_1.__values(Object.entries(data)), _c = _b.next(); !_c.done; _c = _b.next()) {
                        var _d = tslib_1.__read(_c.value, 2), key = _d[0], value = _d[1];
                        formData.set(key, value);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                return formData;
            }
        }
        return data;
    };
    /**
     * Serializes data and returns it as a query string.
     * @param data Data to be serialized.
     * @param prefix Prefix for the passed data.
     */
    /**
     * Serializes data and returns it as a query string.
     * @private
     * @param {?} data Data to be serialized.
     * @param {?=} prefix Prefix for the passed data.
     * @return {?}
     */
    DataService.prototype.serializeData = /**
     * Serializes data and returns it as a query string.
     * @private
     * @param {?} data Data to be serialized.
     * @param {?=} prefix Prefix for the passed data.
     * @return {?}
     */
    function (data, prefix) {
        /** @type {?} */
        var str = [];
        for (var p in data) {
            /** @type {?} */
            var k = prefix ? prefix + '[' + p + ']' : p;
            /** @type {?} */
            var v = data[p];
            str.push(typeof v === 'object' ?
                this.serializeData(v, k) :
                encodeURIComponent(k) + '=' + encodeURIComponent(v));
        }
        return str.join('&');
    };
    DataService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    DataService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: ErrorService }
    ]; };
    /** @nocollapse */ DataService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DataService_Factory() { return new DataService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.ErrorService)); }, token: DataService, providedIn: "root" });
    return DataService;
}());
export { DataService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DataService.prototype.cache;
    /**
     * @type {?}
     * @private
     */
    DataService.prototype.http;
    /**
     * @type {?}
     * @private
     */
    DataService.prototype.errorService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlL2RhdGEuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sV0FBVyxDQUFDOzs7Ozs7O0FBTXJDO0lBTUU7O09BRUc7SUFDSCxxQkFDVSxJQUFnQixFQUNoQixZQUEwQjtRQUQxQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBUDVCLFVBQUssR0FBd0IsRUFBRSxDQUFDO0lBUXBDLENBQUM7SUFFTDs7Ozs7T0FLRzs7Ozs7Ozs7O0lBQ0cseUJBQUc7Ozs7Ozs7O0lBQVQsVUFBbUIsS0FBYSxFQUFFLEVBQVcsRUFBRSxRQUFrQjs7Ozs7Z0JBQy9ELElBQUk7b0JBQ0YsSUFBSSxFQUFFLEVBQUU7d0JBQ04sS0FBSyxJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7cUJBQ25CO29CQUVLLFlBQVksR0FBZSxJQUFJLE9BQU87Ozs7O29CQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07d0JBQzNELElBQUksUUFBUSxJQUFJLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUU7NEJBQ2pDLFVBQVU7Ozs0QkFBQztnQ0FDVCxPQUFPLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzRCQUM3QixDQUFDLEdBQUUsSUFBSSxDQUFDLENBQUM7eUJBQ1Y7b0JBQ0gsQ0FBQyxFQUFDO29CQUVJLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBSSxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsU0FBUyxFQUFFO3lCQUNqRSxJQUFJOzs7O29CQUFDLFVBQUEsTUFBTTt3QkFDVixLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLE1BQU0sQ0FBQzt3QkFDM0IsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQztvQkFFRixzQkFBTyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsWUFBWSxFQUFFLFdBQVcsQ0FBQyxDQUFDLEVBQUM7aUJBQ2xEO2dCQUFDLE9BQU8sR0FBRyxFQUFFO29CQUNaLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQzdDOzs7O0tBQ0Y7SUFFRDs7OztPQUlHOzs7Ozs7OztJQUNHLDBCQUFJOzs7Ozs7O0lBQVYsVUFBb0IsS0FBYSxFQUFFLElBQVM7OztnQkFDMUMsSUFBSTtvQkFDRixJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDaEMsc0JBQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUksUUFBUSxHQUFHLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQztpQkFDOUQ7Z0JBQUMsT0FBTyxHQUFHLEVBQUU7b0JBQ1osTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDN0M7Ozs7S0FDRjtJQUVEOzs7O09BSUc7Ozs7Ozs7O0lBQ0cseUJBQUc7Ozs7Ozs7SUFBVCxVQUFtQixLQUFhLEVBQUUsSUFBUzs7O2dCQUN6QyxJQUFJO29CQUNGLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNoQyxzQkFBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBSSxRQUFRLEdBQUcsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFDO2lCQUM3RDtnQkFBQyxPQUFPLEdBQUcsRUFBRTtvQkFDWixNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUM3Qzs7OztLQUNGO0lBRUQ7Ozs7T0FJRzs7Ozs7Ozs7SUFDRyw0QkFBTTs7Ozs7OztJQUFaLFVBQXVCLElBQVksRUFBRSxJQUF5Qjs7OztnQkFDNUQsSUFBSTtvQkFDRSxLQUFLLEdBQUcsUUFBUTtvQkFDcEIsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7d0JBQzVCLEtBQUssSUFBSSxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQztxQkFDNUI7eUJBQU0sSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRTt3QkFDL0MsS0FBSyxJQUFJLElBQUksR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztxQkFDaEM7eUJBQU07d0JBQ0wsS0FBSyxJQUFJLElBQUksQ0FBQztxQkFDZjtvQkFDRCxzQkFBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBSSxLQUFLLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQztpQkFDL0M7Z0JBQUMsT0FBTyxHQUFHLEVBQUU7b0JBQ1osTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDN0M7Ozs7S0FDRjtJQUVEOzs7O09BSUc7Ozs7Ozs7O0lBQ0csMEJBQUk7Ozs7Ozs7SUFBVixVQUFvQixPQUFlLEVBQUUsSUFBb0I7Ozs7Z0JBQ3ZELElBQUk7O29CQUVJLEtBQUEsZUFBd0IsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBQSxFQUF6QyxNQUFNLFFBQUEsRUFBRSxJQUFJLFFBQUEsRUFBRSxLQUFLLFFBQUE7b0JBQzFCLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7d0JBQzlCLE1BQU0sSUFBSSxLQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQztxQkFDMUM7O29CQUdHLEdBQUcsR0FBRyxRQUFRLEdBQUcsSUFBSSxHQUFHLElBQUk7b0JBQ2hDLElBQUksSUFBSSxZQUFZLFFBQVEsRUFBRTt3QkFDdEIsRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDO3dCQUN6QixJQUFJLEVBQUUsRUFBRTs0QkFDTixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNsQixHQUFHLElBQUksRUFBRSxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUM7eUJBQ3pCOzZCQUFNOzRCQUNMLEdBQUcsSUFBSSxLQUFLLENBQUM7eUJBQ2Q7cUJBQ0Y7eUJBQU0sSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7d0JBQ25DLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFOzRCQUNSLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDOzRCQUMzQixJQUFJLE9BQU8sUUFBUSxLQUFLLFFBQVEsRUFBRTtnQ0FDaEMsR0FBRyxJQUFJLFFBQVEsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEtBQUssQ0FBQztnQ0FDbEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7NkJBQ25CO2lDQUFNLElBQUksT0FBTyxRQUFRLEtBQUssUUFBUSxFQUFFO2dDQUN2QyxHQUFHLElBQUksUUFBUSxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUM7Z0NBQzlCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUNuQjtpQ0FBTTtnQ0FDTCxHQUFHLElBQUksS0FBSyxDQUFDOzZCQUNkO3lCQUNGOzZCQUFNOzRCQUNMLEdBQUcsSUFBSSxLQUFLLENBQUM7eUJBQ2Q7cUJBQ0Y7eUJBQU07d0JBQ0wsR0FBRyxJQUFJLEtBQUssQ0FBQztxQkFDZDtvQkFFRCxvQ0FBb0M7b0JBQ3BDLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUNuRSxHQUFHLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO3FCQUNwRDtvQkFFRCxxQkFBcUI7b0JBQ3JCLElBQUksTUFBTSxLQUFLLEtBQUssSUFBSSxNQUFNLEtBQUssUUFBUSxFQUFFO3dCQUMzQyxHQUFHLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ3RDLHNCQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQztxQkFDcEU7eUJBQU0sSUFBSSxNQUFNLEtBQUssTUFBTSxJQUFJLE1BQU0sS0FBSyxLQUFLLEVBQUU7d0JBQ2hELHNCQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQztxQkFDcEY7eUJBQU07d0JBQ0wsc0JBQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUksR0FBRyxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUM7cUJBQzFDO2lCQUNGO2dCQUFDLE9BQU8sR0FBRyxFQUFFO29CQUNaLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQzdDOzs7O0tBQ0Y7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDRyw2QkFBTzs7Ozs7OztJQUFiLFVBQWMsS0FBYTs7Ozs7Ozs7d0JBR2pCLFVBQVUsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLG9CQUFvQjs7Ozs7O3dCQUFFLFVBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFOzRCQUNoRSxJQUFJLEVBQUUsRUFBRTtnQ0FDTixPQUFPLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDOzZCQUM5QjtpQ0FBTTtnQ0FDTCxPQUFPLEVBQUUsQ0FBQzs2QkFDWDt3QkFDSCxDQUFDLEVBQUM7O3dCQUdhLHFCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUNoQyxRQUFRLElBQUcsaUJBQWUsVUFBVSxNQUFHLENBQUEsQ0FDeEMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBRlAsTUFBTSxHQUFHLFNBRUY7d0JBQ2IsSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFOzRCQUNqQixNQUFNLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt5QkFDbkM7d0JBQ0Qsc0JBQU8sTUFBTSxFQUFDOzs7d0JBRWQsTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxLQUFHLENBQUMsQ0FBQzs7Ozs7S0FFL0M7SUFFRDs7T0FFRzs7Ozs7SUFDSCxnQ0FBVTs7OztJQUFWO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7OztJQUNLLG1DQUFhOzs7Ozs7SUFBckIsVUFBc0IsSUFBWTs7UUFDaEMsSUFBSSxDQUFDLENBQUMsSUFBSSxZQUFZLFFBQVEsQ0FBQyxFQUFFO1lBQy9CLHlDQUF5QztZQUN6QyxJQUNFLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxZQUFZLElBQUksSUFBSSxLQUFLLFlBQVksSUFBSSxFQUE5QyxDQUE4QyxFQUFDLEVBQ2pGOzs7b0JBRU0sUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFOztvQkFDL0IsS0FBMkIsSUFBQSxLQUFBLGlCQUFBLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUEsZ0JBQUEsNEJBQUU7d0JBQXRDLElBQUEsZ0NBQVksRUFBWCxXQUFHLEVBQUUsYUFBSzt3QkFDcEIsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7cUJBQzFCOzs7Ozs7Ozs7Z0JBQ0QsT0FBTyxRQUFRLENBQUM7YUFDakI7U0FDRjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7O0lBQ0ssbUNBQWE7Ozs7Ozs7SUFBckIsVUFBc0IsSUFBWSxFQUFFLE1BQWU7O1lBQzNDLEdBQUcsR0FBRyxFQUFFO1FBQ2QsS0FBSyxJQUFNLENBQUMsSUFBSSxJQUFJLEVBQUU7O2dCQUNkLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7Z0JBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDMUQsR0FBRyxDQUFDLElBQUksQ0FDTixPQUFPLENBQUMsS0FBSyxRQUFRLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDMUIsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUNwRCxDQUFDO1NBQ0g7UUFDRCxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdkIsQ0FBQzs7Z0JBdE9GLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7Z0JBVlEsVUFBVTtnQkFDVixZQUFZOzs7c0JBRnJCO0NBZ1BDLEFBdk9ELElBdU9DO1NBcE9ZLFdBQVc7Ozs7OztJQUN0Qiw0QkFBd0M7Ozs7O0lBTXRDLDJCQUF3Qjs7Ozs7SUFDeEIsbUNBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBFcnJvclNlcnZpY2UgfSBmcm9tICcuL2Vycm9yLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBhcGlSb3V0ZSB9IGZyb20gJy4uL2NvbmZpZyc7XHJcbmltcG9ydCB7IEtlcHNPYmplY3QsIEdyYXBocWxSZXN1bHQgfSBmcm9tICcuLi9pbnRlcmZhY2UnO1xyXG5cclxuLyoqXHJcbiAqIEEgc2VydmljZSB0byBzZW5kIHJlcXVlc3RzIHRvIEtlcHMgc2VydmVyLlxyXG4gKi9cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRGF0YVNlcnZpY2Uge1xyXG4gIHByaXZhdGUgY2FjaGU6IFJlY29yZDxzdHJpbmcsIGFueT4gPSB7fTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXHJcbiAgICBwcml2YXRlIGVycm9yU2VydmljZTogRXJyb3JTZXJ2aWNlXHJcbiAgKSB7IH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2VuZHMgYSBHRVQgcmVxdWVzdCB0byBhIHJvdXRlIG9mIEtlcHMgc2VydmVyLlxyXG4gICAqIEBwYXJhbSByb3V0ZSBSb3V0ZSB0byBnZXQgZnJvbS5cclxuICAgKiBAcGFyYW0gaWQgSUQgb2YgYW4gb2JqZWN0LlxyXG4gICAqIEBwYXJhbSB1c2VDYWNoZSBXaGV0aGVyIHdlIHNob3VsZCB1c2UgY2FjaGUgb3Igbm90LlxyXG4gICAqL1xyXG4gIGFzeW5jIGdldDxUID0gYW55Pihyb3V0ZTogc3RyaW5nLCBpZD86IHN0cmluZywgdXNlQ2FjaGU/OiBib29sZWFuKTogUHJvbWlzZTxUPiB7XHJcbiAgICB0cnkge1xyXG4gICAgICBpZiAoaWQpIHtcclxuICAgICAgICByb3V0ZSArPSAnLycgKyBpZDtcclxuICAgICAgfVxyXG5cclxuICAgICAgY29uc3QgZ2V0RnJvbUNhY2hlOiBQcm9taXNlPFQ+ID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgIGlmICh1c2VDYWNoZSAmJiB0aGlzLmNhY2hlW3JvdXRlXSkge1xyXG4gICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHJlc29sdmUodGhpcy5jYWNoZVtyb3V0ZV0pO1xyXG4gICAgICAgICAgfSwgMzAwMCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGNvbnN0IGdldEZyb21LZXBzID0gdGhpcy5odHRwLmdldDxUPihhcGlSb3V0ZSArIHJvdXRlKS50b1Byb21pc2UoKVxyXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgIHRoaXMuY2FjaGVbcm91dGVdID0gcmVzdWx0O1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgcmV0dXJuIFByb21pc2UucmFjZShbZ2V0RnJvbUNhY2hlLCBnZXRGcm9tS2Vwc10pO1xyXG4gICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgIHRocm93IHRoaXMuZXJyb3JTZXJ2aWNlLm5vcm1hbGl6ZUVycm9yKGVycik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZW5kcyBhIFBPU1QgcmVxdWVzdCB0byBhIHJvdXRlIG9mIEtlcHMgc2VydmVyLlxyXG4gICAqIEBwYXJhbSByb3V0ZSBSb3V0ZSB0byBwb3N0IHRvLlxyXG4gICAqIEBwYXJhbSBkYXRhIERhdGEgdG8gYmUgcGFzc2VkIG9uIHRvIHRoZSByb3V0ZS5cclxuICAgKi9cclxuICBhc3luYyBwb3N0PFQgPSBhbnk+KHJvdXRlOiBzdHJpbmcsIGRhdGE6IGFueSk6IFByb21pc2U8VD4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgZGF0YSA9IHRoaXMubm9ybWFsaXplRGF0YShkYXRhKTtcclxuICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0PFQ+KGFwaVJvdXRlICsgcm91dGUsIGRhdGEpLnRvUHJvbWlzZSgpO1xyXG4gICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgIHRocm93IHRoaXMuZXJyb3JTZXJ2aWNlLm5vcm1hbGl6ZUVycm9yKGVycik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZW5kcyBhIFBVVCByZXF1ZXN0IHRvIGEgcm91dGUgb2YgS2VwcyBzZXJ2ZXIuXHJcbiAgICogQHBhcmFtIHJvdXRlIFJvdXRlIHRvIHB1dCB0by5cclxuICAgKiBAcGFyYW0gZGF0YSBEYXRhIHRvIGJlIHBhc3NlZCBvbiB0byB0aGUgcm91dGUuXHJcbiAgICovXHJcbiAgYXN5bmMgcHV0PFQgPSBhbnk+KHJvdXRlOiBzdHJpbmcsIGRhdGE6IGFueSk6IFByb21pc2U8VD4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgZGF0YSA9IHRoaXMubm9ybWFsaXplRGF0YShkYXRhKTtcclxuICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wdXQ8VD4oYXBpUm91dGUgKyByb3V0ZSwgZGF0YSkudG9Qcm9taXNlKCk7XHJcbiAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgdGhyb3cgdGhpcy5lcnJvclNlcnZpY2Uubm9ybWFsaXplRXJyb3IoZXJyKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNlbmRzIGEgREVMRVRFIHJlcXVlc3QgdG8gYSByb3V0ZSBvZiBLZXBzIHNlcnZlci5cclxuICAgKiBAcGFyYW0gdHlwZSBUeXBlIG9mIHRoZSBvYmplY3QgdG8gZGVsZXRlIChjYW4gYWxzbyBiZSBqdXN0IGEgcm91dGUpLlxyXG4gICAqIEBwYXJhbSBkYXRhIEFuIElEIHN0cmluZyBvciBhIEtlcHMgb2JqZWN0IHRvIGJlIGRlbGV0ZWQuXHJcbiAgICovXHJcbiAgYXN5bmMgZGVsZXRlPFQgPSB2b2lkPih0eXBlOiBzdHJpbmcsIGRhdGE6IHN0cmluZyB8IEtlcHNPYmplY3QpOiBQcm9taXNlPFQ+IHtcclxuICAgIHRyeSB7XHJcbiAgICAgIGxldCByb3V0ZSA9IGFwaVJvdXRlO1xyXG4gICAgICBpZiAodHlwZW9mIGRhdGEgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgcm91dGUgKz0gdHlwZSArICcvJyArIGRhdGE7XHJcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGRhdGEgPT09ICdvYmplY3QnICYmIGRhdGEuX2lkKSB7XHJcbiAgICAgICAgcm91dGUgKz0gdHlwZSArICcvJyArIGRhdGEuX2lkO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJvdXRlICs9IHR5cGU7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHRoaXMuaHR0cC5kZWxldGU8VD4ocm91dGUpLnRvUHJvbWlzZSgpO1xyXG4gICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgIHRocm93IHRoaXMuZXJyb3JTZXJ2aWNlLm5vcm1hbGl6ZUVycm9yKGVycik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZW5kcyBhIHJlcXVlc3QgZGVwZW5kaW5nIG9uIHRoZSBjb21tYW5kIHRvIEtlcHMgc2VydmVyLlxyXG4gICAqIEBwYXJhbSBjb21tYW5kIEEgS2VwcyBjb21tYW5kIChlLmcuICdwdXQudXNlcnMudXBkYXRlJykuXHJcbiAgICogQHBhcmFtIGRhdGEgRGF0YSB0byBiZSBwYXNzZWQgb24gdG8gdGhlIHNlcnZlci5cclxuICAgKi9cclxuICBhc3luYyBjYWxsPFQgPSBhbnk+KGNvbW1hbmQ6IHN0cmluZywgZGF0YTogRm9ybURhdGEgfCBhbnkpOiBQcm9taXNlPFQ+IHtcclxuICAgIHRyeSB7XHJcbiAgICAgIC8vIFBhcnNlIHRoZSBjb21tYW5kLlxyXG4gICAgICBjb25zdCBbbWV0aG9kLCB0eXBlLCByb3V0ZV0gPSBjb21tYW5kLnNwbGl0KCcuJyk7XHJcbiAgICAgIGlmICghbWV0aG9kIHx8ICF0eXBlIHx8ICFyb3V0ZSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCBLZXBzIGNvbW1hbmQuJyk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIEJ1aWxkIFVSTCBiYXNlZCBvbiBkYXRhLlxyXG4gICAgICBsZXQgdXJsID0gYXBpUm91dGUgKyB0eXBlICsgJ3MvJztcclxuICAgICAgaWYgKGRhdGEgaW5zdGFuY2VvZiBGb3JtRGF0YSkge1xyXG4gICAgICAgIGNvbnN0IGlkID0gZGF0YS5nZXQodHlwZSk7XHJcbiAgICAgICAgaWYgKGlkKSB7XHJcbiAgICAgICAgICBkYXRhLmRlbGV0ZSh0eXBlKTtcclxuICAgICAgICAgIHVybCArPSBpZCArICcvJyArIHJvdXRlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB1cmwgKz0gcm91dGU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKHR5cGVvZiBkYXRhID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgIGlmIChkYXRhW3R5cGVdKSB7XHJcbiAgICAgICAgICBjb25zdCB0eXBlRGF0YSA9IGRhdGFbdHlwZV07XHJcbiAgICAgICAgICBpZiAodHlwZW9mIHR5cGVEYXRhID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICB1cmwgKz0gdHlwZURhdGEuX2lkICsgJy8nICsgcm91dGU7XHJcbiAgICAgICAgICAgIGRlbGV0ZSBkYXRhW3R5cGVdO1xyXG4gICAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgdHlwZURhdGEgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIHVybCArPSB0eXBlRGF0YSArICcvJyArIHJvdXRlO1xyXG4gICAgICAgICAgICBkZWxldGUgZGF0YVt0eXBlXTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHVybCArPSByb3V0ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdXJsICs9IHJvdXRlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB1cmwgKz0gcm91dGU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIFRydW5jYXRlIFVSTCBmb3Igc3BlY2lmaWMgcm91dGVzLlxyXG4gICAgICBpZiAoWydxdWVyeScsICdyZWFkJywgJ2RlbGV0ZScsICd1cGRhdGUnLCAnY3JlYXRlJ10uaW5jbHVkZXMocm91dGUpKSB7XHJcbiAgICAgICAgdXJsID0gdXJsLnN1YnN0cigwLCB1cmwubGVuZ3RoIC0gcm91dGUubGVuZ3RoIC0gMSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIFNlbmQgb3V0IHJlcXVlc3RzLlxyXG4gICAgICBpZiAobWV0aG9kID09PSAnZ2V0JyB8fCBtZXRob2QgPT09ICdkZWxldGUnKSB7XHJcbiAgICAgICAgdXJsICs9ICc/JyArIHRoaXMuc2VyaWFsaXplRGF0YShkYXRhKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnJlcXVlc3Q8VD4obWV0aG9kLnRvVXBwZXJDYXNlKCksIHVybCkudG9Qcm9taXNlKCk7XHJcbiAgICAgIH0gZWxzZSBpZiAobWV0aG9kID09PSAncG9zdCcgfHwgbWV0aG9kID09PSAncHV0Jykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucmVxdWVzdDxUPihtZXRob2QudG9VcHBlckNhc2UoKSwgdXJsLCB7IGJvZHk6IGRhdGEgfSkudG9Qcm9taXNlKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8VD4odXJsKS50b1Byb21pc2UoKTtcclxuICAgICAgfVxyXG4gICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgIHRocm93IHRoaXMuZXJyb3JTZXJ2aWNlLm5vcm1hbGl6ZUVycm9yKGVycik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZW5kcyBhIEdyYXBoUUwgcXVlcnkgcmVxdWVzdCB0byB0aGUgS2VwcyBzZXJ2ZXIgYW5kIHJldHVybnMgdGhlIHJlc3VsdCBiYWNrLlxyXG4gICAqXHJcbiAgICogV2lsbCB0aHJvdyBhbiBlcnJvciBpZiB0aGUgcmVzdWx0IGNvbnRhaW5zIGFueSBlcnJvci5cclxuICAgKiBAcGFyYW0gcXVlcnkgQSBHcmFwaFFMIHF1ZXJ5LlxyXG4gICAqL1xyXG4gIGFzeW5jIGdyYXBocWwocXVlcnk6IHN0cmluZyk6IFByb21pc2U8R3JhcGhxbFJlc3VsdD4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgLy8gQ2xlYW4gdXAgd2hpdGVzcGFjZXMgaW4gcXVlcnkuXHJcbiAgICAgIGNvbnN0IGNsZWFuUXVlcnkgPSBxdWVyeS5yZXBsYWNlKC8oW15cIl0rKXwoXCJbXlwiXStcIikvZywgKCQwLCAkMSwgJDIpID0+IHtcclxuICAgICAgICBpZiAoJDEpIHtcclxuICAgICAgICAgIHJldHVybiAkMS5yZXBsYWNlKC9cXHMvZywgJycpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICByZXR1cm4gJDI7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIC8vIFNlbmQgYSBHcmFwaFFMIHJlcXVlc3QgYW5kIHRocm93IGVycm9yIGlmIGFueS5cclxuICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgdGhpcy5odHRwLmdldDxHcmFwaHFsUmVzdWx0PihcclxuICAgICAgICBhcGlSb3V0ZSArIGBncmFwaHFscz9xPXske2NsZWFuUXVlcnl9fWBcclxuICAgICAgKS50b1Byb21pc2UoKTtcclxuICAgICAgaWYgKHJlc3VsdC5lcnJvcnMpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IocmVzdWx0LmVycm9yc1swXSk7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICB0aHJvdyB0aGlzLmVycm9yU2VydmljZS5ub3JtYWxpemVFcnJvcihlcnIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ2xlYXJzIGFsbCBzdG9yZWQgY2FjaGUuXHJcbiAgICovXHJcbiAgY2xlYXJDYWNoZSgpIHtcclxuICAgIHRoaXMuY2FjaGUgPSB7fTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnZlcnRzIGRhdGEgdG8gRm9ybURhdGEgaWYgaXQgY29udGFpbnMgYSBGaWxlIG9yIEJsb2IuXHJcbiAgICogQHBhcmFtIGRhdGEgRGF0YSB0byBiZSBub3JtYWxpemVkLlxyXG4gICAqL1xyXG4gIHByaXZhdGUgbm9ybWFsaXplRGF0YShkYXRhOiBvYmplY3QpOiBvYmplY3QgfCBGb3JtRGF0YSB7XHJcbiAgICBpZiAoIShkYXRhIGluc3RhbmNlb2YgRm9ybURhdGEpKSB7XHJcbiAgICAgIC8vIENoZWNrIGlmIGRhdGEgY29udGFpbnMgYSBGaWxlIG9yIEJsb2IuXHJcbiAgICAgIGlmIChcclxuICAgICAgICBPYmplY3QudmFsdWVzKGRhdGEpLnNvbWUodmFsdWUgPT4gdmFsdWUgaW5zdGFuY2VvZiBGaWxlIHx8IHZhbHVlIGluc3RhbmNlb2YgQmxvYilcclxuICAgICAgKSB7XHJcbiAgICAgICAgLy8gQ29udmVydCB0byBGb3JtRGF0YSBhbmQgcmV0dXJuIGl0LlxyXG4gICAgICAgIGNvbnN0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgZm9yIChjb25zdCBba2V5LCB2YWx1ZV0gb2YgT2JqZWN0LmVudHJpZXMoZGF0YSkpIHtcclxuICAgICAgICAgIGZvcm1EYXRhLnNldChrZXksIHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZvcm1EYXRhO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZGF0YTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNlcmlhbGl6ZXMgZGF0YSBhbmQgcmV0dXJucyBpdCBhcyBhIHF1ZXJ5IHN0cmluZy5cclxuICAgKiBAcGFyYW0gZGF0YSBEYXRhIHRvIGJlIHNlcmlhbGl6ZWQuXHJcbiAgICogQHBhcmFtIHByZWZpeCBQcmVmaXggZm9yIHRoZSBwYXNzZWQgZGF0YS5cclxuICAgKi9cclxuICBwcml2YXRlIHNlcmlhbGl6ZURhdGEoZGF0YTogb2JqZWN0LCBwcmVmaXg/OiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgY29uc3Qgc3RyID0gW107XHJcbiAgICBmb3IgKGNvbnN0IHAgaW4gZGF0YSkge1xyXG4gICAgICBjb25zdCBrID0gcHJlZml4ID8gcHJlZml4ICsgJ1snICsgcCArICddJyA6IHAsIHYgPSBkYXRhW3BdO1xyXG4gICAgICBzdHIucHVzaChcclxuICAgICAgICB0eXBlb2YgdiA9PT0gJ29iamVjdCcgP1xyXG4gICAgICAgIHRoaXMuc2VyaWFsaXplRGF0YSh2LCBrKSA6XHJcbiAgICAgICAgZW5jb2RlVVJJQ29tcG9uZW50KGspICsgJz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHYpXHJcbiAgICAgICk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gc3RyLmpvaW4oJyYnKTtcclxuICB9XHJcbn1cclxuIl19