/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject } from 'rxjs';
import { webAlias, apiRoute } from '../config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
/** @type {?} */
var storageKey = webAlias + '-user';
/**
 * A service that handles authentication with a Keps server.
 */
var AuthService = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function AuthService(http) {
        this.http = http;
        this.user$ = new ReplaySubject(1);
        this.loggedIn$ = new ReplaySubject(1);
    }
    /**
     * Starts the auth service.
     * Should be called only when the app is bootstrapping.
     */
    /**
     * Starts the auth service.
     * Should be called only when the app is bootstrapping.
     * @return {?}
     */
    AuthService.prototype.start = /**
     * Starts the auth service.
     * Should be called only when the app is bootstrapping.
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var storageUser, serverUser, err_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // Try getting user from storage.
                        try {
                            storageUser = JSON.parse(localStorage.getItem(storageKey));
                        }
                        catch (err) {
                            console.error('Error getting user from local storage');
                            this.setUser(null);
                        }
                        if (!storageUser) return [3 /*break*/, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.http.get(apiRoute + 'users/me').toPromise()];
                    case 2:
                        serverUser = _a.sent();
                        if (serverUser.token) {
                            this.setUser(serverUser);
                        }
                        else if (storageUser.tokenExpires > new Date().getTime()) {
                            this.setUser(storageUser);
                        }
                        else {
                            // Token expired.
                            this.user$.error(serverUser);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        // Token error.
                        this.user$.error(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        this.setUser(null);
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Saves/removes user and emits the user and logged in status to observables.
     * @internal Do not use!
     * @param user A user or null
     */
    /**
     * Saves/removes user and emits the user and logged in status to observables.
     * \@internal Do not use!
     * @param {?=} user A user or null
     * @return {?}
     */
    AuthService.prototype.setUser = /**
     * Saves/removes user and emits the user and logged in status to observables.
     * \@internal Do not use!
     * @param {?=} user A user or null
     * @return {?}
     */
    function (user) {
        if (user) {
            localStorage.setItem(storageKey, JSON.stringify(user));
            this.user = user;
            this.user$.next(user);
            this.loggedIn$.next(true);
        }
        else {
            localStorage.removeItem(storageKey);
            this.user = null;
            this.user$.next(null);
            this.loggedIn$.next(false);
        }
    };
    /**
     * Returns the current user.
     */
    /**
     * Returns the current user.
     * @return {?}
     */
    AuthService.prototype.getUser = /**
     * Returns the current user.
     * @return {?}
     */
    function () {
        return this.user;
    };
    /**
     * Returns an observable that will emit the current user.
     */
    /**
     * Returns an observable that will emit the current user.
     * @return {?}
     */
    AuthService.prototype.getUser$ = /**
     * Returns an observable that will emit the current user.
     * @return {?}
     */
    function () {
        return this.user$.asObservable();
    };
    /**
     * Returns token from the user.
     */
    /**
     * Returns token from the user.
     * @return {?}
     */
    AuthService.prototype.getToken = /**
     * Returns token from the user.
     * @return {?}
     */
    function () {
        // If user is stored in auth service, use that user's token.
        if (this.user) {
            return this.user.token;
        }
        else {
            // Otherwise, try getting from the storage.
            try {
                /** @type {?} */
                var storageUser = JSON.parse(localStorage.getItem(storageKey));
                return storageUser.token;
            }
            catch (err) {
                console.error('Error getting user from local storage');
                return null;
            }
        }
    };
    /**
     * Returns an observable that emits logged in status.
     */
    /**
     * Returns an observable that emits logged in status.
     * @return {?}
     */
    AuthService.prototype.isLoggedIn$ = /**
     * Returns an observable that emits logged in status.
     * @return {?}
     */
    function () {
        return this.loggedIn$.asObservable();
    };
    /**
     * Logs in using the passed in provider name.
     * @param provider Provider to use to login.
     * @param data Data to be passed on to the provider.
     */
    /**
     * Logs in using the passed in provider name.
     * @param {?} provider Provider to use to login.
     * @param {?=} data Data to be passed on to the provider.
     * @return {?}
     */
    AuthService.prototype.loginWithProvider = /**
     * Logs in using the passed in provider name.
     * @param {?} provider Provider to use to login.
     * @param {?=} data Data to be passed on to the provider.
     * @return {?}
     */
    function (provider, data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var serverUser;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(provider === 'local')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.http.post(apiRoute + 'users/signin', data).toPromise()];
                    case 1:
                        serverUser = _a.sent();
                        this.setUser(serverUser);
                        return [2 /*return*/, this.user];
                    case 2:
                        window.location.href = apiRoute + 'oauths/' + provider;
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Signs up user using the passed in provider name.
     * @param provider Provider to use to sign up.
     * @param data Data to be passed on to the provider.
     * @param customRoute Custom route to use instead of the default 'users/signup'.
     */
    /**
     * Signs up user using the passed in provider name.
     * @param {?} provider Provider to use to sign up.
     * @param {?=} data Data to be passed on to the provider.
     * @param {?=} customRoute Custom route to use instead of the default 'users/signup'.
     * @return {?}
     */
    AuthService.prototype.signupWithProvider = /**
     * Signs up user using the passed in provider name.
     * @param {?} provider Provider to use to sign up.
     * @param {?=} data Data to be passed on to the provider.
     * @param {?=} customRoute Custom route to use instead of the default 'users/signup'.
     * @return {?}
     */
    function (provider, data, customRoute) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var route, serverUser;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(provider === 'local')) return [3 /*break*/, 2];
                        route = apiRoute + customRoute || 'users/signup';
                        return [4 /*yield*/, this.http.post(route, data).toPromise()];
                    case 1:
                        serverUser = _a.sent();
                        if (data.noLogin) {
                            return [2 /*return*/, serverUser];
                        }
                        else {
                            this.setUser(serverUser);
                            return [2 /*return*/, this.user];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        window.location.href = webAlias + '/auth/' + provider;
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Switch current user.
     */
    /**
     * Switch current user.
     * @param {?} newUser
     * @return {?}
     */
    AuthService.prototype.switchUser = /**
     * Switch current user.
     * @param {?} newUser
     * @return {?}
     */
    function (newUser) {
        this.setUser(newUser);
    };
    /**
     * Logs out the current user.
     */
    /**
     * Logs out the current user.
     * @return {?}
     */
    AuthService.prototype.logout = /**
     * Logs out the current user.
     * @return {?}
     */
    function () {
        this.setUser(null);
    };
    AuthService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AuthService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ AuthService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthService_Factory() { return new AuthService(i0.ɵɵinject(i1.HttpClient)); }, token: AuthService, providedIn: "root" });
    return AuthService;
}());
export { AuthService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.user;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.user$;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.loggedIn$;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9hdXRoL2F1dGguc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxhQUFhLEVBQWMsTUFBTSxNQUFNLENBQUM7QUFDakQsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxXQUFXLENBQUM7Ozs7SUFJekMsVUFBVSxHQUFHLFFBQVEsR0FBRyxPQUFPOzs7O0FBS3JDO0lBUUU7O09BRUc7SUFDSCxxQkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQU41QixVQUFLLEdBQUcsSUFBSSxhQUFhLENBQVcsQ0FBQyxDQUFDLENBQUM7UUFDdkMsY0FBUyxHQUFHLElBQUksYUFBYSxDQUFVLENBQUMsQ0FBQyxDQUFDO0lBS1YsQ0FBQztJQUV6Qzs7O09BR0c7Ozs7OztJQUNHLDJCQUFLOzs7OztJQUFYOzs7Ozs7O3dCQUdFLElBQUk7NEJBQ0YsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO3lCQUM1RDt3QkFBQyxPQUFPLEdBQUcsRUFBRTs0QkFDWixPQUFPLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7NEJBQ3ZELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7eUJBQ3BCOzZCQUdHLFdBQVcsRUFBWCx3QkFBVzs7Ozt3QkFFUSxxQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBVyxRQUFRLEdBQUcsVUFBVSxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUE7O3dCQUE3RSxVQUFVLEdBQUcsU0FBZ0U7d0JBQ25GLElBQUksVUFBVSxDQUFDLEtBQUssRUFBRTs0QkFDcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQzt5QkFDMUI7NkJBQU0sSUFBSSxXQUFXLENBQUMsWUFBWSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUU7NEJBQzFELElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7eUJBQzNCOzZCQUFNOzRCQUNMLGlCQUFpQjs0QkFDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7eUJBQzlCOzs7O3dCQUVELGVBQWU7d0JBQ2YsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBRyxDQUFDLENBQUM7Ozs7d0JBR3hCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7Ozs7OztLQUV0QjtJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCw2QkFBTzs7Ozs7O0lBQVAsVUFBUSxJQUFlO1FBQ3JCLElBQUksSUFBSSxFQUFFO1lBQ1IsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzNCO2FBQU07WUFDTCxZQUFZLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzVCO0lBQ0gsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILDZCQUFPOzs7O0lBQVA7UUFDRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDbkIsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILDhCQUFROzs7O0lBQVI7UUFDRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILDhCQUFROzs7O0lBQVI7UUFDRSw0REFBNEQ7UUFDNUQsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUN4QjthQUFNO1lBQ0wsMkNBQTJDO1lBQzNDLElBQUk7O29CQUNJLFdBQVcsR0FBYSxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzFFLE9BQU8sV0FBVyxDQUFDLEtBQUssQ0FBQzthQUMxQjtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNaLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQztnQkFDdkQsT0FBTyxJQUFJLENBQUM7YUFDYjtTQUNGO0lBQ0gsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILGlDQUFXOzs7O0lBQVg7UUFDRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQWFEOzs7O09BSUc7Ozs7Ozs7SUFDRyx1Q0FBaUI7Ozs7OztJQUF2QixVQUF3QixRQUFnQixFQUFFLElBQW9COzs7Ozs7NkJBQ3hELENBQUEsUUFBUSxLQUFLLE9BQU8sQ0FBQSxFQUFwQix3QkFBb0I7d0JBQ0gscUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQVcsUUFBUSxHQUFHLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBQXhGLFVBQVUsR0FBRyxTQUEyRTt3QkFDOUYsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQzt3QkFDekIsc0JBQU8sSUFBSSxDQUFDLElBQUksRUFBQzs7d0JBRWpCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLFFBQVEsR0FBRyxTQUFTLEdBQUcsUUFBUSxDQUFDOzs7Ozs7S0FFMUQ7SUFjRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDRyx3Q0FBa0I7Ozs7Ozs7SUFBeEIsVUFBeUIsUUFBZ0IsRUFBRSxJQUFxQixFQUFFLFdBQW9COzs7Ozs7NkJBQ2hGLENBQUEsUUFBUSxLQUFLLE9BQU8sQ0FBQSxFQUFwQix3QkFBb0I7d0JBQ2hCLEtBQUssR0FBRyxRQUFRLEdBQUcsV0FBVyxJQUFJLGNBQWM7d0JBQ25DLHFCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFXLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBQXBFLFVBQVUsR0FBRyxTQUF1RDt3QkFDMUUsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFOzRCQUNoQixzQkFBTyxVQUFVLEVBQUM7eUJBQ25COzZCQUFNOzRCQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7NEJBQ3pCLHNCQUFPLElBQUksQ0FBQyxJQUFJLEVBQUM7eUJBQ2xCOzs7d0JBRUQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsUUFBUSxHQUFHLFFBQVEsR0FBRyxRQUFRLENBQUM7Ozs7OztLQUV6RDtJQUVEOztPQUVHOzs7Ozs7SUFDSCxnQ0FBVTs7Ozs7SUFBVixVQUFXLE9BQWlCO1FBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDeEIsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILDRCQUFNOzs7O0lBQU47UUFDRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3JCLENBQUM7O2dCQWxMRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQWJRLFVBQVU7OztzQkFEbkI7Q0ErTEMsQUFuTEQsSUFtTEM7U0FoTFksV0FBVzs7Ozs7O0lBQ3RCLDJCQUF1Qjs7Ozs7SUFDdkIsNEJBQStDOzs7OztJQUMvQyxnQ0FBa0Q7Ozs7O0lBS3RDLDJCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgUmVwbGF5U3ViamVjdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB3ZWJBbGlhcywgYXBpUm91dGUgfSBmcm9tICcuLi9jb25maWcnO1xyXG5pbXBvcnQgeyBLZXBzVXNlciB9IGZyb20gJy4uL2ludGVyZmFjZSc7XHJcbmltcG9ydCB7IEF1dGhMb2dpbkRhdGEsIEF1dGhTaWdudXBEYXRhIH0gZnJvbSAnLi9hdXRoLWludGVyZmFjZXMnO1xyXG5cclxuY29uc3Qgc3RvcmFnZUtleSA9IHdlYkFsaWFzICsgJy11c2VyJztcclxuXHJcbi8qKlxyXG4gKiBBIHNlcnZpY2UgdGhhdCBoYW5kbGVzIGF1dGhlbnRpY2F0aW9uIHdpdGggYSBLZXBzIHNlcnZlci5cclxuICovXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEF1dGhTZXJ2aWNlIHtcclxuICBwcml2YXRlIHVzZXI6IEtlcHNVc2VyO1xyXG4gIHByaXZhdGUgdXNlciQgPSBuZXcgUmVwbGF5U3ViamVjdDxLZXBzVXNlcj4oMSk7XHJcbiAgcHJpdmF0ZSBsb2dnZWRJbiQgPSBuZXcgUmVwbGF5U3ViamVjdDxib29sZWFuPigxKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7IH1cclxuXHJcbiAgLyoqXHJcbiAgICogU3RhcnRzIHRoZSBhdXRoIHNlcnZpY2UuXHJcbiAgICogU2hvdWxkIGJlIGNhbGxlZCBvbmx5IHdoZW4gdGhlIGFwcCBpcyBib290c3RyYXBwaW5nLlxyXG4gICAqL1xyXG4gIGFzeW5jIHN0YXJ0KCk6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgLy8gVHJ5IGdldHRpbmcgdXNlciBmcm9tIHN0b3JhZ2UuXHJcbiAgICBsZXQgc3RvcmFnZVVzZXI6IEtlcHNVc2VyO1xyXG4gICAgdHJ5IHtcclxuICAgICAgc3RvcmFnZVVzZXIgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHN0b3JhZ2VLZXkpKTtcclxuICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICBjb25zb2xlLmVycm9yKCdFcnJvciBnZXR0aW5nIHVzZXIgZnJvbSBsb2NhbCBzdG9yYWdlJyk7XHJcbiAgICAgIHRoaXMuc2V0VXNlcihudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBJZiB1c2VyIGV4aXN0LCB0aGVuIGdldCB1c2VyIGRhdGEgZnJvbSBzZXJ2ZXIuXHJcbiAgICBpZiAoc3RvcmFnZVVzZXIpIHtcclxuICAgICAgdHJ5IHtcclxuICAgICAgICBjb25zdCBzZXJ2ZXJVc2VyID0gYXdhaXQgdGhpcy5odHRwLmdldDxLZXBzVXNlcj4oYXBpUm91dGUgKyAndXNlcnMvbWUnKS50b1Byb21pc2UoKTtcclxuICAgICAgICBpZiAoc2VydmVyVXNlci50b2tlbikge1xyXG4gICAgICAgICAgdGhpcy5zZXRVc2VyKHNlcnZlclVzZXIpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc3RvcmFnZVVzZXIudG9rZW5FeHBpcmVzID4gbmV3IERhdGUoKS5nZXRUaW1lKCkpIHtcclxuICAgICAgICAgIHRoaXMuc2V0VXNlcihzdG9yYWdlVXNlcik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIC8vIFRva2VuIGV4cGlyZWQuXHJcbiAgICAgICAgICB0aGlzLnVzZXIkLmVycm9yKHNlcnZlclVzZXIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgLy8gVG9rZW4gZXJyb3IuXHJcbiAgICAgICAgdGhpcy51c2VyJC5lcnJvcihlcnIpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNldFVzZXIobnVsbCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTYXZlcy9yZW1vdmVzIHVzZXIgYW5kIGVtaXRzIHRoZSB1c2VyIGFuZCBsb2dnZWQgaW4gc3RhdHVzIHRvIG9ic2VydmFibGVzLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqIEBwYXJhbSB1c2VyIEEgdXNlciBvciBudWxsXHJcbiAgICovXHJcbiAgc2V0VXNlcih1c2VyPzogS2Vwc1VzZXIpIHtcclxuICAgIGlmICh1c2VyKSB7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKHN0b3JhZ2VLZXksIEpTT04uc3RyaW5naWZ5KHVzZXIpKTtcclxuICAgICAgdGhpcy51c2VyID0gdXNlcjtcclxuICAgICAgdGhpcy51c2VyJC5uZXh0KHVzZXIpO1xyXG4gICAgICB0aGlzLmxvZ2dlZEluJC5uZXh0KHRydWUpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oc3RvcmFnZUtleSk7XHJcbiAgICAgIHRoaXMudXNlciA9IG51bGw7XHJcbiAgICAgIHRoaXMudXNlciQubmV4dChudWxsKTtcclxuICAgICAgdGhpcy5sb2dnZWRJbiQubmV4dChmYWxzZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIHRoZSBjdXJyZW50IHVzZXIuXHJcbiAgICovXHJcbiAgZ2V0VXNlcigpOiBLZXBzVXNlciB7XHJcbiAgICByZXR1cm4gdGhpcy51c2VyO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUmV0dXJucyBhbiBvYnNlcnZhYmxlIHRoYXQgd2lsbCBlbWl0IHRoZSBjdXJyZW50IHVzZXIuXHJcbiAgICovXHJcbiAgZ2V0VXNlciQoKTogT2JzZXJ2YWJsZTxLZXBzVXNlcj4ge1xyXG4gICAgcmV0dXJuIHRoaXMudXNlciQuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIHRva2VuIGZyb20gdGhlIHVzZXIuXHJcbiAgICovXHJcbiAgZ2V0VG9rZW4oKTogc3RyaW5nIHwgbnVsbCB7XHJcbiAgICAvLyBJZiB1c2VyIGlzIHN0b3JlZCBpbiBhdXRoIHNlcnZpY2UsIHVzZSB0aGF0IHVzZXIncyB0b2tlbi5cclxuICAgIGlmICh0aGlzLnVzZXIpIHtcclxuICAgICAgcmV0dXJuIHRoaXMudXNlci50b2tlbjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIE90aGVyd2lzZSwgdHJ5IGdldHRpbmcgZnJvbSB0aGUgc3RvcmFnZS5cclxuICAgICAgdHJ5IHtcclxuICAgICAgICBjb25zdCBzdG9yYWdlVXNlcjogS2Vwc1VzZXIgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHN0b3JhZ2VLZXkpKTtcclxuICAgICAgICByZXR1cm4gc3RvcmFnZVVzZXIudG9rZW47XHJcbiAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIGdldHRpbmcgdXNlciBmcm9tIGxvY2FsIHN0b3JhZ2UnKTtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUmV0dXJucyBhbiBvYnNlcnZhYmxlIHRoYXQgZW1pdHMgbG9nZ2VkIGluIHN0YXR1cy5cclxuICAgKi9cclxuICBpc0xvZ2dlZEluJCgpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgIHJldHVybiB0aGlzLmxvZ2dlZEluJC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIExvZ3MgaW4gdXNpbmcgbG9jYWwgcHJvdmlkZXIsIHNldHMgY3VycmVudCB1c2VyLCBhbmQgcmV0dXJucyB0aGUgdXNlciBvYmplY3QuXHJcbiAgICogQHBhcmFtIHByb3ZpZGVyIExvY2FsIHByb3ZpZGVyLlxyXG4gICAqIEBwYXJhbSBkYXRhIEFuIG9iamVjdCB3aXRoIHVzZXJuYW1lIGFuZCBwYXNzd29yZCBmaWVsZHMuXHJcbiAgICovXHJcbiAgYXN5bmMgbG9naW5XaXRoUHJvdmlkZXIocHJvdmlkZXI6ICdsb2NhbCcsIGRhdGE6IEF1dGhMb2dpbkRhdGEpOiBQcm9taXNlPEtlcHNVc2VyPjtcclxuICAvKipcclxuICAgKiBMb2dzIGluIHVzaW5nIHRoZSBwYXNzZWQgaW4gcHJvdmlkZXIgbmFtZS5cclxuICAgKiBAcGFyYW0gcHJvdmlkZXIgUHJvdmlkZXIgdG8gdXNlIHRvIGxvZ2luLlxyXG4gICAqL1xyXG4gIGFzeW5jIGxvZ2luV2l0aFByb3ZpZGVyKHByb3ZpZGVyOiBzdHJpbmcpOiBQcm9taXNlPHZvaWQ+O1xyXG4gIC8qKlxyXG4gICAqIExvZ3MgaW4gdXNpbmcgdGhlIHBhc3NlZCBpbiBwcm92aWRlciBuYW1lLlxyXG4gICAqIEBwYXJhbSBwcm92aWRlciBQcm92aWRlciB0byB1c2UgdG8gbG9naW4uXHJcbiAgICogQHBhcmFtIGRhdGEgRGF0YSB0byBiZSBwYXNzZWQgb24gdG8gdGhlIHByb3ZpZGVyLlxyXG4gICAqL1xyXG4gIGFzeW5jIGxvZ2luV2l0aFByb3ZpZGVyKHByb3ZpZGVyOiBzdHJpbmcsIGRhdGE/OiBBdXRoTG9naW5EYXRhKTogUHJvbWlzZTxLZXBzVXNlciB8IHZvaWQ+IHtcclxuICAgIGlmIChwcm92aWRlciA9PT0gJ2xvY2FsJykge1xyXG4gICAgICBjb25zdCBzZXJ2ZXJVc2VyID0gYXdhaXQgdGhpcy5odHRwLnBvc3Q8S2Vwc1VzZXI+KGFwaVJvdXRlICsgJ3VzZXJzL3NpZ25pbicsIGRhdGEpLnRvUHJvbWlzZSgpO1xyXG4gICAgICB0aGlzLnNldFVzZXIoc2VydmVyVXNlcik7XHJcbiAgICAgIHJldHVybiB0aGlzLnVzZXI7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGFwaVJvdXRlICsgJ29hdXRocy8nICsgcHJvdmlkZXI7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTaWducyB1cCB1c2luZyBsb2NhbCBwcm92aWRlciwgc2V0cyBjdXJyZW50IHVzZXIsIGFuZCByZXR1cm5zIHRoZSB1c2VyIG9iamVjdC5cclxuICAgKiBAcGFyYW0gcHJvdmlkZXIgTG9jYWwgcHJvdmlkZXIuXHJcbiAgICogQHBhcmFtIGRhdGEgQW4gb2JqZWN0IHdpdGggdXNlIGRhdGEuXHJcbiAgICogQHBhcmFtIGN1c3RvbVJvdXRlIEN1c3RvbSByb3V0ZSB0byB1c2UgaW5zdGVhZCBvZiB0aGUgZGVmYXVsdCAndXNlcnMvc2lnbnVwJy5cclxuICAgKi9cclxuICBhc3luYyBzaWdudXBXaXRoUHJvdmlkZXIocHJvdmlkZXI6ICdsb2NhbCcsIGRhdGE6IEF1dGhTaWdudXBEYXRhLCBjdXN0b21Sb3V0ZT86IHN0cmluZyk6IFByb21pc2U8S2Vwc1VzZXI+O1xyXG4gIC8qKlxyXG4gICAqIFNpZ25zIHVwIHVzZXIgdXNpbmcgdGhlIHBhc3NlZCBpbiBwcm92aWRlciBuYW1lLlxyXG4gICAqIEBwYXJhbSBwcm92aWRlciBQcm92aWRlciB0byB1c2UgdG8gc2lnbiB1cC5cclxuICAgKi9cclxuICBhc3luYyBzaWdudXBXaXRoUHJvdmlkZXIocHJvdmlkZXI6IHN0cmluZyk6IFByb21pc2U8dm9pZD47XHJcbiAgLyoqXHJcbiAgICogU2lnbnMgdXAgdXNlciB1c2luZyB0aGUgcGFzc2VkIGluIHByb3ZpZGVyIG5hbWUuXHJcbiAgICogQHBhcmFtIHByb3ZpZGVyIFByb3ZpZGVyIHRvIHVzZSB0byBzaWduIHVwLlxyXG4gICAqIEBwYXJhbSBkYXRhIERhdGEgdG8gYmUgcGFzc2VkIG9uIHRvIHRoZSBwcm92aWRlci5cclxuICAgKiBAcGFyYW0gY3VzdG9tUm91dGUgQ3VzdG9tIHJvdXRlIHRvIHVzZSBpbnN0ZWFkIG9mIHRoZSBkZWZhdWx0ICd1c2Vycy9zaWdudXAnLlxyXG4gICAqL1xyXG4gIGFzeW5jIHNpZ251cFdpdGhQcm92aWRlcihwcm92aWRlcjogc3RyaW5nLCBkYXRhPzogQXV0aFNpZ251cERhdGEsIGN1c3RvbVJvdXRlPzogc3RyaW5nKTogUHJvbWlzZTxLZXBzVXNlciB8IHZvaWQ+IHtcclxuICAgIGlmIChwcm92aWRlciA9PT0gJ2xvY2FsJykge1xyXG4gICAgICBjb25zdCByb3V0ZSA9IGFwaVJvdXRlICsgY3VzdG9tUm91dGUgfHwgJ3VzZXJzL3NpZ251cCc7XHJcbiAgICAgIGNvbnN0IHNlcnZlclVzZXIgPSBhd2FpdCB0aGlzLmh0dHAucG9zdDxLZXBzVXNlcj4ocm91dGUsIGRhdGEpLnRvUHJvbWlzZSgpO1xyXG4gICAgICBpZiAoZGF0YS5ub0xvZ2luKSB7XHJcbiAgICAgICAgcmV0dXJuIHNlcnZlclVzZXI7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zZXRVc2VyKHNlcnZlclVzZXIpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLnVzZXI7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gd2ViQWxpYXMgKyAnL2F1dGgvJyArIHByb3ZpZGVyO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU3dpdGNoIGN1cnJlbnQgdXNlci5cclxuICAgKi9cclxuICBzd2l0Y2hVc2VyKG5ld1VzZXI6IEtlcHNVc2VyKSB7XHJcbiAgICB0aGlzLnNldFVzZXIobmV3VXNlcik7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBMb2dzIG91dCB0aGUgY3VycmVudCB1c2VyLlxyXG4gICAqL1xyXG4gIGxvZ291dCgpIHtcclxuICAgIHRoaXMuc2V0VXNlcihudWxsKTtcclxuICB9XHJcbn1cclxuIl19