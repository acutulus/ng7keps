/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Represents data to be send to the local login provider.
 * @record
 */
export function AuthLoginData() { }
if (false) {
    /** @type {?} */
    AuthLoginData.prototype.username;
    /** @type {?} */
    AuthLoginData.prototype.password;
    /**
     * How long in miliseconds should the user token be active.
     * @type {?|undefined}
     */
    AuthLoginData.prototype.expires;
}
/**
 * Represents data to be send to the local signup provider.
 * @record
 */
export function AuthSignupData() { }
if (false) {
    /** @type {?|undefined} */
    AuthSignupData.prototype.username;
    /** @type {?|undefined} */
    AuthSignupData.prototype.password;
    /** @type {?|undefined} */
    AuthSignupData.prototype.email;
    /** @type {?|undefined} */
    AuthSignupData.prototype.firstName;
    /** @type {?|undefined} */
    AuthSignupData.prototype.lastName;
    /** @type {?|undefined} */
    AuthSignupData.prototype.pending;
    /**
     * If true, the user will not be automatically logged in after signing up.
     * @type {?|undefined}
     */
    AuthSignupData.prototype.noLogin;
}
/**
 * Represents options for the Auth Guard.
 * @record
 */
export function AuthGuardOptions() { }
if (false) {
    /**
     * Allow only user with this role to the route.
     * @type {?|undefined}
     */
    AuthGuardOptions.prototype.role;
    /**
     * If true, the auth service will logs out the user trying to access the route.
     * @type {?|undefined}
     */
    AuthGuardOptions.prototype.logout;
    /**
     * Url to take user to if they sign out; If not supplied, default url is 'window.location.origin'
     * @type {?|undefined}
     */
    AuthGuardOptions.prototype.logoutUrl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1pbnRlcmZhY2VzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9hdXRoL2F1dGgtaW50ZXJmYWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBLG1DQVFDOzs7SUFQQyxpQ0FBNkI7O0lBQzdCLGlDQUE2Qjs7Ozs7SUFLN0IsZ0NBQTZCOzs7Ozs7QUFNL0Isb0NBWUM7OztJQVhDLGtDQUE2Qjs7SUFDN0Isa0NBQTZCOztJQUM3QiwrQkFBNkI7O0lBQzdCLG1DQUE2Qjs7SUFDN0Isa0NBQTZCOztJQUM3QixpQ0FBOEI7Ozs7O0lBSzlCLGlDQUE4Qjs7Ozs7O0FBTWhDLHNDQWVDOzs7Ozs7SUFYQyxnQ0FBYzs7Ozs7SUFLZCxrQ0FBaUI7Ozs7O0lBS2pCLHFDQUFtQiIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBSZXByZXNlbnRzIGRhdGEgdG8gYmUgc2VuZCB0byB0aGUgbG9jYWwgbG9naW4gcHJvdmlkZXIuXHJcbiAqL1xyXG5leHBvcnQgaW50ZXJmYWNlIEF1dGhMb2dpbkRhdGEge1xyXG4gIHVzZXJuYW1lOiAgICAgICAgICAgICBzdHJpbmc7XHJcbiAgcGFzc3dvcmQ6ICAgICAgICAgICAgIHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogSG93IGxvbmcgaW4gbWlsaXNlY29uZHMgc2hvdWxkIHRoZSB1c2VyIHRva2VuIGJlIGFjdGl2ZS5cclxuICAgKi9cclxuICBleHBpcmVzPzogICAgICAgICAgICAgbnVtYmVyO1xyXG59XHJcblxyXG4vKipcclxuICogUmVwcmVzZW50cyBkYXRhIHRvIGJlIHNlbmQgdG8gdGhlIGxvY2FsIHNpZ251cCBwcm92aWRlci5cclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgQXV0aFNpZ251cERhdGEge1xyXG4gIHVzZXJuYW1lPzogICAgICAgICAgICBzdHJpbmc7XHJcbiAgcGFzc3dvcmQ/OiAgICAgICAgICAgIHN0cmluZztcclxuICBlbWFpbD86ICAgICAgICAgICAgICAgc3RyaW5nO1xyXG4gIGZpcnN0TmFtZT86ICAgICAgICAgICBzdHJpbmc7XHJcbiAgbGFzdE5hbWU/OiAgICAgICAgICAgIHN0cmluZztcclxuICBwZW5kaW5nPzogICAgICAgICAgICAgYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogSWYgdHJ1ZSwgdGhlIHVzZXIgd2lsbCBub3QgYmUgYXV0b21hdGljYWxseSBsb2dnZWQgaW4gYWZ0ZXIgc2lnbmluZyB1cC5cclxuICAgKi9cclxuICBub0xvZ2luPzogICAgICAgICAgICAgYm9vbGVhbjtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJlcHJlc2VudHMgb3B0aW9ucyBmb3IgdGhlIEF1dGggR3VhcmQuXHJcbiAqL1xyXG5leHBvcnQgaW50ZXJmYWNlIEF1dGhHdWFyZE9wdGlvbnMge1xyXG4gIC8qKlxyXG4gICAqIEFsbG93IG9ubHkgdXNlciB3aXRoIHRoaXMgcm9sZSB0byB0aGUgcm91dGUuXHJcbiAgICovXHJcbiAgcm9sZT86IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogSWYgdHJ1ZSwgdGhlIGF1dGggc2VydmljZSB3aWxsIGxvZ3Mgb3V0IHRoZSB1c2VyIHRyeWluZyB0byBhY2Nlc3MgdGhlIHJvdXRlLlxyXG4gICAqL1xyXG4gIGxvZ291dD86IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIFVybCB0byB0YWtlIHVzZXIgdG8gaWYgdGhleSBzaWduIG91dDsgSWYgbm90IHN1cHBsaWVkLCBkZWZhdWx0IHVybCBpcyAnd2luZG93LmxvY2F0aW9uLm9yaWdpbidcclxuICAgKi9cclxuICBsb2dvdXRVcmw/OiBzdHJpbmc7XHJcbn1cclxuIl19