/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { take, map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "./auth.service";
/**
 * A route guard that rejects user from accessing the route if they're not logged in.
 *
 * Can also allows only specific user role from accessing the route based on the route
 * data's `guardOpt` field. See {\@link AuthGuardOptions}.
 */
var AuthGuard = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function AuthGuard(router, auth) {
        this.router = router;
        this.auth = auth;
    }
    /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    AuthGuard.prototype.canActivate = /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    function (next, state) {
        var _this = this;
        return this.auth.isLoggedIn$()
            .pipe(take(1), map((/**
         * @param {?} isLoggedIn
         * @return {?}
         */
        function (isLoggedIn) {
            if (next.data && next.data.guardOpt) {
                /** @type {?} */
                var opts = (/** @type {?} */ (next.data.guardOpt));
                if (isLoggedIn) {
                    /** @type {?} */
                    var user = _this.auth.getUser();
                    if (opts.role) {
                        if (user.roles.includes(opts.role)) {
                            return true;
                        }
                        else {
                            alert('Access Denied');
                            if (opts.logout) {
                                _this.auth.logout();
                                if (opts.logoutUrl) {
                                    window.location.href = opts.logoutUrl;
                                }
                                else {
                                    window.location.href = window.location.origin;
                                }
                            }
                            else {
                                _this.router.navigate([_this.router.url]);
                            }
                            return false;
                        }
                    }
                    return true;
                }
                else {
                    if (opts.logoutUrl) {
                        window.location.href = opts.logoutUrl;
                    }
                    else {
                        window.location.href = window.location.origin;
                    }
                    return false;
                }
            }
        })));
    };
    AuthGuard.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AuthGuard.ctorParameters = function () { return [
        { type: Router },
        { type: AuthService }
    ]; };
    /** @nocollapse */ AuthGuard.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthGuard_Factory() { return new AuthGuard(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.AuthService)); }, token: AuthGuard, providedIn: "root" });
    return AuthGuard;
}());
export { AuthGuard };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthGuard.prototype.router;
    /**
     * @type {?}
     * @private
     */
    AuthGuard.prototype.auth;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5ndWFyZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvYXV0aC9hdXRoLmd1YXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFDTCxNQUFNLEVBSVAsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHN0MsT0FBTyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7Ozs7OztBQVEzQztJQUtFOztPQUVHO0lBQ0gsbUJBQ1UsTUFBYyxFQUNkLElBQWlCO1FBRGpCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxTQUFJLEdBQUosSUFBSSxDQUFhO0lBQ3hCLENBQUM7Ozs7OztJQUVKLCtCQUFXOzs7OztJQUFYLFVBQ0UsSUFBNEIsRUFDNUIsS0FBMEI7UUFGNUIsaUJBMkNDO1FBdkNDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUU7YUFDN0IsSUFBSSxDQUNILElBQUksQ0FBQyxDQUFDLENBQUMsRUFDUCxHQUFHOzs7O1FBQUMsVUFBQyxVQUFtQjtZQUN0QixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7O29CQUM3QixJQUFJLEdBQUcsbUJBQWtCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFBO2dCQUNqRCxJQUFJLFVBQVUsRUFBRTs7d0JBQ04sSUFBSSxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNoQyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7d0JBQ2IsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7NEJBQ2xDLE9BQU8sSUFBSSxDQUFDO3lCQUNiOzZCQUFNOzRCQUNMLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQzs0QkFDdkIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO2dDQUNmLEtBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0NBQ25CLElBQUcsSUFBSSxDQUFDLFNBQVMsRUFBQztvQ0FDaEIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztpQ0FDdkM7cUNBQUs7b0NBQ0osTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7aUNBQy9DOzZCQUNGO2lDQUFNO2dDQUNMLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDOzZCQUN6Qzs0QkFDRCxPQUFPLEtBQUssQ0FBQzt5QkFDZDtxQkFFSjtvQkFDRCxPQUFPLElBQUksQ0FBQztpQkFDYjtxQkFBTTtvQkFDTCxJQUFHLElBQUksQ0FBQyxTQUFTLEVBQUM7d0JBQ2hCLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7cUJBQ3ZDO3lCQUFLO3dCQUNKLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO3FCQUMvQztvQkFDRCxPQUFPLEtBQUssQ0FBQztpQkFDZDthQUNKO1FBQ0QsQ0FBQyxFQUFDLENBQ0gsQ0FBQztJQUNKLENBQUM7O2dCQXhERixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQWxCQyxNQUFNO2dCQUtDLFdBQVc7OztvQkFQcEI7Q0E0RUMsQUExREQsSUEwREM7U0F2RFksU0FBUzs7Ozs7O0lBTWxCLDJCQUFzQjs7Ozs7SUFDdEIseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gIFJvdXRlcixcclxuICBDYW5BY3RpdmF0ZSxcclxuICBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gIFJvdXRlclN0YXRlU25hcHNob3RcclxufSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4vYXV0aC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXV0aEd1YXJkT3B0aW9ucyB9IGZyb20gJy4vYXV0aC1pbnRlcmZhY2VzJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG4vKipcclxuICogQSByb3V0ZSBndWFyZCB0aGF0IHJlamVjdHMgdXNlciBmcm9tIGFjY2Vzc2luZyB0aGUgcm91dGUgaWYgdGhleSdyZSBub3QgbG9nZ2VkIGluLlxyXG4gKlxyXG4gKiBDYW4gYWxzbyBhbGxvd3Mgb25seSBzcGVjaWZpYyB1c2VyIHJvbGUgZnJvbSBhY2Nlc3NpbmcgdGhlIHJvdXRlIGJhc2VkIG9uIHRoZSByb3V0ZVxyXG4gKiBkYXRhJ3MgYGd1YXJkT3B0YCBmaWVsZC4gU2VlIHtAbGluayBBdXRoR3VhcmRPcHRpb25zfS5cclxuICovXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEF1dGhHdWFyZCBpbXBsZW1lbnRzIENhbkFjdGl2YXRlIHtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgcHJpdmF0ZSBhdXRoOiBBdXRoU2VydmljZVxyXG4gICkge31cclxuXHJcbiAgY2FuQWN0aXZhdGUoXHJcbiAgICBuZXh0OiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gICAgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3RcclxuICApOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgIHJldHVybiB0aGlzLmF1dGguaXNMb2dnZWRJbiQoKVxyXG4gICAgLnBpcGUoXHJcbiAgICAgIHRha2UoMSksXHJcbiAgICAgIG1hcCgoaXNMb2dnZWRJbjogYm9vbGVhbikgPT4ge1xyXG4gICAgICAgIGlmIChuZXh0LmRhdGEgJiYgbmV4dC5kYXRhLmd1YXJkT3B0KSB7XHJcbiAgICAgICAgICBjb25zdCBvcHRzID0gPEF1dGhHdWFyZE9wdGlvbnM+bmV4dC5kYXRhLmd1YXJkT3B0O1xyXG4gICAgICAgICAgaWYgKGlzTG9nZ2VkSW4pIHtcclxuICAgICAgICAgICAgICBjb25zdCB1c2VyID0gdGhpcy5hdXRoLmdldFVzZXIoKTtcclxuICAgICAgICAgICAgICBpZiAob3B0cy5yb2xlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodXNlci5yb2xlcy5pbmNsdWRlcyhvcHRzLnJvbGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgYWxlcnQoJ0FjY2VzcyBEZW5pZWQnKTtcclxuICAgICAgICAgICAgICAgICAgaWYgKG9wdHMubG9nb3V0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hdXRoLmxvZ291dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKG9wdHMubG9nb3V0VXJsKXtcclxuICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gb3B0cy5sb2dvdXRVcmw7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNle1xyXG4gICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdGhpcy5yb3V0ZXIudXJsXSk7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYob3B0cy5sb2dvdXRVcmwpe1xyXG4gICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gb3B0cy5sb2dvdXRVcmw7XHJcbiAgICAgICAgICAgIH0gZWxzZXtcclxuICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IHdpbmRvdy5sb2NhdGlvbi5vcmlnaW47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbn1cclxuIl19