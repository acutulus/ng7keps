/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
/**
 * A component that wraps around the default HTML file select button.
 */
var FileSelectComponent = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function FileSelectComponent() {
        /**
         * File to be selected.
         */
        this.selectedFile = null;
        /**
         * An event emitter that emits the selected file.
         */
        this.selectedFileChange = new EventEmitter();
        /**
         * Label for the file select button.
         */
        this.label = 'Choose File';
        /**
         * Whether the component should display the file name.
         */
        this.showFilename = true;
        /**
         * The type(s) of file that the file select dialog can accept.
         *
         * See: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#Unique_file_type_specifiers
         *
         * If the value is "image", "video", or "audio", it will accept every possible file types
         * of that media type.
         */
        this.type = '';
        /**
         * Angular Material color for the file select button.
         */
        this.color = 'primary';
    }
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    FileSelectComponent.prototype.ngOnInit = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        if (this.type === 'image' ||
            this.type === 'video' ||
            this.type === 'audio') {
            this.type += '/*';
        }
    };
    /**
     * Open the file select dialog manually.
     */
    /**
     * Open the file select dialog manually.
     * @return {?}
     */
    FileSelectComponent.prototype.openFileSelect = /**
     * Open the file select dialog manually.
     * @return {?}
     */
    function () {
        this.fileSelector.nativeElement.click();
    };
    /**
     * Select a file.
     * @param files Files to be passed. Only the first one will be selected.
     */
    /**
     * Select a file.
     * @param {?} files Files to be passed. Only the first one will be selected.
     * @return {?}
     */
    FileSelectComponent.prototype.selectFile = /**
     * Select a file.
     * @param {?} files Files to be passed. Only the first one will be selected.
     * @return {?}
     */
    function (files) {
        if (files && files.length > 0) {
            this.selectedFile = files[0];
            this.selectedFileChange.emit(this.selectedFile);
        }
    };
    FileSelectComponent.decorators = [
        { type: Component, args: [{
                    selector: 'keps-file-select',
                    template: "<button\r\n  (click)=\"openFileSelect()\"\r\n  mat-raised-button [color]=\"color\"\r\n>\r\n  {{ label }}\r\n</button>\r\n<span *ngIf=\"showFilename\" class=\"file-name\">\r\n  {{ selectedFile ? selectedFile.name : 'No file chosen.' }}\r\n</span>\r\n<input\r\n  #fileSelector\r\n  (change)=\"selectFile($event.target.files)\"\r\n  type=\"file\"\r\n  [attr.accept]=\"type\"\r\n  hidden\r\n/>\r\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    FileSelectComponent.ctorParameters = function () { return []; };
    FileSelectComponent.propDecorators = {
        selectedFile: [{ type: Input }],
        selectedFileChange: [{ type: Output }],
        label: [{ type: Input }],
        showFilename: [{ type: Input }],
        type: [{ type: Input }],
        color: [{ type: Input }],
        fileSelector: [{ type: ViewChild, args: ['fileSelector', { static: true },] }]
    };
    return FileSelectComponent;
}());
export { FileSelectComponent };
if (false) {
    /**
     * File to be selected.
     * @type {?}
     */
    FileSelectComponent.prototype.selectedFile;
    /**
     * An event emitter that emits the selected file.
     * @type {?}
     */
    FileSelectComponent.prototype.selectedFileChange;
    /**
     * Label for the file select button.
     * @type {?}
     */
    FileSelectComponent.prototype.label;
    /**
     * Whether the component should display the file name.
     * @type {?}
     */
    FileSelectComponent.prototype.showFilename;
    /**
     * The type(s) of file that the file select dialog can accept.
     *
     * See: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#Unique_file_type_specifiers
     *
     * If the value is "image", "video", or "audio", it will accept every possible file types
     * of that media type.
     * @type {?}
     */
    FileSelectComponent.prototype.type;
    /**
     * Angular Material color for the file select button.
     * @type {?}
     */
    FileSelectComponent.prototype.color;
    /**
     * The `input` element itself.
     * @type {?}
     * @private
     */
    FileSelectComponent.prototype.fileSelector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS1zZWxlY3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvZmlsZS1zZWxlY3QvZmlsZS1zZWxlY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULEtBQUssRUFDTCxNQUFNLEVBQ04sWUFBWSxFQUNaLFNBQVMsRUFDVCxVQUFVLEVBQ1gsTUFBTSxlQUFlLENBQUM7Ozs7QUFNdkI7SUE4Q0U7O09BRUc7SUFDSDs7OztRQXhDUyxpQkFBWSxHQUFTLElBQUksQ0FBQzs7OztRQUt6Qix1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBUSxDQUFDOzs7O1FBSy9DLFVBQUssR0FBRyxhQUFhLENBQUM7Ozs7UUFLdEIsaUJBQVksR0FBRyxJQUFJLENBQUM7Ozs7Ozs7OztRQVVwQixTQUFJLEdBQUcsRUFBRSxDQUFDOzs7O1FBS1YsVUFBSyxHQUFpQixTQUFTLENBQUM7SUFVekIsQ0FBQztJQUVqQjs7T0FFRzs7Ozs7SUFDSCxzQ0FBUTs7OztJQUFSO1FBQ0UsSUFDRSxJQUFJLENBQUMsSUFBSSxLQUFLLE9BQU87WUFDckIsSUFBSSxDQUFDLElBQUksS0FBSyxPQUFPO1lBQ3JCLElBQUksQ0FBQyxJQUFJLEtBQUssT0FBTyxFQUNyQjtZQUNBLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDO1NBQ25CO0lBQ0gsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILDRDQUFjOzs7O0lBQWQ7UUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMxQyxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSCx3Q0FBVTs7Ozs7SUFBVixVQUFXLEtBQWE7UUFDdEIsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDakQ7SUFDSCxDQUFDOztnQkFoRkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLHFaQUEyQzs7aUJBRTVDOzs7OzsrQkFLRSxLQUFLO3FDQUtMLE1BQU07d0JBS04sS0FBSzsrQkFLTCxLQUFLO3VCQVVMLEtBQUs7d0JBS0wsS0FBSzsrQkFLTCxTQUFTLFNBQUMsY0FBYyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTs7SUFzQzdDLDBCQUFDO0NBQUEsQUFsRkQsSUFrRkM7U0E3RVksbUJBQW1COzs7Ozs7SUFJOUIsMkNBQW1DOzs7OztJQUtuQyxpREFBd0Q7Ozs7O0lBS3hELG9DQUErQjs7Ozs7SUFLL0IsMkNBQTZCOzs7Ozs7Ozs7O0lBVTdCLG1DQUFtQjs7Ozs7SUFLbkIsb0NBQXlDOzs7Ozs7SUFLekMsMkNBQThFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25Jbml0LFxyXG4gIElucHV0LFxyXG4gIE91dHB1dCxcclxuICBFdmVudEVtaXR0ZXIsXHJcbiAgVmlld0NoaWxkLFxyXG4gIEVsZW1lbnRSZWZcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVGhlbWVQYWxldHRlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuLyoqXHJcbiAqIEEgY29tcG9uZW50IHRoYXQgd3JhcHMgYXJvdW5kIHRoZSBkZWZhdWx0IEhUTUwgZmlsZSBzZWxlY3QgYnV0dG9uLlxyXG4gKi9cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdrZXBzLWZpbGUtc2VsZWN0JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZmlsZS1zZWxlY3QuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZpbGUtc2VsZWN0LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZpbGVTZWxlY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIC8qKlxyXG4gICAqIEZpbGUgdG8gYmUgc2VsZWN0ZWQuXHJcbiAgICovXHJcbiAgQElucHV0KCkgc2VsZWN0ZWRGaWxlOiBGaWxlID0gbnVsbDtcclxuXHJcbiAgLyoqXHJcbiAgICogQW4gZXZlbnQgZW1pdHRlciB0aGF0IGVtaXRzIHRoZSBzZWxlY3RlZCBmaWxlLlxyXG4gICAqL1xyXG4gIEBPdXRwdXQoKSBzZWxlY3RlZEZpbGVDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPEZpbGU+KCk7XHJcblxyXG4gIC8qKlxyXG4gICAqIExhYmVsIGZvciB0aGUgZmlsZSBzZWxlY3QgYnV0dG9uLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGxhYmVsID0gJ0Nob29zZSBGaWxlJztcclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29tcG9uZW50IHNob3VsZCBkaXNwbGF5IHRoZSBmaWxlIG5hbWUuXHJcbiAgICovXHJcbiAgQElucHV0KCkgc2hvd0ZpbGVuYW1lID0gdHJ1ZTtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIHR5cGUocykgb2YgZmlsZSB0aGF0IHRoZSBmaWxlIHNlbGVjdCBkaWFsb2cgY2FuIGFjY2VwdC5cclxuICAgKlxyXG4gICAqIFNlZTogaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvSFRNTC9FbGVtZW50L2lucHV0L2ZpbGUjVW5pcXVlX2ZpbGVfdHlwZV9zcGVjaWZpZXJzXHJcbiAgICpcclxuICAgKiBJZiB0aGUgdmFsdWUgaXMgXCJpbWFnZVwiLCBcInZpZGVvXCIsIG9yIFwiYXVkaW9cIiwgaXQgd2lsbCBhY2NlcHQgZXZlcnkgcG9zc2libGUgZmlsZSB0eXBlc1xyXG4gICAqIG9mIHRoYXQgbWVkaWEgdHlwZS5cclxuICAgKi9cclxuICBASW5wdXQoKSB0eXBlID0gJyc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFuZ3VsYXIgTWF0ZXJpYWwgY29sb3IgZm9yIHRoZSBmaWxlIHNlbGVjdCBidXR0b24uXHJcbiAgICovXHJcbiAgQElucHV0KCkgY29sb3I6IFRoZW1lUGFsZXR0ZSA9ICdwcmltYXJ5JztcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIGBpbnB1dGAgZWxlbWVudCBpdHNlbGYuXHJcbiAgICovXHJcbiAgQFZpZXdDaGlsZCgnZmlsZVNlbGVjdG9yJywgeyBzdGF0aWM6IHRydWUgfSkgcHJpdmF0ZSBmaWxlU2VsZWN0b3I6IEVsZW1lbnRSZWY7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgaWYgKFxyXG4gICAgICB0aGlzLnR5cGUgPT09ICdpbWFnZScgfHxcclxuICAgICAgdGhpcy50eXBlID09PSAndmlkZW8nIHx8XHJcbiAgICAgIHRoaXMudHlwZSA9PT0gJ2F1ZGlvJ1xyXG4gICAgKSB7XHJcbiAgICAgIHRoaXMudHlwZSArPSAnLyonO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogT3BlbiB0aGUgZmlsZSBzZWxlY3QgZGlhbG9nIG1hbnVhbGx5LlxyXG4gICAqL1xyXG4gIG9wZW5GaWxlU2VsZWN0KCkge1xyXG4gICAgdGhpcy5maWxlU2VsZWN0b3IubmF0aXZlRWxlbWVudC5jbGljaygpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2VsZWN0IGEgZmlsZS5cclxuICAgKiBAcGFyYW0gZmlsZXMgRmlsZXMgdG8gYmUgcGFzc2VkLiBPbmx5IHRoZSBmaXJzdCBvbmUgd2lsbCBiZSBzZWxlY3RlZC5cclxuICAgKi9cclxuICBzZWxlY3RGaWxlKGZpbGVzOiBGaWxlW10pIHtcclxuICAgIGlmIChmaWxlcyAmJiBmaWxlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRGaWxlID0gZmlsZXNbMF07XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRGaWxlQ2hhbmdlLmVtaXQodGhpcy5zZWxlY3RlZEZpbGUpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19