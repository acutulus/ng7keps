/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
/**
 * A component that wraps around an Angular Material dialog.
 *
 * Use the {\@link PopupService} to create this dialog.
 */
var DialogComponent = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function DialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.buttons = this.data.buttons || [];
        if (this.data.showClose !== undefined) {
            this.showClose = this.data.showClose;
        }
        else {
            this.showClose = !(this.buttons.length > 0);
        }
    }
    DialogComponent.decorators = [
        { type: Component, args: [{
                    selector: 'keps-dialog',
                    template: "<section class=\"mat-typography\">\r\n  <h1 mat-dialog-title>{{ data.title }}</h1>\r\n  <div mat-dialog-content>\r\n    <p>{{ data.text }}</p>\r\n  </div>\r\n  <div mat-dialog-actions class=\"dialog-actions\">\r\n    <button\r\n      *ngFor=\"let button of buttons\"\r\n      [mat-dialog-close]=\"button.result\"\r\n      mat-button [color]=\"button.color\"\r\n    >\r\n     {{ button.label }}\r\n    </button>\r\n    <button\r\n      *ngIf=\"showClose\"\r\n      [mat-dialog-close]=\"undefined\"\r\n      mat-button\r\n    >\r\n      Close\r\n    </button>\r\n  </div>\r\n</section>\r\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    DialogComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
    ]; };
    return DialogComponent;
}());
export { DialogComponent };
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    DialogComponent.prototype.buttons;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    DialogComponent.prototype.showClose;
    /** @type {?} */
    DialogComponent.prototype.dialogRef;
    /** @type {?} */
    DialogComponent.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50L2RpYWxvZy9kaWFsb2cuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLG1CQUFtQixDQUFDOzs7Ozs7QUFRbEU7SUFnQkU7O09BRUc7SUFDSCx5QkFDUyxTQUF3QyxFQUNmLElBQWdCO1FBRHpDLGNBQVMsR0FBVCxTQUFTLENBQStCO1FBQ2YsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUVoRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUN2QyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxLQUFLLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1NBQ3RDO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztTQUM3QztJQUNILENBQUM7O2dCQTdCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLHVsQkFBc0M7O2lCQUV2Qzs7OztnQkFaUSxZQUFZO2dEQTZCaEIsTUFBTSxTQUFDLGVBQWU7O0lBVTNCLHNCQUFDO0NBQUEsQUEvQkQsSUErQkM7U0ExQlksZUFBZTs7Ozs7O0lBSTFCLGtDQUF3Qjs7Ozs7SUFLeEIsb0NBQW1COztJQU1qQixvQ0FBK0M7O0lBQy9DLCtCQUFnRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBEaWFsb2dEYXRhLCBEaWFsb2dCdXR0b24gfSBmcm9tICcuL2RpYWxvZy1pbnRlcmZhY2VzJztcclxuXHJcbi8qKlxyXG4gKiBBIGNvbXBvbmVudCB0aGF0IHdyYXBzIGFyb3VuZCBhbiBBbmd1bGFyIE1hdGVyaWFsIGRpYWxvZy5cclxuICpcclxuICogVXNlIHRoZSB7QGxpbmsgUG9wdXBTZXJ2aWNlfSB0byBjcmVhdGUgdGhpcyBkaWFsb2cuXHJcbiAqL1xyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2tlcHMtZGlhbG9nJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZGlhbG9nLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9kaWFsb2cuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGlhbG9nQ29tcG9uZW50IHtcclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBidXR0b25zOiBEaWFsb2dCdXR0b25bXTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2hvd0Nsb3NlOiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxEaWFsb2dDb21wb25lbnQ+LFxyXG4gICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhOiBEaWFsb2dEYXRhXHJcbiAgKSB7XHJcbiAgICB0aGlzLmJ1dHRvbnMgPSB0aGlzLmRhdGEuYnV0dG9ucyB8fCBbXTtcclxuICAgIGlmICh0aGlzLmRhdGEuc2hvd0Nsb3NlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgdGhpcy5zaG93Q2xvc2UgPSB0aGlzLmRhdGEuc2hvd0Nsb3NlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zaG93Q2xvc2UgPSAhKHRoaXMuYnV0dG9ucy5sZW5ndGggPiAwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiJdfQ==