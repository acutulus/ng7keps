/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ElementRef, Optional, Self } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { Subject } from 'rxjs';
import moment from 'moment';
var ɵ0 = MAT_MOMENT_DATE_FORMATS;
var FieldDatetimeComponent = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function FieldDatetimeComponent(fm, elRef, ngControl) {
        var _this = this;
        this.fm = fm;
        this.elRef = elRef;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-datetime';
        /**
         * \@internal Do not use!
         */
        this.id = "keps-field-datetime-" + FieldDatetimeComponent.nextId++;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new FormControl(null);
        /**
         * Form control for the date.
         * \@internal Do not use!
         */
        this.dateForm = new FormControl(null);
        /**
         * The current date for placeholder.
         * \@internal Do not use!
         */
        this.startDate = moment();
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        function (origin) {
            _this.focused = !!origin;
            _this.stateChanges.next();
        }));
    }
    Object.defineProperty(FieldDatetimeComponent.prototype, "value", {
        /**
         * The value of the control.
         */
        get: /**
         * The value of the control.
         * @return {?}
         */
        function () {
            return this.form.value;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} d
         * @return {?}
         */
        function (d) {
            this.form.setValue(d);
            if (d) {
                this.dateForm.setValue(moment(d));
            }
            else {
                this.dateForm.setValue(null);
            }
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldDatetimeComponent.prototype, "required", {
        /**
         * Whether the control is required.
         */
        get: /**
         * Whether the control is required.
         * @return {?}
         */
        function () {
            return this._required;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._required = coerceBooleanProperty(value);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldDatetimeComponent.prototype, "disabled", {
        /**
         * Whether the control is disabled.
         */
        get: /**
         * Whether the control is disabled.
         * @return {?}
         */
        function () {
            return this._disabled;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._disabled = coerceBooleanProperty(value);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldDatetimeComponent.prototype, "placeholder", {
        /**
         * The placeholder for this control.
         */
        get: /**
         * The placeholder for this control.
         * @return {?}
         */
        function () {
            return this._placeholder;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._placeholder = value;
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldDatetimeComponent.prototype, "empty", {
        /**
         * Whether the control is empty.
         */
        get: /**
         * Whether the control is empty.
         * @return {?}
         */
        function () {
            return this.form.value == null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldDatetimeComponent.prototype, "shouldLabelFloat", {
        /**
         * Whether the MatFormField label should try to float.
         */
        get: /**
         * Whether the MatFormField label should try to float.
         * @return {?}
         */
        function () {
            return this.focused || !this.empty;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    FieldDatetimeComponent.prototype.setDescribedByIds = /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    function (ids) {
        this.describedBy = ids.join(' ');
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    FieldDatetimeComponent.prototype.onContainerClick = /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    };
    /**
     * Sets the forms value after selecting a date on datepicker.
     * @internal Do not use!
     * @param event Changes to the date picker.
     */
    /**
     * Sets the forms value after selecting a date on datepicker.
     * \@internal Do not use!
     * @param {?} event Changes to the date picker.
     * @return {?}
     */
    FieldDatetimeComponent.prototype.onDateChange = /**
     * Sets the forms value after selecting a date on datepicker.
     * \@internal Do not use!
     * @param {?} event Changes to the date picker.
     * @return {?}
     */
    function (event) {
        if (event.value) {
            this.value = event.value.valueOf();
        }
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    FieldDatetimeComponent.prototype.ngOnDestroy = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    };
    /**
     * \@internal Do not use!
     */
    FieldDatetimeComponent.nextId = 0;
    FieldDatetimeComponent.decorators = [
        { type: Component, args: [{
                    selector: 'keps-field-datetime',
                    template: "<div class=\"field-datetime\">\r\n  <input\r\n    matInput\r\n    [matDatepicker]=\"dp\"\r\n    [formControl]=\"dateForm\"\r\n    (dateInput)=\"onDateChange($event)\"\r\n    placeholder=\"Choose a date\"\r\n  >\r\n  <mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n  <mat-datepicker #dp [startAt]=\"startDate\"></mat-datepicker>\r\n</div>  \r\n",
                    providers: [
                        {
                            provide: MatFormFieldControl,
                            useExisting: FieldDatetimeComponent
                        },
                        {
                            provide: DateAdapter,
                            useClass: MomentDateAdapter,
                            deps: [MAT_DATE_LOCALE]
                        },
                        {
                            provide: MAT_DATE_FORMATS,
                            useValue: ɵ0
                        }
                    ],
                    styles: [".field-datetime{width:100%;display:flex;height:13.5px}::ng-deep mat-datepicker-toggle>button{bottom:16px}"]
                }] }
    ];
    /** @nocollapse */
    FieldDatetimeComponent.ctorParameters = function () { return [
        { type: FocusMonitor },
        { type: ElementRef },
        { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
    ]; };
    FieldDatetimeComponent.propDecorators = {
        form: [{ type: Input }],
        value: [{ type: Input }],
        required: [{ type: Input }],
        disabled: [{ type: Input }],
        placeholder: [{ type: Input }]
    };
    return FieldDatetimeComponent;
}());
export { FieldDatetimeComponent };
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.nextId;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype._required;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype._disabled;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype._placeholder;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.stateChanges;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.focused;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.errorState;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.controlType;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.id;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.describedBy;
    /**
     * Form control of the field.
     * @type {?}
     */
    FieldDatetimeComponent.prototype.form;
    /**
     * Form control for the date.
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.dateForm;
    /**
     * The current date for placeholder.
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.startDate;
    /**
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype.fm;
    /**
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype.elRef;
    /** @type {?} */
    FieldDatetimeComponent.prototype.ngControl;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtZGF0ZXRpbWUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvZm9ybS9maWVsZC1kYXRldGltZS9maWVsZC1kYXRldGltZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBRVQsS0FBSyxFQUNMLFVBQVUsRUFDVixRQUFRLEVBQ1IsSUFBSSxFQUNMLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUEyQixNQUFNLG1CQUFtQixDQUFDO0FBQ2pJLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlGLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxNQUFNLE1BQU0sUUFBUSxDQUFDO1NBa0JaLHVCQUF1QjtBQWhCdkM7SUF3RkU7O09BRUc7SUFDSCxnQ0FDVSxFQUFnQixFQUNoQixLQUE4QixFQUNYLFNBQW9CO1FBSGpELGlCQVNDO1FBUlMsT0FBRSxHQUFGLEVBQUUsQ0FBYztRQUNoQixVQUFLLEdBQUwsS0FBSyxDQUF5QjtRQUNYLGNBQVMsR0FBVCxTQUFTLENBQVc7Ozs7UUFqRXpDLGNBQVMsR0FBRyxLQUFLLENBQUM7Ozs7UUFLbEIsY0FBUyxHQUFHLEtBQUssQ0FBQzs7OztRQVUxQixpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7Ozs7UUFLbkMsWUFBTyxHQUFHLEtBQUssQ0FBQzs7OztRQUtoQixlQUFVLEdBQUcsS0FBSyxDQUFDOzs7O1FBS25CLGdCQUFXLEdBQUcscUJBQXFCLENBQUM7Ozs7UUFLcEMsT0FBRSxHQUFHLHlCQUF1QixzQkFBc0IsQ0FBQyxNQUFNLEVBQUksQ0FBQzs7OztRQUs5RCxnQkFBVyxHQUFHLEVBQUUsQ0FBQzs7OztRQUtSLFNBQUksR0FBRyxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7Ozs7UUFNdEMsYUFBUSxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDOzs7OztRQU1qQyxjQUFTLEdBQUcsTUFBTSxFQUFFLENBQUM7UUFVbkIsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTTtZQUN0QyxLQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDeEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7SUFLRCxzQkFDSSx5Q0FBSztRQUpUOztXQUVHOzs7OztRQUNIO1lBRUUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6QixDQUFDO1FBRUQ7O1dBRUc7Ozs7OztRQUNILFVBQVUsQ0FBZ0I7WUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLEVBQUU7Z0JBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDbkM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDOUI7WUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLENBQUM7OztPQWJBO0lBa0JELHNCQUNJLDRDQUFRO1FBSlo7O1dBRUc7Ozs7O1FBQ0g7WUFFRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDeEIsQ0FBQztRQUVEOztXQUVHOzs7Ozs7UUFDSCxVQUFhLEtBQWM7WUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLENBQUM7OztPQVJBO0lBYUQsc0JBQ0ksNENBQVE7UUFKWjs7V0FFRzs7Ozs7UUFDSDtZQUVFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN4QixDQUFDO1FBRUQ7O1dBRUc7Ozs7OztRQUNILFVBQWEsS0FBYztZQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsQ0FBQzs7O09BUkE7SUFhRCxzQkFDSSwrQ0FBVztRQUpmOztXQUVHOzs7OztRQUNIO1lBRUUsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQzNCLENBQUM7UUFFRDs7V0FFRzs7Ozs7O1FBQ0gsVUFBZ0IsS0FBYTtZQUMzQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLENBQUM7OztPQVJBO0lBYUQsc0JBQUkseUNBQUs7UUFIVDs7V0FFRzs7Ozs7UUFDSDtZQUNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDO1FBQ2pDLENBQUM7OztPQUFBO0lBS0Qsc0JBQUksb0RBQWdCO1FBSHBCOztXQUVHOzs7OztRQUNIO1lBQ0UsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNyQyxDQUFDOzs7T0FBQTtJQUVEOztPQUVHOzs7Ozs7SUFDSCxrREFBaUI7Ozs7O0lBQWpCLFVBQWtCLEdBQWE7UUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0gsaURBQWdCOzs7OztJQUFoQixVQUFpQixLQUFpQjtRQUNoQyxJQUFJLENBQUMsbUJBQUEsS0FBSyxDQUFDLE1BQU0sRUFBVyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxLQUFLLE9BQU8sRUFBRTtZQUMvRCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDekQ7SUFDSCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7OztJQUNILDZDQUFZOzs7Ozs7SUFBWixVQUFhLEtBQTZDO1FBQ3hELElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtZQUNmLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNwQztJQUNILENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCw0Q0FBVzs7OztJQUFYO1FBQ0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQzs7OztJQWxNTSw2QkFBTSxHQUFHLENBQUMsQ0FBQzs7Z0JBeEJuQixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0IsNlhBQThDO29CQUU5QyxTQUFTLEVBQUU7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLG1CQUFtQjs0QkFDNUIsV0FBVyxFQUFFLHNCQUFzQjt5QkFDcEM7d0JBQ0Q7NEJBQ0UsT0FBTyxFQUFFLFdBQVc7NEJBQ3BCLFFBQVEsRUFBRSxpQkFBaUI7NEJBQzNCLElBQUksRUFBRSxDQUFDLGVBQWUsQ0FBQzt5QkFDeEI7d0JBQ0Q7NEJBQ0UsT0FBTyxFQUFFLGdCQUFnQjs0QkFDekIsUUFBUSxJQUF5Qjt5QkFDbEM7cUJBQ0Y7O2lCQUNGOzs7O2dCQTNCUSxZQUFZO2dCQUpuQixVQUFVO2dCQU1VLFNBQVMsdUJBb0cxQixRQUFRLFlBQUksSUFBSTs7O3VCQXBCbEIsS0FBSzt3QkErQkwsS0FBSzsyQkFxQkwsS0FBSzsyQkFnQkwsS0FBSzs4QkFnQkwsS0FBSzs7SUE4RFIsNkJBQUM7Q0FBQSxBQTVORCxJQTROQztTQXhNWSxzQkFBc0I7Ozs7OztJQUlqQyw4QkFBa0I7Ozs7OztJQUtsQiwyQ0FBMEI7Ozs7OztJQUsxQiwyQ0FBMEI7Ozs7OztJQUsxQiw4Q0FBNkI7Ozs7O0lBSzdCLDhDQUFtQzs7Ozs7SUFLbkMseUNBQWdCOzs7OztJQUtoQiw0Q0FBbUI7Ozs7O0lBS25CLDZDQUFvQzs7Ozs7SUFLcEMsb0NBQThEOzs7OztJQUs5RCw2Q0FBaUI7Ozs7O0lBS2pCLHNDQUFzQzs7Ozs7O0lBTXRDLDBDQUFpQzs7Ozs7O0lBTWpDLDJDQUFxQjs7Ozs7SUFNbkIsb0NBQXdCOzs7OztJQUN4Qix1Q0FBc0M7O0lBQ3RDLDJDQUErQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIE9uRGVzdHJveSxcclxuICBJbnB1dCxcclxuICBFbGVtZW50UmVmLFxyXG4gIE9wdGlvbmFsLFxyXG4gIFNlbGZcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9jdXNNb25pdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2ExMXknO1xyXG5pbXBvcnQgeyBjb2VyY2VCb29sZWFuUHJvcGVydHkgfSBmcm9tICdAYW5ndWxhci9jZGsvY29lcmNpb24nO1xyXG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgTmdDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBNYXRGb3JtRmllbGRDb250cm9sLCBEYXRlQWRhcHRlciwgTUFUX0RBVEVfRk9STUFUUywgTUFUX0RBVEVfTE9DQUxFLCBNYXREYXRlcGlja2VySW5wdXRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgTUFUX01PTUVOVF9EQVRFX0ZPUk1BVFMsIE1vbWVudERhdGVBZGFwdGVyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwtbW9tZW50LWFkYXB0ZXInO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy1maWVsZC1kYXRldGltZScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZpZWxkLWRhdGV0aW1lLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9maWVsZC1kYXRldGltZS5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBNYXRGb3JtRmllbGRDb250cm9sLFxyXG4gICAgICB1c2VFeGlzdGluZzogRmllbGREYXRldGltZUNvbXBvbmVudFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogRGF0ZUFkYXB0ZXIsXHJcbiAgICAgIHVzZUNsYXNzOiBNb21lbnREYXRlQWRhcHRlcixcclxuICAgICAgZGVwczogW01BVF9EQVRFX0xPQ0FMRV1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IE1BVF9EQVRFX0ZPUk1BVFMsXHJcbiAgICAgIHVzZVZhbHVlOiBNQVRfTU9NRU5UX0RBVEVfRk9STUFUU1xyXG4gICAgfVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZpZWxkRGF0ZXRpbWVDb21wb25lbnQgaW1wbGVtZW50cyBNYXRGb3JtRmllbGRDb250cm9sPG51bWJlcj4sIE9uRGVzdHJveSB7XHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc3RhdGljIG5leHRJZCA9IDA7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX3JlcXVpcmVkID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX2Rpc2FibGVkID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX3BsYWNlaG9sZGVyOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHN0YXRlQ2hhbmdlcyA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGZvY3VzZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZXJyb3JTdGF0ZSA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb250cm9sVHlwZSA9ICdrZXBzLWZpZWxkLWRhdGV0aW1lJztcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgaWQgPSBga2Vwcy1maWVsZC1kYXRldGltZS0ke0ZpZWxkRGF0ZXRpbWVDb21wb25lbnQubmV4dElkKyt9YDtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZGVzY3JpYmVkQnkgPSAnJztcclxuXHJcbiAgLyoqXHJcbiAgICogRm9ybSBjb250cm9sIG9mIHRoZSBmaWVsZC5cclxuICAgKi9cclxuICBASW5wdXQoKSBmb3JtID0gbmV3IEZvcm1Db250cm9sKG51bGwpO1xyXG5cclxuICAvKipcclxuICAgKiBGb3JtIGNvbnRyb2wgZm9yIHRoZSBkYXRlLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGRhdGVGb3JtID0gbmV3IEZvcm1Db250cm9sKG51bGwpO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgY3VycmVudCBkYXRlIGZvciBwbGFjZWhvbGRlci5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzdGFydERhdGUgPSBtb21lbnQoKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZtOiBGb2N1c01vbml0b3IsXHJcbiAgICBwcml2YXRlIGVsUmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PixcclxuICAgIEBPcHRpb25hbCgpIEBTZWxmKCkgcHVibGljIG5nQ29udHJvbDogTmdDb250cm9sXHJcbiAgKSB7XHJcbiAgICBmbS5tb25pdG9yKGVsUmVmLCB0cnVlKS5zdWJzY3JpYmUob3JpZ2luID0+IHtcclxuICAgICAgdGhpcy5mb2N1c2VkID0gISFvcmlnaW47XHJcbiAgICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIHZhbHVlIG9mIHRoZSBjb250cm9sLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IHZhbHVlKCk6IG51bWJlciB8IG51bGwge1xyXG4gICAgcmV0dXJuIHRoaXMuZm9ybS52YWx1ZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCB2YWx1ZShkOiBudW1iZXIgfCBudWxsKSB7XHJcbiAgICB0aGlzLmZvcm0uc2V0VmFsdWUoZCk7XHJcbiAgICBpZiAoZCkge1xyXG4gICAgICB0aGlzLmRhdGVGb3JtLnNldFZhbHVlKG1vbWVudChkKSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmRhdGVGb3JtLnNldFZhbHVlKG51bGwpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyByZXF1aXJlZC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCByZXF1aXJlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9yZXF1aXJlZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCByZXF1aXJlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fcmVxdWlyZWQgPSBjb2VyY2VCb29sZWFuUHJvcGVydHkodmFsdWUpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyBkaXNhYmxlZC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCBkaXNhYmxlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9kaXNhYmxlZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCBkaXNhYmxlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fZGlzYWJsZWQgPSBjb2VyY2VCb29sZWFuUHJvcGVydHkodmFsdWUpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIHBsYWNlaG9sZGVyIGZvciB0aGlzIGNvbnRyb2wuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgcGxhY2Vob2xkZXIoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLl9wbGFjZWhvbGRlcjtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCBwbGFjZWhvbGRlcih2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLl9wbGFjZWhvbGRlciA9IHZhbHVlO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyBlbXB0eS5cclxuICAgKi9cclxuICBnZXQgZW1wdHkoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5mb3JtLnZhbHVlID09IG51bGw7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBNYXRGb3JtRmllbGQgbGFiZWwgc2hvdWxkIHRyeSB0byBmbG9hdC5cclxuICAgKi9cclxuICBnZXQgc2hvdWxkTGFiZWxGbG9hdCgpIHtcclxuICAgIHJldHVybiB0aGlzLmZvY3VzZWQgfHwgIXRoaXMuZW1wdHk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXREZXNjcmliZWRCeUlkcyhpZHM6IHN0cmluZ1tdKSB7XHJcbiAgICB0aGlzLmRlc2NyaWJlZEJ5ID0gaWRzLmpvaW4oJyAnKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG9uQ29udGFpbmVyQ2xpY2soZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgIGlmICgoZXZlbnQudGFyZ2V0IGFzIEVsZW1lbnQpLnRhZ05hbWUudG9Mb3dlckNhc2UoKSAhPT0gJ2lucHV0Jykge1xyXG4gICAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXQnKS5mb2N1cygpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2V0cyB0aGUgZm9ybXMgdmFsdWUgYWZ0ZXIgc2VsZWN0aW5nIGEgZGF0ZSBvbiBkYXRlcGlja2VyLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqIEBwYXJhbSBldmVudCBDaGFuZ2VzIHRvIHRoZSBkYXRlIHBpY2tlci5cclxuICAgKi9cclxuICBvbkRhdGVDaGFuZ2UoZXZlbnQ6IE1hdERhdGVwaWNrZXJJbnB1dEV2ZW50PG1vbWVudC5Nb21lbnQ+KSB7XHJcbiAgICBpZiAoZXZlbnQudmFsdWUpIHtcclxuICAgICAgdGhpcy52YWx1ZSA9IGV2ZW50LnZhbHVlLnZhbHVlT2YoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMuY29tcGxldGUoKTtcclxuICAgIHRoaXMuZm0uc3RvcE1vbml0b3JpbmcodGhpcy5lbFJlZik7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=