/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Inject, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { KepsForm } from './keps-form';
import { KepsFormArray } from './keps-form-array';
import { isType, getReferenceDisplay } from '../../utils';
/**
 * A component that renders form based on Keps schema.
 */
var FormComponent = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function FormComponent(models) {
        this.models = models;
        /**
         * An instance of Keps form.
         */
        this.form = new KepsForm({});
        /**
         * A map of reference to Keps objects to be shown for reference fields.
         *
         * Notes:-The key of the map is based on the field name of the Keps form model.
         *        Not the name of the model it is referenced to.
         *       -If field type is a string and a refObjs is provided,
         *        instead of a normal text input, it will be a select. see html for more info.
         *
         * **sample usage**
         * given the following models:
         *  PERSON {
         *    _id
         *    name: 'string',
         *    pet: ':ANIMAL'
         *  }
         *
         *  ANIMAL {
         *    _id
         *    type: 'string'
         *  }
         *
         *  if one wish to create a form for
         *  object PERSON where the options for ANIMAL
         *  comes from DB, that dev can create a refObjs:KepsReference
         *  which  will look like:
         *
         *  refObjs = {
         *              'pet': [
         *                <ANIMAL>{
         *                    _id
         *                    type
         *                  },
         *                <ANIMAL>{
         *                    _id
         *                    type
         *                },
         *                ...
         *                ...
         *                ..
         *              ],
         *            }
         *
         *  SIDENOTE: The annotation <TYPE> is used to make this exmaple more explicit.
         */
        this.refObjs = {};
        /**
         * Appearance style of the form.
         */
        this.appearance = 'standard';
        /**
         * Angular Material color of the form field underline and floating label.
         */
        this.color = 'primary';
        /**
         * Fields to be displayed on the form.
         */
        this.displayedFields = [];
        /**
         * Fields to be hidden on the form.
         */
        this.hiddenFields = [];
        /**
         * Placeholders to be displayed on fields.
         */
        this.placeholders = {};
        /**
         * Hints for fields input.
         */
        this.hints = {};
        /**
         * Whether the form has done initializing or not.
         */
        this.initialized = false;
    }
    /**
     * Initializes the form with fields based on the schema.
     * @internal Do not use!
     */
    /**
     * Initializes the form with fields based on the schema.
     * \@internal Do not use!
     * @return {?}
     */
    FormComponent.prototype.ngOnInit = /**
     * Initializes the form with fields based on the schema.
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        var e_1, _a;
        var _this = this;
        // Get schema from model name if supplied.
        if (this.model && this.models[this.model]) {
            this.schema = this.models[this.model].schema;
        }
        else if (this.model && !this.models[this.model]) {
            throw new Error("Can't find a model named " + this.model + "!");
        }
        // Assert schema must exist.
        if (!this.schema) {
            throw new Error("Must supply valid model name or schema to Keps form!");
        }
        // Set schema fields.
        if (this.displayedFields.length > 0) {
            this.schemaFields = this.displayedFields;
        }
        else {
            this.schemaFields = Object.keys(this.schema)
                .filter((/**
             * @param {?} field
             * @return {?}
             */
            function (field) { return field[0] !== '_' && !_this.hiddenFields.includes(field); }));
        }
        this.form.setSchema(this.schema);
        try {
            for (var _b = tslib_1.__values(this.schemaFields), _c = _b.next(); !_c.done; _c = _b.next()) {
                var fieldName = _c.value;
                /** @type {?} */
                var field = this.schema[fieldName];
                // Create validators if needed.
                /** @type {?} */
                var validators = [];
                if (!field.optional) {
                    validators.push(Validators.required);
                }
                if (field.type === 'number') {
                }
                if (field.type === 'email') {
                    validators.push(Validators.email);
                }
                // Create the appropriate form control for each field.
                if (field.type === 'address') {
                    this.form.addControl(fieldName, new FormGroup({}, validators));
                }
                else if (field.type === 'array') {
                    this.form.addControl(fieldName, new KepsFormArray([], validators));
                }
                else if (field.type === 'datetime') {
                    this.form.addControl(fieldName, new FormControl(null, validators));
                }
                else {
                    this.form.addControl(fieldName, new FormControl(field.default || null, validators));
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this.form.initialize();
        this.initialized = true;
    };
    /**
     * Checks the type of a field.
     * @internal Do not use!
     * @param field Field to be checked.
     * @param type Type to be checked.
     */
    /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    FormComponent.prototype.isType = /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    function (field, type) {
        return isType(this.schema, field, type);
    };
    /**
     * Returns the display expression of a reference field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    FormComponent.prototype.getReferenceDisplay = /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    function (field) {
        return getReferenceDisplay(this.models, this.schema, field);
    };
    /**
     * Check if MatFormField should hide the underline or not.
     * @internal Do not use!
     * @param field Field to be checked.
     */
    /**
     * Check if MatFormField should hide the underline or not.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @return {?}
     */
    FormComponent.prototype.isNotUnderlined = /**
     * Check if MatFormField should hide the underline or not.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @return {?}
     */
    function (field) {
        return this.schema[field].type === 'boolean' ||
            this.schema[field].type === 'address' ||
            this.schema[field].type === 'array';
    };
    /**
    * Check if a field has a reference in refObjs.
    * if has reference in refobjs, use mat-select instead of input.
    * supports only type string at the moment.
    * @internal Do not use!
    * @param field Field to be checked.
    */
    /**
     * Check if a field has a reference in refObjs.
     * if has reference in refobjs, use mat-select instead of input.
     * supports only type string at the moment.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @return {?}
     */
    FormComponent.prototype.hasReference = /**
     * Check if a field has a reference in refObjs.
     * if has reference in refobjs, use mat-select instead of input.
     * supports only type string at the moment.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @return {?}
     */
    function (field) {
        return this.schema[field].type === 'string' &&
            this.refObjs &&
            this.refObjs[field];
    };
    FormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'keps-form',
                    template: "<form\r\n  *ngIf=\"initialized\"\r\n  [formGroup]=\"form\" \r\n  class=\"keps-form mat-typography\"\r\n>\r\n  <ng-container *ngFor=\"let fieldName of schemaFields\">\r\n    <ng-container *ngIf=\"schema[fieldName]; let field\">\r\n      <mat-form-field\r\n        floatLabel=\"always\"\r\n        [appearance]=\"appearance\"\r\n        [color]=\"color\"\r\n        [class.hide-underline]=\"isNotUnderlined(fieldName)\"\r\n      >\r\n        <mat-label>\r\n          {{ field.label || fieldName | splitCamel | titlecase }}\r\n        </mat-label>\r\n  \r\n        <!-- Type: String -->\r\n        <!-- reference is not provided, use input-->\r\n        <input\r\n          *ngIf=\"isType(fieldName, 'string') && !hasReference(fieldName)\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          type=\"text\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n          matInput\r\n        >\r\n        <!-- Type: String -->\r\n        <!-- If reference is provided, use select -->\r\n        <mat-select \r\n          *ngIf=\"isType(fieldName, 'string') && hasReference(fieldName)\" \r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\" >\r\n          <mat-option *ngIf=\"field.optional\">None</mat-option>\r\n          <mat-option *ngFor=\"let option of refObjs[fieldName]\" [value]=\"option\">{{option}}</mat-option>\r\n        </mat-select>\r\n  \r\n        <!-- Type: Number -->\r\n        <input\r\n          *ngIf=\"isType(fieldName, 'number')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          type=\"number\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n          [min]=\"field.min\"\r\n          [max]=\"field.max\"\r\n          [step]=\"field.step\"\r\n          matInput\r\n        >\r\n\r\n        <!-- Type: Email -->\r\n        <input\r\n          *ngIf=\"isType(fieldName, 'email')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          type=\"email\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n          matInput\r\n        >\r\n\r\n        <!-- Type: Boolean -->\r\n        <keps-field-boolean\r\n          *ngIf=\"isType(fieldName, 'boolean')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n        >\r\n        </keps-field-boolean>\r\n\r\n        <!-- Type: Enum & Multi -->\r\n        <mat-select\r\n          *ngIf=\"isType(fieldName, 'enum') || isType(fieldName, 'multi')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          [multiple]=\"isType(fieldName, 'multi')\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n        >\r\n          <mat-option\r\n            *ngFor=\"let option of field.options\"\r\n            [value]=\"option\"\r\n          >\r\n            {{ field.labels && field.labels[option] ? field.labels[option] : option }}\r\n          </mat-option>\r\n        </mat-select>\r\n\r\n        <!-- Type: Address -->\r\n        <keps-field-address\r\n          *ngIf=\"isType(fieldName, 'address')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          [appearance]=\"appearance\"\r\n          [color]=\"color\"\r\n        >\r\n        </keps-field-address>\r\n\r\n        <!-- Type: Array -->\r\n        <keps-field-array\r\n          *ngIf=\"isType(fieldName, 'array')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [subSchema]=\"field.subSchema\"\r\n          [refObjs]=\"refObjs[field]\"\r\n          [required]=\"!field.optional\"\r\n          [appearance]=\"appearance\"\r\n          [color]=\"color\"\r\n        >\r\n        </keps-field-array>\r\n\r\n        <!-- Type: Reference -->\r\n        <mat-select\r\n          *ngIf=\"isType(fieldName, 'reference')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n        >\r\n          <mat-option\r\n            *ngFor=\"let option of refObjs[fieldName]\"\r\n            [value]=\"option._id\"\r\n          >\r\n            {{ option | eval:getReferenceDisplay(fieldName) }}\r\n          </mat-option>\r\n        </mat-select>\r\n\r\n        <!-- Type: Datetime -->\r\n        <keps-field-datetime\r\n          *ngIf=\"isType(fieldName, 'datetime')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n        >\r\n        </keps-field-datetime>\r\n\r\n        <!-- Hints -->\r\n        <mat-hint *ngIf=\"hints\">{{hints[fieldName]}}</mat-hint>\r\n\r\n      </mat-form-field>\r\n    </ng-container>\r\n  </ng-container>\r\n</form>\r\n",
                    styles: [".keps-form{display:flex;flex-direction:column}.keps-form>*{margin-bottom:1em}.keps-form-radio-group{display:inline-flex;flex-direction:column}.keps-form-radio-group>*{margin-bottom:.5em}.hide-underline::ng-deep .mat-form-field-underline{display:none!important}"]
                }] }
    ];
    /** @nocollapse */
    FormComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] }
    ]; };
    FormComponent.propDecorators = {
        form: [{ type: Input }],
        model: [{ type: Input }],
        schema: [{ type: Input }],
        refObjs: [{ type: Input }],
        appearance: [{ type: Input }],
        color: [{ type: Input }],
        displayedFields: [{ type: Input }],
        hiddenFields: [{ type: Input }],
        placeholders: [{ type: Input }],
        hints: [{ type: Input }]
    };
    return FormComponent;
}());
export { FormComponent };
if (false) {
    /**
     * An instance of Keps form.
     * @type {?}
     */
    FormComponent.prototype.form;
    /**
     * Name of the model for the form.
     * @type {?}
     */
    FormComponent.prototype.model;
    /**
     * Schema for the form.
     *
     * If the model name is supplied,
     * then the form will get the schema from that model.
     * @type {?}
     */
    FormComponent.prototype.schema;
    /**
     * A map of reference to Keps objects to be shown for reference fields.
     *
     * Notes:-The key of the map is based on the field name of the Keps form model.
     *        Not the name of the model it is referenced to.
     *       -If field type is a string and a refObjs is provided,
     *        instead of a normal text input, it will be a select. see html for more info.
     *
     * **sample usage**
     * given the following models:
     *  PERSON {
     *    _id
     *    name: 'string',
     *    pet: ':ANIMAL'
     *  }
     *
     *  ANIMAL {
     *    _id
     *    type: 'string'
     *  }
     *
     *  if one wish to create a form for
     *  object PERSON where the options for ANIMAL
     *  comes from DB, that dev can create a refObjs:KepsReference
     *  which  will look like:
     *
     *  refObjs = {
     *              'pet': [
     *                <ANIMAL>{
     *                    _id
     *                    type
     *                  },
     *                <ANIMAL>{
     *                    _id
     *                    type
     *                },
     *                ...
     *                ...
     *                ..
     *              ],
     *            }
     *
     *  SIDENOTE: The annotation <TYPE> is used to make this exmaple more explicit.
     * @type {?}
     */
    FormComponent.prototype.refObjs;
    /**
     * Appearance style of the form.
     * @type {?}
     */
    FormComponent.prototype.appearance;
    /**
     * Angular Material color of the form field underline and floating label.
     * @type {?}
     */
    FormComponent.prototype.color;
    /**
     * Fields to be displayed on the form.
     * @type {?}
     */
    FormComponent.prototype.displayedFields;
    /**
     * Fields to be hidden on the form.
     * @type {?}
     */
    FormComponent.prototype.hiddenFields;
    /**
     * Placeholders to be displayed on fields.
     * @type {?}
     */
    FormComponent.prototype.placeholders;
    /**
     * Hints for fields input.
     * @type {?}
     */
    FormComponent.prototype.hints;
    /**
     * Fields to be displayed on the form based on displayedFields and hiddenFields.
     * \@internal Do not use!
     * @type {?}
     */
    FormComponent.prototype.schemaFields;
    /**
     * Whether the form has done initializing or not.
     * @type {?}
     */
    FormComponent.prototype.initialized;
    /**
     * @type {?}
     * @private
     */
    FormComponent.prototype.models;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudC9mb3JtL2Zvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXBFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDdkMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRWxELE9BQU8sRUFBRSxNQUFNLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxhQUFhLENBQUM7Ozs7QUFLMUQ7SUErR0U7O09BRUc7SUFDSCx1QkFDNEIsTUFBa0I7UUFBbEIsV0FBTSxHQUFOLE1BQU0sQ0FBWTs7OztRQTFHckMsU0FBSSxHQUFHLElBQUksUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUEyRHhCLFlBQU8sR0FBa0IsRUFBRSxDQUFDOzs7O1FBSzVCLGVBQVUsR0FBMkIsVUFBVSxDQUFDOzs7O1FBS2hELFVBQUssR0FBaUIsU0FBUyxDQUFDOzs7O1FBS2hDLG9CQUFlLEdBQWEsRUFBRSxDQUFDOzs7O1FBSy9CLGlCQUFZLEdBQWEsRUFBRSxDQUFDOzs7O1FBSzVCLGlCQUFZLEdBQXdCLEVBQUUsQ0FBQzs7OztRQUt2QyxVQUFLLEdBQTJCLEVBQUUsQ0FBQzs7OztRQVc1QyxnQkFBVyxHQUFHLEtBQUssQ0FBQztJQU9oQixDQUFDO0lBRUw7OztPQUdHOzs7Ozs7SUFDSCxnQ0FBUTs7Ozs7SUFBUjs7UUFBQSxpQkF1REM7UUF0REMsMENBQTBDO1FBQzFDLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN6QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQztTQUM5QzthQUFNLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2pELE1BQU0sSUFBSSxLQUFLLENBQUMsOEJBQTRCLElBQUksQ0FBQyxLQUFLLE1BQUcsQ0FBQyxDQUFDO1NBQzVEO1FBRUQsNEJBQTRCO1FBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLE1BQU0sSUFBSSxLQUFLLENBQUMsc0RBQXNELENBQUMsQ0FBQztTQUN6RTtRQUVELHFCQUFxQjtRQUNyQixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNuQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7U0FDMUM7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO2lCQUMzQyxNQUFNOzs7O1lBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQXRELENBQXNELEVBQUMsQ0FBQztTQUMxRTtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzs7WUFFakMsS0FBd0IsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxZQUFZLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQXRDLElBQU0sU0FBUyxXQUFBOztvQkFDWixLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7OztvQkFHOUIsVUFBVSxHQUFHLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO29CQUNuQixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztpQkFDdEM7Z0JBQ0QsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFBRTtpQkFDNUI7Z0JBQ0QsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLE9BQU8sRUFBRTtvQkFDMUIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ25DO2dCQUVELHNEQUFzRDtnQkFDdEQsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtvQkFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLElBQUksU0FBUyxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO2lCQUNoRTtxQkFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssT0FBTyxFQUFFO29CQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxhQUFhLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUM7aUJBQ3BFO3FCQUFNLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxVQUFVLEVBQUU7b0JBQ3BDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQztpQkFDcEU7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQ2xCLFNBQVMsRUFDVCxJQUFJLFdBQVcsQ0FDYixLQUFLLENBQUMsT0FBTyxJQUFJLElBQUksRUFDckIsVUFBVSxDQUNYLENBQ0YsQ0FBQztpQkFDSDthQUNGOzs7Ozs7Ozs7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO0lBQzFCLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDSCw4QkFBTTs7Ozs7OztJQUFOLFVBQU8sS0FBYSxFQUFFLElBQVk7UUFDaEMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCwyQ0FBbUI7Ozs7OztJQUFuQixVQUFvQixLQUFhO1FBQy9CLE9BQU8sbUJBQW1CLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsdUNBQWU7Ozs7OztJQUFmLFVBQWdCLEtBQWE7UUFDM0IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTO1lBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxLQUFLLFNBQVM7WUFDckMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLEtBQUssT0FBTyxDQUFDO0lBQ3RDLENBQUM7SUFFRDs7Ozs7O01BTUU7Ozs7Ozs7OztJQUNGLG9DQUFZOzs7Ozs7OztJQUFaLFVBQWEsS0FBYTtRQUN4QixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxLQUFLLFFBQVE7WUFDM0MsSUFBSSxDQUFDLE9BQU87WUFDWixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RCLENBQUM7O2dCQTVORixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFdBQVc7b0JBQ3JCLCtySkFBb0M7O2lCQUVyQzs7OztnREErR0ksTUFBTSxTQUFDLFFBQVE7Ozt1QkExR2pCLEtBQUs7d0JBS0wsS0FBSzt5QkFRTCxLQUFLOzBCQThDTCxLQUFLOzZCQUtMLEtBQUs7d0JBS0wsS0FBSztrQ0FLTCxLQUFLOytCQUtMLEtBQUs7K0JBS0wsS0FBSzt3QkFLTCxLQUFLOztJQTRIUixvQkFBQztDQUFBLEFBOU5ELElBOE5DO1NBek5ZLGFBQWE7Ozs7OztJQUl4Qiw2QkFBaUM7Ozs7O0lBS2pDLDhCQUF1Qjs7Ozs7Ozs7SUFRdkIsK0JBQTRCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBOEM1QixnQ0FBcUM7Ozs7O0lBS3JDLG1DQUF5RDs7Ozs7SUFLekQsOEJBQXlDOzs7OztJQUt6Qyx3Q0FBd0M7Ozs7O0lBS3hDLHFDQUFxQzs7Ozs7SUFLckMscUNBQWdEOzs7OztJQUtoRCw4QkFBNEM7Ozs7OztJQU01QyxxQ0FBdUI7Ozs7O0lBS3ZCLG9DQUFvQjs7Ozs7SUFNbEIsK0JBQTRDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQ29udHJvbCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgVGhlbWVQYWxldHRlLCBNYXRGb3JtRmllbGRBcHBlYXJhbmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBLZXBzRm9ybSB9IGZyb20gJy4va2Vwcy1mb3JtJztcclxuaW1wb3J0IHsgS2Vwc0Zvcm1BcnJheSB9IGZyb20gJy4va2Vwcy1mb3JtLWFycmF5JztcclxuaW1wb3J0IHsgS2Vwc1NjaGVtYSwgS2Vwc01vZGVscywgS2Vwc1JlZmVyZW5jZSB9IGZyb20gJy4uLy4uL2ludGVyZmFjZSc7XHJcbmltcG9ydCB7IGlzVHlwZSwgZ2V0UmVmZXJlbmNlRGlzcGxheSB9IGZyb20gJy4uLy4uL3V0aWxzJztcclxuXHJcbi8qKlxyXG4gKiBBIGNvbXBvbmVudCB0aGF0IHJlbmRlcnMgZm9ybSBiYXNlZCBvbiBLZXBzIHNjaGVtYS5cclxuICovXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy1mb3JtJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZm9ybS5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9ybS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAvKipcclxuICAgKiBBbiBpbnN0YW5jZSBvZiBLZXBzIGZvcm0uXHJcbiAgICovXHJcbiAgQElucHV0KCkgZm9ybSA9IG5ldyBLZXBzRm9ybSh7fSk7XHJcblxyXG4gIC8qKlxyXG4gICAqIE5hbWUgb2YgdGhlIG1vZGVsIGZvciB0aGUgZm9ybS5cclxuICAgKi9cclxuICBASW5wdXQoKSBtb2RlbDogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBTY2hlbWEgZm9yIHRoZSBmb3JtLlxyXG4gICAqXHJcbiAgICogSWYgdGhlIG1vZGVsIG5hbWUgaXMgc3VwcGxpZWQsXHJcbiAgICogdGhlbiB0aGUgZm9ybSB3aWxsIGdldCB0aGUgc2NoZW1hIGZyb20gdGhhdCBtb2RlbC5cclxuICAgKi9cclxuICBASW5wdXQoKSBzY2hlbWE6IEtlcHNTY2hlbWE7XHJcblxyXG4gIC8qKlxyXG4gICAqIEEgbWFwIG9mIHJlZmVyZW5jZSB0byBLZXBzIG9iamVjdHMgdG8gYmUgc2hvd24gZm9yIHJlZmVyZW5jZSBmaWVsZHMuXHJcbiAgICpcclxuICAgKiBOb3RlczotVGhlIGtleSBvZiB0aGUgbWFwIGlzIGJhc2VkIG9uIHRoZSBmaWVsZCBuYW1lIG9mIHRoZSBLZXBzIGZvcm0gbW9kZWwuXHJcbiAgICogICAgICAgIE5vdCB0aGUgbmFtZSBvZiB0aGUgbW9kZWwgaXQgaXMgcmVmZXJlbmNlZCB0by5cclxuICAgKiAgICAgICAtSWYgZmllbGQgdHlwZSBpcyBhIHN0cmluZyBhbmQgYSByZWZPYmpzIGlzIHByb3ZpZGVkLFxyXG4gICAqICAgICAgICBpbnN0ZWFkIG9mIGEgbm9ybWFsIHRleHQgaW5wdXQsIGl0IHdpbGwgYmUgYSBzZWxlY3QuIHNlZSBodG1sIGZvciBtb3JlIGluZm8uXHJcbiAgICogXHJcbiAgICogKipzYW1wbGUgdXNhZ2UqKlxyXG4gICAqIGdpdmVuIHRoZSBmb2xsb3dpbmcgbW9kZWxzOiBcclxuICAgKiAgUEVSU09OIHtcclxuICAgKiAgICBfaWRcclxuICAgKiAgICBuYW1lOiAnc3RyaW5nJyxcclxuICAgKiAgICBwZXQ6ICc6QU5JTUFMJ1xyXG4gICAqICB9XHJcbiAgICogXHJcbiAgICogIEFOSU1BTCB7XHJcbiAgICogICAgX2lkXHJcbiAgICogICAgdHlwZTogJ3N0cmluZydcclxuICAgKiAgfVxyXG4gICAqIFxyXG4gICAqICBpZiBvbmUgd2lzaCB0byBjcmVhdGUgYSBmb3JtIGZvciBcclxuICAgKiAgb2JqZWN0IFBFUlNPTiB3aGVyZSB0aGUgb3B0aW9ucyBmb3IgQU5JTUFMIFxyXG4gICAqICBjb21lcyBmcm9tIERCLCB0aGF0IGRldiBjYW4gY3JlYXRlIGEgcmVmT2JqczpLZXBzUmVmZXJlbmNlXHJcbiAgICogIHdoaWNoICB3aWxsIGxvb2sgbGlrZTpcclxuICAgKiAgXHJcbiAgICogIHJlZk9ianMgPSB7XHJcbiAgICogICAgICAgICAgICAgICdwZXQnOiBbXHJcbiAgICogICAgICAgICAgICAgICAgPEFOSU1BTD57XHJcbiAgICogICAgICAgICAgICAgICAgICAgIF9pZFxyXG4gICAqICAgICAgICAgICAgICAgICAgICB0eXBlXHJcbiAgICogICAgICAgICAgICAgICAgICB9LFxyXG4gICAqICAgICAgICAgICAgICAgIDxBTklNQUw+e1xyXG4gICAqICAgICAgICAgICAgICAgICAgICBfaWRcclxuICAgKiAgICAgICAgICAgICAgICAgICAgdHlwZVxyXG4gICAqICAgICAgICAgICAgICAgIH0sXHJcbiAgICogICAgICAgICAgICAgICAgLi4uXHJcbiAgICogICAgICAgICAgICAgICAgLi4uXHJcbiAgICogICAgICAgICAgICAgICAgLi5cclxuICAgKiAgICAgICAgICAgICAgXSxcclxuICAgKiAgICAgICAgICAgIH1cclxuICAgKiAgXHJcbiAgICogIFNJREVOT1RFOiBUaGUgYW5ub3RhdGlvbiA8VFlQRT4gaXMgdXNlZCB0byBtYWtlIHRoaXMgZXhtYXBsZSBtb3JlIGV4cGxpY2l0LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHJlZk9ianM6IEtlcHNSZWZlcmVuY2UgPSB7fTtcclxuXHJcbiAgLyoqXHJcbiAgICogQXBwZWFyYW5jZSBzdHlsZSBvZiB0aGUgZm9ybS5cclxuICAgKi9cclxuICBASW5wdXQoKSBhcHBlYXJhbmNlOiBNYXRGb3JtRmllbGRBcHBlYXJhbmNlID0gJ3N0YW5kYXJkJztcclxuXHJcbiAgLyoqXHJcbiAgICogQW5ndWxhciBNYXRlcmlhbCBjb2xvciBvZiB0aGUgZm9ybSBmaWVsZCB1bmRlcmxpbmUgYW5kIGZsb2F0aW5nIGxhYmVsLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGNvbG9yOiBUaGVtZVBhbGV0dGUgPSAncHJpbWFyeSc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEZpZWxkcyB0byBiZSBkaXNwbGF5ZWQgb24gdGhlIGZvcm0uXHJcbiAgICovXHJcbiAgQElucHV0KCkgZGlzcGxheWVkRmllbGRzOiBzdHJpbmdbXSA9IFtdO1xyXG5cclxuICAvKipcclxuICAgKiBGaWVsZHMgdG8gYmUgaGlkZGVuIG9uIHRoZSBmb3JtLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGhpZGRlbkZpZWxkczogc3RyaW5nW10gPSBbXTtcclxuXHJcbiAgLyoqXHJcbiAgICogUGxhY2Vob2xkZXJzIHRvIGJlIGRpc3BsYXllZCBvbiBmaWVsZHMuXHJcbiAgICovXHJcbiAgQElucHV0KCkgcGxhY2Vob2xkZXJzOiBSZWNvcmQ8c3RyaW5nLCBhbnk+ID0ge307XHJcblxyXG4gIC8qKlxyXG4gICAqIEhpbnRzIGZvciBmaWVsZHMgaW5wdXQuXHJcbiAgICovXHJcbiAgQElucHV0KCkgaGludHM6IFJlY29yZDxzdHJpbmcsIHN0cmluZz4gPSB7fTtcclxuXHJcbiAgLyoqXHJcbiAgICogRmllbGRzIHRvIGJlIGRpc3BsYXllZCBvbiB0aGUgZm9ybSBiYXNlZCBvbiBkaXNwbGF5ZWRGaWVsZHMgYW5kIGhpZGRlbkZpZWxkcy5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzY2hlbWFGaWVsZHM6IHN0cmluZ1tdO1xyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBmb3JtIGhhcyBkb25lIGluaXRpYWxpemluZyBvciBub3QuXHJcbiAgICovXHJcbiAgaW5pdGlhbGl6ZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBASW5qZWN0KCdNT0RFTFMnKSBwcml2YXRlIG1vZGVsczogS2Vwc01vZGVsc1xyXG4gICkgeyB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEluaXRpYWxpemVzIHRoZSBmb3JtIHdpdGggZmllbGRzIGJhc2VkIG9uIHRoZSBzY2hlbWEuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICAvLyBHZXQgc2NoZW1hIGZyb20gbW9kZWwgbmFtZSBpZiBzdXBwbGllZC5cclxuICAgIGlmICh0aGlzLm1vZGVsICYmIHRoaXMubW9kZWxzW3RoaXMubW9kZWxdKSB7XHJcbiAgICAgIHRoaXMuc2NoZW1hID0gdGhpcy5tb2RlbHNbdGhpcy5tb2RlbF0uc2NoZW1hO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLm1vZGVsICYmICF0aGlzLm1vZGVsc1t0aGlzLm1vZGVsXSkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoYENhbid0IGZpbmQgYSBtb2RlbCBuYW1lZCAke3RoaXMubW9kZWx9IWApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFzc2VydCBzY2hlbWEgbXVzdCBleGlzdC5cclxuICAgIGlmICghdGhpcy5zY2hlbWEpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKGBNdXN0IHN1cHBseSB2YWxpZCBtb2RlbCBuYW1lIG9yIHNjaGVtYSB0byBLZXBzIGZvcm0hYCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU2V0IHNjaGVtYSBmaWVsZHMuXHJcbiAgICBpZiAodGhpcy5kaXNwbGF5ZWRGaWVsZHMubGVuZ3RoID4gMCkge1xyXG4gICAgICB0aGlzLnNjaGVtYUZpZWxkcyA9IHRoaXMuZGlzcGxheWVkRmllbGRzO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zY2hlbWFGaWVsZHMgPSBPYmplY3Qua2V5cyh0aGlzLnNjaGVtYSlcclxuICAgICAgLmZpbHRlcihmaWVsZCA9PiBmaWVsZFswXSAhPT0gJ18nICYmICF0aGlzLmhpZGRlbkZpZWxkcy5pbmNsdWRlcyhmaWVsZCkpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5mb3JtLnNldFNjaGVtYSh0aGlzLnNjaGVtYSk7XHJcblxyXG4gICAgZm9yIChjb25zdCBmaWVsZE5hbWUgb2YgdGhpcy5zY2hlbWFGaWVsZHMpIHtcclxuICAgICAgY29uc3QgZmllbGQgPSB0aGlzLnNjaGVtYVtmaWVsZE5hbWVdO1xyXG5cclxuICAgICAgLy8gQ3JlYXRlIHZhbGlkYXRvcnMgaWYgbmVlZGVkLlxyXG4gICAgICBjb25zdCB2YWxpZGF0b3JzID0gW107XHJcbiAgICAgIGlmICghZmllbGQub3B0aW9uYWwpIHtcclxuICAgICAgICB2YWxpZGF0b3JzLnB1c2goVmFsaWRhdG9ycy5yZXF1aXJlZCk7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGZpZWxkLnR5cGUgPT09ICdudW1iZXInKSB7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGZpZWxkLnR5cGUgPT09ICdlbWFpbCcpIHtcclxuICAgICAgICB2YWxpZGF0b3JzLnB1c2goVmFsaWRhdG9ycy5lbWFpbCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIENyZWF0ZSB0aGUgYXBwcm9wcmlhdGUgZm9ybSBjb250cm9sIGZvciBlYWNoIGZpZWxkLlxyXG4gICAgICBpZiAoZmllbGQudHlwZSA9PT0gJ2FkZHJlc3MnKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtLmFkZENvbnRyb2woZmllbGROYW1lLCBuZXcgRm9ybUdyb3VwKHt9LCB2YWxpZGF0b3JzKSk7XHJcbiAgICAgIH0gZWxzZSBpZiAoZmllbGQudHlwZSA9PT0gJ2FycmF5Jykge1xyXG4gICAgICAgIHRoaXMuZm9ybS5hZGRDb250cm9sKGZpZWxkTmFtZSwgbmV3IEtlcHNGb3JtQXJyYXkoW10sIHZhbGlkYXRvcnMpKTtcclxuICAgICAgfSBlbHNlIGlmIChmaWVsZC50eXBlID09PSAnZGF0ZXRpbWUnKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtLmFkZENvbnRyb2woZmllbGROYW1lLCBuZXcgRm9ybUNvbnRyb2wobnVsbCwgdmFsaWRhdG9ycykpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZm9ybS5hZGRDb250cm9sKFxyXG4gICAgICAgICAgZmllbGROYW1lLFxyXG4gICAgICAgICAgbmV3IEZvcm1Db250cm9sKFxyXG4gICAgICAgICAgICBmaWVsZC5kZWZhdWx0IHx8IG51bGwsXHJcbiAgICAgICAgICAgIHZhbGlkYXRvcnNcclxuICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICB0aGlzLmZvcm0uaW5pdGlhbGl6ZSgpO1xyXG4gICAgdGhpcy5pbml0aWFsaXplZCA9IHRydWU7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBDaGVja3MgdGhlIHR5cGUgb2YgYSBmaWVsZC5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKiBAcGFyYW0gZmllbGQgRmllbGQgdG8gYmUgY2hlY2tlZC5cclxuICAgKiBAcGFyYW0gdHlwZSBUeXBlIHRvIGJlIGNoZWNrZWQuXHJcbiAgICovXHJcbiAgaXNUeXBlKGZpZWxkOiBzdHJpbmcsIHR5cGU6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIGlzVHlwZSh0aGlzLnNjaGVtYSwgZmllbGQsIHR5cGUpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUmV0dXJucyB0aGUgZGlzcGxheSBleHByZXNzaW9uIG9mIGEgcmVmZXJlbmNlIGZpZWxkLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqIEBwYXJhbSBmaWVsZCBGaWVsZCB0byBnZXQgZnJvbS5cclxuICAgKi9cclxuICBnZXRSZWZlcmVuY2VEaXNwbGF5KGZpZWxkOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBnZXRSZWZlcmVuY2VEaXNwbGF5KHRoaXMubW9kZWxzLCB0aGlzLnNjaGVtYSwgZmllbGQpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ2hlY2sgaWYgTWF0Rm9ybUZpZWxkIHNob3VsZCBoaWRlIHRoZSB1bmRlcmxpbmUgb3Igbm90LlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqIEBwYXJhbSBmaWVsZCBGaWVsZCB0byBiZSBjaGVja2VkLlxyXG4gICAqL1xyXG4gIGlzTm90VW5kZXJsaW5lZChmaWVsZDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gdGhpcy5zY2hlbWFbZmllbGRdLnR5cGUgPT09ICdib29sZWFuJyB8fFxyXG4gICAgdGhpcy5zY2hlbWFbZmllbGRdLnR5cGUgPT09ICdhZGRyZXNzJyB8fFxyXG4gICAgdGhpcy5zY2hlbWFbZmllbGRdLnR5cGUgPT09ICdhcnJheSc7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAqIENoZWNrIGlmIGEgZmllbGQgaGFzIGEgcmVmZXJlbmNlIGluIHJlZk9ianMuXHJcbiAgKiBpZiBoYXMgcmVmZXJlbmNlIGluIHJlZm9ianMsIHVzZSBtYXQtc2VsZWN0IGluc3RlYWQgb2YgaW5wdXQuXHJcbiAgKiBzdXBwb3J0cyBvbmx5IHR5cGUgc3RyaW5nIGF0IHRoZSBtb21lbnQuXHJcbiAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAqIEBwYXJhbSBmaWVsZCBGaWVsZCB0byBiZSBjaGVja2VkLlxyXG4gICovXHJcbiAgaGFzUmVmZXJlbmNlKGZpZWxkOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiB0aGlzLnNjaGVtYVtmaWVsZF0udHlwZSA9PT0gJ3N0cmluZycgJiYgXHJcbiAgICB0aGlzLnJlZk9ianMgJiYgXHJcbiAgICB0aGlzLnJlZk9ianNbZmllbGRdO1xyXG4gIH1cclxuXHJcbn1cclxuIl19