/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject, ElementRef, Input, Optional, Self } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';
import { KepsFormArray } from '../keps-form-array';
import { Subject, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
var FieldArrayComponent = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function FieldArrayComponent(fm, elRef, models, ngControl) {
        var _this = this;
        this.fm = fm;
        this.elRef = elRef;
        this.models = models;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-array';
        /**
         * \@internal Do not use!
         */
        this.id = "keps-field-array-" + FieldArrayComponent.nextId++;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new KepsFormArray([]);
        /**
         * Subschema of the form array.
         */
        this.subSchema = 'string';
        /**
         * Objects to be shown for reference field.
         */
        this.refObjs = [];
        /**
         * Appearance style of the form.
         */
        this.appearance = 'standard';
        /**
         * Angular Material color of the form field underline and floating label.
         */
        this.color = 'primary';
        /**
         * A behavior subject that emits a number which is the length of the form array.
         *
         * It should emit whenever a form control is added/removed from the array.
         * \@internal Do not use!
         */
        this.counter$ = new BehaviorSubject(0);
        /**
         * An observable that emits an empty array with the same length as the form array.
         *
         * It is used for *ngFor in this component's HTML.
         * \@internal Do not use!
         */
        this.counterArr$ = this.counter$.pipe(map((/**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            return new Array(value);
        })));
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        function (origin) {
            _this.focused = !!origin;
            _this.stateChanges.next();
        }));
    }
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    FieldArrayComponent.prototype.ngOnInit = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        this.form.setCounter$(this.counter$);
    };
    /**
     * Adds a form control to the form array.
     */
    /**
     * Adds a form control to the form array.
     * @return {?}
     */
    FieldArrayComponent.prototype.add = /**
     * Adds a form control to the form array.
     * @return {?}
     */
    function () {
        this.form.push(new FormControl(''));
        this.counter$.next(this.form.length);
    };
    /**
     * Removes a form control from the form array.
     * @param index Index of the form control to be removed.
     */
    /**
     * Removes a form control from the form array.
     * @param {?} index Index of the form control to be removed.
     * @return {?}
     */
    FieldArrayComponent.prototype.remove = /**
     * Removes a form control from the form array.
     * @param {?} index Index of the form control to be removed.
     * @return {?}
     */
    function (index) {
        this.form.removeAt(index);
        this.focused = false;
        this.counter$.next(this.form.length);
    };
    /**
     * Rearranges form controls inside the form array.
     * @internal Do not use!
     * @param event The drag event containing the form control.
     */
    /**
     * Rearranges form controls inside the form array.
     * \@internal Do not use!
     * @param {?} event The drag event containing the form control.
     * @return {?}
     */
    FieldArrayComponent.prototype.drop = /**
     * Rearranges form controls inside the form array.
     * \@internal Do not use!
     * @param {?} event The drag event containing the form control.
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var prevIndex = event.previousIndex;
        /** @type {?} */
        var nextIndex = event.currentIndex;
        /** @type {?} */
        var movedForm = this.form.at(prevIndex);
        this.form.removeAt(prevIndex);
        this.form.insert(nextIndex, movedForm);
    };
    /**
     * Returns the display expression of a reference field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @return {?}
     */
    FieldArrayComponent.prototype.getReferenceDisplay = /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        /** @type {?} */
        var referenceTo = this.subSchema.slice(1);
        if (this.models && this.models[referenceTo]) {
            return this.models[referenceTo].properties.displayExpression;
        }
        return null;
    };
    Object.defineProperty(FieldArrayComponent.prototype, "value", {
        /**
         * The value of the control.
         */
        get: /**
         * The value of the control.
         * @return {?}
         */
        function () {
            return this.form.value;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} v
         * @return {?}
         */
        function (v) {
            this.form.patchValue(v);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldArrayComponent.prototype, "required", {
        /**
         * Whether the control is required.
         */
        get: /**
         * Whether the control is required.
         * @return {?}
         */
        function () {
            return this._required;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._required = coerceBooleanProperty(value);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldArrayComponent.prototype, "disabled", {
        /**
         * Whether the control is disabled.
         */
        get: /**
         * Whether the control is disabled.
         * @return {?}
         */
        function () {
            return this._disabled;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._disabled = coerceBooleanProperty(value);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldArrayComponent.prototype, "placeholder", {
        /**
         * The placeholder for this control.
         */
        get: /**
         * The placeholder for this control.
         * @return {?}
         */
        function () {
            return this._placeholder;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._placeholder = value;
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldArrayComponent.prototype, "empty", {
        /**
         * Whether the control is empty.
         */
        get: /**
         * Whether the control is empty.
         * @return {?}
         */
        function () {
            return !this.form.value || this.form.value.length === 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldArrayComponent.prototype, "shouldLabelFloat", {
        /**
         * Whether the MatFormField label should try to float.
         */
        get: /**
         * Whether the MatFormField label should try to float.
         * @return {?}
         */
        function () {
            return this.focused || !this.empty;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    FieldArrayComponent.prototype.setDescribedByIds = /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    function (ids) {
        this.describedBy = ids.join(' ');
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    FieldArrayComponent.prototype.onContainerClick = /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    FieldArrayComponent.prototype.ngOnDestroy = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    };
    /**
     * \@internal Do not use!
     */
    FieldArrayComponent.nextId = 0;
    FieldArrayComponent.decorators = [
        { type: Component, args: [{
                    selector: 'keps-field-array',
                    template: "<div cdkDropList (cdkDropListDropped)=\"drop($event)\">\n  <div *ngFor=\"let c of counterArr$ | async; let i = index\" class=\"field-array-container\" cdkDrag>\n    <button cdkDragHandle mat-icon-button color=\"accent\" class=\"field-array-reorder\">\n      <mat-icon>reorder</mat-icon>\n    </button>\n    <mat-form-field\n      [appearance]=\"appearance\"\n      [color]=\"color\"\n      floatLabel=\"always\"\n      class=\"field-array\"\n    >\n      <!-- Type: String -->\n      <input\n        *ngIf=\"subSchema === 'string'\"\n        [formControl]=\"form.at(i)\"\n        [required]=\"required\"\n        type=\"text\"\n        matInput\n      >\n      <!-- Type: number -->\n      <input\n        *ngIf=\"subSchema === 'number'\"\n        [formControl]=\"form.at(i)\"\n        [required]=\"required\"\n        type=\"number\"\n        matInput\n      >\n      <!-- Type: Reference -->\n      <mat-select\n        *ngIf=\"subSchema[0] === ':'\"\n        [formControl]=\"form.at(i)\"\n        [required]=\"required\"\n      >\n        <mat-option\n          *ngFor=\"let option of refObjs\"\n          [value]=\"option._id\"\n        >\n          {{ option | eval:getReferenceDisplay() }}\n        </mat-option>\n      </mat-select>\n\n      <!-- TODO: If subschema is another schema object, we should make something here. -->\n    </mat-form-field>\n    <button (click)=\"remove(i)\" mat-icon-button color=\"warn\" class=\"field-array-delete\">\n      <mat-icon>delete</mat-icon>\n    </button>\n  </div>\n</div>\n\n<button (click)=\"add()\" mat-icon-button color=\"accent\">\n  <mat-icon>add</mat-icon>\n</button>\n  ",
                    providers: [
                        {
                            provide: MatFormFieldControl,
                            useExisting: FieldArrayComponent
                        }
                    ],
                    styles: [".field-array-container{width:100%;display:flex;flex-direction:row;justify-content:center}.field-array-reorder{max-width:30px}.field-array-delete{max-width:20px}.field-array-container::ng-deep mat-form-field{flex:1 1 auto;margin-top:5px}.field-array::ng-deep .mat-form-field-underline{display:block!important}.field-array::ng-deep .mat-form-field-flex{padding-top:0!important}.field-array::ng-deep .mat-form-field-infix{border-top:0!important;width:100%}"]
                }] }
    ];
    /** @nocollapse */
    FieldArrayComponent.ctorParameters = function () { return [
        { type: FocusMonitor },
        { type: ElementRef },
        { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] },
        { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
    ]; };
    FieldArrayComponent.propDecorators = {
        form: [{ type: Input }],
        subSchema: [{ type: Input }],
        refObjs: [{ type: Input }],
        appearance: [{ type: Input }],
        color: [{ type: Input }],
        value: [{ type: Input }],
        required: [{ type: Input }],
        disabled: [{ type: Input }],
        placeholder: [{ type: Input }]
    };
    return FieldArrayComponent;
}());
export { FieldArrayComponent };
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.nextId;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype._required;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype._disabled;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype._placeholder;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.stateChanges;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.focused;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.errorState;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.controlType;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.id;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.describedBy;
    /**
     * Form control of the field.
     * @type {?}
     */
    FieldArrayComponent.prototype.form;
    /**
     * Subschema of the form array.
     * @type {?}
     */
    FieldArrayComponent.prototype.subSchema;
    /**
     * Objects to be shown for reference field.
     * @type {?}
     */
    FieldArrayComponent.prototype.refObjs;
    /**
     * Appearance style of the form.
     * @type {?}
     */
    FieldArrayComponent.prototype.appearance;
    /**
     * Angular Material color of the form field underline and floating label.
     * @type {?}
     */
    FieldArrayComponent.prototype.color;
    /**
     * A behavior subject that emits a number which is the length of the form array.
     *
     * It should emit whenever a form control is added/removed from the array.
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.counter$;
    /**
     * An observable that emits an empty array with the same length as the form array.
     *
     * It is used for *ngFor in this component's HTML.
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.counterArr$;
    /**
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype.fm;
    /**
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype.elRef;
    /**
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype.models;
    /** @type {?} */
    FieldArrayComponent.prototype.ngControl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtYXJyYXkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvZm9ybS9maWVsZC1hcnJheS9maWVsZC1hcnJheS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBR1QsTUFBTSxFQUNOLFVBQVUsRUFDVixLQUFLLEVBQ0wsUUFBUSxFQUNSLElBQUksRUFDTCxNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDakQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsbUJBQW1CLEVBQXdDLE1BQU0sbUJBQW1CLENBQUM7QUFFOUYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRW5ELE9BQU8sRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2hELE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVyQztJQTJHRTs7T0FFRztJQUNILDZCQUNVLEVBQWdCLEVBQ2hCLEtBQThCLEVBQ1osTUFBa0IsRUFDakIsU0FBb0I7UUFKakQsaUJBVUM7UUFUUyxPQUFFLEdBQUYsRUFBRSxDQUFjO1FBQ2hCLFVBQUssR0FBTCxLQUFLLENBQXlCO1FBQ1osV0FBTSxHQUFOLE1BQU0sQ0FBWTtRQUNqQixjQUFTLEdBQVQsU0FBUyxDQUFXOzs7O1FBOUZ6QyxjQUFTLEdBQUcsS0FBSyxDQUFDOzs7O1FBS2xCLGNBQVMsR0FBRyxLQUFLLENBQUM7Ozs7UUFVMUIsaUJBQVksR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDOzs7O1FBS25DLFlBQU8sR0FBRyxLQUFLLENBQUM7Ozs7UUFLaEIsZUFBVSxHQUFHLEtBQUssQ0FBQzs7OztRQUtuQixnQkFBVyxHQUFHLGtCQUFrQixDQUFDOzs7O1FBS2pDLE9BQUUsR0FBRyxzQkFBb0IsbUJBQW1CLENBQUMsTUFBTSxFQUFJLENBQUM7Ozs7UUFLeEQsZ0JBQVcsR0FBRyxFQUFFLENBQUM7Ozs7UUFLUixTQUFJLEdBQUcsSUFBSSxhQUFhLENBQUMsRUFBRSxDQUFDLENBQUM7Ozs7UUFLN0IsY0FBUyxHQUFHLFFBQVEsQ0FBQzs7OztRQUtyQixZQUFPLEdBQVUsRUFBRSxDQUFDOzs7O1FBS3BCLGVBQVUsR0FBMkIsVUFBVSxDQUFDOzs7O1FBS2hELFVBQUssR0FBaUIsU0FBUyxDQUFDOzs7Ozs7O1FBUXpDLGFBQVEsR0FBRyxJQUFJLGVBQWUsQ0FBUyxDQUFDLENBQUMsQ0FBQzs7Ozs7OztRQVExQyxnQkFBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUM5QixHQUFHOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ1AsT0FBTyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixDQUFDLEVBQUMsQ0FDSCxDQUFDO1FBV0EsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTTtZQUN0QyxLQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDeEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCxzQ0FBUTs7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCxpQ0FBRzs7OztJQUFIO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRDs7O09BR0c7Ozs7OztJQUNILG9DQUFNOzs7OztJQUFOLFVBQU8sS0FBYTtRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsa0NBQUk7Ozs7OztJQUFKLFVBQUssS0FBaUM7O1lBQzlCLFNBQVMsR0FBRyxLQUFLLENBQUMsYUFBYTs7WUFDL0IsU0FBUyxHQUFHLEtBQUssQ0FBQyxZQUFZOztZQUM5QixTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsaURBQW1COzs7OztJQUFuQjs7WUFDUSxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQzNDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQzNDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUM7U0FDOUQ7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFLRCxzQkFDSSxzQ0FBSztRQUpUOztXQUVHOzs7OztRQUNIO1lBRUUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN6QixDQUFDO1FBRUQ7O1dBRUc7Ozs7OztRQUNILFVBQVUsQ0FBZTtZQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLENBQUM7OztPQVJBO0lBYUQsc0JBQ0kseUNBQVE7UUFKWjs7V0FFRzs7Ozs7UUFDSDtZQUVFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN4QixDQUFDO1FBRUQ7O1dBRUc7Ozs7OztRQUNILFVBQWEsS0FBYztZQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsQ0FBQzs7O09BUkE7SUFhRCxzQkFDSSx5Q0FBUTtRQUpaOztXQUVHOzs7OztRQUNIO1lBRUUsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3hCLENBQUM7UUFFRDs7V0FFRzs7Ozs7O1FBQ0gsVUFBYSxLQUFjO1lBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDOzs7T0FSQTtJQWFELHNCQUNJLDRDQUFXO1FBSmY7O1dBRUc7Ozs7O1FBQ0g7WUFFRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDM0IsQ0FBQztRQUVEOztXQUVHOzs7Ozs7UUFDSCxVQUFnQixLQUFhO1lBQzNCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsQ0FBQzs7O09BUkE7SUFhRCxzQkFBSSxzQ0FBSztRQUhUOztXQUVHOzs7OztRQUNIO1lBQ0UsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7UUFDMUQsQ0FBQzs7O09BQUE7SUFLRCxzQkFBSSxpREFBZ0I7UUFIcEI7O1dBRUc7Ozs7O1FBQ0g7WUFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3JDLENBQUM7OztPQUFBO0lBRUQ7O09BRUc7Ozs7OztJQUNILCtDQUFpQjs7Ozs7SUFBakIsVUFBa0IsR0FBYTtRQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVEOztPQUVHOzs7Ozs7SUFDSCw4Q0FBZ0I7Ozs7O0lBQWhCLFVBQWlCLEtBQWlCO1FBQ2hDLElBQUksQ0FBQyxtQkFBQSxLQUFLLENBQUMsTUFBTSxFQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEtBQUssT0FBTyxFQUFFO1lBQy9ELElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN6RDtJQUNILENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCx5Q0FBVzs7OztJQUFYO1FBQ0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQzs7OztJQWxRTSwwQkFBTSxHQUFHLENBQUMsQ0FBQzs7Z0JBZm5CLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixzbURBQTJDO29CQUUzQyxTQUFTLEVBQUU7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLG1CQUFtQjs0QkFDNUIsV0FBVyxFQUFFLG1CQUFtQjt5QkFDakM7cUJBQ0Y7O2lCQUNGOzs7O2dCQXBCUSxZQUFZO2dCQUxuQixVQUFVO2dEQWdJUCxNQUFNLFNBQUMsUUFBUTtnQkF6SEUsU0FBUyx1QkEwSDFCLFFBQVEsWUFBSSxJQUFJOzs7dUJBakRsQixLQUFLOzRCQUtMLEtBQUs7MEJBS0wsS0FBSzs2QkFLTCxLQUFLO3dCQUtMLEtBQUs7d0JBMkZMLEtBQUs7MkJBZ0JMLEtBQUs7MkJBZ0JMLEtBQUs7OEJBZ0JMLEtBQUs7O0lBa0RSLDBCQUFDO0NBQUEsQUFsUkQsSUFrUkM7U0F2UVksbUJBQW1COzs7Ozs7SUFJOUIsMkJBQWtCOzs7Ozs7SUFLbEIsd0NBQTBCOzs7Ozs7SUFLMUIsd0NBQTBCOzs7Ozs7SUFLMUIsMkNBQTZCOzs7OztJQUs3QiwyQ0FBbUM7Ozs7O0lBS25DLHNDQUFnQjs7Ozs7SUFLaEIseUNBQW1COzs7OztJQUtuQiwwQ0FBaUM7Ozs7O0lBS2pDLGlDQUF3RDs7Ozs7SUFLeEQsMENBQWlCOzs7OztJQUtqQixtQ0FBc0M7Ozs7O0lBS3RDLHdDQUE4Qjs7Ozs7SUFLOUIsc0NBQTZCOzs7OztJQUs3Qix5Q0FBeUQ7Ozs7O0lBS3pELG9DQUF5Qzs7Ozs7Ozs7SUFRekMsdUNBQTBDOzs7Ozs7OztJQVExQywwQ0FJRTs7Ozs7SUFNQSxpQ0FBd0I7Ozs7O0lBQ3hCLG9DQUFzQzs7Ozs7SUFDdEMscUNBQTRDOztJQUM1Qyx3Q0FBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkluaXQsXHJcbiAgT25EZXN0cm95LFxyXG4gIEluamVjdCxcclxuICBFbGVtZW50UmVmLFxyXG4gIElucHV0LFxyXG4gIE9wdGlvbmFsLFxyXG4gIFNlbGZcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9jdXNNb25pdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2ExMXknO1xyXG5pbXBvcnQgeyBjb2VyY2VCb29sZWFuUHJvcGVydHkgfSBmcm9tICdAYW5ndWxhci9jZGsvY29lcmNpb24nO1xyXG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgTmdDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBNYXRGb3JtRmllbGRDb250cm9sLCBNYXRGb3JtRmllbGRBcHBlYXJhbmNlLCBUaGVtZVBhbGV0dGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IENka0RyYWdEcm9wIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2RyYWctZHJvcCc7XHJcbmltcG9ydCB7IEtlcHNGb3JtQXJyYXkgfSBmcm9tICcuLi9rZXBzLWZvcm0tYXJyYXknO1xyXG5pbXBvcnQgeyBLZXBzTW9kZWxzIH0gZnJvbSAnLi4vLi4vLi4vaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgU3ViamVjdCwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy1maWVsZC1hcnJheScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZpZWxkLWFycmF5LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9maWVsZC1hcnJheS5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBNYXRGb3JtRmllbGRDb250cm9sLFxyXG4gICAgICB1c2VFeGlzdGluZzogRmllbGRBcnJheUNvbXBvbmVudFxyXG4gICAgfVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZpZWxkQXJyYXlDb21wb25lbnQgaW1wbGVtZW50cyBNYXRGb3JtRmllbGRDb250cm9sPGFueVtdPiwgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHN0YXRpYyBuZXh0SWQgPSAwO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBwcml2YXRlIF9yZXF1aXJlZCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBwcml2YXRlIF9kaXNhYmxlZCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBwcml2YXRlIF9wbGFjZWhvbGRlcjogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzdGF0ZUNoYW5nZXMgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBmb2N1c2VkID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGVycm9yU3RhdGUgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29udHJvbFR5cGUgPSAna2Vwcy1maWVsZC1hcnJheSc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGlkID0gYGtlcHMtZmllbGQtYXJyYXktJHtGaWVsZEFycmF5Q29tcG9uZW50Lm5leHRJZCsrfWA7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGRlc2NyaWJlZEJ5ID0gJyc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEZvcm0gY29udHJvbCBvZiB0aGUgZmllbGQuXHJcbiAgICovXHJcbiAgQElucHV0KCkgZm9ybSA9IG5ldyBLZXBzRm9ybUFycmF5KFtdKTtcclxuXHJcbiAgLyoqXHJcbiAgICogU3Vic2NoZW1hIG9mIHRoZSBmb3JtIGFycmF5LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHN1YlNjaGVtYSA9ICdzdHJpbmcnO1xyXG5cclxuICAvKipcclxuICAgKiBPYmplY3RzIHRvIGJlIHNob3duIGZvciByZWZlcmVuY2UgZmllbGQuXHJcbiAgICovXHJcbiAgQElucHV0KCkgcmVmT2JqczogYW55W10gPSBbXTtcclxuXHJcbiAgLyoqXHJcbiAgICogQXBwZWFyYW5jZSBzdHlsZSBvZiB0aGUgZm9ybS5cclxuICAgKi9cclxuICBASW5wdXQoKSBhcHBlYXJhbmNlOiBNYXRGb3JtRmllbGRBcHBlYXJhbmNlID0gJ3N0YW5kYXJkJztcclxuXHJcbiAgLyoqXHJcbiAgICogQW5ndWxhciBNYXRlcmlhbCBjb2xvciBvZiB0aGUgZm9ybSBmaWVsZCB1bmRlcmxpbmUgYW5kIGZsb2F0aW5nIGxhYmVsLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGNvbG9yOiBUaGVtZVBhbGV0dGUgPSAncHJpbWFyeSc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEEgYmVoYXZpb3Igc3ViamVjdCB0aGF0IGVtaXRzIGEgbnVtYmVyIHdoaWNoIGlzIHRoZSBsZW5ndGggb2YgdGhlIGZvcm0gYXJyYXkuXHJcbiAgICpcclxuICAgKiBJdCBzaG91bGQgZW1pdCB3aGVuZXZlciBhIGZvcm0gY29udHJvbCBpcyBhZGRlZC9yZW1vdmVkIGZyb20gdGhlIGFycmF5LlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvdW50ZXIkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxudW1iZXI+KDApO1xyXG5cclxuICAvKipcclxuICAgKiBBbiBvYnNlcnZhYmxlIHRoYXQgZW1pdHMgYW4gZW1wdHkgYXJyYXkgd2l0aCB0aGUgc2FtZSBsZW5ndGggYXMgdGhlIGZvcm0gYXJyYXkuXHJcbiAgICpcclxuICAgKiBJdCBpcyB1c2VkIGZvciAqbmdGb3IgaW4gdGhpcyBjb21wb25lbnQncyBIVE1MLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvdW50ZXJBcnIkID0gdGhpcy5jb3VudGVyJC5waXBlKFxyXG4gICAgbWFwKHZhbHVlID0+IHtcclxuICAgICAgcmV0dXJuIG5ldyBBcnJheSh2YWx1ZSk7XHJcbiAgICB9KVxyXG4gICk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmbTogRm9jdXNNb25pdG9yLFxyXG4gICAgcHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4sXHJcbiAgICBASW5qZWN0KCdNT0RFTFMnKSBwcml2YXRlIG1vZGVsczogS2Vwc01vZGVscyxcclxuICAgIEBPcHRpb25hbCgpIEBTZWxmKCkgcHVibGljIG5nQ29udHJvbDogTmdDb250cm9sXHJcbiAgKSB7XHJcbiAgICBmbS5tb25pdG9yKGVsUmVmLCB0cnVlKS5zdWJzY3JpYmUob3JpZ2luID0+IHtcclxuICAgICAgdGhpcy5mb2N1c2VkID0gISFvcmlnaW47XHJcbiAgICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmZvcm0uc2V0Q291bnRlciQodGhpcy5jb3VudGVyJCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBBZGRzIGEgZm9ybSBjb250cm9sIHRvIHRoZSBmb3JtIGFycmF5LlxyXG4gICAqL1xyXG4gIGFkZCgpIHtcclxuICAgIHRoaXMuZm9ybS5wdXNoKG5ldyBGb3JtQ29udHJvbCgnJykpO1xyXG4gICAgdGhpcy5jb3VudGVyJC5uZXh0KHRoaXMuZm9ybS5sZW5ndGgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUmVtb3ZlcyBhIGZvcm0gY29udHJvbCBmcm9tIHRoZSBmb3JtIGFycmF5LlxyXG4gICAqIEBwYXJhbSBpbmRleCBJbmRleCBvZiB0aGUgZm9ybSBjb250cm9sIHRvIGJlIHJlbW92ZWQuXHJcbiAgICovXHJcbiAgcmVtb3ZlKGluZGV4OiBudW1iZXIpIHtcclxuICAgIHRoaXMuZm9ybS5yZW1vdmVBdChpbmRleCk7XHJcbiAgICB0aGlzLmZvY3VzZWQgPSBmYWxzZTtcclxuICAgIHRoaXMuY291bnRlciQubmV4dCh0aGlzLmZvcm0ubGVuZ3RoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJlYXJyYW5nZXMgZm9ybSBjb250cm9scyBpbnNpZGUgdGhlIGZvcm0gYXJyYXkuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICogQHBhcmFtIGV2ZW50IFRoZSBkcmFnIGV2ZW50IGNvbnRhaW5pbmcgdGhlIGZvcm0gY29udHJvbC5cclxuICAgKi9cclxuICBkcm9wKGV2ZW50OiBDZGtEcmFnRHJvcDxGb3JtQ29udHJvbFtdPikge1xyXG4gICAgY29uc3QgcHJldkluZGV4ID0gZXZlbnQucHJldmlvdXNJbmRleDtcclxuICAgIGNvbnN0IG5leHRJbmRleCA9IGV2ZW50LmN1cnJlbnRJbmRleDtcclxuICAgIGNvbnN0IG1vdmVkRm9ybSA9IHRoaXMuZm9ybS5hdChwcmV2SW5kZXgpO1xyXG4gICAgdGhpcy5mb3JtLnJlbW92ZUF0KHByZXZJbmRleCk7XHJcbiAgICB0aGlzLmZvcm0uaW5zZXJ0KG5leHRJbmRleCwgbW92ZWRGb3JtKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJldHVybnMgdGhlIGRpc3BsYXkgZXhwcmVzc2lvbiBvZiBhIHJlZmVyZW5jZSBmaWVsZC5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKiBAcGFyYW0gZmllbGQgRmllbGQgdG8gZ2V0IGZyb20uXHJcbiAgICovXHJcbiAgZ2V0UmVmZXJlbmNlRGlzcGxheSgpIHtcclxuICAgIGNvbnN0IHJlZmVyZW5jZVRvID0gdGhpcy5zdWJTY2hlbWEuc2xpY2UoMSk7XHJcbiAgICBpZiAodGhpcy5tb2RlbHMgJiYgdGhpcy5tb2RlbHNbcmVmZXJlbmNlVG9dKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLm1vZGVsc1tyZWZlcmVuY2VUb10ucHJvcGVydGllcy5kaXNwbGF5RXhwcmVzc2lvbjtcclxuICAgIH1cclxuICAgIHJldHVybiBudWxsO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIHZhbHVlIG9mIHRoZSBjb250cm9sLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IHZhbHVlKCk6IGFueVtdIHwgbnVsbCB7XHJcbiAgICByZXR1cm4gdGhpcy5mb3JtLnZhbHVlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2V0IHZhbHVlKHY6IGFueVtdIHwgbnVsbCkge1xyXG4gICAgdGhpcy5mb3JtLnBhdGNoVmFsdWUodik7XHJcbiAgICB0aGlzLnN0YXRlQ2hhbmdlcy5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBjb250cm9sIGlzIHJlcXVpcmVkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IHJlcXVpcmVkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX3JlcXVpcmVkO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2V0IHJlcXVpcmVkKHZhbHVlOiBib29sZWFuKSB7XHJcbiAgICB0aGlzLl9yZXF1aXJlZCA9IGNvZXJjZUJvb2xlYW5Qcm9wZXJ0eSh2YWx1ZSk7XHJcbiAgICB0aGlzLnN0YXRlQ2hhbmdlcy5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBjb250cm9sIGlzIGRpc2FibGVkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IGRpc2FibGVkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2Rpc2FibGVkO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2V0IGRpc2FibGVkKHZhbHVlOiBib29sZWFuKSB7XHJcbiAgICB0aGlzLl9kaXNhYmxlZCA9IGNvZXJjZUJvb2xlYW5Qcm9wZXJ0eSh2YWx1ZSk7XHJcbiAgICB0aGlzLnN0YXRlQ2hhbmdlcy5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBUaGUgcGxhY2Vob2xkZXIgZm9yIHRoaXMgY29udHJvbC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCBwbGFjZWhvbGRlcigpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuIHRoaXMuX3BsYWNlaG9sZGVyO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2V0IHBsYWNlaG9sZGVyKHZhbHVlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuX3BsYWNlaG9sZGVyID0gdmFsdWU7XHJcbiAgICB0aGlzLnN0YXRlQ2hhbmdlcy5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBjb250cm9sIGlzIGVtcHR5LlxyXG4gICAqL1xyXG4gIGdldCBlbXB0eSgpIHtcclxuICAgIHJldHVybiAhdGhpcy5mb3JtLnZhbHVlIHx8IHRoaXMuZm9ybS52YWx1ZS5sZW5ndGggPT09IDA7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBNYXRGb3JtRmllbGQgbGFiZWwgc2hvdWxkIHRyeSB0byBmbG9hdC5cclxuICAgKi9cclxuICBnZXQgc2hvdWxkTGFiZWxGbG9hdCgpIHtcclxuICAgIHJldHVybiB0aGlzLmZvY3VzZWQgfHwgIXRoaXMuZW1wdHk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXREZXNjcmliZWRCeUlkcyhpZHM6IHN0cmluZ1tdKSB7XHJcbiAgICB0aGlzLmRlc2NyaWJlZEJ5ID0gaWRzLmpvaW4oJyAnKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG9uQ29udGFpbmVyQ2xpY2soZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgIGlmICgoZXZlbnQudGFyZ2V0IGFzIEVsZW1lbnQpLnRhZ05hbWUudG9Mb3dlckNhc2UoKSAhPT0gJ2lucHV0Jykge1xyXG4gICAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXQnKS5mb2N1cygpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnN0YXRlQ2hhbmdlcy5jb21wbGV0ZSgpO1xyXG4gICAgdGhpcy5mbS5zdG9wTW9uaXRvcmluZyh0aGlzLmVsUmVmKTtcclxuICB9XHJcbn1cclxuIl19