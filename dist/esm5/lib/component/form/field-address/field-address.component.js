/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, ElementRef, Optional, Self } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormGroup, FormControl, NgControl, Validators } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';
import { Subject } from 'rxjs';
var FieldAddressComponent = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function FieldAddressComponent(fm, elRef, ngControl) {
        var _this = this;
        this.fm = fm;
        this.elRef = elRef;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-address';
        /**
         * \@internal Do not use!
         */
        this.id = "keps-field-address-" + FieldAddressComponent.nextId++;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new FormGroup({});
        /**
         * Appearance style of the form.
         */
        this.appearance = 'standard';
        /**
         * Angular Material color of the form field underline and floating label.
         */
        this.color = 'primary';
        /**
         * Whether the field has been initialized or not.
         * \@internal Do not use!
         */
        this.initialized = false;
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        function (origin) {
            _this.focused = !!origin;
            _this.stateChanges.next();
        }));
    }
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    FieldAddressComponent.prototype.ngOnInit = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        var e_1, _a;
        /** @type {?} */
        var validators = [];
        if (this.required) {
            validators.push(Validators.required);
        }
        try {
            for (var _b = tslib_1.__values(['address1', 'address2', 'city', 'region', 'postal', 'country']), _c = _b.next(); !_c.done; _c = _b.next()) {
                var field = _c.value;
                this.form.addControl(field, new FormControl(null, validators));
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        this.initialized = true;
    };
    Object.defineProperty(FieldAddressComponent.prototype, "value", {
        /**
         * The value of the control.
         */
        get: /**
         * The value of the control.
         * @return {?}
         */
        function () {
            return this.form.value;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} v
         * @return {?}
         */
        function (v) {
            this.form.patchValue(v);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldAddressComponent.prototype, "required", {
        /**
         * Whether the control is required.
         */
        get: /**
         * Whether the control is required.
         * @return {?}
         */
        function () {
            return this._required;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._required = coerceBooleanProperty(value);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldAddressComponent.prototype, "disabled", {
        /**
         * Whether the control is disabled.
         */
        get: /**
         * Whether the control is disabled.
         * @return {?}
         */
        function () {
            return this._disabled;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._disabled = coerceBooleanProperty(value);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldAddressComponent.prototype, "placeholder", {
        /**
         * The placeholder for this control.
         */
        get: /**
         * The placeholder for this control.
         * @return {?}
         */
        function () {
            return this._placeholder;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._placeholder = value;
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldAddressComponent.prototype, "empty", {
        /**
         * Whether the control is empty.
         */
        get: /**
         * Whether the control is empty.
         * @return {?}
         */
        function () {
            var _a = this.form.value, address1 = _a.address1, address2 = _a.address2, city = _a.city, region = _a.region, postal = _a.postal, country = _a.country;
            return !address1 && !address2 && !city && !region && !postal && !country;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldAddressComponent.prototype, "shouldLabelFloat", {
        /**
         * Whether the MatFormField label should try to float.
         */
        get: /**
         * Whether the MatFormField label should try to float.
         * @return {?}
         */
        function () {
            return this.focused || !this.empty;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    FieldAddressComponent.prototype.setDescribedByIds = /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    function (ids) {
        this.describedBy = ids.join(' ');
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    FieldAddressComponent.prototype.onContainerClick = /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    FieldAddressComponent.prototype.ngOnDestroy = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    };
    /**
     * \@internal Do not use!
     */
    FieldAddressComponent.nextId = 0;
    FieldAddressComponent.decorators = [
        { type: Component, args: [{
                    selector: 'keps-field-address',
                    template: "<ng-container *ngIf=\"initialized\">\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Address 1\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('address1')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Address 2\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('address2')\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      City\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('city')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      State\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('region')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Zipcode\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('postal')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Country\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('country')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n</ng-container>\r\n",
                    providers: [
                        {
                            provide: MatFormFieldControl,
                            useExisting: FieldAddressComponent
                        }
                    ],
                    styles: [".field-address{width:100%}.field-address::ng-deep .mat-form-field-underline{display:block!important}"]
                }] }
    ];
    /** @nocollapse */
    FieldAddressComponent.ctorParameters = function () { return [
        { type: FocusMonitor },
        { type: ElementRef },
        { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
    ]; };
    FieldAddressComponent.propDecorators = {
        form: [{ type: Input }],
        appearance: [{ type: Input }],
        color: [{ type: Input }],
        value: [{ type: Input }],
        required: [{ type: Input }],
        disabled: [{ type: Input }],
        placeholder: [{ type: Input }]
    };
    return FieldAddressComponent;
}());
export { FieldAddressComponent };
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.nextId;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype._required;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype._disabled;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype._placeholder;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.stateChanges;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.focused;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.errorState;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.controlType;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.id;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.describedBy;
    /**
     * Form control of the field.
     * @type {?}
     */
    FieldAddressComponent.prototype.form;
    /**
     * Appearance style of the form.
     * @type {?}
     */
    FieldAddressComponent.prototype.appearance;
    /**
     * Angular Material color of the form field underline and floating label.
     * @type {?}
     */
    FieldAddressComponent.prototype.color;
    /**
     * Whether the field has been initialized or not.
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.initialized;
    /**
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype.fm;
    /**
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype.elRef;
    /** @type {?} */
    FieldAddressComponent.prototype.ngControl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtYWRkcmVzcy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudC9mb3JtL2ZpZWxkLWFkZHJlc3MvZmllbGQtYWRkcmVzcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUdULEtBQUssRUFDTCxVQUFVLEVBQ1YsUUFBUSxFQUNSLElBQUksRUFDTCxNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDakQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQy9FLE9BQU8sRUFBRSxtQkFBbUIsRUFBd0MsTUFBTSxtQkFBbUIsQ0FBQztBQUU5RixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRS9CO0lBbUZFOztPQUVHO0lBQ0gsK0JBQ1UsRUFBZ0IsRUFDaEIsS0FBaUIsRUFDRSxTQUFvQjtRQUhqRCxpQkFTQztRQVJTLE9BQUUsR0FBRixFQUFFLENBQWM7UUFDaEIsVUFBSyxHQUFMLEtBQUssQ0FBWTtRQUNFLGNBQVMsR0FBVCxTQUFTLENBQVc7Ozs7UUFyRXpDLGNBQVMsR0FBRyxLQUFLLENBQUM7Ozs7UUFLbEIsY0FBUyxHQUFHLEtBQUssQ0FBQzs7OztRQVUxQixpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7Ozs7UUFLbkMsWUFBTyxHQUFHLEtBQUssQ0FBQzs7OztRQUtoQixlQUFVLEdBQUcsS0FBSyxDQUFDOzs7O1FBS25CLGdCQUFXLEdBQUcsb0JBQW9CLENBQUM7Ozs7UUFLbkMsT0FBRSxHQUFHLHdCQUFzQixxQkFBcUIsQ0FBQyxNQUFNLEVBQUksQ0FBQzs7OztRQUs1RCxnQkFBVyxHQUFHLEVBQUUsQ0FBQzs7OztRQUtSLFNBQUksR0FBRyxJQUFJLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQzs7OztRQUt6QixlQUFVLEdBQTJCLFVBQVUsQ0FBQzs7OztRQUtoRCxVQUFLLEdBQWlCLFNBQVMsQ0FBQzs7Ozs7UUFNekMsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFVbEIsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTTtZQUN0QyxLQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDeEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCx3Q0FBUTs7OztJQUFSOzs7WUFDUSxVQUFVLEdBQUcsRUFBRTtRQUNyQixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDdEM7O1lBQ0QsS0FBb0IsSUFBQSxLQUFBLGlCQUFBLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQSxnQkFBQSw0QkFBRTtnQkFBaEYsSUFBTSxLQUFLLFdBQUE7Z0JBQ2QsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO2FBQ2hFOzs7Ozs7Ozs7UUFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBS0Qsc0JBQ0ksd0NBQUs7UUFKVDs7V0FFRzs7Ozs7UUFDSDtZQUVFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekIsQ0FBQztRQUVEOztXQUVHOzs7Ozs7UUFDSCxVQUFVLENBQXFCO1lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsQ0FBQzs7O09BUkE7SUFhRCxzQkFDSSwyQ0FBUTtRQUpaOztXQUVHOzs7OztRQUNIO1lBRUUsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3hCLENBQUM7UUFFRDs7V0FFRzs7Ozs7O1FBQ0gsVUFBYSxLQUFjO1lBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDOzs7T0FSQTtJQWFELHNCQUNJLDJDQUFRO1FBSlo7O1dBRUc7Ozs7O1FBQ0g7WUFFRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDeEIsQ0FBQztRQUVEOztXQUVHOzs7Ozs7UUFDSCxVQUFhLEtBQWM7WUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLENBQUM7OztPQVJBO0lBYUQsc0JBQ0ksOENBQVc7UUFKZjs7V0FFRzs7Ozs7UUFDSDtZQUVFLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQztRQUMzQixDQUFDO1FBRUQ7O1dBRUc7Ozs7OztRQUNILFVBQWdCLEtBQWE7WUFDM0IsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7WUFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDOzs7T0FSQTtJQWFELHNCQUFJLHdDQUFLO1FBSFQ7O1dBRUc7Ozs7O1FBQ0g7WUFDUSxJQUFBLG9CQUF1RSxFQUFyRSxzQkFBUSxFQUFFLHNCQUFRLEVBQUUsY0FBSSxFQUFFLGtCQUFNLEVBQUUsa0JBQU0sRUFBRSxvQkFBMkI7WUFDN0UsT0FBTyxDQUFDLFFBQVEsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUMzRSxDQUFDOzs7T0FBQTtJQUtELHNCQUFJLG1EQUFnQjtRQUhwQjs7V0FFRzs7Ozs7UUFDSDtZQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDckMsQ0FBQzs7O09BQUE7SUFFRDs7T0FFRzs7Ozs7O0lBQ0gsaURBQWlCOzs7OztJQUFqQixVQUFrQixHQUFhO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQ7O09BRUc7Ozs7OztJQUNILGdEQUFnQjs7Ozs7SUFBaEIsVUFBaUIsS0FBaUI7UUFDaEMsSUFBSSxDQUFDLG1CQUFBLEtBQUssQ0FBQyxNQUFNLEVBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsS0FBSyxPQUFPLEVBQUU7WUFDL0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3pEO0lBQ0gsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILDJDQUFXOzs7O0lBQVg7UUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7O0lBck1NLDRCQUFNLEdBQUcsQ0FBQyxDQUFDOztnQkFmbkIsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLG90RUFBNkM7b0JBRTdDLFNBQVMsRUFBRTt3QkFDVDs0QkFDRSxPQUFPLEVBQUUsbUJBQW1COzRCQUM1QixXQUFXLEVBQUUscUJBQXFCO3lCQUNuQztxQkFDRjs7aUJBQ0Y7Ozs7Z0JBakJRLFlBQVk7Z0JBSm5CLFVBQVU7Z0JBTXFCLFNBQVMsdUJBOEZyQyxRQUFRLFlBQUksSUFBSTs7O3VCQXhCbEIsS0FBSzs2QkFLTCxLQUFLO3dCQUtMLEtBQUs7d0JBdUNMLEtBQUs7MkJBZ0JMLEtBQUs7MkJBZ0JMLEtBQUs7OEJBZ0JMLEtBQUs7O0lBb0RSLDRCQUFDO0NBQUEsQUF0TkQsSUFzTkM7U0EzTVkscUJBQXFCOzs7Ozs7SUFJaEMsNkJBQWtCOzs7Ozs7SUFLbEIsMENBQTBCOzs7Ozs7SUFLMUIsMENBQTBCOzs7Ozs7SUFLMUIsNkNBQTZCOzs7OztJQUs3Qiw2Q0FBbUM7Ozs7O0lBS25DLHdDQUFnQjs7Ozs7SUFLaEIsMkNBQW1COzs7OztJQUtuQiw0Q0FBbUM7Ozs7O0lBS25DLG1DQUE0RDs7Ozs7SUFLNUQsNENBQWlCOzs7OztJQUtqQixxQ0FBa0M7Ozs7O0lBS2xDLDJDQUF5RDs7Ozs7SUFLekQsc0NBQXlDOzs7Ozs7SUFNekMsNENBQW9COzs7OztJQU1sQixtQ0FBd0I7Ozs7O0lBQ3hCLHNDQUF5Qjs7SUFDekIsMENBQStDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25Jbml0LFxyXG4gIE9uRGVzdHJveSxcclxuICBJbnB1dCxcclxuICBFbGVtZW50UmVmLFxyXG4gIE9wdGlvbmFsLFxyXG4gIFNlbGZcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9jdXNNb25pdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2ExMXknO1xyXG5pbXBvcnQgeyBjb2VyY2VCb29sZWFuUHJvcGVydHkgfSBmcm9tICdAYW5ndWxhci9jZGsvY29lcmNpb24nO1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1Db250cm9sLCBOZ0NvbnRyb2wsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE1hdEZvcm1GaWVsZENvbnRyb2wsIE1hdEZvcm1GaWVsZEFwcGVhcmFuY2UsIFRoZW1lUGFsZXR0ZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgS2Vwc0FkZHJlc3MgfSBmcm9tICcuLi8uLi8uLi9pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2tlcHMtZmllbGQtYWRkcmVzcycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZpZWxkLWFkZHJlc3MuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZpZWxkLWFkZHJlc3MuY29tcG9uZW50LnNjc3MnXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogTWF0Rm9ybUZpZWxkQ29udHJvbCxcclxuICAgICAgdXNlRXhpc3Rpbmc6IEZpZWxkQWRkcmVzc0NvbXBvbmVudFxyXG4gICAgfVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZpZWxkQWRkcmVzc0NvbXBvbmVudCBpbXBsZW1lbnRzIE1hdEZvcm1GaWVsZENvbnRyb2w8S2Vwc0FkZHJlc3M+LCBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc3RhdGljIG5leHRJZCA9IDA7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX3JlcXVpcmVkID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX2Rpc2FibGVkID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX3BsYWNlaG9sZGVyOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHN0YXRlQ2hhbmdlcyA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGZvY3VzZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZXJyb3JTdGF0ZSA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb250cm9sVHlwZSA9ICdrZXBzLWZpZWxkLWFkZHJlc3MnO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBpZCA9IGBrZXBzLWZpZWxkLWFkZHJlc3MtJHtGaWVsZEFkZHJlc3NDb21wb25lbnQubmV4dElkKyt9YDtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZGVzY3JpYmVkQnkgPSAnJztcclxuXHJcbiAgLyoqXHJcbiAgICogRm9ybSBjb250cm9sIG9mIHRoZSBmaWVsZC5cclxuICAgKi9cclxuICBASW5wdXQoKSBmb3JtID0gbmV3IEZvcm1Hcm91cCh7fSk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFwcGVhcmFuY2Ugc3R5bGUgb2YgdGhlIGZvcm0uXHJcbiAgICovXHJcbiAgQElucHV0KCkgYXBwZWFyYW5jZTogTWF0Rm9ybUZpZWxkQXBwZWFyYW5jZSA9ICdzdGFuZGFyZCc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFuZ3VsYXIgTWF0ZXJpYWwgY29sb3Igb2YgdGhlIGZvcm0gZmllbGQgdW5kZXJsaW5lIGFuZCBmbG9hdGluZyBsYWJlbC5cclxuICAgKi9cclxuICBASW5wdXQoKSBjb2xvcjogVGhlbWVQYWxldHRlID0gJ3ByaW1hcnknO1xyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBmaWVsZCBoYXMgYmVlbiBpbml0aWFsaXplZCBvciBub3QuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgaW5pdGlhbGl6ZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZtOiBGb2N1c01vbml0b3IsXHJcbiAgICBwcml2YXRlIGVsUmVmOiBFbGVtZW50UmVmLFxyXG4gICAgQE9wdGlvbmFsKCkgQFNlbGYoKSBwdWJsaWMgbmdDb250cm9sOiBOZ0NvbnRyb2xcclxuICApIHtcclxuICAgIGZtLm1vbml0b3IoZWxSZWYsIHRydWUpLnN1YnNjcmliZShvcmlnaW4gPT4ge1xyXG4gICAgICB0aGlzLmZvY3VzZWQgPSAhIW9yaWdpbjtcclxuICAgICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIGNvbnN0IHZhbGlkYXRvcnMgPSBbXTtcclxuICAgIGlmICh0aGlzLnJlcXVpcmVkKSB7XHJcbiAgICAgIHZhbGlkYXRvcnMucHVzaChWYWxpZGF0b3JzLnJlcXVpcmVkKTtcclxuICAgIH1cclxuICAgIGZvciAoY29uc3QgZmllbGQgb2YgWydhZGRyZXNzMScsICdhZGRyZXNzMicsICdjaXR5JywgJ3JlZ2lvbicsICdwb3N0YWwnLCAnY291bnRyeSddKSB7XHJcbiAgICAgIHRoaXMuZm9ybS5hZGRDb250cm9sKGZpZWxkLCBuZXcgRm9ybUNvbnRyb2wobnVsbCwgdmFsaWRhdG9ycykpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5pbml0aWFsaXplZCA9IHRydWU7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBUaGUgdmFsdWUgb2YgdGhlIGNvbnRyb2wuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgdmFsdWUoKTogS2Vwc0FkZHJlc3MgfCBudWxsIHtcclxuICAgIHJldHVybiB0aGlzLmZvcm0udmFsdWU7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgdmFsdWUodjogS2Vwc0FkZHJlc3MgfCBudWxsKSB7XHJcbiAgICB0aGlzLmZvcm0ucGF0Y2hWYWx1ZSh2KTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbnRyb2wgaXMgcmVxdWlyZWQuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgcmVxdWlyZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5fcmVxdWlyZWQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgcmVxdWlyZWQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuX3JlcXVpcmVkID0gY29lcmNlQm9vbGVhblByb3BlcnR5KHZhbHVlKTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbnRyb2wgaXMgZGlzYWJsZWQuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgZGlzYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5fZGlzYWJsZWQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgZGlzYWJsZWQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuX2Rpc2FibGVkID0gY29lcmNlQm9vbGVhblByb3BlcnR5KHZhbHVlKTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBwbGFjZWhvbGRlciBmb3IgdGhpcyBjb250cm9sLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IHBsYWNlaG9sZGVyKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5fcGxhY2Vob2xkZXI7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgcGxhY2Vob2xkZXIodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5fcGxhY2Vob2xkZXIgPSB2YWx1ZTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbnRyb2wgaXMgZW1wdHkuXHJcbiAgICovXHJcbiAgZ2V0IGVtcHR5KCkge1xyXG4gICAgY29uc3QgeyBhZGRyZXNzMSwgYWRkcmVzczIsIGNpdHksIHJlZ2lvbiwgcG9zdGFsLCBjb3VudHJ5IH0gPSB0aGlzLmZvcm0udmFsdWU7XHJcbiAgICByZXR1cm4gIWFkZHJlc3MxICYmICFhZGRyZXNzMiAmJiAhY2l0eSAmJiAhcmVnaW9uICYmICFwb3N0YWwgJiYgIWNvdW50cnk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBNYXRGb3JtRmllbGQgbGFiZWwgc2hvdWxkIHRyeSB0byBmbG9hdC5cclxuICAgKi9cclxuICBnZXQgc2hvdWxkTGFiZWxGbG9hdCgpIHtcclxuICAgIHJldHVybiB0aGlzLmZvY3VzZWQgfHwgIXRoaXMuZW1wdHk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXREZXNjcmliZWRCeUlkcyhpZHM6IHN0cmluZ1tdKSB7XHJcbiAgICB0aGlzLmRlc2NyaWJlZEJ5ID0gaWRzLmpvaW4oJyAnKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG9uQ29udGFpbmVyQ2xpY2soZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgIGlmICgoZXZlbnQudGFyZ2V0IGFzIEVsZW1lbnQpLnRhZ05hbWUudG9Mb3dlckNhc2UoKSAhPT0gJ2lucHV0Jykge1xyXG4gICAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXQnKS5mb2N1cygpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnN0YXRlQ2hhbmdlcy5jb21wbGV0ZSgpO1xyXG4gICAgdGhpcy5mbS5zdG9wTW9uaXRvcmluZyh0aGlzLmVsUmVmKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==