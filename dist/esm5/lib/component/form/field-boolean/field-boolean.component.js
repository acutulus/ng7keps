/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, Input, Optional, Self } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';
import { Subject } from 'rxjs';
/**
 * A custom field that displays two radio buttons: True, False.
 */
var FieldBooleanComponent = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function FieldBooleanComponent(fm, elRef, ngControl) {
        var _this = this;
        this.fm = fm;
        this.elRef = elRef;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-boolean';
        /**
         * \@internal Do not use!
         */
        this.id = "keps-field-boolean-" + FieldBooleanComponent.nextId++;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new FormControl(null);
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        function (origin) {
            _this.focused = !!origin;
            _this.stateChanges.next();
        }));
    }
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} change
     * @return {?}
     */
    FieldBooleanComponent.prototype.onRadioChange = /**
     * \@internal Do not use!
     * @param {?} change
     * @return {?}
     */
    function (change) {
        this.value = change.value;
    };
    Object.defineProperty(FieldBooleanComponent.prototype, "value", {
        /**
         * The value of the control.
         */
        get: /**
         * The value of the control.
         * @return {?}
         */
        function () {
            return this.form.value;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} b
         * @return {?}
         */
        function (b) {
            this.form.patchValue(b);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldBooleanComponent.prototype, "required", {
        /**
         * Whether the control is required.
         */
        get: /**
         * Whether the control is required.
         * @return {?}
         */
        function () {
            return this._required;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._required = coerceBooleanProperty(value);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldBooleanComponent.prototype, "disabled", {
        /**
         * Whether the control is disabled.
         */
        get: /**
         * Whether the control is disabled.
         * @return {?}
         */
        function () {
            return this._disabled;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._disabled = coerceBooleanProperty(value);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldBooleanComponent.prototype, "placeholder", {
        /**
         * The placeholder for this control.
         */
        get: /**
         * The placeholder for this control.
         * @return {?}
         */
        function () {
            return this._placeholder;
        },
        /**
         * @internal Do not use!
         */
        set: /**
         * \@internal Do not use!
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._placeholder = value;
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldBooleanComponent.prototype, "empty", {
        /**
         * Whether the control is empty.
         */
        get: /**
         * Whether the control is empty.
         * @return {?}
         */
        function () {
            return this.form.value == null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FieldBooleanComponent.prototype, "shouldLabelFloat", {
        /**
         * Whether the MatFormField label should try to float.
         */
        get: /**
         * Whether the MatFormField label should try to float.
         * @return {?}
         */
        function () {
            return this.focused || !this.empty;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    FieldBooleanComponent.prototype.setDescribedByIds = /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    function (ids) {
        this.describedBy = ids.join(' ');
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    FieldBooleanComponent.prototype.onContainerClick = /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    FieldBooleanComponent.prototype.ngOnDestroy = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    };
    /**
     * \@internal Do not use!
     */
    FieldBooleanComponent.nextId = 0;
    FieldBooleanComponent.decorators = [
        { type: Component, args: [{
                    selector: 'keps-field-boolean',
                    template: "<mat-radio-button\r\n  [checked]=\"form.value === true\"\r\n  [disabled]=\"disabled\"\r\n  (change)=\"onRadioChange($event)\"\r\n  [value]=\"true\"\r\n>\r\n  True\r\n</mat-radio-button>\r\n<mat-radio-button\r\n  [checked]=\"form.value === false\"\r\n  [disabled]=\"disabled\"\r\n  (change)=\"onRadioChange($event)\"\r\n  [value]=\"false\"\r\n>\r\n  False\r\n</mat-radio-button>\r\n",
                    providers: [
                        {
                            provide: MatFormFieldControl,
                            useExisting: FieldBooleanComponent
                        }
                    ],
                    styles: [":host{display:flex;flex-direction:column}mat-radio-button{margin-bottom:8px}"]
                }] }
    ];
    /** @nocollapse */
    FieldBooleanComponent.ctorParameters = function () { return [
        { type: FocusMonitor },
        { type: ElementRef },
        { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
    ]; };
    FieldBooleanComponent.propDecorators = {
        form: [{ type: Input }],
        value: [{ type: Input }],
        required: [{ type: Input }],
        disabled: [{ type: Input }],
        placeholder: [{ type: Input }]
    };
    return FieldBooleanComponent;
}());
export { FieldBooleanComponent };
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.nextId;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype._required;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype._disabled;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype._placeholder;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.stateChanges;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.focused;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.errorState;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.controlType;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.id;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.describedBy;
    /**
     * Form control of the field.
     * @type {?}
     */
    FieldBooleanComponent.prototype.form;
    /**
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype.fm;
    /**
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype.elRef;
    /** @type {?} */
    FieldBooleanComponent.prototype.ngControl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtYm9vbGVhbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudC9mb3JtL2ZpZWxkLWJvb2xlYW4vZmllbGQtYm9vbGVhbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBRVQsVUFBVSxFQUNWLEtBQUssRUFDTCxRQUFRLEVBQ1IsSUFBSSxFQUNMLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxtQkFBbUIsRUFBa0IsTUFBTSxtQkFBbUIsQ0FBQztBQUN4RSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDOzs7O0FBSy9CO0lBbUVFOztPQUVHO0lBQ0gsK0JBQ1UsRUFBZ0IsRUFDaEIsS0FBOEIsRUFDWCxTQUFvQjtRQUhqRCxpQkFTQztRQVJTLE9BQUUsR0FBRixFQUFFLENBQWM7UUFDaEIsVUFBSyxHQUFMLEtBQUssQ0FBeUI7UUFDWCxjQUFTLEdBQVQsU0FBUyxDQUFXOzs7O1FBckR6QyxjQUFTLEdBQUcsS0FBSyxDQUFDOzs7O1FBS2xCLGNBQVMsR0FBRyxLQUFLLENBQUM7Ozs7UUFVMUIsaUJBQVksR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDOzs7O1FBS25DLFlBQU8sR0FBRyxLQUFLLENBQUM7Ozs7UUFLaEIsZUFBVSxHQUFHLEtBQUssQ0FBQzs7OztRQUtuQixnQkFBVyxHQUFHLG9CQUFvQixDQUFDOzs7O1FBS25DLE9BQUUsR0FBRyx3QkFBc0IscUJBQXFCLENBQUMsTUFBTSxFQUFJLENBQUM7Ozs7UUFLNUQsZ0JBQVcsR0FBRyxFQUFFLENBQUM7Ozs7UUFLUixTQUFJLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFVcEMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTTtZQUN0QyxLQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDeEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0gsNkNBQWE7Ozs7O0lBQWIsVUFBYyxNQUFzQjtRQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDNUIsQ0FBQztJQUtELHNCQUNJLHdDQUFLO1FBSlQ7O1dBRUc7Ozs7O1FBQ0g7WUFFRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3pCLENBQUM7UUFFRDs7V0FFRzs7Ozs7O1FBQ0gsVUFBVSxDQUFpQjtZQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLENBQUM7OztPQVJBO0lBYUQsc0JBQ0ksMkNBQVE7UUFKWjs7V0FFRzs7Ozs7UUFDSDtZQUVFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUN4QixDQUFDO1FBRUQ7O1dBRUc7Ozs7OztRQUNILFVBQWEsS0FBYztZQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsQ0FBQzs7O09BUkE7SUFhRCxzQkFDSSwyQ0FBUTtRQUpaOztXQUVHOzs7OztRQUNIO1lBRUUsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3hCLENBQUM7UUFFRDs7V0FFRzs7Ozs7O1FBQ0gsVUFBYSxLQUFjO1lBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDOzs7T0FSQTtJQWFELHNCQUNJLDhDQUFXO1FBSmY7O1dBRUc7Ozs7O1FBQ0g7WUFFRSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDM0IsQ0FBQztRQUVEOztXQUVHOzs7Ozs7UUFDSCxVQUFnQixLQUFhO1lBQzNCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsQ0FBQzs7O09BUkE7SUFhRCxzQkFBSSx3Q0FBSztRQUhUOztXQUVHOzs7OztRQUNIO1lBQ0UsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7UUFDakMsQ0FBQzs7O09BQUE7SUFLRCxzQkFBSSxtREFBZ0I7UUFIcEI7O1dBRUc7Ozs7O1FBQ0g7WUFDRSxPQUFPLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3JDLENBQUM7OztPQUFBO0lBRUQ7O09BRUc7Ozs7OztJQUNILGlEQUFpQjs7Ozs7SUFBakIsVUFBa0IsR0FBYTtRQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVEOztPQUVHOzs7Ozs7SUFDSCxnREFBZ0I7Ozs7O0lBQWhCLFVBQWlCLEtBQWlCO1FBQ2hDLElBQUksQ0FBQyxtQkFBQSxLQUFLLENBQUMsTUFBTSxFQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEtBQUssT0FBTyxFQUFFO1lBQy9ELElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN6RDtJQUNILENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCwyQ0FBVzs7OztJQUFYO1FBQ0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDckMsQ0FBQzs7OztJQTdLTSw0QkFBTSxHQUFHLENBQUMsQ0FBQzs7Z0JBZm5CLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5Qix5WUFBNkM7b0JBRTdDLFNBQVMsRUFBRTt3QkFDVDs0QkFDRSxPQUFPLEVBQUUsbUJBQW1COzRCQUM1QixXQUFXLEVBQUUscUJBQXFCO3lCQUNuQztxQkFDRjs7aUJBQ0Y7Ozs7Z0JBbkJRLFlBQVk7Z0JBTG5CLFVBQVU7Z0JBT1UsU0FBUyx1QkFnRjFCLFFBQVEsWUFBSSxJQUFJOzs7dUJBUmxCLEtBQUs7d0JBMEJMLEtBQUs7MkJBZ0JMLEtBQUs7MkJBZ0JMLEtBQUs7OEJBZ0JMLEtBQUs7O0lBa0RSLDRCQUFDO0NBQUEsQUE3TEQsSUE2TEM7U0FsTFkscUJBQXFCOzs7Ozs7SUFJaEMsNkJBQWtCOzs7Ozs7SUFLbEIsMENBQTBCOzs7Ozs7SUFLMUIsMENBQTBCOzs7Ozs7SUFLMUIsNkNBQTZCOzs7OztJQUs3Qiw2Q0FBbUM7Ozs7O0lBS25DLHdDQUFnQjs7Ozs7SUFLaEIsMkNBQW1COzs7OztJQUtuQiw0Q0FBbUM7Ozs7O0lBS25DLG1DQUE0RDs7Ozs7SUFLNUQsNENBQWlCOzs7OztJQUtqQixxQ0FBc0M7Ozs7O0lBTXBDLG1DQUF3Qjs7Ozs7SUFDeEIsc0NBQXNDOztJQUN0QywwQ0FBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkRlc3Ryb3ksXHJcbiAgRWxlbWVudFJlZixcclxuICBJbnB1dCxcclxuICBPcHRpb25hbCxcclxuICBTZWxmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvY3VzTW9uaXRvciB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9hMTF5JztcclxuaW1wb3J0IHsgY29lcmNlQm9vbGVhblByb3BlcnR5IH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvZXJjaW9uJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIE5nQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTWF0Rm9ybUZpZWxkQ29udHJvbCwgTWF0UmFkaW9DaGFuZ2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuXHJcbi8qKlxyXG4gKiBBIGN1c3RvbSBmaWVsZCB0aGF0IGRpc3BsYXlzIHR3byByYWRpbyBidXR0b25zOiBUcnVlLCBGYWxzZS5cclxuICovXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy1maWVsZC1ib29sZWFuJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZmllbGQtYm9vbGVhbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZmllbGQtYm9vbGVhbi5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBNYXRGb3JtRmllbGRDb250cm9sLFxyXG4gICAgICB1c2VFeGlzdGluZzogRmllbGRCb29sZWFuQ29tcG9uZW50XHJcbiAgICB9XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmllbGRCb29sZWFuQ29tcG9uZW50IGltcGxlbWVudHMgTWF0Rm9ybUZpZWxkQ29udHJvbDxib29sZWFuPiwgT25EZXN0cm95IHtcclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzdGF0aWMgbmV4dElkID0gMDtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfcmVxdWlyZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfZGlzYWJsZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfcGxhY2Vob2xkZXI6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc3RhdGVDaGFuZ2VzID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZm9jdXNlZCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBlcnJvclN0YXRlID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnRyb2xUeXBlID0gJ2tlcHMtZmllbGQtYm9vbGVhbic7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGlkID0gYGtlcHMtZmllbGQtYm9vbGVhbi0ke0ZpZWxkQm9vbGVhbkNvbXBvbmVudC5uZXh0SWQrK31gO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBkZXNjcmliZWRCeSA9ICcnO1xyXG5cclxuICAvKipcclxuICAgKiBGb3JtIGNvbnRyb2wgb2YgdGhlIGZpZWxkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGZvcm0gPSBuZXcgRm9ybUNvbnRyb2wobnVsbCk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmbTogRm9jdXNNb25pdG9yLFxyXG4gICAgcHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4sXHJcbiAgICBAT3B0aW9uYWwoKSBAU2VsZigpIHB1YmxpYyBuZ0NvbnRyb2w6IE5nQ29udHJvbFxyXG4gICkge1xyXG4gICAgZm0ubW9uaXRvcihlbFJlZiwgdHJ1ZSkuc3Vic2NyaWJlKG9yaWdpbiA9PiB7XHJcbiAgICAgIHRoaXMuZm9jdXNlZCA9ICEhb3JpZ2luO1xyXG4gICAgICB0aGlzLnN0YXRlQ2hhbmdlcy5uZXh0KCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG9uUmFkaW9DaGFuZ2UoY2hhbmdlOiBNYXRSYWRpb0NoYW5nZSkge1xyXG4gICAgdGhpcy52YWx1ZSA9IGNoYW5nZS52YWx1ZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSB2YWx1ZSBvZiB0aGUgY29udHJvbC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCB2YWx1ZSgpOiBib29sZWFuIHwgbnVsbCB7XHJcbiAgICByZXR1cm4gdGhpcy5mb3JtLnZhbHVlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2V0IHZhbHVlKGI6IGJvb2xlYW4gfCBudWxsKSB7XHJcbiAgICB0aGlzLmZvcm0ucGF0Y2hWYWx1ZShiKTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbnRyb2wgaXMgcmVxdWlyZWQuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgcmVxdWlyZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5fcmVxdWlyZWQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgcmVxdWlyZWQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuX3JlcXVpcmVkID0gY29lcmNlQm9vbGVhblByb3BlcnR5KHZhbHVlKTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbnRyb2wgaXMgZGlzYWJsZWQuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgZGlzYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5fZGlzYWJsZWQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgZGlzYWJsZWQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuX2Rpc2FibGVkID0gY29lcmNlQm9vbGVhblByb3BlcnR5KHZhbHVlKTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBwbGFjZWhvbGRlciBmb3IgdGhpcyBjb250cm9sLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IHBsYWNlaG9sZGVyKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5fcGxhY2Vob2xkZXI7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgcGxhY2Vob2xkZXIodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5fcGxhY2Vob2xkZXIgPSB2YWx1ZTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbnRyb2wgaXMgZW1wdHkuXHJcbiAgICovXHJcbiAgZ2V0IGVtcHR5KCkge1xyXG4gICAgcmV0dXJuIHRoaXMuZm9ybS52YWx1ZSA9PSBudWxsO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgTWF0Rm9ybUZpZWxkIGxhYmVsIHNob3VsZCB0cnkgdG8gZmxvYXQuXHJcbiAgICovXHJcbiAgZ2V0IHNob3VsZExhYmVsRmxvYXQoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5mb2N1c2VkIHx8ICF0aGlzLmVtcHR5O1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2V0RGVzY3JpYmVkQnlJZHMoaWRzOiBzdHJpbmdbXSkge1xyXG4gICAgdGhpcy5kZXNjcmliZWRCeSA9IGlkcy5qb2luKCcgJyk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBvbkNvbnRhaW5lckNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICBpZiAoKGV2ZW50LnRhcmdldCBhcyBFbGVtZW50KS50YWdOYW1lLnRvTG93ZXJDYXNlKCkgIT09ICdpbnB1dCcpIHtcclxuICAgICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2lucHV0JykuZm9jdXMoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMuY29tcGxldGUoKTtcclxuICAgIHRoaXMuZm0uc3RvcE1vbml0b3JpbmcodGhpcy5lbFJlZik7XHJcbiAgfVxyXG59XHJcbiJdfQ==