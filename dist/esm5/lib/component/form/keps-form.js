/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { first } from 'rxjs/operators';
import _ from 'lodash';
/**
 * A Keps form.
 *
 * It extends Angular's `FormGroup` class
 * and changes its `patchValue` and `reset` methods to
 * wait until the form is initialized.
 */
var /**
 * A Keps form.
 *
 * It extends Angular's `FormGroup` class
 * and changes its `patchValue` and `reset` methods to
 * wait until the form is initialized.
 */
KepsForm = /** @class */ (function (_super) {
    tslib_1.__extends(KepsForm, _super);
    function KepsForm() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.initialized = false;
        _this.initialized$ = new BehaviorSubject(false);
        return _this;
    }
    /**
     * Lets the Keps form know that is has been initialized.
     * @internal Do not use!
     */
    /**
     * Lets the Keps form know that is has been initialized.
     * \@internal Do not use!
     * @return {?}
     */
    KepsForm.prototype.initialize = /**
     * Lets the Keps form know that is has been initialized.
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        this.initialized = true;
        this.initialized$.next(true);
    };
    /**
     * Sets the schema for this form.
     * @internal Do not use!
     * @param schema Schema to be used.
     */
    /**
     * Sets the schema for this form.
     * \@internal Do not use!
     * @param {?} schema Schema to be used.
     * @return {?}
     */
    KepsForm.prototype.setSchema = /**
     * Sets the schema for this form.
     * \@internal Do not use!
     * @param {?} schema Schema to be used.
     * @return {?}
     */
    function (schema) {
        this.schema = schema;
    };
    /**
     * Patches the value of the Keps form.
     * It accepts an object with control names as keys, and does its best to match the values to the correct controls in the group.
     *
     * It accepts both super-sets and sub-sets of the group without throwing an error.
     * @param values Values to be passed to the fields.
     * @param options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     */
    /**
     * Patches the value of the Keps form.
     * It accepts an object with control names as keys, and does its best to match the values to the correct controls in the group.
     *
     * It accepts both super-sets and sub-sets of the group without throwing an error.
     * @param {?} values Values to be passed to the fields.
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    KepsForm.prototype.patchValue = /**
     * Patches the value of the Keps form.
     * It accepts an object with control names as keys, and does its best to match the values to the correct controls in the group.
     *
     * It accepts both super-sets and sub-sets of the group without throwing an error.
     * @param {?} values Values to be passed to the fields.
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    function (values, options) {
        var e_1, _a;
        var _this = this;
        if (options === void 0) { options = {}; }
        // Check if form is initialized or not.
        if (this.initialized) {
            if (options === void 0) {
                options = {};
            }
            try {
                for (var _b = tslib_1.__values(Object.keys(values)), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var field = _c.value;
                    /** @type {?} */
                    var value = values[field];
                    // If value is an object with an ID, convert to ID reference.
                    if (_.isPlainObject(value) && value._id) {
                        value = value._id;
                    }
                    // Pass in value to child controls.
                    if (this.get(field)) {
                        this.get(field).patchValue(value, { onlySelf: true, emitEvent: options.emitEvent });
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            this.updateValueAndValidity(options);
        }
        else {
            // If form is not initialized, wait until it is before patching.
            this.initialized$
                .pipe(first((/**
             * @param {?} value
             * @return {?}
             */
            function (value) { return value; })))
                .subscribe((/**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                _this.patchValue(values, options);
            }));
        }
    };
    /**
     * Resets the Keps form, marks all descendants are marked pristine and untouched, and the value of all descendants to null.
     *
     * You reset to a specific form state by passing in a map of states that matches the structure of your form, with control names as keys.
     * The state is a standalone value or a form state object with both a value and a disabled status.
     * @param values Resets the control with an initial value, or an object that defines the initial value and disabled state.
     * @param options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     */
    /**
     * Resets the Keps form, marks all descendants are marked pristine and untouched, and the value of all descendants to null.
     *
     * You reset to a specific form state by passing in a map of states that matches the structure of your form, with control names as keys.
     * The state is a standalone value or a form state object with both a value and a disabled status.
     * @param {?=} values Resets the control with an initial value, or an object that defines the initial value and disabled state.
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    KepsForm.prototype.reset = /**
     * Resets the Keps form, marks all descendants are marked pristine and untouched, and the value of all descendants to null.
     *
     * You reset to a specific form state by passing in a map of states that matches the structure of your form, with control names as keys.
     * The state is a standalone value or a form state object with both a value and a disabled status.
     * @param {?=} values Resets the control with an initial value, or an object that defines the initial value and disabled state.
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    function (values, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        if (this.initialized) {
            if (values === void 0) {
                values = {};
            }
            // Reset each fields in the control.
            for (var field in this.controls) {
                // If no value supplied for this field, try using the schema's default value.
                if (!values[field] && this.schema[field] && this.schema[field].default) {
                    values[field] = this.schema[field].default;
                }
                this.get(field).reset(values[field], { onlySelf: true, emitEvent: options.emitEvent });
            }
            this.markAsPristine(options);
            this.markAsUntouched(options);
            this.updateValueAndValidity(options);
        }
        else {
            // If form is not initialized, wait until it is before reseting.
            this.initialized$
                .pipe(first((/**
             * @param {?} value
             * @return {?}
             */
            function (value) { return value; })))
                .subscribe((/**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                _this.reset(values, options);
            }));
        }
    };
    return KepsForm;
}(FormGroup));
/**
 * A Keps form.
 *
 * It extends Angular's `FormGroup` class
 * and changes its `patchValue` and `reset` methods to
 * wait until the form is initialized.
 */
export { KepsForm };
if (false) {
    /**
     * @type {?}
     * @private
     */
    KepsForm.prototype.schema;
    /**
     * @type {?}
     * @private
     */
    KepsForm.prototype.initialized;
    /**
     * @type {?}
     * @private
     */
    KepsForm.prototype.initialized$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2Vwcy1mb3JtLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvZm9ybS9rZXBzLWZvcm0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN2QyxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDdkMsT0FBTyxDQUFDLE1BQU0sUUFBUSxDQUFDOzs7Ozs7OztBQVN2Qjs7Ozs7Ozs7SUFBOEIsb0NBQVM7SUFBdkM7UUFBQSxxRUF1SEM7UUFySFMsaUJBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsa0JBQVksR0FBRyxJQUFJLGVBQWUsQ0FBVSxLQUFLLENBQUMsQ0FBQzs7SUFvSDdELENBQUM7SUFsSEM7OztPQUdHOzs7Ozs7SUFDSCw2QkFBVTs7Ozs7SUFBVjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsNEJBQVM7Ozs7OztJQUFULFVBQVUsTUFBa0I7UUFDMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7OztPQWNHOzs7Ozs7Ozs7Ozs7Ozs7OztJQUNILDZCQUFVOzs7Ozs7Ozs7Ozs7Ozs7O0lBQVYsVUFDRSxNQUErQixFQUMvQixPQUEwRDs7UUFGNUQsaUJBaUNDO1FBL0JDLHdCQUFBLEVBQUEsWUFBMEQ7UUFFMUQsdUNBQXVDO1FBQ3ZDLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNwQixJQUFJLE9BQU8sS0FBSyxLQUFLLENBQUMsRUFBRTtnQkFBRSxPQUFPLEdBQUcsRUFBRSxDQUFDO2FBQUU7O2dCQUV6QyxLQUFvQixJQUFBLEtBQUEsaUJBQUEsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQSxnQkFBQSw0QkFBRTtvQkFBcEMsSUFBTSxLQUFLLFdBQUE7O3dCQUNWLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDO29CQUV6Qiw2REFBNkQ7b0JBQzdELElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsR0FBRyxFQUFFO3dCQUN2QyxLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQztxQkFDbkI7b0JBRUQsbUNBQW1DO29CQUNuQyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQ25CLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO3FCQUNyRjtpQkFDRjs7Ozs7Ozs7O1lBRUQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3RDO2FBQU07WUFDTCxnRUFBZ0U7WUFDaEUsSUFBSSxDQUFDLFlBQVk7aUJBQ2hCLElBQUksQ0FDSCxLQUFLOzs7O1lBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLEVBQUwsQ0FBSyxFQUFDLENBQ3RCO2lCQUNBLFNBQVM7Ozs7WUFBQyxVQUFBLEtBQUs7Z0JBQ2QsS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDbkMsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFRDs7Ozs7Ozs7Ozs7Ozs7T0FjRzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFDSCx3QkFBSzs7Ozs7Ozs7Ozs7Ozs7OztJQUFMLFVBQ0UsTUFBWSxFQUNaLE9BQTBEO1FBRjVELGlCQTZCQztRQTNCQyx3QkFBQSxFQUFBLFlBQTBEO1FBRTFELElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNwQixJQUFJLE1BQU0sS0FBSyxLQUFLLENBQUMsRUFBRTtnQkFBRSxNQUFNLEdBQUcsRUFBRSxDQUFDO2FBQUU7WUFFdkMsb0NBQW9DO1lBQ3BDLEtBQUssSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakMsNkVBQTZFO2dCQUM3RSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLEVBQUU7b0JBQ3RFLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQztpQkFDNUM7Z0JBQ0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7YUFDeEY7WUFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3RDO2FBQU07WUFDTCxnRUFBZ0U7WUFDaEUsSUFBSSxDQUFDLFlBQVk7aUJBQ2hCLElBQUksQ0FDSCxLQUFLOzs7O1lBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLEVBQUwsQ0FBSyxFQUFDLENBQ3RCO2lCQUNBLFNBQVM7Ozs7WUFBQyxVQUFBLEtBQUs7Z0JBQ2QsS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDOUIsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFSCxlQUFDO0FBQUQsQ0FBQyxBQXZIRCxDQUE4QixTQUFTLEdBdUh0Qzs7Ozs7Ozs7Ozs7Ozs7SUF0SEMsMEJBQTJCOzs7OztJQUMzQiwrQkFBNEI7Ozs7O0lBQzVCLGdDQUEyRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1Hcm91cCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgS2Vwc1NjaGVtYSB9IGZyb20gJy4uLy4uL2ludGVyZmFjZSc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBmaXJzdCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcclxuXHJcbi8qKlxyXG4gKiBBIEtlcHMgZm9ybS5cclxuICpcclxuICogSXQgZXh0ZW5kcyBBbmd1bGFyJ3MgYEZvcm1Hcm91cGAgY2xhc3NcclxuICogYW5kIGNoYW5nZXMgaXRzIGBwYXRjaFZhbHVlYCBhbmQgYHJlc2V0YCBtZXRob2RzIHRvXHJcbiAqIHdhaXQgdW50aWwgdGhlIGZvcm0gaXMgaW5pdGlhbGl6ZWQuXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgS2Vwc0Zvcm0gZXh0ZW5kcyBGb3JtR3JvdXAge1xyXG4gIHByaXZhdGUgc2NoZW1hOiBLZXBzU2NoZW1hO1xyXG4gIHByaXZhdGUgaW5pdGlhbGl6ZWQgPSBmYWxzZTtcclxuICBwcml2YXRlIGluaXRpYWxpemVkJCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xyXG5cclxuICAvKipcclxuICAgKiBMZXRzIHRoZSBLZXBzIGZvcm0ga25vdyB0aGF0IGlzIGhhcyBiZWVuIGluaXRpYWxpemVkLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGluaXRpYWxpemUoKSB7XHJcbiAgICB0aGlzLmluaXRpYWxpemVkID0gdHJ1ZTtcclxuICAgIHRoaXMuaW5pdGlhbGl6ZWQkLm5leHQodHJ1ZSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZXRzIHRoZSBzY2hlbWEgZm9yIHRoaXMgZm9ybS5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKiBAcGFyYW0gc2NoZW1hIFNjaGVtYSB0byBiZSB1c2VkLlxyXG4gICAqL1xyXG4gIHNldFNjaGVtYShzY2hlbWE6IEtlcHNTY2hlbWEpIHtcclxuICAgIHRoaXMuc2NoZW1hID0gc2NoZW1hO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUGF0Y2hlcyB0aGUgdmFsdWUgb2YgdGhlIEtlcHMgZm9ybS5cclxuICAgKiBJdCBhY2NlcHRzIGFuIG9iamVjdCB3aXRoIGNvbnRyb2wgbmFtZXMgYXMga2V5cywgYW5kIGRvZXMgaXRzIGJlc3QgdG8gbWF0Y2ggdGhlIHZhbHVlcyB0byB0aGUgY29ycmVjdCBjb250cm9scyBpbiB0aGUgZ3JvdXAuXHJcbiAgICpcclxuICAgKiBJdCBhY2NlcHRzIGJvdGggc3VwZXItc2V0cyBhbmQgc3ViLXNldHMgb2YgdGhlIGdyb3VwIHdpdGhvdXQgdGhyb3dpbmcgYW4gZXJyb3IuXHJcbiAgICogQHBhcmFtIHZhbHVlcyBWYWx1ZXMgdG8gYmUgcGFzc2VkIHRvIHRoZSBmaWVsZHMuXHJcbiAgICogQHBhcmFtIG9wdGlvbnNcclxuICAgKiBDb25maWd1cmF0aW9uIG9wdGlvbnMgdGhhdCBkZXRlcm1pbmUgaG93IHRoZSBjb250cm9sIHByb3BhZ2F0ZXMgY2hhbmdlcyBhbmQgZW1pdHMgZXZlbnRzIGFmdGVyIHRoZSB2YWx1ZSBpcyBwYXRjaGVkLlxyXG4gICAqIC0gYG9ubHlTZWxmYDogV2hlbiB0cnVlLCBlYWNoIGNoYW5nZSBvbmx5IGFmZmVjdHMgdGhpcyBjb250cm9sIGFuZCBub3QgaXRzIHBhcmVudC4gRGVmYXVsdCBpcyB0cnVlLlxyXG4gICAqIC0gYGVtaXRFdmVudGA6XHJcbiAgICogV2hlbiB0cnVlIG9yIG5vdCBzdXBwbGllZCAodGhlIGRlZmF1bHQpLFxyXG4gICAqIGJvdGggdGhlIGBzdGF0dXNDaGFuZ2VzYCBhbmQgYHZhbHVlQ2hhbmdlc2Agb2JzZXJ2YWJsZXMgZW1pdCBldmVudHMgd2l0aCB0aGUgbGF0ZXN0IHN0YXR1cyBhbmQgdmFsdWUgd2hlbiB0aGUgY29udHJvbCB2YWx1ZSBpcyB1cGRhdGVkLlxyXG4gICAqIFdoZW4gZmFsc2UsIG5vIGV2ZW50cyBhcmUgZW1pdHRlZC5cclxuICAgKiBUaGUgY29uZmlndXJhdGlvbiBvcHRpb25zIGFyZSBwYXNzZWQgdG8gdGhlIGB1cGRhdGVWYWx1ZUFuZFZhbGlkaXR5YCBtZXRob2QuXHJcbiAgICovXHJcbiAgcGF0Y2hWYWx1ZShcclxuICAgIHZhbHVlczogeyBba2V5OiBzdHJpbmddOiBhbnk7IH0sXHJcbiAgICBvcHRpb25zOiB7IG9ubHlTZWxmPzogYm9vbGVhbjsgZW1pdEV2ZW50PzogYm9vbGVhbjsgfSA9IHt9XHJcbiAgKTogdm9pZCB7XHJcbiAgICAvLyBDaGVjayBpZiBmb3JtIGlzIGluaXRpYWxpemVkIG9yIG5vdC5cclxuICAgIGlmICh0aGlzLmluaXRpYWxpemVkKSB7XHJcbiAgICAgIGlmIChvcHRpb25zID09PSB2b2lkIDApIHsgb3B0aW9ucyA9IHt9OyB9XHJcblxyXG4gICAgICBmb3IgKGNvbnN0IGZpZWxkIG9mIE9iamVjdC5rZXlzKHZhbHVlcykpIHtcclxuICAgICAgICBsZXQgdmFsdWUgPSB2YWx1ZXNbZmllbGRdO1xyXG5cclxuICAgICAgICAvLyBJZiB2YWx1ZSBpcyBhbiBvYmplY3Qgd2l0aCBhbiBJRCwgY29udmVydCB0byBJRCByZWZlcmVuY2UuXHJcbiAgICAgICAgaWYgKF8uaXNQbGFpbk9iamVjdCh2YWx1ZSkgJiYgdmFsdWUuX2lkKSB7XHJcbiAgICAgICAgICB2YWx1ZSA9IHZhbHVlLl9pZDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFBhc3MgaW4gdmFsdWUgdG8gY2hpbGQgY29udHJvbHMuXHJcbiAgICAgICAgaWYgKHRoaXMuZ2V0KGZpZWxkKSkge1xyXG4gICAgICAgICAgdGhpcy5nZXQoZmllbGQpLnBhdGNoVmFsdWUodmFsdWUsIHsgb25seVNlbGY6IHRydWUsIGVtaXRFdmVudDogb3B0aW9ucy5lbWl0RXZlbnQgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLnVwZGF0ZVZhbHVlQW5kVmFsaWRpdHkob3B0aW9ucyk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBJZiBmb3JtIGlzIG5vdCBpbml0aWFsaXplZCwgd2FpdCB1bnRpbCBpdCBpcyBiZWZvcmUgcGF0Y2hpbmcuXHJcbiAgICAgIHRoaXMuaW5pdGlhbGl6ZWQkXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIGZpcnN0KHZhbHVlID0+IHZhbHVlKVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUodmFsdWUgPT4ge1xyXG4gICAgICAgIHRoaXMucGF0Y2hWYWx1ZSh2YWx1ZXMsIG9wdGlvbnMpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJlc2V0cyB0aGUgS2VwcyBmb3JtLCBtYXJrcyBhbGwgZGVzY2VuZGFudHMgYXJlIG1hcmtlZCBwcmlzdGluZSBhbmQgdW50b3VjaGVkLCBhbmQgdGhlIHZhbHVlIG9mIGFsbCBkZXNjZW5kYW50cyB0byBudWxsLlxyXG4gICAqXHJcbiAgICogWW91IHJlc2V0IHRvIGEgc3BlY2lmaWMgZm9ybSBzdGF0ZSBieSBwYXNzaW5nIGluIGEgbWFwIG9mIHN0YXRlcyB0aGF0IG1hdGNoZXMgdGhlIHN0cnVjdHVyZSBvZiB5b3VyIGZvcm0sIHdpdGggY29udHJvbCBuYW1lcyBhcyBrZXlzLlxyXG4gICAqIFRoZSBzdGF0ZSBpcyBhIHN0YW5kYWxvbmUgdmFsdWUgb3IgYSBmb3JtIHN0YXRlIG9iamVjdCB3aXRoIGJvdGggYSB2YWx1ZSBhbmQgYSBkaXNhYmxlZCBzdGF0dXMuXHJcbiAgICogQHBhcmFtIHZhbHVlcyBSZXNldHMgdGhlIGNvbnRyb2wgd2l0aCBhbiBpbml0aWFsIHZhbHVlLCBvciBhbiBvYmplY3QgdGhhdCBkZWZpbmVzIHRoZSBpbml0aWFsIHZhbHVlIGFuZCBkaXNhYmxlZCBzdGF0ZS5cclxuICAgKiBAcGFyYW0gb3B0aW9uc1xyXG4gICAqIENvbmZpZ3VyYXRpb24gb3B0aW9ucyB0aGF0IGRldGVybWluZSBob3cgdGhlIGNvbnRyb2wgcHJvcGFnYXRlcyBjaGFuZ2VzIGFuZCBlbWl0cyBldmVudHMgYWZ0ZXIgdGhlIHZhbHVlIGlzIHBhdGNoZWQuXHJcbiAgICogLSBgb25seVNlbGZgOiBXaGVuIHRydWUsIGVhY2ggY2hhbmdlIG9ubHkgYWZmZWN0cyB0aGlzIGNvbnRyb2wgYW5kIG5vdCBpdHMgcGFyZW50LiBEZWZhdWx0IGlzIHRydWUuXHJcbiAgICogLSBgZW1pdEV2ZW50YDpcclxuICAgKiBXaGVuIHRydWUgb3Igbm90IHN1cHBsaWVkICh0aGUgZGVmYXVsdCksXHJcbiAgICogYm90aCB0aGUgYHN0YXR1c0NoYW5nZXNgIGFuZCBgdmFsdWVDaGFuZ2VzYCBvYnNlcnZhYmxlcyBlbWl0IGV2ZW50cyB3aXRoIHRoZSBsYXRlc3Qgc3RhdHVzIGFuZCB2YWx1ZSB3aGVuIHRoZSBjb250cm9sIHZhbHVlIGlzIHVwZGF0ZWQuXHJcbiAgICogV2hlbiBmYWxzZSwgbm8gZXZlbnRzIGFyZSBlbWl0dGVkLlxyXG4gICAqIFRoZSBjb25maWd1cmF0aW9uIG9wdGlvbnMgYXJlIHBhc3NlZCB0byB0aGUgYHVwZGF0ZVZhbHVlQW5kVmFsaWRpdHlgIG1ldGhvZC5cclxuICAgKi9cclxuICByZXNldChcclxuICAgIHZhbHVlcz86IGFueSxcclxuICAgIG9wdGlvbnM6IHsgb25seVNlbGY/OiBib29sZWFuOyBlbWl0RXZlbnQ/OiBib29sZWFuOyB9ID0ge31cclxuICApOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmluaXRpYWxpemVkKSB7XHJcbiAgICAgIGlmICh2YWx1ZXMgPT09IHZvaWQgMCkgeyB2YWx1ZXMgPSB7fTsgfVxyXG5cclxuICAgICAgLy8gUmVzZXQgZWFjaCBmaWVsZHMgaW4gdGhlIGNvbnRyb2wuXHJcbiAgICAgIGZvciAoY29uc3QgZmllbGQgaW4gdGhpcy5jb250cm9scykge1xyXG4gICAgICAgIC8vIElmIG5vIHZhbHVlIHN1cHBsaWVkIGZvciB0aGlzIGZpZWxkLCB0cnkgdXNpbmcgdGhlIHNjaGVtYSdzIGRlZmF1bHQgdmFsdWUuXHJcbiAgICAgICAgaWYgKCF2YWx1ZXNbZmllbGRdICYmIHRoaXMuc2NoZW1hW2ZpZWxkXSAmJiB0aGlzLnNjaGVtYVtmaWVsZF0uZGVmYXVsdCkge1xyXG4gICAgICAgICAgdmFsdWVzW2ZpZWxkXSA9IHRoaXMuc2NoZW1hW2ZpZWxkXS5kZWZhdWx0O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmdldChmaWVsZCkucmVzZXQodmFsdWVzW2ZpZWxkXSwgeyBvbmx5U2VsZjogdHJ1ZSwgZW1pdEV2ZW50OiBvcHRpb25zLmVtaXRFdmVudCB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5tYXJrQXNQcmlzdGluZShvcHRpb25zKTtcclxuICAgICAgdGhpcy5tYXJrQXNVbnRvdWNoZWQob3B0aW9ucyk7XHJcbiAgICAgIHRoaXMudXBkYXRlVmFsdWVBbmRWYWxpZGl0eShvcHRpb25zKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIElmIGZvcm0gaXMgbm90IGluaXRpYWxpemVkLCB3YWl0IHVudGlsIGl0IGlzIGJlZm9yZSByZXNldGluZy5cclxuICAgICAgdGhpcy5pbml0aWFsaXplZCRcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgZmlyc3QodmFsdWUgPT4gdmFsdWUpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZSh2YWx1ZSA9PiB7XHJcbiAgICAgICAgdGhpcy5yZXNldCh2YWx1ZXMsIG9wdGlvbnMpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiJdfQ==