/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, ViewChild, ContentChildren, QueryList, TemplateRef, EventEmitter, Inject } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataService } from '../../../service/data.service';
import { Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { TableColDirective } from '../table-col.directive';
import { isType, getReferenceDisplay, getReferenceDisplayArray } from '../../../utils';
import _ from 'lodash';
var DynamicTableComponent = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function DynamicTableComponent(models, dataService) {
        this.models = models;
        this.dataService = dataService;
        /**
         * Data to be shown on the table from the GraphQL query.
         * \@readonly
         */
        this.data = [];
        /**
         * Columns to be displayed on the table.
         */
        this.displayedColumns = [];
        /**
         * Columns to be hidden on the table.
         */
        this.hiddenColumns = [];
        /**
         * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
         * \@internal Do not use!
         */
        this.allColumns = [];
        /**
         * A record of custom column data and template keyed by the custom column ID.
         */
        this.customCols = {};
        /**
         * The table's default paginator page size.
         */
        this.defaultPageSize = 10;
        /**
         * Length of the data to be shown on the paginator.
         * \@readonly
         */
        this.dataLength = Infinity;
        /**
         * Whether the data length has been known (from last query) or not.
         */
        this.isDataLengthSet = false;
        /**
         * Whether the table should show the default search box or not.
         */
        this.showSearch = false;
        /**
         * A subject that emits every time the search string changes.
         */
        this.searchChange = new Subject();
        /**
         * Delay for the search function to be called in milliseconds.
         */
        this.searchDelay = 1000;
        /**
         * A subject that emits every time the filter function changes.
         */
        this.filterFunctionChange = new Subject();
        /**
         * An event emitter that emits the object of the row clicked by the user.
         */
        this.rowClick = new EventEmitter();
        /**
         * Whether the table is currently loading or not.
         * \@readonly
         */
        this.tableLoading = true;
        /**
         * Whether the table has done initializing or not.
         * \@readonly
         */
        this.initialized = false;
    }
    Object.defineProperty(DynamicTableComponent.prototype, "search", {
        /**
         * String to be searched based on `searchFields`.
         */
        get: /**
         * String to be searched based on `searchFields`.
         * @return {?}
         */
        function () {
            return this._search;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._search = value;
            this.searchChange.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DynamicTableComponent.prototype, "filterFunction", {
        /**
         * Function to filter the result after query.
         */
        get: /**
         * Function to filter the result after query.
         * @return {?}
         */
        function () {
            return this._filterFunction;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._filterFunction = value;
            this.filterFunctionChange.next();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    DynamicTableComponent.prototype.ngOnInit = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        // Get the previous session's page size if any, or use the default page size.
        this.pageSize = Number(localStorage.getItem(this.savePageSize)) || this.defaultPageSize;
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    DynamicTableComponent.prototype.ngAfterViewInit = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        var _this = this;
        // Get data as soon as view initialized.
        this.getData();
        // Setup delayed search.
        /** @type {?} */
        var delayedSearch = this.searchChange
            .pipe(debounceTime(this.searchDelay), distinctUntilChanged());
        // If any of these observables emit, then we reset the paginator.
        delayedSearch.subscribe((/**
         * @return {?}
         */
        function () {
            _this.paginator.pageIndex = 0;
            _this.dataLength = Infinity;
        }));
        this.filterFunctionChange.subscribe((/**
         * @return {?}
         */
        function () {
            _this.paginator.pageIndex = 0;
            _this.dataLength = Infinity;
        }));
        this.sort.sortChange.subscribe((/**
         * @return {?}
         */
        function () { return _this.paginator.pageIndex = 0; }));
        // Save page size if paginator changes.
        if (this.savePageSize) {
            this.paginator.page.subscribe((/**
             * @return {?}
             */
            function () {
                localStorage.setItem(_this.savePageSize, _this.paginator.pageSize.toString());
            }));
        }
        // If any of these observables emit, then we refresh the data.
        merge(this.sort.sortChange, this.paginator.page, delayedSearch, this.filterFunctionChange)
            .subscribe((/**
         * @return {?}
         */
        function () { return _this.getData(); }));
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    DynamicTableComponent.prototype.ngAfterContentInit = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        // We are using `ngAfterContentInit` here because we can
        // only get the custom columns data after the component content
        // has been initialized.
        var _this = this;
        // Get schema from model name if supplied.
        if (this.model && this.models[this.model]) {
            this.schema = this.models[this.model].schema;
        }
        else if (this.model && !this.models[this.model]) {
            throw new Error("Can't find a model named " + this.model + "!");
        }
        // Assert schema must exist.
        if (!this.schema) {
            throw new Error("Must supply valid model name to Keps dynamic table!");
        }
        // Set `allColumns` to be every public field in the schema.
        this.allColumns = Object.keys(this.schema).filter((/**
         * @param {?} field
         * @return {?}
         */
        function (field) { return field[0] !== '_'; }));
        // Process custom columns.
        /** @type {?} */
        var colDatas = this.customColDatas.toArray();
        /** @type {?} */
        var colTemplates = this.customColTemplates.toArray();
        for (var i = 0; i < colDatas.length; i++) {
            /** @type {?} */
            var colId = colDatas[i].id;
            this.customCols[colId] = {
                data: colDatas[i],
                template: colTemplates[i]
            };
            this.allColumns.push(colId);
        }
        // If `displayedColumns` is not provided, use `allColumns` filtered by `hiddenColumns`.
        if (this.displayedColumns.length === 0) {
            this.displayedColumns = this.allColumns.filter((/**
             * @param {?} field
             * @return {?}
             */
            function (field) { return !_this.hiddenColumns.includes(field); }));
        }
        this.initialized = true;
    };
    /**
     * Combines properties as GraphQL query and gets data from data service.
     */
    /**
     * Combines properties as GraphQL query and gets data from data service.
     * @return {?}
     */
    DynamicTableComponent.prototype.getData = /**
     * Combines properties as GraphQL query and gets data from data service.
     * @return {?}
     */
    function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var filters, _a, pageIndex, pageSize, _b, active, direction, mongoFilter, _c, _d, field, result, data, err_1;
            var e_1, _e;
            return tslib_1.__generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        _f.trys.push([0, 2, , 3]);
                        this.tableLoading = true;
                        filters = {};
                        // Setup page query.
                        _a = this.paginator, pageIndex = _a.pageIndex, pageSize = _a.pageSize;
                        filters.offset = pageIndex * pageSize;
                        // Setup sort query.
                        _b = this.sort, active = _b.active, direction = _b.direction;
                        if (active) {
                            filters.sort = (direction === 'desc' ? '-' : '+') + active;
                        }
                        // Setup search query.
                        if (this.searchFields && this.search) {
                            mongoFilter = " { '$or': [";
                            try {
                                for (_c = tslib_1.__values(this.searchFields), _d = _c.next(); !_d.done; _d = _c.next()) {
                                    field = _d.value;
                                    mongoFilter += "{'" + field + "': {\n            '$regex': '" + this.search.replace('(', '%28').replace(')', '%29') + "',\n            '$options': 'i' }},";
                                }
                            }
                            catch (e_1_1) { e_1 = { error: e_1_1 }; }
                            finally {
                                try {
                                    if (_d && !_d.done && (_e = _c.return)) _e.call(_c);
                                }
                                finally { if (e_1) throw e_1.error; }
                            }
                            mongoFilter = _.trimEnd(mongoFilter, ',');
                            mongoFilter += ']}';
                            filters.mongo = mongoFilter;
                        }
                        // Set limit if there is not filter function.
                        if (!this.filterFunction) {
                            filters.limit = pageSize;
                        }
                        // Get data from data service.
                        return [4 /*yield*/, this.dataService.graphql("\n        " + this.model + "s(\n          " + this.convertFilter(Object.assign(filters, this.filterQuery)) + "\n        ){" + this.query + "}\n      ")];
                    case 1:
                        result = _f.sent();
                        data = result.data[this.model + 's'];
                        // If filter function exists, filter with that function.
                        if (this.filterFunction) {
                            this.data = _.filter(data, this.filterFunction);
                        }
                        else {
                            // If this is not the first page and data is empty,
                            // then we go back to the last page.
                            if (pageIndex > 1 && data.length === 0) {
                                // Calculate data length.
                                this.dataLength = (pageIndex - 1) * pageSize + this.data.length;
                                this.paginator.pageIndex = pageIndex - 1;
                            }
                            else {
                                // If data length is smaller than page size,
                                // we assume we're on last page and recalculate data length.
                                if (data.length < pageSize) {
                                    this.dataLength = pageIndex * pageSize + data.length;
                                }
                                // Set data as is.
                                this.data = data;
                            }
                        }
                        this.tableLoading = false;
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _f.sent();
                        console.error(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Converts a filter object to Mongo filter string.
     * @param filters Pair of keys & values to be converted to Mongo filter.
     */
    /**
     * Converts a filter object to Mongo filter string.
     * @private
     * @param {?} filters Pair of keys & values to be converted to Mongo filter.
     * @return {?}
     */
    DynamicTableComponent.prototype.convertFilter = /**
     * Converts a filter object to Mongo filter string.
     * @private
     * @param {?} filters Pair of keys & values to be converted to Mongo filter.
     * @return {?}
     */
    function (filters) {
        /** @type {?} */
        var str = '';
        for (var filterName in filters) {
            if (filterName === 'limit' || filterName === 'offset') {
                str += filterName + ":" + filters[filterName] + ",";
            }
            else {
                str += filterName + ":\"" + filters[filterName] + "\",";
            }
        }
        return str;
    };
    /**
     * Checks the type of a field.
     * @internal Do not use!
     * @param field Field to be checked.
     * @param type Type to be checked.
     */
    /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    DynamicTableComponent.prototype.isType = /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    function (field, type) {
        return isType(this.schema, field, type);
    };
    /**
     * Returns the display expression of a reference field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    DynamicTableComponent.prototype.getReferenceDisplay = /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    function (field) {
        return getReferenceDisplay(this.models, this.schema, field);
    };
    /**
     * Returns the display expression of an array of references field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    /**
     * Returns the display expression of an array of references field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    DynamicTableComponent.prototype.getReferenceDisplayArray = /**
     * Returns the display expression of an array of references field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    function (field) {
        return getReferenceDisplayArray(this.models, this.schema, field);
    };
    DynamicTableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'keps-dynamic-table',
                    template: "<div *ngIf=\"tableLoading\" class=\"table-loader\">\r\n  <mat-spinner></mat-spinner>\r\n</div>\r\n\r\n<!-- Table's Search Box -->\r\n<mat-form-field\r\n  *ngIf=\"showSearch\"\r\n  floatLabel=\"never\"\r\n  class=\"keps-table-search\"\r\n>\r\n  <input\r\n    matInput\r\n    (keyup)=\"search = $event.target.value\"\r\n    placeholder=\"Search\"\r\n  >\r\n</mat-form-field>\r\n\r\n<!-- Table -->\r\n<table\r\n  mat-table [dataSource]=\"data\"\r\n  matSort [matSortActive]=\"activeSort\" matSortDirection=\"asc\"\r\n  class=\"keps-table\"\r\n>\r\n  <!-- Table Columns from Schema -->\r\n  <ng-container\r\n    *ngFor=\"let fieldName of schema | keys\"\r\n    [matColumnDef]=\"fieldName\"\r\n  >\r\n    <ng-container *ngIf=\"schema[fieldName]; let field\">\r\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n        {{ (field.label ? field.label : fieldName) | splitCamel | titlecase }}\r\n      </th>\r\n      <td \r\n        mat-cell *matCellDef=\"let element\" \r\n        class=\"keps-table-cell\"\r\n      >\r\n        <ng-container *ngIf=\"element[fieldName]; let fieldData\">\r\n          <span *ngIf=\"isType(fieldName, 'reference')\">\r\n            {{ fieldData | eval:getReferenceDisplay(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'address')\">\r\n            {{ fieldData | address:4 }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'datetime')\">\r\n            {{ fieldData | moment:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-reference')\">\r\n            {{ fieldData | arrayDisplay:', ':getReferenceDisplayArray(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-string')\">\r\n            {{ fieldData | arrayDisplay:', ' }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'number')\">\r\n            {{ fieldData | numberFormat:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'enum')\">\r\n            {{ field.labels && field.labels[fieldData] ? field.labels[fieldData] : fieldData }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'table-normal')\">\r\n            {{ fieldData }}\r\n          </span>\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Table Columns from Custom Columns -->\r\n  <ng-container\r\n    *ngFor=\"let colName of customCols | keys\"\r\n    [matColumnDef]=\"colName\"\r\n  >\r\n    <ng-container *ngIf=\"customCols[colName].data; let colData\">\r\n      <th\r\n        mat-header-cell *matHeaderCellDef\r\n        [width]=\"colData.width\"\r\n        [align]=\"colData.align\"\r\n        mat-sort-header [disabled]=\"!colData.sortable\" [start]=\"colData.sortStart\"\r\n      >\r\n        {{ (colData.label || colName) | splitCamel | titlecase }}\r\n      </th>\r\n\r\n      <td\r\n        mat-cell *matCellDef=\"let element; let i = index;\"\r\n        [align]=\"colData.align\"\r\n        class=\"keps-table-column\"\r\n      >\r\n        <ng-container\r\n          *ngTemplateOutlet=\"customCols[colName].template; context: { $implicit: element, index: i, element: element }\"\r\n        >\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Rows -->\r\n  <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n  <tr\r\n    mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n    (click)=\"rowClick.emit(row)\"\r\n    [class.clickable-row]=\"rowClick.observers.length > 0\"\r\n  >\r\n  </tr>\r\n</table>\r\n\r\n<!-- Empty Table Row -->\r\n<mat-card *ngIf=\"data.length === 0\" class=\"empty-table\">\r\n  No Data.\r\n</mat-card>\r\n\r\n<!-- Paginator -->\r\n<mat-paginator\r\n  [class.hide-paginator]=\"filterFunction\"\r\n  [pageSizeOptions]=\"[10, 25, 50, 100]\"\r\n  [pageSize]=\"pageSize\"\r\n  [length]=\"dataLength\"\r\n>\r\n</mat-paginator>\r\n",
                    styles: [":host{display:block;position:relative}.keps-table{width:100%}.empty-table,.keps-paginator,.keps-table{background:0 0!important}.keps-table-cell{white-space:pre-line}.keps-table-cell *{display:block;margin:16px 0}.empty-table{border-radius:0;text-align:center;box-shadow:none!important}.hide-paginator{display:none!important}.keps-table-search{width:95%;margin:0 2.5%}.clickable-row:hover{background:rgba(0,0,0,.05);cursor:pointer}.table-loader{background:rgba(0,0,0,.125);z-index:999;width:100%;height:100%;position:absolute;display:flex;justify-content:center;align-items:center}"]
                }] }
    ];
    /** @nocollapse */
    DynamicTableComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] },
        { type: DataService }
    ]; };
    DynamicTableComponent.propDecorators = {
        model: [{ type: Input }],
        query: [{ type: Input }],
        filterQuery: [{ type: Input }],
        displayedColumns: [{ type: Input }],
        hiddenColumns: [{ type: Input }],
        customColDatas: [{ type: ContentChildren, args: [TableColDirective,] }],
        customColTemplates: [{ type: ContentChildren, args: [TableColDirective, { read: TemplateRef },] }],
        paginator: [{ type: ViewChild, args: [MatPaginator, { static: true },] }],
        savePageSize: [{ type: Input }],
        defaultPageSize: [{ type: Input }],
        sort: [{ type: ViewChild, args: [MatSort, { static: true },] }],
        activeSort: [{ type: Input }],
        showSearch: [{ type: Input }],
        searchFields: [{ type: Input }],
        search: [{ type: Input, args: ['search',] }],
        searchDelay: [{ type: Input }],
        filterFunction: [{ type: Input, args: ['filterFunction',] }],
        rowClick: [{ type: Output }]
    };
    return DynamicTableComponent;
}());
export { DynamicTableComponent };
if (false) {
    /**
     * Name of the model for the table.
     * @type {?}
     */
    DynamicTableComponent.prototype.model;
    /**
     * Schema of the model.
     * \@internal Do not use!
     * @type {?}
     */
    DynamicTableComponent.prototype.schema;
    /**
     * Data to be shown on the table from the GraphQL query.
     * \@readonly
     * @type {?}
     */
    DynamicTableComponent.prototype.data;
    /**
     * Comma-separated list of fields for the GraphQL query.
     * @type {?}
     */
    DynamicTableComponent.prototype.query;
    /**
     * Additional filter to be added to the GraphQL query.
     * @type {?}
     */
    DynamicTableComponent.prototype.filterQuery;
    /**
     * Columns to be displayed on the table.
     * @type {?}
     */
    DynamicTableComponent.prototype.displayedColumns;
    /**
     * Columns to be hidden on the table.
     * @type {?}
     */
    DynamicTableComponent.prototype.hiddenColumns;
    /**
     * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
     * \@internal Do not use!
     * @type {?}
     */
    DynamicTableComponent.prototype.allColumns;
    /**
     * Queries all `TableColDirective` data inside the component.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.customColDatas;
    /**
     * Queries all `TableColDirective` templates inside the component.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.customColTemplates;
    /**
     * A record of custom column data and template keyed by the custom column ID.
     * @type {?}
     */
    DynamicTableComponent.prototype.customCols;
    /**
     * Reference to the MatPaginator.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.paginator;
    /**
     * A key used when saving page size number to the local storage.
     * @type {?}
     */
    DynamicTableComponent.prototype.savePageSize;
    /**
     * The table's default paginator page size.
     * @type {?}
     */
    DynamicTableComponent.prototype.defaultPageSize;
    /**
     * The current page size on paginator.
     * @type {?}
     */
    DynamicTableComponent.prototype.pageSize;
    /**
     * Length of the data to be shown on the paginator.
     * \@readonly
     * @type {?}
     */
    DynamicTableComponent.prototype.dataLength;
    /**
     * Whether the data length has been known (from last query) or not.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.isDataLengthSet;
    /**
     * Reference to the MatSort.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.sort;
    /**
     * The ID of the column that should be sorted.
     * @type {?}
     */
    DynamicTableComponent.prototype.activeSort;
    /**
     * Whether the table should show the default search box or not.
     * @type {?}
     */
    DynamicTableComponent.prototype.showSearch;
    /**
     * Field used for the search.
     * @type {?}
     */
    DynamicTableComponent.prototype.searchFields;
    /**
     * String to be searched based on `searchFields`.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype._search;
    /**
     * A subject that emits every time the search string changes.
     * @type {?}
     */
    DynamicTableComponent.prototype.searchChange;
    /**
     * Delay for the search function to be called in milliseconds.
     * @type {?}
     */
    DynamicTableComponent.prototype.searchDelay;
    /**
     * Function to filter the result after query.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype._filterFunction;
    /**
     * A subject that emits every time the filter function changes.
     * @type {?}
     */
    DynamicTableComponent.prototype.filterFunctionChange;
    /**
     * An event emitter that emits the object of the row clicked by the user.
     * @type {?}
     */
    DynamicTableComponent.prototype.rowClick;
    /**
     * Whether the table is currently loading or not.
     * \@readonly
     * @type {?}
     */
    DynamicTableComponent.prototype.tableLoading;
    /**
     * Whether the table has done initializing or not.
     * \@readonly
     * @type {?}
     */
    DynamicTableComponent.prototype.initialized;
    /** @type {?} */
    DynamicTableComponent.prototype.models;
    /**
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.dataService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy10YWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudC90YWJsZS9keW5hbWljLXRhYmxlL2R5bmFtaWMtdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNMLFNBQVMsRUFJVCxLQUFLLEVBQ0wsTUFBTSxFQUNOLFNBQVMsRUFDVCxlQUFlLEVBQ2YsU0FBUyxFQUNULFdBQVcsRUFDWCxZQUFZLEVBQ1osTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDMUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBRTVELE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxZQUFZLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQWdCLE1BQU0sd0JBQXdCLENBQUM7QUFDekUsT0FBTyxFQUFFLE1BQU0sRUFBRSxtQkFBbUIsRUFBRSx3QkFBd0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3ZGLE9BQU8sQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQVF2QjtJQXFMRTs7T0FFRztJQUNILCtCQUMyQixNQUFrQixFQUNuQyxXQUF3QjtRQURQLFdBQU0sR0FBTixNQUFNLENBQVk7UUFDbkMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7Ozs7O1FBcktsQyxTQUFJLEdBQWlCLEVBQUUsQ0FBQzs7OztRQWVmLHFCQUFnQixHQUFhLEVBQUUsQ0FBQzs7OztRQUtoQyxrQkFBYSxHQUFhLEVBQUUsQ0FBQzs7Ozs7UUFNdEMsZUFBVSxHQUFhLEVBQUUsQ0FBQzs7OztRQWUxQixlQUFVLEdBQWlDLEVBQUUsQ0FBQzs7OztRQWVyQyxvQkFBZSxHQUFHLEVBQUUsQ0FBQzs7Ozs7UUFXOUIsZUFBVSxHQUFHLFFBQVEsQ0FBQzs7OztRQUtkLG9CQUFlLEdBQUcsS0FBSyxDQUFDOzs7O1FBZXZCLGVBQVUsR0FBRyxLQUFLLENBQUM7Ozs7UUFlNUIsaUJBQVksR0FBRyxJQUFJLE9BQU8sRUFBVSxDQUFDOzs7O1FBaUI1QixnQkFBVyxHQUFHLElBQUksQ0FBQzs7OztRQVU1Qix5QkFBb0IsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDOzs7O1FBaUJqQyxhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQWMsQ0FBQzs7Ozs7UUFNcEQsaUJBQVksR0FBRyxJQUFJLENBQUM7Ozs7O1FBTXBCLGdCQUFXLEdBQUcsS0FBSyxDQUFDO0lBUWhCLENBQUM7SUEzREwsc0JBQUkseUNBQU07UUFIVjs7V0FFRzs7Ozs7UUFDSDtZQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN0QixDQUFDOzs7OztRQUNELFVBQ1csS0FBYTtZQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNoQyxDQUFDOzs7T0FMQTtJQXlCRCxzQkFBSSxpREFBYztRQUhsQjs7V0FFRzs7Ozs7UUFDSDtZQUNFLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUM5QixDQUFDOzs7OztRQUNELFVBQ21CLEtBQXFCO1lBQ3RDLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNuQyxDQUFDOzs7T0FMQTtJQWdDRDs7T0FFRzs7Ozs7SUFDSCx3Q0FBUTs7OztJQUFSO1FBQ0UsNkVBQTZFO1FBQzdFLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQztJQUMxRixDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gsK0NBQWU7Ozs7SUFBZjtRQUFBLGlCQWdDQztRQS9CQyx3Q0FBd0M7UUFDeEMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDOzs7WUFHVCxhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVk7YUFDdEMsSUFBSSxDQUNILFlBQVksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQzlCLG9CQUFvQixFQUFFLENBQ3ZCO1FBRUQsaUVBQWlFO1FBQ2pFLGFBQWEsQ0FBQyxTQUFTOzs7UUFBQztZQUN0QixLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7WUFDN0IsS0FBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7UUFDN0IsQ0FBQyxFQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUzs7O1FBQUM7WUFDbEMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1lBQzdCLEtBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO1FBQzdCLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBNUIsQ0FBNEIsRUFBQyxDQUFDO1FBRW5FLHVDQUF1QztRQUN2QyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUzs7O1lBQUM7Z0JBQzVCLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLFlBQVksRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQzlFLENBQUMsRUFBQyxDQUFDO1NBQ0o7UUFFRCw4REFBOEQ7UUFDOUQsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLGFBQWEsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUM7YUFDekYsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxPQUFPLEVBQUUsRUFBZCxDQUFjLEVBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gsa0RBQWtCOzs7O0lBQWxCO1FBQ0Usd0RBQXdEO1FBQ3hELCtEQUErRDtRQUMvRCx3QkFBd0I7UUFIMUIsaUJBc0NDO1FBakNDLDBDQUEwQztRQUMxQyxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUM7U0FDOUM7YUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqRCxNQUFNLElBQUksS0FBSyxDQUFDLDhCQUE0QixJQUFJLENBQUMsS0FBSyxNQUFHLENBQUMsQ0FBQztTQUM1RDtRQUVELDRCQUE0QjtRQUM1QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLHFEQUFxRCxDQUFDLENBQUM7U0FDeEU7UUFFRCwyREFBMkQ7UUFDM0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFoQixDQUFnQixFQUFDLENBQUM7OztZQUd2RSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUU7O1lBQ3hDLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFO1FBQ3RELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztnQkFDbEMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUc7Z0JBQ3ZCLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNqQixRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQzthQUMxQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0I7UUFFRCx1RkFBdUY7UUFDdkYsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN0QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNOzs7O1lBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFuQyxDQUFtQyxFQUFDLENBQUM7U0FDOUY7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0csdUNBQU87Ozs7SUFBYjs7Ozs7Ozs7d0JBRUksSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7d0JBRW5CLE9BQU8sR0FBd0IsRUFBRTs7d0JBR2pDLEtBQTBCLElBQUksQ0FBQyxTQUFTLEVBQXRDLFNBQVMsZUFBQSxFQUFFLFFBQVEsY0FBQTt3QkFDM0IsT0FBTyxDQUFDLE1BQU0sR0FBRyxTQUFTLEdBQUcsUUFBUSxDQUFDOzt3QkFHaEMsS0FBd0IsSUFBSSxDQUFDLElBQUksRUFBL0IsTUFBTSxZQUFBLEVBQUUsU0FBUyxlQUFBO3dCQUN6QixJQUFJLE1BQU0sRUFBRTs0QkFDVixPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsU0FBUyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxNQUFNLENBQUM7eUJBQzVEO3dCQUVELHNCQUFzQjt3QkFDdEIsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7NEJBQ2hDLFdBQVcsR0FBRyxhQUFhOztnQ0FDL0IsS0FBa0IsS0FBQSxpQkFBQSxJQUFJLENBQUMsWUFBWSxDQUFBLDRDQUFFO29DQUE1QixLQUFLO29DQUNaLFdBQVcsSUFBSSxPQUFLLEtBQUsscUNBQ1YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLHdDQUM1QyxDQUFBO2lDQUN2Qjs7Ozs7Ozs7OzRCQUNELFdBQVcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsQ0FBQzs0QkFDMUMsV0FBVyxJQUFJLElBQUksQ0FBQzs0QkFDcEIsT0FBTyxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUM7eUJBQzdCO3dCQUVELDZDQUE2Qzt3QkFDN0MsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7NEJBQ3hCLE9BQU8sQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDO3lCQUMxQjs7d0JBR2MscUJBQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsZUFDMUMsSUFBSSxDQUFDLEtBQUssc0JBQ1IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsb0JBQzVELElBQUksQ0FBQyxLQUFLLGNBQ2YsQ0FBQyxFQUFBOzt3QkFKSSxNQUFNLEdBQUcsU0FJYjt3QkFDSSxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQzt3QkFFMUMsd0RBQXdEO3dCQUN4RCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7NEJBQ3ZCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO3lCQUNqRDs2QkFBTTs0QkFDTCxtREFBbUQ7NEJBQ25ELG9DQUFvQzs0QkFDcEMsSUFBSSxTQUFTLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dDQUN0Qyx5QkFBeUI7Z0NBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO2dDQUNoRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxTQUFTLEdBQUcsQ0FBQyxDQUFDOzZCQUMxQztpQ0FBTTtnQ0FDTCw0Q0FBNEM7Z0NBQzVDLDREQUE0RDtnQ0FDNUQsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsRUFBRTtvQ0FDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7aUNBQ3REO2dDQUVELGtCQUFrQjtnQ0FDbEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7NkJBQ2xCO3lCQUNGO3dCQUVELElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDOzs7O3dCQUUxQixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUcsQ0FBQyxDQUFDOzs7Ozs7S0FFdEI7SUFFRDs7O09BR0c7Ozs7Ozs7SUFDSyw2Q0FBYTs7Ozs7O0lBQXJCLFVBQXNCLE9BQStCOztZQUMvQyxHQUFHLEdBQUcsRUFBRTtRQUNaLEtBQUssSUFBTSxVQUFVLElBQUksT0FBTyxFQUFFO1lBQ2hDLElBQUksVUFBVSxLQUFLLE9BQU8sSUFBSSxVQUFVLEtBQUssUUFBUSxFQUFFO2dCQUNyRCxHQUFHLElBQU8sVUFBVSxTQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUMsTUFBRyxDQUFDO2FBQ2hEO2lCQUFNO2dCQUNMLEdBQUcsSUFBTyxVQUFVLFdBQUssT0FBTyxDQUFDLFVBQVUsQ0FBQyxRQUFJLENBQUM7YUFDbEQ7U0FDRjtRQUNELE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7OztJQUNILHNDQUFNOzs7Ozs7O0lBQU4sVUFBTyxLQUFhLEVBQUUsSUFBWTtRQUNoQyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7OztJQUNILG1EQUFtQjs7Ozs7O0lBQW5CLFVBQW9CLEtBQWE7UUFDL0IsT0FBTyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCx3REFBd0I7Ozs7OztJQUF4QixVQUF5QixLQUFhO1FBQ3BDLE9BQU8sd0JBQXdCLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ25FLENBQUM7O2dCQXhZRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsZzRIQUE2Qzs7aUJBRTlDOzs7O2dEQXFMSSxNQUFNLFNBQUMsUUFBUTtnQkF2TVgsV0FBVzs7O3dCQXVCakIsS0FBSzt3QkFpQkwsS0FBSzs4QkFLTCxLQUFLO21DQUtMLEtBQUs7Z0NBS0wsS0FBSztpQ0FXTCxlQUFlLFNBQUMsaUJBQWlCO3FDQUtqQyxlQUFlLFNBQUMsaUJBQWlCLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFOzRCQVV4RCxTQUFTLFNBQUMsWUFBWSxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTsrQkFLeEMsS0FBSztrQ0FLTCxLQUFLO3VCQXFCTCxTQUFTLFNBQUMsT0FBTyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTs2QkFLbkMsS0FBSzs2QkFLTCxLQUFLOytCQUtMLEtBQUs7eUJBa0JMLEtBQUssU0FBQyxRQUFROzhCQVNkLEtBQUs7aUNBa0JMLEtBQUssU0FBQyxnQkFBZ0I7MkJBU3RCLE1BQU07O0lBbU9ULDRCQUFDO0NBQUEsQUExWUQsSUEwWUM7U0FyWVkscUJBQXFCOzs7Ozs7SUFJaEMsc0NBQXVCOzs7Ozs7SUFNdkIsdUNBQW1COzs7Ozs7SUFNbkIscUNBQXdCOzs7OztJQUt4QixzQ0FBdUI7Ozs7O0lBS3ZCLDRDQUE2Qzs7Ozs7SUFLN0MsaURBQXlDOzs7OztJQUt6Qyw4Q0FBc0M7Ozs7OztJQU10QywyQ0FBMEI7Ozs7OztJQUsxQiwrQ0FBeUY7Ozs7OztJQUt6RixtREFBbUg7Ozs7O0lBS25ILDJDQUE4Qzs7Ozs7O0lBSzlDLDBDQUEyRTs7Ozs7SUFLM0UsNkNBQThCOzs7OztJQUs5QixnREFBOEI7Ozs7O0lBSzlCLHlDQUFpQjs7Ozs7O0lBTWpCLDJDQUFzQjs7Ozs7O0lBS3RCLGdEQUFnQzs7Ozs7O0lBS2hDLHFDQUE0RDs7Ozs7SUFLNUQsMkNBQTRCOzs7OztJQUs1QiwyQ0FBNEI7Ozs7O0lBSzVCLDZDQUFnQzs7Ozs7O0lBS2hDLHdDQUF3Qjs7Ozs7SUFLeEIsNkNBQXFDOzs7OztJQWlCckMsNENBQTRCOzs7Ozs7SUFLNUIsZ0RBQXdDOzs7OztJQUt4QyxxREFBMkM7Ozs7O0lBaUIzQyx5Q0FBb0Q7Ozs7OztJQU1wRCw2Q0FBb0I7Ozs7OztJQU1wQiw0Q0FBb0I7O0lBTWxCLHVDQUEyQzs7Ozs7SUFDM0MsNENBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBPbkluaXQsXG4gIEFmdGVyVmlld0luaXQsXG4gIEFmdGVyQ29udGVudEluaXQsXG4gIElucHV0LFxuICBPdXRwdXQsXG4gIFZpZXdDaGlsZCxcbiAgQ29udGVudENoaWxkcmVuLFxuICBRdWVyeUxpc3QsXG4gIFRlbXBsYXRlUmVmLFxuICBFdmVudEVtaXR0ZXIsXG4gIEluamVjdFxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hdFBhZ2luYXRvciwgTWF0U29ydCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IERhdGFTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZS9kYXRhLnNlcnZpY2UnO1xuaW1wb3J0IHsgS2Vwc01vZGVscywgS2Vwc1NjaGVtYSwgR3JhcGhxbFJlc3VsdCwgS2Vwc09iamVjdCB9IGZyb20gJy4uLy4uLy4uL2ludGVyZmFjZSc7XG5pbXBvcnQgeyBTdWJqZWN0LCBtZXJnZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgZGVib3VuY2VUaW1lLCBkaXN0aW5jdFVudGlsQ2hhbmdlZCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IFRhYmxlQ29sRGlyZWN0aXZlLCBDdXN0b21Db2x1bW4gfSBmcm9tICcuLi90YWJsZS1jb2wuZGlyZWN0aXZlJztcbmltcG9ydCB7IGlzVHlwZSwgZ2V0UmVmZXJlbmNlRGlzcGxheSwgZ2V0UmVmZXJlbmNlRGlzcGxheUFycmF5IH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMnO1xuaW1wb3J0IF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCB7IGZpbHRlciB9IGZyb20gJ21pbmltYXRjaCc7XG5cbi8qKlxuICogQSBmdW5jdGlvbiB1c2VkIGJ5IHRoZSBkeW5hbWljIHRhYmxlIHRvIGZpbHRlciByZXN1bHQuXG4gKi9cbnR5cGUgRmlsdGVyRnVuY3Rpb24gPSAoZGF0YTogS2Vwc09iamVjdCkgPT4gYm9vbGVhbjtcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAna2Vwcy1keW5hbWljLXRhYmxlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2R5bmFtaWMtdGFibGUuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9keW5hbWljLXRhYmxlLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgRHluYW1pY1RhYmxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0LCBBZnRlckNvbnRlbnRJbml0IHtcbiAgLyoqXG4gICAqIE5hbWUgb2YgdGhlIG1vZGVsIGZvciB0aGUgdGFibGUuXG4gICAqL1xuICBASW5wdXQoKSBtb2RlbDogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBTY2hlbWEgb2YgdGhlIG1vZGVsLlxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcbiAgICovXG4gIHNjaGVtYTogS2Vwc1NjaGVtYTtcblxuICAvKipcbiAgICogRGF0YSB0byBiZSBzaG93biBvbiB0aGUgdGFibGUgZnJvbSB0aGUgR3JhcGhRTCBxdWVyeS5cbiAgICogQHJlYWRvbmx5XG4gICAqL1xuICBkYXRhOiBLZXBzT2JqZWN0W10gPSBbXTtcblxuICAvKipcbiAgICogQ29tbWEtc2VwYXJhdGVkIGxpc3Qgb2YgZmllbGRzIGZvciB0aGUgR3JhcGhRTCBxdWVyeS5cbiAgICovXG4gIEBJbnB1dCgpIHF1ZXJ5OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEFkZGl0aW9uYWwgZmlsdGVyIHRvIGJlIGFkZGVkIHRvIHRoZSBHcmFwaFFMIHF1ZXJ5LlxuICAgKi9cbiAgQElucHV0KCkgZmlsdGVyUXVlcnk6IFJlY29yZDxzdHJpbmcsIHN0cmluZz47XG5cbiAgLyoqXG4gICAqIENvbHVtbnMgdG8gYmUgZGlzcGxheWVkIG9uIHRoZSB0YWJsZS5cbiAgICovXG4gIEBJbnB1dCgpIGRpc3BsYXllZENvbHVtbnM6IHN0cmluZ1tdID0gW107XG5cbiAgLyoqXG4gICAqIENvbHVtbnMgdG8gYmUgaGlkZGVuIG9uIHRoZSB0YWJsZS5cbiAgICovXG4gIEBJbnB1dCgpIGhpZGRlbkNvbHVtbnM6IHN0cmluZ1tdID0gW107XG5cbiAgLyoqXG4gICAqIENvbHVtbnMgdG8gYmUgZGlzcGxheWVkIG9uIHRoZSB0YWJsZSBiYXNlZCBvbiBkaXNwbGF5ZWRDb2x1bW5zIGFuZCBoaWRkZW5Db2x1bW5zLlxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcbiAgICovXG4gIGFsbENvbHVtbnM6IHN0cmluZ1tdID0gW107XG5cbiAgLyoqXG4gICAqIFF1ZXJpZXMgYWxsIGBUYWJsZUNvbERpcmVjdGl2ZWAgZGF0YSBpbnNpZGUgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIEBDb250ZW50Q2hpbGRyZW4oVGFibGVDb2xEaXJlY3RpdmUpIHByaXZhdGUgY3VzdG9tQ29sRGF0YXM6IFF1ZXJ5TGlzdDxUYWJsZUNvbERpcmVjdGl2ZT47XG5cbiAgLyoqXG4gICAqIFF1ZXJpZXMgYWxsIGBUYWJsZUNvbERpcmVjdGl2ZWAgdGVtcGxhdGVzIGluc2lkZSB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgQENvbnRlbnRDaGlsZHJlbihUYWJsZUNvbERpcmVjdGl2ZSwgeyByZWFkOiBUZW1wbGF0ZVJlZiB9KSBwcml2YXRlIGN1c3RvbUNvbFRlbXBsYXRlczogUXVlcnlMaXN0PFRlbXBsYXRlUmVmPGFueT4+O1xuXG4gIC8qKlxuICAgKiBBIHJlY29yZCBvZiBjdXN0b20gY29sdW1uIGRhdGEgYW5kIHRlbXBsYXRlIGtleWVkIGJ5IHRoZSBjdXN0b20gY29sdW1uIElELlxuICAgKi9cbiAgY3VzdG9tQ29sczogUmVjb3JkPHN0cmluZywgQ3VzdG9tQ29sdW1uPiA9IHt9O1xuXG4gIC8qKlxuICAgKiBSZWZlcmVuY2UgdG8gdGhlIE1hdFBhZ2luYXRvci5cbiAgICovXG4gIEBWaWV3Q2hpbGQoTWF0UGFnaW5hdG9yLCB7IHN0YXRpYzogdHJ1ZSB9KSBwcml2YXRlIHBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xuXG4gIC8qKlxuICAgKiBBIGtleSB1c2VkIHdoZW4gc2F2aW5nIHBhZ2Ugc2l6ZSBudW1iZXIgdG8gdGhlIGxvY2FsIHN0b3JhZ2UuXG4gICAqL1xuICBASW5wdXQoKSBzYXZlUGFnZVNpemU6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIHRhYmxlJ3MgZGVmYXVsdCBwYWdpbmF0b3IgcGFnZSBzaXplLlxuICAgKi9cbiAgQElucHV0KCkgZGVmYXVsdFBhZ2VTaXplID0gMTA7XG5cbiAgLyoqXG4gICAqIFRoZSBjdXJyZW50IHBhZ2Ugc2l6ZSBvbiBwYWdpbmF0b3IuXG4gICAqL1xuICBwYWdlU2l6ZTogbnVtYmVyO1xuXG4gIC8qKlxuICAgKiBMZW5ndGggb2YgdGhlIGRhdGEgdG8gYmUgc2hvd24gb24gdGhlIHBhZ2luYXRvci5cbiAgICogQHJlYWRvbmx5XG4gICAqL1xuICBkYXRhTGVuZ3RoID0gSW5maW5pdHk7XG5cbiAgLyoqXG4gICAqIFdoZXRoZXIgdGhlIGRhdGEgbGVuZ3RoIGhhcyBiZWVuIGtub3duIChmcm9tIGxhc3QgcXVlcnkpIG9yIG5vdC5cbiAgICovXG4gIHByaXZhdGUgaXNEYXRhTGVuZ3RoU2V0ID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIFJlZmVyZW5jZSB0byB0aGUgTWF0U29ydC5cbiAgICovXG4gIEBWaWV3Q2hpbGQoTWF0U29ydCwgeyBzdGF0aWM6IHRydWUgfSkgcHJpdmF0ZSBzb3J0OiBNYXRTb3J0O1xuXG4gIC8qKlxuICAgKiBUaGUgSUQgb2YgdGhlIGNvbHVtbiB0aGF0IHNob3VsZCBiZSBzb3J0ZWQuXG4gICAqL1xuICBASW5wdXQoKSBhY3RpdmVTb3J0OiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFdoZXRoZXIgdGhlIHRhYmxlIHNob3VsZCBzaG93IHRoZSBkZWZhdWx0IHNlYXJjaCBib3ggb3Igbm90LlxuICAgKi9cbiAgQElucHV0KCkgc2hvd1NlYXJjaCA9IGZhbHNlO1xuXG4gIC8qKlxuICAgKiBGaWVsZCB1c2VkIGZvciB0aGUgc2VhcmNoLlxuICAgKi9cbiAgQElucHV0KCkgc2VhcmNoRmllbGRzOiBzdHJpbmdbXTtcblxuICAvKipcbiAgICogU3RyaW5nIHRvIGJlIHNlYXJjaGVkIGJhc2VkIG9uIGBzZWFyY2hGaWVsZHNgLlxuICAgKi9cbiAgcHJpdmF0ZSBfc2VhcmNoOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIEEgc3ViamVjdCB0aGF0IGVtaXRzIGV2ZXJ5IHRpbWUgdGhlIHNlYXJjaCBzdHJpbmcgY2hhbmdlcy5cbiAgICovXG4gIHNlYXJjaENoYW5nZSA9IG5ldyBTdWJqZWN0PHN0cmluZz4oKTtcblxuICAvKipcbiAgICogU3RyaW5nIHRvIGJlIHNlYXJjaGVkIGJhc2VkIG9uIGBzZWFyY2hGaWVsZHNgLlxuICAgKi9cbiAgZ2V0IHNlYXJjaCgpOiBzdHJpbmcge1xuICAgIHJldHVybiB0aGlzLl9zZWFyY2g7XG4gIH1cbiAgQElucHV0KCdzZWFyY2gnKVxuICBzZXQgc2VhcmNoKHZhbHVlOiBzdHJpbmcpIHtcbiAgICB0aGlzLl9zZWFyY2ggPSB2YWx1ZTtcbiAgICB0aGlzLnNlYXJjaENoYW5nZS5uZXh0KHZhbHVlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBEZWxheSBmb3IgdGhlIHNlYXJjaCBmdW5jdGlvbiB0byBiZSBjYWxsZWQgaW4gbWlsbGlzZWNvbmRzLlxuICAgKi9cbiAgQElucHV0KCkgc2VhcmNoRGVsYXkgPSAxMDAwO1xuXG4gIC8qKlxuICAgKiBGdW5jdGlvbiB0byBmaWx0ZXIgdGhlIHJlc3VsdCBhZnRlciBxdWVyeS5cbiAgICovXG4gIHByaXZhdGUgX2ZpbHRlckZ1bmN0aW9uOiBGaWx0ZXJGdW5jdGlvbjtcblxuICAvKipcbiAgICogQSBzdWJqZWN0IHRoYXQgZW1pdHMgZXZlcnkgdGltZSB0aGUgZmlsdGVyIGZ1bmN0aW9uIGNoYW5nZXMuXG4gICAqL1xuICBmaWx0ZXJGdW5jdGlvbkNoYW5nZSA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XG5cbiAgLyoqXG4gICAqIEZ1bmN0aW9uIHRvIGZpbHRlciB0aGUgcmVzdWx0IGFmdGVyIHF1ZXJ5LlxuICAgKi9cbiAgZ2V0IGZpbHRlckZ1bmN0aW9uKCk6IEZpbHRlckZ1bmN0aW9uIHtcbiAgICByZXR1cm4gdGhpcy5fZmlsdGVyRnVuY3Rpb247XG4gIH1cbiAgQElucHV0KCdmaWx0ZXJGdW5jdGlvbicpXG4gIHNldCBmaWx0ZXJGdW5jdGlvbih2YWx1ZTogRmlsdGVyRnVuY3Rpb24pIHtcbiAgICB0aGlzLl9maWx0ZXJGdW5jdGlvbiA9IHZhbHVlO1xuICAgIHRoaXMuZmlsdGVyRnVuY3Rpb25DaGFuZ2UubmV4dCgpO1xuICB9XG5cbiAgLyoqXG4gICAqIEFuIGV2ZW50IGVtaXR0ZXIgdGhhdCBlbWl0cyB0aGUgb2JqZWN0IG9mIHRoZSByb3cgY2xpY2tlZCBieSB0aGUgdXNlci5cbiAgICovXG4gIEBPdXRwdXQoKSByb3dDbGljayA9IG5ldyBFdmVudEVtaXR0ZXI8S2Vwc09iamVjdD4oKTtcblxuICAvKipcbiAgICogV2hldGhlciB0aGUgdGFibGUgaXMgY3VycmVudGx5IGxvYWRpbmcgb3Igbm90LlxuICAgKiBAcmVhZG9ubHlcbiAgICovXG4gIHRhYmxlTG9hZGluZyA9IHRydWU7XG5cbiAgLyoqXG4gICAqIFdoZXRoZXIgdGhlIHRhYmxlIGhhcyBkb25lIGluaXRpYWxpemluZyBvciBub3QuXG4gICAqIEByZWFkb25seVxuICAgKi9cbiAgaW5pdGlhbGl6ZWQgPSBmYWxzZTtcblxuICAvKipcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXG4gICAqL1xuICBjb25zdHJ1Y3RvcihcbiAgICBASW5qZWN0KCdNT0RFTFMnKSBwdWJsaWMgbW9kZWxzOiBLZXBzTW9kZWxzLFxuICAgIHByaXZhdGUgZGF0YVNlcnZpY2U6IERhdGFTZXJ2aWNlXG4gICkgeyB9XG5cbiAgLyoqXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxuICAgKi9cbiAgbmdPbkluaXQoKSB7XG4gICAgLy8gR2V0IHRoZSBwcmV2aW91cyBzZXNzaW9uJ3MgcGFnZSBzaXplIGlmIGFueSwgb3IgdXNlIHRoZSBkZWZhdWx0IHBhZ2Ugc2l6ZS5cbiAgICB0aGlzLnBhZ2VTaXplID0gTnVtYmVyKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMuc2F2ZVBhZ2VTaXplKSkgfHwgdGhpcy5kZWZhdWx0UGFnZVNpemU7XG4gIH1cblxuICAvKipcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXG4gICAqL1xuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgLy8gR2V0IGRhdGEgYXMgc29vbiBhcyB2aWV3IGluaXRpYWxpemVkLlxuICAgIHRoaXMuZ2V0RGF0YSgpO1xuXG4gICAgLy8gU2V0dXAgZGVsYXllZCBzZWFyY2guXG4gICAgY29uc3QgZGVsYXllZFNlYXJjaCA9IHRoaXMuc2VhcmNoQ2hhbmdlXG4gICAgLnBpcGUoXG4gICAgICBkZWJvdW5jZVRpbWUodGhpcy5zZWFyY2hEZWxheSksXG4gICAgICBkaXN0aW5jdFVudGlsQ2hhbmdlZCgpXG4gICAgKTtcblxuICAgIC8vIElmIGFueSBvZiB0aGVzZSBvYnNlcnZhYmxlcyBlbWl0LCB0aGVuIHdlIHJlc2V0IHRoZSBwYWdpbmF0b3IuXG4gICAgZGVsYXllZFNlYXJjaC5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgdGhpcy5wYWdpbmF0b3IucGFnZUluZGV4ID0gMDtcbiAgICAgIHRoaXMuZGF0YUxlbmd0aCA9IEluZmluaXR5O1xuICAgIH0pO1xuICAgIHRoaXMuZmlsdGVyRnVuY3Rpb25DaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgIHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCA9IDA7XG4gICAgICB0aGlzLmRhdGFMZW5ndGggPSBJbmZpbml0eTtcbiAgICB9KTtcbiAgICB0aGlzLnNvcnQuc29ydENoYW5nZS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5wYWdpbmF0b3IucGFnZUluZGV4ID0gMCk7XG5cbiAgICAvLyBTYXZlIHBhZ2Ugc2l6ZSBpZiBwYWdpbmF0b3IgY2hhbmdlcy5cbiAgICBpZiAodGhpcy5zYXZlUGFnZVNpemUpIHtcbiAgICAgIHRoaXMucGFnaW5hdG9yLnBhZ2Uuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy5zYXZlUGFnZVNpemUsIHRoaXMucGFnaW5hdG9yLnBhZ2VTaXplLnRvU3RyaW5nKCkpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gSWYgYW55IG9mIHRoZXNlIG9ic2VydmFibGVzIGVtaXQsIHRoZW4gd2UgcmVmcmVzaCB0aGUgZGF0YS5cbiAgICBtZXJnZSh0aGlzLnNvcnQuc29ydENoYW5nZSwgdGhpcy5wYWdpbmF0b3IucGFnZSwgZGVsYXllZFNlYXJjaCwgdGhpcy5maWx0ZXJGdW5jdGlvbkNoYW5nZSlcbiAgICAuc3Vic2NyaWJlKCgpID0+IHRoaXMuZ2V0RGF0YSgpKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcbiAgICovXG4gIG5nQWZ0ZXJDb250ZW50SW5pdCgpIHtcbiAgICAvLyBXZSBhcmUgdXNpbmcgYG5nQWZ0ZXJDb250ZW50SW5pdGAgaGVyZSBiZWNhdXNlIHdlIGNhblxuICAgIC8vIG9ubHkgZ2V0IHRoZSBjdXN0b20gY29sdW1ucyBkYXRhIGFmdGVyIHRoZSBjb21wb25lbnQgY29udGVudFxuICAgIC8vIGhhcyBiZWVuIGluaXRpYWxpemVkLlxuXG4gICAgLy8gR2V0IHNjaGVtYSBmcm9tIG1vZGVsIG5hbWUgaWYgc3VwcGxpZWQuXG4gICAgaWYgKHRoaXMubW9kZWwgJiYgdGhpcy5tb2RlbHNbdGhpcy5tb2RlbF0pIHtcbiAgICAgIHRoaXMuc2NoZW1hID0gdGhpcy5tb2RlbHNbdGhpcy5tb2RlbF0uc2NoZW1hO1xuICAgIH0gZWxzZSBpZiAodGhpcy5tb2RlbCAmJiAhdGhpcy5tb2RlbHNbdGhpcy5tb2RlbF0pIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihgQ2FuJ3QgZmluZCBhIG1vZGVsIG5hbWVkICR7dGhpcy5tb2RlbH0hYCk7XG4gICAgfVxuXG4gICAgLy8gQXNzZXJ0IHNjaGVtYSBtdXN0IGV4aXN0LlxuICAgIGlmICghdGhpcy5zY2hlbWEpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihgTXVzdCBzdXBwbHkgdmFsaWQgbW9kZWwgbmFtZSB0byBLZXBzIGR5bmFtaWMgdGFibGUhYCk7XG4gICAgfVxuXG4gICAgLy8gU2V0IGBhbGxDb2x1bW5zYCB0byBiZSBldmVyeSBwdWJsaWMgZmllbGQgaW4gdGhlIHNjaGVtYS5cbiAgICB0aGlzLmFsbENvbHVtbnMgPSBPYmplY3Qua2V5cyh0aGlzLnNjaGVtYSkuZmlsdGVyKGZpZWxkID0+IGZpZWxkWzBdICE9PSAnXycpO1xuICAgIFxuICAgIC8vIFByb2Nlc3MgY3VzdG9tIGNvbHVtbnMuXG4gICAgY29uc3QgY29sRGF0YXMgPSB0aGlzLmN1c3RvbUNvbERhdGFzLnRvQXJyYXkoKTtcbiAgICBjb25zdCBjb2xUZW1wbGF0ZXMgPSB0aGlzLmN1c3RvbUNvbFRlbXBsYXRlcy50b0FycmF5KCk7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjb2xEYXRhcy5sZW5ndGg7IGkrKykge1xuICAgICAgY29uc3QgY29sSWQgPSBjb2xEYXRhc1tpXS5pZDtcbiAgICAgIHRoaXMuY3VzdG9tQ29sc1tjb2xJZF0gPSB7XG4gICAgICAgIGRhdGE6IGNvbERhdGFzW2ldLFxuICAgICAgICB0ZW1wbGF0ZTogY29sVGVtcGxhdGVzW2ldXG4gICAgICB9O1xuICAgICAgdGhpcy5hbGxDb2x1bW5zLnB1c2goY29sSWQpO1xuICAgIH1cbiAgICBcbiAgICAvLyBJZiBgZGlzcGxheWVkQ29sdW1uc2AgaXMgbm90IHByb3ZpZGVkLCB1c2UgYGFsbENvbHVtbnNgIGZpbHRlcmVkIGJ5IGBoaWRkZW5Db2x1bW5zYC5cbiAgICBpZiAodGhpcy5kaXNwbGF5ZWRDb2x1bW5zLmxlbmd0aCA9PT0gMCkge1xuICAgICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gdGhpcy5hbGxDb2x1bW5zLmZpbHRlcihmaWVsZCA9PiAhdGhpcy5oaWRkZW5Db2x1bW5zLmluY2x1ZGVzKGZpZWxkKSk7XG4gICAgfVxuXG4gICAgdGhpcy5pbml0aWFsaXplZCA9IHRydWU7XG4gIH1cblxuICAvKipcbiAgICogQ29tYmluZXMgcHJvcGVydGllcyBhcyBHcmFwaFFMIHF1ZXJ5IGFuZCBnZXRzIGRhdGEgZnJvbSBkYXRhIHNlcnZpY2UuXG4gICAqL1xuICBhc3luYyBnZXREYXRhKCkge1xuICAgIHRyeSB7XG4gICAgICB0aGlzLnRhYmxlTG9hZGluZyA9IHRydWU7XG5cbiAgICAgIGNvbnN0IGZpbHRlcnM6IFJlY29yZDxzdHJpbmcsIGFueT4gPSB7fTtcblxuICAgICAgLy8gU2V0dXAgcGFnZSBxdWVyeS5cbiAgICAgIGNvbnN0IHsgcGFnZUluZGV4LCBwYWdlU2l6ZSB9ID0gdGhpcy5wYWdpbmF0b3I7XG4gICAgICBmaWx0ZXJzLm9mZnNldCA9IHBhZ2VJbmRleCAqIHBhZ2VTaXplO1xuXG4gICAgICAvLyBTZXR1cCBzb3J0IHF1ZXJ5LlxuICAgICAgY29uc3QgeyBhY3RpdmUsIGRpcmVjdGlvbiB9ID0gdGhpcy5zb3J0O1xuICAgICAgaWYgKGFjdGl2ZSkge1xuICAgICAgICBmaWx0ZXJzLnNvcnQgPSAoZGlyZWN0aW9uID09PSAnZGVzYycgPyAnLScgOiAnKycpICsgYWN0aXZlO1xuICAgICAgfVxuXG4gICAgICAvLyBTZXR1cCBzZWFyY2ggcXVlcnkuXG4gICAgICBpZiAodGhpcy5zZWFyY2hGaWVsZHMgJiYgdGhpcy5zZWFyY2gpIHtcbiAgICAgICAgbGV0IG1vbmdvRmlsdGVyID0gYCB7ICckb3InOiBbYDtcbiAgICAgICAgZm9yIChsZXQgZmllbGQgb2YgdGhpcy5zZWFyY2hGaWVsZHMpIHtcbiAgICAgICAgICBtb25nb0ZpbHRlciArPSBgeycke2ZpZWxkfSc6IHtcbiAgICAgICAgICAgICckcmVnZXgnOiAnJHt0aGlzLnNlYXJjaC5yZXBsYWNlKCcoJywgJyUyOCcpLnJlcGxhY2UoJyknLCAnJTI5Jyl9JyxcbiAgICAgICAgICAgICckb3B0aW9ucyc6ICdpJyB9fSxgXG4gICAgICAgIH1cbiAgICAgICAgbW9uZ29GaWx0ZXIgPSBfLnRyaW1FbmQobW9uZ29GaWx0ZXIsICcsJyk7XG4gICAgICAgIG1vbmdvRmlsdGVyICs9ICddfSc7XG4gICAgICAgIGZpbHRlcnMubW9uZ28gPSBtb25nb0ZpbHRlcjtcbiAgICAgIH1cdFxuXG4gICAgICAvLyBTZXQgbGltaXQgaWYgdGhlcmUgaXMgbm90IGZpbHRlciBmdW5jdGlvbi5cbiAgICAgIGlmICghdGhpcy5maWx0ZXJGdW5jdGlvbikge1xuICAgICAgICBmaWx0ZXJzLmxpbWl0ID0gcGFnZVNpemU7XG4gICAgICB9XG5cbiAgICAgIC8vIEdldCBkYXRhIGZyb20gZGF0YSBzZXJ2aWNlLlxuICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgdGhpcy5kYXRhU2VydmljZS5ncmFwaHFsKGBcbiAgICAgICAgJHt0aGlzLm1vZGVsfXMoXG4gICAgICAgICAgJHt0aGlzLmNvbnZlcnRGaWx0ZXIoT2JqZWN0LmFzc2lnbihmaWx0ZXJzLCB0aGlzLmZpbHRlclF1ZXJ5KSl9XG4gICAgICAgICl7JHt0aGlzLnF1ZXJ5fX1cbiAgICAgIGApO1xuICAgICAgY29uc3QgZGF0YSA9IHJlc3VsdC5kYXRhW3RoaXMubW9kZWwgKyAncyddO1xuXG4gICAgICAvLyBJZiBmaWx0ZXIgZnVuY3Rpb24gZXhpc3RzLCBmaWx0ZXIgd2l0aCB0aGF0IGZ1bmN0aW9uLlxuICAgICAgaWYgKHRoaXMuZmlsdGVyRnVuY3Rpb24pIHtcbiAgICAgICAgdGhpcy5kYXRhID0gXy5maWx0ZXIoZGF0YSwgdGhpcy5maWx0ZXJGdW5jdGlvbik7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBJZiB0aGlzIGlzIG5vdCB0aGUgZmlyc3QgcGFnZSBhbmQgZGF0YSBpcyBlbXB0eSxcbiAgICAgICAgLy8gdGhlbiB3ZSBnbyBiYWNrIHRvIHRoZSBsYXN0IHBhZ2UuXG4gICAgICAgIGlmIChwYWdlSW5kZXggPiAxICYmIGRhdGEubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgLy8gQ2FsY3VsYXRlIGRhdGEgbGVuZ3RoLlxuICAgICAgICAgIHRoaXMuZGF0YUxlbmd0aCA9IChwYWdlSW5kZXggLSAxKSAqIHBhZ2VTaXplICsgdGhpcy5kYXRhLmxlbmd0aDtcbiAgICAgICAgICB0aGlzLnBhZ2luYXRvci5wYWdlSW5kZXggPSBwYWdlSW5kZXggLSAxO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIElmIGRhdGEgbGVuZ3RoIGlzIHNtYWxsZXIgdGhhbiBwYWdlIHNpemUsXG4gICAgICAgICAgLy8gd2UgYXNzdW1lIHdlJ3JlIG9uIGxhc3QgcGFnZSBhbmQgcmVjYWxjdWxhdGUgZGF0YSBsZW5ndGguXG4gICAgICAgICAgaWYgKGRhdGEubGVuZ3RoIDwgcGFnZVNpemUpIHtcbiAgICAgICAgICAgIHRoaXMuZGF0YUxlbmd0aCA9IHBhZ2VJbmRleCAqIHBhZ2VTaXplICsgZGF0YS5sZW5ndGg7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLy8gU2V0IGRhdGEgYXMgaXMuXG4gICAgICAgICAgdGhpcy5kYXRhID0gZGF0YTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB0aGlzLnRhYmxlTG9hZGluZyA9IGZhbHNlO1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBDb252ZXJ0cyBhIGZpbHRlciBvYmplY3QgdG8gTW9uZ28gZmlsdGVyIHN0cmluZy5cbiAgICogQHBhcmFtIGZpbHRlcnMgUGFpciBvZiBrZXlzICYgdmFsdWVzIHRvIGJlIGNvbnZlcnRlZCB0byBNb25nbyBmaWx0ZXIuXG4gICAqL1xuICBwcml2YXRlIGNvbnZlcnRGaWx0ZXIoZmlsdGVyczogUmVjb3JkPHN0cmluZywgc3RyaW5nPik6IHN0cmluZyB7XG4gICAgbGV0IHN0ciA9ICcnO1xuICAgIGZvciAoY29uc3QgZmlsdGVyTmFtZSBpbiBmaWx0ZXJzKSB7XG4gICAgICBpZiAoZmlsdGVyTmFtZSA9PT0gJ2xpbWl0JyB8fCBmaWx0ZXJOYW1lID09PSAnb2Zmc2V0Jykge1xuICAgICAgICBzdHIgKz0gYCR7ZmlsdGVyTmFtZX06JHtmaWx0ZXJzW2ZpbHRlck5hbWVdfSxgO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc3RyICs9IGAke2ZpbHRlck5hbWV9OlwiJHtmaWx0ZXJzW2ZpbHRlck5hbWVdfVwiLGA7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBzdHI7XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2tzIHRoZSB0eXBlIG9mIGEgZmllbGQuXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxuICAgKiBAcGFyYW0gZmllbGQgRmllbGQgdG8gYmUgY2hlY2tlZC5cbiAgICogQHBhcmFtIHR5cGUgVHlwZSB0byBiZSBjaGVja2VkLlxuICAgKi9cbiAgaXNUeXBlKGZpZWxkOiBzdHJpbmcsIHR5cGU6IHN0cmluZykge1xuICAgIHJldHVybiBpc1R5cGUodGhpcy5zY2hlbWEsIGZpZWxkLCB0eXBlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBkaXNwbGF5IGV4cHJlc3Npb24gb2YgYSByZWZlcmVuY2UgZmllbGQuXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxuICAgKiBAcGFyYW0gZmllbGQgRmllbGQgdG8gZ2V0IGZyb20uXG4gICAqL1xuICBnZXRSZWZlcmVuY2VEaXNwbGF5KGZpZWxkOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gZ2V0UmVmZXJlbmNlRGlzcGxheSh0aGlzLm1vZGVscywgdGhpcy5zY2hlbWEsIGZpZWxkKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIHRoZSBkaXNwbGF5IGV4cHJlc3Npb24gb2YgYW4gYXJyYXkgb2YgcmVmZXJlbmNlcyBmaWVsZC5cbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXG4gICAqIEBwYXJhbSBmaWVsZCBGaWVsZCB0byBnZXQgZnJvbS5cbiAgICovXG4gIGdldFJlZmVyZW5jZURpc3BsYXlBcnJheShmaWVsZDogc3RyaW5nKSB7XG4gICAgcmV0dXJuIGdldFJlZmVyZW5jZURpc3BsYXlBcnJheSh0aGlzLm1vZGVscywgdGhpcy5zY2hlbWEsIGZpZWxkKTtcbiAgfVxuXG59XG4iXX0=