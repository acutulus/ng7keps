/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter, ViewChild, ContentChildren, QueryList, TemplateRef, IterableDiffers, Inject } from '@angular/core';
import { MatTable, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { TableColDirective } from '../table-col.directive';
import { EvalPipe } from '../../../pipe/eval.pipe';
import { isType, getReferenceDisplay, getReferenceDisplayArray } from '../../../utils';
var TableComponent = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function TableComponent(models, iterableDiffers) {
        this.models = models;
        this.iterableDiffers = iterableDiffers;
        /**
         * Data to be shown on the table.
         */
        this.data = [];
        /**
         * Columns to be displayed on the table.
         */
        this.displayedColumns = [];
        /**
         * Columns to be hidden on the table.
         */
        this.hiddenColumns = [];
        /**
         * Custom function to filter the data source
         * See example below:
         * <pre>
         *  customFilter(Data: T, Filter: string): boolean {
         *     return <true if Data matches filter>
         *  }
         * <pre>
         *
         * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
         */
        this.filterPredicate = null;
        /**
         * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
         * \@internal Do not use!
         */
        this.allColumns = [];
        /**
         * A record of custom column data and template keyed by the custom column ID.
         */
        this.customCols = {};
        /**
         * The table's default paginator page size.
         */
        this.defaultPageSize = 10;
        /**
         * Whether the table should display sort on headers or not.
         */
        this.showSort = false;
        /**
         * Whether the table should show the default search box or not.
         */
        this.showSearch = false;
        /**
         * The search query to be used to filter the data.
         */
        this.search = '';
        /**
         * An event emitter that emits the object of the row clicked by the user.
         */
        this.rowClick = new EventEmitter();
        /**
         * Whether the table has done initializing or not.
         * \@readonly
         */
        this.initialized = false;
        this.diff = this.iterableDiffers.find([]).create(null);
    }
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    TableComponent.prototype.ngOnInit = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        var _this = this;
        // Initialize the data source.
        this.dataSource = new MatTableDataSource(this.data);
        // Initialize the paginator.
        if (this.showPaginator) {
            this.dataSource.paginator = this.paginator;
            // Get the previous session's page size if any, or use the default page size.
            this.pageSize = Number(localStorage.getItem(this.savePageSize)) || this.defaultPageSize;
        }
        // Initialize the sort.
        if (this.showSort) {
            this.dataSource.sort = this.sort;
            // Modify the data source's `sortingDataAccessor` to use `EvalPipe` for custom columns.
            this.evalPipe = new EvalPipe();
            this.dataSource.sortingDataAccessor = (/**
             * @param {?} data
             * @param {?} sortHeaderId
             * @return {?}
             */
            function (data, sortHeaderId) {
                /** @type {?} */
                var customCol = _this.customCols[sortHeaderId];
                if (customCol && customCol.data.sortAccessor) {
                    return _this.evalPipe.transform(data, customCol.data.sortAccessor);
                }
                return data[sortHeaderId];
            });
        }
        this.setFilterPredicate(this.filterPredicate);
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    TableComponent.prototype.ngAfterContentInit = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        // We are using `ngAfterContentInit` here because we can
        // only get the custom columns data after the component content
        // has been initialized.
        var _this = this;
        // Get schema from model name if supplied.
        if (this.model && this.models[this.model]) {
            this.schema = this.models[this.model].schema;
        }
        else if (this.model && !this.models[this.model]) {
            throw new Error("Can't find a model named " + this.model + "!");
        }
        // Assert schema must exist.
        if (!this.schema) {
            throw new Error("Must supply valid model name or schema to Keps table!");
        }
        // Set `allColumns` to be every public field in the schema.
        this.allColumns = Object.keys(this.schema).filter((/**
         * @param {?} field
         * @return {?}
         */
        function (field) { return field[0] !== '_'; }));
        // Process custom columns.
        /** @type {?} */
        var colDatas = this.customColDatas.toArray();
        /** @type {?} */
        var colTemplates = this.customColTemplates.toArray();
        for (var i = 0; i < colDatas.length; i++) {
            /** @type {?} */
            var colId = colDatas[i].id;
            this.customCols[colId] = {
                data: colDatas[i],
                template: colTemplates[i]
            };
            this.allColumns.push(colId);
        }
        // If `displayedColumns` is not provided, use `allColumns` filtered by `hiddenColumns`.
        if (this.displayedColumns.length === 0) {
            this.displayedColumns = this.allColumns.filter((/**
             * @param {?} field
             * @return {?}
             */
            function (field) { return !_this.hiddenColumns.includes(field); }));
        }
        this.initialized = true;
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @return {?}
     */
    TableComponent.prototype.ngDoCheck = /**
     * \@internal Do not use!
     * @return {?}
     */
    function () {
        // This will refresh the table data source and rerender
        // the table if there is any change to the data.
        if (this.initialized) {
            /** @type {?} */
            var changes = this.diff.diff(this.data);
            if (changes) {
                this.dataSource.data = this.data;
                this.table.renderRows();
            }
        }
    };
    /**
     * @internal Do not use!
     */
    /**
     * \@internal Do not use!
     * @param {?} changes
     * @return {?}
     */
    TableComponent.prototype.ngOnChanges = /**
     * \@internal Do not use!
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        // This check if the `search` property has changed and apply search if it does.
        if (this.initialized && changes.search) {
            this.applySearch(this.search);
        }
    };
    /**
     * Saves current page size to local storage if property `savePageSize` is supplied.
     * @internal Do not use!
     * @param event Event from paginator.
     */
    /**
     * Saves current page size to local storage if property `savePageSize` is supplied.
     * \@internal Do not use!
     * @param {?} event Event from paginator.
     * @return {?}
     */
    TableComponent.prototype.onPageChange = /**
     * Saves current page size to local storage if property `savePageSize` is supplied.
     * \@internal Do not use!
     * @param {?} event Event from paginator.
     * @return {?}
     */
    function (event) {
        if (this.savePageSize) {
            localStorage.setItem(this.savePageSize, event.pageSize.toString());
        }
    };
    /**
     * Applies a search query to filter the data.
     * @param search Query for the search.
     */
    /**
     * Applies a search query to filter the data.
     * @param {?} search Query for the search.
     * @return {?}
     */
    TableComponent.prototype.applySearch = /**
     * Applies a search query to filter the data.
     * @param {?} search Query for the search.
     * @return {?}
     */
    function (search) {
        this.dataSource.filter = search.trim().toLowerCase();
    };
    /**
     * Checks the type of a field.
     * @internal Do not use!
     * @param field Field to be checked.
     * @param type Type to be checked.
     */
    /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    TableComponent.prototype.isType = /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    function (field, type) {
        return isType(this.schema, field, type);
    };
    /**
     * Returns the display expression of a reference field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    TableComponent.prototype.getReferenceDisplay = /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    function (field) {
        return getReferenceDisplay(this.models, this.schema, field);
    };
    /**
     * Returns the display expression of an array of references field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    /**
     * Returns the display expression of an array of references field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    TableComponent.prototype.getReferenceDisplayArray = /**
     * Returns the display expression of an array of references field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    function (field) {
        return getReferenceDisplayArray(this.models, this.schema, field);
    };
    /**
     * Set the filter predicate of data source.
     * this function accepts a function as a parameter.
     * See example below:
     * <pre>
     *  customFilter(Data: T, Filter: string): boolean {
     *     return <true if Data matches filter>
     *  }
     * <pre>
     * If no funtion is provided, this function will use the default
     * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
     * @param filterPredicateFunction custom filterPredicateFunction that follows the above format
     */
    /**
     * Set the filter predicate of data source.
     * this function accepts a function as a parameter.
     * See example below:
     * <pre>
     *  customFilter(Data: T, Filter: string): boolean {
     *     return <true if Data matches filter>
     *  }
     * <pre>
     * If no funtion is provided, this function will use the default
     * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
     * @param {?=} filterPredicateFunction custom filterPredicateFunction that follows the above format
     * @return {?}
     */
    TableComponent.prototype.setFilterPredicate = /**
     * Set the filter predicate of data source.
     * this function accepts a function as a parameter.
     * See example below:
     * <pre>
     *  customFilter(Data: T, Filter: string): boolean {
     *     return <true if Data matches filter>
     *  }
     * <pre>
     * If no funtion is provided, this function will use the default
     * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
     * @param {?=} filterPredicateFunction custom filterPredicateFunction that follows the above format
     * @return {?}
     */
    function (filterPredicateFunction) {
        if (filterPredicateFunction === void 0) { filterPredicateFunction = null; }
        if (filterPredicateFunction) {
            this.dataSource.filterPredicate = filterPredicateFunction;
        }
    };
    TableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'keps-table',
                    template: "<!-- Table's Search Box -->\r\n<mat-form-field\r\n  *ngIf=\"showSearch\"\r\n  floatLabel=\"never\"\r\n  class=\"keps-table-search\"\r\n>\r\n  <input\r\n    matInput\r\n    (keyup)=\"applySearch($event.target.value)\"\r\n    placeholder=\"Search\"\r\n  >\r\n</mat-form-field>\r\n\r\n<!-- Table -->\r\n<table\r\n  mat-table [dataSource]=\"dataSource\"\r\n  matSort [matSortDisabled]=\"!showSort\" [matSortActive]=\"activeSort\" matSortDirection=\"asc\"\r\n  class=\"keps-table\"\r\n>\r\n  <!-- Table Columns from Schema -->\r\n  <ng-container\r\n    *ngFor=\"let fieldName of schema | keys\"\r\n    [matColumnDef]=\"fieldName\"\r\n  >\r\n    <ng-container *ngIf=\"schema[fieldName]; let field\">\r\n      <th mat-header-cell *matHeaderCellDef mat-sort-header [ngClass]=\"'keps-table-col-' + fieldName\" >\r\n        {{ (field.label ? field.label : '') }}{{ (field.label ?  '' : fieldName) | splitCamel | titlecase }}\r\n      </th>\r\n      <td \r\n        mat-cell *matCellDef=\"let element\" \r\n        class=\"keps-table-cell\"\r\n        [ngClass]=\"'keps-table-col-' + fieldName\"\r\n      >\r\n        <ng-container *ngIf=\"element[fieldName]; let fieldData\">\r\n          <span *ngIf=\"isType(fieldName, 'reference')\">\r\n            {{ fieldData | eval:getReferenceDisplay(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'address')\">\r\n            {{ fieldData | address:4 }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'datetime')\">\r\n            {{ fieldData | moment:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-reference')\">\r\n            {{ fieldData | arrayDisplay:', ':getReferenceDisplayArray(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-string')\">\r\n            {{ fieldData | arrayDisplay:', ' }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'number')\">\r\n            {{ fieldData | numberFormat:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'enum')\">\r\n            {{ field.labels && field.labels[fieldData] ? field.labels[fieldData] : fieldData }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'table-normal')\">\r\n            {{ fieldData }}\r\n          </span>\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Table Columns from Custom Columns -->\r\n  <ng-container\r\n    *ngFor=\"let colName of customCols | keys\"\r\n    [matColumnDef]=\"colName\"\r\n  >\r\n    <ng-container *ngIf=\"customCols[colName].data; let colData\">\r\n      <th\r\n        mat-header-cell *matHeaderCellDef\r\n        [width]=\"colData.width\"\r\n        [align]=\"colData.align\"\r\n        mat-sort-header [disabled]=\"!colData.sortable\" [start]=\"colData.sortStart\"\r\n      >\r\n        {{ (colData.label || colName) | splitCamel | titlecase }}\r\n      </th>\r\n\r\n      <td\r\n        mat-cell *matCellDef=\"let element; let i = index;\"\r\n        [align]=\"colData.align\"\r\n        class=\"keps-table-column\"\r\n      >\r\n        <ng-container\r\n          *ngTemplateOutlet=\"customCols[colName].template; context: { $implicit: element, index: i, element: element }\"\r\n        >\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Rows -->\r\n  <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n  <tr\r\n    mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n    (click)=\"rowClick.emit(row)\"\r\n    [class.clickable-row]=\"rowClick.observers.length > 0\"\r\n  >\r\n  </tr>\r\n</table>\r\n\r\n<!-- Empty Table Row -->\r\n<mat-card *ngIf=\"data.length === 0 || dataSource.filteredData.length === 0\" class=\"empty-table\">\r\n  No Data.\r\n</mat-card>\r\n\r\n<!-- Paginator -->\r\n<mat-paginator\r\n  [class.hide-paginator]=\"!showPaginator\"\r\n  [pageSizeOptions]=\"[10, 25, 50, 100]\"\r\n  [pageSize]=\"pageSize\"\r\n  (page)=\"onPageChange($event)\"\r\n  showFirstLastButtons\r\n  class=\"keps-paginator\"\r\n>\r\n</mat-paginator>\r\n",
                    styles: [":host{display:block;position:relative}.keps-table{width:100%}.empty-table,.keps-paginator,.keps-table{background:0 0!important}.keps-table-cell{white-space:pre-line}.keps-table-cell *{display:block;margin:16px 0}.empty-table{border-radius:0;text-align:center;box-shadow:none!important}.hide-paginator{display:none!important}.keps-table-search{width:95%;margin:0 2.5%}.clickable-row:hover{background:rgba(0,0,0,.05);cursor:pointer}"]
                }] }
    ];
    /** @nocollapse */
    TableComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] },
        { type: IterableDiffers }
    ]; };
    TableComponent.propDecorators = {
        model: [{ type: Input }],
        schema: [{ type: Input }],
        table: [{ type: ViewChild, args: [MatTable, { static: true },] }],
        data: [{ type: Input }],
        displayedColumns: [{ type: Input }],
        hiddenColumns: [{ type: Input }],
        filterPredicate: [{ type: Input }],
        customColDatas: [{ type: ContentChildren, args: [TableColDirective,] }],
        customColTemplates: [{ type: ContentChildren, args: [TableColDirective, { read: TemplateRef },] }],
        paginator: [{ type: ViewChild, args: [MatPaginator, { static: true },] }],
        showPaginator: [{ type: Input }],
        savePageSize: [{ type: Input }],
        defaultPageSize: [{ type: Input }],
        sort: [{ type: ViewChild, args: [MatSort, { static: true },] }],
        showSort: [{ type: Input }],
        activeSort: [{ type: Input }],
        showSearch: [{ type: Input }],
        search: [{ type: Input }],
        rowClick: [{ type: Output }]
    };
    return TableComponent;
}());
export { TableComponent };
if (false) {
    /**
     * Name of the model for the table.
     * @type {?}
     */
    TableComponent.prototype.model;
    /**
     * Schema of the model for the table.
     *
     * If the model name is supplied,
     * then the table will get the schema from that model.
     * @type {?}
     */
    TableComponent.prototype.schema;
    /**
     * Reference to the MatTable.
     * @type {?}
     * @private
     */
    TableComponent.prototype.table;
    /**
     * Data to be shown on the table.
     * @type {?}
     */
    TableComponent.prototype.data;
    /**
     * Wrapper for the data using MatTableDataSource.
     *
     * Do not change this. Use the `data` property instead to change the data.
     *
     * \@internal Do not use!
     * @type {?}
     */
    TableComponent.prototype.dataSource;
    /**
     * Columns to be displayed on the table.
     * @type {?}
     */
    TableComponent.prototype.displayedColumns;
    /**
     * Columns to be hidden on the table.
     * @type {?}
     */
    TableComponent.prototype.hiddenColumns;
    /**
     * Custom function to filter the data source
     * See example below:
     * <pre>
     *  customFilter(Data: T, Filter: string): boolean {
     *     return <true if Data matches filter>
     *  }
     * <pre>
     *
     * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
     * @type {?}
     */
    TableComponent.prototype.filterPredicate;
    /**
     * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
     * \@internal Do not use!
     * @type {?}
     */
    TableComponent.prototype.allColumns;
    /**
     * Queries all `TableColDirective` data inside the component.
     * @type {?}
     * @private
     */
    TableComponent.prototype.customColDatas;
    /**
     * Queries all `TableColDirective` templates inside the component.
     * @type {?}
     * @private
     */
    TableComponent.prototype.customColTemplates;
    /**
     * A record of custom column data and template keyed by the custom column ID.
     * @type {?}
     */
    TableComponent.prototype.customCols;
    /**
     * Reference to the MatPaginator.
     * @type {?}
     * @private
     */
    TableComponent.prototype.paginator;
    /**
     * Whether the table should display the paginator or not.
     * @type {?}
     */
    TableComponent.prototype.showPaginator;
    /**
     * A key used when saving page size number to the local storage.
     * @type {?}
     */
    TableComponent.prototype.savePageSize;
    /**
     * The table's default paginator page size.
     * @type {?}
     */
    TableComponent.prototype.defaultPageSize;
    /**
     * The current page size on paginator.
     * @type {?}
     */
    TableComponent.prototype.pageSize;
    /**
     * Reference to the MatSort.
     * @type {?}
     * @private
     */
    TableComponent.prototype.sort;
    /**
     * Whether the table should display sort on headers or not.
     * @type {?}
     */
    TableComponent.prototype.showSort;
    /**
     * The ID of the column that should be sorted.
     * @type {?}
     */
    TableComponent.prototype.activeSort;
    /**
     * Whether the table should show the default search box or not.
     * @type {?}
     */
    TableComponent.prototype.showSearch;
    /**
     * The search query to be used to filter the data.
     * @type {?}
     */
    TableComponent.prototype.search;
    /**
     * An event emitter that emits the object of the row clicked by the user.
     * @type {?}
     */
    TableComponent.prototype.rowClick;
    /**
     * An iterable differ instance to track changes to the data.
     * @type {?}
     * @private
     */
    TableComponent.prototype.diff;
    /**
     * Instance of the `EvalPipe` to be used for custom columns sorting.
     * @type {?}
     * @private
     */
    TableComponent.prototype.evalPipe;
    /**
     * Whether the table has done initializing or not.
     * \@readonly
     * @type {?}
     */
    TableComponent.prototype.initialized;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype.models;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype.iterableDiffers;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvdGFibGUvdGFibGUvdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQU1ULEtBQUssRUFDTCxNQUFNLEVBQ04sWUFBWSxFQUNaLFNBQVMsRUFDVCxlQUFlLEVBQ2YsU0FBUyxFQUNULFdBQVcsRUFFWCxlQUFlLEVBQ2YsTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFDTCxRQUFRLEVBQ1Isa0JBQWtCLEVBQ2xCLFlBQVksRUFFWixPQUFPLEVBQ1IsTUFBTSxtQkFBbUIsQ0FBQztBQUMzQixPQUFPLEVBQUUsaUJBQWlCLEVBQWdCLE1BQU0sd0JBQXdCLENBQUM7QUFFekUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ25ELE9BQU8sRUFBRSxNQUFNLEVBQUUsbUJBQW1CLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUV2RjtJQTBKRTs7T0FFRztJQUNILHdCQUM0QixNQUFrQixFQUNwQyxlQUFnQztRQURkLFdBQU0sR0FBTixNQUFNLENBQVk7UUFDcEMsb0JBQWUsR0FBZixlQUFlLENBQWlCOzs7O1FBcElqQyxTQUFJLEdBQWlCLEVBQUUsQ0FBQzs7OztRQWN4QixxQkFBZ0IsR0FBYSxFQUFFLENBQUM7Ozs7UUFLaEMsa0JBQWEsR0FBYSxFQUFFLENBQUM7Ozs7Ozs7Ozs7OztRQWM3QixvQkFBZSxHQUFhLElBQUksQ0FBQzs7Ozs7UUFNMUMsZUFBVSxHQUFhLEVBQUUsQ0FBQzs7OztRQWUxQixlQUFVLEdBQWlDLEVBQUUsQ0FBQzs7OztRQW9CckMsb0JBQWUsR0FBRyxFQUFFLENBQUM7Ozs7UUFlckIsYUFBUSxHQUFHLEtBQUssQ0FBQzs7OztRQVVqQixlQUFVLEdBQUcsS0FBSyxDQUFDOzs7O1FBS25CLFdBQU0sR0FBRyxFQUFFLENBQUM7Ozs7UUFLWCxhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQWMsQ0FBQzs7Ozs7UUFnQnBELGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBU2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCxpQ0FBUTs7OztJQUFSO1FBQUEsaUJBMkJDO1FBMUJDLDhCQUE4QjtRQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWhFLDRCQUE0QjtRQUM1QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUUzQyw2RUFBNkU7WUFDN0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDO1NBQ3pGO1FBRUQsdUJBQXVCO1FBQ3ZCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBRWpDLHVGQUF1RjtZQUN2RixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUI7Ozs7O1lBQUcsVUFBQyxJQUFnQixFQUFFLFlBQW9COztvQkFDckUsU0FBUyxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDO2dCQUMvQyxJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtvQkFDNUMsT0FBTyxLQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztpQkFDbkU7Z0JBQ0QsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDNUIsQ0FBQyxDQUFBLENBQUM7U0FDSDtRQUNELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILDJDQUFrQjs7OztJQUFsQjtRQUNFLHdEQUF3RDtRQUN4RCwrREFBK0Q7UUFDL0Qsd0JBQXdCO1FBSDFCLGlCQXNDQztRQWpDQywwQ0FBMEM7UUFDMUMsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3pDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDO1NBQzlDO2FBQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDakQsTUFBTSxJQUFJLEtBQUssQ0FBQyw4QkFBNEIsSUFBSSxDQUFDLEtBQUssTUFBRyxDQUFDLENBQUM7U0FDNUQ7UUFFRCw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDaEIsTUFBTSxJQUFJLEtBQUssQ0FBQyx1REFBdUQsQ0FBQyxDQUFDO1NBQzFFO1FBRUQsMkRBQTJEO1FBQzNELElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBaEIsQ0FBZ0IsRUFBQyxDQUFDOzs7WUFHdkUsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFOztZQUN4QyxZQUFZLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRTtRQUN0RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7Z0JBQ2xDLEtBQUssR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUM1QixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHO2dCQUN2QixJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDakIsUUFBUSxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUM7YUFDMUIsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzdCO1FBRUQsdUZBQXVGO1FBQ3ZGLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDdEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTTs7OztZQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsQ0FBQyxLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBbkMsQ0FBbUMsRUFBQyxDQUFDO1NBQzlGO1FBRUQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7SUFDMUIsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILGtDQUFTOzs7O0lBQVQ7UUFDRSx1REFBdUQ7UUFDdkQsZ0RBQWdEO1FBQ2hELElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTs7Z0JBQ2QsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDekMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDakMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQzthQUN6QjtTQUNGO0lBQ0gsQ0FBQztJQUVEOztPQUVHOzs7Ozs7SUFDSCxvQ0FBVzs7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQ2hDLCtFQUErRTtRQUMvRSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksT0FBTyxDQUFDLE1BQU0sRUFBRTtZQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMvQjtJQUNILENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gscUNBQVk7Ozs7OztJQUFaLFVBQWEsS0FBZ0I7UUFDM0IsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7U0FDcEU7SUFDSCxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSCxvQ0FBVzs7Ozs7SUFBWCxVQUFZLE1BQWM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3ZELENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDSCwrQkFBTTs7Ozs7OztJQUFOLFVBQU8sS0FBYSxFQUFFLElBQVk7UUFDaEMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCw0Q0FBbUI7Ozs7OztJQUFuQixVQUFvQixLQUFhO1FBQy9CLE9BQU8sbUJBQW1CLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsaURBQXdCOzs7Ozs7SUFBeEIsVUFBeUIsS0FBYTtRQUNwQyxPQUFPLHdCQUF3QixDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBR0Q7Ozs7Ozs7Ozs7OztPQVlHOzs7Ozs7Ozs7Ozs7Ozs7SUFDSCwyQ0FBa0I7Ozs7Ozs7Ozs7Ozs7O0lBQWxCLFVBQW1CLHVCQUE2QjtRQUE3Qix3Q0FBQSxFQUFBLDhCQUE2QjtRQUM5QyxJQUFJLHVCQUF1QixFQUFFO1lBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxHQUFHLHVCQUF1QixDQUFDO1NBQzNEO0lBQ0gsQ0FBQzs7Z0JBelVGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsWUFBWTtvQkFDdEIsOGlJQUFxQzs7aUJBRXRDOzs7O2dEQTBKSSxNQUFNLFNBQUMsUUFBUTtnQkE3S2xCLGVBQWU7Ozt3QkF3QmQsS0FBSzt5QkFRTCxLQUFLO3dCQUtMLFNBQVMsU0FBQyxRQUFRLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFO3VCQUtwQyxLQUFLO21DQWNMLEtBQUs7Z0NBS0wsS0FBSztrQ0FjTCxLQUFLO2lDQVdMLGVBQWUsU0FBQyxpQkFBaUI7cUNBS2pDLGVBQWUsU0FBQyxpQkFBaUIsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUU7NEJBVXhELFNBQVMsU0FBQyxZQUFZLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFO2dDQUt4QyxLQUFLOytCQUtMLEtBQUs7a0NBS0wsS0FBSzt1QkFVTCxTQUFTLFNBQUMsT0FBTyxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTsyQkFLbkMsS0FBSzs2QkFLTCxLQUFLOzZCQUtMLEtBQUs7eUJBS0wsS0FBSzsyQkFLTCxNQUFNOztJQW1NVCxxQkFBQztDQUFBLEFBM1VELElBMlVDO1NBdFVZLGNBQWM7Ozs7OztJQUl6QiwrQkFBdUI7Ozs7Ozs7O0lBUXZCLGdDQUE0Qjs7Ozs7O0lBSzVCLCtCQUFvRTs7Ozs7SUFLcEUsOEJBQWlDOzs7Ozs7Ozs7SUFTakMsb0NBQTJDOzs7OztJQUszQywwQ0FBeUM7Ozs7O0lBS3pDLHVDQUFzQzs7Ozs7Ozs7Ozs7OztJQWN0Qyx5Q0FBMEM7Ozs7OztJQU0xQyxvQ0FBMEI7Ozs7OztJQUsxQix3Q0FBeUY7Ozs7OztJQUt6Riw0Q0FBbUg7Ozs7O0lBS25ILG9DQUE4Qzs7Ozs7O0lBSzlDLG1DQUEyRTs7Ozs7SUFLM0UsdUNBQThCOzs7OztJQUs5QixzQ0FBOEI7Ozs7O0lBSzlCLHlDQUE4Qjs7Ozs7SUFLOUIsa0NBQWlCOzs7Ozs7SUFLakIsOEJBQTREOzs7OztJQUs1RCxrQ0FBMEI7Ozs7O0lBSzFCLG9DQUE0Qjs7Ozs7SUFLNUIsb0NBQTRCOzs7OztJQUs1QixnQ0FBcUI7Ozs7O0lBS3JCLGtDQUFvRDs7Ozs7O0lBS3BELDhCQUFrQzs7Ozs7O0lBS2xDLGtDQUEyQjs7Ozs7O0lBTTNCLHFDQUFvQjs7Ozs7SUFNbEIsZ0NBQTRDOzs7OztJQUM1Qyx5Q0FBd0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkluaXQsXHJcbiAgQWZ0ZXJDb250ZW50SW5pdCxcclxuICBEb0NoZWNrLFxyXG4gIE9uQ2hhbmdlcyxcclxuICBTaW1wbGVDaGFuZ2VzLFxyXG4gIElucHV0LFxyXG4gIE91dHB1dCxcclxuICBFdmVudEVtaXR0ZXIsXHJcbiAgVmlld0NoaWxkLFxyXG4gIENvbnRlbnRDaGlsZHJlbixcclxuICBRdWVyeUxpc3QsXHJcbiAgVGVtcGxhdGVSZWYsXHJcbiAgSXRlcmFibGVEaWZmZXIsXHJcbiAgSXRlcmFibGVEaWZmZXJzLFxyXG4gIEluamVjdFxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gIE1hdFRhYmxlLFxyXG4gIE1hdFRhYmxlRGF0YVNvdXJjZSxcclxuICBNYXRQYWdpbmF0b3IsXHJcbiAgUGFnZUV2ZW50LFxyXG4gIE1hdFNvcnRcclxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IFRhYmxlQ29sRGlyZWN0aXZlLCBDdXN0b21Db2x1bW4gfSBmcm9tICcuLi90YWJsZS1jb2wuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgS2Vwc1NjaGVtYSwgS2Vwc01vZGVscywgS2Vwc09iamVjdCB9IGZyb20gJy4uLy4uLy4uL2ludGVyZmFjZSc7XHJcbmltcG9ydCB7IEV2YWxQaXBlIH0gZnJvbSAnLi4vLi4vLi4vcGlwZS9ldmFsLnBpcGUnO1xyXG5pbXBvcnQgeyBpc1R5cGUsIGdldFJlZmVyZW5jZURpc3BsYXksIGdldFJlZmVyZW5jZURpc3BsYXlBcnJheSB9IGZyb20gJy4uLy4uLy4uL3V0aWxzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy10YWJsZScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3RhYmxlLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi90YWJsZS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUYWJsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJDb250ZW50SW5pdCwgRG9DaGVjaywgT25DaGFuZ2VzIHtcclxuICAvKipcclxuICAgKiBOYW1lIG9mIHRoZSBtb2RlbCBmb3IgdGhlIHRhYmxlLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIG1vZGVsOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIFNjaGVtYSBvZiB0aGUgbW9kZWwgZm9yIHRoZSB0YWJsZS5cclxuICAgKlxyXG4gICAqIElmIHRoZSBtb2RlbCBuYW1lIGlzIHN1cHBsaWVkLFxyXG4gICAqIHRoZW4gdGhlIHRhYmxlIHdpbGwgZ2V0IHRoZSBzY2hlbWEgZnJvbSB0aGF0IG1vZGVsLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHNjaGVtYTogS2Vwc1NjaGVtYTtcclxuXHJcbiAgLyoqXHJcbiAgICogUmVmZXJlbmNlIHRvIHRoZSBNYXRUYWJsZS5cclxuICAgKi9cclxuICBAVmlld0NoaWxkKE1hdFRhYmxlLCB7IHN0YXRpYzogdHJ1ZSB9KSBwcml2YXRlIHRhYmxlOiBNYXRUYWJsZTxhbnk+O1xyXG5cclxuICAvKipcclxuICAgKiBEYXRhIHRvIGJlIHNob3duIG9uIHRoZSB0YWJsZS5cclxuICAgKi9cclxuICBASW5wdXQoKSBkYXRhOiBLZXBzT2JqZWN0W10gPSBbXTtcclxuXHJcbiAgLyoqXHJcbiAgICogV3JhcHBlciBmb3IgdGhlIGRhdGEgdXNpbmcgTWF0VGFibGVEYXRhU291cmNlLlxyXG4gICAqXHJcbiAgICogRG8gbm90IGNoYW5nZSB0aGlzLiBVc2UgdGhlIGBkYXRhYCBwcm9wZXJ0eSBpbnN0ZWFkIHRvIGNoYW5nZSB0aGUgZGF0YS5cclxuICAgKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGRhdGFTb3VyY2U6IE1hdFRhYmxlRGF0YVNvdXJjZTxLZXBzT2JqZWN0PjtcclxuXHJcbiAgLyoqXHJcbiAgICogQ29sdW1ucyB0byBiZSBkaXNwbGF5ZWQgb24gdGhlIHRhYmxlLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGRpc3BsYXllZENvbHVtbnM6IHN0cmluZ1tdID0gW107XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbHVtbnMgdG8gYmUgaGlkZGVuIG9uIHRoZSB0YWJsZS5cclxuICAgKi9cclxuICBASW5wdXQoKSBoaWRkZW5Db2x1bW5zOiBzdHJpbmdbXSA9IFtdO1xyXG5cclxuXHJcbiAgLyoqXHJcbiAgICogQ3VzdG9tIGZ1bmN0aW9uIHRvIGZpbHRlciB0aGUgZGF0YSBzb3VyY2VcclxuICAgKiBTZWUgZXhhbXBsZSBiZWxvdzpcclxuICAgKiA8cHJlPlxyXG4gICAqICBjdXN0b21GaWx0ZXIoRGF0YTogVCwgRmlsdGVyOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgKiAgICAgcmV0dXJuIDx0cnVlIGlmIERhdGEgbWF0Y2hlcyBmaWx0ZXI+XHJcbiAgICogIH1cclxuICAgKiA8cHJlPlxyXG4gICAqIFxyXG4gICAqIEBzZWUgPGEgaHJlZj1cImh0dHBzOi8vbWF0ZXJpYWwuYW5ndWxhci5pby9jb21wb25lbnRzL3RhYmxlL2FwaVwiPkFuZ3VsYXIgVGFibGU8L2E+IGZvciBtb3JlIGluZm8uXHJcbiAgICovXHJcbiAgQElucHV0KCkgZmlsdGVyUHJlZGljYXRlOiBGdW5jdGlvbiA9IG51bGw7XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbHVtbnMgdG8gYmUgZGlzcGxheWVkIG9uIHRoZSB0YWJsZSBiYXNlZCBvbiBkaXNwbGF5ZWRDb2x1bW5zIGFuZCBoaWRkZW5Db2x1bW5zLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGFsbENvbHVtbnM6IHN0cmluZ1tdID0gW107XHJcblxyXG4gIC8qKlxyXG4gICAqIFF1ZXJpZXMgYWxsIGBUYWJsZUNvbERpcmVjdGl2ZWAgZGF0YSBpbnNpZGUgdGhlIGNvbXBvbmVudC5cclxuICAgKi9cclxuICBAQ29udGVudENoaWxkcmVuKFRhYmxlQ29sRGlyZWN0aXZlKSBwcml2YXRlIGN1c3RvbUNvbERhdGFzOiBRdWVyeUxpc3Q8VGFibGVDb2xEaXJlY3RpdmU+O1xyXG5cclxuICAvKipcclxuICAgKiBRdWVyaWVzIGFsbCBgVGFibGVDb2xEaXJlY3RpdmVgIHRlbXBsYXRlcyBpbnNpZGUgdGhlIGNvbXBvbmVudC5cclxuICAgKi9cclxuICBAQ29udGVudENoaWxkcmVuKFRhYmxlQ29sRGlyZWN0aXZlLCB7IHJlYWQ6IFRlbXBsYXRlUmVmIH0pIHByaXZhdGUgY3VzdG9tQ29sVGVtcGxhdGVzOiBRdWVyeUxpc3Q8VGVtcGxhdGVSZWY8YW55Pj47XHJcblxyXG4gIC8qKlxyXG4gICAqIEEgcmVjb3JkIG9mIGN1c3RvbSBjb2x1bW4gZGF0YSBhbmQgdGVtcGxhdGUga2V5ZWQgYnkgdGhlIGN1c3RvbSBjb2x1bW4gSUQuXHJcbiAgICovXHJcbiAgY3VzdG9tQ29sczogUmVjb3JkPHN0cmluZywgQ3VzdG9tQ29sdW1uPiA9IHt9O1xyXG5cclxuICAvKipcclxuICAgKiBSZWZlcmVuY2UgdG8gdGhlIE1hdFBhZ2luYXRvci5cclxuICAgKi9cclxuICBAVmlld0NoaWxkKE1hdFBhZ2luYXRvciwgeyBzdGF0aWM6IHRydWUgfSkgcHJpdmF0ZSBwYWdpbmF0b3I6IE1hdFBhZ2luYXRvcjtcclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgdGFibGUgc2hvdWxkIGRpc3BsYXkgdGhlIHBhZ2luYXRvciBvciBub3QuXHJcbiAgICovXHJcbiAgQElucHV0KCkgc2hvd1BhZ2luYXRvcjogZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEEga2V5IHVzZWQgd2hlbiBzYXZpbmcgcGFnZSBzaXplIG51bWJlciB0byB0aGUgbG9jYWwgc3RvcmFnZS5cclxuICAgKi9cclxuICBASW5wdXQoKSBzYXZlUGFnZVNpemU6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIHRhYmxlJ3MgZGVmYXVsdCBwYWdpbmF0b3IgcGFnZSBzaXplLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGRlZmF1bHRQYWdlU2l6ZSA9IDEwO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgY3VycmVudCBwYWdlIHNpemUgb24gcGFnaW5hdG9yLlxyXG4gICAqL1xyXG4gIHBhZ2VTaXplOiBudW1iZXI7XHJcblxyXG4gIC8qKlxyXG4gICAqIFJlZmVyZW5jZSB0byB0aGUgTWF0U29ydC5cclxuICAgKi9cclxuICBAVmlld0NoaWxkKE1hdFNvcnQsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgc29ydDogTWF0U29ydDtcclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgdGFibGUgc2hvdWxkIGRpc3BsYXkgc29ydCBvbiBoZWFkZXJzIG9yIG5vdC5cclxuICAgKi9cclxuICBASW5wdXQoKSBzaG93U29ydCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgSUQgb2YgdGhlIGNvbHVtbiB0aGF0IHNob3VsZCBiZSBzb3J0ZWQuXHJcbiAgICovXHJcbiAgQElucHV0KCkgYWN0aXZlU29ydDogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSB0YWJsZSBzaG91bGQgc2hvdyB0aGUgZGVmYXVsdCBzZWFyY2ggYm94IG9yIG5vdC5cclxuICAgKi9cclxuICBASW5wdXQoKSBzaG93U2VhcmNoID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBzZWFyY2ggcXVlcnkgdG8gYmUgdXNlZCB0byBmaWx0ZXIgdGhlIGRhdGEuXHJcbiAgICovXHJcbiAgQElucHV0KCkgc2VhcmNoID0gJyc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFuIGV2ZW50IGVtaXR0ZXIgdGhhdCBlbWl0cyB0aGUgb2JqZWN0IG9mIHRoZSByb3cgY2xpY2tlZCBieSB0aGUgdXNlci5cclxuICAgKi9cclxuICBAT3V0cHV0KCkgcm93Q2xpY2sgPSBuZXcgRXZlbnRFbWl0dGVyPEtlcHNPYmplY3Q+KCk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFuIGl0ZXJhYmxlIGRpZmZlciBpbnN0YW5jZSB0byB0cmFjayBjaGFuZ2VzIHRvIHRoZSBkYXRhLlxyXG4gICAqL1xyXG4gIHByaXZhdGUgZGlmZjogSXRlcmFibGVEaWZmZXI8YW55PjtcclxuXHJcbiAgLyoqXHJcbiAgICogSW5zdGFuY2Ugb2YgdGhlIGBFdmFsUGlwZWAgdG8gYmUgdXNlZCBmb3IgY3VzdG9tIGNvbHVtbnMgc29ydGluZy5cclxuICAgKi9cclxuICBwcml2YXRlIGV2YWxQaXBlOiBFdmFsUGlwZTtcclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgdGFibGUgaGFzIGRvbmUgaW5pdGlhbGl6aW5nIG9yIG5vdC5cclxuICAgKiBAcmVhZG9ubHlcclxuICAgKi9cclxuICBpbml0aWFsaXplZCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIEBJbmplY3QoJ01PREVMUycpIHByaXZhdGUgbW9kZWxzOiBLZXBzTW9kZWxzLFxyXG4gICAgcHJpdmF0ZSBpdGVyYWJsZURpZmZlcnM6IEl0ZXJhYmxlRGlmZmVyc1xyXG4gICkge1xyXG4gICAgdGhpcy5kaWZmID0gdGhpcy5pdGVyYWJsZURpZmZlcnMuZmluZChbXSkuY3JlYXRlKG51bGwpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICAvLyBJbml0aWFsaXplIHRoZSBkYXRhIHNvdXJjZS5cclxuICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2U8S2Vwc09iamVjdD4odGhpcy5kYXRhKTtcclxuXHJcbiAgICAvLyBJbml0aWFsaXplIHRoZSBwYWdpbmF0b3IuXHJcbiAgICBpZiAodGhpcy5zaG93UGFnaW5hdG9yKSB7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZS5wYWdpbmF0b3IgPSB0aGlzLnBhZ2luYXRvcjtcclxuXHJcbiAgICAgIC8vIEdldCB0aGUgcHJldmlvdXMgc2Vzc2lvbidzIHBhZ2Ugc2l6ZSBpZiBhbnksIG9yIHVzZSB0aGUgZGVmYXVsdCBwYWdlIHNpemUuXHJcbiAgICAgIHRoaXMucGFnZVNpemUgPSBOdW1iZXIobG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy5zYXZlUGFnZVNpemUpKSB8fCB0aGlzLmRlZmF1bHRQYWdlU2l6ZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBJbml0aWFsaXplIHRoZSBzb3J0LlxyXG4gICAgaWYgKHRoaXMuc2hvd1NvcnQpIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlLnNvcnQgPSB0aGlzLnNvcnQ7XHJcblxyXG4gICAgICAvLyBNb2RpZnkgdGhlIGRhdGEgc291cmNlJ3MgYHNvcnRpbmdEYXRhQWNjZXNzb3JgIHRvIHVzZSBgRXZhbFBpcGVgIGZvciBjdXN0b20gY29sdW1ucy5cclxuICAgICAgdGhpcy5ldmFsUGlwZSA9IG5ldyBFdmFsUGlwZSgpO1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2Uuc29ydGluZ0RhdGFBY2Nlc3NvciA9IChkYXRhOiBLZXBzT2JqZWN0LCBzb3J0SGVhZGVySWQ6IHN0cmluZykgPT4ge1xyXG4gICAgICAgIGNvbnN0IGN1c3RvbUNvbCA9IHRoaXMuY3VzdG9tQ29sc1tzb3J0SGVhZGVySWRdO1xyXG4gICAgICAgIGlmIChjdXN0b21Db2wgJiYgY3VzdG9tQ29sLmRhdGEuc29ydEFjY2Vzc29yKSB7XHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5ldmFsUGlwZS50cmFuc2Zvcm0oZGF0YSwgY3VzdG9tQ29sLmRhdGEuc29ydEFjY2Vzc29yKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGRhdGFbc29ydEhlYWRlcklkXTtcclxuICAgICAgfTtcclxuICAgIH1cclxuICAgIHRoaXMuc2V0RmlsdGVyUHJlZGljYXRlKHRoaXMuZmlsdGVyUHJlZGljYXRlKTsgICAgXHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBuZ0FmdGVyQ29udGVudEluaXQoKSB7XHJcbiAgICAvLyBXZSBhcmUgdXNpbmcgYG5nQWZ0ZXJDb250ZW50SW5pdGAgaGVyZSBiZWNhdXNlIHdlIGNhblxyXG4gICAgLy8gb25seSBnZXQgdGhlIGN1c3RvbSBjb2x1bW5zIGRhdGEgYWZ0ZXIgdGhlIGNvbXBvbmVudCBjb250ZW50XHJcbiAgICAvLyBoYXMgYmVlbiBpbml0aWFsaXplZC5cclxuXHJcbiAgICAvLyBHZXQgc2NoZW1hIGZyb20gbW9kZWwgbmFtZSBpZiBzdXBwbGllZC5cclxuICAgIGlmICh0aGlzLm1vZGVsICYmIHRoaXMubW9kZWxzW3RoaXMubW9kZWxdKSB7XHJcbiAgICAgIHRoaXMuc2NoZW1hID0gdGhpcy5tb2RlbHNbdGhpcy5tb2RlbF0uc2NoZW1hO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLm1vZGVsICYmICF0aGlzLm1vZGVsc1t0aGlzLm1vZGVsXSkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoYENhbid0IGZpbmQgYSBtb2RlbCBuYW1lZCAke3RoaXMubW9kZWx9IWApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFzc2VydCBzY2hlbWEgbXVzdCBleGlzdC5cclxuICAgIGlmICghdGhpcy5zY2hlbWEpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKGBNdXN0IHN1cHBseSB2YWxpZCBtb2RlbCBuYW1lIG9yIHNjaGVtYSB0byBLZXBzIHRhYmxlIWApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFNldCBgYWxsQ29sdW1uc2AgdG8gYmUgZXZlcnkgcHVibGljIGZpZWxkIGluIHRoZSBzY2hlbWEuXHJcbiAgICB0aGlzLmFsbENvbHVtbnMgPSBPYmplY3Qua2V5cyh0aGlzLnNjaGVtYSkuZmlsdGVyKGZpZWxkID0+IGZpZWxkWzBdICE9PSAnXycpO1xyXG4gICAgXHJcbiAgICAvLyBQcm9jZXNzIGN1c3RvbSBjb2x1bW5zLlxyXG4gICAgY29uc3QgY29sRGF0YXMgPSB0aGlzLmN1c3RvbUNvbERhdGFzLnRvQXJyYXkoKTtcclxuICAgIGNvbnN0IGNvbFRlbXBsYXRlcyA9IHRoaXMuY3VzdG9tQ29sVGVtcGxhdGVzLnRvQXJyYXkoKTtcclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY29sRGF0YXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgY29uc3QgY29sSWQgPSBjb2xEYXRhc1tpXS5pZDtcclxuICAgICAgdGhpcy5jdXN0b21Db2xzW2NvbElkXSA9IHtcclxuICAgICAgICBkYXRhOiBjb2xEYXRhc1tpXSxcclxuICAgICAgICB0ZW1wbGF0ZTogY29sVGVtcGxhdGVzW2ldXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuYWxsQ29sdW1ucy5wdXNoKGNvbElkKTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLy8gSWYgYGRpc3BsYXllZENvbHVtbnNgIGlzIG5vdCBwcm92aWRlZCwgdXNlIGBhbGxDb2x1bW5zYCBmaWx0ZXJlZCBieSBgaGlkZGVuQ29sdW1uc2AuXHJcbiAgICBpZiAodGhpcy5kaXNwbGF5ZWRDb2x1bW5zLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSB0aGlzLmFsbENvbHVtbnMuZmlsdGVyKGZpZWxkID0+ICF0aGlzLmhpZGRlbkNvbHVtbnMuaW5jbHVkZXMoZmllbGQpKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmluaXRpYWxpemVkID0gdHJ1ZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nRG9DaGVjaygpIHtcclxuICAgIC8vIFRoaXMgd2lsbCByZWZyZXNoIHRoZSB0YWJsZSBkYXRhIHNvdXJjZSBhbmQgcmVyZW5kZXJcclxuICAgIC8vIHRoZSB0YWJsZSBpZiB0aGVyZSBpcyBhbnkgY2hhbmdlIHRvIHRoZSBkYXRhLlxyXG4gICAgaWYgKHRoaXMuaW5pdGlhbGl6ZWQpIHtcclxuICAgICAgY29uc3QgY2hhbmdlcyA9IHRoaXMuZGlmZi5kaWZmKHRoaXMuZGF0YSk7XHJcbiAgICAgIGlmIChjaGFuZ2VzKSB7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlLmRhdGEgPSB0aGlzLmRhdGE7XHJcbiAgICAgICAgdGhpcy50YWJsZS5yZW5kZXJSb3dzKCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIC8vIFRoaXMgY2hlY2sgaWYgdGhlIGBzZWFyY2hgIHByb3BlcnR5IGhhcyBjaGFuZ2VkIGFuZCBhcHBseSBzZWFyY2ggaWYgaXQgZG9lcy5cclxuICAgIGlmICh0aGlzLmluaXRpYWxpemVkICYmIGNoYW5nZXMuc2VhcmNoKSB7XHJcbiAgICAgIHRoaXMuYXBwbHlTZWFyY2godGhpcy5zZWFyY2gpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2F2ZXMgY3VycmVudCBwYWdlIHNpemUgdG8gbG9jYWwgc3RvcmFnZSBpZiBwcm9wZXJ0eSBgc2F2ZVBhZ2VTaXplYCBpcyBzdXBwbGllZC5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKiBAcGFyYW0gZXZlbnQgRXZlbnQgZnJvbSBwYWdpbmF0b3IuXHJcbiAgICovXHJcbiAgb25QYWdlQ2hhbmdlKGV2ZW50OiBQYWdlRXZlbnQpIHtcclxuICAgIGlmICh0aGlzLnNhdmVQYWdlU2l6ZSkge1xyXG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnNhdmVQYWdlU2l6ZSwgZXZlbnQucGFnZVNpemUudG9TdHJpbmcoKSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBBcHBsaWVzIGEgc2VhcmNoIHF1ZXJ5IHRvIGZpbHRlciB0aGUgZGF0YS5cclxuICAgKiBAcGFyYW0gc2VhcmNoIFF1ZXJ5IGZvciB0aGUgc2VhcmNoLlxyXG4gICAqL1xyXG4gIGFwcGx5U2VhcmNoKHNlYXJjaDogc3RyaW5nKSB7XHJcbiAgICB0aGlzLmRhdGFTb3VyY2UuZmlsdGVyID0gc2VhcmNoLnRyaW0oKS50b0xvd2VyQ2FzZSgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ2hlY2tzIHRoZSB0eXBlIG9mIGEgZmllbGQuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICogQHBhcmFtIGZpZWxkIEZpZWxkIHRvIGJlIGNoZWNrZWQuXHJcbiAgICogQHBhcmFtIHR5cGUgVHlwZSB0byBiZSBjaGVja2VkLlxyXG4gICAqL1xyXG4gIGlzVHlwZShmaWVsZDogc3RyaW5nLCB0eXBlOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBpc1R5cGUodGhpcy5zY2hlbWEsIGZpZWxkLCB0eXBlKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJldHVybnMgdGhlIGRpc3BsYXkgZXhwcmVzc2lvbiBvZiBhIHJlZmVyZW5jZSBmaWVsZC5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKiBAcGFyYW0gZmllbGQgRmllbGQgdG8gZ2V0IGZyb20uXHJcbiAgICovXHJcbiAgZ2V0UmVmZXJlbmNlRGlzcGxheShmaWVsZDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gZ2V0UmVmZXJlbmNlRGlzcGxheSh0aGlzLm1vZGVscywgdGhpcy5zY2hlbWEsIGZpZWxkKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJldHVybnMgdGhlIGRpc3BsYXkgZXhwcmVzc2lvbiBvZiBhbiBhcnJheSBvZiByZWZlcmVuY2VzIGZpZWxkLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqIEBwYXJhbSBmaWVsZCBGaWVsZCB0byBnZXQgZnJvbS5cclxuICAgKi9cclxuICBnZXRSZWZlcmVuY2VEaXNwbGF5QXJyYXkoZmllbGQ6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIGdldFJlZmVyZW5jZURpc3BsYXlBcnJheSh0aGlzLm1vZGVscywgdGhpcy5zY2hlbWEsIGZpZWxkKTtcclxuICB9XHJcblxyXG5cclxuICAvKipcclxuICAgKiBTZXQgdGhlIGZpbHRlciBwcmVkaWNhdGUgb2YgZGF0YSBzb3VyY2UuIFxyXG4gICAqIHRoaXMgZnVuY3Rpb24gYWNjZXB0cyBhIGZ1bmN0aW9uIGFzIGEgcGFyYW1ldGVyLiBcclxuICAgKiBTZWUgZXhhbXBsZSBiZWxvdzpcclxuICAgKiA8cHJlPiBcclxuICAgKiAgY3VzdG9tRmlsdGVyKERhdGE6IFQsIEZpbHRlcjogc3RyaW5nKTogYm9vbGVhbiB7IFxyXG4gICAqICAgICByZXR1cm4gPHRydWUgaWYgRGF0YSBtYXRjaGVzIGZpbHRlcj4gXHJcbiAgICogIH1cclxuICAgKiA8cHJlPlxyXG4gICAqIElmIG5vIGZ1bnRpb24gaXMgcHJvdmlkZWQsIHRoaXMgZnVuY3Rpb24gd2lsbCB1c2UgdGhlIGRlZmF1bHQgXHJcbiAgICogQHNlZSA8YSBocmVmPVwiaHR0cHM6Ly9tYXRlcmlhbC5hbmd1bGFyLmlvL2NvbXBvbmVudHMvdGFibGUvYXBpXCI+QW5ndWxhciBUYWJsZTwvYT4gZm9yIG1vcmUgaW5mby5cclxuICAgKiBAcGFyYW0gZmlsdGVyUHJlZGljYXRlRnVuY3Rpb24gY3VzdG9tIGZpbHRlclByZWRpY2F0ZUZ1bmN0aW9uIHRoYXQgZm9sbG93cyB0aGUgYWJvdmUgZm9ybWF0XHJcbiAgICovXHJcbiAgc2V0RmlsdGVyUHJlZGljYXRlKGZpbHRlclByZWRpY2F0ZUZ1bmN0aW9uPSBudWxsKSB7XHJcbiAgICBpZiAoZmlsdGVyUHJlZGljYXRlRnVuY3Rpb24pIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlLmZpbHRlclByZWRpY2F0ZSA9IGZpbHRlclByZWRpY2F0ZUZ1bmN0aW9uO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19