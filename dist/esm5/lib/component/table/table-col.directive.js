/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Input } from '@angular/core';
/**
 * Represents a custom column data & template.
 * \@internal Do not use!
 * @record
 */
export function CustomColumn() { }
if (false) {
    /** @type {?} */
    CustomColumn.prototype.data;
    /** @type {?} */
    CustomColumn.prototype.template;
}
/**
 * A directive to create custom column of a Keps Table.
 */
var TableColDirective = /** @class */ (function () {
    /**
     * @internal Do not use!
     */
    function TableColDirective() {
    }
    TableColDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[kepsTableCol]'
                },] }
    ];
    /** @nocollapse */
    TableColDirective.ctorParameters = function () { return []; };
    TableColDirective.propDecorators = {
        id: [{ type: Input, args: ['kepsTableCol',] }],
        label: [{ type: Input }],
        width: [{ type: Input }],
        align: [{ type: Input }],
        sortable: [{ type: Input }],
        sortAccessor: [{ type: Input }],
        sortStart: [{ type: Input }]
    };
    return TableColDirective;
}());
export { TableColDirective };
if (false) {
    /**
     * The ID to be used to display/hide the column.
     * @type {?}
     */
    TableColDirective.prototype.id;
    /**
     * Label to be shown in the column header.
     *
     * If not specified, the table will use the ID as the label.
     * @type {?}
     */
    TableColDirective.prototype.label;
    /**
     * Width of the column in pixels (e.g. "50") or percent (e.g. "50%").
     * @type {?}
     */
    TableColDirective.prototype.width;
    /**
     * Text alignment of the column cells (including the header).
     * @type {?}
     */
    TableColDirective.prototype.align;
    /**
     * Whether the column is sortable or not.
     * @type {?}
     */
    TableColDirective.prototype.sortable;
    /**
     * Field of the table element to be used for sorting.
     * @type {?}
     */
    TableColDirective.prototype.sortAccessor;
    /**
     * The default sort direction of the column.
     * @type {?}
     */
    TableColDirective.prototype.sortStart;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUtY29sLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50L3RhYmxlL3RhYmxlLWNvbC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFlLE1BQU0sZUFBZSxDQUFDOzs7Ozs7QUFNOUQsa0NBR0M7OztJQUZDLDRCQUF3Qjs7SUFDeEIsZ0NBQTJCOzs7OztBQU03QjtJQXlDRTs7T0FFRztJQUNIO0lBQWdCLENBQUM7O2dCQTVDbEIsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxnQkFBZ0I7aUJBQzNCOzs7OztxQkFLRSxLQUFLLFNBQUMsY0FBYzt3QkFPcEIsS0FBSzt3QkFLTCxLQUFLO3dCQUtMLEtBQUs7MkJBS0wsS0FBSzsrQkFLTCxLQUFLOzRCQUtMLEtBQUs7O0lBT1Isd0JBQUM7Q0FBQSxBQTlDRCxJQThDQztTQTNDWSxpQkFBaUI7Ozs7OztJQUk1QiwrQkFBa0M7Ozs7Ozs7SUFPbEMsa0NBQXVCOzs7OztJQUt2QixrQ0FBZ0M7Ozs7O0lBS2hDLGtDQUF1Qjs7Ozs7SUFLdkIscUNBQTJCOzs7OztJQUszQix5Q0FBOEI7Ozs7O0lBSzlCLHNDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSW5wdXQsIFRlbXBsYXRlUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vKipcclxuICogUmVwcmVzZW50cyBhIGN1c3RvbSBjb2x1bW4gZGF0YSAmIHRlbXBsYXRlLlxyXG4gKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgQ3VzdG9tQ29sdW1uIHtcclxuICBkYXRhOiBUYWJsZUNvbERpcmVjdGl2ZTtcclxuICB0ZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcclxufVxyXG5cclxuLyoqXHJcbiAqIEEgZGlyZWN0aXZlIHRvIGNyZWF0ZSBjdXN0b20gY29sdW1uIG9mIGEgS2VwcyBUYWJsZS5cclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gIHNlbGVjdG9yOiAnW2tlcHNUYWJsZUNvbF0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUYWJsZUNvbERpcmVjdGl2ZSB7XHJcbiAgLyoqXHJcbiAgICogVGhlIElEIHRvIGJlIHVzZWQgdG8gZGlzcGxheS9oaWRlIHRoZSBjb2x1bW4uXHJcbiAgICovXHJcbiAgQElucHV0KCdrZXBzVGFibGVDb2wnKSBpZDogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBMYWJlbCB0byBiZSBzaG93biBpbiB0aGUgY29sdW1uIGhlYWRlci5cclxuICAgKlxyXG4gICAqIElmIG5vdCBzcGVjaWZpZWQsIHRoZSB0YWJsZSB3aWxsIHVzZSB0aGUgSUQgYXMgdGhlIGxhYmVsLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIFdpZHRoIG9mIHRoZSBjb2x1bW4gaW4gcGl4ZWxzIChlLmcuIFwiNTBcIikgb3IgcGVyY2VudCAoZS5nLiBcIjUwJVwiKS5cclxuICAgKi9cclxuICBASW5wdXQoKSB3aWR0aDogbnVtYmVyIHwgc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBUZXh0IGFsaWdubWVudCBvZiB0aGUgY29sdW1uIGNlbGxzIChpbmNsdWRpbmcgdGhlIGhlYWRlcikuXHJcbiAgICovXHJcbiAgQElucHV0KCkgYWxpZ246IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29sdW1uIGlzIHNvcnRhYmxlIG9yIG5vdC5cclxuICAgKi9cclxuICBASW5wdXQoKSBzb3J0YWJsZTogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogRmllbGQgb2YgdGhlIHRhYmxlIGVsZW1lbnQgdG8gYmUgdXNlZCBmb3Igc29ydGluZy5cclxuICAgKi9cclxuICBASW5wdXQoKSBzb3J0QWNjZXNzb3I6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIGRlZmF1bHQgc29ydCBkaXJlY3Rpb24gb2YgdGhlIGNvbHVtbi5cclxuICAgKi9cclxuICBASW5wdXQoKSBzb3J0U3RhcnQ6ICdhc2MnIHwgJ2Rlc2MnO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxufVxyXG4iXX0=