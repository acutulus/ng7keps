/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Represents an error received from HTTP request to Keps.
 * @record
 */
export function KepsError() { }
if (false) {
    /** @type {?} */
    KepsError.prototype.args;
    /** @type {?} */
    KepsError.prototype.friendly;
    /** @type {?} */
    KepsError.prototype.message;
    /** @type {?} */
    KepsError.prototype.param;
    /** @type {?} */
    KepsError.prototype.status;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2Vwcy1lcnJvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvaW50ZXJmYWNlL2tlcHMtZXJyb3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQSwrQkFNQzs7O0lBTEMseUJBQTZCOztJQUM3Qiw2QkFBNkI7O0lBQzdCLDRCQUE2Qjs7SUFDN0IsMEJBQTZCOztJQUM3QiwyQkFBNkIiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogUmVwcmVzZW50cyBhbiBlcnJvciByZWNlaXZlZCBmcm9tIEhUVFAgcmVxdWVzdCB0byBLZXBzLlxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBLZXBzRXJyb3Ige1xyXG4gIGFyZ3M6ICAgICAgICAgICAgICAgICBvYmplY3Q7XHJcbiAgZnJpZW5kbHk6ICAgICAgICAgICAgIHN0cmluZztcclxuICBtZXNzYWdlOiAgICAgICAgICAgICAgc3RyaW5nO1xyXG4gIHBhcmFtOiAgICAgICAgICAgICAgICBzdHJpbmc7XHJcbiAgc3RhdHVzOiAgICAgICAgICAgICAgIG51bWJlcjtcclxufVxyXG4iXX0=