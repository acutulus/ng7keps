/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Represents a map of KepsModel.
 * @record
 */
export function KepsModels() { }
/**
 * Represents a Keps model.
 * @record
 */
export function KepsModel() { }
if (false) {
    /** @type {?} */
    KepsModel.prototype.properties;
    /** @type {?} */
    KepsModel.prototype.schema;
}
/**
 * Represents a Keps model's property.
 * @record
 */
export function KepsProperty() { }
if (false) {
    /** @type {?|undefined} */
    KepsProperty.prototype.displayExpression;
}
/**
 * Represents a Keps model's schema.
 * @record
 */
export function KepsSchema() { }
/**
 * Represents a Keps schema's field.
 * @record
 */
export function KepsField() { }
if (false) {
    /** @type {?} */
    KepsField.prototype.type;
    /** @type {?|undefined} */
    KepsField.prototype.description;
    /** @type {?|undefined} */
    KepsField.prototype.label;
    /** @type {?|undefined} */
    KepsField.prototype.required;
    /** @type {?|undefined} */
    KepsField.prototype.optional;
    /** @type {?|undefined} */
    KepsField.prototype.default;
    /** @type {?|undefined} */
    KepsField.prototype.min;
    /** @type {?|undefined} */
    KepsField.prototype.max;
    /** @type {?|undefined} */
    KepsField.prototype.step;
    /** @type {?|undefined} */
    KepsField.prototype.format;
    /** @type {?|undefined} */
    KepsField.prototype.options;
    /** @type {?|undefined} */
    KepsField.prototype.labels;
    /** @type {?|undefined} */
    KepsField.prototype.subSchema;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2Vwcy1tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvaW50ZXJmYWNlL2tlcHMtbW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQSxnQ0FFQzs7Ozs7QUFLRCwrQkFHQzs7O0lBRkMsK0JBQW1DOztJQUNuQywyQkFBaUM7Ozs7OztBQU1uQyxrQ0FFQzs7O0lBREMseUNBQThCOzs7Ozs7QUFNaEMsZ0NBRUM7Ozs7O0FBS0QsK0JBd0JDOzs7SUF2QkMseUJBQTZCOztJQUM3QixnQ0FBNkI7O0lBQzdCLDBCQUE2Qjs7SUFDN0IsNkJBQXVDOztJQUN2Qyw2QkFBOEI7O0lBQzlCLDRCQUEwQjs7SUFHMUIsd0JBQTZCOztJQUM3Qix3QkFBNkI7O0lBQzdCLHlCQUE2Qjs7SUFHN0IsMkJBQTZCOztJQUc3Qiw0QkFBK0I7O0lBQy9CLDJCQUVFOztJQUdGLDhCQUEwQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBSZXByZXNlbnRzIGEgbWFwIG9mIEtlcHNNb2RlbC5cclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgS2Vwc01vZGVscyB7XHJcbiAgW21vZGVsOiBzdHJpbmddOiAgICAgIEtlcHNNb2RlbDtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJlcHJlc2VudHMgYSBLZXBzIG1vZGVsLlxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBLZXBzTW9kZWwge1xyXG4gIHByb3BlcnRpZXM6ICAgICAgICAgICBLZXBzUHJvcGVydHk7XHJcbiAgc2NoZW1hOiAgICAgICAgICAgICAgIEtlcHNTY2hlbWE7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBSZXByZXNlbnRzIGEgS2VwcyBtb2RlbCdzIHByb3BlcnR5LlxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBLZXBzUHJvcGVydHkge1xyXG4gIGRpc3BsYXlFeHByZXNzaW9uPzogICAgc3RyaW5nO1xyXG59XHJcblxyXG4vKipcclxuICogUmVwcmVzZW50cyBhIEtlcHMgbW9kZWwncyBzY2hlbWEuXHJcbiAqL1xyXG5leHBvcnQgaW50ZXJmYWNlIEtlcHNTY2hlbWEge1xyXG4gIFtmaWVsZDogc3RyaW5nXTogICAgICBLZXBzRmllbGQ7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBSZXByZXNlbnRzIGEgS2VwcyBzY2hlbWEncyBmaWVsZC5cclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgS2Vwc0ZpZWxkIGV4dGVuZHMgUmVjb3JkPHN0cmluZywgYW55PiB7XHJcbiAgdHlwZTogICAgICAgICAgICAgICAgIHN0cmluZztcclxuICBkZXNjcmlwdGlvbj86ICAgICAgICAgc3RyaW5nO1xyXG4gIGxhYmVsPzogICAgICAgICAgICAgICBzdHJpbmc7XHJcbiAgcmVxdWlyZWQ/OiAgICAgICAgICAgIGJvb2xlYW4gfCBzdHJpbmc7XHJcbiAgb3B0aW9uYWw/OiAgICAgICAgICAgIGJvb2xlYW47XHJcbiAgZGVmYXVsdD86ICAgICAgICAgICAgIGFueTtcclxuXHJcbiAgLy8gVHlwZTogTnVtYmVyXHJcbiAgbWluPzogICAgICAgICAgICAgICAgIG51bWJlcjtcclxuICBtYXg/OiAgICAgICAgICAgICAgICAgbnVtYmVyO1xyXG4gIHN0ZXA/OiAgICAgICAgICAgICAgICBudW1iZXI7XHJcblxyXG4gIC8vIFR5cGU6IERhdGVcclxuICBmb3JtYXQ/OiAgICAgICAgICAgICAgc3RyaW5nO1xyXG5cclxuICAvLyBUeXBlOiBFbnVtXHJcbiAgb3B0aW9ucz86ICAgICAgICAgICAgIHN0cmluZ1tdO1xyXG4gIGxhYmVscz86IHtcclxuICAgIFtvcHRpb246IHN0cmluZ106ICAgc3RyaW5nO1xyXG4gIH07XHJcblxyXG4gIC8vIFR5cGU6IEFycmF5XHJcbiAgc3ViU2NoZW1hPzogICAgICAgICAgIHN0cmluZyB8IEtlcHNTY2hlbWE7XHJcbn1cclxuIl19