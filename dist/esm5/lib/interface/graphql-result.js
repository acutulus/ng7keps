/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Represents the result of a GraphQL query.
 * @record
 */
export function GraphqlResult() { }
if (false) {
    /** @type {?} */
    GraphqlResult.prototype.data;
    /** @type {?|undefined} */
    GraphqlResult.prototype.errors;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JhcGhxbC1yZXN1bHQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2ludGVyZmFjZS9ncmFwaHFsLXJlc3VsdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBLG1DQUtDOzs7SUFKQyw2QkFFRTs7SUFDRiwrQkFBK0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogUmVwcmVzZW50cyB0aGUgcmVzdWx0IG9mIGEgR3JhcGhRTCBxdWVyeS5cclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgR3JhcGhxbFJlc3VsdCB7XHJcbiAgZGF0YToge1xyXG4gICAgW21vZGVsOiBzdHJpbmddOiAgICBhbnk7XHJcbiAgfTtcclxuICBlcnJvcnM/OiAgICAgICAgICAgICAgc3RyaW5nW107XHJcbn1cclxuIl19