/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Represents basic Keps user.
 *
 * A user on a project may have more fields.
 * @record
 */
export function KepsUser() { }
if (false) {
    /** @type {?} */
    KepsUser.prototype._id;
    /** @type {?} */
    KepsUser.prototype._createdAt;
    /** @type {?} */
    KepsUser.prototype._updatedAt;
    /** @type {?} */
    KepsUser.prototype.email;
    /** @type {?} */
    KepsUser.prototype.firstName;
    /** @type {?} */
    KepsUser.prototype.lastName;
    /** @type {?} */
    KepsUser.prototype.displayName;
    /** @type {?} */
    KepsUser.prototype.roles;
    /** @type {?} */
    KepsUser.prototype.token;
    /** @type {?} */
    KepsUser.prototype.tokenExpires;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2Vwcy11c2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmZhY2Uva2Vwcy11c2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFLQSw4QkFXQzs7O0lBVkMsdUJBQTZCOztJQUM3Qiw4QkFBNkI7O0lBQzdCLDhCQUE2Qjs7SUFDN0IseUJBQTZCOztJQUM3Qiw2QkFBNkI7O0lBQzdCLDRCQUE2Qjs7SUFDN0IsK0JBQTZCOztJQUM3Qix5QkFBK0I7O0lBQy9CLHlCQUE2Qjs7SUFDN0IsZ0NBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIFJlcHJlc2VudHMgYmFzaWMgS2VwcyB1c2VyLlxyXG4gKlxyXG4gKiBBIHVzZXIgb24gYSBwcm9qZWN0IG1heSBoYXZlIG1vcmUgZmllbGRzLlxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBLZXBzVXNlciB7XHJcbiAgX2lkOiAgICAgICAgICAgICAgICAgIHN0cmluZztcclxuICBfY3JlYXRlZEF0OiAgICAgICAgICAgbnVtYmVyO1xyXG4gIF91cGRhdGVkQXQ6ICAgICAgICAgICBudW1iZXI7XHJcbiAgZW1haWw6ICAgICAgICAgICAgICAgIHN0cmluZztcclxuICBmaXJzdE5hbWU6ICAgICAgICAgICAgc3RyaW5nO1xyXG4gIGxhc3ROYW1lOiAgICAgICAgICAgICBzdHJpbmc7XHJcbiAgZGlzcGxheU5hbWU6ICAgICAgICAgIHN0cmluZztcclxuICByb2xlczogICAgICAgICAgICAgICAgc3RyaW5nW107XHJcbiAgdG9rZW46ICAgICAgICAgICAgICAgIHN0cmluZztcclxuICB0b2tlbkV4cGlyZXM6ICAgICAgICAgbnVtYmVyO1xyXG59XHJcbiJdfQ==