# ng7keps

Angular 8 module for Keps projects.

## Installation

Requirements:
- Node 10.9.0+
- Angular CLI 8.3.1+


```bash
npm install https://bitbucket.org/acutulus/ng7keps.git
```

## Usage

- [Setting Up Ng7Keps](helps/setup.md)
- [Upgrading from Old Ng7Keps](helps/upgrade.md)

## Documentation

1. Make sure you have `compodoc` installed:
```bash
npm install @compodoc/compodoc -g
```
2. Generate the documentation:
```bash
npm run doc
```
3. Open the documentation at `/doc/index.html`.

## Development

1. Run `npm install` to install all the development dependencies.
2. Run `npm run build` to compile the library.
3. For testing purposes, you can just copy the `dist` folder to your project's `node_modules/ng7keps/dist`.

## Misc

- [To Do](helps/todo.md)
- [Known Bugs](helps/bugs.md)
