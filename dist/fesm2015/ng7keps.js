import { NgModule, Injectable, ɵɵdefineInjectable, ɵɵinject, Component, Inject, Pipe, EventEmitter, Input, Output, ViewChild, ElementRef, Optional, Self, Directive, IterableDiffers, ContentChildren, TemplateRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpClientModule } from '@angular/common/http';
import { FormGroup, FormArray, FormControl, Validators, NgControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRadioModule, MatSelectModule, MatSnackBarModule, MatProgressSpinnerModule, MatSortModule, MatTableModule, MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar, MatFormFieldControl, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS, MatTableDataSource, MatTable, MatPaginator, MatSort } from '@angular/material';
import { __awaiter } from 'tslib';
import { ReplaySubject, BehaviorSubject, Subject, merge } from 'rxjs';
import { Router } from '@angular/router';
import { take, map, tap, first, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { MatDialog as MatDialog$1 } from '@angular/material/dialog';
import { MatSnackBar as MatSnackBar$1 } from '@angular/material/snack-bar';
import moment from 'moment';
import _ from 'lodash';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { DecimalPipe } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @ignore
 * @type {?}
 */
const modules = [
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule
];
/**
 * @ignore
 */
class MaterialModule {
}
MaterialModule.decorators = [
    { type: NgModule, args: [{
                imports: modules,
                exports: modules
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const webAlias = location.host;
/** @type {?} */
const apiPrefix = '/api/v1/';
/** @type {?} */
const apiRoute = new URL(apiPrefix, location.origin).href;

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const storageKey = webAlias + '-user';
/**
 * A service that handles authentication with a Keps server.
 */
class AuthService {
    /**
     * \@internal Do not use!
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.user$ = new ReplaySubject(1);
        this.loggedIn$ = new ReplaySubject(1);
    }
    /**
     * Starts the auth service.
     * Should be called only when the app is bootstrapping.
     * @return {?}
     */
    start() {
        return __awaiter(this, void 0, void 0, /** @this {!AuthService} */ function* () {
            // Try getting user from storage.
            /** @type {?} */
            let storageUser;
            try {
                storageUser = JSON.parse(localStorage.getItem(storageKey));
            }
            catch (err) {
                console.error('Error getting user from local storage');
                this.setUser(null);
            }
            // If user exist, then get user data from server.
            if (storageUser) {
                try {
                    /** @type {?} */
                    const serverUser = yield this.http.get(apiRoute + 'users/me').toPromise();
                    if (serverUser.token) {
                        this.setUser(serverUser);
                    }
                    else if (storageUser.tokenExpires > new Date().getTime()) {
                        this.setUser(storageUser);
                    }
                    else {
                        // Token expired.
                        this.user$.error(serverUser);
                    }
                }
                catch (err) {
                    // Token error.
                    this.user$.error(err);
                }
            }
            else {
                this.setUser(null);
            }
        });
    }
    /**
     * Saves/removes user and emits the user and logged in status to observables.
     * \@internal Do not use!
     * @param {?=} user A user or null
     * @return {?}
     */
    setUser(user) {
        if (user) {
            localStorage.setItem(storageKey, JSON.stringify(user));
            this.user = user;
            this.user$.next(user);
            this.loggedIn$.next(true);
        }
        else {
            localStorage.removeItem(storageKey);
            this.user = null;
            this.user$.next(null);
            this.loggedIn$.next(false);
        }
    }
    /**
     * Returns the current user.
     * @return {?}
     */
    getUser() {
        return this.user;
    }
    /**
     * Returns an observable that will emit the current user.
     * @return {?}
     */
    getUser$() {
        return this.user$.asObservable();
    }
    /**
     * Returns token from the user.
     * @return {?}
     */
    getToken() {
        // If user is stored in auth service, use that user's token.
        if (this.user) {
            return this.user.token;
        }
        else {
            // Otherwise, try getting from the storage.
            try {
                /** @type {?} */
                const storageUser = JSON.parse(localStorage.getItem(storageKey));
                return storageUser.token;
            }
            catch (err) {
                console.error('Error getting user from local storage');
                return null;
            }
        }
    }
    /**
     * Returns an observable that emits logged in status.
     * @return {?}
     */
    isLoggedIn$() {
        return this.loggedIn$.asObservable();
    }
    /**
     * Logs in using the passed in provider name.
     * @param {?} provider Provider to use to login.
     * @param {?=} data Data to be passed on to the provider.
     * @return {?}
     */
    loginWithProvider(provider, data) {
        return __awaiter(this, void 0, void 0, /** @this {!AuthService} */ function* () {
            if (provider === 'local') {
                /** @type {?} */
                const serverUser = yield this.http.post(apiRoute + 'users/signin', data).toPromise();
                this.setUser(serverUser);
                return this.user;
            }
            else {
                window.location.href = apiRoute + 'oauths/' + provider;
            }
        });
    }
    /**
     * Signs up user using the passed in provider name.
     * @param {?} provider Provider to use to sign up.
     * @param {?=} data Data to be passed on to the provider.
     * @param {?=} customRoute Custom route to use instead of the default 'users/signup'.
     * @return {?}
     */
    signupWithProvider(provider, data, customRoute) {
        return __awaiter(this, void 0, void 0, /** @this {!AuthService} */ function* () {
            if (provider === 'local') {
                /** @type {?} */
                const route = apiRoute + customRoute || 'users/signup';
                /** @type {?} */
                const serverUser = yield this.http.post(route, data).toPromise();
                if (data.noLogin) {
                    return serverUser;
                }
                else {
                    this.setUser(serverUser);
                    return this.user;
                }
            }
            else {
                window.location.href = webAlias + '/auth/' + provider;
            }
        });
    }
    /**
     * Switch current user.
     * @param {?} newUser
     * @return {?}
     */
    switchUser(newUser) {
        this.setUser(newUser);
    }
    /**
     * Logs out the current user.
     * @return {?}
     */
    logout() {
        this.setUser(null);
    }
}
AuthService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ AuthService.ngInjectableDef = ɵɵdefineInjectable({ factory: function AuthService_Factory() { return new AuthService(ɵɵinject(HttpClient)); }, token: AuthService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A route guard that rejects user from accessing the route if they're not logged in.
 *
 * Can also allows only specific user role from accessing the route based on the route
 * data's `guardOpt` field. See {\@link AuthGuardOptions}.
 */
class AuthGuard {
    /**
     * \@internal Do not use!
     * @param {?} router
     * @param {?} auth
     */
    constructor(router, auth) {
        this.router = router;
        this.auth = auth;
    }
    /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    canActivate(next, state) {
        return this.auth.isLoggedIn$()
            .pipe(take(1), map((/**
         * @param {?} isLoggedIn
         * @return {?}
         */
        (isLoggedIn) => {
            if (next.data && next.data.guardOpt) {
                /** @type {?} */
                const opts = (/** @type {?} */ (next.data.guardOpt));
                if (isLoggedIn) {
                    /** @type {?} */
                    const user = this.auth.getUser();
                    if (opts.role) {
                        if (user.roles.includes(opts.role)) {
                            return true;
                        }
                        else {
                            alert('Access Denied');
                            if (opts.logout) {
                                this.auth.logout();
                                if (opts.logoutUrl) {
                                    window.location.href = opts.logoutUrl;
                                }
                                else {
                                    window.location.href = window.location.origin;
                                }
                            }
                            else {
                                this.router.navigate([this.router.url]);
                            }
                            return false;
                        }
                    }
                    return true;
                }
                else {
                    if (opts.logoutUrl) {
                        window.location.href = opts.logoutUrl;
                    }
                    else {
                        window.location.href = window.location.origin;
                    }
                    return false;
                }
            }
        })));
    }
}
AuthGuard.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthGuard.ctorParameters = () => [
    { type: Router },
    { type: AuthService }
];
/** @nocollapse */ AuthGuard.ngInjectableDef = ɵɵdefineInjectable({ factory: function AuthGuard_Factory() { return new AuthGuard(ɵɵinject(Router), ɵɵinject(AuthService)); }, token: AuthGuard, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Intercepts outgoing requests and adds token if needed.
 * Intercepts incoming responses and sets/removes user if needed.
 */
class AuthInterceptor {
    /**
     * \@internal Do not use!
     * @param {?} auth
     */
    constructor(auth) {
        this.auth = auth;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        // Intercept request and add token.
        /** @type {?} */
        let newUrl = req.url;
        /** @type {?} */
        let newHeaders = req.headers;
        if (req.url.startsWith(apiRoute)) {
            /** @type {?} */
            const token = this.auth.getToken();
            if (token) {
                newHeaders = newHeaders.set('Authorization', `Bearer ${token}`);
            }
            if (newUrl.indexOf('?') > -1) {
                newUrl = `${newUrl}&cache_bust=${(new Date()).getTime()}`;
            }
            else {
                newUrl = `${newUrl}?cache_bust=${(new Date()).getTime()}`;
            }
        }
        // Modify request and pass it to the next handler.
        req = req.clone({
            url: newUrl,
            headers: newHeaders
        });
        return next.handle(req)
            .pipe(tap((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            if (res instanceof HttpResponse) {
                // Intercept response and refresh user data if needed.
                if (req.url.startsWith(apiRoute)) {
                    if (res.headers.has('x-user-token-refresh') && res.headers.get('x-user-token-refresh') !== '') {
                        /** @type {?} */
                        const refreshedUser = JSON.parse(decodeURIComponent(res.headers.get('x-user-token-refresh')));
                        this.auth.setUser(refreshedUser);
                    }
                }
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        err => {
            // Intercept error response and logout user if session timed out.
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    if (err.headers.has('x-user-logout')) {
                        alert('Your Session has timed out please login again.');
                        this.auth.logout();
                        window.location.reload();
                    }
                }
            }
        })));
    }
}
AuthInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthInterceptor.ctorParameters = () => [
    { type: AuthService }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A service to do stuffs related to errors.
 */
class ErrorService {
    /**
     * \@internal Do not use!
     */
    constructor() { }
    /**
     * Normalizes error.
     *
     * Will also log HttpErrorResponse for debugging purpose before normalizing it.
     * @param {?} err Error to be normalized.
     * @return {?}
     */
    normalizeError(err) {
        if (err instanceof HttpErrorResponse) {
            console.error(err);
            // Check if error from Keps.
            if (err.error && err.error.errors && err.error.errors[0]) {
                /** @type {?} */
                const kepsErr = err.error.errors[0];
                err = new Error(kepsErr.friendly || kepsErr.message);
            }
            else {
                err = new Error(err.message);
            }
        }
        return err;
    }
}
ErrorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ErrorService.ctorParameters = () => [];
/** @nocollapse */ ErrorService.ngInjectableDef = ɵɵdefineInjectable({ factory: function ErrorService_Factory() { return new ErrorService(); }, token: ErrorService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A service to send requests to Keps server.
 */
class DataService {
    /**
     * \@internal Do not use!
     * @param {?} http
     * @param {?} errorService
     */
    constructor(http, errorService) {
        this.http = http;
        this.errorService = errorService;
        this.cache = {};
    }
    /**
     * Sends a GET request to a route of Keps server.
     * @template T
     * @param {?} route Route to get from.
     * @param {?=} id ID of an object.
     * @param {?=} useCache Whether we should use cache or not.
     * @return {?}
     */
    get(route, id, useCache) {
        return __awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                if (id) {
                    route += '/' + id;
                }
                /** @type {?} */
                const getFromCache = new Promise((/**
                 * @param {?} resolve
                 * @param {?} reject
                 * @return {?}
                 */
                (resolve, reject) => {
                    if (useCache && this.cache[route]) {
                        setTimeout((/**
                         * @return {?}
                         */
                        () => {
                            resolve(this.cache[route]);
                        }), 3000);
                    }
                }));
                /** @type {?} */
                const getFromKeps = this.http.get(apiRoute + route).toPromise()
                    .then((/**
                 * @param {?} result
                 * @return {?}
                 */
                result => {
                    this.cache[route] = result;
                    return result;
                }));
                return Promise.race([getFromCache, getFromKeps]);
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a POST request to a route of Keps server.
     * @template T
     * @param {?} route Route to post to.
     * @param {?} data Data to be passed on to the route.
     * @return {?}
     */
    post(route, data) {
        return __awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                data = this.normalizeData(data);
                return this.http.post(apiRoute + route, data).toPromise();
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a PUT request to a route of Keps server.
     * @template T
     * @param {?} route Route to put to.
     * @param {?} data Data to be passed on to the route.
     * @return {?}
     */
    put(route, data) {
        return __awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                data = this.normalizeData(data);
                return this.http.put(apiRoute + route, data).toPromise();
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a DELETE request to a route of Keps server.
     * @template T
     * @param {?} type Type of the object to delete (can also be just a route).
     * @param {?} data An ID string or a Keps object to be deleted.
     * @return {?}
     */
    delete(type, data) {
        return __awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                /** @type {?} */
                let route = apiRoute;
                if (typeof data === 'string') {
                    route += type + '/' + data;
                }
                else if (typeof data === 'object' && data._id) {
                    route += type + '/' + data._id;
                }
                else {
                    route += type;
                }
                return this.http.delete(route).toPromise();
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a request depending on the command to Keps server.
     * @template T
     * @param {?} command A Keps command (e.g. 'put.users.update').
     * @param {?} data Data to be passed on to the server.
     * @return {?}
     */
    call(command, data) {
        return __awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                // Parse the command.
                const [method, type, route] = command.split('.');
                if (!method || !type || !route) {
                    throw new Error('Invalid Keps command.');
                }
                // Build URL based on data.
                /** @type {?} */
                let url = apiRoute + type + 's/';
                if (data instanceof FormData) {
                    /** @type {?} */
                    const id = data.get(type);
                    if (id) {
                        data.delete(type);
                        url += id + '/' + route;
                    }
                    else {
                        url += route;
                    }
                }
                else if (typeof data === 'object') {
                    if (data[type]) {
                        /** @type {?} */
                        const typeData = data[type];
                        if (typeof typeData === 'object') {
                            url += typeData._id + '/' + route;
                            delete data[type];
                        }
                        else if (typeof typeData === 'string') {
                            url += typeData + '/' + route;
                            delete data[type];
                        }
                        else {
                            url += route;
                        }
                    }
                    else {
                        url += route;
                    }
                }
                else {
                    url += route;
                }
                // Truncate URL for specific routes.
                if (['query', 'read', 'delete', 'update', 'create'].includes(route)) {
                    url = url.substr(0, url.length - route.length - 1);
                }
                // Send out requests.
                if (method === 'get' || method === 'delete') {
                    url += '?' + this.serializeData(data);
                    return this.http.request(method.toUpperCase(), url).toPromise();
                }
                else if (method === 'post' || method === 'put') {
                    return this.http.request(method.toUpperCase(), url, { body: data }).toPromise();
                }
                else {
                    return this.http.get(url).toPromise();
                }
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a GraphQL query request to the Keps server and returns the result back.
     *
     * Will throw an error if the result contains any error.
     * @param {?} query A GraphQL query.
     * @return {?}
     */
    graphql(query) {
        return __awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                // Clean up whitespaces in query.
                /** @type {?} */
                const cleanQuery = query.replace(/([^"]+)|("[^"]+")/g, (/**
                 * @param {?} $0
                 * @param {?} $1
                 * @param {?} $2
                 * @return {?}
                 */
                ($0, $1, $2) => {
                    if ($1) {
                        return $1.replace(/\s/g, '');
                    }
                    else {
                        return $2;
                    }
                }));
                // Send a GraphQL request and throw error if any.
                /** @type {?} */
                const result = yield this.http.get(apiRoute + `graphqls?q={${cleanQuery}}`).toPromise();
                if (result.errors) {
                    throw new Error(result.errors[0]);
                }
                return result;
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Clears all stored cache.
     * @return {?}
     */
    clearCache() {
        this.cache = {};
    }
    /**
     * Converts data to FormData if it contains a File or Blob.
     * @private
     * @param {?} data Data to be normalized.
     * @return {?}
     */
    normalizeData(data) {
        if (!(data instanceof FormData)) {
            // Check if data contains a File or Blob.
            if (Object.values(data).some((/**
             * @param {?} value
             * @return {?}
             */
            value => value instanceof File || value instanceof Blob))) {
                // Convert to FormData and return it.
                /** @type {?} */
                const formData = new FormData();
                for (const [key, value] of Object.entries(data)) {
                    formData.set(key, value);
                }
                return formData;
            }
        }
        return data;
    }
    /**
     * Serializes data and returns it as a query string.
     * @private
     * @param {?} data Data to be serialized.
     * @param {?=} prefix Prefix for the passed data.
     * @return {?}
     */
    serializeData(data, prefix) {
        /** @type {?} */
        const str = [];
        for (const p in data) {
            /** @type {?} */
            const k = prefix ? prefix + '[' + p + ']' : p;
            /** @type {?} */
            const v = data[p];
            str.push(typeof v === 'object' ?
                this.serializeData(v, k) :
                encodeURIComponent(k) + '=' + encodeURIComponent(v));
        }
        return str.join('&');
    }
}
DataService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
DataService.ctorParameters = () => [
    { type: HttpClient },
    { type: ErrorService }
];
/** @nocollapse */ DataService.ngInjectableDef = ɵɵdefineInjectable({ factory: function DataService_Factory() { return new DataService(ɵɵinject(HttpClient), ɵɵinject(ErrorService)); }, token: DataService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A component that wraps around an Angular Material dialog.
 *
 * Use the {\@link PopupService} to create this dialog.
 */
class DialogComponent {
    /**
     * \@internal Do not use!
     * @param {?} dialogRef
     * @param {?} data
     */
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.buttons = this.data.buttons || [];
        if (this.data.showClose !== undefined) {
            this.showClose = this.data.showClose;
        }
        else {
            this.showClose = !(this.buttons.length > 0);
        }
    }
}
DialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-dialog',
                template: "<section class=\"mat-typography\">\r\n  <h1 mat-dialog-title>{{ data.title }}</h1>\r\n  <div mat-dialog-content>\r\n    <p>{{ data.text }}</p>\r\n  </div>\r\n  <div mat-dialog-actions class=\"dialog-actions\">\r\n    <button\r\n      *ngFor=\"let button of buttons\"\r\n      [mat-dialog-close]=\"button.result\"\r\n      mat-button [color]=\"button.color\"\r\n    >\r\n     {{ button.label }}\r\n    </button>\r\n    <button\r\n      *ngIf=\"showClose\"\r\n      [mat-dialog-close]=\"undefined\"\r\n      mat-button\r\n    >\r\n      Close\r\n    </button>\r\n  </div>\r\n</section>\r\n",
                styles: [""]
            }] }
];
/** @nocollapse */
DialogComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A service to show custom dialogs and toasts.
 */
class PopupService {
    /**
     * \@internal Do not use!
     * @param {?} dialog
     * @param {?} toast
     */
    constructor(dialog, toast) {
        this.dialog = dialog;
        this.toast = toast;
    }
    /**
     * Shows a custom dialog.
     *
     * @param {?} data
     * Data to be shown on the dialog.
     * See {\@link DialogData}.
     *
     * @param {?=} options
     * Additional options for the dialog.
     * See [MatDialogConfig](https://material.angular.io/components/dialog/api#MatDialogConfig).
     *
     * @return {?}
     * The value set on the dialog buttons.
     * If the user clicks the 'Close' button or outside the dialog, it will return undefined.
     */
    showDialog(data, options = {}) {
        options.data = data;
        return this.dialog
            .open(DialogComponent, options)
            .afterClosed()
            .toPromise();
    }
    /**
     * Show a custom toast.
     * @param {?} message Text to be shown on the toast.
     * @param {?=} duration How long the toast should be shown.
     * @return {?}
     */
    showToast(message, duration = 3000) {
        this.toast.open(message, null, { duration });
    }
}
PopupService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PopupService.ctorParameters = () => [
    { type: MatDialog },
    { type: MatSnackBar }
];
/** @nocollapse */ PopupService.ngInjectableDef = ɵɵdefineInjectable({ factory: function PopupService_Factory() { return new PopupService(ɵɵinject(MatDialog$1), ɵɵinject(MatSnackBar$1)); }, token: PopupService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A pipe that transforms a Keps address object to a nicely formatted string.
 */
class AddressPipe {
    /**
     * @param {?} address
     * @param {?=} lines
     * @return {?}
     */
    transform(address, lines = 4) {
        /** @type {?} */
        let str = '';
        if (!address) {
            return str;
        }
        str += address.address1;
        if (lines === 4 && address.address2) {
            str += `\n${address.address2}`;
        }
        else if (lines === 3 && address.address2) {
            str += `, ${address.address2}`;
        }
        str += `\n${address.city}, ${address.region} ${address.postal}`;
        str += `\n${address.country}`;
        return str;
    }
}
AddressPipe.decorators = [
    { type: Pipe, args: [{
                name: 'address'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A pipe to evaluate an expression from supplied context.
 */
class EvalPipe {
    /**
     * @param {?} context
     * @param {?} expression
     * @return {?}
     */
    transform(context, expression) {
        // If expression is not a string, return the context.
        if (typeof expression !== 'string') {
            return context;
        }
        // Convert expression to tokens.
        /** @type {?} */
        let tokens = this.getTokens(expression);
        // Parse each token based on the context.
        tokens = tokens.map((/**
         * @param {?} token
         * @return {?}
         */
        token => this.parseToken(token, context)));
        // Combine tokens back as a string.
        return this.combineTokens(tokens);
    }
    /**
     * @private
     * @param {?} str
     * @return {?}
     */
    getTokens(str) {
        return str.match(/[^\s"']+|"([^"]*)"|'([^']*)'/gm);
    }
    /**
     * @private
     * @param {?} token
     * @param {?} context
     * @return {?}
     */
    parseToken(token, context) {
        if ((token[0] === '"' && token[token.length - 1] === '"') ||
            (token[0] === `'` && token[token.length - 1] === `'`)) {
            // If token is a quoted string, return the text inside the quotes.
            return token.substring(1, token.length - 1);
        }
        else if (token === '+') {
            // Return '+' as it is.
            return token;
        }
        else if (token[0] === '.') {
            // If the first character is a dot, continue parsing after the dot.
            return this.parseToken(token.substring(1), context);
        }
        else {
            // If context is missing, log the error and return null.
            if (context === null) {
                this.parseError('No context supplied.');
                return null;
            }
            // Check for special characters.
            /** @type {?} */
            const specials = token.match(/(\[|\.)/gm);
            if (specials && specials.length > 0) {
                if (specials[0] === '[') {
                    /** @type {?} */
                    const inbetween = token.match(/\[(\S)\]/);
                    if (inbetween && inbetween.length > 0) {
                        /** @type {?} */
                        const num = parseInt(inbetween[1], 10);
                        /** @type {?} */
                        const before = token.substring(0, token.indexOf('['));
                        if (!context[before]) {
                            this.parseError(`${before} not found`);
                            return null;
                        }
                        /** @type {?} */
                        const after = token.substring(token.indexOf(']') + 1);
                        /** @type {?} */
                        let parsed;
                        if (isNaN(num)) {
                            parsed = context[before][inbetween[1]];
                        }
                        else {
                            parsed = context[before][num];
                        }
                        if (after.length > 0) {
                            return this.parseToken(after, parsed);
                        }
                        else {
                            return parsed;
                        }
                    }
                    else {
                        this.parseError('No closing bracket.');
                        return null;
                    }
                }
                else {
                    /** @type {?} */
                    const i = token.indexOf('.');
                    return this.parseToken(token.substring(i + 1), context[token.substring(0, i)]);
                }
            }
            else {
                return context[token];
            }
        }
    }
    /**
     * @private
     * @param {?} tokens
     * @return {?}
     */
    combineTokens(tokens) {
        /** @type {?} */
        let result;
        for (let i = 0; i < tokens.length; i++) {
            if (i % 2 === 1) {
                if (tokens[i] !== '+') {
                    this.parseError('error');
                    return null;
                }
            }
            else {
                if (!result) {
                    result = tokens[i];
                }
                else {
                    result += tokens[i];
                }
            }
        }
        return result;
    }
    /**
     * @private
     * @param {?} message
     * @return {?}
     */
    parseError(message) {
        console.error('EvalPipeError: ' + message);
    }
}
EvalPipe.decorators = [
    { type: Pipe, args: [{
                name: 'eval'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A pipe that transforms the keys of an object to an array of strings.
 */
class KeysPipe {
    /**
     * @param {?} obj
     * @return {?}
     */
    transform(obj) {
        return Object.keys(obj);
    }
}
KeysPipe.decorators = [
    { type: Pipe, args: [{
                name: 'keys'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A pipe that transforms UNIX timestamp to a nice moment-formatted string.
 */
class MomentPipe {
    /**
     * @param {?} timestamp
     * @param {?=} format
     * @return {?}
     */
    transform(timestamp, format = 'MM/DD/YYYY') {
        return moment(timestamp).format(format);
    }
}
MomentPipe.decorators = [
    { type: Pipe, args: [{
                name: 'moment'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A pipe that transforms a camel-cased string to words separated by space.
 */
class SplitCamelPipe {
    /**
     * @param {?} str
     * @return {?}
     */
    transform(str) {
        /** @type {?} */
        const re = /([A-Z])([A-Z])([a-z])|([a-z])([A-Z])|([a-z])([0-9])/g;
        return str.replace(re, '$1$4$6 $2$3$5$7');
    }
}
SplitCamelPipe.decorators = [
    { type: Pipe, args: [{
                name: 'splitCamel'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A component that wraps around the default HTML file select button.
 */
class FileSelectComponent {
    /**
     * \@internal Do not use!
     */
    constructor() {
        /**
         * File to be selected.
         */
        this.selectedFile = null;
        /**
         * An event emitter that emits the selected file.
         */
        this.selectedFileChange = new EventEmitter();
        /**
         * Label for the file select button.
         */
        this.label = 'Choose File';
        /**
         * Whether the component should display the file name.
         */
        this.showFilename = true;
        /**
         * The type(s) of file that the file select dialog can accept.
         *
         * See: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#Unique_file_type_specifiers
         *
         * If the value is "image", "video", or "audio", it will accept every possible file types
         * of that media type.
         */
        this.type = '';
        /**
         * Angular Material color for the file select button.
         */
        this.color = 'primary';
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        if (this.type === 'image' ||
            this.type === 'video' ||
            this.type === 'audio') {
            this.type += '/*';
        }
    }
    /**
     * Open the file select dialog manually.
     * @return {?}
     */
    openFileSelect() {
        this.fileSelector.nativeElement.click();
    }
    /**
     * Select a file.
     * @param {?} files Files to be passed. Only the first one will be selected.
     * @return {?}
     */
    selectFile(files) {
        if (files && files.length > 0) {
            this.selectedFile = files[0];
            this.selectedFileChange.emit(this.selectedFile);
        }
    }
}
FileSelectComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-file-select',
                template: "<button\r\n  (click)=\"openFileSelect()\"\r\n  mat-raised-button [color]=\"color\"\r\n>\r\n  {{ label }}\r\n</button>\r\n<span *ngIf=\"showFilename\" class=\"file-name\">\r\n  {{ selectedFile ? selectedFile.name : 'No file chosen.' }}\r\n</span>\r\n<input\r\n  #fileSelector\r\n  (change)=\"selectFile($event.target.files)\"\r\n  type=\"file\"\r\n  [attr.accept]=\"type\"\r\n  hidden\r\n/>\r\n",
                styles: [""]
            }] }
];
/** @nocollapse */
FileSelectComponent.ctorParameters = () => [];
FileSelectComponent.propDecorators = {
    selectedFile: [{ type: Input }],
    selectedFileChange: [{ type: Output }],
    label: [{ type: Input }],
    showFilename: [{ type: Input }],
    type: [{ type: Input }],
    color: [{ type: Input }],
    fileSelector: [{ type: ViewChild, args: ['fileSelector', { static: true },] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A Keps form.
 *
 * It extends Angular's `FormGroup` class
 * and changes its `patchValue` and `reset` methods to
 * wait until the form is initialized.
 */
class KepsForm extends FormGroup {
    constructor() {
        super(...arguments);
        this.initialized = false;
        this.initialized$ = new BehaviorSubject(false);
    }
    /**
     * Lets the Keps form know that is has been initialized.
     * \@internal Do not use!
     * @return {?}
     */
    initialize() {
        this.initialized = true;
        this.initialized$.next(true);
    }
    /**
     * Sets the schema for this form.
     * \@internal Do not use!
     * @param {?} schema Schema to be used.
     * @return {?}
     */
    setSchema(schema) {
        this.schema = schema;
    }
    /**
     * Patches the value of the Keps form.
     * It accepts an object with control names as keys, and does its best to match the values to the correct controls in the group.
     *
     * It accepts both super-sets and sub-sets of the group without throwing an error.
     * @param {?} values Values to be passed to the fields.
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    patchValue(values, options = {}) {
        // Check if form is initialized or not.
        if (this.initialized) {
            if (options === void 0) {
                options = {};
            }
            for (const field of Object.keys(values)) {
                /** @type {?} */
                let value = values[field];
                // If value is an object with an ID, convert to ID reference.
                if (_.isPlainObject(value) && value._id) {
                    value = value._id;
                }
                // Pass in value to child controls.
                if (this.get(field)) {
                    this.get(field).patchValue(value, { onlySelf: true, emitEvent: options.emitEvent });
                }
            }
            this.updateValueAndValidity(options);
        }
        else {
            // If form is not initialized, wait until it is before patching.
            this.initialized$
                .pipe(first((/**
             * @param {?} value
             * @return {?}
             */
            value => value)))
                .subscribe((/**
             * @param {?} value
             * @return {?}
             */
            value => {
                this.patchValue(values, options);
            }));
        }
    }
    /**
     * Resets the Keps form, marks all descendants are marked pristine and untouched, and the value of all descendants to null.
     *
     * You reset to a specific form state by passing in a map of states that matches the structure of your form, with control names as keys.
     * The state is a standalone value or a form state object with both a value and a disabled status.
     * @param {?=} values Resets the control with an initial value, or an object that defines the initial value and disabled state.
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    reset(values, options = {}) {
        if (this.initialized) {
            if (values === void 0) {
                values = {};
            }
            // Reset each fields in the control.
            for (const field in this.controls) {
                // If no value supplied for this field, try using the schema's default value.
                if (!values[field] && this.schema[field] && this.schema[field].default) {
                    values[field] = this.schema[field].default;
                }
                this.get(field).reset(values[field], { onlySelf: true, emitEvent: options.emitEvent });
            }
            this.markAsPristine(options);
            this.markAsUntouched(options);
            this.updateValueAndValidity(options);
        }
        else {
            // If form is not initialized, wait until it is before reseting.
            this.initialized$
                .pipe(first((/**
             * @param {?} value
             * @return {?}
             */
            value => value)))
                .subscribe((/**
             * @param {?} value
             * @return {?}
             */
            value => {
                this.reset(values, options);
            }));
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A Keps form for array field.
 *
 * It extends Angular's `FormArray` class
 * and changes its `patchValue` and `reset` methods to
 * deal with arrays appropriately.
 */
class KepsFormArray extends FormArray {
    /**
     * Set an optional BehaviorSubject counter.
     * @param {?} counter$ A BehaviorSubject from parent component.
     * @return {?}
     */
    setCounter$(counter$) {
        this.counter$ = counter$;
    }
    /**
     * Patches the value of the `KepsFormArray`.
     * It accepts an array that matches the structure of the control,
     * and does its best to match the values to the correct controls in the group.
     *
     * It accepts both super-sets and sub-sets of the array without throwing an error.
     * @param {?} values Array of latest values for the controls
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    patchValue(values, options = {}) {
        values = values.map((/**
         * @param {?} value
         * @return {?}
         */
        value => {
            if (_.isPlainObject(value) && value._id && value._id) {
                return value._id;
            }
            else {
                return value;
            }
        }));
        // Reset the form first.
        this.reset();
        // Add each form control with value and counter.
        for (let i = 0; i < values.length; i++) {
            /** @type {?} */
            const value = values[i];
            // If value is an object, then we assume that we're making a KepsForm.
            if (_.isPlainObject(value)) {
                this.push(new KepsForm({}));
                this.at(this.length - 1).patchValue(value);
            }
            else {
                this.push(new FormControl(value));
            }
        }
        // Let the parent component know that the length changed.
        // The `setTimeout` is here in case the `setCounter$` function is called after `patchValue`.
        setTimeout((/**
         * @return {?}
         */
        () => {
            if (this.counter$) {
                this.counter$.next(this.length);
            }
        }), 5);
        this.updateValueAndValidity(options);
    }
    /**
     * Resets the FormArray and all descendants are marked `pristine` and `untouched`,
     * and the value of all descendants to null or null maps.
     *
     * You reset to a specific form state by passing in an array of states that matches the structure of the control.
     * The state is a standalone value or a form state object with both a value and a disabled status.
     * @param {?=} values Array of latest values for the controls
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    reset(values, options = {}) {
        while (this.length > 0) {
            this.removeAt(0);
        }
        if (this.counter$) {
            this.counter$.next(0);
        }
        this.markAsPristine(options);
        this.markAsUntouched(options);
        this.updateValueAndValidity(options);
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A helper function that checks for a schema's field type.
 * \@param schema A Keps schema.
 * \@param field A field name.
 * \@param type A type name to be checked.
 * @type {?}
 */
const isType = (/**
 * @param {?} schema
 * @param {?} field
 * @param {?} type
 * @return {?}
 */
(schema, field, type) => {
    /** @type {?} */
    const fieldType = schema[field].type;
    if (type === 'reference') {
        return fieldType[0] === ':';
    }
    if (type === 'array-reference') {
        return fieldType === 'array' && schema[field].subSchema[0] === ':';
    }
    if (type === 'array-string') {
        return fieldType === 'array' && schema[field].subSchema === 'string';
    }
    // This is to check if table should render the object as is with no additional processing.
    if (type === 'table-normal') {
        return !(isType(schema, field, 'reference') ||
            isType(schema, field, 'array-reference') ||
            isType(schema, field, 'array-string') ||
            fieldType === 'address' ||
            fieldType === 'datetime' ||
            fieldType === 'enum' ||
            fieldType === 'number');
    }
    return fieldType === type;
});
/**
 * A helper function to get the display expression of a field in schema.
 * \@param models Models from the project to get the reference from.
 * \@param schema A Keps schema.
 * \@param field A field name.
 * @type {?}
 */
const getReferenceDisplay = (/**
 * @param {?} models
 * @param {?} schema
 * @param {?} field
 * @return {?}
 */
(models, schema, field) => {
    /** @type {?} */
    const referenceTo = schema[field].type.slice(1);
    if (models[referenceTo]) {
        return models[referenceTo].properties.displayExpression;
    }
    return null;
});
/**
 * A helper function to get the display expression of an array field in schema.
 * \@param models Models from the project to get the reference from.
 * \@param schema A Keps schema.
 * \@param field A field name.
 * @type {?}
 */
const getReferenceDisplayArray = (/**
 * @param {?} models
 * @param {?} schema
 * @param {?} field
 * @return {?}
 */
(models, schema, field) => {
    if (schema[field].type === 'array' &&
        typeof schema[field].subSchema === 'string' &&
        schema[field].subSchema[0] === ':') {
        /** @type {?} */
        const referenceTo = ((/** @type {?} */ (schema[field].subSchema))).slice(1);
        if (models[referenceTo]) {
            return models[referenceTo].properties.displayExpression;
        }
    }
    return null;
});

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A component that renders form based on Keps schema.
 */
class FormComponent {
    /**
     * \@internal Do not use!
     * @param {?} models
     */
    constructor(models) {
        this.models = models;
        /**
         * An instance of Keps form.
         */
        this.form = new KepsForm({});
        /**
         * A map of reference to Keps objects to be shown for reference fields.
         *
         * Notes:-The key of the map is based on the field name of the Keps form model.
         *        Not the name of the model it is referenced to.
         *       -If field type is a string and a refObjs is provided,
         *        instead of a normal text input, it will be a select. see html for more info.
         *
         * **sample usage**
         * given the following models:
         *  PERSON {
         *    _id
         *    name: 'string',
         *    pet: ':ANIMAL'
         *  }
         *
         *  ANIMAL {
         *    _id
         *    type: 'string'
         *  }
         *
         *  if one wish to create a form for
         *  object PERSON where the options for ANIMAL
         *  comes from DB, that dev can create a refObjs:KepsReference
         *  which  will look like:
         *
         *  refObjs = {
         *              'pet': [
         *                <ANIMAL>{
         *                    _id
         *                    type
         *                  },
         *                <ANIMAL>{
         *                    _id
         *                    type
         *                },
         *                ...
         *                ...
         *                ..
         *              ],
         *            }
         *
         *  SIDENOTE: The annotation <TYPE> is used to make this exmaple more explicit.
         */
        this.refObjs = {};
        /**
         * Appearance style of the form.
         */
        this.appearance = 'standard';
        /**
         * Angular Material color of the form field underline and floating label.
         */
        this.color = 'primary';
        /**
         * Fields to be displayed on the form.
         */
        this.displayedFields = [];
        /**
         * Fields to be hidden on the form.
         */
        this.hiddenFields = [];
        /**
         * Placeholders to be displayed on fields.
         */
        this.placeholders = {};
        /**
         * Hints for fields input.
         */
        this.hints = {};
        /**
         * Whether the form has done initializing or not.
         */
        this.initialized = false;
    }
    /**
     * Initializes the form with fields based on the schema.
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        // Get schema from model name if supplied.
        if (this.model && this.models[this.model]) {
            this.schema = this.models[this.model].schema;
        }
        else if (this.model && !this.models[this.model]) {
            throw new Error(`Can't find a model named ${this.model}!`);
        }
        // Assert schema must exist.
        if (!this.schema) {
            throw new Error(`Must supply valid model name or schema to Keps form!`);
        }
        // Set schema fields.
        if (this.displayedFields.length > 0) {
            this.schemaFields = this.displayedFields;
        }
        else {
            this.schemaFields = Object.keys(this.schema)
                .filter((/**
             * @param {?} field
             * @return {?}
             */
            field => field[0] !== '_' && !this.hiddenFields.includes(field)));
        }
        this.form.setSchema(this.schema);
        for (const fieldName of this.schemaFields) {
            /** @type {?} */
            const field = this.schema[fieldName];
            // Create validators if needed.
            /** @type {?} */
            const validators = [];
            if (!field.optional) {
                validators.push(Validators.required);
            }
            if (field.type === 'number') ;
            if (field.type === 'email') {
                validators.push(Validators.email);
            }
            // Create the appropriate form control for each field.
            if (field.type === 'address') {
                this.form.addControl(fieldName, new FormGroup({}, validators));
            }
            else if (field.type === 'array') {
                this.form.addControl(fieldName, new KepsFormArray([], validators));
            }
            else if (field.type === 'datetime') {
                this.form.addControl(fieldName, new FormControl(null, validators));
            }
            else {
                this.form.addControl(fieldName, new FormControl(field.default || null, validators));
            }
        }
        this.form.initialize();
        this.initialized = true;
    }
    /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    isType(field, type) {
        return isType(this.schema, field, type);
    }
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplay(field) {
        return getReferenceDisplay(this.models, this.schema, field);
    }
    /**
     * Check if MatFormField should hide the underline or not.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @return {?}
     */
    isNotUnderlined(field) {
        return this.schema[field].type === 'boolean' ||
            this.schema[field].type === 'address' ||
            this.schema[field].type === 'array';
    }
    /**
     * Check if a field has a reference in refObjs.
     * if has reference in refobjs, use mat-select instead of input.
     * supports only type string at the moment.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @return {?}
     */
    hasReference(field) {
        return this.schema[field].type === 'string' &&
            this.refObjs &&
            this.refObjs[field];
    }
}
FormComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-form',
                template: "<form\r\n  *ngIf=\"initialized\"\r\n  [formGroup]=\"form\" \r\n  class=\"keps-form mat-typography\"\r\n>\r\n  <ng-container *ngFor=\"let fieldName of schemaFields\">\r\n    <ng-container *ngIf=\"schema[fieldName]; let field\">\r\n      <mat-form-field\r\n        floatLabel=\"always\"\r\n        [appearance]=\"appearance\"\r\n        [color]=\"color\"\r\n        [class.hide-underline]=\"isNotUnderlined(fieldName)\"\r\n      >\r\n        <mat-label>\r\n          {{ field.label || fieldName | splitCamel | titlecase }}\r\n        </mat-label>\r\n  \r\n        <!-- Type: String -->\r\n        <!-- reference is not provided, use input-->\r\n        <input\r\n          *ngIf=\"isType(fieldName, 'string') && !hasReference(fieldName)\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          type=\"text\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n          matInput\r\n        >\r\n        <!-- Type: String -->\r\n        <!-- If reference is provided, use select -->\r\n        <mat-select \r\n          *ngIf=\"isType(fieldName, 'string') && hasReference(fieldName)\" \r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\" >\r\n          <mat-option *ngIf=\"field.optional\">None</mat-option>\r\n          <mat-option *ngFor=\"let option of refObjs[fieldName]\" [value]=\"option\">{{option}}</mat-option>\r\n        </mat-select>\r\n  \r\n        <!-- Type: Number -->\r\n        <input\r\n          *ngIf=\"isType(fieldName, 'number')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          type=\"number\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n          [min]=\"field.min\"\r\n          [max]=\"field.max\"\r\n          [step]=\"field.step\"\r\n          matInput\r\n        >\r\n\r\n        <!-- Type: Email -->\r\n        <input\r\n          *ngIf=\"isType(fieldName, 'email')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          type=\"email\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n          matInput\r\n        >\r\n\r\n        <!-- Type: Boolean -->\r\n        <keps-field-boolean\r\n          *ngIf=\"isType(fieldName, 'boolean')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n        >\r\n        </keps-field-boolean>\r\n\r\n        <!-- Type: Enum & Multi -->\r\n        <mat-select\r\n          *ngIf=\"isType(fieldName, 'enum') || isType(fieldName, 'multi')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          [multiple]=\"isType(fieldName, 'multi')\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n        >\r\n          <mat-option\r\n            *ngFor=\"let option of field.options\"\r\n            [value]=\"option\"\r\n          >\r\n            {{ field.labels && field.labels[option] ? field.labels[option] : option }}\r\n          </mat-option>\r\n        </mat-select>\r\n\r\n        <!-- Type: Address -->\r\n        <keps-field-address\r\n          *ngIf=\"isType(fieldName, 'address')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          [appearance]=\"appearance\"\r\n          [color]=\"color\"\r\n        >\r\n        </keps-field-address>\r\n\r\n        <!-- Type: Array -->\r\n        <keps-field-array\r\n          *ngIf=\"isType(fieldName, 'array')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [subSchema]=\"field.subSchema\"\r\n          [refObjs]=\"refObjs[field]\"\r\n          [required]=\"!field.optional\"\r\n          [appearance]=\"appearance\"\r\n          [color]=\"color\"\r\n        >\r\n        </keps-field-array>\r\n\r\n        <!-- Type: Reference -->\r\n        <mat-select\r\n          *ngIf=\"isType(fieldName, 'reference')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n        >\r\n          <mat-option\r\n            *ngFor=\"let option of refObjs[fieldName]\"\r\n            [value]=\"option._id\"\r\n          >\r\n            {{ option | eval:getReferenceDisplay(fieldName) }}\r\n          </mat-option>\r\n        </mat-select>\r\n\r\n        <!-- Type: Datetime -->\r\n        <keps-field-datetime\r\n          *ngIf=\"isType(fieldName, 'datetime')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n        >\r\n        </keps-field-datetime>\r\n\r\n        <!-- Hints -->\r\n        <mat-hint *ngIf=\"hints\">{{hints[fieldName]}}</mat-hint>\r\n\r\n      </mat-form-field>\r\n    </ng-container>\r\n  </ng-container>\r\n</form>\r\n",
                styles: [".keps-form{display:flex;flex-direction:column}.keps-form>*{margin-bottom:1em}.keps-form-radio-group{display:inline-flex;flex-direction:column}.keps-form-radio-group>*{margin-bottom:.5em}.hide-underline::ng-deep .mat-form-field-underline{display:none!important}"]
            }] }
];
/** @nocollapse */
FormComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] }
];
FormComponent.propDecorators = {
    form: [{ type: Input }],
    model: [{ type: Input }],
    schema: [{ type: Input }],
    refObjs: [{ type: Input }],
    appearance: [{ type: Input }],
    color: [{ type: Input }],
    displayedFields: [{ type: Input }],
    hiddenFields: [{ type: Input }],
    placeholders: [{ type: Input }],
    hints: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FieldAddressComponent {
    /**
     * \@internal Do not use!
     * @param {?} fm
     * @param {?} elRef
     * @param {?} ngControl
     */
    constructor(fm, elRef, ngControl) {
        this.fm = fm;
        this.elRef = elRef;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-address';
        /**
         * \@internal Do not use!
         */
        this.id = `keps-field-address-${FieldAddressComponent.nextId++}`;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new FormGroup({});
        /**
         * Appearance style of the form.
         */
        this.appearance = 'standard';
        /**
         * Angular Material color of the form field underline and floating label.
         */
        this.color = 'primary';
        /**
         * Whether the field has been initialized or not.
         * \@internal Do not use!
         */
        this.initialized = false;
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        }));
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const validators = [];
        if (this.required) {
            validators.push(Validators.required);
        }
        for (const field of ['address1', 'address2', 'city', 'region', 'postal', 'country']) {
            this.form.addControl(field, new FormControl(null, validators));
        }
        this.initialized = true;
    }
    /**
     * The value of the control.
     * @return {?}
     */
    get value() {
        return this.form.value;
    }
    /**
     * \@internal Do not use!
     * @param {?} v
     * @return {?}
     */
    set value(v) {
        this.form.patchValue(v);
        this.stateChanges.next();
    }
    /**
     * Whether the control is required.
     * @return {?}
     */
    get required() {
        return this._required;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set required(value) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * Whether the control is disabled.
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * The placeholder for this control.
     * @return {?}
     */
    get placeholder() {
        return this._placeholder;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }
    /**
     * Whether the control is empty.
     * @return {?}
     */
    get empty() {
        const { address1, address2, city, region, postal, country } = this.form.value;
        return !address1 && !address2 && !city && !region && !postal && !country;
    }
    /**
     * Whether the MatFormField label should try to float.
     * @return {?}
     */
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    setDescribedByIds(ids) {
        this.describedBy = ids.join(' ');
    }
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    onContainerClick(event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    }
}
/**
 * \@internal Do not use!
 */
FieldAddressComponent.nextId = 0;
FieldAddressComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-field-address',
                template: "<ng-container *ngIf=\"initialized\">\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Address 1\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('address1')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Address 2\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('address2')\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      City\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('city')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      State\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('region')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Zipcode\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('postal')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Country\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('country')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n</ng-container>\r\n",
                providers: [
                    {
                        provide: MatFormFieldControl,
                        useExisting: FieldAddressComponent
                    }
                ],
                styles: [".field-address{width:100%}.field-address::ng-deep .mat-form-field-underline{display:block!important}"]
            }] }
];
/** @nocollapse */
FieldAddressComponent.ctorParameters = () => [
    { type: FocusMonitor },
    { type: ElementRef },
    { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
];
FieldAddressComponent.propDecorators = {
    form: [{ type: Input }],
    appearance: [{ type: Input }],
    color: [{ type: Input }],
    value: [{ type: Input }],
    required: [{ type: Input }],
    disabled: [{ type: Input }],
    placeholder: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FieldArrayComponent {
    /**
     * \@internal Do not use!
     * @param {?} fm
     * @param {?} elRef
     * @param {?} models
     * @param {?} ngControl
     */
    constructor(fm, elRef, models, ngControl) {
        this.fm = fm;
        this.elRef = elRef;
        this.models = models;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-array';
        /**
         * \@internal Do not use!
         */
        this.id = `keps-field-array-${FieldArrayComponent.nextId++}`;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new KepsFormArray([]);
        /**
         * Subschema of the form array.
         */
        this.subSchema = 'string';
        /**
         * Objects to be shown for reference field.
         */
        this.refObjs = [];
        /**
         * Appearance style of the form.
         */
        this.appearance = 'standard';
        /**
         * Angular Material color of the form field underline and floating label.
         */
        this.color = 'primary';
        /**
         * A behavior subject that emits a number which is the length of the form array.
         *
         * It should emit whenever a form control is added/removed from the array.
         * \@internal Do not use!
         */
        this.counter$ = new BehaviorSubject(0);
        /**
         * An observable that emits an empty array with the same length as the form array.
         *
         * It is used for *ngFor in this component's HTML.
         * \@internal Do not use!
         */
        this.counterArr$ = this.counter$.pipe(map((/**
         * @param {?} value
         * @return {?}
         */
        value => {
            return new Array(value);
        })));
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        }));
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        this.form.setCounter$(this.counter$);
    }
    /**
     * Adds a form control to the form array.
     * @return {?}
     */
    add() {
        this.form.push(new FormControl(''));
        this.counter$.next(this.form.length);
    }
    /**
     * Removes a form control from the form array.
     * @param {?} index Index of the form control to be removed.
     * @return {?}
     */
    remove(index) {
        this.form.removeAt(index);
        this.focused = false;
        this.counter$.next(this.form.length);
    }
    /**
     * Rearranges form controls inside the form array.
     * \@internal Do not use!
     * @param {?} event The drag event containing the form control.
     * @return {?}
     */
    drop(event) {
        /** @type {?} */
        const prevIndex = event.previousIndex;
        /** @type {?} */
        const nextIndex = event.currentIndex;
        /** @type {?} */
        const movedForm = this.form.at(prevIndex);
        this.form.removeAt(prevIndex);
        this.form.insert(nextIndex, movedForm);
    }
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @return {?}
     */
    getReferenceDisplay() {
        /** @type {?} */
        const referenceTo = this.subSchema.slice(1);
        if (this.models && this.models[referenceTo]) {
            return this.models[referenceTo].properties.displayExpression;
        }
        return null;
    }
    /**
     * The value of the control.
     * @return {?}
     */
    get value() {
        return this.form.value;
    }
    /**
     * \@internal Do not use!
     * @param {?} v
     * @return {?}
     */
    set value(v) {
        this.form.patchValue(v);
        this.stateChanges.next();
    }
    /**
     * Whether the control is required.
     * @return {?}
     */
    get required() {
        return this._required;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set required(value) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * Whether the control is disabled.
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * The placeholder for this control.
     * @return {?}
     */
    get placeholder() {
        return this._placeholder;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }
    /**
     * Whether the control is empty.
     * @return {?}
     */
    get empty() {
        return !this.form.value || this.form.value.length === 0;
    }
    /**
     * Whether the MatFormField label should try to float.
     * @return {?}
     */
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    setDescribedByIds(ids) {
        this.describedBy = ids.join(' ');
    }
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    onContainerClick(event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    }
}
/**
 * \@internal Do not use!
 */
FieldArrayComponent.nextId = 0;
FieldArrayComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-field-array',
                template: "<div cdkDropList (cdkDropListDropped)=\"drop($event)\">\n  <div *ngFor=\"let c of counterArr$ | async; let i = index\" class=\"field-array-container\" cdkDrag>\n    <button cdkDragHandle mat-icon-button color=\"accent\" class=\"field-array-reorder\">\n      <mat-icon>reorder</mat-icon>\n    </button>\n    <mat-form-field\n      [appearance]=\"appearance\"\n      [color]=\"color\"\n      floatLabel=\"always\"\n      class=\"field-array\"\n    >\n      <!-- Type: String -->\n      <input\n        *ngIf=\"subSchema === 'string'\"\n        [formControl]=\"form.at(i)\"\n        [required]=\"required\"\n        type=\"text\"\n        matInput\n      >\n      <!-- Type: number -->\n      <input\n        *ngIf=\"subSchema === 'number'\"\n        [formControl]=\"form.at(i)\"\n        [required]=\"required\"\n        type=\"number\"\n        matInput\n      >\n      <!-- Type: Reference -->\n      <mat-select\n        *ngIf=\"subSchema[0] === ':'\"\n        [formControl]=\"form.at(i)\"\n        [required]=\"required\"\n      >\n        <mat-option\n          *ngFor=\"let option of refObjs\"\n          [value]=\"option._id\"\n        >\n          {{ option | eval:getReferenceDisplay() }}\n        </mat-option>\n      </mat-select>\n\n      <!-- TODO: If subschema is another schema object, we should make something here. -->\n    </mat-form-field>\n    <button (click)=\"remove(i)\" mat-icon-button color=\"warn\" class=\"field-array-delete\">\n      <mat-icon>delete</mat-icon>\n    </button>\n  </div>\n</div>\n\n<button (click)=\"add()\" mat-icon-button color=\"accent\">\n  <mat-icon>add</mat-icon>\n</button>\n  ",
                providers: [
                    {
                        provide: MatFormFieldControl,
                        useExisting: FieldArrayComponent
                    }
                ],
                styles: [".field-array-container{width:100%;display:flex;flex-direction:row;justify-content:center}.field-array-reorder{max-width:30px}.field-array-delete{max-width:20px}.field-array-container::ng-deep mat-form-field{flex:1 1 auto;margin-top:5px}.field-array::ng-deep .mat-form-field-underline{display:block!important}.field-array::ng-deep .mat-form-field-flex{padding-top:0!important}.field-array::ng-deep .mat-form-field-infix{border-top:0!important;width:100%}"]
            }] }
];
/** @nocollapse */
FieldArrayComponent.ctorParameters = () => [
    { type: FocusMonitor },
    { type: ElementRef },
    { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] },
    { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
];
FieldArrayComponent.propDecorators = {
    form: [{ type: Input }],
    subSchema: [{ type: Input }],
    refObjs: [{ type: Input }],
    appearance: [{ type: Input }],
    color: [{ type: Input }],
    value: [{ type: Input }],
    required: [{ type: Input }],
    disabled: [{ type: Input }],
    placeholder: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A custom field that displays two radio buttons: True, False.
 */
class FieldBooleanComponent {
    /**
     * \@internal Do not use!
     * @param {?} fm
     * @param {?} elRef
     * @param {?} ngControl
     */
    constructor(fm, elRef, ngControl) {
        this.fm = fm;
        this.elRef = elRef;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-boolean';
        /**
         * \@internal Do not use!
         */
        this.id = `keps-field-boolean-${FieldBooleanComponent.nextId++}`;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new FormControl(null);
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        }));
    }
    /**
     * \@internal Do not use!
     * @param {?} change
     * @return {?}
     */
    onRadioChange(change) {
        this.value = change.value;
    }
    /**
     * The value of the control.
     * @return {?}
     */
    get value() {
        return this.form.value;
    }
    /**
     * \@internal Do not use!
     * @param {?} b
     * @return {?}
     */
    set value(b) {
        this.form.patchValue(b);
        this.stateChanges.next();
    }
    /**
     * Whether the control is required.
     * @return {?}
     */
    get required() {
        return this._required;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set required(value) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * Whether the control is disabled.
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * The placeholder for this control.
     * @return {?}
     */
    get placeholder() {
        return this._placeholder;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }
    /**
     * Whether the control is empty.
     * @return {?}
     */
    get empty() {
        return this.form.value == null;
    }
    /**
     * Whether the MatFormField label should try to float.
     * @return {?}
     */
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    setDescribedByIds(ids) {
        this.describedBy = ids.join(' ');
    }
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    onContainerClick(event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    }
}
/**
 * \@internal Do not use!
 */
FieldBooleanComponent.nextId = 0;
FieldBooleanComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-field-boolean',
                template: "<mat-radio-button\r\n  [checked]=\"form.value === true\"\r\n  [disabled]=\"disabled\"\r\n  (change)=\"onRadioChange($event)\"\r\n  [value]=\"true\"\r\n>\r\n  True\r\n</mat-radio-button>\r\n<mat-radio-button\r\n  [checked]=\"form.value === false\"\r\n  [disabled]=\"disabled\"\r\n  (change)=\"onRadioChange($event)\"\r\n  [value]=\"false\"\r\n>\r\n  False\r\n</mat-radio-button>\r\n",
                providers: [
                    {
                        provide: MatFormFieldControl,
                        useExisting: FieldBooleanComponent
                    }
                ],
                styles: [":host{display:flex;flex-direction:column}mat-radio-button{margin-bottom:8px}"]
            }] }
];
/** @nocollapse */
FieldBooleanComponent.ctorParameters = () => [
    { type: FocusMonitor },
    { type: ElementRef },
    { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
];
FieldBooleanComponent.propDecorators = {
    form: [{ type: Input }],
    value: [{ type: Input }],
    required: [{ type: Input }],
    disabled: [{ type: Input }],
    placeholder: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
const ɵ0 = MAT_MOMENT_DATE_FORMATS;
class FieldDatetimeComponent {
    /**
     * \@internal Do not use!
     * @param {?} fm
     * @param {?} elRef
     * @param {?} ngControl
     */
    constructor(fm, elRef, ngControl) {
        this.fm = fm;
        this.elRef = elRef;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-datetime';
        /**
         * \@internal Do not use!
         */
        this.id = `keps-field-datetime-${FieldDatetimeComponent.nextId++}`;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new FormControl(null);
        /**
         * Form control for the date.
         * \@internal Do not use!
         */
        this.dateForm = new FormControl(null);
        /**
         * The current date for placeholder.
         * \@internal Do not use!
         */
        this.startDate = moment();
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        }));
    }
    /**
     * The value of the control.
     * @return {?}
     */
    get value() {
        return this.form.value;
    }
    /**
     * \@internal Do not use!
     * @param {?} d
     * @return {?}
     */
    set value(d) {
        this.form.setValue(d);
        if (d) {
            this.dateForm.setValue(moment(d));
        }
        else {
            this.dateForm.setValue(null);
        }
        this.stateChanges.next();
    }
    /**
     * Whether the control is required.
     * @return {?}
     */
    get required() {
        return this._required;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set required(value) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * Whether the control is disabled.
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * The placeholder for this control.
     * @return {?}
     */
    get placeholder() {
        return this._placeholder;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }
    /**
     * Whether the control is empty.
     * @return {?}
     */
    get empty() {
        return this.form.value == null;
    }
    /**
     * Whether the MatFormField label should try to float.
     * @return {?}
     */
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    setDescribedByIds(ids) {
        this.describedBy = ids.join(' ');
    }
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    onContainerClick(event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    }
    /**
     * Sets the forms value after selecting a date on datepicker.
     * \@internal Do not use!
     * @param {?} event Changes to the date picker.
     * @return {?}
     */
    onDateChange(event) {
        if (event.value) {
            this.value = event.value.valueOf();
        }
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    }
}
/**
 * \@internal Do not use!
 */
FieldDatetimeComponent.nextId = 0;
FieldDatetimeComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-field-datetime',
                template: "<div class=\"field-datetime\">\r\n  <input\r\n    matInput\r\n    [matDatepicker]=\"dp\"\r\n    [formControl]=\"dateForm\"\r\n    (dateInput)=\"onDateChange($event)\"\r\n    placeholder=\"Choose a date\"\r\n  >\r\n  <mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n  <mat-datepicker #dp [startAt]=\"startDate\"></mat-datepicker>\r\n</div>  \r\n",
                providers: [
                    {
                        provide: MatFormFieldControl,
                        useExisting: FieldDatetimeComponent
                    },
                    {
                        provide: DateAdapter,
                        useClass: MomentDateAdapter,
                        deps: [MAT_DATE_LOCALE]
                    },
                    {
                        provide: MAT_DATE_FORMATS,
                        useValue: ɵ0
                    }
                ],
                styles: [".field-datetime{width:100%;display:flex;height:13.5px}::ng-deep mat-datepicker-toggle>button{bottom:16px}"]
            }] }
];
/** @nocollapse */
FieldDatetimeComponent.ctorParameters = () => [
    { type: FocusMonitor },
    { type: ElementRef },
    { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
];
FieldDatetimeComponent.propDecorators = {
    form: [{ type: Input }],
    value: [{ type: Input }],
    required: [{ type: Input }],
    disabled: [{ type: Input }],
    placeholder: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A pipe that transforms an array to a nicely formatted list.
 */
class ArrayDisplayPipe {
    /**
     * @param {?} value
     * @param {?=} delimiter
     * @param {?=} displayExpression
     * @return {?}
     */
    transform(value, delimiter = ', ', displayExpression = '') {
        if (Array.isArray(value)) {
            // Assume that array contains object of the same type.
            if (_.isPlainObject(value[0])) {
                // If it's an array of objects, map the objects using display expression.
                /** @type {?} */
                const evalPipe = new EvalPipe();
                value = value.map((/**
                 * @param {?} obj
                 * @return {?}
                 */
                obj => evalPipe.transform(obj, displayExpression)));
            }
            return value.join(delimiter);
        }
        else {
            return value;
        }
    }
}
ArrayDisplayPipe.decorators = [
    { type: Pipe, args: [{
                name: 'arrayDisplay'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A pipe that transforms number to formatted number.
 */
class NumberFormatPipe {
    /**
     * @param {?} value
     * @param {?=} format
     * @return {?}
     */
    transform(value, format = '') {
        if (typeof value !== 'number' || !format) {
            return value;
        }
        else {
            // Get prefix.
            /** @type {?} */
            let prefix = '';
            while (!format.startsWith('0') && format.length > 1) {
                prefix += format[0];
                format = format.slice(1);
            }
            // Get number of 0s.
            /** @type {?} */
            const re = /[0]+/g;
            /** @type {?} */
            const zeroes = format.match(re);
            /** @type {?} */
            const decimalPipe = new DecimalPipe('en-US');
            /** @type {?} */
            let number = '';
            if (zeroes.length === 1) {
                number = decimalPipe.transform(value, `${0}.${zeroes[0].length}-${zeroes[0].length}`);
            }
            else if (zeroes.length === 2) {
                number = decimalPipe.transform(value, `${zeroes[0].length}.${zeroes[1].length}-${zeroes[1].length}`);
            }
            else if (zeroes.length === 3) {
                number = decimalPipe.transform(value, `${zeroes[1].length}.${zeroes[2].length}-${zeroes[2].length}`);
            }
            else {
                number = value.toString();
            }
            // Get positions after zeroes.
            /** @type {?} */
            const pos = [];
            /** @type {?} */
            let x;
            while ((x = re.exec(format)) !== null) {
                pos.push(re.lastIndex);
            }
            // Get symbols.
            /** @type {?} */
            const symbols = [];
            for (let i = 1; i <= pos.length; i++) {
                /** @type {?} */
                const symbol = format.substring(pos[i - 1], i === pos.length ? format.length : pos[i] - 1);
                symbols.push(symbol.replace(/[0]/g, ''));
            }
            /** @type {?} */
            let suffix = '';
            if (zeroes.length === 2) {
                number = number.replace(/\./g, symbols.shift());
                suffix = symbols.shift() || '';
            }
            else if (zeroes.length === 3) {
                number = number.replace(/\,/g, '$');
                number = number.replace(/\./g, '^');
                number = number.replace(/\$/g, symbols.shift());
                number = number.replace(/\^/g, symbols.shift());
                suffix = symbols.shift() || '';
            }
            return prefix + number + suffix;
        }
    }
}
NumberFormatPipe.decorators = [
    { type: Pipe, args: [{
                name: 'numberFormat'
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A directive to create custom column of a Keps Table.
 */
class TableColDirective {
    /**
     * \@internal Do not use!
     */
    constructor() { }
}
TableColDirective.decorators = [
    { type: Directive, args: [{
                selector: '[kepsTableCol]'
            },] }
];
/** @nocollapse */
TableColDirective.ctorParameters = () => [];
TableColDirective.propDecorators = {
    id: [{ type: Input, args: ['kepsTableCol',] }],
    label: [{ type: Input }],
    width: [{ type: Input }],
    align: [{ type: Input }],
    sortable: [{ type: Input }],
    sortAccessor: [{ type: Input }],
    sortStart: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TableComponent {
    /**
     * \@internal Do not use!
     * @param {?} models
     * @param {?} iterableDiffers
     */
    constructor(models, iterableDiffers) {
        this.models = models;
        this.iterableDiffers = iterableDiffers;
        /**
         * Data to be shown on the table.
         */
        this.data = [];
        /**
         * Columns to be displayed on the table.
         */
        this.displayedColumns = [];
        /**
         * Columns to be hidden on the table.
         */
        this.hiddenColumns = [];
        /**
         * Custom function to filter the data source
         * See example below:
         * <pre>
         *  customFilter(Data: T, Filter: string): boolean {
         *     return <true if Data matches filter>
         *  }
         * <pre>
         *
         * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
         */
        this.filterPredicate = null;
        /**
         * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
         * \@internal Do not use!
         */
        this.allColumns = [];
        /**
         * A record of custom column data and template keyed by the custom column ID.
         */
        this.customCols = {};
        /**
         * The table's default paginator page size.
         */
        this.defaultPageSize = 10;
        /**
         * Whether the table should display sort on headers or not.
         */
        this.showSort = false;
        /**
         * Whether the table should show the default search box or not.
         */
        this.showSearch = false;
        /**
         * The search query to be used to filter the data.
         */
        this.search = '';
        /**
         * An event emitter that emits the object of the row clicked by the user.
         */
        this.rowClick = new EventEmitter();
        /**
         * Whether the table has done initializing or not.
         * \@readonly
         */
        this.initialized = false;
        this.diff = this.iterableDiffers.find([]).create(null);
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        // Initialize the data source.
        this.dataSource = new MatTableDataSource(this.data);
        // Initialize the paginator.
        if (this.showPaginator) {
            this.dataSource.paginator = this.paginator;
            // Get the previous session's page size if any, or use the default page size.
            this.pageSize = Number(localStorage.getItem(this.savePageSize)) || this.defaultPageSize;
        }
        // Initialize the sort.
        if (this.showSort) {
            this.dataSource.sort = this.sort;
            // Modify the data source's `sortingDataAccessor` to use `EvalPipe` for custom columns.
            this.evalPipe = new EvalPipe();
            this.dataSource.sortingDataAccessor = (/**
             * @param {?} data
             * @param {?} sortHeaderId
             * @return {?}
             */
            (data, sortHeaderId) => {
                /** @type {?} */
                const customCol = this.customCols[sortHeaderId];
                if (customCol && customCol.data.sortAccessor) {
                    return this.evalPipe.transform(data, customCol.data.sortAccessor);
                }
                return data[sortHeaderId];
            });
        }
        this.setFilterPredicate(this.filterPredicate);
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngAfterContentInit() {
        // We are using `ngAfterContentInit` here because we can
        // only get the custom columns data after the component content
        // has been initialized.
        // Get schema from model name if supplied.
        if (this.model && this.models[this.model]) {
            this.schema = this.models[this.model].schema;
        }
        else if (this.model && !this.models[this.model]) {
            throw new Error(`Can't find a model named ${this.model}!`);
        }
        // Assert schema must exist.
        if (!this.schema) {
            throw new Error(`Must supply valid model name or schema to Keps table!`);
        }
        // Set `allColumns` to be every public field in the schema.
        this.allColumns = Object.keys(this.schema).filter((/**
         * @param {?} field
         * @return {?}
         */
        field => field[0] !== '_'));
        // Process custom columns.
        /** @type {?} */
        const colDatas = this.customColDatas.toArray();
        /** @type {?} */
        const colTemplates = this.customColTemplates.toArray();
        for (let i = 0; i < colDatas.length; i++) {
            /** @type {?} */
            const colId = colDatas[i].id;
            this.customCols[colId] = {
                data: colDatas[i],
                template: colTemplates[i]
            };
            this.allColumns.push(colId);
        }
        // If `displayedColumns` is not provided, use `allColumns` filtered by `hiddenColumns`.
        if (this.displayedColumns.length === 0) {
            this.displayedColumns = this.allColumns.filter((/**
             * @param {?} field
             * @return {?}
             */
            field => !this.hiddenColumns.includes(field)));
        }
        this.initialized = true;
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngDoCheck() {
        // This will refresh the table data source and rerender
        // the table if there is any change to the data.
        if (this.initialized) {
            /** @type {?} */
            const changes = this.diff.diff(this.data);
            if (changes) {
                this.dataSource.data = this.data;
                this.table.renderRows();
            }
        }
    }
    /**
     * \@internal Do not use!
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        // This check if the `search` property has changed and apply search if it does.
        if (this.initialized && changes.search) {
            this.applySearch(this.search);
        }
    }
    /**
     * Saves current page size to local storage if property `savePageSize` is supplied.
     * \@internal Do not use!
     * @param {?} event Event from paginator.
     * @return {?}
     */
    onPageChange(event) {
        if (this.savePageSize) {
            localStorage.setItem(this.savePageSize, event.pageSize.toString());
        }
    }
    /**
     * Applies a search query to filter the data.
     * @param {?} search Query for the search.
     * @return {?}
     */
    applySearch(search) {
        this.dataSource.filter = search.trim().toLowerCase();
    }
    /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    isType(field, type) {
        return isType(this.schema, field, type);
    }
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplay(field) {
        return getReferenceDisplay(this.models, this.schema, field);
    }
    /**
     * Returns the display expression of an array of references field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplayArray(field) {
        return getReferenceDisplayArray(this.models, this.schema, field);
    }
    /**
     * Set the filter predicate of data source.
     * this function accepts a function as a parameter.
     * See example below:
     * <pre>
     *  customFilter(Data: T, Filter: string): boolean {
     *     return <true if Data matches filter>
     *  }
     * <pre>
     * If no funtion is provided, this function will use the default
     * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
     * @param {?=} filterPredicateFunction custom filterPredicateFunction that follows the above format
     * @return {?}
     */
    setFilterPredicate(filterPredicateFunction = null) {
        if (filterPredicateFunction) {
            this.dataSource.filterPredicate = filterPredicateFunction;
        }
    }
}
TableComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-table',
                template: "<!-- Table's Search Box -->\r\n<mat-form-field\r\n  *ngIf=\"showSearch\"\r\n  floatLabel=\"never\"\r\n  class=\"keps-table-search\"\r\n>\r\n  <input\r\n    matInput\r\n    (keyup)=\"applySearch($event.target.value)\"\r\n    placeholder=\"Search\"\r\n  >\r\n</mat-form-field>\r\n\r\n<!-- Table -->\r\n<table\r\n  mat-table [dataSource]=\"dataSource\"\r\n  matSort [matSortDisabled]=\"!showSort\" [matSortActive]=\"activeSort\" matSortDirection=\"asc\"\r\n  class=\"keps-table\"\r\n>\r\n  <!-- Table Columns from Schema -->\r\n  <ng-container\r\n    *ngFor=\"let fieldName of schema | keys\"\r\n    [matColumnDef]=\"fieldName\"\r\n  >\r\n    <ng-container *ngIf=\"schema[fieldName]; let field\">\r\n      <th mat-header-cell *matHeaderCellDef mat-sort-header [ngClass]=\"'keps-table-col-' + fieldName\" >\r\n        {{ (field.label ? field.label : '') }}{{ (field.label ?  '' : fieldName) | splitCamel | titlecase }}\r\n      </th>\r\n      <td \r\n        mat-cell *matCellDef=\"let element\" \r\n        class=\"keps-table-cell\"\r\n        [ngClass]=\"'keps-table-col-' + fieldName\"\r\n      >\r\n        <ng-container *ngIf=\"element[fieldName]; let fieldData\">\r\n          <span *ngIf=\"isType(fieldName, 'reference')\">\r\n            {{ fieldData | eval:getReferenceDisplay(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'address')\">\r\n            {{ fieldData | address:4 }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'datetime')\">\r\n            {{ fieldData | moment:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-reference')\">\r\n            {{ fieldData | arrayDisplay:', ':getReferenceDisplayArray(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-string')\">\r\n            {{ fieldData | arrayDisplay:', ' }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'number')\">\r\n            {{ fieldData | numberFormat:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'enum')\">\r\n            {{ field.labels && field.labels[fieldData] ? field.labels[fieldData] : fieldData }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'table-normal')\">\r\n            {{ fieldData }}\r\n          </span>\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Table Columns from Custom Columns -->\r\n  <ng-container\r\n    *ngFor=\"let colName of customCols | keys\"\r\n    [matColumnDef]=\"colName\"\r\n  >\r\n    <ng-container *ngIf=\"customCols[colName].data; let colData\">\r\n      <th\r\n        mat-header-cell *matHeaderCellDef\r\n        [width]=\"colData.width\"\r\n        [align]=\"colData.align\"\r\n        mat-sort-header [disabled]=\"!colData.sortable\" [start]=\"colData.sortStart\"\r\n      >\r\n        {{ (colData.label || colName) | splitCamel | titlecase }}\r\n      </th>\r\n\r\n      <td\r\n        mat-cell *matCellDef=\"let element; let i = index;\"\r\n        [align]=\"colData.align\"\r\n        class=\"keps-table-column\"\r\n      >\r\n        <ng-container\r\n          *ngTemplateOutlet=\"customCols[colName].template; context: { $implicit: element, index: i, element: element }\"\r\n        >\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Rows -->\r\n  <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n  <tr\r\n    mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n    (click)=\"rowClick.emit(row)\"\r\n    [class.clickable-row]=\"rowClick.observers.length > 0\"\r\n  >\r\n  </tr>\r\n</table>\r\n\r\n<!-- Empty Table Row -->\r\n<mat-card *ngIf=\"data.length === 0 || dataSource.filteredData.length === 0\" class=\"empty-table\">\r\n  No Data.\r\n</mat-card>\r\n\r\n<!-- Paginator -->\r\n<mat-paginator\r\n  [class.hide-paginator]=\"!showPaginator\"\r\n  [pageSizeOptions]=\"[10, 25, 50, 100]\"\r\n  [pageSize]=\"pageSize\"\r\n  (page)=\"onPageChange($event)\"\r\n  showFirstLastButtons\r\n  class=\"keps-paginator\"\r\n>\r\n</mat-paginator>\r\n",
                styles: [":host{display:block;position:relative}.keps-table{width:100%}.empty-table,.keps-paginator,.keps-table{background:0 0!important}.keps-table-cell{white-space:pre-line}.keps-table-cell *{display:block;margin:16px 0}.empty-table{border-radius:0;text-align:center;box-shadow:none!important}.hide-paginator{display:none!important}.keps-table-search{width:95%;margin:0 2.5%}.clickable-row:hover{background:rgba(0,0,0,.05);cursor:pointer}"]
            }] }
];
/** @nocollapse */
TableComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] },
    { type: IterableDiffers }
];
TableComponent.propDecorators = {
    model: [{ type: Input }],
    schema: [{ type: Input }],
    table: [{ type: ViewChild, args: [MatTable, { static: true },] }],
    data: [{ type: Input }],
    displayedColumns: [{ type: Input }],
    hiddenColumns: [{ type: Input }],
    filterPredicate: [{ type: Input }],
    customColDatas: [{ type: ContentChildren, args: [TableColDirective,] }],
    customColTemplates: [{ type: ContentChildren, args: [TableColDirective, { read: TemplateRef },] }],
    paginator: [{ type: ViewChild, args: [MatPaginator, { static: true },] }],
    showPaginator: [{ type: Input }],
    savePageSize: [{ type: Input }],
    defaultPageSize: [{ type: Input }],
    sort: [{ type: ViewChild, args: [MatSort, { static: true },] }],
    showSort: [{ type: Input }],
    activeSort: [{ type: Input }],
    showSearch: [{ type: Input }],
    search: [{ type: Input }],
    rowClick: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DynamicTableComponent {
    /**
     * \@internal Do not use!
     * @param {?} models
     * @param {?} dataService
     */
    constructor(models, dataService) {
        this.models = models;
        this.dataService = dataService;
        /**
         * Data to be shown on the table from the GraphQL query.
         * \@readonly
         */
        this.data = [];
        /**
         * Columns to be displayed on the table.
         */
        this.displayedColumns = [];
        /**
         * Columns to be hidden on the table.
         */
        this.hiddenColumns = [];
        /**
         * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
         * \@internal Do not use!
         */
        this.allColumns = [];
        /**
         * A record of custom column data and template keyed by the custom column ID.
         */
        this.customCols = {};
        /**
         * The table's default paginator page size.
         */
        this.defaultPageSize = 10;
        /**
         * Length of the data to be shown on the paginator.
         * \@readonly
         */
        this.dataLength = Infinity;
        /**
         * Whether the data length has been known (from last query) or not.
         */
        this.isDataLengthSet = false;
        /**
         * Whether the table should show the default search box or not.
         */
        this.showSearch = false;
        /**
         * A subject that emits every time the search string changes.
         */
        this.searchChange = new Subject();
        /**
         * Delay for the search function to be called in milliseconds.
         */
        this.searchDelay = 1000;
        /**
         * A subject that emits every time the filter function changes.
         */
        this.filterFunctionChange = new Subject();
        /**
         * An event emitter that emits the object of the row clicked by the user.
         */
        this.rowClick = new EventEmitter();
        /**
         * Whether the table is currently loading or not.
         * \@readonly
         */
        this.tableLoading = true;
        /**
         * Whether the table has done initializing or not.
         * \@readonly
         */
        this.initialized = false;
    }
    /**
     * String to be searched based on `searchFields`.
     * @return {?}
     */
    get search() {
        return this._search;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set search(value) {
        this._search = value;
        this.searchChange.next(value);
    }
    /**
     * Function to filter the result after query.
     * @return {?}
     */
    get filterFunction() {
        return this._filterFunction;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set filterFunction(value) {
        this._filterFunction = value;
        this.filterFunctionChange.next();
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        // Get the previous session's page size if any, or use the default page size.
        this.pageSize = Number(localStorage.getItem(this.savePageSize)) || this.defaultPageSize;
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngAfterViewInit() {
        // Get data as soon as view initialized.
        this.getData();
        // Setup delayed search.
        /** @type {?} */
        const delayedSearch = this.searchChange
            .pipe(debounceTime(this.searchDelay), distinctUntilChanged());
        // If any of these observables emit, then we reset the paginator.
        delayedSearch.subscribe((/**
         * @return {?}
         */
        () => {
            this.paginator.pageIndex = 0;
            this.dataLength = Infinity;
        }));
        this.filterFunctionChange.subscribe((/**
         * @return {?}
         */
        () => {
            this.paginator.pageIndex = 0;
            this.dataLength = Infinity;
        }));
        this.sort.sortChange.subscribe((/**
         * @return {?}
         */
        () => this.paginator.pageIndex = 0));
        // Save page size if paginator changes.
        if (this.savePageSize) {
            this.paginator.page.subscribe((/**
             * @return {?}
             */
            () => {
                localStorage.setItem(this.savePageSize, this.paginator.pageSize.toString());
            }));
        }
        // If any of these observables emit, then we refresh the data.
        merge(this.sort.sortChange, this.paginator.page, delayedSearch, this.filterFunctionChange)
            .subscribe((/**
         * @return {?}
         */
        () => this.getData()));
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngAfterContentInit() {
        // We are using `ngAfterContentInit` here because we can
        // only get the custom columns data after the component content
        // has been initialized.
        // Get schema from model name if supplied.
        if (this.model && this.models[this.model]) {
            this.schema = this.models[this.model].schema;
        }
        else if (this.model && !this.models[this.model]) {
            throw new Error(`Can't find a model named ${this.model}!`);
        }
        // Assert schema must exist.
        if (!this.schema) {
            throw new Error(`Must supply valid model name to Keps dynamic table!`);
        }
        // Set `allColumns` to be every public field in the schema.
        this.allColumns = Object.keys(this.schema).filter((/**
         * @param {?} field
         * @return {?}
         */
        field => field[0] !== '_'));
        // Process custom columns.
        /** @type {?} */
        const colDatas = this.customColDatas.toArray();
        /** @type {?} */
        const colTemplates = this.customColTemplates.toArray();
        for (let i = 0; i < colDatas.length; i++) {
            /** @type {?} */
            const colId = colDatas[i].id;
            this.customCols[colId] = {
                data: colDatas[i],
                template: colTemplates[i]
            };
            this.allColumns.push(colId);
        }
        // If `displayedColumns` is not provided, use `allColumns` filtered by `hiddenColumns`.
        if (this.displayedColumns.length === 0) {
            this.displayedColumns = this.allColumns.filter((/**
             * @param {?} field
             * @return {?}
             */
            field => !this.hiddenColumns.includes(field)));
        }
        this.initialized = true;
    }
    /**
     * Combines properties as GraphQL query and gets data from data service.
     * @return {?}
     */
    getData() {
        return __awaiter(this, void 0, void 0, /** @this {!DynamicTableComponent} */ function* () {
            try {
                this.tableLoading = true;
                /** @type {?} */
                const filters = {};
                // Setup page query.
                const { pageIndex, pageSize } = this.paginator;
                filters.offset = pageIndex * pageSize;
                // Setup sort query.
                const { active, direction } = this.sort;
                if (active) {
                    filters.sort = (direction === 'desc' ? '-' : '+') + active;
                }
                // Setup search query.
                if (this.searchFields && this.search) {
                    /** @type {?} */
                    let mongoFilter = ` { '$or': [`;
                    for (let field of this.searchFields) {
                        mongoFilter += `{'${field}': {
            '$regex': '${this.search.replace('(', '%28').replace(')', '%29')}',
            '$options': 'i' }},`;
                    }
                    mongoFilter = _.trimEnd(mongoFilter, ',');
                    mongoFilter += ']}';
                    filters.mongo = mongoFilter;
                }
                // Set limit if there is not filter function.
                if (!this.filterFunction) {
                    filters.limit = pageSize;
                }
                // Get data from data service.
                /** @type {?} */
                const result = yield this.dataService.graphql(`
        ${this.model}s(
          ${this.convertFilter(Object.assign(filters, this.filterQuery))}
        ){${this.query}}
      `);
                /** @type {?} */
                const data = result.data[this.model + 's'];
                // If filter function exists, filter with that function.
                if (this.filterFunction) {
                    this.data = _.filter(data, this.filterFunction);
                }
                else {
                    // If this is not the first page and data is empty,
                    // then we go back to the last page.
                    if (pageIndex > 1 && data.length === 0) {
                        // Calculate data length.
                        this.dataLength = (pageIndex - 1) * pageSize + this.data.length;
                        this.paginator.pageIndex = pageIndex - 1;
                    }
                    else {
                        // If data length is smaller than page size,
                        // we assume we're on last page and recalculate data length.
                        if (data.length < pageSize) {
                            this.dataLength = pageIndex * pageSize + data.length;
                        }
                        // Set data as is.
                        this.data = data;
                    }
                }
                this.tableLoading = false;
            }
            catch (err) {
                console.error(err);
            }
        });
    }
    /**
     * Converts a filter object to Mongo filter string.
     * @private
     * @param {?} filters Pair of keys & values to be converted to Mongo filter.
     * @return {?}
     */
    convertFilter(filters) {
        /** @type {?} */
        let str = '';
        for (const filterName in filters) {
            if (filterName === 'limit' || filterName === 'offset') {
                str += `${filterName}:${filters[filterName]},`;
            }
            else {
                str += `${filterName}:"${filters[filterName]}",`;
            }
        }
        return str;
    }
    /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    isType(field, type) {
        return isType(this.schema, field, type);
    }
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplay(field) {
        return getReferenceDisplay(this.models, this.schema, field);
    }
    /**
     * Returns the display expression of an array of references field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplayArray(field) {
        return getReferenceDisplayArray(this.models, this.schema, field);
    }
}
DynamicTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-dynamic-table',
                template: "<div *ngIf=\"tableLoading\" class=\"table-loader\">\r\n  <mat-spinner></mat-spinner>\r\n</div>\r\n\r\n<!-- Table's Search Box -->\r\n<mat-form-field\r\n  *ngIf=\"showSearch\"\r\n  floatLabel=\"never\"\r\n  class=\"keps-table-search\"\r\n>\r\n  <input\r\n    matInput\r\n    (keyup)=\"search = $event.target.value\"\r\n    placeholder=\"Search\"\r\n  >\r\n</mat-form-field>\r\n\r\n<!-- Table -->\r\n<table\r\n  mat-table [dataSource]=\"data\"\r\n  matSort [matSortActive]=\"activeSort\" matSortDirection=\"asc\"\r\n  class=\"keps-table\"\r\n>\r\n  <!-- Table Columns from Schema -->\r\n  <ng-container\r\n    *ngFor=\"let fieldName of schema | keys\"\r\n    [matColumnDef]=\"fieldName\"\r\n  >\r\n    <ng-container *ngIf=\"schema[fieldName]; let field\">\r\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n        {{ (field.label ? field.label : fieldName) | splitCamel | titlecase }}\r\n      </th>\r\n      <td \r\n        mat-cell *matCellDef=\"let element\" \r\n        class=\"keps-table-cell\"\r\n      >\r\n        <ng-container *ngIf=\"element[fieldName]; let fieldData\">\r\n          <span *ngIf=\"isType(fieldName, 'reference')\">\r\n            {{ fieldData | eval:getReferenceDisplay(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'address')\">\r\n            {{ fieldData | address:4 }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'datetime')\">\r\n            {{ fieldData | moment:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-reference')\">\r\n            {{ fieldData | arrayDisplay:', ':getReferenceDisplayArray(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-string')\">\r\n            {{ fieldData | arrayDisplay:', ' }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'number')\">\r\n            {{ fieldData | numberFormat:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'enum')\">\r\n            {{ field.labels && field.labels[fieldData] ? field.labels[fieldData] : fieldData }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'table-normal')\">\r\n            {{ fieldData }}\r\n          </span>\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Table Columns from Custom Columns -->\r\n  <ng-container\r\n    *ngFor=\"let colName of customCols | keys\"\r\n    [matColumnDef]=\"colName\"\r\n  >\r\n    <ng-container *ngIf=\"customCols[colName].data; let colData\">\r\n      <th\r\n        mat-header-cell *matHeaderCellDef\r\n        [width]=\"colData.width\"\r\n        [align]=\"colData.align\"\r\n        mat-sort-header [disabled]=\"!colData.sortable\" [start]=\"colData.sortStart\"\r\n      >\r\n        {{ (colData.label || colName) | splitCamel | titlecase }}\r\n      </th>\r\n\r\n      <td\r\n        mat-cell *matCellDef=\"let element; let i = index;\"\r\n        [align]=\"colData.align\"\r\n        class=\"keps-table-column\"\r\n      >\r\n        <ng-container\r\n          *ngTemplateOutlet=\"customCols[colName].template; context: { $implicit: element, index: i, element: element }\"\r\n        >\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Rows -->\r\n  <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n  <tr\r\n    mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n    (click)=\"rowClick.emit(row)\"\r\n    [class.clickable-row]=\"rowClick.observers.length > 0\"\r\n  >\r\n  </tr>\r\n</table>\r\n\r\n<!-- Empty Table Row -->\r\n<mat-card *ngIf=\"data.length === 0\" class=\"empty-table\">\r\n  No Data.\r\n</mat-card>\r\n\r\n<!-- Paginator -->\r\n<mat-paginator\r\n  [class.hide-paginator]=\"filterFunction\"\r\n  [pageSizeOptions]=\"[10, 25, 50, 100]\"\r\n  [pageSize]=\"pageSize\"\r\n  [length]=\"dataLength\"\r\n>\r\n</mat-paginator>\r\n",
                styles: [":host{display:block;position:relative}.keps-table{width:100%}.empty-table,.keps-paginator,.keps-table{background:0 0!important}.keps-table-cell{white-space:pre-line}.keps-table-cell *{display:block;margin:16px 0}.empty-table{border-radius:0;text-align:center;box-shadow:none!important}.hide-paginator{display:none!important}.keps-table-search{width:95%;margin:0 2.5%}.clickable-row:hover{background:rgba(0,0,0,.05);cursor:pointer}.table-loader{background:rgba(0,0,0,.125);z-index:999;width:100%;height:100%;position:absolute;display:flex;justify-content:center;align-items:center}"]
            }] }
];
/** @nocollapse */
DynamicTableComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] },
    { type: DataService }
];
DynamicTableComponent.propDecorators = {
    model: [{ type: Input }],
    query: [{ type: Input }],
    filterQuery: [{ type: Input }],
    displayedColumns: [{ type: Input }],
    hiddenColumns: [{ type: Input }],
    customColDatas: [{ type: ContentChildren, args: [TableColDirective,] }],
    customColTemplates: [{ type: ContentChildren, args: [TableColDirective, { read: TemplateRef },] }],
    paginator: [{ type: ViewChild, args: [MatPaginator, { static: true },] }],
    savePageSize: [{ type: Input }],
    defaultPageSize: [{ type: Input }],
    sort: [{ type: ViewChild, args: [MatSort, { static: true },] }],
    activeSort: [{ type: Input }],
    showSearch: [{ type: Input }],
    searchFields: [{ type: Input }],
    search: [{ type: Input, args: ['search',] }],
    searchDelay: [{ type: Input }],
    filterFunction: [{ type: Input, args: ['filterFunction',] }],
    rowClick: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Ng7Keps main module.
 */
class Ng7KepsModule {
    /**
     * Initializes Ng7Keps with the project models.
     * @param {?} models Generated models of the project.
     * @return {?}
     */
    static forRoot(models) {
        return {
            ngModule: Ng7KepsModule,
            providers: [
                AuthService,
                AuthGuard,
                AuthInterceptor,
                DataService,
                ErrorService,
                PopupService,
                {
                    provide: 'MODELS',
                    useValue: models
                }
            ]
        };
    }
}
Ng7KepsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    AddressPipe,
                    ArrayDisplayPipe,
                    EvalPipe,
                    KeysPipe,
                    MomentPipe,
                    NumberFormatPipe,
                    SplitCamelPipe,
                    FileSelectComponent,
                    DialogComponent,
                    FormComponent,
                    FieldAddressComponent,
                    FieldArrayComponent,
                    FieldBooleanComponent,
                    FieldDatetimeComponent,
                    TableComponent,
                    TableColDirective,
                    DynamicTableComponent
                ],
                imports: [
                    BrowserModule,
                    BrowserAnimationsModule,
                    HttpClientModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule
                ],
                exports: [
                    AddressPipe,
                    ArrayDisplayPipe,
                    EvalPipe,
                    KeysPipe,
                    MomentPipe,
                    NumberFormatPipe,
                    SplitCamelPipe,
                    FileSelectComponent,
                    FormComponent,
                    FieldAddressComponent,
                    FieldArrayComponent,
                    FieldBooleanComponent,
                    FieldDatetimeComponent,
                    TableComponent,
                    DynamicTableComponent,
                    TableColDirective
                ],
                entryComponents: [
                    DialogComponent
                ]
            },] }
];

export { AddressPipe, ArrayDisplayPipe, AuthGuard, AuthInterceptor, AuthService, DataService, DialogComponent, DynamicTableComponent, ErrorService, EvalPipe, FieldAddressComponent, FieldArrayComponent, FieldBooleanComponent, FieldDatetimeComponent, FileSelectComponent, FormComponent, KepsForm, KepsFormArray, KeysPipe, MomentPipe, Ng7KepsModule, NumberFormatPipe, PopupService, SplitCamelPipe, TableColDirective, TableComponent, DialogComponent as ɵa, FormComponent as ɵc, FieldAddressComponent as ɵe, FieldArrayComponent as ɵf, FieldBooleanComponent as ɵg, FieldDatetimeComponent as ɵh, TableComponent as ɵi, TableColDirective as ɵj, DynamicTableComponent as ɵk, MaterialModule as ɵl, AuthService as ɵm, AuthGuard as ɵn, AuthInterceptor as ɵo };
//# sourceMappingURL=ng7keps.js.map
