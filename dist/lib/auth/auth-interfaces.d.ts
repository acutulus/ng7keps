/**
 * Represents data to be send to the local login provider.
 */
export interface AuthLoginData {
    username: string;
    password: string;
    /**
     * How long in miliseconds should the user token be active.
     */
    expires?: number;
}
/**
 * Represents data to be send to the local signup provider.
 */
export interface AuthSignupData {
    username?: string;
    password?: string;
    email?: string;
    firstName?: string;
    lastName?: string;
    pending?: boolean;
    /**
     * If true, the user will not be automatically logged in after signing up.
     */
    noLogin?: boolean;
}
/**
 * Represents options for the Auth Guard.
 */
export interface AuthGuardOptions {
    /**
     * Allow only user with this role to the route.
     */
    role?: string;
    /**
     * If true, the auth service will logs out the user trying to access the route.
     */
    logout?: boolean;
    /**
     * Url to take user to if they sign out; If not supplied, default url is 'window.location.origin'
     */
    logoutUrl?: string;
}
