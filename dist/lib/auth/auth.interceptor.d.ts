import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
/**
 * Intercepts outgoing requests and adds token if needed.
 * Intercepts incoming responses and sets/removes user if needed.
 */
export declare class AuthInterceptor implements HttpInterceptor {
    private auth;
    /**
     * @internal Do not use!
     */
    constructor(auth: AuthService);
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
