import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
/**
 * A route guard that rejects user from accessing the route if they're not logged in.
 *
 * Can also allows only specific user role from accessing the route based on the route
 * data's `guardOpt` field. See {@link AuthGuardOptions}.
 */
export declare class AuthGuard implements CanActivate {
    private router;
    private auth;
    /**
     * @internal Do not use!
     */
    constructor(router: Router, auth: AuthService);
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>;
}
