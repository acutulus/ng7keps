import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { KepsUser } from '../interface';
import { AuthLoginData, AuthSignupData } from './auth-interfaces';
/**
 * A service that handles authentication with a Keps server.
 */
export declare class AuthService {
    private http;
    private user;
    private user$;
    private loggedIn$;
    /**
     * @internal Do not use!
     */
    constructor(http: HttpClient);
    /**
     * Starts the auth service.
     * Should be called only when the app is bootstrapping.
     */
    start(): Promise<void>;
    /**
     * Saves/removes user and emits the user and logged in status to observables.
     * @internal Do not use!
     * @param user A user or null
     */
    setUser(user?: KepsUser): void;
    /**
     * Returns the current user.
     */
    getUser(): KepsUser;
    /**
     * Returns an observable that will emit the current user.
     */
    getUser$(): Observable<KepsUser>;
    /**
     * Returns token from the user.
     */
    getToken(): string | null;
    /**
     * Returns an observable that emits logged in status.
     */
    isLoggedIn$(): Observable<boolean>;
    /**
     * Logs in using local provider, sets current user, and returns the user object.
     * @param provider Local provider.
     * @param data An object with username and password fields.
     */
    loginWithProvider(provider: 'local', data: AuthLoginData): Promise<KepsUser>;
    /**
     * Logs in using the passed in provider name.
     * @param provider Provider to use to login.
     */
    loginWithProvider(provider: string): Promise<void>;
    /**
     * Signs up using local provider, sets current user, and returns the user object.
     * @param provider Local provider.
     * @param data An object with use data.
     * @param customRoute Custom route to use instead of the default 'users/signup'.
     */
    signupWithProvider(provider: 'local', data: AuthSignupData, customRoute?: string): Promise<KepsUser>;
    /**
     * Signs up user using the passed in provider name.
     * @param provider Provider to use to sign up.
     */
    signupWithProvider(provider: string): Promise<void>;
    /**
     * Switch current user.
     */
    switchUser(newUser: KepsUser): void;
    /**
     * Logs out the current user.
     */
    logout(): void;
}
