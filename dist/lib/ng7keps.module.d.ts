import { ModuleWithProviders } from '@angular/core';
import { KepsModels } from './interface';
/**
 * Ng7Keps main module.
 */
export declare class Ng7KepsModule {
    /**
     * Initializes Ng7Keps with the project models.
     * @param models Generated models of the project.
     */
    static forRoot(models: KepsModels): ModuleWithProviders;
}
