import { MatDialogRef } from '@angular/material';
import { DialogData, DialogButton } from './dialog-interfaces';
/**
 * A component that wraps around an Angular Material dialog.
 *
 * Use the {@link PopupService} to create this dialog.
 */
export declare class DialogComponent {
    dialogRef: MatDialogRef<DialogComponent>;
    data: DialogData;
    /**
     * @internal Do not use!
     */
    buttons: DialogButton[];
    /**
     * @internal Do not use!
     */
    showClose: boolean;
    /**
     * @internal Do not use!
     */
    constructor(dialogRef: MatDialogRef<DialogComponent>, data: DialogData);
}
