import { TemplateRef } from '@angular/core';
/**
 * Represents a custom column data & template.
 * @internal Do not use!
 */
export interface CustomColumn {
    data: TableColDirective;
    template: TemplateRef<any>;
}
/**
 * A directive to create custom column of a Keps Table.
 */
export declare class TableColDirective {
    /**
     * The ID to be used to display/hide the column.
     */
    id: string;
    /**
     * Label to be shown in the column header.
     *
     * If not specified, the table will use the ID as the label.
     */
    label: string;
    /**
     * Width of the column in pixels (e.g. "50") or percent (e.g. "50%").
     */
    width: number | string;
    /**
     * Text alignment of the column cells (including the header).
     */
    align: string;
    /**
     * Whether the column is sortable or not.
     */
    sortable: boolean;
    /**
     * Field of the table element to be used for sorting.
     */
    sortAccessor: string;
    /**
     * The default sort direction of the column.
     */
    sortStart: 'asc' | 'desc';
    /**
     * @internal Do not use!
     */
    constructor();
}
