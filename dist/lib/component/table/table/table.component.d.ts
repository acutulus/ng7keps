import { OnInit, AfterContentInit, DoCheck, OnChanges, SimpleChanges, EventEmitter, IterableDiffers } from '@angular/core';
import { MatTableDataSource, PageEvent } from '@angular/material';
import { CustomColumn } from '../table-col.directive';
import { KepsSchema, KepsModels, KepsObject } from '../../../interface';
export declare class TableComponent implements OnInit, AfterContentInit, DoCheck, OnChanges {
    private models;
    private iterableDiffers;
    /**
     * Name of the model for the table.
     */
    model: string;
    /**
     * Schema of the model for the table.
     *
     * If the model name is supplied,
     * then the table will get the schema from that model.
     */
    schema: KepsSchema;
    /**
     * Reference to the MatTable.
     */
    private table;
    /**
     * Data to be shown on the table.
     */
    data: KepsObject[];
    /**
     * Wrapper for the data using MatTableDataSource.
     *
     * Do not change this. Use the `data` property instead to change the data.
     *
     * @internal Do not use!
     */
    dataSource: MatTableDataSource<KepsObject>;
    /**
     * Columns to be displayed on the table.
     */
    displayedColumns: string[];
    /**
     * Columns to be hidden on the table.
     */
    hiddenColumns: string[];
    /**
     * Custom function to filter the data source
     * See example below:
     * <pre>
     *  customFilter(Data: T, Filter: string): boolean {
     *     return <true if Data matches filter>
     *  }
     * <pre>
     *
     * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
     */
    filterPredicate: Function;
    /**
     * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
     * @internal Do not use!
     */
    allColumns: string[];
    /**
     * Queries all `TableColDirective` data inside the component.
     */
    private customColDatas;
    /**
     * Queries all `TableColDirective` templates inside the component.
     */
    private customColTemplates;
    /**
     * A record of custom column data and template keyed by the custom column ID.
     */
    customCols: Record<string, CustomColumn>;
    /**
     * Reference to the MatPaginator.
     */
    private paginator;
    /**
     * Whether the table should display the paginator or not.
     */
    showPaginator: false;
    /**
     * A key used when saving page size number to the local storage.
     */
    savePageSize: string;
    /**
     * The table's default paginator page size.
     */
    defaultPageSize: number;
    /**
     * The current page size on paginator.
     */
    pageSize: number;
    /**
     * Reference to the MatSort.
     */
    private sort;
    /**
     * Whether the table should display sort on headers or not.
     */
    showSort: boolean;
    /**
     * The ID of the column that should be sorted.
     */
    activeSort: string;
    /**
     * Whether the table should show the default search box or not.
     */
    showSearch: boolean;
    /**
     * The search query to be used to filter the data.
     */
    search: string;
    /**
     * An event emitter that emits the object of the row clicked by the user.
     */
    rowClick: EventEmitter<KepsObject>;
    /**
     * An iterable differ instance to track changes to the data.
     */
    private diff;
    /**
     * Instance of the `EvalPipe` to be used for custom columns sorting.
     */
    private evalPipe;
    /**
     * Whether the table has done initializing or not.
     * @readonly
     */
    initialized: boolean;
    /**
     * @internal Do not use!
     */
    constructor(models: KepsModels, iterableDiffers: IterableDiffers);
    /**
     * @internal Do not use!
     */
    ngOnInit(): void;
    /**
     * @internal Do not use!
     */
    ngAfterContentInit(): void;
    /**
     * @internal Do not use!
     */
    ngDoCheck(): void;
    /**
     * @internal Do not use!
     */
    ngOnChanges(changes: SimpleChanges): void;
    /**
     * Saves current page size to local storage if property `savePageSize` is supplied.
     * @internal Do not use!
     * @param event Event from paginator.
     */
    onPageChange(event: PageEvent): void;
    /**
     * Applies a search query to filter the data.
     * @param search Query for the search.
     */
    applySearch(search: string): void;
    /**
     * Checks the type of a field.
     * @internal Do not use!
     * @param field Field to be checked.
     * @param type Type to be checked.
     */
    isType(field: string, type: string): any;
    /**
     * Returns the display expression of a reference field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    getReferenceDisplay(field: string): string;
    /**
     * Returns the display expression of an array of references field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    getReferenceDisplayArray(field: string): string;
    /**
     * Set the filter predicate of data source.
     * this function accepts a function as a parameter.
     * See example below:
     * <pre>
     *  customFilter(Data: T, Filter: string): boolean {
     *     return <true if Data matches filter>
     *  }
     * <pre>
     * If no funtion is provided, this function will use the default
     * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
     * @param filterPredicateFunction custom filterPredicateFunction that follows the above format
     */
    setFilterPredicate(filterPredicateFunction?: any): void;
}
