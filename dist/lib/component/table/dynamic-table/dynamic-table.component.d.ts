import { OnInit, AfterViewInit, AfterContentInit, EventEmitter } from '@angular/core';
import { DataService } from '../../../service/data.service';
import { KepsModels, KepsSchema, KepsObject } from '../../../interface';
import { Subject } from 'rxjs';
import { CustomColumn } from '../table-col.directive';
/**
 * A function used by the dynamic table to filter result.
 */
declare type FilterFunction = (data: KepsObject) => boolean;
export declare class DynamicTableComponent implements OnInit, AfterViewInit, AfterContentInit {
    models: KepsModels;
    private dataService;
    /**
     * Name of the model for the table.
     */
    model: string;
    /**
     * Schema of the model.
     * @internal Do not use!
     */
    schema: KepsSchema;
    /**
     * Data to be shown on the table from the GraphQL query.
     * @readonly
     */
    data: KepsObject[];
    /**
     * Comma-separated list of fields for the GraphQL query.
     */
    query: string;
    /**
     * Additional filter to be added to the GraphQL query.
     */
    filterQuery: Record<string, string>;
    /**
     * Columns to be displayed on the table.
     */
    displayedColumns: string[];
    /**
     * Columns to be hidden on the table.
     */
    hiddenColumns: string[];
    /**
     * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
     * @internal Do not use!
     */
    allColumns: string[];
    /**
     * Queries all `TableColDirective` data inside the component.
     */
    private customColDatas;
    /**
     * Queries all `TableColDirective` templates inside the component.
     */
    private customColTemplates;
    /**
     * A record of custom column data and template keyed by the custom column ID.
     */
    customCols: Record<string, CustomColumn>;
    /**
     * Reference to the MatPaginator.
     */
    private paginator;
    /**
     * A key used when saving page size number to the local storage.
     */
    savePageSize: string;
    /**
     * The table's default paginator page size.
     */
    defaultPageSize: number;
    /**
     * The current page size on paginator.
     */
    pageSize: number;
    /**
     * Length of the data to be shown on the paginator.
     * @readonly
     */
    dataLength: number;
    /**
     * Whether the data length has been known (from last query) or not.
     */
    private isDataLengthSet;
    /**
     * Reference to the MatSort.
     */
    private sort;
    /**
     * The ID of the column that should be sorted.
     */
    activeSort: string;
    /**
     * Whether the table should show the default search box or not.
     */
    showSearch: boolean;
    /**
     * Field used for the search.
     */
    searchFields: string[];
    /**
     * String to be searched based on `searchFields`.
     */
    private _search;
    /**
     * A subject that emits every time the search string changes.
     */
    searchChange: Subject<string>;
    /**
     * String to be searched based on `searchFields`.
     */
    search: string;
    /**
     * Delay for the search function to be called in milliseconds.
     */
    searchDelay: number;
    /**
     * Function to filter the result after query.
     */
    private _filterFunction;
    /**
     * A subject that emits every time the filter function changes.
     */
    filterFunctionChange: Subject<void>;
    /**
     * Function to filter the result after query.
     */
    filterFunction: FilterFunction;
    /**
     * An event emitter that emits the object of the row clicked by the user.
     */
    rowClick: EventEmitter<KepsObject>;
    /**
     * Whether the table is currently loading or not.
     * @readonly
     */
    tableLoading: boolean;
    /**
     * Whether the table has done initializing or not.
     * @readonly
     */
    initialized: boolean;
    /**
     * @internal Do not use!
     */
    constructor(models: KepsModels, dataService: DataService);
    /**
     * @internal Do not use!
     */
    ngOnInit(): void;
    /**
     * @internal Do not use!
     */
    ngAfterViewInit(): void;
    /**
     * @internal Do not use!
     */
    ngAfterContentInit(): void;
    /**
     * Combines properties as GraphQL query and gets data from data service.
     */
    getData(): Promise<void>;
    /**
     * Converts a filter object to Mongo filter string.
     * @param filters Pair of keys & values to be converted to Mongo filter.
     */
    private convertFilter;
    /**
     * Checks the type of a field.
     * @internal Do not use!
     * @param field Field to be checked.
     * @param type Type to be checked.
     */
    isType(field: string, type: string): any;
    /**
     * Returns the display expression of a reference field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    getReferenceDisplay(field: string): string;
    /**
     * Returns the display expression of an array of references field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    getReferenceDisplayArray(field: string): string;
}
export {};
