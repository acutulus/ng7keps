import { FormArray } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
/**
 * A Keps form for array field.
 *
 * It extends Angular's `FormArray` class
 * and changes its `patchValue` and `reset` methods to
 * deal with arrays appropriately.
 */
export declare class KepsFormArray extends FormArray {
    /**
     * Let the parent knows if there is update to the controls array length.
     */
    private counter$;
    /**
     * Set an optional BehaviorSubject counter.
     * @param counter$ A BehaviorSubject from parent component.
     */
    setCounter$(counter$: BehaviorSubject<number>): void;
    /**
     * Patches the value of the `KepsFormArray`.
     * It accepts an array that matches the structure of the control,
     * and does its best to match the values to the correct controls in the group.
     *
     * It accepts both super-sets and sub-sets of the array without throwing an error.
     * @param values Array of latest values for the controls
     * @param options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     */
    patchValue(values: any[], options?: {
        onlySelf?: boolean;
        emitEvent?: boolean;
    }): void;
    /**
     * Resets the FormArray and all descendants are marked `pristine` and `untouched`,
     * and the value of all descendants to null or null maps.
     *
     * You reset to a specific form state by passing in an array of states that matches the structure of the control.
     * The state is a standalone value or a form state object with both a value and a disabled status.
     * @param values Array of latest values for the controls
     * @param options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     */
    reset(values?: any[], options?: {
        onlySelf?: boolean;
        emitEvent?: boolean;
    }): void;
}
