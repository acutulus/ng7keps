import { OnInit, OnDestroy, ElementRef } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { FormGroup, NgControl } from '@angular/forms';
import { MatFormFieldControl, MatFormFieldAppearance, ThemePalette } from '@angular/material';
import { KepsAddress } from '../../../interface';
import { Subject } from 'rxjs';
export declare class FieldAddressComponent implements MatFormFieldControl<KepsAddress>, OnInit, OnDestroy {
    private fm;
    private elRef;
    ngControl: NgControl;
    /**
     * @internal Do not use!
     */
    static nextId: number;
    /**
     * @internal Do not use!
     */
    private _required;
    /**
     * @internal Do not use!
     */
    private _disabled;
    /**
     * @internal Do not use!
     */
    private _placeholder;
    /**
     * @internal Do not use!
     */
    stateChanges: Subject<void>;
    /**
     * @internal Do not use!
     */
    focused: boolean;
    /**
     * @internal Do not use!
     */
    errorState: boolean;
    /**
     * @internal Do not use!
     */
    controlType: string;
    /**
     * @internal Do not use!
     */
    id: string;
    /**
     * @internal Do not use!
     */
    describedBy: string;
    /**
     * Form control of the field.
     */
    form: FormGroup;
    /**
     * Appearance style of the form.
     */
    appearance: MatFormFieldAppearance;
    /**
     * Angular Material color of the form field underline and floating label.
     */
    color: ThemePalette;
    /**
     * Whether the field has been initialized or not.
     * @internal Do not use!
     */
    initialized: boolean;
    /**
     * @internal Do not use!
     */
    constructor(fm: FocusMonitor, elRef: ElementRef, ngControl: NgControl);
    /**
     * @internal Do not use!
     */
    ngOnInit(): void;
    /**
     * The value of the control.
     */
    /**
    * @internal Do not use!
    */
    value: KepsAddress | null;
    /**
     * Whether the control is required.
     */
    /**
    * @internal Do not use!
    */
    required: boolean;
    /**
     * Whether the control is disabled.
     */
    /**
    * @internal Do not use!
    */
    disabled: boolean;
    /**
     * The placeholder for this control.
     */
    /**
    * @internal Do not use!
    */
    placeholder: string;
    /**
     * Whether the control is empty.
     */
    readonly empty: boolean;
    /**
     * Whether the MatFormField label should try to float.
     */
    readonly shouldLabelFloat: boolean;
    /**
     * @internal Do not use!
     */
    setDescribedByIds(ids: string[]): void;
    /**
     * @internal Do not use!
     */
    onContainerClick(event: MouseEvent): void;
    /**
     * @internal Do not use!
     */
    ngOnDestroy(): void;
}
