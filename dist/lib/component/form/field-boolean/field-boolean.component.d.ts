import { OnDestroy, ElementRef } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl, MatRadioChange } from '@angular/material';
import { Subject } from 'rxjs';
/**
 * A custom field that displays two radio buttons: True, False.
 */
export declare class FieldBooleanComponent implements MatFormFieldControl<boolean>, OnDestroy {
    private fm;
    private elRef;
    ngControl: NgControl;
    /**
     * @internal Do not use!
     */
    static nextId: number;
    /**
     * @internal Do not use!
     */
    private _required;
    /**
     * @internal Do not use!
     */
    private _disabled;
    /**
     * @internal Do not use!
     */
    private _placeholder;
    /**
     * @internal Do not use!
     */
    stateChanges: Subject<void>;
    /**
     * @internal Do not use!
     */
    focused: boolean;
    /**
     * @internal Do not use!
     */
    errorState: boolean;
    /**
     * @internal Do not use!
     */
    controlType: string;
    /**
     * @internal Do not use!
     */
    id: string;
    /**
     * @internal Do not use!
     */
    describedBy: string;
    /**
     * Form control of the field.
     */
    form: FormControl;
    /**
     * @internal Do not use!
     */
    constructor(fm: FocusMonitor, elRef: ElementRef<HTMLElement>, ngControl: NgControl);
    /**
     * @internal Do not use!
     */
    onRadioChange(change: MatRadioChange): void;
    /**
     * The value of the control.
     */
    /**
    * @internal Do not use!
    */
    value: boolean | null;
    /**
     * Whether the control is required.
     */
    /**
    * @internal Do not use!
    */
    required: boolean;
    /**
     * Whether the control is disabled.
     */
    /**
    * @internal Do not use!
    */
    disabled: boolean;
    /**
     * The placeholder for this control.
     */
    /**
    * @internal Do not use!
    */
    placeholder: string;
    /**
     * Whether the control is empty.
     */
    readonly empty: boolean;
    /**
     * Whether the MatFormField label should try to float.
     */
    readonly shouldLabelFloat: boolean;
    /**
     * @internal Do not use!
     */
    setDescribedByIds(ids: string[]): void;
    /**
     * @internal Do not use!
     */
    onContainerClick(event: MouseEvent): void;
    /**
     * @internal Do not use!
     */
    ngOnDestroy(): void;
}
