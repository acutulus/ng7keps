import { FormGroup } from '@angular/forms';
import { KepsSchema } from '../../interface';
/**
 * A Keps form.
 *
 * It extends Angular's `FormGroup` class
 * and changes its `patchValue` and `reset` methods to
 * wait until the form is initialized.
 */
export declare class KepsForm extends FormGroup {
    private schema;
    private initialized;
    private initialized$;
    /**
     * Lets the Keps form know that is has been initialized.
     * @internal Do not use!
     */
    initialize(): void;
    /**
     * Sets the schema for this form.
     * @internal Do not use!
     * @param schema Schema to be used.
     */
    setSchema(schema: KepsSchema): void;
    /**
     * Patches the value of the Keps form.
     * It accepts an object with control names as keys, and does its best to match the values to the correct controls in the group.
     *
     * It accepts both super-sets and sub-sets of the group without throwing an error.
     * @param values Values to be passed to the fields.
     * @param options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     */
    patchValue(values: {
        [key: string]: any;
    }, options?: {
        onlySelf?: boolean;
        emitEvent?: boolean;
    }): void;
    /**
     * Resets the Keps form, marks all descendants are marked pristine and untouched, and the value of all descendants to null.
     *
     * You reset to a specific form state by passing in a map of states that matches the structure of your form, with control names as keys.
     * The state is a standalone value or a form state object with both a value and a disabled status.
     * @param values Resets the control with an initial value, or an object that defines the initial value and disabled state.
     * @param options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     */
    reset(values?: any, options?: {
        onlySelf?: boolean;
        emitEvent?: boolean;
    }): void;
}
