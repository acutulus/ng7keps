import { OnDestroy, ElementRef } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl, MatDatepickerInputEvent } from '@angular/material';
import { Subject } from 'rxjs';
import moment from 'moment';
export declare class FieldDatetimeComponent implements MatFormFieldControl<number>, OnDestroy {
    private fm;
    private elRef;
    ngControl: NgControl;
    /**
     * @internal Do not use!
     */
    static nextId: number;
    /**
     * @internal Do not use!
     */
    private _required;
    /**
     * @internal Do not use!
     */
    private _disabled;
    /**
     * @internal Do not use!
     */
    private _placeholder;
    /**
     * @internal Do not use!
     */
    stateChanges: Subject<void>;
    /**
     * @internal Do not use!
     */
    focused: boolean;
    /**
     * @internal Do not use!
     */
    errorState: boolean;
    /**
     * @internal Do not use!
     */
    controlType: string;
    /**
     * @internal Do not use!
     */
    id: string;
    /**
     * @internal Do not use!
     */
    describedBy: string;
    /**
     * Form control of the field.
     */
    form: FormControl;
    /**
     * Form control for the date.
     * @internal Do not use!
     */
    dateForm: FormControl;
    /**
     * The current date for placeholder.
     * @internal Do not use!
     */
    startDate: moment.Moment;
    /**
     * @internal Do not use!
     */
    constructor(fm: FocusMonitor, elRef: ElementRef<HTMLElement>, ngControl: NgControl);
    /**
     * The value of the control.
     */
    /**
    * @internal Do not use!
    */
    value: number | null;
    /**
     * Whether the control is required.
     */
    /**
    * @internal Do not use!
    */
    required: boolean;
    /**
     * Whether the control is disabled.
     */
    /**
    * @internal Do not use!
    */
    disabled: boolean;
    /**
     * The placeholder for this control.
     */
    /**
    * @internal Do not use!
    */
    placeholder: string;
    /**
     * Whether the control is empty.
     */
    readonly empty: boolean;
    /**
     * Whether the MatFormField label should try to float.
     */
    readonly shouldLabelFloat: boolean;
    /**
     * @internal Do not use!
     */
    setDescribedByIds(ids: string[]): void;
    /**
     * @internal Do not use!
     */
    onContainerClick(event: MouseEvent): void;
    /**
     * Sets the forms value after selecting a date on datepicker.
     * @internal Do not use!
     * @param event Changes to the date picker.
     */
    onDateChange(event: MatDatepickerInputEvent<moment.Moment>): void;
    /**
     * @internal Do not use!
     */
    ngOnDestroy(): void;
}
