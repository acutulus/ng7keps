import { OnInit, OnDestroy, ElementRef } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl, MatFormFieldAppearance, ThemePalette } from '@angular/material';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { KepsFormArray } from '../keps-form-array';
import { KepsModels } from '../../../interface';
import { Subject, BehaviorSubject } from 'rxjs';
export declare class FieldArrayComponent implements MatFormFieldControl<any[]>, OnInit, OnDestroy {
    private fm;
    private elRef;
    private models;
    ngControl: NgControl;
    /**
     * @internal Do not use!
     */
    static nextId: number;
    /**
     * @internal Do not use!
     */
    private _required;
    /**
     * @internal Do not use!
     */
    private _disabled;
    /**
     * @internal Do not use!
     */
    private _placeholder;
    /**
     * @internal Do not use!
     */
    stateChanges: Subject<void>;
    /**
     * @internal Do not use!
     */
    focused: boolean;
    /**
     * @internal Do not use!
     */
    errorState: boolean;
    /**
     * @internal Do not use!
     */
    controlType: string;
    /**
     * @internal Do not use!
     */
    id: string;
    /**
     * @internal Do not use!
     */
    describedBy: string;
    /**
     * Form control of the field.
     */
    form: KepsFormArray;
    /**
     * Subschema of the form array.
     */
    subSchema: string;
    /**
     * Objects to be shown for reference field.
     */
    refObjs: any[];
    /**
     * Appearance style of the form.
     */
    appearance: MatFormFieldAppearance;
    /**
     * Angular Material color of the form field underline and floating label.
     */
    color: ThemePalette;
    /**
     * A behavior subject that emits a number which is the length of the form array.
     *
     * It should emit whenever a form control is added/removed from the array.
     * @internal Do not use!
     */
    counter$: BehaviorSubject<number>;
    /**
     * An observable that emits an empty array with the same length as the form array.
     *
     * It is used for *ngFor in this component's HTML.
     * @internal Do not use!
     */
    counterArr$: import("rxjs").Observable<any[]>;
    /**
     * @internal Do not use!
     */
    constructor(fm: FocusMonitor, elRef: ElementRef<HTMLElement>, models: KepsModels, ngControl: NgControl);
    /**
     * @internal Do not use!
     */
    ngOnInit(): void;
    /**
     * Adds a form control to the form array.
     */
    add(): void;
    /**
     * Removes a form control from the form array.
     * @param index Index of the form control to be removed.
     */
    remove(index: number): void;
    /**
     * Rearranges form controls inside the form array.
     * @internal Do not use!
     * @param event The drag event containing the form control.
     */
    drop(event: CdkDragDrop<FormControl[]>): void;
    /**
     * Returns the display expression of a reference field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    getReferenceDisplay(): string;
    /**
     * The value of the control.
     */
    /**
    * @internal Do not use!
    */
    value: any[] | null;
    /**
     * Whether the control is required.
     */
    /**
    * @internal Do not use!
    */
    required: boolean;
    /**
     * Whether the control is disabled.
     */
    /**
    * @internal Do not use!
    */
    disabled: boolean;
    /**
     * The placeholder for this control.
     */
    /**
    * @internal Do not use!
    */
    placeholder: string;
    /**
     * Whether the control is empty.
     */
    readonly empty: boolean;
    /**
     * Whether the MatFormField label should try to float.
     */
    readonly shouldLabelFloat: boolean;
    /**
     * @internal Do not use!
     */
    setDescribedByIds(ids: string[]): void;
    /**
     * @internal Do not use!
     */
    onContainerClick(event: MouseEvent): void;
    /**
     * @internal Do not use!
     */
    ngOnDestroy(): void;
}
