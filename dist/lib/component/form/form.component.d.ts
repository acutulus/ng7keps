import { OnInit } from '@angular/core';
import { ThemePalette, MatFormFieldAppearance } from '@angular/material';
import { KepsForm } from './keps-form';
import { KepsSchema, KepsModels, KepsReference } from '../../interface';
/**
 * A component that renders form based on Keps schema.
 */
export declare class FormComponent implements OnInit {
    private models;
    /**
     * An instance of Keps form.
     */
    form: KepsForm;
    /**
     * Name of the model for the form.
     */
    model: string;
    /**
     * Schema for the form.
     *
     * If the model name is supplied,
     * then the form will get the schema from that model.
     */
    schema: KepsSchema;
    /**
     * A map of reference to Keps objects to be shown for reference fields.
     *
     * Notes:-The key of the map is based on the field name of the Keps form model.
     *        Not the name of the model it is referenced to.
     *       -If field type is a string and a refObjs is provided,
     *        instead of a normal text input, it will be a select. see html for more info.
     *
     * **sample usage**
     * given the following models:
     *  PERSON {
     *    _id
     *    name: 'string',
     *    pet: ':ANIMAL'
     *  }
     *
     *  ANIMAL {
     *    _id
     *    type: 'string'
     *  }
     *
     *  if one wish to create a form for
     *  object PERSON where the options for ANIMAL
     *  comes from DB, that dev can create a refObjs:KepsReference
     *  which  will look like:
     *
     *  refObjs = {
     *              'pet': [
     *                <ANIMAL>{
     *                    _id
     *                    type
     *                  },
     *                <ANIMAL>{
     *                    _id
     *                    type
     *                },
     *                ...
     *                ...
     *                ..
     *              ],
     *            }
     *
     *  SIDENOTE: The annotation <TYPE> is used to make this exmaple more explicit.
     */
    refObjs: KepsReference;
    /**
     * Appearance style of the form.
     */
    appearance: MatFormFieldAppearance;
    /**
     * Angular Material color of the form field underline and floating label.
     */
    color: ThemePalette;
    /**
     * Fields to be displayed on the form.
     */
    displayedFields: string[];
    /**
     * Fields to be hidden on the form.
     */
    hiddenFields: string[];
    /**
     * Placeholders to be displayed on fields.
     */
    placeholders: Record<string, any>;
    /**
     * Hints for fields input.
     */
    hints: Record<string, string>;
    /**
     * Fields to be displayed on the form based on displayedFields and hiddenFields.
     * @internal Do not use!
     */
    schemaFields: string[];
    /**
     * Whether the form has done initializing or not.
     */
    initialized: boolean;
    /**
     * @internal Do not use!
     */
    constructor(models: KepsModels);
    /**
     * Initializes the form with fields based on the schema.
     * @internal Do not use!
     */
    ngOnInit(): void;
    /**
     * Checks the type of a field.
     * @internal Do not use!
     * @param field Field to be checked.
     * @param type Type to be checked.
     */
    isType(field: string, type: string): any;
    /**
     * Returns the display expression of a reference field.
     * @internal Do not use!
     * @param field Field to get from.
     */
    getReferenceDisplay(field: string): string;
    /**
     * Check if MatFormField should hide the underline or not.
     * @internal Do not use!
     * @param field Field to be checked.
     */
    isNotUnderlined(field: string): boolean;
    /**
    * Check if a field has a reference in refObjs.
    * if has reference in refobjs, use mat-select instead of input.
    * supports only type string at the moment.
    * @internal Do not use!
    * @param field Field to be checked.
    */
    hasReference(field: string): import("../../interface").KepsObject[];
}
