import { OnInit, EventEmitter } from '@angular/core';
import { ThemePalette } from '@angular/material';
/**
 * A component that wraps around the default HTML file select button.
 */
export declare class FileSelectComponent implements OnInit {
    /**
     * File to be selected.
     */
    selectedFile: File;
    /**
     * An event emitter that emits the selected file.
     */
    selectedFileChange: EventEmitter<File>;
    /**
     * Label for the file select button.
     */
    label: string;
    /**
     * Whether the component should display the file name.
     */
    showFilename: boolean;
    /**
     * The type(s) of file that the file select dialog can accept.
     *
     * See: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#Unique_file_type_specifiers
     *
     * If the value is "image", "video", or "audio", it will accept every possible file types
     * of that media type.
     */
    type: string;
    /**
     * Angular Material color for the file select button.
     */
    color: ThemePalette;
    /**
     * The `input` element itself.
     */
    private fileSelector;
    /**
     * @internal Do not use!
     */
    constructor();
    /**
     * @internal Do not use!
     */
    ngOnInit(): void;
    /**
     * Open the file select dialog manually.
     */
    openFileSelect(): void;
    /**
     * Select a file.
     * @param files Files to be passed. Only the first one will be selected.
     */
    selectFile(files: File[]): void;
}
