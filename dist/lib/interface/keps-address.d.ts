/**
 * Represents an address. Second line of address and country is optional.
 */
export interface KepsAddress {
    address1: string;
    address2?: string;
    city: string;
    region: string;
    postal: string;
    country?: string;
}
