/**
 * Represents the result of a GraphQL query.
 */
export interface GraphqlResult {
    data: {
        [model: string]: any;
    };
    errors?: string[];
}
