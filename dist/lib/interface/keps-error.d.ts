/**
 * Represents an error received from HTTP request to Keps.
 */
export interface KepsError {
    args: object;
    friendly: string;
    message: string;
    param: string;
    status: number;
}
