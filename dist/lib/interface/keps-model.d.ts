/**
 * Represents a map of KepsModel.
 */
export interface KepsModels {
    [model: string]: KepsModel;
}
/**
 * Represents a Keps model.
 */
export interface KepsModel {
    properties: KepsProperty;
    schema: KepsSchema;
}
/**
 * Represents a Keps model's property.
 */
export interface KepsProperty {
    displayExpression?: string;
}
/**
 * Represents a Keps model's schema.
 */
export interface KepsSchema {
    [field: string]: KepsField;
}
/**
 * Represents a Keps schema's field.
 */
export interface KepsField extends Record<string, any> {
    type: string;
    description?: string;
    label?: string;
    required?: boolean | string;
    optional?: boolean;
    default?: any;
    min?: number;
    max?: number;
    step?: number;
    format?: string;
    options?: string[];
    labels?: {
        [option: string]: string;
    };
    subSchema?: string | KepsSchema;
}
