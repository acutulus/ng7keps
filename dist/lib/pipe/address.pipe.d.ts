import { PipeTransform } from '@angular/core';
import { KepsAddress } from '../interface';
/**
 * A pipe that transforms a Keps address object to a nicely formatted string.
 */
export declare class AddressPipe implements PipeTransform {
    transform(address: KepsAddress, lines?: number): string;
}
