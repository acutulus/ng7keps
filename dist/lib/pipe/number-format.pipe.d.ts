import { PipeTransform } from '@angular/core';
/**
 * A pipe that transforms number to formatted number.
 */
export declare class NumberFormatPipe implements PipeTransform {
    transform(value: number, format?: string): any;
}
