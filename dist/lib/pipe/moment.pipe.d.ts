import { PipeTransform } from '@angular/core';
/**
 * A pipe that transforms UNIX timestamp to a nice moment-formatted string.
 */
export declare class MomentPipe implements PipeTransform {
    transform(timestamp: number, format?: string): string;
}
