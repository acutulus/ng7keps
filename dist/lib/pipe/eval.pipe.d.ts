import { PipeTransform } from '@angular/core';
/**
 * A pipe to evaluate an expression from supplied context.
 */
export declare class EvalPipe implements PipeTransform {
    transform(context: any, expression: string): any;
    private getTokens;
    private parseToken;
    private combineTokens;
    private parseError;
}
