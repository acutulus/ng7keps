import { PipeTransform } from '@angular/core';
/**
 * A pipe that transforms the keys of an object to an array of strings.
 */
export declare class KeysPipe implements PipeTransform {
    transform(obj: object): string[];
}
