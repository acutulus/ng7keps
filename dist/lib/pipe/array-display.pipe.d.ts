import { PipeTransform } from '@angular/core';
/**
 * A pipe that transforms an array to a nicely formatted list.
 */
export declare class ArrayDisplayPipe implements PipeTransform {
    transform(value: any, delimiter?: string, displayExpression?: string): string;
}
