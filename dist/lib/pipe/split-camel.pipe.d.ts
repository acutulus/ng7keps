import { PipeTransform } from '@angular/core';
/**
 * A pipe that transforms a camel-cased string to words separated by space.
 */
export declare class SplitCamelPipe implements PipeTransform {
    transform(str: string): string;
}
