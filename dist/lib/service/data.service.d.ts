import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { KepsObject, GraphqlResult } from '../interface';
/**
 * A service to send requests to Keps server.
 */
export declare class DataService {
    private http;
    private errorService;
    private cache;
    /**
     * @internal Do not use!
     */
    constructor(http: HttpClient, errorService: ErrorService);
    /**
     * Sends a GET request to a route of Keps server.
     * @param route Route to get from.
     * @param id ID of an object.
     * @param useCache Whether we should use cache or not.
     */
    get<T = any>(route: string, id?: string, useCache?: boolean): Promise<T>;
    /**
     * Sends a POST request to a route of Keps server.
     * @param route Route to post to.
     * @param data Data to be passed on to the route.
     */
    post<T = any>(route: string, data: any): Promise<T>;
    /**
     * Sends a PUT request to a route of Keps server.
     * @param route Route to put to.
     * @param data Data to be passed on to the route.
     */
    put<T = any>(route: string, data: any): Promise<T>;
    /**
     * Sends a DELETE request to a route of Keps server.
     * @param type Type of the object to delete (can also be just a route).
     * @param data An ID string or a Keps object to be deleted.
     */
    delete<T = void>(type: string, data: string | KepsObject): Promise<T>;
    /**
     * Sends a request depending on the command to Keps server.
     * @param command A Keps command (e.g. 'put.users.update').
     * @param data Data to be passed on to the server.
     */
    call<T = any>(command: string, data: FormData | any): Promise<T>;
    /**
     * Sends a GraphQL query request to the Keps server and returns the result back.
     *
     * Will throw an error if the result contains any error.
     * @param query A GraphQL query.
     */
    graphql(query: string): Promise<GraphqlResult>;
    /**
     * Clears all stored cache.
     */
    clearCache(): void;
    /**
     * Converts data to FormData if it contains a File or Blob.
     * @param data Data to be normalized.
     */
    private normalizeData;
    /**
     * Serializes data and returns it as a query string.
     * @param data Data to be serialized.
     * @param prefix Prefix for the passed data.
     */
    private serializeData;
}
