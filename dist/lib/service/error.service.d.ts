/**
 * A service to do stuffs related to errors.
 */
export declare class ErrorService {
    /**
     * @internal Do not use!
     */
    constructor();
    /**
     * Normalizes error.
     *
     * Will also log HttpErrorResponse for debugging purpose before normalizing it.
     * @param err Error to be normalized.
     */
    normalizeError(err: Error): Error;
}
