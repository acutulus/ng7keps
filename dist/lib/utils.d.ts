import { KepsModels, KepsSchema } from './interface';
/**
 * A helper function that checks for a schema's field type.
 * @param schema A Keps schema.
 * @param field A field name.
 * @param type A type name to be checked.
 */
export declare const isType: (schema: KepsSchema, field: string, type: string) => any;
/**
 * A helper function to get the display expression of a field in schema.
 * @param models Models from the project to get the reference from.
 * @param schema A Keps schema.
 * @param field A field name.
 */
export declare const getReferenceDisplay: (models: KepsModels, schema: KepsSchema, field: string) => string;
/**
 * A helper function to get the display expression of an array field in schema.
 * @param models Models from the project to get the reference from.
 * @param schema A Keps schema.
 * @param field A field name.
 */
export declare const getReferenceDisplayArray: (models: KepsModels, schema: KepsSchema, field: string) => string;
