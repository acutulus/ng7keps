/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
/**
 * A pipe that transforms a camel-cased string to words separated by space.
 */
export class SplitCamelPipe {
    /**
     * @param {?} str
     * @return {?}
     */
    transform(str) {
        /** @type {?} */
        const re = /([A-Z])([A-Z])([a-z])|([a-z])([A-Z])|([a-z])([0-9])/g;
        return str.replace(re, '$1$4$6 $2$3$5$7');
    }
}
SplitCamelPipe.decorators = [
    { type: Pipe, args: [{
                name: 'splitCamel'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3BsaXQtY2FtZWwucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvcGlwZS9zcGxpdC1jYW1lbC5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQzs7OztBQVFwRCxNQUFNLE9BQU8sY0FBYzs7Ozs7SUFFekIsU0FBUyxDQUFDLEdBQVc7O2NBQ2IsRUFBRSxHQUFHLHNEQUFzRDtRQUNqRSxPQUFPLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUFFLGlCQUFpQixDQUFDLENBQUM7SUFDNUMsQ0FBQzs7O1lBUkYsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxZQUFZO2FBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqIEEgcGlwZSB0aGF0IHRyYW5zZm9ybXMgYSBjYW1lbC1jYXNlZCBzdHJpbmcgdG8gd29yZHMgc2VwYXJhdGVkIGJ5IHNwYWNlLlxyXG4gKi9cclxuQFBpcGUoe1xyXG4gIG5hbWU6ICdzcGxpdENhbWVsJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU3BsaXRDYW1lbFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgdHJhbnNmb3JtKHN0cjogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IHJlID0gLyhbQS1aXSkoW0EtWl0pKFthLXpdKXwoW2Etel0pKFtBLVpdKXwoW2Etel0pKFswLTldKS9nO1xyXG4gICAgcmV0dXJuIHN0ci5yZXBsYWNlKHJlLCAnJDEkNCQ2ICQyJDMkNSQ3Jyk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=