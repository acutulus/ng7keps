/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
/**
 * A pipe to evaluate an expression from supplied context.
 */
export class EvalPipe {
    /**
     * @param {?} context
     * @param {?} expression
     * @return {?}
     */
    transform(context, expression) {
        // If expression is not a string, return the context.
        if (typeof expression !== 'string') {
            return context;
        }
        // Convert expression to tokens.
        /** @type {?} */
        let tokens = this.getTokens(expression);
        // Parse each token based on the context.
        tokens = tokens.map((/**
         * @param {?} token
         * @return {?}
         */
        token => this.parseToken(token, context)));
        // Combine tokens back as a string.
        return this.combineTokens(tokens);
    }
    /**
     * @private
     * @param {?} str
     * @return {?}
     */
    getTokens(str) {
        return str.match(/[^\s"']+|"([^"]*)"|'([^']*)'/gm);
    }
    /**
     * @private
     * @param {?} token
     * @param {?} context
     * @return {?}
     */
    parseToken(token, context) {
        if ((token[0] === '"' && token[token.length - 1] === '"') ||
            (token[0] === `'` && token[token.length - 1] === `'`)) {
            // If token is a quoted string, return the text inside the quotes.
            return token.substring(1, token.length - 1);
        }
        else if (token === '+') {
            // Return '+' as it is.
            return token;
        }
        else if (token[0] === '.') {
            // If the first character is a dot, continue parsing after the dot.
            return this.parseToken(token.substring(1), context);
        }
        else {
            // If context is missing, log the error and return null.
            if (context === null) {
                this.parseError('No context supplied.');
                return null;
            }
            // Check for special characters.
            /** @type {?} */
            const specials = token.match(/(\[|\.)/gm);
            if (specials && specials.length > 0) {
                if (specials[0] === '[') {
                    /** @type {?} */
                    const inbetween = token.match(/\[(\S)\]/);
                    if (inbetween && inbetween.length > 0) {
                        /** @type {?} */
                        const num = parseInt(inbetween[1], 10);
                        /** @type {?} */
                        const before = token.substring(0, token.indexOf('['));
                        if (!context[before]) {
                            this.parseError(`${before} not found`);
                            return null;
                        }
                        /** @type {?} */
                        const after = token.substring(token.indexOf(']') + 1);
                        /** @type {?} */
                        let parsed;
                        if (isNaN(num)) {
                            parsed = context[before][inbetween[1]];
                        }
                        else {
                            parsed = context[before][num];
                        }
                        if (after.length > 0) {
                            return this.parseToken(after, parsed);
                        }
                        else {
                            return parsed;
                        }
                    }
                    else {
                        this.parseError('No closing bracket.');
                        return null;
                    }
                }
                else {
                    /** @type {?} */
                    const i = token.indexOf('.');
                    return this.parseToken(token.substring(i + 1), context[token.substring(0, i)]);
                }
            }
            else {
                return context[token];
            }
        }
    }
    /**
     * @private
     * @param {?} tokens
     * @return {?}
     */
    combineTokens(tokens) {
        /** @type {?} */
        let result;
        for (let i = 0; i < tokens.length; i++) {
            if (i % 2 === 1) {
                if (tokens[i] !== '+') {
                    this.parseError('error');
                    return null;
                }
            }
            else {
                if (!result) {
                    result = tokens[i];
                }
                else {
                    result += tokens[i];
                }
            }
        }
        return result;
    }
    /**
     * @private
     * @param {?} message
     * @return {?}
     */
    parseError(message) {
        console.error('EvalPipeError: ' + message);
    }
}
EvalPipe.decorators = [
    { type: Pipe, args: [{
                name: 'eval'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZhbC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9waXBlL2V2YWwucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7Ozs7QUFRcEQsTUFBTSxPQUFPLFFBQVE7Ozs7OztJQUVuQixTQUFTLENBQUMsT0FBWSxFQUFFLFVBQWtCO1FBQ3hDLHFEQUFxRDtRQUNyRCxJQUFJLE9BQU8sVUFBVSxLQUFLLFFBQVEsRUFBRTtZQUNsQyxPQUFPLE9BQU8sQ0FBQztTQUNoQjs7O1lBR0csTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDO1FBRXZDLHlDQUF5QztRQUN6QyxNQUFNLEdBQUcsTUFBTSxDQUFDLEdBQUc7Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxFQUFDLENBQUM7UUFFOUQsbUNBQW1DO1FBQ25DLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7Ozs7SUFFTyxTQUFTLENBQUMsR0FBVztRQUMzQixPQUFPLEdBQUcsQ0FBQyxLQUFLLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztJQUNyRCxDQUFDOzs7Ozs7O0lBRU8sVUFBVSxDQUFDLEtBQWEsRUFBRSxPQUFlO1FBQy9DLElBQ0UsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQztZQUNyRCxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQ3JEO1lBQ0Esa0VBQWtFO1lBQ2xFLE9BQU8sS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztTQUM3QzthQUFNLElBQUksS0FBSyxLQUFLLEdBQUcsRUFBRTtZQUN4Qix1QkFBdUI7WUFDdkIsT0FBTyxLQUFLLENBQUM7U0FDZDthQUFNLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRTtZQUMzQixtRUFBbUU7WUFDbkUsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7U0FDckQ7YUFBTTtZQUNMLHdEQUF3RDtZQUN4RCxJQUFJLE9BQU8sS0FBSyxJQUFJLEVBQUU7Z0JBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDeEMsT0FBTyxJQUFJLENBQUM7YUFDYjs7O2tCQUdLLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQztZQUN6QyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDbkMsSUFBSSxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFOzswQkFDakIsU0FBUyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO29CQUN6QyxJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7OEJBQy9CLEdBQUcsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQzs7OEJBQ2hDLE1BQU0sR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUNyRCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFOzRCQUNwQixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsTUFBTSxZQUFZLENBQUMsQ0FBQzs0QkFDdkMsT0FBTyxJQUFJLENBQUM7eUJBQ2I7OzhCQUNLLEtBQUssR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDOzs0QkFDakQsTUFBVzt3QkFDZixJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRTs0QkFDZCxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3lCQUN4Qzs2QkFBTTs0QkFDTCxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3lCQUMvQjt3QkFDRCxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzRCQUNwQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO3lCQUN2Qzs2QkFBTTs0QkFDTCxPQUFPLE1BQU0sQ0FBQzt5QkFDZjtxQkFDRjt5QkFBTTt3QkFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLHFCQUFxQixDQUFDLENBQUM7d0JBQ3ZDLE9BQU8sSUFBSSxDQUFDO3FCQUNiO2lCQUNGO3FCQUFNOzswQkFDQyxDQUFDLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7b0JBQzVCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNoRjthQUNGO2lCQUFNO2dCQUNMLE9BQU8sT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3ZCO1NBQ0Y7SUFDSCxDQUFDOzs7Ozs7SUFFTyxhQUFhLENBQUMsTUFBYTs7WUFDN0IsTUFBVztRQUNmLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3RDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2YsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFO29CQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUN6QixPQUFPLElBQUksQ0FBQztpQkFDYjthQUNGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxNQUFNLEVBQUU7b0JBQ1gsTUFBTSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDcEI7cUJBQU07b0JBQ0wsTUFBTSxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDckI7YUFDRjtTQUNGO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQzs7Ozs7O0lBRU8sVUFBVSxDQUFDLE9BQWU7UUFDaEMsT0FBTyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsR0FBRyxPQUFPLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7WUF4R0YsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxNQUFNO2FBQ2IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vKipcclxuICogQSBwaXBlIHRvIGV2YWx1YXRlIGFuIGV4cHJlc3Npb24gZnJvbSBzdXBwbGllZCBjb250ZXh0LlxyXG4gKi9cclxuQFBpcGUoe1xyXG4gIG5hbWU6ICdldmFsJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRXZhbFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgdHJhbnNmb3JtKGNvbnRleHQ6IGFueSwgZXhwcmVzc2lvbjogc3RyaW5nKTogYW55IHtcclxuICAgIC8vIElmIGV4cHJlc3Npb24gaXMgbm90IGEgc3RyaW5nLCByZXR1cm4gdGhlIGNvbnRleHQuXHJcbiAgICBpZiAodHlwZW9mIGV4cHJlc3Npb24gIT09ICdzdHJpbmcnKSB7XHJcbiAgICAgIHJldHVybiBjb250ZXh0O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENvbnZlcnQgZXhwcmVzc2lvbiB0byB0b2tlbnMuXHJcbiAgICBsZXQgdG9rZW5zID0gdGhpcy5nZXRUb2tlbnMoZXhwcmVzc2lvbik7XHJcblxyXG4gICAgLy8gUGFyc2UgZWFjaCB0b2tlbiBiYXNlZCBvbiB0aGUgY29udGV4dC5cclxuICAgIHRva2VucyA9IHRva2Vucy5tYXAodG9rZW4gPT4gdGhpcy5wYXJzZVRva2VuKHRva2VuLCBjb250ZXh0KSk7XHJcblxyXG4gICAgLy8gQ29tYmluZSB0b2tlbnMgYmFjayBhcyBhIHN0cmluZy5cclxuICAgIHJldHVybiB0aGlzLmNvbWJpbmVUb2tlbnModG9rZW5zKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0VG9rZW5zKHN0cjogc3RyaW5nKTogc3RyaW5nW10ge1xyXG4gICAgcmV0dXJuIHN0ci5tYXRjaCgvW15cXHNcIiddK3xcIihbXlwiXSopXCJ8JyhbXiddKiknL2dtKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcGFyc2VUb2tlbih0b2tlbjogc3RyaW5nLCBjb250ZXh0OiBvYmplY3QpOiBhbnkge1xyXG4gICAgaWYgKFxyXG4gICAgICAodG9rZW5bMF0gPT09ICdcIicgJiYgdG9rZW5bdG9rZW4ubGVuZ3RoIC0gMV0gPT09ICdcIicpIHx8XHJcbiAgICAgICh0b2tlblswXSA9PT0gYCdgICYmIHRva2VuW3Rva2VuLmxlbmd0aCAtIDFdID09PSBgJ2ApXHJcbiAgICApIHtcclxuICAgICAgLy8gSWYgdG9rZW4gaXMgYSBxdW90ZWQgc3RyaW5nLCByZXR1cm4gdGhlIHRleHQgaW5zaWRlIHRoZSBxdW90ZXMuXHJcbiAgICAgIHJldHVybiB0b2tlbi5zdWJzdHJpbmcoMSwgdG9rZW4ubGVuZ3RoIC0gMSk7XHJcbiAgICB9IGVsc2UgaWYgKHRva2VuID09PSAnKycpIHtcclxuICAgICAgLy8gUmV0dXJuICcrJyBhcyBpdCBpcy5cclxuICAgICAgcmV0dXJuIHRva2VuO1xyXG4gICAgfSBlbHNlIGlmICh0b2tlblswXSA9PT0gJy4nKSB7XHJcbiAgICAgIC8vIElmIHRoZSBmaXJzdCBjaGFyYWN0ZXIgaXMgYSBkb3QsIGNvbnRpbnVlIHBhcnNpbmcgYWZ0ZXIgdGhlIGRvdC5cclxuICAgICAgcmV0dXJuIHRoaXMucGFyc2VUb2tlbih0b2tlbi5zdWJzdHJpbmcoMSksIGNvbnRleHQpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gSWYgY29udGV4dCBpcyBtaXNzaW5nLCBsb2cgdGhlIGVycm9yIGFuZCByZXR1cm4gbnVsbC5cclxuICAgICAgaWYgKGNvbnRleHQgPT09IG51bGwpIHtcclxuICAgICAgICB0aGlzLnBhcnNlRXJyb3IoJ05vIGNvbnRleHQgc3VwcGxpZWQuJyk7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIENoZWNrIGZvciBzcGVjaWFsIGNoYXJhY3RlcnMuXHJcbiAgICAgIGNvbnN0IHNwZWNpYWxzID0gdG9rZW4ubWF0Y2goLyhcXFt8XFwuKS9nbSk7XHJcbiAgICAgIGlmIChzcGVjaWFscyAmJiBzcGVjaWFscy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgaWYgKHNwZWNpYWxzWzBdID09PSAnWycpIHtcclxuICAgICAgICAgIGNvbnN0IGluYmV0d2VlbiA9IHRva2VuLm1hdGNoKC9cXFsoXFxTKVxcXS8pO1xyXG4gICAgICAgICAgaWYgKGluYmV0d2VlbiAmJiBpbmJldHdlZW4ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBudW0gPSBwYXJzZUludChpbmJldHdlZW5bMV0sIDEwKTtcclxuICAgICAgICAgICAgY29uc3QgYmVmb3JlID0gdG9rZW4uc3Vic3RyaW5nKDAsIHRva2VuLmluZGV4T2YoJ1snKSk7XHJcbiAgICAgICAgICAgIGlmICghY29udGV4dFtiZWZvcmVdKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5wYXJzZUVycm9yKGAke2JlZm9yZX0gbm90IGZvdW5kYCk7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgYWZ0ZXIgPSB0b2tlbi5zdWJzdHJpbmcodG9rZW4uaW5kZXhPZignXScpICsgMSk7XHJcbiAgICAgICAgICAgIGxldCBwYXJzZWQ6IGFueTtcclxuICAgICAgICAgICAgaWYgKGlzTmFOKG51bSkpIHtcclxuICAgICAgICAgICAgICBwYXJzZWQgPSBjb250ZXh0W2JlZm9yZV1baW5iZXR3ZWVuWzFdXTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBwYXJzZWQgPSBjb250ZXh0W2JlZm9yZV1bbnVtXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoYWZ0ZXIubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgIHJldHVybiB0aGlzLnBhcnNlVG9rZW4oYWZ0ZXIsIHBhcnNlZCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIHBhcnNlZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5wYXJzZUVycm9yKCdObyBjbG9zaW5nIGJyYWNrZXQuJyk7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb25zdCBpID0gdG9rZW4uaW5kZXhPZignLicpO1xyXG4gICAgICAgICAgcmV0dXJuIHRoaXMucGFyc2VUb2tlbih0b2tlbi5zdWJzdHJpbmcoaSArIDEpLCBjb250ZXh0W3Rva2VuLnN1YnN0cmluZygwLCBpKV0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gY29udGV4dFt0b2tlbl07XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgY29tYmluZVRva2Vucyh0b2tlbnM6IGFueVtdKSB7XHJcbiAgICBsZXQgcmVzdWx0OiBhbnk7XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRva2Vucy5sZW5ndGg7IGkrKykge1xyXG4gICAgICBpZiAoaSAlIDIgPT09IDEpIHtcclxuICAgICAgICBpZiAodG9rZW5zW2ldICE9PSAnKycpIHtcclxuICAgICAgICAgIHRoaXMucGFyc2VFcnJvcignZXJyb3InKTtcclxuICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAoIXJlc3VsdCkge1xyXG4gICAgICAgICAgcmVzdWx0ID0gdG9rZW5zW2ldO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICByZXN1bHQgKz0gdG9rZW5zW2ldO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIHJlc3VsdDtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcGFyc2VFcnJvcihtZXNzYWdlOiBzdHJpbmcpIHtcclxuICAgIGNvbnNvbGUuZXJyb3IoJ0V2YWxQaXBlRXJyb3I6ICcgKyBtZXNzYWdlKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==