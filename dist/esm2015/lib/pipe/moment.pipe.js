/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import moment from 'moment';
/**
 * A pipe that transforms UNIX timestamp to a nice moment-formatted string.
 */
export class MomentPipe {
    /**
     * @param {?} timestamp
     * @param {?=} format
     * @return {?}
     */
    transform(timestamp, format = 'MM/DD/YYYY') {
        return moment(timestamp).format(format);
    }
}
MomentPipe.decorators = [
    { type: Pipe, args: [{
                name: 'moment'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9tZW50LnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL3BpcGUvbW9tZW50LnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ3BELE9BQU8sTUFBTSxNQUFNLFFBQVEsQ0FBQzs7OztBQVE1QixNQUFNLE9BQU8sVUFBVTs7Ozs7O0lBRXJCLFNBQVMsQ0FBQyxTQUFpQixFQUFFLFNBQWlCLFlBQVk7UUFDeEQsT0FBTyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzFDLENBQUM7OztZQVBGLElBQUksU0FBQztnQkFDSixJQUFJLEVBQUUsUUFBUTthQUNmIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCc7XHJcblxyXG4vKipcclxuICogQSBwaXBlIHRoYXQgdHJhbnNmb3JtcyBVTklYIHRpbWVzdGFtcCB0byBhIG5pY2UgbW9tZW50LWZvcm1hdHRlZCBzdHJpbmcuXHJcbiAqL1xyXG5AUGlwZSh7XHJcbiAgbmFtZTogJ21vbWVudCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE1vbWVudFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgdHJhbnNmb3JtKHRpbWVzdGFtcDogbnVtYmVyLCBmb3JtYXQ6IHN0cmluZyA9ICdNTS9ERC9ZWVlZJyk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gbW9tZW50KHRpbWVzdGFtcCkuZm9ybWF0KGZvcm1hdCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=