/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
/**
 * A pipe that transforms a Keps address object to a nicely formatted string.
 */
export class AddressPipe {
    /**
     * @param {?} address
     * @param {?=} lines
     * @return {?}
     */
    transform(address, lines = 4) {
        /** @type {?} */
        let str = '';
        if (!address) {
            return str;
        }
        str += address.address1;
        if (lines === 4 && address.address2) {
            str += `\n${address.address2}`;
        }
        else if (lines === 3 && address.address2) {
            str += `, ${address.address2}`;
        }
        str += `\n${address.city}, ${address.region} ${address.postal}`;
        str += `\n${address.country}`;
        return str;
    }
}
AddressPipe.decorators = [
    { type: Pipe, args: [{
                name: 'address'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkcmVzcy5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9waXBlL2FkZHJlc3MucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7Ozs7QUFTcEQsTUFBTSxPQUFPLFdBQVc7Ozs7OztJQUV0QixTQUFTLENBQUMsT0FBb0IsRUFBRSxRQUFnQixDQUFDOztZQUMzQyxHQUFHLEdBQUcsRUFBRTtRQUVaLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixPQUFPLEdBQUcsQ0FBQztTQUNaO1FBRUQsR0FBRyxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUM7UUFDeEIsSUFBSSxLQUFLLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDbkMsR0FBRyxJQUFJLEtBQUssT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2hDO2FBQU0sSUFBSSxLQUFLLEtBQUssQ0FBQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDMUMsR0FBRyxJQUFJLEtBQUssT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2hDO1FBQ0QsR0FBRyxJQUFJLEtBQUssT0FBTyxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsTUFBTSxJQUFJLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNoRSxHQUFHLElBQUksS0FBSyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7UUFFOUIsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDOzs7WUF0QkYsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxTQUFTO2FBQ2hCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBLZXBzQWRkcmVzcyB9IGZyb20gJy4uL2ludGVyZmFjZSc7XHJcblxyXG4vKipcclxuICogQSBwaXBlIHRoYXQgdHJhbnNmb3JtcyBhIEtlcHMgYWRkcmVzcyBvYmplY3QgdG8gYSBuaWNlbHkgZm9ybWF0dGVkIHN0cmluZy5cclxuICovXHJcbkBQaXBlKHtcclxuICBuYW1lOiAnYWRkcmVzcydcclxufSlcclxuZXhwb3J0IGNsYXNzIEFkZHJlc3NQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcblxyXG4gIHRyYW5zZm9ybShhZGRyZXNzOiBLZXBzQWRkcmVzcywgbGluZXM6IG51bWJlciA9IDQpOiBzdHJpbmcge1xyXG4gICAgbGV0IHN0ciA9ICcnO1xyXG5cclxuICAgIGlmICghYWRkcmVzcykge1xyXG4gICAgICByZXR1cm4gc3RyO1xyXG4gICAgfVxyXG5cclxuICAgIHN0ciArPSBhZGRyZXNzLmFkZHJlc3MxO1xyXG4gICAgaWYgKGxpbmVzID09PSA0ICYmIGFkZHJlc3MuYWRkcmVzczIpIHtcclxuICAgICAgc3RyICs9IGBcXG4ke2FkZHJlc3MuYWRkcmVzczJ9YDtcclxuICAgIH0gZWxzZSBpZiAobGluZXMgPT09IDMgJiYgYWRkcmVzcy5hZGRyZXNzMikge1xyXG4gICAgICBzdHIgKz0gYCwgJHthZGRyZXNzLmFkZHJlc3MyfWA7XHJcbiAgICB9XHJcbiAgICBzdHIgKz0gYFxcbiR7YWRkcmVzcy5jaXR5fSwgJHthZGRyZXNzLnJlZ2lvbn0gJHthZGRyZXNzLnBvc3RhbH1gO1xyXG4gICAgc3RyICs9IGBcXG4ke2FkZHJlc3MuY291bnRyeX1gO1xyXG5cclxuICAgIHJldHVybiBzdHI7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=