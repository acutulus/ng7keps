/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import { DecimalPipe } from '@angular/common';
/**
 * A pipe that transforms number to formatted number.
 */
export class NumberFormatPipe {
    /**
     * @param {?} value
     * @param {?=} format
     * @return {?}
     */
    transform(value, format = '') {
        if (typeof value !== 'number' || !format) {
            return value;
        }
        else {
            // Get prefix.
            /** @type {?} */
            let prefix = '';
            while (!format.startsWith('0') && format.length > 1) {
                prefix += format[0];
                format = format.slice(1);
            }
            // Get number of 0s.
            /** @type {?} */
            const re = /[0]+/g;
            /** @type {?} */
            const zeroes = format.match(re);
            /** @type {?} */
            const decimalPipe = new DecimalPipe('en-US');
            /** @type {?} */
            let number = '';
            if (zeroes.length === 1) {
                number = decimalPipe.transform(value, `${0}.${zeroes[0].length}-${zeroes[0].length}`);
            }
            else if (zeroes.length === 2) {
                number = decimalPipe.transform(value, `${zeroes[0].length}.${zeroes[1].length}-${zeroes[1].length}`);
            }
            else if (zeroes.length === 3) {
                number = decimalPipe.transform(value, `${zeroes[1].length}.${zeroes[2].length}-${zeroes[2].length}`);
            }
            else {
                number = value.toString();
            }
            // Get positions after zeroes.
            /** @type {?} */
            const pos = [];
            /** @type {?} */
            let x;
            while ((x = re.exec(format)) !== null) {
                pos.push(re.lastIndex);
            }
            // Get symbols.
            /** @type {?} */
            const symbols = [];
            for (let i = 1; i <= pos.length; i++) {
                /** @type {?} */
                const symbol = format.substring(pos[i - 1], i === pos.length ? format.length : pos[i] - 1);
                symbols.push(symbol.replace(/[0]/g, ''));
            }
            /** @type {?} */
            let suffix = '';
            if (zeroes.length === 2) {
                number = number.replace(/\./g, symbols.shift());
                suffix = symbols.shift() || '';
            }
            else if (zeroes.length === 3) {
                number = number.replace(/\,/g, '$');
                number = number.replace(/\./g, '^');
                number = number.replace(/\$/g, symbols.shift());
                number = number.replace(/\^/g, symbols.shift());
                suffix = symbols.shift() || '';
            }
            return prefix + number + suffix;
        }
    }
}
NumberFormatPipe.decorators = [
    { type: Pipe, args: [{
                name: 'numberFormat'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtYmVyLWZvcm1hdC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9waXBlL251bWJlci1mb3JtYXQucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDcEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDOzs7O0FBUTlDLE1BQU0sT0FBTyxnQkFBZ0I7Ozs7OztJQUUzQixTQUFTLENBQUMsS0FBYSxFQUFFLE1BQU0sR0FBRyxFQUFFO1FBQ2xDLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3hDLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTTs7O2dCQUVELE1BQU0sR0FBRyxFQUFFO1lBQ2YsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ25ELE1BQU0sSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzFCOzs7a0JBR0ssRUFBRSxHQUFHLE9BQU87O2tCQUNaLE1BQU0sR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQzs7a0JBQ3pCLFdBQVcsR0FBRyxJQUFJLFdBQVcsQ0FBQyxPQUFPLENBQUM7O2dCQUN4QyxNQUFNLEdBQUcsRUFBRTtZQUNmLElBQUksTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQ3ZCLE1BQU0sR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZGO2lCQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQzlCLE1BQU0sR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQzthQUN0RztpQkFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dCQUM5QixNQUFNLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7YUFDdEc7aUJBQU07Z0JBQ0wsTUFBTSxHQUFHLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUMzQjs7O2tCQUdLLEdBQUcsR0FBRyxFQUFFOztnQkFDVixDQUFNO1lBQ1YsT0FBTyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUNyQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUN4Qjs7O2tCQUdLLE9BQU8sR0FBRyxFQUFFO1lBQ2xCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztzQkFDOUIsTUFBTSxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDMUYsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzFDOztnQkFFRyxNQUFNLEdBQUcsRUFBRTtZQUNmLElBQUksTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQ3ZCLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDaEQsTUFBTSxHQUFHLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUM7YUFDaEM7aUJBQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDOUIsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNwQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3BDLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDaEQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUNoRCxNQUFNLEdBQUcsT0FBTyxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQzthQUNoQztZQUVELE9BQU8sTUFBTSxHQUFHLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDakM7SUFDSCxDQUFDOzs7WUEzREYsSUFBSSxTQUFDO2dCQUNKLElBQUksRUFBRSxjQUFjO2FBQ3JCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEZWNpbWFsUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG4vKipcclxuICogQSBwaXBlIHRoYXQgdHJhbnNmb3JtcyBudW1iZXIgdG8gZm9ybWF0dGVkIG51bWJlci5cclxuICovXHJcbkBQaXBlKHtcclxuICBuYW1lOiAnbnVtYmVyRm9ybWF0J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTnVtYmVyRm9ybWF0UGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG5cclxuICB0cmFuc2Zvcm0odmFsdWU6IG51bWJlciwgZm9ybWF0ID0gJycpOiBhbnkge1xyXG4gICAgaWYgKHR5cGVvZiB2YWx1ZSAhPT0gJ251bWJlcicgfHwgIWZvcm1hdCkge1xyXG4gICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBHZXQgcHJlZml4LlxyXG4gICAgICBsZXQgcHJlZml4ID0gJyc7XHJcbiAgICAgIHdoaWxlICghZm9ybWF0LnN0YXJ0c1dpdGgoJzAnKSAmJiBmb3JtYXQubGVuZ3RoID4gMSkge1xyXG4gICAgICAgIHByZWZpeCArPSBmb3JtYXRbMF07XHJcbiAgICAgICAgZm9ybWF0ID0gZm9ybWF0LnNsaWNlKDEpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBHZXQgbnVtYmVyIG9mIDBzLlxyXG4gICAgICBjb25zdCByZSA9IC9bMF0rL2c7XHJcbiAgICAgIGNvbnN0IHplcm9lcyA9IGZvcm1hdC5tYXRjaChyZSk7XHJcbiAgICAgIGNvbnN0IGRlY2ltYWxQaXBlID0gbmV3IERlY2ltYWxQaXBlKCdlbi1VUycpO1xyXG4gICAgICBsZXQgbnVtYmVyID0gJyc7XHJcbiAgICAgIGlmICh6ZXJvZXMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgbnVtYmVyID0gZGVjaW1hbFBpcGUudHJhbnNmb3JtKHZhbHVlLCBgJHswfS4ke3plcm9lc1swXS5sZW5ndGh9LSR7emVyb2VzWzBdLmxlbmd0aH1gKTtcclxuICAgICAgfSBlbHNlIGlmICh6ZXJvZXMubGVuZ3RoID09PSAyKSB7XHJcbiAgICAgICAgbnVtYmVyID0gZGVjaW1hbFBpcGUudHJhbnNmb3JtKHZhbHVlLCBgJHt6ZXJvZXNbMF0ubGVuZ3RofS4ke3plcm9lc1sxXS5sZW5ndGh9LSR7emVyb2VzWzFdLmxlbmd0aH1gKTtcclxuICAgICAgfSBlbHNlIGlmICh6ZXJvZXMubGVuZ3RoID09PSAzKSB7XHJcbiAgICAgICAgbnVtYmVyID0gZGVjaW1hbFBpcGUudHJhbnNmb3JtKHZhbHVlLCBgJHt6ZXJvZXNbMV0ubGVuZ3RofS4ke3plcm9lc1syXS5sZW5ndGh9LSR7emVyb2VzWzJdLmxlbmd0aH1gKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBudW1iZXIgPSB2YWx1ZS50b1N0cmluZygpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBHZXQgcG9zaXRpb25zIGFmdGVyIHplcm9lcy5cclxuICAgICAgY29uc3QgcG9zID0gW107XHJcbiAgICAgIGxldCB4OiBhbnk7XHJcbiAgICAgIHdoaWxlICgoeCA9IHJlLmV4ZWMoZm9ybWF0KSkgIT09IG51bGwpIHtcclxuICAgICAgICBwb3MucHVzaChyZS5sYXN0SW5kZXgpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBHZXQgc3ltYm9scy5cclxuICAgICAgY29uc3Qgc3ltYm9scyA9IFtdO1xyXG4gICAgICBmb3IgKGxldCBpID0gMTsgaSA8PSBwb3MubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBjb25zdCBzeW1ib2wgPSBmb3JtYXQuc3Vic3RyaW5nKHBvc1tpIC0gMV0sIGkgPT09IHBvcy5sZW5ndGggPyBmb3JtYXQubGVuZ3RoIDogcG9zW2ldIC0gMSk7XHJcbiAgICAgICAgc3ltYm9scy5wdXNoKHN5bWJvbC5yZXBsYWNlKC9bMF0vZywgJycpKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgbGV0IHN1ZmZpeCA9ICcnO1xyXG4gICAgICBpZiAoemVyb2VzLmxlbmd0aCA9PT0gMikge1xyXG4gICAgICAgIG51bWJlciA9IG51bWJlci5yZXBsYWNlKC9cXC4vZywgc3ltYm9scy5zaGlmdCgpKTtcclxuICAgICAgICBzdWZmaXggPSBzeW1ib2xzLnNoaWZ0KCkgfHwgJyc7XHJcbiAgICAgIH0gZWxzZSBpZiAoemVyb2VzLmxlbmd0aCA9PT0gMykge1xyXG4gICAgICAgIG51bWJlciA9IG51bWJlci5yZXBsYWNlKC9cXCwvZywgJyQnKTtcclxuICAgICAgICBudW1iZXIgPSBudW1iZXIucmVwbGFjZSgvXFwuL2csICdeJyk7XHJcbiAgICAgICAgbnVtYmVyID0gbnVtYmVyLnJlcGxhY2UoL1xcJC9nLCBzeW1ib2xzLnNoaWZ0KCkpO1xyXG4gICAgICAgIG51bWJlciA9IG51bWJlci5yZXBsYWNlKC9cXF4vZywgc3ltYm9scy5zaGlmdCgpKTtcclxuICAgICAgICBzdWZmaXggPSBzeW1ib2xzLnNoaWZ0KCkgfHwgJyc7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBwcmVmaXggKyBudW1iZXIgKyBzdWZmaXg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=