/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import { EvalPipe } from './eval.pipe';
import _ from 'lodash';
/**
 * A pipe that transforms an array to a nicely formatted list.
 */
export class ArrayDisplayPipe {
    /**
     * @param {?} value
     * @param {?=} delimiter
     * @param {?=} displayExpression
     * @return {?}
     */
    transform(value, delimiter = ', ', displayExpression = '') {
        if (Array.isArray(value)) {
            // Assume that array contains object of the same type.
            if (_.isPlainObject(value[0])) {
                // If it's an array of objects, map the objects using display expression.
                /** @type {?} */
                const evalPipe = new EvalPipe();
                value = value.map((/**
                 * @param {?} obj
                 * @return {?}
                 */
                obj => evalPipe.transform(obj, displayExpression)));
            }
            return value.join(delimiter);
        }
        else {
            return value;
        }
    }
}
ArrayDisplayPipe.decorators = [
    { type: Pipe, args: [{
                name: 'arrayDisplay'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXJyYXktZGlzcGxheS5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9waXBlL2FycmF5LWRpc3BsYXkucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDcEQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUN2QyxPQUFPLENBQUMsTUFBTSxRQUFRLENBQUM7Ozs7QUFRdkIsTUFBTSxPQUFPLGdCQUFnQjs7Ozs7OztJQUUzQixTQUFTLENBQUMsS0FBVSxFQUFFLFNBQVMsR0FBRyxJQUFJLEVBQUUsaUJBQWlCLEdBQUcsRUFBRTtRQUM1RCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDeEIsc0RBQXNEO1lBQ3RELElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTs7O3NCQUV2QixRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUU7Z0JBQy9CLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRzs7OztnQkFBQyxHQUFHLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLGlCQUFpQixDQUFDLEVBQUMsQ0FBQzthQUN0RTtZQUNELE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUM5QjthQUFNO1lBQ0wsT0FBTyxLQUFLLENBQUM7U0FDZDtJQUNILENBQUM7OztZQWpCRixJQUFJLFNBQUM7Z0JBQ0osSUFBSSxFQUFFLGNBQWM7YUFDckIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEV2YWxQaXBlIH0gZnJvbSAnLi9ldmFsLnBpcGUnO1xyXG5pbXBvcnQgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuLyoqXHJcbiAqIEEgcGlwZSB0aGF0IHRyYW5zZm9ybXMgYW4gYXJyYXkgdG8gYSBuaWNlbHkgZm9ybWF0dGVkIGxpc3QuXHJcbiAqL1xyXG5AUGlwZSh7XHJcbiAgbmFtZTogJ2FycmF5RGlzcGxheSdcclxufSlcclxuZXhwb3J0IGNsYXNzIEFycmF5RGlzcGxheVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIGRlbGltaXRlciA9ICcsICcsIGRpc3BsYXlFeHByZXNzaW9uID0gJycpOiBzdHJpbmcge1xyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgIC8vIEFzc3VtZSB0aGF0IGFycmF5IGNvbnRhaW5zIG9iamVjdCBvZiB0aGUgc2FtZSB0eXBlLlxyXG4gICAgICBpZiAoXy5pc1BsYWluT2JqZWN0KHZhbHVlWzBdKSkge1xyXG4gICAgICAgIC8vIElmIGl0J3MgYW4gYXJyYXkgb2Ygb2JqZWN0cywgbWFwIHRoZSBvYmplY3RzIHVzaW5nIGRpc3BsYXkgZXhwcmVzc2lvbi5cclxuICAgICAgICBjb25zdCBldmFsUGlwZSA9IG5ldyBFdmFsUGlwZSgpO1xyXG4gICAgICAgIHZhbHVlID0gdmFsdWUubWFwKG9iaiA9PiBldmFsUGlwZS50cmFuc2Zvcm0ob2JqLCBkaXNwbGF5RXhwcmVzc2lvbikpO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB2YWx1ZS5qb2luKGRlbGltaXRlcik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=