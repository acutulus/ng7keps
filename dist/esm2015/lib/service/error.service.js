/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import * as i0 from "@angular/core";
/**
 * A service to do stuffs related to errors.
 */
export class ErrorService {
    /**
     * \@internal Do not use!
     */
    constructor() { }
    /**
     * Normalizes error.
     *
     * Will also log HttpErrorResponse for debugging purpose before normalizing it.
     * @param {?} err Error to be normalized.
     * @return {?}
     */
    normalizeError(err) {
        if (err instanceof HttpErrorResponse) {
            console.error(err);
            // Check if error from Keps.
            if (err.error && err.error.errors && err.error.errors[0]) {
                /** @type {?} */
                const kepsErr = err.error.errors[0];
                err = new Error(kepsErr.friendly || kepsErr.message);
            }
            else {
                err = new Error(err.message);
            }
        }
        return err;
    }
}
ErrorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ErrorService.ctorParameters = () => [];
/** @nocollapse */ ErrorService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ErrorService_Factory() { return new ErrorService(); }, token: ErrorService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3Iuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvc2VydmljZS9lcnJvci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDOzs7OztBQVN6RCxNQUFNLE9BQU8sWUFBWTs7OztJQUt2QixnQkFBZ0IsQ0FBQzs7Ozs7Ozs7SUFRakIsY0FBYyxDQUFDLEdBQVU7UUFDdkIsSUFBSSxHQUFHLFlBQVksaUJBQWlCLEVBQUU7WUFDcEMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQiw0QkFBNEI7WUFDNUIsSUFBSSxHQUFHLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFOztzQkFDbEQsT0FBTyxHQUFjLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDOUMsR0FBRyxHQUFHLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3REO2lCQUFNO2dCQUNMLEdBQUcsR0FBRyxJQUFJLEtBQUssQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDOUI7U0FDRjtRQUNELE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQzs7O1lBNUJGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEtlcHNFcnJvciB9IGZyb20gJy4uL2ludGVyZmFjZSc7XHJcblxyXG4vKipcclxuICogQSBzZXJ2aWNlIHRvIGRvIHN0dWZmcyByZWxhdGVkIHRvIGVycm9ycy5cclxuICovXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEVycm9yU2VydmljZSB7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIC8qKlxyXG4gICAqIE5vcm1hbGl6ZXMgZXJyb3IuXHJcbiAgICpcclxuICAgKiBXaWxsIGFsc28gbG9nIEh0dHBFcnJvclJlc3BvbnNlIGZvciBkZWJ1Z2dpbmcgcHVycG9zZSBiZWZvcmUgbm9ybWFsaXppbmcgaXQuXHJcbiAgICogQHBhcmFtIGVyciBFcnJvciB0byBiZSBub3JtYWxpemVkLlxyXG4gICAqL1xyXG4gIG5vcm1hbGl6ZUVycm9yKGVycjogRXJyb3IpOiBFcnJvciB7XHJcbiAgICBpZiAoZXJyIGluc3RhbmNlb2YgSHR0cEVycm9yUmVzcG9uc2UpIHtcclxuICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xyXG4gICAgICAvLyBDaGVjayBpZiBlcnJvciBmcm9tIEtlcHMuXHJcbiAgICAgIGlmIChlcnIuZXJyb3IgJiYgZXJyLmVycm9yLmVycm9ycyAmJiBlcnIuZXJyb3IuZXJyb3JzWzBdKSB7XHJcbiAgICAgICAgY29uc3Qga2Vwc0VycjogS2Vwc0Vycm9yID0gZXJyLmVycm9yLmVycm9yc1swXTtcclxuICAgICAgICBlcnIgPSBuZXcgRXJyb3Ioa2Vwc0Vyci5mcmllbmRseSB8fCBrZXBzRXJyLm1lc3NhZ2UpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGVyciA9IG5ldyBFcnJvcihlcnIubWVzc2FnZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBlcnI7XHJcbiAgfVxyXG59XHJcbiJdfQ==