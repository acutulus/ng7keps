/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { apiRoute } from '../config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "./error.service";
/**
 * A service to send requests to Keps server.
 */
export class DataService {
    /**
     * \@internal Do not use!
     * @param {?} http
     * @param {?} errorService
     */
    constructor(http, errorService) {
        this.http = http;
        this.errorService = errorService;
        this.cache = {};
    }
    /**
     * Sends a GET request to a route of Keps server.
     * @template T
     * @param {?} route Route to get from.
     * @param {?=} id ID of an object.
     * @param {?=} useCache Whether we should use cache or not.
     * @return {?}
     */
    get(route, id, useCache) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                if (id) {
                    route += '/' + id;
                }
                /** @type {?} */
                const getFromCache = new Promise((/**
                 * @param {?} resolve
                 * @param {?} reject
                 * @return {?}
                 */
                (resolve, reject) => {
                    if (useCache && this.cache[route]) {
                        setTimeout((/**
                         * @return {?}
                         */
                        () => {
                            resolve(this.cache[route]);
                        }), 3000);
                    }
                }));
                /** @type {?} */
                const getFromKeps = this.http.get(apiRoute + route).toPromise()
                    .then((/**
                 * @param {?} result
                 * @return {?}
                 */
                result => {
                    this.cache[route] = result;
                    return result;
                }));
                return Promise.race([getFromCache, getFromKeps]);
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a POST request to a route of Keps server.
     * @template T
     * @param {?} route Route to post to.
     * @param {?} data Data to be passed on to the route.
     * @return {?}
     */
    post(route, data) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                data = this.normalizeData(data);
                return this.http.post(apiRoute + route, data).toPromise();
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a PUT request to a route of Keps server.
     * @template T
     * @param {?} route Route to put to.
     * @param {?} data Data to be passed on to the route.
     * @return {?}
     */
    put(route, data) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                data = this.normalizeData(data);
                return this.http.put(apiRoute + route, data).toPromise();
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a DELETE request to a route of Keps server.
     * @template T
     * @param {?} type Type of the object to delete (can also be just a route).
     * @param {?} data An ID string or a Keps object to be deleted.
     * @return {?}
     */
    delete(type, data) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                /** @type {?} */
                let route = apiRoute;
                if (typeof data === 'string') {
                    route += type + '/' + data;
                }
                else if (typeof data === 'object' && data._id) {
                    route += type + '/' + data._id;
                }
                else {
                    route += type;
                }
                return this.http.delete(route).toPromise();
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a request depending on the command to Keps server.
     * @template T
     * @param {?} command A Keps command (e.g. 'put.users.update').
     * @param {?} data Data to be passed on to the server.
     * @return {?}
     */
    call(command, data) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                // Parse the command.
                const [method, type, route] = command.split('.');
                if (!method || !type || !route) {
                    throw new Error('Invalid Keps command.');
                }
                // Build URL based on data.
                /** @type {?} */
                let url = apiRoute + type + 's/';
                if (data instanceof FormData) {
                    /** @type {?} */
                    const id = data.get(type);
                    if (id) {
                        data.delete(type);
                        url += id + '/' + route;
                    }
                    else {
                        url += route;
                    }
                }
                else if (typeof data === 'object') {
                    if (data[type]) {
                        /** @type {?} */
                        const typeData = data[type];
                        if (typeof typeData === 'object') {
                            url += typeData._id + '/' + route;
                            delete data[type];
                        }
                        else if (typeof typeData === 'string') {
                            url += typeData + '/' + route;
                            delete data[type];
                        }
                        else {
                            url += route;
                        }
                    }
                    else {
                        url += route;
                    }
                }
                else {
                    url += route;
                }
                // Truncate URL for specific routes.
                if (['query', 'read', 'delete', 'update', 'create'].includes(route)) {
                    url = url.substr(0, url.length - route.length - 1);
                }
                // Send out requests.
                if (method === 'get' || method === 'delete') {
                    url += '?' + this.serializeData(data);
                    return this.http.request(method.toUpperCase(), url).toPromise();
                }
                else if (method === 'post' || method === 'put') {
                    return this.http.request(method.toUpperCase(), url, { body: data }).toPromise();
                }
                else {
                    return this.http.get(url).toPromise();
                }
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Sends a GraphQL query request to the Keps server and returns the result back.
     *
     * Will throw an error if the result contains any error.
     * @param {?} query A GraphQL query.
     * @return {?}
     */
    graphql(query) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!DataService} */ function* () {
            try {
                // Clean up whitespaces in query.
                /** @type {?} */
                const cleanQuery = query.replace(/([^"]+)|("[^"]+")/g, (/**
                 * @param {?} $0
                 * @param {?} $1
                 * @param {?} $2
                 * @return {?}
                 */
                ($0, $1, $2) => {
                    if ($1) {
                        return $1.replace(/\s/g, '');
                    }
                    else {
                        return $2;
                    }
                }));
                // Send a GraphQL request and throw error if any.
                /** @type {?} */
                const result = yield this.http.get(apiRoute + `graphqls?q={${cleanQuery}}`).toPromise();
                if (result.errors) {
                    throw new Error(result.errors[0]);
                }
                return result;
            }
            catch (err) {
                throw this.errorService.normalizeError(err);
            }
        });
    }
    /**
     * Clears all stored cache.
     * @return {?}
     */
    clearCache() {
        this.cache = {};
    }
    /**
     * Converts data to FormData if it contains a File or Blob.
     * @private
     * @param {?} data Data to be normalized.
     * @return {?}
     */
    normalizeData(data) {
        if (!(data instanceof FormData)) {
            // Check if data contains a File or Blob.
            if (Object.values(data).some((/**
             * @param {?} value
             * @return {?}
             */
            value => value instanceof File || value instanceof Blob))) {
                // Convert to FormData and return it.
                /** @type {?} */
                const formData = new FormData();
                for (const [key, value] of Object.entries(data)) {
                    formData.set(key, value);
                }
                return formData;
            }
        }
        return data;
    }
    /**
     * Serializes data and returns it as a query string.
     * @private
     * @param {?} data Data to be serialized.
     * @param {?=} prefix Prefix for the passed data.
     * @return {?}
     */
    serializeData(data, prefix) {
        /** @type {?} */
        const str = [];
        for (const p in data) {
            /** @type {?} */
            const k = prefix ? prefix + '[' + p + ']' : p;
            /** @type {?} */
            const v = data[p];
            str.push(typeof v === 'object' ?
                this.serializeData(v, k) :
                encodeURIComponent(k) + '=' + encodeURIComponent(v));
        }
        return str.join('&');
    }
}
DataService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
DataService.ctorParameters = () => [
    { type: HttpClient },
    { type: ErrorService }
];
/** @nocollapse */ DataService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DataService_Factory() { return new DataService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.ErrorService)); }, token: DataService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    DataService.prototype.cache;
    /**
     * @type {?}
     * @private
     */
    DataService.prototype.http;
    /**
     * @type {?}
     * @private
     */
    DataService.prototype.errorService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlL2RhdGEuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sV0FBVyxDQUFDOzs7Ozs7O0FBU3JDLE1BQU0sT0FBTyxXQUFXOzs7Ozs7SUFNdEIsWUFDVSxJQUFnQixFQUNoQixZQUEwQjtRQUQxQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBUDVCLFVBQUssR0FBd0IsRUFBRSxDQUFDO0lBUXBDLENBQUM7Ozs7Ozs7OztJQVFDLEdBQUcsQ0FBVSxLQUFhLEVBQUUsRUFBVyxFQUFFLFFBQWtCOztZQUMvRCxJQUFJO2dCQUNGLElBQUksRUFBRSxFQUFFO29CQUNOLEtBQUssSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO2lCQUNuQjs7c0JBRUssWUFBWSxHQUFlLElBQUksT0FBTzs7Ozs7Z0JBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7b0JBQy9ELElBQUksUUFBUSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQ2pDLFVBQVU7Ozt3QkFBQyxHQUFHLEVBQUU7NEJBQ2QsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzt3QkFDN0IsQ0FBQyxHQUFFLElBQUksQ0FBQyxDQUFDO3FCQUNWO2dCQUNILENBQUMsRUFBQzs7c0JBRUksV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFJLFFBQVEsR0FBRyxLQUFLLENBQUMsQ0FBQyxTQUFTLEVBQUU7cUJBQ2pFLElBQUk7Ozs7Z0JBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxNQUFNLENBQUM7b0JBQzNCLE9BQU8sTUFBTSxDQUFDO2dCQUNoQixDQUFDLEVBQUM7Z0JBRUYsT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsWUFBWSxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUM7YUFDbEQ7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDWixNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQzdDO1FBQ0gsQ0FBQztLQUFBOzs7Ozs7OztJQU9LLElBQUksQ0FBVSxLQUFhLEVBQUUsSUFBUzs7WUFDMUMsSUFBSTtnQkFDRixJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDaEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBSSxRQUFRLEdBQUcsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQzlEO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1osTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUM3QztRQUNILENBQUM7S0FBQTs7Ozs7Ozs7SUFPSyxHQUFHLENBQVUsS0FBYSxFQUFFLElBQVM7O1lBQ3pDLElBQUk7Z0JBQ0YsSUFBSSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2hDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUksUUFBUSxHQUFHLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUM3RDtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNaLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDN0M7UUFDSCxDQUFDO0tBQUE7Ozs7Ozs7O0lBT0ssTUFBTSxDQUFXLElBQVksRUFBRSxJQUF5Qjs7WUFDNUQsSUFBSTs7b0JBQ0UsS0FBSyxHQUFHLFFBQVE7Z0JBQ3BCLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFFO29CQUM1QixLQUFLLElBQUksSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUM7aUJBQzVCO3FCQUFNLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUU7b0JBQy9DLEtBQUssSUFBSSxJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7aUJBQ2hDO3FCQUFNO29CQUNMLEtBQUssSUFBSSxJQUFJLENBQUM7aUJBQ2Y7Z0JBQ0QsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBSSxLQUFLLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUMvQztZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNaLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDN0M7UUFDSCxDQUFDO0tBQUE7Ozs7Ozs7O0lBT0ssSUFBSSxDQUFVLE9BQWUsRUFBRSxJQUFvQjs7WUFDdkQsSUFBSTs7c0JBRUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEtBQUssQ0FBQyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO2dCQUNoRCxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUM5QixNQUFNLElBQUksS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUM7aUJBQzFDOzs7b0JBR0csR0FBRyxHQUFHLFFBQVEsR0FBRyxJQUFJLEdBQUcsSUFBSTtnQkFDaEMsSUFBSSxJQUFJLFlBQVksUUFBUSxFQUFFOzswQkFDdEIsRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDO29CQUN6QixJQUFJLEVBQUUsRUFBRTt3QkFDTixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNsQixHQUFHLElBQUksRUFBRSxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUM7cUJBQ3pCO3lCQUFNO3dCQUNMLEdBQUcsSUFBSSxLQUFLLENBQUM7cUJBQ2Q7aUJBQ0Y7cUJBQU0sSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7b0JBQ25DLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFOzs4QkFDUixRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQzt3QkFDM0IsSUFBSSxPQUFPLFFBQVEsS0FBSyxRQUFRLEVBQUU7NEJBQ2hDLEdBQUcsSUFBSSxRQUFRLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUM7NEJBQ2xDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUNuQjs2QkFBTSxJQUFJLE9BQU8sUUFBUSxLQUFLLFFBQVEsRUFBRTs0QkFDdkMsR0FBRyxJQUFJLFFBQVEsR0FBRyxHQUFHLEdBQUcsS0FBSyxDQUFDOzRCQUM5QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDbkI7NkJBQU07NEJBQ0wsR0FBRyxJQUFJLEtBQUssQ0FBQzt5QkFDZDtxQkFDRjt5QkFBTTt3QkFDTCxHQUFHLElBQUksS0FBSyxDQUFDO3FCQUNkO2lCQUNGO3FCQUFNO29CQUNMLEdBQUcsSUFBSSxLQUFLLENBQUM7aUJBQ2Q7Z0JBRUQsb0NBQW9DO2dCQUNwQyxJQUFJLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDbkUsR0FBRyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDcEQ7Z0JBRUQscUJBQXFCO2dCQUNyQixJQUFJLE1BQU0sS0FBSyxLQUFLLElBQUksTUFBTSxLQUFLLFFBQVEsRUFBRTtvQkFDM0MsR0FBRyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztpQkFDcEU7cUJBQU0sSUFBSSxNQUFNLEtBQUssTUFBTSxJQUFJLE1BQU0sS0FBSyxLQUFLLEVBQUU7b0JBQ2hELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUksTUFBTSxDQUFDLFdBQVcsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO2lCQUNwRjtxQkFBTTtvQkFDTCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFJLEdBQUcsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO2lCQUMxQzthQUNGO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1osTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUM3QztRQUNILENBQUM7S0FBQTs7Ozs7Ozs7SUFRSyxPQUFPLENBQUMsS0FBYTs7WUFDekIsSUFBSTs7O3NCQUVJLFVBQVUsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLG9CQUFvQjs7Ozs7O2dCQUFFLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRTtvQkFDcEUsSUFBSSxFQUFFLEVBQUU7d0JBQ04sT0FBTyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztxQkFDOUI7eUJBQU07d0JBQ0wsT0FBTyxFQUFFLENBQUM7cUJBQ1g7Z0JBQ0gsQ0FBQyxFQUFDOzs7c0JBR0ksTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQ2hDLFFBQVEsR0FBRyxlQUFlLFVBQVUsR0FBRyxDQUN4QyxDQUFDLFNBQVMsRUFBRTtnQkFDYixJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0JBQ2pCLE1BQU0sSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNuQztnQkFDRCxPQUFPLE1BQU0sQ0FBQzthQUNmO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1osTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUM3QztRQUNILENBQUM7S0FBQTs7Ozs7SUFLRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDbEIsQ0FBQzs7Ozs7OztJQU1PLGFBQWEsQ0FBQyxJQUFZO1FBQ2hDLElBQUksQ0FBQyxDQUFDLElBQUksWUFBWSxRQUFRLENBQUMsRUFBRTtZQUMvQix5Q0FBeUM7WUFDekMsSUFDRSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUk7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssWUFBWSxJQUFJLElBQUksS0FBSyxZQUFZLElBQUksRUFBQyxFQUNqRjs7O3NCQUVNLFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRTtnQkFDL0IsS0FBSyxNQUFNLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQy9DLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO2lCQUMxQjtnQkFDRCxPQUFPLFFBQVEsQ0FBQzthQUNqQjtTQUNGO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7Ozs7OztJQU9PLGFBQWEsQ0FBQyxJQUFZLEVBQUUsTUFBZTs7Y0FDM0MsR0FBRyxHQUFHLEVBQUU7UUFDZCxLQUFLLE1BQU0sQ0FBQyxJQUFJLElBQUksRUFBRTs7a0JBQ2QsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDOztrQkFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMxRCxHQUFHLENBQUMsSUFBSSxDQUNOLE9BQU8sQ0FBQyxLQUFLLFFBQVEsQ0FBQyxDQUFDO2dCQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxQixrQkFBa0IsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQ3BELENBQUM7U0FDSDtRQUNELE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN2QixDQUFDOzs7WUF0T0YsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBVlEsVUFBVTtZQUNWLFlBQVk7Ozs7Ozs7O0lBV25CLDRCQUF3Qzs7Ozs7SUFNdEMsMkJBQXdCOzs7OztJQUN4QixtQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEVycm9yU2VydmljZSB9IGZyb20gJy4vZXJyb3Iuc2VydmljZSc7XHJcbmltcG9ydCB7IGFwaVJvdXRlIH0gZnJvbSAnLi4vY29uZmlnJztcclxuaW1wb3J0IHsgS2Vwc09iamVjdCwgR3JhcGhxbFJlc3VsdCB9IGZyb20gJy4uL2ludGVyZmFjZSc7XHJcblxyXG4vKipcclxuICogQSBzZXJ2aWNlIHRvIHNlbmQgcmVxdWVzdHMgdG8gS2VwcyBzZXJ2ZXIuXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEYXRhU2VydmljZSB7XHJcbiAgcHJpdmF0ZSBjYWNoZTogUmVjb3JkPHN0cmluZywgYW55PiA9IHt9O1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgaHR0cDogSHR0cENsaWVudCxcclxuICAgIHByaXZhdGUgZXJyb3JTZXJ2aWNlOiBFcnJvclNlcnZpY2VcclxuICApIHsgfVxyXG5cclxuICAvKipcclxuICAgKiBTZW5kcyBhIEdFVCByZXF1ZXN0IHRvIGEgcm91dGUgb2YgS2VwcyBzZXJ2ZXIuXHJcbiAgICogQHBhcmFtIHJvdXRlIFJvdXRlIHRvIGdldCBmcm9tLlxyXG4gICAqIEBwYXJhbSBpZCBJRCBvZiBhbiBvYmplY3QuXHJcbiAgICogQHBhcmFtIHVzZUNhY2hlIFdoZXRoZXIgd2Ugc2hvdWxkIHVzZSBjYWNoZSBvciBub3QuXHJcbiAgICovXHJcbiAgYXN5bmMgZ2V0PFQgPSBhbnk+KHJvdXRlOiBzdHJpbmcsIGlkPzogc3RyaW5nLCB1c2VDYWNoZT86IGJvb2xlYW4pOiBQcm9taXNlPFQ+IHtcclxuICAgIHRyeSB7XHJcbiAgICAgIGlmIChpZCkge1xyXG4gICAgICAgIHJvdXRlICs9ICcvJyArIGlkO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBjb25zdCBnZXRGcm9tQ2FjaGU6IFByb21pc2U8VD4gPSBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgaWYgKHVzZUNhY2hlICYmIHRoaXMuY2FjaGVbcm91dGVdKSB7XHJcbiAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgcmVzb2x2ZSh0aGlzLmNhY2hlW3JvdXRlXSk7XHJcbiAgICAgICAgICB9LCAzMDAwKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgY29uc3QgZ2V0RnJvbUtlcHMgPSB0aGlzLmh0dHAuZ2V0PFQ+KGFwaVJvdXRlICsgcm91dGUpLnRvUHJvbWlzZSgpXHJcbiAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgdGhpcy5jYWNoZVtyb3V0ZV0gPSByZXN1bHQ7XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICByZXR1cm4gUHJvbWlzZS5yYWNlKFtnZXRGcm9tQ2FjaGUsIGdldEZyb21LZXBzXSk7XHJcbiAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgdGhyb3cgdGhpcy5lcnJvclNlcnZpY2Uubm9ybWFsaXplRXJyb3IoZXJyKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNlbmRzIGEgUE9TVCByZXF1ZXN0IHRvIGEgcm91dGUgb2YgS2VwcyBzZXJ2ZXIuXHJcbiAgICogQHBhcmFtIHJvdXRlIFJvdXRlIHRvIHBvc3QgdG8uXHJcbiAgICogQHBhcmFtIGRhdGEgRGF0YSB0byBiZSBwYXNzZWQgb24gdG8gdGhlIHJvdXRlLlxyXG4gICAqL1xyXG4gIGFzeW5jIHBvc3Q8VCA9IGFueT4ocm91dGU6IHN0cmluZywgZGF0YTogYW55KTogUHJvbWlzZTxUPiB7XHJcbiAgICB0cnkge1xyXG4gICAgICBkYXRhID0gdGhpcy5ub3JtYWxpemVEYXRhKGRhdGEpO1xyXG4gICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Q8VD4oYXBpUm91dGUgKyByb3V0ZSwgZGF0YSkudG9Qcm9taXNlKCk7XHJcbiAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgdGhyb3cgdGhpcy5lcnJvclNlcnZpY2Uubm9ybWFsaXplRXJyb3IoZXJyKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNlbmRzIGEgUFVUIHJlcXVlc3QgdG8gYSByb3V0ZSBvZiBLZXBzIHNlcnZlci5cclxuICAgKiBAcGFyYW0gcm91dGUgUm91dGUgdG8gcHV0IHRvLlxyXG4gICAqIEBwYXJhbSBkYXRhIERhdGEgdG8gYmUgcGFzc2VkIG9uIHRvIHRoZSByb3V0ZS5cclxuICAgKi9cclxuICBhc3luYyBwdXQ8VCA9IGFueT4ocm91dGU6IHN0cmluZywgZGF0YTogYW55KTogUHJvbWlzZTxUPiB7XHJcbiAgICB0cnkge1xyXG4gICAgICBkYXRhID0gdGhpcy5ub3JtYWxpemVEYXRhKGRhdGEpO1xyXG4gICAgICByZXR1cm4gdGhpcy5odHRwLnB1dDxUPihhcGlSb3V0ZSArIHJvdXRlLCBkYXRhKS50b1Byb21pc2UoKTtcclxuICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICB0aHJvdyB0aGlzLmVycm9yU2VydmljZS5ub3JtYWxpemVFcnJvcihlcnIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2VuZHMgYSBERUxFVEUgcmVxdWVzdCB0byBhIHJvdXRlIG9mIEtlcHMgc2VydmVyLlxyXG4gICAqIEBwYXJhbSB0eXBlIFR5cGUgb2YgdGhlIG9iamVjdCB0byBkZWxldGUgKGNhbiBhbHNvIGJlIGp1c3QgYSByb3V0ZSkuXHJcbiAgICogQHBhcmFtIGRhdGEgQW4gSUQgc3RyaW5nIG9yIGEgS2VwcyBvYmplY3QgdG8gYmUgZGVsZXRlZC5cclxuICAgKi9cclxuICBhc3luYyBkZWxldGU8VCA9IHZvaWQ+KHR5cGU6IHN0cmluZywgZGF0YTogc3RyaW5nIHwgS2Vwc09iamVjdCk6IFByb21pc2U8VD4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgbGV0IHJvdXRlID0gYXBpUm91dGU7XHJcbiAgICAgIGlmICh0eXBlb2YgZGF0YSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICByb3V0ZSArPSB0eXBlICsgJy8nICsgZGF0YTtcclxuICAgICAgfSBlbHNlIGlmICh0eXBlb2YgZGF0YSA9PT0gJ29iamVjdCcgJiYgZGF0YS5faWQpIHtcclxuICAgICAgICByb3V0ZSArPSB0eXBlICsgJy8nICsgZGF0YS5faWQ7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcm91dGUgKz0gdHlwZTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gdGhpcy5odHRwLmRlbGV0ZTxUPihyb3V0ZSkudG9Qcm9taXNlKCk7XHJcbiAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgdGhyb3cgdGhpcy5lcnJvclNlcnZpY2Uubm9ybWFsaXplRXJyb3IoZXJyKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNlbmRzIGEgcmVxdWVzdCBkZXBlbmRpbmcgb24gdGhlIGNvbW1hbmQgdG8gS2VwcyBzZXJ2ZXIuXHJcbiAgICogQHBhcmFtIGNvbW1hbmQgQSBLZXBzIGNvbW1hbmQgKGUuZy4gJ3B1dC51c2Vycy51cGRhdGUnKS5cclxuICAgKiBAcGFyYW0gZGF0YSBEYXRhIHRvIGJlIHBhc3NlZCBvbiB0byB0aGUgc2VydmVyLlxyXG4gICAqL1xyXG4gIGFzeW5jIGNhbGw8VCA9IGFueT4oY29tbWFuZDogc3RyaW5nLCBkYXRhOiBGb3JtRGF0YSB8IGFueSk6IFByb21pc2U8VD4ge1xyXG4gICAgdHJ5IHtcclxuICAgICAgLy8gUGFyc2UgdGhlIGNvbW1hbmQuXHJcbiAgICAgIGNvbnN0IFttZXRob2QsIHR5cGUsIHJvdXRlXSA9IGNvbW1hbmQuc3BsaXQoJy4nKTtcclxuICAgICAgaWYgKCFtZXRob2QgfHwgIXR5cGUgfHwgIXJvdXRlKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIEtlcHMgY29tbWFuZC4nKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gQnVpbGQgVVJMIGJhc2VkIG9uIGRhdGEuXHJcbiAgICAgIGxldCB1cmwgPSBhcGlSb3V0ZSArIHR5cGUgKyAncy8nO1xyXG4gICAgICBpZiAoZGF0YSBpbnN0YW5jZW9mIEZvcm1EYXRhKSB7XHJcbiAgICAgICAgY29uc3QgaWQgPSBkYXRhLmdldCh0eXBlKTtcclxuICAgICAgICBpZiAoaWQpIHtcclxuICAgICAgICAgIGRhdGEuZGVsZXRlKHR5cGUpO1xyXG4gICAgICAgICAgdXJsICs9IGlkICsgJy8nICsgcm91dGU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHVybCArPSByb3V0ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGRhdGEgPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgaWYgKGRhdGFbdHlwZV0pIHtcclxuICAgICAgICAgIGNvbnN0IHR5cGVEYXRhID0gZGF0YVt0eXBlXTtcclxuICAgICAgICAgIGlmICh0eXBlb2YgdHlwZURhdGEgPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgIHVybCArPSB0eXBlRGF0YS5faWQgKyAnLycgKyByb3V0ZTtcclxuICAgICAgICAgICAgZGVsZXRlIGRhdGFbdHlwZV07XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiB0eXBlRGF0YSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgdXJsICs9IHR5cGVEYXRhICsgJy8nICsgcm91dGU7XHJcbiAgICAgICAgICAgIGRlbGV0ZSBkYXRhW3R5cGVdO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdXJsICs9IHJvdXRlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB1cmwgKz0gcm91dGU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHVybCArPSByb3V0ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gVHJ1bmNhdGUgVVJMIGZvciBzcGVjaWZpYyByb3V0ZXMuXHJcbiAgICAgIGlmIChbJ3F1ZXJ5JywgJ3JlYWQnLCAnZGVsZXRlJywgJ3VwZGF0ZScsICdjcmVhdGUnXS5pbmNsdWRlcyhyb3V0ZSkpIHtcclxuICAgICAgICB1cmwgPSB1cmwuc3Vic3RyKDAsIHVybC5sZW5ndGggLSByb3V0ZS5sZW5ndGggLSAxKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gU2VuZCBvdXQgcmVxdWVzdHMuXHJcbiAgICAgIGlmIChtZXRob2QgPT09ICdnZXQnIHx8IG1ldGhvZCA9PT0gJ2RlbGV0ZScpIHtcclxuICAgICAgICB1cmwgKz0gJz8nICsgdGhpcy5zZXJpYWxpemVEYXRhKGRhdGEpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucmVxdWVzdDxUPihtZXRob2QudG9VcHBlckNhc2UoKSwgdXJsKS50b1Byb21pc2UoKTtcclxuICAgICAgfSBlbHNlIGlmIChtZXRob2QgPT09ICdwb3N0JyB8fCBtZXRob2QgPT09ICdwdXQnKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5yZXF1ZXN0PFQ+KG1ldGhvZC50b1VwcGVyQ2FzZSgpLCB1cmwsIHsgYm9keTogZGF0YSB9KS50b1Byb21pc2UoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldDxUPih1cmwpLnRvUHJvbWlzZSgpO1xyXG4gICAgICB9XHJcbiAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgdGhyb3cgdGhpcy5lcnJvclNlcnZpY2Uubm9ybWFsaXplRXJyb3IoZXJyKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNlbmRzIGEgR3JhcGhRTCBxdWVyeSByZXF1ZXN0IHRvIHRoZSBLZXBzIHNlcnZlciBhbmQgcmV0dXJucyB0aGUgcmVzdWx0IGJhY2suXHJcbiAgICpcclxuICAgKiBXaWxsIHRocm93IGFuIGVycm9yIGlmIHRoZSByZXN1bHQgY29udGFpbnMgYW55IGVycm9yLlxyXG4gICAqIEBwYXJhbSBxdWVyeSBBIEdyYXBoUUwgcXVlcnkuXHJcbiAgICovXHJcbiAgYXN5bmMgZ3JhcGhxbChxdWVyeTogc3RyaW5nKTogUHJvbWlzZTxHcmFwaHFsUmVzdWx0PiB7XHJcbiAgICB0cnkge1xyXG4gICAgICAvLyBDbGVhbiB1cCB3aGl0ZXNwYWNlcyBpbiBxdWVyeS5cclxuICAgICAgY29uc3QgY2xlYW5RdWVyeSA9IHF1ZXJ5LnJlcGxhY2UoLyhbXlwiXSspfChcIlteXCJdK1wiKS9nLCAoJDAsICQxLCAkMikgPT4ge1xyXG4gICAgICAgIGlmICgkMSkge1xyXG4gICAgICAgICAgcmV0dXJuICQxLnJlcGxhY2UoL1xccy9nLCAnJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJldHVybiAkMjtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgLy8gU2VuZCBhIEdyYXBoUUwgcmVxdWVzdCBhbmQgdGhyb3cgZXJyb3IgaWYgYW55LlxyXG4gICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCB0aGlzLmh0dHAuZ2V0PEdyYXBocWxSZXN1bHQ+KFxyXG4gICAgICAgIGFwaVJvdXRlICsgYGdyYXBocWxzP3E9eyR7Y2xlYW5RdWVyeX19YFxyXG4gICAgICApLnRvUHJvbWlzZSgpO1xyXG4gICAgICBpZiAocmVzdWx0LmVycm9ycykge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihyZXN1bHQuZXJyb3JzWzBdKTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgIHRocm93IHRoaXMuZXJyb3JTZXJ2aWNlLm5vcm1hbGl6ZUVycm9yKGVycik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBDbGVhcnMgYWxsIHN0b3JlZCBjYWNoZS5cclxuICAgKi9cclxuICBjbGVhckNhY2hlKCkge1xyXG4gICAgdGhpcy5jYWNoZSA9IHt9O1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ29udmVydHMgZGF0YSB0byBGb3JtRGF0YSBpZiBpdCBjb250YWlucyBhIEZpbGUgb3IgQmxvYi5cclxuICAgKiBAcGFyYW0gZGF0YSBEYXRhIHRvIGJlIG5vcm1hbGl6ZWQuXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBub3JtYWxpemVEYXRhKGRhdGE6IG9iamVjdCk6IG9iamVjdCB8IEZvcm1EYXRhIHtcclxuICAgIGlmICghKGRhdGEgaW5zdGFuY2VvZiBGb3JtRGF0YSkpIHtcclxuICAgICAgLy8gQ2hlY2sgaWYgZGF0YSBjb250YWlucyBhIEZpbGUgb3IgQmxvYi5cclxuICAgICAgaWYgKFxyXG4gICAgICAgIE9iamVjdC52YWx1ZXMoZGF0YSkuc29tZSh2YWx1ZSA9PiB2YWx1ZSBpbnN0YW5jZW9mIEZpbGUgfHwgdmFsdWUgaW5zdGFuY2VvZiBCbG9iKVxyXG4gICAgICApIHtcclxuICAgICAgICAvLyBDb252ZXJ0IHRvIEZvcm1EYXRhIGFuZCByZXR1cm4gaXQuXHJcbiAgICAgICAgY29uc3QgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcclxuICAgICAgICBmb3IgKGNvbnN0IFtrZXksIHZhbHVlXSBvZiBPYmplY3QuZW50cmllcyhkYXRhKSkge1xyXG4gICAgICAgICAgZm9ybURhdGEuc2V0KGtleSwgdmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZm9ybURhdGE7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBkYXRhO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2VyaWFsaXplcyBkYXRhIGFuZCByZXR1cm5zIGl0IGFzIGEgcXVlcnkgc3RyaW5nLlxyXG4gICAqIEBwYXJhbSBkYXRhIERhdGEgdG8gYmUgc2VyaWFsaXplZC5cclxuICAgKiBAcGFyYW0gcHJlZml4IFByZWZpeCBmb3IgdGhlIHBhc3NlZCBkYXRhLlxyXG4gICAqL1xyXG4gIHByaXZhdGUgc2VyaWFsaXplRGF0YShkYXRhOiBvYmplY3QsIHByZWZpeD86IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBzdHIgPSBbXTtcclxuICAgIGZvciAoY29uc3QgcCBpbiBkYXRhKSB7XHJcbiAgICAgIGNvbnN0IGsgPSBwcmVmaXggPyBwcmVmaXggKyAnWycgKyBwICsgJ10nIDogcCwgdiA9IGRhdGFbcF07XHJcbiAgICAgIHN0ci5wdXNoKFxyXG4gICAgICAgIHR5cGVvZiB2ID09PSAnb2JqZWN0JyA/XHJcbiAgICAgICAgdGhpcy5zZXJpYWxpemVEYXRhKHYsIGspIDpcclxuICAgICAgICBlbmNvZGVVUklDb21wb25lbnQoaykgKyAnPScgKyBlbmNvZGVVUklDb21wb25lbnQodilcclxuICAgICAgKTtcclxuICAgIH1cclxuICAgIHJldHVybiBzdHIuam9pbignJicpO1xyXG4gIH1cclxufVxyXG4iXX0=