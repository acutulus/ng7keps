/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DialogComponent } from '../component/dialog';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
import * as i2 from "@angular/material/snack-bar";
/**
 * A service to show custom dialogs and toasts.
 */
export class PopupService {
    /**
     * \@internal Do not use!
     * @param {?} dialog
     * @param {?} toast
     */
    constructor(dialog, toast) {
        this.dialog = dialog;
        this.toast = toast;
    }
    /**
     * Shows a custom dialog.
     *
     * @param {?} data
     * Data to be shown on the dialog.
     * See {\@link DialogData}.
     *
     * @param {?=} options
     * Additional options for the dialog.
     * See [MatDialogConfig](https://material.angular.io/components/dialog/api#MatDialogConfig).
     *
     * @return {?}
     * The value set on the dialog buttons.
     * If the user clicks the 'Close' button or outside the dialog, it will return undefined.
     */
    showDialog(data, options = {}) {
        options.data = data;
        return this.dialog
            .open(DialogComponent, options)
            .afterClosed()
            .toPromise();
    }
    /**
     * Show a custom toast.
     * @param {?} message Text to be shown on the toast.
     * @param {?=} duration How long the toast should be shown.
     * @return {?}
     */
    showToast(message, duration = 3000) {
        this.toast.open(message, null, { duration });
    }
}
PopupService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PopupService.ctorParameters = () => [
    { type: MatDialog },
    { type: MatSnackBar }
];
/** @nocollapse */ PopupService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PopupService_Factory() { return new PopupService(i0.ɵɵinject(i1.MatDialog), i0.ɵɵinject(i2.MatSnackBar)); }, token: PopupService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    PopupService.prototype.dialog;
    /**
     * @type {?}
     * @private
     */
    PopupService.prototype.toast;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9wdXAuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvc2VydmljZS9wb3B1cC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFDTCxTQUFTLEVBRVQsV0FBVyxFQUNaLE1BQU0sbUJBQW1CLENBQUM7QUFDM0IsT0FBTyxFQUFFLGVBQWUsRUFBYyxNQUFNLHFCQUFxQixDQUFDOzs7Ozs7O0FBUWxFLE1BQU0sT0FBTyxZQUFZOzs7Ozs7SUFLdkIsWUFDVSxNQUFpQixFQUNqQixLQUFrQjtRQURsQixXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2pCLFVBQUssR0FBTCxLQUFLLENBQWE7SUFDeEIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7OztJQWlCTCxVQUFVLENBQUMsSUFBZ0IsRUFBRSxVQUEyQixFQUFFO1FBQ3hELE9BQU8sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBRXBCLE9BQU8sSUFBSSxDQUFDLE1BQU07YUFDakIsSUFBSSxDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUM7YUFDOUIsV0FBVyxFQUFFO2FBQ2IsU0FBUyxFQUFFLENBQUM7SUFDZixDQUFDOzs7Ozs7O0lBT0QsU0FBUyxDQUFDLE9BQWUsRUFBRSxRQUFRLEdBQUcsSUFBSTtRQUN4QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7WUE1Q0YsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBWEMsU0FBUztZQUVULFdBQVc7Ozs7Ozs7O0lBZ0JULDhCQUF5Qjs7Ozs7SUFDekIsNkJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gIE1hdERpYWxvZyxcclxuICBNYXREaWFsb2dDb25maWcsXHJcbiAgTWF0U25hY2tCYXJcclxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IERpYWxvZ0NvbXBvbmVudCwgRGlhbG9nRGF0YSB9IGZyb20gJy4uL2NvbXBvbmVudC9kaWFsb2cnO1xyXG5cclxuLyoqXHJcbiAqIEEgc2VydmljZSB0byBzaG93IGN1c3RvbSBkaWFsb2dzIGFuZCB0b2FzdHMuXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQb3B1cFNlcnZpY2Uge1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICBwcml2YXRlIHRvYXN0OiBNYXRTbmFja0JhclxyXG4gICkgeyB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNob3dzIGEgY3VzdG9tIGRpYWxvZy5cclxuICAgKlxyXG4gICAqIEBwYXJhbSBkYXRhXHJcbiAgICogRGF0YSB0byBiZSBzaG93biBvbiB0aGUgZGlhbG9nLlxyXG4gICAqIFNlZSB7QGxpbmsgRGlhbG9nRGF0YX0uXHJcbiAgICpcclxuICAgKiBAcGFyYW0gb3B0aW9uc1xyXG4gICAqIEFkZGl0aW9uYWwgb3B0aW9ucyBmb3IgdGhlIGRpYWxvZy5cclxuICAgKiBTZWUgW01hdERpYWxvZ0NvbmZpZ10oaHR0cHM6Ly9tYXRlcmlhbC5hbmd1bGFyLmlvL2NvbXBvbmVudHMvZGlhbG9nL2FwaSNNYXREaWFsb2dDb25maWcpLlxyXG4gICAqXHJcbiAgICogQHJldHVybnNcclxuICAgKiBUaGUgdmFsdWUgc2V0IG9uIHRoZSBkaWFsb2cgYnV0dG9ucy5cclxuICAgKiBJZiB0aGUgdXNlciBjbGlja3MgdGhlICdDbG9zZScgYnV0dG9uIG9yIG91dHNpZGUgdGhlIGRpYWxvZywgaXQgd2lsbCByZXR1cm4gdW5kZWZpbmVkLlxyXG4gICAqL1xyXG4gIHNob3dEaWFsb2coZGF0YTogRGlhbG9nRGF0YSwgb3B0aW9uczogTWF0RGlhbG9nQ29uZmlnID0ge30pIHtcclxuICAgIG9wdGlvbnMuZGF0YSA9IGRhdGE7XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuZGlhbG9nXHJcbiAgICAub3BlbihEaWFsb2dDb21wb25lbnQsIG9wdGlvbnMpXHJcbiAgICAuYWZ0ZXJDbG9zZWQoKVxyXG4gICAgLnRvUHJvbWlzZSgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2hvdyBhIGN1c3RvbSB0b2FzdC5cclxuICAgKiBAcGFyYW0gbWVzc2FnZSBUZXh0IHRvIGJlIHNob3duIG9uIHRoZSB0b2FzdC5cclxuICAgKiBAcGFyYW0gZHVyYXRpb24gSG93IGxvbmcgdGhlIHRvYXN0IHNob3VsZCBiZSBzaG93bi5cclxuICAgKi9cclxuICBzaG93VG9hc3QobWVzc2FnZTogc3RyaW5nLCBkdXJhdGlvbiA9IDMwMDApIHtcclxuICAgIHRoaXMudG9hc3Qub3BlbihtZXNzYWdlLCBudWxsLCB7IGR1cmF0aW9uIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=