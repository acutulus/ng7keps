/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// Import Angular modules.
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// Import other external modules.
import { MaterialModule } from './material-module';
// Import services.
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { DataService } from './service/data.service';
import { ErrorService } from './service/error.service';
import { PopupService } from './service/popup.service';
// Import pipes.
import { AddressPipe } from './pipe/address.pipe';
import { EvalPipe } from './pipe/eval.pipe';
import { KeysPipe } from './pipe/keys.pipe';
import { MomentPipe } from './pipe/moment.pipe';
import { SplitCamelPipe } from './pipe/split-camel.pipe';
// Import components.
import { FileSelectComponent } from './component/file-select/file-select.component';
import { DialogComponent } from './component/dialog/dialog.component';
import { FormComponent } from './component/form/form.component';
import { FieldAddressComponent } from './component/form/field-address/field-address.component';
import { FieldArrayComponent } from './component/form/field-array/field-array.component';
import { FieldBooleanComponent } from './component/form/field-boolean/field-boolean.component';
import { FieldDatetimeComponent } from './component/form/field-datetime/field-datetime.component';
import { ArrayDisplayPipe } from './pipe/array-display.pipe';
import { NumberFormatPipe } from './pipe/number-format.pipe';
import { TableComponent } from './component/table/table/table.component';
import { TableColDirective } from './component/table/table-col.directive';
import { DynamicTableComponent } from './component/table/dynamic-table/dynamic-table.component';
/**
 * Ng7Keps main module.
 */
export class Ng7KepsModule {
    /**
     * Initializes Ng7Keps with the project models.
     * @param {?} models Generated models of the project.
     * @return {?}
     */
    static forRoot(models) {
        return {
            ngModule: Ng7KepsModule,
            providers: [
                AuthService,
                AuthGuard,
                AuthInterceptor,
                DataService,
                ErrorService,
                PopupService,
                {
                    provide: 'MODELS',
                    useValue: models
                }
            ]
        };
    }
}
Ng7KepsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    AddressPipe,
                    ArrayDisplayPipe,
                    EvalPipe,
                    KeysPipe,
                    MomentPipe,
                    NumberFormatPipe,
                    SplitCamelPipe,
                    FileSelectComponent,
                    DialogComponent,
                    FormComponent,
                    FieldAddressComponent,
                    FieldArrayComponent,
                    FieldBooleanComponent,
                    FieldDatetimeComponent,
                    TableComponent,
                    TableColDirective,
                    DynamicTableComponent
                ],
                imports: [
                    BrowserModule,
                    BrowserAnimationsModule,
                    HttpClientModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule
                ],
                exports: [
                    AddressPipe,
                    ArrayDisplayPipe,
                    EvalPipe,
                    KeysPipe,
                    MomentPipe,
                    NumberFormatPipe,
                    SplitCamelPipe,
                    FileSelectComponent,
                    FormComponent,
                    FieldAddressComponent,
                    FieldArrayComponent,
                    FieldBooleanComponent,
                    FieldDatetimeComponent,
                    TableComponent,
                    DynamicTableComponent,
                    TableColDirective
                ],
                entryComponents: [
                    DialogComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmc3a2Vwcy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL25nN2tlcHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQ0EsT0FBTyxFQUFFLFFBQVEsRUFBdUIsTUFBTSxlQUFlLENBQUM7QUFDOUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzFELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7QUFHbEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDOztBQU1uRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDckQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQzs7QUFHdkQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUM1QyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDNUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ2hELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQzs7QUFHekQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDcEYsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNoRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUMvRixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUN6RixPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUMvRixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwwREFBMEQsQ0FBQztBQUNsRyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDekUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDMUUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0seURBQXlELENBQUM7Ozs7QUF1RGhHLE1BQU0sT0FBTyxhQUFhOzs7Ozs7SUFLeEIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFrQjtRQUMvQixPQUFPO1lBQ0wsUUFBUSxFQUFFLGFBQWE7WUFDdkIsU0FBUyxFQUFFO2dCQUNULFdBQVc7Z0JBQ1gsU0FBUztnQkFDVCxlQUFlO2dCQUNmLFdBQVc7Z0JBQ1gsWUFBWTtnQkFDWixZQUFZO2dCQUNaO29CQUNFLE9BQU8sRUFBRSxRQUFRO29CQUNqQixRQUFRLEVBQUUsTUFBTTtpQkFDakI7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDOzs7WUF2RUYsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWixXQUFXO29CQUNYLGdCQUFnQjtvQkFDaEIsUUFBUTtvQkFDUixRQUFRO29CQUNSLFVBQVU7b0JBQ1YsZ0JBQWdCO29CQUNoQixjQUFjO29CQUNkLG1CQUFtQjtvQkFDbkIsZUFBZTtvQkFDZixhQUFhO29CQUNiLHFCQUFxQjtvQkFDckIsbUJBQW1CO29CQUNuQixxQkFBcUI7b0JBQ3JCLHNCQUFzQjtvQkFDdEIsY0FBYztvQkFDZCxpQkFBaUI7b0JBQ2pCLHFCQUFxQjtpQkFDdEI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLGFBQWE7b0JBQ2IsdUJBQXVCO29CQUN2QixnQkFBZ0I7b0JBQ2hCLFdBQVc7b0JBQ1gsbUJBQW1CO29CQUNuQixjQUFjO2lCQUNmO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxXQUFXO29CQUNYLGdCQUFnQjtvQkFDaEIsUUFBUTtvQkFDUixRQUFRO29CQUNSLFVBQVU7b0JBQ1YsZ0JBQWdCO29CQUNoQixjQUFjO29CQUNkLG1CQUFtQjtvQkFDbkIsYUFBYTtvQkFDYixxQkFBcUI7b0JBQ3JCLG1CQUFtQjtvQkFDbkIscUJBQXFCO29CQUNyQixzQkFBc0I7b0JBQ3RCLGNBQWM7b0JBQ2QscUJBQXFCO29CQUNyQixpQkFBaUI7aUJBQ2xCO2dCQUNELGVBQWUsRUFBRTtvQkFDZixlQUFlO2lCQUNoQjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiLy8gSW1wb3J0IEFuZ3VsYXIgbW9kdWxlcy5cbmltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBCcm93c2VyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XG5pbXBvcnQgeyBCcm93c2VyQW5pbWF0aW9uc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXIvYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbi8vIEltcG9ydCBvdGhlciBleHRlcm5hbCBtb2R1bGVzLlxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuL21hdGVyaWFsLW1vZHVsZSc7XG5cbi8vIEltcG9ydCBtb2RlbC5cbmltcG9ydCB7IEtlcHNNb2RlbHMgfSBmcm9tICcuL2ludGVyZmFjZSc7XG5cbi8vIEltcG9ydCBzZXJ2aWNlcy5cbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoL2F1dGguc2VydmljZSc7XG5pbXBvcnQgeyBBdXRoR3VhcmQgfSBmcm9tICcuL2F1dGgvYXV0aC5ndWFyZCc7XG5pbXBvcnQgeyBBdXRoSW50ZXJjZXB0b3IgfSBmcm9tICcuL2F1dGgvYXV0aC5pbnRlcmNlcHRvcic7XG5pbXBvcnQgeyBEYXRhU2VydmljZSB9IGZyb20gJy4vc2VydmljZS9kYXRhLnNlcnZpY2UnO1xuaW1wb3J0IHsgRXJyb3JTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlL2Vycm9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgUG9wdXBTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlL3BvcHVwLnNlcnZpY2UnO1xuXG4vLyBJbXBvcnQgcGlwZXMuXG5pbXBvcnQgeyBBZGRyZXNzUGlwZSB9IGZyb20gJy4vcGlwZS9hZGRyZXNzLnBpcGUnO1xuaW1wb3J0IHsgRXZhbFBpcGUgfSBmcm9tICcuL3BpcGUvZXZhbC5waXBlJztcbmltcG9ydCB7IEtleXNQaXBlIH0gZnJvbSAnLi9waXBlL2tleXMucGlwZSc7XG5pbXBvcnQgeyBNb21lbnRQaXBlIH0gZnJvbSAnLi9waXBlL21vbWVudC5waXBlJztcbmltcG9ydCB7IFNwbGl0Q2FtZWxQaXBlIH0gZnJvbSAnLi9waXBlL3NwbGl0LWNhbWVsLnBpcGUnO1xuXG4vLyBJbXBvcnQgY29tcG9uZW50cy5cbmltcG9ydCB7IEZpbGVTZWxlY3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudC9maWxlLXNlbGVjdC9maWxlLXNlbGVjdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGlhbG9nQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnQvZGlhbG9nL2RpYWxvZy5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50L2Zvcm0vZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmllbGRBZGRyZXNzQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnQvZm9ybS9maWVsZC1hZGRyZXNzL2ZpZWxkLWFkZHJlc3MuY29tcG9uZW50JztcbmltcG9ydCB7IEZpZWxkQXJyYXlDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudC9mb3JtL2ZpZWxkLWFycmF5L2ZpZWxkLWFycmF5LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGaWVsZEJvb2xlYW5Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudC9mb3JtL2ZpZWxkLWJvb2xlYW4vZmllbGQtYm9vbGVhbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgRmllbGREYXRldGltZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50L2Zvcm0vZmllbGQtZGF0ZXRpbWUvZmllbGQtZGF0ZXRpbWUuY29tcG9uZW50JztcbmltcG9ydCB7IEFycmF5RGlzcGxheVBpcGUgfSBmcm9tICcuL3BpcGUvYXJyYXktZGlzcGxheS5waXBlJztcbmltcG9ydCB7IE51bWJlckZvcm1hdFBpcGUgfSBmcm9tICcuL3BpcGUvbnVtYmVyLWZvcm1hdC5waXBlJztcbmltcG9ydCB7IFRhYmxlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnQvdGFibGUvdGFibGUvdGFibGUuY29tcG9uZW50JztcbmltcG9ydCB7IFRhYmxlQ29sRGlyZWN0aXZlIH0gZnJvbSAnLi9jb21wb25lbnQvdGFibGUvdGFibGUtY29sLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBEeW5hbWljVGFibGVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudC90YWJsZS9keW5hbWljLXRhYmxlL2R5bmFtaWMtdGFibGUuY29tcG9uZW50JztcblxuLyoqXG4gKiBOZzdLZXBzIG1haW4gbW9kdWxlLlxuICovXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBBZGRyZXNzUGlwZSxcbiAgICBBcnJheURpc3BsYXlQaXBlLFxuICAgIEV2YWxQaXBlLFxuICAgIEtleXNQaXBlLFxuICAgIE1vbWVudFBpcGUsXG4gICAgTnVtYmVyRm9ybWF0UGlwZSxcbiAgICBTcGxpdENhbWVsUGlwZSxcbiAgICBGaWxlU2VsZWN0Q29tcG9uZW50LFxuICAgIERpYWxvZ0NvbXBvbmVudCxcbiAgICBGb3JtQ29tcG9uZW50LFxuICAgIEZpZWxkQWRkcmVzc0NvbXBvbmVudCxcbiAgICBGaWVsZEFycmF5Q29tcG9uZW50LFxuICAgIEZpZWxkQm9vbGVhbkNvbXBvbmVudCxcbiAgICBGaWVsZERhdGV0aW1lQ29tcG9uZW50LFxuICAgIFRhYmxlQ29tcG9uZW50LFxuICAgIFRhYmxlQ29sRGlyZWN0aXZlLFxuICAgIER5bmFtaWNUYWJsZUNvbXBvbmVudFxuICBdLFxuICBpbXBvcnRzOiBbXG4gICAgQnJvd3Nlck1vZHVsZSxcbiAgICBCcm93c2VyQW5pbWF0aW9uc01vZHVsZSxcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgIEZvcm1zTW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgTWF0ZXJpYWxNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW1xuICAgIEFkZHJlc3NQaXBlLFxuICAgIEFycmF5RGlzcGxheVBpcGUsXG4gICAgRXZhbFBpcGUsXG4gICAgS2V5c1BpcGUsXG4gICAgTW9tZW50UGlwZSxcbiAgICBOdW1iZXJGb3JtYXRQaXBlLFxuICAgIFNwbGl0Q2FtZWxQaXBlLFxuICAgIEZpbGVTZWxlY3RDb21wb25lbnQsXG4gICAgRm9ybUNvbXBvbmVudCxcbiAgICBGaWVsZEFkZHJlc3NDb21wb25lbnQsXG4gICAgRmllbGRBcnJheUNvbXBvbmVudCxcbiAgICBGaWVsZEJvb2xlYW5Db21wb25lbnQsXG4gICAgRmllbGREYXRldGltZUNvbXBvbmVudCxcbiAgICBUYWJsZUNvbXBvbmVudCxcbiAgICBEeW5hbWljVGFibGVDb21wb25lbnQsXG4gICAgVGFibGVDb2xEaXJlY3RpdmVcbiAgXSxcbiAgZW50cnlDb21wb25lbnRzOiBbXG4gICAgRGlhbG9nQ29tcG9uZW50XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgTmc3S2Vwc01vZHVsZSB7XG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyBOZzdLZXBzIHdpdGggdGhlIHByb2plY3QgbW9kZWxzLlxuICAgKiBAcGFyYW0gbW9kZWxzIEdlbmVyYXRlZCBtb2RlbHMgb2YgdGhlIHByb2plY3QuXG4gICAqL1xuICBzdGF0aWMgZm9yUm9vdChtb2RlbHM6IEtlcHNNb2RlbHMpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IE5nN0tlcHNNb2R1bGUsXG4gICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgQXV0aFNlcnZpY2UsXG4gICAgICAgIEF1dGhHdWFyZCxcbiAgICAgICAgQXV0aEludGVyY2VwdG9yLFxuICAgICAgICBEYXRhU2VydmljZSxcbiAgICAgICAgRXJyb3JTZXJ2aWNlLFxuICAgICAgICBQb3B1cFNlcnZpY2UsXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiAnTU9ERUxTJyxcbiAgICAgICAgICB1c2VWYWx1ZTogbW9kZWxzXG4gICAgICAgIH1cbiAgICAgIF1cbiAgICB9O1xuICB9XG59XG4iXX0=