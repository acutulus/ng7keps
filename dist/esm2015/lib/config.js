/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
export const webAlias = location.host;
/** @type {?} */
export const apiPrefix = '/api/v1/';
/** @type {?} */
export const apiRoute = new URL(apiPrefix, location.origin).href;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxNQUFNLE9BQU8sUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJOztBQUNyQyxNQUFNLE9BQU8sU0FBUyxHQUFHLFVBQVU7O0FBQ25DLE1BQU0sT0FBTyxRQUFRLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNvbnN0IHdlYkFsaWFzID0gbG9jYXRpb24uaG9zdDtcclxuZXhwb3J0IGNvbnN0IGFwaVByZWZpeCA9ICcvYXBpL3YxLyc7XHJcbmV4cG9ydCBjb25zdCBhcGlSb3V0ZSA9IG5ldyBVUkwoYXBpUHJlZml4LCBsb2NhdGlvbi5vcmlnaW4pLmhyZWY7XHJcbiJdfQ==