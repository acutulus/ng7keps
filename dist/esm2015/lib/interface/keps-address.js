/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Represents an address. Second line of address and country is optional.
 * @record
 */
export function KepsAddress() { }
if (false) {
    /** @type {?} */
    KepsAddress.prototype.address1;
    /** @type {?|undefined} */
    KepsAddress.prototype.address2;
    /** @type {?} */
    KepsAddress.prototype.city;
    /** @type {?} */
    KepsAddress.prototype.region;
    /** @type {?} */
    KepsAddress.prototype.postal;
    /** @type {?|undefined} */
    KepsAddress.prototype.country;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2Vwcy1hZGRyZXNzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmZhY2Uva2Vwcy1hZGRyZXNzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBR0EsaUNBT0M7OztJQU5DLCtCQUE2Qjs7SUFDN0IsK0JBQTZCOztJQUM3QiwyQkFBNkI7O0lBQzdCLDZCQUE2Qjs7SUFDN0IsNkJBQTZCOztJQUM3Qiw4QkFBNkIiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICogUmVwcmVzZW50cyBhbiBhZGRyZXNzLiBTZWNvbmQgbGluZSBvZiBhZGRyZXNzIGFuZCBjb3VudHJ5IGlzIG9wdGlvbmFsLlxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBLZXBzQWRkcmVzcyB7XHJcbiAgYWRkcmVzczE6ICAgICAgICAgICAgIHN0cmluZztcclxuICBhZGRyZXNzMj86ICAgICAgICAgICAgc3RyaW5nO1xyXG4gIGNpdHk6ICAgICAgICAgICAgICAgICBzdHJpbmc7XHJcbiAgcmVnaW9uOiAgICAgICAgICAgICAgIHN0cmluZztcclxuICBwb3N0YWw6ICAgICAgICAgICAgICAgc3RyaW5nO1xyXG4gIGNvdW50cnk/OiAgICAgICAgICAgICBzdHJpbmc7XHJcbn1cclxuIl19