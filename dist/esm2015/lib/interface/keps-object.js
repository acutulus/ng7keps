/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Represents basic Keps object.
 * @record
 */
export function KepsObject() { }
if (false) {
    /** @type {?} */
    KepsObject.prototype._id;
    /** @type {?|undefined} */
    KepsObject.prototype._createdAt;
    /** @type {?|undefined} */
    KepsObject.prototype._updatedAt;
    /* Skipping unhandled member: [otherFields: string]: any;*/
}
;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2Vwcy1vYmplY3QuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2ludGVyZmFjZS9rZXBzLW9iamVjdC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUdBLGdDQUtDOzs7SUFKQyx5QkFBOEI7O0lBQzlCLGdDQUE4Qjs7SUFDOUIsZ0NBQThCOzs7QUFFL0IsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBSZXByZXNlbnRzIGJhc2ljIEtlcHMgb2JqZWN0LlxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBLZXBzT2JqZWN0IHtcclxuICBfaWQ6ICAgICAgICAgICAgICAgICAgIHN0cmluZztcclxuICBfY3JlYXRlZEF0PzogICAgICAgICAgIG51bWJlcjtcclxuICBfdXBkYXRlZEF0PzogICAgICAgICAgIG51bWJlcjtcclxuICBbb3RoZXJGaWVsZHM6IHN0cmluZ106IGFueTtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBSZXByZXNlbnRzIGEgbWFwIHdoZXJlIHRoZSBrZXlzIGFyZSBuYW1lcyBvZiBtb2RlbHMgYW5kIHRoZSB2YWx1ZXNcclxuICogYXJlIGFycmF5cyBvZiBgS2Vwc09iamVjdGBzLlxyXG4gKi9cclxuZXhwb3J0IHR5cGUgS2Vwc1JlZmVyZW5jZSA9IFJlY29yZDxzdHJpbmcsIEtlcHNPYmplY3RbXT47XHJcbiJdfQ==