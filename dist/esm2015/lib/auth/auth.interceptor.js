/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { apiRoute } from '../config';
import { tap } from 'rxjs/operators';
/**
 * Intercepts outgoing requests and adds token if needed.
 * Intercepts incoming responses and sets/removes user if needed.
 */
export class AuthInterceptor {
    /**
     * \@internal Do not use!
     * @param {?} auth
     */
    constructor(auth) {
        this.auth = auth;
    }
    /**
     * @param {?} req
     * @param {?} next
     * @return {?}
     */
    intercept(req, next) {
        // Intercept request and add token.
        /** @type {?} */
        let newUrl = req.url;
        /** @type {?} */
        let newHeaders = req.headers;
        if (req.url.startsWith(apiRoute)) {
            /** @type {?} */
            const token = this.auth.getToken();
            if (token) {
                newHeaders = newHeaders.set('Authorization', `Bearer ${token}`);
            }
            if (newUrl.indexOf('?') > -1) {
                newUrl = `${newUrl}&cache_bust=${(new Date()).getTime()}`;
            }
            else {
                newUrl = `${newUrl}?cache_bust=${(new Date()).getTime()}`;
            }
        }
        // Modify request and pass it to the next handler.
        req = req.clone({
            url: newUrl,
            headers: newHeaders
        });
        return next.handle(req)
            .pipe(tap((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            if (res instanceof HttpResponse) {
                // Intercept response and refresh user data if needed.
                if (req.url.startsWith(apiRoute)) {
                    if (res.headers.has('x-user-token-refresh') && res.headers.get('x-user-token-refresh') !== '') {
                        /** @type {?} */
                        const refreshedUser = JSON.parse(decodeURIComponent(res.headers.get('x-user-token-refresh')));
                        this.auth.setUser(refreshedUser);
                    }
                }
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        err => {
            // Intercept error response and logout user if session timed out.
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    if (err.headers.has('x-user-logout')) {
                        alert('Your Session has timed out please login again.');
                        this.auth.logout();
                        window.location.reload();
                    }
                }
            }
        })));
    }
}
AuthInterceptor.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AuthInterceptor.ctorParameters = () => [
    { type: AuthService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthInterceptor.prototype.auth;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvYXV0aC9hdXRoLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFJTCxZQUFZLEVBQ1osaUJBQWlCLEVBRWxCLE1BQU0sc0JBQXNCLENBQUM7QUFDOUIsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFFckMsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7OztBQU9yQyxNQUFNLE9BQU8sZUFBZTs7Ozs7SUFLMUIsWUFBb0IsSUFBaUI7UUFBakIsU0FBSSxHQUFKLElBQUksQ0FBYTtJQUFJLENBQUM7Ozs7OztJQUUxQyxTQUFTLENBQUMsR0FBcUIsRUFBRSxJQUFpQjs7O1lBRTVDLE1BQU0sR0FBRyxHQUFHLENBQUMsR0FBRzs7WUFDaEIsVUFBVSxHQUFHLEdBQUcsQ0FBQyxPQUFPO1FBQzVCLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7O2tCQUMxQixLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDbEMsSUFBSSxLQUFLLEVBQUU7Z0JBQ1QsVUFBVSxHQUFHLFVBQVUsQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLFVBQVUsS0FBSyxFQUFFLENBQUMsQ0FBQzthQUNqRTtZQUNELElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDNUIsTUFBTSxHQUFHLEdBQUcsTUFBTSxlQUFlLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUM7YUFDM0Q7aUJBQU07Z0JBQ0wsTUFBTSxHQUFHLEdBQUcsTUFBTSxlQUFlLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUM7YUFDM0Q7U0FDRjtRQUVELGtEQUFrRDtRQUNsRCxHQUFHLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQztZQUNkLEdBQUcsRUFBRSxNQUFNO1lBQ1gsT0FBTyxFQUFFLFVBQVU7U0FDcEIsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQzthQUN0QixJQUFJLENBQ0gsR0FBRzs7OztRQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ1IsSUFBSSxHQUFHLFlBQVksWUFBWSxFQUFFO2dCQUMvQixzREFBc0Q7Z0JBQ3RELElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2hDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLEVBQUUsRUFBRTs7OEJBQ3ZGLGFBQWEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQzt3QkFDN0YsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7cUJBQ2xDO2lCQUNGO2FBQ0Y7UUFDSCxDQUFDOzs7O1FBQUUsR0FBRyxDQUFDLEVBQUU7WUFDUCxpRUFBaUU7WUFDakUsSUFBSSxHQUFHLFlBQVksaUJBQWlCLEVBQUU7Z0JBQ3BDLElBQUksR0FBRyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7b0JBQ3RCLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLEVBQUU7d0JBQ3BDLEtBQUssQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO3dCQUN4RCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUNuQixNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO3FCQUMxQjtpQkFDRjthQUNGO1FBQ0gsQ0FBQyxFQUFDLENBQ0gsQ0FBQztJQUNKLENBQUM7OztZQXRERixVQUFVOzs7O1lBVEYsV0FBVzs7Ozs7OztJQWVOLCtCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICBIdHRwSW50ZXJjZXB0b3IsXHJcbiAgSHR0cEhhbmRsZXIsXHJcbiAgSHR0cFJlcXVlc3QsXHJcbiAgSHR0cFJlc3BvbnNlLFxyXG4gIEh0dHBFcnJvclJlc3BvbnNlLFxyXG4gIEh0dHBFdmVudFxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuL2F1dGguc2VydmljZSc7XHJcbmltcG9ydCB7IGFwaVJvdXRlIH0gZnJvbSAnLi4vY29uZmlnJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG4vKipcclxuICogSW50ZXJjZXB0cyBvdXRnb2luZyByZXF1ZXN0cyBhbmQgYWRkcyB0b2tlbiBpZiBuZWVkZWQuXHJcbiAqIEludGVyY2VwdHMgaW5jb21pbmcgcmVzcG9uc2VzIGFuZCBzZXRzL3JlbW92ZXMgdXNlciBpZiBuZWVkZWQuXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBdXRoSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGF1dGg6IEF1dGhTZXJ2aWNlKSB7IH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcTogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XHJcbiAgICAvLyBJbnRlcmNlcHQgcmVxdWVzdCBhbmQgYWRkIHRva2VuLlxyXG4gICAgbGV0IG5ld1VybCA9IHJlcS51cmw7XHJcbiAgICBsZXQgbmV3SGVhZGVycyA9IHJlcS5oZWFkZXJzO1xyXG4gICAgaWYgKHJlcS51cmwuc3RhcnRzV2l0aChhcGlSb3V0ZSkpIHtcclxuICAgICAgY29uc3QgdG9rZW4gPSB0aGlzLmF1dGguZ2V0VG9rZW4oKTtcclxuICAgICAgaWYgKHRva2VuKSB7XHJcbiAgICAgICAgbmV3SGVhZGVycyA9IG5ld0hlYWRlcnMuc2V0KCdBdXRob3JpemF0aW9uJywgYEJlYXJlciAke3Rva2VufWApO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChuZXdVcmwuaW5kZXhPZignPycpID4gLTEpIHtcclxuICAgICAgICBuZXdVcmwgPSBgJHtuZXdVcmx9JmNhY2hlX2J1c3Q9JHsobmV3IERhdGUoKSkuZ2V0VGltZSgpfWA7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbmV3VXJsID0gYCR7bmV3VXJsfT9jYWNoZV9idXN0PSR7KG5ldyBEYXRlKCkpLmdldFRpbWUoKX1gO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gTW9kaWZ5IHJlcXVlc3QgYW5kIHBhc3MgaXQgdG8gdGhlIG5leHQgaGFuZGxlci5cclxuICAgIHJlcSA9IHJlcS5jbG9uZSh7XHJcbiAgICAgIHVybDogbmV3VXJsLFxyXG4gICAgICBoZWFkZXJzOiBuZXdIZWFkZXJzXHJcbiAgICB9KTtcclxuICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXEpXHJcbiAgICAucGlwZShcclxuICAgICAgdGFwKHJlcyA9PiB7XHJcbiAgICAgICAgaWYgKHJlcyBpbnN0YW5jZW9mIEh0dHBSZXNwb25zZSkge1xyXG4gICAgICAgICAgLy8gSW50ZXJjZXB0IHJlc3BvbnNlIGFuZCByZWZyZXNoIHVzZXIgZGF0YSBpZiBuZWVkZWQuXHJcbiAgICAgICAgICBpZiAocmVxLnVybC5zdGFydHNXaXRoKGFwaVJvdXRlKSkge1xyXG4gICAgICAgICAgICBpZiAocmVzLmhlYWRlcnMuaGFzKCd4LXVzZXItdG9rZW4tcmVmcmVzaCcpICYmIHJlcy5oZWFkZXJzLmdldCgneC11c2VyLXRva2VuLXJlZnJlc2gnKSAhPT0gJycpIHtcclxuICAgICAgICAgICAgICBjb25zdCByZWZyZXNoZWRVc2VyID0gSlNPTi5wYXJzZShkZWNvZGVVUklDb21wb25lbnQocmVzLmhlYWRlcnMuZ2V0KCd4LXVzZXItdG9rZW4tcmVmcmVzaCcpKSk7XHJcbiAgICAgICAgICAgICAgdGhpcy5hdXRoLnNldFVzZXIocmVmcmVzaGVkVXNlcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0sIGVyciA9PiB7XHJcbiAgICAgICAgLy8gSW50ZXJjZXB0IGVycm9yIHJlc3BvbnNlIGFuZCBsb2dvdXQgdXNlciBpZiBzZXNzaW9uIHRpbWVkIG91dC5cclxuICAgICAgICBpZiAoZXJyIGluc3RhbmNlb2YgSHR0cEVycm9yUmVzcG9uc2UpIHtcclxuICAgICAgICAgIGlmIChlcnIuc3RhdHVzID09PSA0MDEpIHtcclxuICAgICAgICAgICAgaWYgKGVyci5oZWFkZXJzLmhhcygneC11c2VyLWxvZ291dCcpKSB7XHJcbiAgICAgICAgICAgICAgYWxlcnQoJ1lvdXIgU2Vzc2lvbiBoYXMgdGltZWQgb3V0IHBsZWFzZSBsb2dpbiBhZ2Fpbi4nKTtcclxuICAgICAgICAgICAgICB0aGlzLmF1dGgubG9nb3V0KCk7XHJcbiAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==