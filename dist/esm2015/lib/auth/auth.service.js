/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject } from 'rxjs';
import { webAlias, apiRoute } from '../config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
/** @type {?} */
const storageKey = webAlias + '-user';
/**
 * A service that handles authentication with a Keps server.
 */
export class AuthService {
    /**
     * \@internal Do not use!
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.user$ = new ReplaySubject(1);
        this.loggedIn$ = new ReplaySubject(1);
    }
    /**
     * Starts the auth service.
     * Should be called only when the app is bootstrapping.
     * @return {?}
     */
    start() {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!AuthService} */ function* () {
            // Try getting user from storage.
            /** @type {?} */
            let storageUser;
            try {
                storageUser = JSON.parse(localStorage.getItem(storageKey));
            }
            catch (err) {
                console.error('Error getting user from local storage');
                this.setUser(null);
            }
            // If user exist, then get user data from server.
            if (storageUser) {
                try {
                    /** @type {?} */
                    const serverUser = yield this.http.get(apiRoute + 'users/me').toPromise();
                    if (serverUser.token) {
                        this.setUser(serverUser);
                    }
                    else if (storageUser.tokenExpires > new Date().getTime()) {
                        this.setUser(storageUser);
                    }
                    else {
                        // Token expired.
                        this.user$.error(serverUser);
                    }
                }
                catch (err) {
                    // Token error.
                    this.user$.error(err);
                }
            }
            else {
                this.setUser(null);
            }
        });
    }
    /**
     * Saves/removes user and emits the user and logged in status to observables.
     * \@internal Do not use!
     * @param {?=} user A user or null
     * @return {?}
     */
    setUser(user) {
        if (user) {
            localStorage.setItem(storageKey, JSON.stringify(user));
            this.user = user;
            this.user$.next(user);
            this.loggedIn$.next(true);
        }
        else {
            localStorage.removeItem(storageKey);
            this.user = null;
            this.user$.next(null);
            this.loggedIn$.next(false);
        }
    }
    /**
     * Returns the current user.
     * @return {?}
     */
    getUser() {
        return this.user;
    }
    /**
     * Returns an observable that will emit the current user.
     * @return {?}
     */
    getUser$() {
        return this.user$.asObservable();
    }
    /**
     * Returns token from the user.
     * @return {?}
     */
    getToken() {
        // If user is stored in auth service, use that user's token.
        if (this.user) {
            return this.user.token;
        }
        else {
            // Otherwise, try getting from the storage.
            try {
                /** @type {?} */
                const storageUser = JSON.parse(localStorage.getItem(storageKey));
                return storageUser.token;
            }
            catch (err) {
                console.error('Error getting user from local storage');
                return null;
            }
        }
    }
    /**
     * Returns an observable that emits logged in status.
     * @return {?}
     */
    isLoggedIn$() {
        return this.loggedIn$.asObservable();
    }
    /**
     * Logs in using the passed in provider name.
     * @param {?} provider Provider to use to login.
     * @param {?=} data Data to be passed on to the provider.
     * @return {?}
     */
    loginWithProvider(provider, data) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!AuthService} */ function* () {
            if (provider === 'local') {
                /** @type {?} */
                const serverUser = yield this.http.post(apiRoute + 'users/signin', data).toPromise();
                this.setUser(serverUser);
                return this.user;
            }
            else {
                window.location.href = apiRoute + 'oauths/' + provider;
            }
        });
    }
    /**
     * Signs up user using the passed in provider name.
     * @param {?} provider Provider to use to sign up.
     * @param {?=} data Data to be passed on to the provider.
     * @param {?=} customRoute Custom route to use instead of the default 'users/signup'.
     * @return {?}
     */
    signupWithProvider(provider, data, customRoute) {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!AuthService} */ function* () {
            if (provider === 'local') {
                /** @type {?} */
                const route = apiRoute + customRoute || 'users/signup';
                /** @type {?} */
                const serverUser = yield this.http.post(route, data).toPromise();
                if (data.noLogin) {
                    return serverUser;
                }
                else {
                    this.setUser(serverUser);
                    return this.user;
                }
            }
            else {
                window.location.href = webAlias + '/auth/' + provider;
            }
        });
    }
    /**
     * Switch current user.
     * @param {?} newUser
     * @return {?}
     */
    switchUser(newUser) {
        this.setUser(newUser);
    }
    /**
     * Logs out the current user.
     * @return {?}
     */
    logout() {
        this.setUser(null);
    }
}
AuthService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ AuthService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthService_Factory() { return new AuthService(i0.ɵɵinject(i1.HttpClient)); }, token: AuthService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.user;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.user$;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.loggedIn$;
    /**
     * @type {?}
     * @private
     */
    AuthService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9hdXRoL2F1dGguc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxhQUFhLEVBQWMsTUFBTSxNQUFNLENBQUM7QUFDakQsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxXQUFXLENBQUM7Ozs7TUFJekMsVUFBVSxHQUFHLFFBQVEsR0FBRyxPQUFPOzs7O0FBUXJDLE1BQU0sT0FBTyxXQUFXOzs7OztJQVF0QixZQUFvQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBTjVCLFVBQUssR0FBRyxJQUFJLGFBQWEsQ0FBVyxDQUFDLENBQUMsQ0FBQztRQUN2QyxjQUFTLEdBQUcsSUFBSSxhQUFhLENBQVUsQ0FBQyxDQUFDLENBQUM7SUFLVixDQUFDOzs7Ozs7SUFNbkMsS0FBSzs7OztnQkFFTCxXQUFxQjtZQUN6QixJQUFJO2dCQUNGLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzthQUM1RDtZQUFDLE9BQU8sR0FBRyxFQUFFO2dCQUNaLE9BQU8sQ0FBQyxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQztnQkFDdkQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNwQjtZQUVELGlEQUFpRDtZQUNqRCxJQUFJLFdBQVcsRUFBRTtnQkFDZixJQUFJOzswQkFDSSxVQUFVLEdBQUcsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBVyxRQUFRLEdBQUcsVUFBVSxDQUFDLENBQUMsU0FBUyxFQUFFO29CQUNuRixJQUFJLFVBQVUsQ0FBQyxLQUFLLEVBQUU7d0JBQ3BCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQzFCO3lCQUFNLElBQUksV0FBVyxDQUFDLFlBQVksR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFFO3dCQUMxRCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO3FCQUMzQjt5QkFBTTt3QkFDTCxpQkFBaUI7d0JBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUM5QjtpQkFDRjtnQkFBQyxPQUFPLEdBQUcsRUFBRTtvQkFDWixlQUFlO29CQUNmLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUN2QjthQUNGO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDcEI7UUFDSCxDQUFDO0tBQUE7Ozs7Ozs7SUFPRCxPQUFPLENBQUMsSUFBZTtRQUNyQixJQUFJLElBQUksRUFBRTtZQUNSLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0wsWUFBWSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM1QjtJQUNILENBQUM7Ozs7O0lBS0QsT0FBTztRQUNMLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztJQUNuQixDQUFDOzs7OztJQUtELFFBQVE7UUFDTixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFLRCxRQUFRO1FBQ04sNERBQTREO1FBQzVELElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNiLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDeEI7YUFBTTtZQUNMLDJDQUEyQztZQUMzQyxJQUFJOztzQkFDSSxXQUFXLEdBQWEsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUMxRSxPQUFPLFdBQVcsQ0FBQyxLQUFLLENBQUM7YUFDMUI7WUFBQyxPQUFPLEdBQUcsRUFBRTtnQkFDWixPQUFPLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7Z0JBQ3ZELE9BQU8sSUFBSSxDQUFDO2FBQ2I7U0FDRjtJQUNILENBQUM7Ozs7O0lBS0QsV0FBVztRQUNULE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN2QyxDQUFDOzs7Ozs7O0lBa0JLLGlCQUFpQixDQUFDLFFBQWdCLEVBQUUsSUFBb0I7O1lBQzVELElBQUksUUFBUSxLQUFLLE9BQU8sRUFBRTs7c0JBQ2xCLFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFXLFFBQVEsR0FBRyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUyxFQUFFO2dCQUM5RixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN6QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUM7YUFDbEI7aUJBQU07Z0JBQ0wsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsUUFBUSxHQUFHLFNBQVMsR0FBRyxRQUFRLENBQUM7YUFDeEQ7UUFDSCxDQUFDO0tBQUE7Ozs7Ozs7O0lBb0JLLGtCQUFrQixDQUFDLFFBQWdCLEVBQUUsSUFBcUIsRUFBRSxXQUFvQjs7WUFDcEYsSUFBSSxRQUFRLEtBQUssT0FBTyxFQUFFOztzQkFDbEIsS0FBSyxHQUFHLFFBQVEsR0FBRyxXQUFXLElBQUksY0FBYzs7c0JBQ2hELFVBQVUsR0FBRyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFXLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTLEVBQUU7Z0JBQzFFLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtvQkFDaEIsT0FBTyxVQUFVLENBQUM7aUJBQ25CO3FCQUFNO29CQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3pCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztpQkFDbEI7YUFDRjtpQkFBTTtnQkFDTCxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxRQUFRLEdBQUcsUUFBUSxHQUFHLFFBQVEsQ0FBQzthQUN2RDtRQUNILENBQUM7S0FBQTs7Ozs7O0lBS0QsVUFBVSxDQUFDLE9BQWlCO1FBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFLRCxNQUFNO1FBQ0osSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyQixDQUFDOzs7WUFsTEYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBYlEsVUFBVTs7Ozs7Ozs7SUFlakIsMkJBQXVCOzs7OztJQUN2Qiw0QkFBK0M7Ozs7O0lBQy9DLGdDQUFrRDs7Ozs7SUFLdEMsMkJBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBSZXBsYXlTdWJqZWN0LCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHdlYkFsaWFzLCBhcGlSb3V0ZSB9IGZyb20gJy4uL2NvbmZpZyc7XHJcbmltcG9ydCB7IEtlcHNVc2VyIH0gZnJvbSAnLi4vaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgQXV0aExvZ2luRGF0YSwgQXV0aFNpZ251cERhdGEgfSBmcm9tICcuL2F1dGgtaW50ZXJmYWNlcyc7XHJcblxyXG5jb25zdCBzdG9yYWdlS2V5ID0gd2ViQWxpYXMgKyAnLXVzZXInO1xyXG5cclxuLyoqXHJcbiAqIEEgc2VydmljZSB0aGF0IGhhbmRsZXMgYXV0aGVudGljYXRpb24gd2l0aCBhIEtlcHMgc2VydmVyLlxyXG4gKi9cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZpY2Uge1xyXG4gIHByaXZhdGUgdXNlcjogS2Vwc1VzZXI7XHJcbiAgcHJpdmF0ZSB1c2VyJCA9IG5ldyBSZXBsYXlTdWJqZWN0PEtlcHNVc2VyPigxKTtcclxuICBwcml2YXRlIGxvZ2dlZEluJCA9IG5ldyBSZXBsYXlTdWJqZWN0PGJvb2xlYW4+KDEpO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHsgfVxyXG5cclxuICAvKipcclxuICAgKiBTdGFydHMgdGhlIGF1dGggc2VydmljZS5cclxuICAgKiBTaG91bGQgYmUgY2FsbGVkIG9ubHkgd2hlbiB0aGUgYXBwIGlzIGJvb3RzdHJhcHBpbmcuXHJcbiAgICovXHJcbiAgYXN5bmMgc3RhcnQoKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICAvLyBUcnkgZ2V0dGluZyB1c2VyIGZyb20gc3RvcmFnZS5cclxuICAgIGxldCBzdG9yYWdlVXNlcjogS2Vwc1VzZXI7XHJcbiAgICB0cnkge1xyXG4gICAgICBzdG9yYWdlVXNlciA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oc3RvcmFnZUtleSkpO1xyXG4gICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIGdldHRpbmcgdXNlciBmcm9tIGxvY2FsIHN0b3JhZ2UnKTtcclxuICAgICAgdGhpcy5zZXRVc2VyKG51bGwpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIElmIHVzZXIgZXhpc3QsIHRoZW4gZ2V0IHVzZXIgZGF0YSBmcm9tIHNlcnZlci5cclxuICAgIGlmIChzdG9yYWdlVXNlcikge1xyXG4gICAgICB0cnkge1xyXG4gICAgICAgIGNvbnN0IHNlcnZlclVzZXIgPSBhd2FpdCB0aGlzLmh0dHAuZ2V0PEtlcHNVc2VyPihhcGlSb3V0ZSArICd1c2Vycy9tZScpLnRvUHJvbWlzZSgpO1xyXG4gICAgICAgIGlmIChzZXJ2ZXJVc2VyLnRva2VuKSB7XHJcbiAgICAgICAgICB0aGlzLnNldFVzZXIoc2VydmVyVXNlcik7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzdG9yYWdlVXNlci50b2tlbkV4cGlyZXMgPiBuZXcgRGF0ZSgpLmdldFRpbWUoKSkge1xyXG4gICAgICAgICAgdGhpcy5zZXRVc2VyKHN0b3JhZ2VVc2VyKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgLy8gVG9rZW4gZXhwaXJlZC5cclxuICAgICAgICAgIHRoaXMudXNlciQuZXJyb3Ioc2VydmVyVXNlcik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGNhdGNoIChlcnIpIHtcclxuICAgICAgICAvLyBUb2tlbiBlcnJvci5cclxuICAgICAgICB0aGlzLnVzZXIkLmVycm9yKGVycik7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2V0VXNlcihudWxsKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNhdmVzL3JlbW92ZXMgdXNlciBhbmQgZW1pdHMgdGhlIHVzZXIgYW5kIGxvZ2dlZCBpbiBzdGF0dXMgdG8gb2JzZXJ2YWJsZXMuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICogQHBhcmFtIHVzZXIgQSB1c2VyIG9yIG51bGxcclxuICAgKi9cclxuICBzZXRVc2VyKHVzZXI/OiBLZXBzVXNlcikge1xyXG4gICAgaWYgKHVzZXIpIHtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oc3RvcmFnZUtleSwgSlNPTi5zdHJpbmdpZnkodXNlcikpO1xyXG4gICAgICB0aGlzLnVzZXIgPSB1c2VyO1xyXG4gICAgICB0aGlzLnVzZXIkLm5leHQodXNlcik7XHJcbiAgICAgIHRoaXMubG9nZ2VkSW4kLm5leHQodHJ1ZSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShzdG9yYWdlS2V5KTtcclxuICAgICAgdGhpcy51c2VyID0gbnVsbDtcclxuICAgICAgdGhpcy51c2VyJC5uZXh0KG51bGwpO1xyXG4gICAgICB0aGlzLmxvZ2dlZEluJC5uZXh0KGZhbHNlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJldHVybnMgdGhlIGN1cnJlbnQgdXNlci5cclxuICAgKi9cclxuICBnZXRVc2VyKCk6IEtlcHNVc2VyIHtcclxuICAgIHJldHVybiB0aGlzLnVzZXI7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIGFuIG9ic2VydmFibGUgdGhhdCB3aWxsIGVtaXQgdGhlIGN1cnJlbnQgdXNlci5cclxuICAgKi9cclxuICBnZXRVc2VyJCgpOiBPYnNlcnZhYmxlPEtlcHNVc2VyPiB7XHJcbiAgICByZXR1cm4gdGhpcy51c2VyJC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJldHVybnMgdG9rZW4gZnJvbSB0aGUgdXNlci5cclxuICAgKi9cclxuICBnZXRUb2tlbigpOiBzdHJpbmcgfCBudWxsIHtcclxuICAgIC8vIElmIHVzZXIgaXMgc3RvcmVkIGluIGF1dGggc2VydmljZSwgdXNlIHRoYXQgdXNlcidzIHRva2VuLlxyXG4gICAgaWYgKHRoaXMudXNlcikge1xyXG4gICAgICByZXR1cm4gdGhpcy51c2VyLnRva2VuO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gT3RoZXJ3aXNlLCB0cnkgZ2V0dGluZyBmcm9tIHRoZSBzdG9yYWdlLlxyXG4gICAgICB0cnkge1xyXG4gICAgICAgIGNvbnN0IHN0b3JhZ2VVc2VyOiBLZXBzVXNlciA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oc3RvcmFnZUtleSkpO1xyXG4gICAgICAgIHJldHVybiBzdG9yYWdlVXNlci50b2tlbjtcclxuICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3IgZ2V0dGluZyB1c2VyIGZyb20gbG9jYWwgc3RvcmFnZScpO1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIGFuIG9ic2VydmFibGUgdGhhdCBlbWl0cyBsb2dnZWQgaW4gc3RhdHVzLlxyXG4gICAqL1xyXG4gIGlzTG9nZ2VkSW4kKCk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgcmV0dXJuIHRoaXMubG9nZ2VkSW4kLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogTG9ncyBpbiB1c2luZyBsb2NhbCBwcm92aWRlciwgc2V0cyBjdXJyZW50IHVzZXIsIGFuZCByZXR1cm5zIHRoZSB1c2VyIG9iamVjdC5cclxuICAgKiBAcGFyYW0gcHJvdmlkZXIgTG9jYWwgcHJvdmlkZXIuXHJcbiAgICogQHBhcmFtIGRhdGEgQW4gb2JqZWN0IHdpdGggdXNlcm5hbWUgYW5kIHBhc3N3b3JkIGZpZWxkcy5cclxuICAgKi9cclxuICBhc3luYyBsb2dpbldpdGhQcm92aWRlcihwcm92aWRlcjogJ2xvY2FsJywgZGF0YTogQXV0aExvZ2luRGF0YSk6IFByb21pc2U8S2Vwc1VzZXI+O1xyXG4gIC8qKlxyXG4gICAqIExvZ3MgaW4gdXNpbmcgdGhlIHBhc3NlZCBpbiBwcm92aWRlciBuYW1lLlxyXG4gICAqIEBwYXJhbSBwcm92aWRlciBQcm92aWRlciB0byB1c2UgdG8gbG9naW4uXHJcbiAgICovXHJcbiAgYXN5bmMgbG9naW5XaXRoUHJvdmlkZXIocHJvdmlkZXI6IHN0cmluZyk6IFByb21pc2U8dm9pZD47XHJcbiAgLyoqXHJcbiAgICogTG9ncyBpbiB1c2luZyB0aGUgcGFzc2VkIGluIHByb3ZpZGVyIG5hbWUuXHJcbiAgICogQHBhcmFtIHByb3ZpZGVyIFByb3ZpZGVyIHRvIHVzZSB0byBsb2dpbi5cclxuICAgKiBAcGFyYW0gZGF0YSBEYXRhIHRvIGJlIHBhc3NlZCBvbiB0byB0aGUgcHJvdmlkZXIuXHJcbiAgICovXHJcbiAgYXN5bmMgbG9naW5XaXRoUHJvdmlkZXIocHJvdmlkZXI6IHN0cmluZywgZGF0YT86IEF1dGhMb2dpbkRhdGEpOiBQcm9taXNlPEtlcHNVc2VyIHwgdm9pZD4ge1xyXG4gICAgaWYgKHByb3ZpZGVyID09PSAnbG9jYWwnKSB7XHJcbiAgICAgIGNvbnN0IHNlcnZlclVzZXIgPSBhd2FpdCB0aGlzLmh0dHAucG9zdDxLZXBzVXNlcj4oYXBpUm91dGUgKyAndXNlcnMvc2lnbmluJywgZGF0YSkudG9Qcm9taXNlKCk7XHJcbiAgICAgIHRoaXMuc2V0VXNlcihzZXJ2ZXJVc2VyKTtcclxuICAgICAgcmV0dXJuIHRoaXMudXNlcjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gYXBpUm91dGUgKyAnb2F1dGhzLycgKyBwcm92aWRlcjtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNpZ25zIHVwIHVzaW5nIGxvY2FsIHByb3ZpZGVyLCBzZXRzIGN1cnJlbnQgdXNlciwgYW5kIHJldHVybnMgdGhlIHVzZXIgb2JqZWN0LlxyXG4gICAqIEBwYXJhbSBwcm92aWRlciBMb2NhbCBwcm92aWRlci5cclxuICAgKiBAcGFyYW0gZGF0YSBBbiBvYmplY3Qgd2l0aCB1c2UgZGF0YS5cclxuICAgKiBAcGFyYW0gY3VzdG9tUm91dGUgQ3VzdG9tIHJvdXRlIHRvIHVzZSBpbnN0ZWFkIG9mIHRoZSBkZWZhdWx0ICd1c2Vycy9zaWdudXAnLlxyXG4gICAqL1xyXG4gIGFzeW5jIHNpZ251cFdpdGhQcm92aWRlcihwcm92aWRlcjogJ2xvY2FsJywgZGF0YTogQXV0aFNpZ251cERhdGEsIGN1c3RvbVJvdXRlPzogc3RyaW5nKTogUHJvbWlzZTxLZXBzVXNlcj47XHJcbiAgLyoqXHJcbiAgICogU2lnbnMgdXAgdXNlciB1c2luZyB0aGUgcGFzc2VkIGluIHByb3ZpZGVyIG5hbWUuXHJcbiAgICogQHBhcmFtIHByb3ZpZGVyIFByb3ZpZGVyIHRvIHVzZSB0byBzaWduIHVwLlxyXG4gICAqL1xyXG4gIGFzeW5jIHNpZ251cFdpdGhQcm92aWRlcihwcm92aWRlcjogc3RyaW5nKTogUHJvbWlzZTx2b2lkPjtcclxuICAvKipcclxuICAgKiBTaWducyB1cCB1c2VyIHVzaW5nIHRoZSBwYXNzZWQgaW4gcHJvdmlkZXIgbmFtZS5cclxuICAgKiBAcGFyYW0gcHJvdmlkZXIgUHJvdmlkZXIgdG8gdXNlIHRvIHNpZ24gdXAuXHJcbiAgICogQHBhcmFtIGRhdGEgRGF0YSB0byBiZSBwYXNzZWQgb24gdG8gdGhlIHByb3ZpZGVyLlxyXG4gICAqIEBwYXJhbSBjdXN0b21Sb3V0ZSBDdXN0b20gcm91dGUgdG8gdXNlIGluc3RlYWQgb2YgdGhlIGRlZmF1bHQgJ3VzZXJzL3NpZ251cCcuXHJcbiAgICovXHJcbiAgYXN5bmMgc2lnbnVwV2l0aFByb3ZpZGVyKHByb3ZpZGVyOiBzdHJpbmcsIGRhdGE/OiBBdXRoU2lnbnVwRGF0YSwgY3VzdG9tUm91dGU/OiBzdHJpbmcpOiBQcm9taXNlPEtlcHNVc2VyIHwgdm9pZD4ge1xyXG4gICAgaWYgKHByb3ZpZGVyID09PSAnbG9jYWwnKSB7XHJcbiAgICAgIGNvbnN0IHJvdXRlID0gYXBpUm91dGUgKyBjdXN0b21Sb3V0ZSB8fCAndXNlcnMvc2lnbnVwJztcclxuICAgICAgY29uc3Qgc2VydmVyVXNlciA9IGF3YWl0IHRoaXMuaHR0cC5wb3N0PEtlcHNVc2VyPihyb3V0ZSwgZGF0YSkudG9Qcm9taXNlKCk7XHJcbiAgICAgIGlmIChkYXRhLm5vTG9naW4pIHtcclxuICAgICAgICByZXR1cm4gc2VydmVyVXNlcjtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnNldFVzZXIoc2VydmVyVXNlcik7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudXNlcjtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSB3ZWJBbGlhcyArICcvYXV0aC8nICsgcHJvdmlkZXI7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTd2l0Y2ggY3VycmVudCB1c2VyLlxyXG4gICAqL1xyXG4gIHN3aXRjaFVzZXIobmV3VXNlcjogS2Vwc1VzZXIpIHtcclxuICAgIHRoaXMuc2V0VXNlcihuZXdVc2VyKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIExvZ3Mgb3V0IHRoZSBjdXJyZW50IHVzZXIuXHJcbiAgICovXHJcbiAgbG9nb3V0KCkge1xyXG4gICAgdGhpcy5zZXRVc2VyKG51bGwpO1xyXG4gIH1cclxufVxyXG4iXX0=