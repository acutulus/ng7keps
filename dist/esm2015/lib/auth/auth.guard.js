/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { take, map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "./auth.service";
/**
 * A route guard that rejects user from accessing the route if they're not logged in.
 *
 * Can also allows only specific user role from accessing the route based on the route
 * data's `guardOpt` field. See {\@link AuthGuardOptions}.
 */
export class AuthGuard {
    /**
     * \@internal Do not use!
     * @param {?} router
     * @param {?} auth
     */
    constructor(router, auth) {
        this.router = router;
        this.auth = auth;
    }
    /**
     * @param {?} next
     * @param {?} state
     * @return {?}
     */
    canActivate(next, state) {
        return this.auth.isLoggedIn$()
            .pipe(take(1), map((/**
         * @param {?} isLoggedIn
         * @return {?}
         */
        (isLoggedIn) => {
            if (next.data && next.data.guardOpt) {
                /** @type {?} */
                const opts = (/** @type {?} */ (next.data.guardOpt));
                if (isLoggedIn) {
                    /** @type {?} */
                    const user = this.auth.getUser();
                    if (opts.role) {
                        if (user.roles.includes(opts.role)) {
                            return true;
                        }
                        else {
                            alert('Access Denied');
                            if (opts.logout) {
                                this.auth.logout();
                                if (opts.logoutUrl) {
                                    window.location.href = opts.logoutUrl;
                                }
                                else {
                                    window.location.href = window.location.origin;
                                }
                            }
                            else {
                                this.router.navigate([this.router.url]);
                            }
                            return false;
                        }
                    }
                    return true;
                }
                else {
                    if (opts.logoutUrl) {
                        window.location.href = opts.logoutUrl;
                    }
                    else {
                        window.location.href = window.location.origin;
                    }
                    return false;
                }
            }
        })));
    }
}
AuthGuard.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthGuard.ctorParameters = () => [
    { type: Router },
    { type: AuthService }
];
/** @nocollapse */ AuthGuard.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthGuard_Factory() { return new AuthGuard(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.AuthService)); }, token: AuthGuard, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthGuard.prototype.router;
    /**
     * @type {?}
     * @private
     */
    AuthGuard.prototype.auth;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5ndWFyZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvYXV0aC9hdXRoLmd1YXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFDTCxNQUFNLEVBSVAsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHN0MsT0FBTyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7Ozs7OztBQVczQyxNQUFNLE9BQU8sU0FBUzs7Ozs7O0lBS3BCLFlBQ1UsTUFBYyxFQUNkLElBQWlCO1FBRGpCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxTQUFJLEdBQUosSUFBSSxDQUFhO0lBQ3hCLENBQUM7Ozs7OztJQUVKLFdBQVcsQ0FDVCxJQUE0QixFQUM1QixLQUEwQjtRQUUxQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO2FBQzdCLElBQUksQ0FDSCxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQ1AsR0FBRzs7OztRQUFDLENBQUMsVUFBbUIsRUFBRSxFQUFFO1lBQzFCLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTs7c0JBQzdCLElBQUksR0FBRyxtQkFBa0IsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUE7Z0JBQ2pELElBQUksVUFBVSxFQUFFOzswQkFDTixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ2hDLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTt3QkFDYixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFDbEMsT0FBTyxJQUFJLENBQUM7eUJBQ2I7NkJBQU07NEJBQ0wsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDOzRCQUN2QixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0NBQ2YsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQ0FDbkIsSUFBRyxJQUFJLENBQUMsU0FBUyxFQUFDO29DQUNoQixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO2lDQUN2QztxQ0FBSztvQ0FDSixNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztpQ0FDL0M7NkJBQ0Y7aUNBQU07Z0NBQ0wsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7NkJBQ3pDOzRCQUNELE9BQU8sS0FBSyxDQUFDO3lCQUNkO3FCQUVKO29CQUNELE9BQU8sSUFBSSxDQUFDO2lCQUNiO3FCQUFNO29CQUNMLElBQUcsSUFBSSxDQUFDLFNBQVMsRUFBQzt3QkFDaEIsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztxQkFDdkM7eUJBQUs7d0JBQ0osTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7cUJBQy9DO29CQUNELE9BQU8sS0FBSyxDQUFDO2lCQUNkO2FBQ0o7UUFDRCxDQUFDLEVBQUMsQ0FDSCxDQUFDO0lBQ0osQ0FBQzs7O1lBeERGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQWxCQyxNQUFNO1lBS0MsV0FBVzs7Ozs7Ozs7SUFvQmhCLDJCQUFzQjs7Ozs7SUFDdEIseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1xyXG4gIFJvdXRlcixcclxuICBDYW5BY3RpdmF0ZSxcclxuICBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gIFJvdXRlclN0YXRlU25hcHNob3RcclxufSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4vYXV0aC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXV0aEd1YXJkT3B0aW9ucyB9IGZyb20gJy4vYXV0aC1pbnRlcmZhY2VzJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG4vKipcclxuICogQSByb3V0ZSBndWFyZCB0aGF0IHJlamVjdHMgdXNlciBmcm9tIGFjY2Vzc2luZyB0aGUgcm91dGUgaWYgdGhleSdyZSBub3QgbG9nZ2VkIGluLlxyXG4gKlxyXG4gKiBDYW4gYWxzbyBhbGxvd3Mgb25seSBzcGVjaWZpYyB1c2VyIHJvbGUgZnJvbSBhY2Nlc3NpbmcgdGhlIHJvdXRlIGJhc2VkIG9uIHRoZSByb3V0ZVxyXG4gKiBkYXRhJ3MgYGd1YXJkT3B0YCBmaWVsZC4gU2VlIHtAbGluayBBdXRoR3VhcmRPcHRpb25zfS5cclxuICovXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEF1dGhHdWFyZCBpbXBsZW1lbnRzIENhbkFjdGl2YXRlIHtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgcHJpdmF0ZSBhdXRoOiBBdXRoU2VydmljZVxyXG4gICkge31cclxuXHJcbiAgY2FuQWN0aXZhdGUoXHJcbiAgICBuZXh0OiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gICAgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3RcclxuICApOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgIHJldHVybiB0aGlzLmF1dGguaXNMb2dnZWRJbiQoKVxyXG4gICAgLnBpcGUoXHJcbiAgICAgIHRha2UoMSksXHJcbiAgICAgIG1hcCgoaXNMb2dnZWRJbjogYm9vbGVhbikgPT4ge1xyXG4gICAgICAgIGlmIChuZXh0LmRhdGEgJiYgbmV4dC5kYXRhLmd1YXJkT3B0KSB7XHJcbiAgICAgICAgICBjb25zdCBvcHRzID0gPEF1dGhHdWFyZE9wdGlvbnM+bmV4dC5kYXRhLmd1YXJkT3B0O1xyXG4gICAgICAgICAgaWYgKGlzTG9nZ2VkSW4pIHtcclxuICAgICAgICAgICAgICBjb25zdCB1c2VyID0gdGhpcy5hdXRoLmdldFVzZXIoKTtcclxuICAgICAgICAgICAgICBpZiAob3B0cy5yb2xlKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodXNlci5yb2xlcy5pbmNsdWRlcyhvcHRzLnJvbGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgYWxlcnQoJ0FjY2VzcyBEZW5pZWQnKTtcclxuICAgICAgICAgICAgICAgICAgaWYgKG9wdHMubG9nb3V0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hdXRoLmxvZ291dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKG9wdHMubG9nb3V0VXJsKXtcclxuICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gb3B0cy5sb2dvdXRVcmw7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNle1xyXG4gICAgICAgICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdGhpcy5yb3V0ZXIudXJsXSk7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYob3B0cy5sb2dvdXRVcmwpe1xyXG4gICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gb3B0cy5sb2dvdXRVcmw7XHJcbiAgICAgICAgICAgIH0gZWxzZXtcclxuICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IHdpbmRvdy5sb2NhdGlvbi5vcmlnaW47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH1cclxuXHJcbn1cclxuIl19