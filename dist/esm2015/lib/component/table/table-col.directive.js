/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, Input } from '@angular/core';
/**
 * Represents a custom column data & template.
 * \@internal Do not use!
 * @record
 */
export function CustomColumn() { }
if (false) {
    /** @type {?} */
    CustomColumn.prototype.data;
    /** @type {?} */
    CustomColumn.prototype.template;
}
/**
 * A directive to create custom column of a Keps Table.
 */
export class TableColDirective {
    /**
     * \@internal Do not use!
     */
    constructor() { }
}
TableColDirective.decorators = [
    { type: Directive, args: [{
                selector: '[kepsTableCol]'
            },] }
];
/** @nocollapse */
TableColDirective.ctorParameters = () => [];
TableColDirective.propDecorators = {
    id: [{ type: Input, args: ['kepsTableCol',] }],
    label: [{ type: Input }],
    width: [{ type: Input }],
    align: [{ type: Input }],
    sortable: [{ type: Input }],
    sortAccessor: [{ type: Input }],
    sortStart: [{ type: Input }]
};
if (false) {
    /**
     * The ID to be used to display/hide the column.
     * @type {?}
     */
    TableColDirective.prototype.id;
    /**
     * Label to be shown in the column header.
     *
     * If not specified, the table will use the ID as the label.
     * @type {?}
     */
    TableColDirective.prototype.label;
    /**
     * Width of the column in pixels (e.g. "50") or percent (e.g. "50%").
     * @type {?}
     */
    TableColDirective.prototype.width;
    /**
     * Text alignment of the column cells (including the header).
     * @type {?}
     */
    TableColDirective.prototype.align;
    /**
     * Whether the column is sortable or not.
     * @type {?}
     */
    TableColDirective.prototype.sortable;
    /**
     * Field of the table element to be used for sorting.
     * @type {?}
     */
    TableColDirective.prototype.sortAccessor;
    /**
     * The default sort direction of the column.
     * @type {?}
     */
    TableColDirective.prototype.sortStart;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUtY29sLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50L3RhYmxlL3RhYmxlLWNvbC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFlLE1BQU0sZUFBZSxDQUFDOzs7Ozs7QUFNOUQsa0NBR0M7OztJQUZDLDRCQUF3Qjs7SUFDeEIsZ0NBQTJCOzs7OztBQVM3QixNQUFNLE9BQU8saUJBQWlCOzs7O0lBeUM1QixnQkFBZ0IsQ0FBQzs7O1lBNUNsQixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjthQUMzQjs7Ozs7aUJBS0UsS0FBSyxTQUFDLGNBQWM7b0JBT3BCLEtBQUs7b0JBS0wsS0FBSztvQkFLTCxLQUFLO3VCQUtMLEtBQUs7MkJBS0wsS0FBSzt3QkFLTCxLQUFLOzs7Ozs7O0lBaENOLCtCQUFrQzs7Ozs7OztJQU9sQyxrQ0FBdUI7Ozs7O0lBS3ZCLGtDQUFnQzs7Ozs7SUFLaEMsa0NBQXVCOzs7OztJQUt2QixxQ0FBMkI7Ozs7O0lBSzNCLHlDQUE4Qjs7Ozs7SUFLOUIsc0NBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBJbnB1dCwgVGVtcGxhdGVSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8qKlxyXG4gKiBSZXByZXNlbnRzIGEgY3VzdG9tIGNvbHVtbiBkYXRhICYgdGVtcGxhdGUuXHJcbiAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBDdXN0b21Db2x1bW4ge1xyXG4gIGRhdGE6IFRhYmxlQ29sRGlyZWN0aXZlO1xyXG4gIHRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xyXG59XHJcblxyXG4vKipcclxuICogQSBkaXJlY3RpdmUgdG8gY3JlYXRlIGN1c3RvbSBjb2x1bW4gb2YgYSBLZXBzIFRhYmxlLlxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6ICdba2Vwc1RhYmxlQ29sXSdcclxufSlcclxuZXhwb3J0IGNsYXNzIFRhYmxlQ29sRGlyZWN0aXZlIHtcclxuICAvKipcclxuICAgKiBUaGUgSUQgdG8gYmUgdXNlZCB0byBkaXNwbGF5L2hpZGUgdGhlIGNvbHVtbi5cclxuICAgKi9cclxuICBASW5wdXQoJ2tlcHNUYWJsZUNvbCcpIGlkOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIExhYmVsIHRvIGJlIHNob3duIGluIHRoZSBjb2x1bW4gaGVhZGVyLlxyXG4gICAqXHJcbiAgICogSWYgbm90IHNwZWNpZmllZCwgdGhlIHRhYmxlIHdpbGwgdXNlIHRoZSBJRCBhcyB0aGUgbGFiZWwuXHJcbiAgICovXHJcbiAgQElucHV0KCkgbGFiZWw6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogV2lkdGggb2YgdGhlIGNvbHVtbiBpbiBwaXhlbHMgKGUuZy4gXCI1MFwiKSBvciBwZXJjZW50IChlLmcuIFwiNTAlXCIpLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHdpZHRoOiBudW1iZXIgfCBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIFRleHQgYWxpZ25tZW50IG9mIHRoZSBjb2x1bW4gY2VsbHMgKGluY2x1ZGluZyB0aGUgaGVhZGVyKS5cclxuICAgKi9cclxuICBASW5wdXQoKSBhbGlnbjogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBjb2x1bW4gaXMgc29ydGFibGUgb3Igbm90LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHNvcnRhYmxlOiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBGaWVsZCBvZiB0aGUgdGFibGUgZWxlbWVudCB0byBiZSB1c2VkIGZvciBzb3J0aW5nLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHNvcnRBY2Nlc3Nvcjogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgZGVmYXVsdCBzb3J0IGRpcmVjdGlvbiBvZiB0aGUgY29sdW1uLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHNvcnRTdGFydDogJ2FzYycgfCAnZGVzYyc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG59XHJcbiJdfQ==