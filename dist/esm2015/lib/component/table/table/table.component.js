/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter, ViewChild, ContentChildren, QueryList, TemplateRef, IterableDiffers, Inject } from '@angular/core';
import { MatTable, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { TableColDirective } from '../table-col.directive';
import { EvalPipe } from '../../../pipe/eval.pipe';
import { isType, getReferenceDisplay, getReferenceDisplayArray } from '../../../utils';
export class TableComponent {
    /**
     * \@internal Do not use!
     * @param {?} models
     * @param {?} iterableDiffers
     */
    constructor(models, iterableDiffers) {
        this.models = models;
        this.iterableDiffers = iterableDiffers;
        /**
         * Data to be shown on the table.
         */
        this.data = [];
        /**
         * Columns to be displayed on the table.
         */
        this.displayedColumns = [];
        /**
         * Columns to be hidden on the table.
         */
        this.hiddenColumns = [];
        /**
         * Custom function to filter the data source
         * See example below:
         * <pre>
         *  customFilter(Data: T, Filter: string): boolean {
         *     return <true if Data matches filter>
         *  }
         * <pre>
         *
         * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
         */
        this.filterPredicate = null;
        /**
         * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
         * \@internal Do not use!
         */
        this.allColumns = [];
        /**
         * A record of custom column data and template keyed by the custom column ID.
         */
        this.customCols = {};
        /**
         * The table's default paginator page size.
         */
        this.defaultPageSize = 10;
        /**
         * Whether the table should display sort on headers or not.
         */
        this.showSort = false;
        /**
         * Whether the table should show the default search box or not.
         */
        this.showSearch = false;
        /**
         * The search query to be used to filter the data.
         */
        this.search = '';
        /**
         * An event emitter that emits the object of the row clicked by the user.
         */
        this.rowClick = new EventEmitter();
        /**
         * Whether the table has done initializing or not.
         * \@readonly
         */
        this.initialized = false;
        this.diff = this.iterableDiffers.find([]).create(null);
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        // Initialize the data source.
        this.dataSource = new MatTableDataSource(this.data);
        // Initialize the paginator.
        if (this.showPaginator) {
            this.dataSource.paginator = this.paginator;
            // Get the previous session's page size if any, or use the default page size.
            this.pageSize = Number(localStorage.getItem(this.savePageSize)) || this.defaultPageSize;
        }
        // Initialize the sort.
        if (this.showSort) {
            this.dataSource.sort = this.sort;
            // Modify the data source's `sortingDataAccessor` to use `EvalPipe` for custom columns.
            this.evalPipe = new EvalPipe();
            this.dataSource.sortingDataAccessor = (/**
             * @param {?} data
             * @param {?} sortHeaderId
             * @return {?}
             */
            (data, sortHeaderId) => {
                /** @type {?} */
                const customCol = this.customCols[sortHeaderId];
                if (customCol && customCol.data.sortAccessor) {
                    return this.evalPipe.transform(data, customCol.data.sortAccessor);
                }
                return data[sortHeaderId];
            });
        }
        this.setFilterPredicate(this.filterPredicate);
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngAfterContentInit() {
        // We are using `ngAfterContentInit` here because we can
        // only get the custom columns data after the component content
        // has been initialized.
        // Get schema from model name if supplied.
        if (this.model && this.models[this.model]) {
            this.schema = this.models[this.model].schema;
        }
        else if (this.model && !this.models[this.model]) {
            throw new Error(`Can't find a model named ${this.model}!`);
        }
        // Assert schema must exist.
        if (!this.schema) {
            throw new Error(`Must supply valid model name or schema to Keps table!`);
        }
        // Set `allColumns` to be every public field in the schema.
        this.allColumns = Object.keys(this.schema).filter((/**
         * @param {?} field
         * @return {?}
         */
        field => field[0] !== '_'));
        // Process custom columns.
        /** @type {?} */
        const colDatas = this.customColDatas.toArray();
        /** @type {?} */
        const colTemplates = this.customColTemplates.toArray();
        for (let i = 0; i < colDatas.length; i++) {
            /** @type {?} */
            const colId = colDatas[i].id;
            this.customCols[colId] = {
                data: colDatas[i],
                template: colTemplates[i]
            };
            this.allColumns.push(colId);
        }
        // If `displayedColumns` is not provided, use `allColumns` filtered by `hiddenColumns`.
        if (this.displayedColumns.length === 0) {
            this.displayedColumns = this.allColumns.filter((/**
             * @param {?} field
             * @return {?}
             */
            field => !this.hiddenColumns.includes(field)));
        }
        this.initialized = true;
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngDoCheck() {
        // This will refresh the table data source and rerender
        // the table if there is any change to the data.
        if (this.initialized) {
            /** @type {?} */
            const changes = this.diff.diff(this.data);
            if (changes) {
                this.dataSource.data = this.data;
                this.table.renderRows();
            }
        }
    }
    /**
     * \@internal Do not use!
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        // This check if the `search` property has changed and apply search if it does.
        if (this.initialized && changes.search) {
            this.applySearch(this.search);
        }
    }
    /**
     * Saves current page size to local storage if property `savePageSize` is supplied.
     * \@internal Do not use!
     * @param {?} event Event from paginator.
     * @return {?}
     */
    onPageChange(event) {
        if (this.savePageSize) {
            localStorage.setItem(this.savePageSize, event.pageSize.toString());
        }
    }
    /**
     * Applies a search query to filter the data.
     * @param {?} search Query for the search.
     * @return {?}
     */
    applySearch(search) {
        this.dataSource.filter = search.trim().toLowerCase();
    }
    /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    isType(field, type) {
        return isType(this.schema, field, type);
    }
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplay(field) {
        return getReferenceDisplay(this.models, this.schema, field);
    }
    /**
     * Returns the display expression of an array of references field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplayArray(field) {
        return getReferenceDisplayArray(this.models, this.schema, field);
    }
    /**
     * Set the filter predicate of data source.
     * this function accepts a function as a parameter.
     * See example below:
     * <pre>
     *  customFilter(Data: T, Filter: string): boolean {
     *     return <true if Data matches filter>
     *  }
     * <pre>
     * If no funtion is provided, this function will use the default
     * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
     * @param {?=} filterPredicateFunction custom filterPredicateFunction that follows the above format
     * @return {?}
     */
    setFilterPredicate(filterPredicateFunction = null) {
        if (filterPredicateFunction) {
            this.dataSource.filterPredicate = filterPredicateFunction;
        }
    }
}
TableComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-table',
                template: "<!-- Table's Search Box -->\r\n<mat-form-field\r\n  *ngIf=\"showSearch\"\r\n  floatLabel=\"never\"\r\n  class=\"keps-table-search\"\r\n>\r\n  <input\r\n    matInput\r\n    (keyup)=\"applySearch($event.target.value)\"\r\n    placeholder=\"Search\"\r\n  >\r\n</mat-form-field>\r\n\r\n<!-- Table -->\r\n<table\r\n  mat-table [dataSource]=\"dataSource\"\r\n  matSort [matSortDisabled]=\"!showSort\" [matSortActive]=\"activeSort\" matSortDirection=\"asc\"\r\n  class=\"keps-table\"\r\n>\r\n  <!-- Table Columns from Schema -->\r\n  <ng-container\r\n    *ngFor=\"let fieldName of schema | keys\"\r\n    [matColumnDef]=\"fieldName\"\r\n  >\r\n    <ng-container *ngIf=\"schema[fieldName]; let field\">\r\n      <th mat-header-cell *matHeaderCellDef mat-sort-header [ngClass]=\"'keps-table-col-' + fieldName\" >\r\n        {{ (field.label ? field.label : '') }}{{ (field.label ?  '' : fieldName) | splitCamel | titlecase }}\r\n      </th>\r\n      <td \r\n        mat-cell *matCellDef=\"let element\" \r\n        class=\"keps-table-cell\"\r\n        [ngClass]=\"'keps-table-col-' + fieldName\"\r\n      >\r\n        <ng-container *ngIf=\"element[fieldName]; let fieldData\">\r\n          <span *ngIf=\"isType(fieldName, 'reference')\">\r\n            {{ fieldData | eval:getReferenceDisplay(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'address')\">\r\n            {{ fieldData | address:4 }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'datetime')\">\r\n            {{ fieldData | moment:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-reference')\">\r\n            {{ fieldData | arrayDisplay:', ':getReferenceDisplayArray(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-string')\">\r\n            {{ fieldData | arrayDisplay:', ' }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'number')\">\r\n            {{ fieldData | numberFormat:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'enum')\">\r\n            {{ field.labels && field.labels[fieldData] ? field.labels[fieldData] : fieldData }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'table-normal')\">\r\n            {{ fieldData }}\r\n          </span>\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Table Columns from Custom Columns -->\r\n  <ng-container\r\n    *ngFor=\"let colName of customCols | keys\"\r\n    [matColumnDef]=\"colName\"\r\n  >\r\n    <ng-container *ngIf=\"customCols[colName].data; let colData\">\r\n      <th\r\n        mat-header-cell *matHeaderCellDef\r\n        [width]=\"colData.width\"\r\n        [align]=\"colData.align\"\r\n        mat-sort-header [disabled]=\"!colData.sortable\" [start]=\"colData.sortStart\"\r\n      >\r\n        {{ (colData.label || colName) | splitCamel | titlecase }}\r\n      </th>\r\n\r\n      <td\r\n        mat-cell *matCellDef=\"let element; let i = index;\"\r\n        [align]=\"colData.align\"\r\n        class=\"keps-table-column\"\r\n      >\r\n        <ng-container\r\n          *ngTemplateOutlet=\"customCols[colName].template; context: { $implicit: element, index: i, element: element }\"\r\n        >\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Rows -->\r\n  <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n  <tr\r\n    mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n    (click)=\"rowClick.emit(row)\"\r\n    [class.clickable-row]=\"rowClick.observers.length > 0\"\r\n  >\r\n  </tr>\r\n</table>\r\n\r\n<!-- Empty Table Row -->\r\n<mat-card *ngIf=\"data.length === 0 || dataSource.filteredData.length === 0\" class=\"empty-table\">\r\n  No Data.\r\n</mat-card>\r\n\r\n<!-- Paginator -->\r\n<mat-paginator\r\n  [class.hide-paginator]=\"!showPaginator\"\r\n  [pageSizeOptions]=\"[10, 25, 50, 100]\"\r\n  [pageSize]=\"pageSize\"\r\n  (page)=\"onPageChange($event)\"\r\n  showFirstLastButtons\r\n  class=\"keps-paginator\"\r\n>\r\n</mat-paginator>\r\n",
                styles: [":host{display:block;position:relative}.keps-table{width:100%}.empty-table,.keps-paginator,.keps-table{background:0 0!important}.keps-table-cell{white-space:pre-line}.keps-table-cell *{display:block;margin:16px 0}.empty-table{border-radius:0;text-align:center;box-shadow:none!important}.hide-paginator{display:none!important}.keps-table-search{width:95%;margin:0 2.5%}.clickable-row:hover{background:rgba(0,0,0,.05);cursor:pointer}"]
            }] }
];
/** @nocollapse */
TableComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] },
    { type: IterableDiffers }
];
TableComponent.propDecorators = {
    model: [{ type: Input }],
    schema: [{ type: Input }],
    table: [{ type: ViewChild, args: [MatTable, { static: true },] }],
    data: [{ type: Input }],
    displayedColumns: [{ type: Input }],
    hiddenColumns: [{ type: Input }],
    filterPredicate: [{ type: Input }],
    customColDatas: [{ type: ContentChildren, args: [TableColDirective,] }],
    customColTemplates: [{ type: ContentChildren, args: [TableColDirective, { read: TemplateRef },] }],
    paginator: [{ type: ViewChild, args: [MatPaginator, { static: true },] }],
    showPaginator: [{ type: Input }],
    savePageSize: [{ type: Input }],
    defaultPageSize: [{ type: Input }],
    sort: [{ type: ViewChild, args: [MatSort, { static: true },] }],
    showSort: [{ type: Input }],
    activeSort: [{ type: Input }],
    showSearch: [{ type: Input }],
    search: [{ type: Input }],
    rowClick: [{ type: Output }]
};
if (false) {
    /**
     * Name of the model for the table.
     * @type {?}
     */
    TableComponent.prototype.model;
    /**
     * Schema of the model for the table.
     *
     * If the model name is supplied,
     * then the table will get the schema from that model.
     * @type {?}
     */
    TableComponent.prototype.schema;
    /**
     * Reference to the MatTable.
     * @type {?}
     * @private
     */
    TableComponent.prototype.table;
    /**
     * Data to be shown on the table.
     * @type {?}
     */
    TableComponent.prototype.data;
    /**
     * Wrapper for the data using MatTableDataSource.
     *
     * Do not change this. Use the `data` property instead to change the data.
     *
     * \@internal Do not use!
     * @type {?}
     */
    TableComponent.prototype.dataSource;
    /**
     * Columns to be displayed on the table.
     * @type {?}
     */
    TableComponent.prototype.displayedColumns;
    /**
     * Columns to be hidden on the table.
     * @type {?}
     */
    TableComponent.prototype.hiddenColumns;
    /**
     * Custom function to filter the data source
     * See example below:
     * <pre>
     *  customFilter(Data: T, Filter: string): boolean {
     *     return <true if Data matches filter>
     *  }
     * <pre>
     *
     * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
     * @type {?}
     */
    TableComponent.prototype.filterPredicate;
    /**
     * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
     * \@internal Do not use!
     * @type {?}
     */
    TableComponent.prototype.allColumns;
    /**
     * Queries all `TableColDirective` data inside the component.
     * @type {?}
     * @private
     */
    TableComponent.prototype.customColDatas;
    /**
     * Queries all `TableColDirective` templates inside the component.
     * @type {?}
     * @private
     */
    TableComponent.prototype.customColTemplates;
    /**
     * A record of custom column data and template keyed by the custom column ID.
     * @type {?}
     */
    TableComponent.prototype.customCols;
    /**
     * Reference to the MatPaginator.
     * @type {?}
     * @private
     */
    TableComponent.prototype.paginator;
    /**
     * Whether the table should display the paginator or not.
     * @type {?}
     */
    TableComponent.prototype.showPaginator;
    /**
     * A key used when saving page size number to the local storage.
     * @type {?}
     */
    TableComponent.prototype.savePageSize;
    /**
     * The table's default paginator page size.
     * @type {?}
     */
    TableComponent.prototype.defaultPageSize;
    /**
     * The current page size on paginator.
     * @type {?}
     */
    TableComponent.prototype.pageSize;
    /**
     * Reference to the MatSort.
     * @type {?}
     * @private
     */
    TableComponent.prototype.sort;
    /**
     * Whether the table should display sort on headers or not.
     * @type {?}
     */
    TableComponent.prototype.showSort;
    /**
     * The ID of the column that should be sorted.
     * @type {?}
     */
    TableComponent.prototype.activeSort;
    /**
     * Whether the table should show the default search box or not.
     * @type {?}
     */
    TableComponent.prototype.showSearch;
    /**
     * The search query to be used to filter the data.
     * @type {?}
     */
    TableComponent.prototype.search;
    /**
     * An event emitter that emits the object of the row clicked by the user.
     * @type {?}
     */
    TableComponent.prototype.rowClick;
    /**
     * An iterable differ instance to track changes to the data.
     * @type {?}
     * @private
     */
    TableComponent.prototype.diff;
    /**
     * Instance of the `EvalPipe` to be used for custom columns sorting.
     * @type {?}
     * @private
     */
    TableComponent.prototype.evalPipe;
    /**
     * Whether the table has done initializing or not.
     * \@readonly
     * @type {?}
     */
    TableComponent.prototype.initialized;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype.models;
    /**
     * @type {?}
     * @private
     */
    TableComponent.prototype.iterableDiffers;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvdGFibGUvdGFibGUvdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQU1ULEtBQUssRUFDTCxNQUFNLEVBQ04sWUFBWSxFQUNaLFNBQVMsRUFDVCxlQUFlLEVBQ2YsU0FBUyxFQUNULFdBQVcsRUFFWCxlQUFlLEVBQ2YsTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFDTCxRQUFRLEVBQ1Isa0JBQWtCLEVBQ2xCLFlBQVksRUFFWixPQUFPLEVBQ1IsTUFBTSxtQkFBbUIsQ0FBQztBQUMzQixPQUFPLEVBQUUsaUJBQWlCLEVBQWdCLE1BQU0sd0JBQXdCLENBQUM7QUFFekUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ25ELE9BQU8sRUFBRSxNQUFNLEVBQUUsbUJBQW1CLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU92RixNQUFNLE9BQU8sY0FBYzs7Ozs7O0lBd0p6QixZQUM0QixNQUFrQixFQUNwQyxlQUFnQztRQURkLFdBQU0sR0FBTixNQUFNLENBQVk7UUFDcEMsb0JBQWUsR0FBZixlQUFlLENBQWlCOzs7O1FBcElqQyxTQUFJLEdBQWlCLEVBQUUsQ0FBQzs7OztRQWN4QixxQkFBZ0IsR0FBYSxFQUFFLENBQUM7Ozs7UUFLaEMsa0JBQWEsR0FBYSxFQUFFLENBQUM7Ozs7Ozs7Ozs7OztRQWM3QixvQkFBZSxHQUFhLElBQUksQ0FBQzs7Ozs7UUFNMUMsZUFBVSxHQUFhLEVBQUUsQ0FBQzs7OztRQWUxQixlQUFVLEdBQWlDLEVBQUUsQ0FBQzs7OztRQW9CckMsb0JBQWUsR0FBRyxFQUFFLENBQUM7Ozs7UUFlckIsYUFBUSxHQUFHLEtBQUssQ0FBQzs7OztRQVVqQixlQUFVLEdBQUcsS0FBSyxDQUFDOzs7O1FBS25CLFdBQU0sR0FBRyxFQUFFLENBQUM7Ozs7UUFLWCxhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQWMsQ0FBQzs7Ozs7UUFnQnBELGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBU2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3pELENBQUM7Ozs7O0lBS0QsUUFBUTtRQUNOLDhCQUE4QjtRQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWhFLDRCQUE0QjtRQUM1QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUUzQyw2RUFBNkU7WUFDN0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDO1NBQ3pGO1FBRUQsdUJBQXVCO1FBQ3ZCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBRWpDLHVGQUF1RjtZQUN2RixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUI7Ozs7O1lBQUcsQ0FBQyxJQUFnQixFQUFFLFlBQW9CLEVBQUUsRUFBRTs7c0JBQ3pFLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztnQkFDL0MsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQzVDLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7aUJBQ25FO2dCQUNELE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzVCLENBQUMsQ0FBQSxDQUFDO1NBQ0g7UUFDRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7Ozs7O0lBS0Qsa0JBQWtCO1FBQ2hCLHdEQUF3RDtRQUN4RCwrREFBK0Q7UUFDL0Qsd0JBQXdCO1FBRXhCLDBDQUEwQztRQUMxQyxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUM7U0FDOUM7YUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqRCxNQUFNLElBQUksS0FBSyxDQUFDLDRCQUE0QixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztTQUM1RDtRQUVELDRCQUE0QjtRQUM1QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLHVEQUF1RCxDQUFDLENBQUM7U0FDMUU7UUFFRCwyREFBMkQ7UUFDM0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFDLENBQUM7OztjQUd2RSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUU7O2NBQ3hDLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFO1FBQ3RELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztrQkFDbEMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUc7Z0JBQ3ZCLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNqQixRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQzthQUMxQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0I7UUFFRCx1RkFBdUY7UUFDdkYsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN0QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUM7U0FDOUY7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDOzs7OztJQUtELFNBQVM7UUFDUCx1REFBdUQ7UUFDdkQsZ0RBQWdEO1FBQ2hELElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTs7a0JBQ2QsT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDekMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDakMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQzthQUN6QjtTQUNGO0lBQ0gsQ0FBQzs7Ozs7O0lBS0QsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLCtFQUErRTtRQUMvRSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksT0FBTyxDQUFDLE1BQU0sRUFBRTtZQUN0QyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMvQjtJQUNILENBQUM7Ozs7Ozs7SUFPRCxZQUFZLENBQUMsS0FBZ0I7UUFDM0IsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7U0FDcEU7SUFDSCxDQUFDOzs7Ozs7SUFNRCxXQUFXLENBQUMsTUFBYztRQUN4QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDdkQsQ0FBQzs7Ozs7Ozs7SUFRRCxNQUFNLENBQUMsS0FBYSxFQUFFLElBQVk7UUFDaEMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7OztJQU9ELG1CQUFtQixDQUFDLEtBQWE7UUFDL0IsT0FBTyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7OztJQU9ELHdCQUF3QixDQUFDLEtBQWE7UUFDcEMsT0FBTyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDbkUsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7O0lBZ0JELGtCQUFrQixDQUFDLHVCQUF1QixHQUFFLElBQUk7UUFDOUMsSUFBSSx1QkFBdUIsRUFBRTtZQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsR0FBRyx1QkFBdUIsQ0FBQztTQUMzRDtJQUNILENBQUM7OztZQXpVRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLDhpSUFBcUM7O2FBRXRDOzs7OzRDQTBKSSxNQUFNLFNBQUMsUUFBUTtZQTdLbEIsZUFBZTs7O29CQXdCZCxLQUFLO3FCQVFMLEtBQUs7b0JBS0wsU0FBUyxTQUFDLFFBQVEsRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7bUJBS3BDLEtBQUs7K0JBY0wsS0FBSzs0QkFLTCxLQUFLOzhCQWNMLEtBQUs7NkJBV0wsZUFBZSxTQUFDLGlCQUFpQjtpQ0FLakMsZUFBZSxTQUFDLGlCQUFpQixFQUFFLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRTt3QkFVeEQsU0FBUyxTQUFDLFlBQVksRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7NEJBS3hDLEtBQUs7MkJBS0wsS0FBSzs4QkFLTCxLQUFLO21CQVVMLFNBQVMsU0FBQyxPQUFPLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFO3VCQUtuQyxLQUFLO3lCQUtMLEtBQUs7eUJBS0wsS0FBSztxQkFLTCxLQUFLO3VCQUtMLE1BQU07Ozs7Ozs7SUEvSFAsK0JBQXVCOzs7Ozs7OztJQVF2QixnQ0FBNEI7Ozs7OztJQUs1QiwrQkFBb0U7Ozs7O0lBS3BFLDhCQUFpQzs7Ozs7Ozs7O0lBU2pDLG9DQUEyQzs7Ozs7SUFLM0MsMENBQXlDOzs7OztJQUt6Qyx1Q0FBc0M7Ozs7Ozs7Ozs7Ozs7SUFjdEMseUNBQTBDOzs7Ozs7SUFNMUMsb0NBQTBCOzs7Ozs7SUFLMUIsd0NBQXlGOzs7Ozs7SUFLekYsNENBQW1IOzs7OztJQUtuSCxvQ0FBOEM7Ozs7OztJQUs5QyxtQ0FBMkU7Ozs7O0lBSzNFLHVDQUE4Qjs7Ozs7SUFLOUIsc0NBQThCOzs7OztJQUs5Qix5Q0FBOEI7Ozs7O0lBSzlCLGtDQUFpQjs7Ozs7O0lBS2pCLDhCQUE0RDs7Ozs7SUFLNUQsa0NBQTBCOzs7OztJQUsxQixvQ0FBNEI7Ozs7O0lBSzVCLG9DQUE0Qjs7Ozs7SUFLNUIsZ0NBQXFCOzs7OztJQUtyQixrQ0FBb0Q7Ozs7OztJQUtwRCw4QkFBa0M7Ozs7OztJQUtsQyxrQ0FBMkI7Ozs7OztJQU0zQixxQ0FBb0I7Ozs7O0lBTWxCLGdDQUE0Qzs7Ozs7SUFDNUMseUNBQXdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25Jbml0LFxyXG4gIEFmdGVyQ29udGVudEluaXQsXHJcbiAgRG9DaGVjayxcclxuICBPbkNoYW5nZXMsXHJcbiAgU2ltcGxlQ2hhbmdlcyxcclxuICBJbnB1dCxcclxuICBPdXRwdXQsXHJcbiAgRXZlbnRFbWl0dGVyLFxyXG4gIFZpZXdDaGlsZCxcclxuICBDb250ZW50Q2hpbGRyZW4sXHJcbiAgUXVlcnlMaXN0LFxyXG4gIFRlbXBsYXRlUmVmLFxyXG4gIEl0ZXJhYmxlRGlmZmVyLFxyXG4gIEl0ZXJhYmxlRGlmZmVycyxcclxuICBJbmplY3RcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICBNYXRUYWJsZSxcclxuICBNYXRUYWJsZURhdGFTb3VyY2UsXHJcbiAgTWF0UGFnaW5hdG9yLFxyXG4gIFBhZ2VFdmVudCxcclxuICBNYXRTb3J0XHJcbn0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBUYWJsZUNvbERpcmVjdGl2ZSwgQ3VzdG9tQ29sdW1uIH0gZnJvbSAnLi4vdGFibGUtY29sLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IEtlcHNTY2hlbWEsIEtlcHNNb2RlbHMsIEtlcHNPYmplY3QgfSBmcm9tICcuLi8uLi8uLi9pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBFdmFsUGlwZSB9IGZyb20gJy4uLy4uLy4uL3BpcGUvZXZhbC5waXBlJztcclxuaW1wb3J0IHsgaXNUeXBlLCBnZXRSZWZlcmVuY2VEaXNwbGF5LCBnZXRSZWZlcmVuY2VEaXNwbGF5QXJyYXkgfSBmcm9tICcuLi8uLi8uLi91dGlscyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2tlcHMtdGFibGUnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi90YWJsZS5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vdGFibGUuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyQ29udGVudEluaXQsIERvQ2hlY2ssIE9uQ2hhbmdlcyB7XHJcbiAgLyoqXHJcbiAgICogTmFtZSBvZiB0aGUgbW9kZWwgZm9yIHRoZSB0YWJsZS5cclxuICAgKi9cclxuICBASW5wdXQoKSBtb2RlbDogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBTY2hlbWEgb2YgdGhlIG1vZGVsIGZvciB0aGUgdGFibGUuXHJcbiAgICpcclxuICAgKiBJZiB0aGUgbW9kZWwgbmFtZSBpcyBzdXBwbGllZCxcclxuICAgKiB0aGVuIHRoZSB0YWJsZSB3aWxsIGdldCB0aGUgc2NoZW1hIGZyb20gdGhhdCBtb2RlbC5cclxuICAgKi9cclxuICBASW5wdXQoKSBzY2hlbWE6IEtlcHNTY2hlbWE7XHJcblxyXG4gIC8qKlxyXG4gICAqIFJlZmVyZW5jZSB0byB0aGUgTWF0VGFibGUuXHJcbiAgICovXHJcbiAgQFZpZXdDaGlsZChNYXRUYWJsZSwgeyBzdGF0aWM6IHRydWUgfSkgcHJpdmF0ZSB0YWJsZTogTWF0VGFibGU8YW55PjtcclxuXHJcbiAgLyoqXHJcbiAgICogRGF0YSB0byBiZSBzaG93biBvbiB0aGUgdGFibGUuXHJcbiAgICovXHJcbiAgQElucHV0KCkgZGF0YTogS2Vwc09iamVjdFtdID0gW107XHJcblxyXG4gIC8qKlxyXG4gICAqIFdyYXBwZXIgZm9yIHRoZSBkYXRhIHVzaW5nIE1hdFRhYmxlRGF0YVNvdXJjZS5cclxuICAgKlxyXG4gICAqIERvIG5vdCBjaGFuZ2UgdGhpcy4gVXNlIHRoZSBgZGF0YWAgcHJvcGVydHkgaW5zdGVhZCB0byBjaGFuZ2UgdGhlIGRhdGEuXHJcbiAgICpcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBkYXRhU291cmNlOiBNYXRUYWJsZURhdGFTb3VyY2U8S2Vwc09iamVjdD47XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbHVtbnMgdG8gYmUgZGlzcGxheWVkIG9uIHRoZSB0YWJsZS5cclxuICAgKi9cclxuICBASW5wdXQoKSBkaXNwbGF5ZWRDb2x1bW5zOiBzdHJpbmdbXSA9IFtdO1xyXG5cclxuICAvKipcclxuICAgKiBDb2x1bW5zIHRvIGJlIGhpZGRlbiBvbiB0aGUgdGFibGUuXHJcbiAgICovXHJcbiAgQElucHV0KCkgaGlkZGVuQ29sdW1uczogc3RyaW5nW10gPSBbXTtcclxuXHJcblxyXG4gIC8qKlxyXG4gICAqIEN1c3RvbSBmdW5jdGlvbiB0byBmaWx0ZXIgdGhlIGRhdGEgc291cmNlXHJcbiAgICogU2VlIGV4YW1wbGUgYmVsb3c6XHJcbiAgICogPHByZT5cclxuICAgKiAgY3VzdG9tRmlsdGVyKERhdGE6IFQsIEZpbHRlcjogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICogICAgIHJldHVybiA8dHJ1ZSBpZiBEYXRhIG1hdGNoZXMgZmlsdGVyPlxyXG4gICAqICB9XHJcbiAgICogPHByZT5cclxuICAgKiBcclxuICAgKiBAc2VlIDxhIGhyZWY9XCJodHRwczovL21hdGVyaWFsLmFuZ3VsYXIuaW8vY29tcG9uZW50cy90YWJsZS9hcGlcIj5Bbmd1bGFyIFRhYmxlPC9hPiBmb3IgbW9yZSBpbmZvLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGZpbHRlclByZWRpY2F0ZTogRnVuY3Rpb24gPSBudWxsO1xyXG5cclxuICAvKipcclxuICAgKiBDb2x1bW5zIHRvIGJlIGRpc3BsYXllZCBvbiB0aGUgdGFibGUgYmFzZWQgb24gZGlzcGxheWVkQ29sdW1ucyBhbmQgaGlkZGVuQ29sdW1ucy5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBhbGxDb2x1bW5zOiBzdHJpbmdbXSA9IFtdO1xyXG5cclxuICAvKipcclxuICAgKiBRdWVyaWVzIGFsbCBgVGFibGVDb2xEaXJlY3RpdmVgIGRhdGEgaW5zaWRlIHRoZSBjb21wb25lbnQuXHJcbiAgICovXHJcbiAgQENvbnRlbnRDaGlsZHJlbihUYWJsZUNvbERpcmVjdGl2ZSkgcHJpdmF0ZSBjdXN0b21Db2xEYXRhczogUXVlcnlMaXN0PFRhYmxlQ29sRGlyZWN0aXZlPjtcclxuXHJcbiAgLyoqXHJcbiAgICogUXVlcmllcyBhbGwgYFRhYmxlQ29sRGlyZWN0aXZlYCB0ZW1wbGF0ZXMgaW5zaWRlIHRoZSBjb21wb25lbnQuXHJcbiAgICovXHJcbiAgQENvbnRlbnRDaGlsZHJlbihUYWJsZUNvbERpcmVjdGl2ZSwgeyByZWFkOiBUZW1wbGF0ZVJlZiB9KSBwcml2YXRlIGN1c3RvbUNvbFRlbXBsYXRlczogUXVlcnlMaXN0PFRlbXBsYXRlUmVmPGFueT4+O1xyXG5cclxuICAvKipcclxuICAgKiBBIHJlY29yZCBvZiBjdXN0b20gY29sdW1uIGRhdGEgYW5kIHRlbXBsYXRlIGtleWVkIGJ5IHRoZSBjdXN0b20gY29sdW1uIElELlxyXG4gICAqL1xyXG4gIGN1c3RvbUNvbHM6IFJlY29yZDxzdHJpbmcsIEN1c3RvbUNvbHVtbj4gPSB7fTtcclxuXHJcbiAgLyoqXHJcbiAgICogUmVmZXJlbmNlIHRvIHRoZSBNYXRQYWdpbmF0b3IuXHJcbiAgICovXHJcbiAgQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIHRhYmxlIHNob3VsZCBkaXNwbGF5IHRoZSBwYWdpbmF0b3Igb3Igbm90LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHNob3dQYWdpbmF0b3I6IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBBIGtleSB1c2VkIHdoZW4gc2F2aW5nIHBhZ2Ugc2l6ZSBudW1iZXIgdG8gdGhlIGxvY2FsIHN0b3JhZ2UuXHJcbiAgICovXHJcbiAgQElucHV0KCkgc2F2ZVBhZ2VTaXplOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSB0YWJsZSdzIGRlZmF1bHQgcGFnaW5hdG9yIHBhZ2Ugc2l6ZS5cclxuICAgKi9cclxuICBASW5wdXQoKSBkZWZhdWx0UGFnZVNpemUgPSAxMDtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIGN1cnJlbnQgcGFnZSBzaXplIG9uIHBhZ2luYXRvci5cclxuICAgKi9cclxuICBwYWdlU2l6ZTogbnVtYmVyO1xyXG5cclxuICAvKipcclxuICAgKiBSZWZlcmVuY2UgdG8gdGhlIE1hdFNvcnQuXHJcbiAgICovXHJcbiAgQFZpZXdDaGlsZChNYXRTb3J0LCB7IHN0YXRpYzogdHJ1ZSB9KSBwcml2YXRlIHNvcnQ6IE1hdFNvcnQ7XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIHRhYmxlIHNob3VsZCBkaXNwbGF5IHNvcnQgb24gaGVhZGVycyBvciBub3QuXHJcbiAgICovXHJcbiAgQElucHV0KCkgc2hvd1NvcnQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIElEIG9mIHRoZSBjb2x1bW4gdGhhdCBzaG91bGQgYmUgc29ydGVkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGFjdGl2ZVNvcnQ6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgdGFibGUgc2hvdWxkIHNob3cgdGhlIGRlZmF1bHQgc2VhcmNoIGJveCBvciBub3QuXHJcbiAgICovXHJcbiAgQElucHV0KCkgc2hvd1NlYXJjaCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgc2VhcmNoIHF1ZXJ5IHRvIGJlIHVzZWQgdG8gZmlsdGVyIHRoZSBkYXRhLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHNlYXJjaCA9ICcnO1xyXG5cclxuICAvKipcclxuICAgKiBBbiBldmVudCBlbWl0dGVyIHRoYXQgZW1pdHMgdGhlIG9iamVjdCBvZiB0aGUgcm93IGNsaWNrZWQgYnkgdGhlIHVzZXIuXHJcbiAgICovXHJcbiAgQE91dHB1dCgpIHJvd0NsaWNrID0gbmV3IEV2ZW50RW1pdHRlcjxLZXBzT2JqZWN0PigpO1xyXG5cclxuICAvKipcclxuICAgKiBBbiBpdGVyYWJsZSBkaWZmZXIgaW5zdGFuY2UgdG8gdHJhY2sgY2hhbmdlcyB0byB0aGUgZGF0YS5cclxuICAgKi9cclxuICBwcml2YXRlIGRpZmY6IEl0ZXJhYmxlRGlmZmVyPGFueT47XHJcblxyXG4gIC8qKlxyXG4gICAqIEluc3RhbmNlIG9mIHRoZSBgRXZhbFBpcGVgIHRvIGJlIHVzZWQgZm9yIGN1c3RvbSBjb2x1bW5zIHNvcnRpbmcuXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBldmFsUGlwZTogRXZhbFBpcGU7XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIHRhYmxlIGhhcyBkb25lIGluaXRpYWxpemluZyBvciBub3QuXHJcbiAgICogQHJlYWRvbmx5XHJcbiAgICovXHJcbiAgaW5pdGlhbGl6ZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBASW5qZWN0KCdNT0RFTFMnKSBwcml2YXRlIG1vZGVsczogS2Vwc01vZGVscyxcclxuICAgIHByaXZhdGUgaXRlcmFibGVEaWZmZXJzOiBJdGVyYWJsZURpZmZlcnNcclxuICApIHtcclxuICAgIHRoaXMuZGlmZiA9IHRoaXMuaXRlcmFibGVEaWZmZXJzLmZpbmQoW10pLmNyZWF0ZShudWxsKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgLy8gSW5pdGlhbGl6ZSB0aGUgZGF0YSBzb3VyY2UuXHJcbiAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlPEtlcHNPYmplY3Q+KHRoaXMuZGF0YSk7XHJcblxyXG4gICAgLy8gSW5pdGlhbGl6ZSB0aGUgcGFnaW5hdG9yLlxyXG4gICAgaWYgKHRoaXMuc2hvd1BhZ2luYXRvcikge1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UucGFnaW5hdG9yID0gdGhpcy5wYWdpbmF0b3I7XHJcblxyXG4gICAgICAvLyBHZXQgdGhlIHByZXZpb3VzIHNlc3Npb24ncyBwYWdlIHNpemUgaWYgYW55LCBvciB1c2UgdGhlIGRlZmF1bHQgcGFnZSBzaXplLlxyXG4gICAgICB0aGlzLnBhZ2VTaXplID0gTnVtYmVyKGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMuc2F2ZVBhZ2VTaXplKSkgfHwgdGhpcy5kZWZhdWx0UGFnZVNpemU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gSW5pdGlhbGl6ZSB0aGUgc29ydC5cclxuICAgIGlmICh0aGlzLnNob3dTb3J0KSB7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZS5zb3J0ID0gdGhpcy5zb3J0O1xyXG5cclxuICAgICAgLy8gTW9kaWZ5IHRoZSBkYXRhIHNvdXJjZSdzIGBzb3J0aW5nRGF0YUFjY2Vzc29yYCB0byB1c2UgYEV2YWxQaXBlYCBmb3IgY3VzdG9tIGNvbHVtbnMuXHJcbiAgICAgIHRoaXMuZXZhbFBpcGUgPSBuZXcgRXZhbFBpcGUoKTtcclxuICAgICAgdGhpcy5kYXRhU291cmNlLnNvcnRpbmdEYXRhQWNjZXNzb3IgPSAoZGF0YTogS2Vwc09iamVjdCwgc29ydEhlYWRlcklkOiBzdHJpbmcpID0+IHtcclxuICAgICAgICBjb25zdCBjdXN0b21Db2wgPSB0aGlzLmN1c3RvbUNvbHNbc29ydEhlYWRlcklkXTtcclxuICAgICAgICBpZiAoY3VzdG9tQ29sICYmIGN1c3RvbUNvbC5kYXRhLnNvcnRBY2Nlc3Nvcikge1xyXG4gICAgICAgICAgcmV0dXJuIHRoaXMuZXZhbFBpcGUudHJhbnNmb3JtKGRhdGEsIGN1c3RvbUNvbC5kYXRhLnNvcnRBY2Nlc3Nvcik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBkYXRhW3NvcnRIZWFkZXJJZF07XHJcbiAgICAgIH07XHJcbiAgICB9XHJcbiAgICB0aGlzLnNldEZpbHRlclByZWRpY2F0ZSh0aGlzLmZpbHRlclByZWRpY2F0ZSk7ICAgIFxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgbmdBZnRlckNvbnRlbnRJbml0KCkge1xyXG4gICAgLy8gV2UgYXJlIHVzaW5nIGBuZ0FmdGVyQ29udGVudEluaXRgIGhlcmUgYmVjYXVzZSB3ZSBjYW5cclxuICAgIC8vIG9ubHkgZ2V0IHRoZSBjdXN0b20gY29sdW1ucyBkYXRhIGFmdGVyIHRoZSBjb21wb25lbnQgY29udGVudFxyXG4gICAgLy8gaGFzIGJlZW4gaW5pdGlhbGl6ZWQuXHJcblxyXG4gICAgLy8gR2V0IHNjaGVtYSBmcm9tIG1vZGVsIG5hbWUgaWYgc3VwcGxpZWQuXHJcbiAgICBpZiAodGhpcy5tb2RlbCAmJiB0aGlzLm1vZGVsc1t0aGlzLm1vZGVsXSkge1xyXG4gICAgICB0aGlzLnNjaGVtYSA9IHRoaXMubW9kZWxzW3RoaXMubW9kZWxdLnNjaGVtYTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5tb2RlbCAmJiAhdGhpcy5tb2RlbHNbdGhpcy5tb2RlbF0pIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKGBDYW4ndCBmaW5kIGEgbW9kZWwgbmFtZWQgJHt0aGlzLm1vZGVsfSFgKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBBc3NlcnQgc2NoZW1hIG11c3QgZXhpc3QuXHJcbiAgICBpZiAoIXRoaXMuc2NoZW1hKSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcihgTXVzdCBzdXBwbHkgdmFsaWQgbW9kZWwgbmFtZSBvciBzY2hlbWEgdG8gS2VwcyB0YWJsZSFgKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBTZXQgYGFsbENvbHVtbnNgIHRvIGJlIGV2ZXJ5IHB1YmxpYyBmaWVsZCBpbiB0aGUgc2NoZW1hLlxyXG4gICAgdGhpcy5hbGxDb2x1bW5zID0gT2JqZWN0LmtleXModGhpcy5zY2hlbWEpLmZpbHRlcihmaWVsZCA9PiBmaWVsZFswXSAhPT0gJ18nKTtcclxuICAgIFxyXG4gICAgLy8gUHJvY2VzcyBjdXN0b20gY29sdW1ucy5cclxuICAgIGNvbnN0IGNvbERhdGFzID0gdGhpcy5jdXN0b21Db2xEYXRhcy50b0FycmF5KCk7XHJcbiAgICBjb25zdCBjb2xUZW1wbGF0ZXMgPSB0aGlzLmN1c3RvbUNvbFRlbXBsYXRlcy50b0FycmF5KCk7XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNvbERhdGFzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGNvbnN0IGNvbElkID0gY29sRGF0YXNbaV0uaWQ7XHJcbiAgICAgIHRoaXMuY3VzdG9tQ29sc1tjb2xJZF0gPSB7XHJcbiAgICAgICAgZGF0YTogY29sRGF0YXNbaV0sXHJcbiAgICAgICAgdGVtcGxhdGU6IGNvbFRlbXBsYXRlc1tpXVxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmFsbENvbHVtbnMucHVzaChjb2xJZCk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC8vIElmIGBkaXNwbGF5ZWRDb2x1bW5zYCBpcyBub3QgcHJvdmlkZWQsIHVzZSBgYWxsQ29sdW1uc2AgZmlsdGVyZWQgYnkgYGhpZGRlbkNvbHVtbnNgLlxyXG4gICAgaWYgKHRoaXMuZGlzcGxheWVkQ29sdW1ucy5sZW5ndGggPT09IDApIHtcclxuICAgICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gdGhpcy5hbGxDb2x1bW5zLmZpbHRlcihmaWVsZCA9PiAhdGhpcy5oaWRkZW5Db2x1bW5zLmluY2x1ZGVzKGZpZWxkKSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5pbml0aWFsaXplZCA9IHRydWU7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBuZ0RvQ2hlY2soKSB7XHJcbiAgICAvLyBUaGlzIHdpbGwgcmVmcmVzaCB0aGUgdGFibGUgZGF0YSBzb3VyY2UgYW5kIHJlcmVuZGVyXHJcbiAgICAvLyB0aGUgdGFibGUgaWYgdGhlcmUgaXMgYW55IGNoYW5nZSB0byB0aGUgZGF0YS5cclxuICAgIGlmICh0aGlzLmluaXRpYWxpemVkKSB7XHJcbiAgICAgIGNvbnN0IGNoYW5nZXMgPSB0aGlzLmRpZmYuZGlmZih0aGlzLmRhdGEpO1xyXG4gICAgICBpZiAoY2hhbmdlcykge1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZS5kYXRhID0gdGhpcy5kYXRhO1xyXG4gICAgICAgIHRoaXMudGFibGUucmVuZGVyUm93cygpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAvLyBUaGlzIGNoZWNrIGlmIHRoZSBgc2VhcmNoYCBwcm9wZXJ0eSBoYXMgY2hhbmdlZCBhbmQgYXBwbHkgc2VhcmNoIGlmIGl0IGRvZXMuXHJcbiAgICBpZiAodGhpcy5pbml0aWFsaXplZCAmJiBjaGFuZ2VzLnNlYXJjaCkge1xyXG4gICAgICB0aGlzLmFwcGx5U2VhcmNoKHRoaXMuc2VhcmNoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNhdmVzIGN1cnJlbnQgcGFnZSBzaXplIHRvIGxvY2FsIHN0b3JhZ2UgaWYgcHJvcGVydHkgYHNhdmVQYWdlU2l6ZWAgaXMgc3VwcGxpZWQuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICogQHBhcmFtIGV2ZW50IEV2ZW50IGZyb20gcGFnaW5hdG9yLlxyXG4gICAqL1xyXG4gIG9uUGFnZUNoYW5nZShldmVudDogUGFnZUV2ZW50KSB7XHJcbiAgICBpZiAodGhpcy5zYXZlUGFnZVNpemUpIHtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy5zYXZlUGFnZVNpemUsIGV2ZW50LnBhZ2VTaXplLnRvU3RyaW5nKCkpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQXBwbGllcyBhIHNlYXJjaCBxdWVyeSB0byBmaWx0ZXIgdGhlIGRhdGEuXHJcbiAgICogQHBhcmFtIHNlYXJjaCBRdWVyeSBmb3IgdGhlIHNlYXJjaC5cclxuICAgKi9cclxuICBhcHBseVNlYXJjaChzZWFyY2g6IHN0cmluZykge1xyXG4gICAgdGhpcy5kYXRhU291cmNlLmZpbHRlciA9IHNlYXJjaC50cmltKCkudG9Mb3dlckNhc2UoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENoZWNrcyB0aGUgdHlwZSBvZiBhIGZpZWxkLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqIEBwYXJhbSBmaWVsZCBGaWVsZCB0byBiZSBjaGVja2VkLlxyXG4gICAqIEBwYXJhbSB0eXBlIFR5cGUgdG8gYmUgY2hlY2tlZC5cclxuICAgKi9cclxuICBpc1R5cGUoZmllbGQ6IHN0cmluZywgdHlwZTogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gaXNUeXBlKHRoaXMuc2NoZW1hLCBmaWVsZCwgdHlwZSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIHRoZSBkaXNwbGF5IGV4cHJlc3Npb24gb2YgYSByZWZlcmVuY2UgZmllbGQuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICogQHBhcmFtIGZpZWxkIEZpZWxkIHRvIGdldCBmcm9tLlxyXG4gICAqL1xyXG4gIGdldFJlZmVyZW5jZURpc3BsYXkoZmllbGQ6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIGdldFJlZmVyZW5jZURpc3BsYXkodGhpcy5tb2RlbHMsIHRoaXMuc2NoZW1hLCBmaWVsZCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIHRoZSBkaXNwbGF5IGV4cHJlc3Npb24gb2YgYW4gYXJyYXkgb2YgcmVmZXJlbmNlcyBmaWVsZC5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKiBAcGFyYW0gZmllbGQgRmllbGQgdG8gZ2V0IGZyb20uXHJcbiAgICovXHJcbiAgZ2V0UmVmZXJlbmNlRGlzcGxheUFycmF5KGZpZWxkOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBnZXRSZWZlcmVuY2VEaXNwbGF5QXJyYXkodGhpcy5tb2RlbHMsIHRoaXMuc2NoZW1hLCBmaWVsZCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLyoqXHJcbiAgICogU2V0IHRoZSBmaWx0ZXIgcHJlZGljYXRlIG9mIGRhdGEgc291cmNlLiBcclxuICAgKiB0aGlzIGZ1bmN0aW9uIGFjY2VwdHMgYSBmdW5jdGlvbiBhcyBhIHBhcmFtZXRlci4gXHJcbiAgICogU2VlIGV4YW1wbGUgYmVsb3c6XHJcbiAgICogPHByZT4gXHJcbiAgICogIGN1c3RvbUZpbHRlcihEYXRhOiBULCBGaWx0ZXI6IHN0cmluZyk6IGJvb2xlYW4geyBcclxuICAgKiAgICAgcmV0dXJuIDx0cnVlIGlmIERhdGEgbWF0Y2hlcyBmaWx0ZXI+IFxyXG4gICAqICB9XHJcbiAgICogPHByZT5cclxuICAgKiBJZiBubyBmdW50aW9uIGlzIHByb3ZpZGVkLCB0aGlzIGZ1bmN0aW9uIHdpbGwgdXNlIHRoZSBkZWZhdWx0IFxyXG4gICAqIEBzZWUgPGEgaHJlZj1cImh0dHBzOi8vbWF0ZXJpYWwuYW5ndWxhci5pby9jb21wb25lbnRzL3RhYmxlL2FwaVwiPkFuZ3VsYXIgVGFibGU8L2E+IGZvciBtb3JlIGluZm8uXHJcbiAgICogQHBhcmFtIGZpbHRlclByZWRpY2F0ZUZ1bmN0aW9uIGN1c3RvbSBmaWx0ZXJQcmVkaWNhdGVGdW5jdGlvbiB0aGF0IGZvbGxvd3MgdGhlIGFib3ZlIGZvcm1hdFxyXG4gICAqL1xyXG4gIHNldEZpbHRlclByZWRpY2F0ZShmaWx0ZXJQcmVkaWNhdGVGdW5jdGlvbj0gbnVsbCkge1xyXG4gICAgaWYgKGZpbHRlclByZWRpY2F0ZUZ1bmN0aW9uKSB7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZS5maWx0ZXJQcmVkaWNhdGUgPSBmaWx0ZXJQcmVkaWNhdGVGdW5jdGlvbjtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiJdfQ==