/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, ViewChild, ContentChildren, QueryList, TemplateRef, EventEmitter, Inject } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataService } from '../../../service/data.service';
import { Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { TableColDirective } from '../table-col.directive';
import { isType, getReferenceDisplay, getReferenceDisplayArray } from '../../../utils';
import _ from 'lodash';
export class DynamicTableComponent {
    /**
     * \@internal Do not use!
     * @param {?} models
     * @param {?} dataService
     */
    constructor(models, dataService) {
        this.models = models;
        this.dataService = dataService;
        /**
         * Data to be shown on the table from the GraphQL query.
         * \@readonly
         */
        this.data = [];
        /**
         * Columns to be displayed on the table.
         */
        this.displayedColumns = [];
        /**
         * Columns to be hidden on the table.
         */
        this.hiddenColumns = [];
        /**
         * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
         * \@internal Do not use!
         */
        this.allColumns = [];
        /**
         * A record of custom column data and template keyed by the custom column ID.
         */
        this.customCols = {};
        /**
         * The table's default paginator page size.
         */
        this.defaultPageSize = 10;
        /**
         * Length of the data to be shown on the paginator.
         * \@readonly
         */
        this.dataLength = Infinity;
        /**
         * Whether the data length has been known (from last query) or not.
         */
        this.isDataLengthSet = false;
        /**
         * Whether the table should show the default search box or not.
         */
        this.showSearch = false;
        /**
         * A subject that emits every time the search string changes.
         */
        this.searchChange = new Subject();
        /**
         * Delay for the search function to be called in milliseconds.
         */
        this.searchDelay = 1000;
        /**
         * A subject that emits every time the filter function changes.
         */
        this.filterFunctionChange = new Subject();
        /**
         * An event emitter that emits the object of the row clicked by the user.
         */
        this.rowClick = new EventEmitter();
        /**
         * Whether the table is currently loading or not.
         * \@readonly
         */
        this.tableLoading = true;
        /**
         * Whether the table has done initializing or not.
         * \@readonly
         */
        this.initialized = false;
    }
    /**
     * String to be searched based on `searchFields`.
     * @return {?}
     */
    get search() {
        return this._search;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set search(value) {
        this._search = value;
        this.searchChange.next(value);
    }
    /**
     * Function to filter the result after query.
     * @return {?}
     */
    get filterFunction() {
        return this._filterFunction;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set filterFunction(value) {
        this._filterFunction = value;
        this.filterFunctionChange.next();
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        // Get the previous session's page size if any, or use the default page size.
        this.pageSize = Number(localStorage.getItem(this.savePageSize)) || this.defaultPageSize;
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngAfterViewInit() {
        // Get data as soon as view initialized.
        this.getData();
        // Setup delayed search.
        /** @type {?} */
        const delayedSearch = this.searchChange
            .pipe(debounceTime(this.searchDelay), distinctUntilChanged());
        // If any of these observables emit, then we reset the paginator.
        delayedSearch.subscribe((/**
         * @return {?}
         */
        () => {
            this.paginator.pageIndex = 0;
            this.dataLength = Infinity;
        }));
        this.filterFunctionChange.subscribe((/**
         * @return {?}
         */
        () => {
            this.paginator.pageIndex = 0;
            this.dataLength = Infinity;
        }));
        this.sort.sortChange.subscribe((/**
         * @return {?}
         */
        () => this.paginator.pageIndex = 0));
        // Save page size if paginator changes.
        if (this.savePageSize) {
            this.paginator.page.subscribe((/**
             * @return {?}
             */
            () => {
                localStorage.setItem(this.savePageSize, this.paginator.pageSize.toString());
            }));
        }
        // If any of these observables emit, then we refresh the data.
        merge(this.sort.sortChange, this.paginator.page, delayedSearch, this.filterFunctionChange)
            .subscribe((/**
         * @return {?}
         */
        () => this.getData()));
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngAfterContentInit() {
        // We are using `ngAfterContentInit` here because we can
        // only get the custom columns data after the component content
        // has been initialized.
        // Get schema from model name if supplied.
        if (this.model && this.models[this.model]) {
            this.schema = this.models[this.model].schema;
        }
        else if (this.model && !this.models[this.model]) {
            throw new Error(`Can't find a model named ${this.model}!`);
        }
        // Assert schema must exist.
        if (!this.schema) {
            throw new Error(`Must supply valid model name to Keps dynamic table!`);
        }
        // Set `allColumns` to be every public field in the schema.
        this.allColumns = Object.keys(this.schema).filter((/**
         * @param {?} field
         * @return {?}
         */
        field => field[0] !== '_'));
        // Process custom columns.
        /** @type {?} */
        const colDatas = this.customColDatas.toArray();
        /** @type {?} */
        const colTemplates = this.customColTemplates.toArray();
        for (let i = 0; i < colDatas.length; i++) {
            /** @type {?} */
            const colId = colDatas[i].id;
            this.customCols[colId] = {
                data: colDatas[i],
                template: colTemplates[i]
            };
            this.allColumns.push(colId);
        }
        // If `displayedColumns` is not provided, use `allColumns` filtered by `hiddenColumns`.
        if (this.displayedColumns.length === 0) {
            this.displayedColumns = this.allColumns.filter((/**
             * @param {?} field
             * @return {?}
             */
            field => !this.hiddenColumns.includes(field)));
        }
        this.initialized = true;
    }
    /**
     * Combines properties as GraphQL query and gets data from data service.
     * @return {?}
     */
    getData() {
        return tslib_1.__awaiter(this, void 0, void 0, /** @this {!DynamicTableComponent} */ function* () {
            try {
                this.tableLoading = true;
                /** @type {?} */
                const filters = {};
                // Setup page query.
                const { pageIndex, pageSize } = this.paginator;
                filters.offset = pageIndex * pageSize;
                // Setup sort query.
                const { active, direction } = this.sort;
                if (active) {
                    filters.sort = (direction === 'desc' ? '-' : '+') + active;
                }
                // Setup search query.
                if (this.searchFields && this.search) {
                    /** @type {?} */
                    let mongoFilter = ` { '$or': [`;
                    for (let field of this.searchFields) {
                        mongoFilter += `{'${field}': {
            '$regex': '${this.search.replace('(', '%28').replace(')', '%29')}',
            '$options': 'i' }},`;
                    }
                    mongoFilter = _.trimEnd(mongoFilter, ',');
                    mongoFilter += ']}';
                    filters.mongo = mongoFilter;
                }
                // Set limit if there is not filter function.
                if (!this.filterFunction) {
                    filters.limit = pageSize;
                }
                // Get data from data service.
                /** @type {?} */
                const result = yield this.dataService.graphql(`
        ${this.model}s(
          ${this.convertFilter(Object.assign(filters, this.filterQuery))}
        ){${this.query}}
      `);
                /** @type {?} */
                const data = result.data[this.model + 's'];
                // If filter function exists, filter with that function.
                if (this.filterFunction) {
                    this.data = _.filter(data, this.filterFunction);
                }
                else {
                    // If this is not the first page and data is empty,
                    // then we go back to the last page.
                    if (pageIndex > 1 && data.length === 0) {
                        // Calculate data length.
                        this.dataLength = (pageIndex - 1) * pageSize + this.data.length;
                        this.paginator.pageIndex = pageIndex - 1;
                    }
                    else {
                        // If data length is smaller than page size,
                        // we assume we're on last page and recalculate data length.
                        if (data.length < pageSize) {
                            this.dataLength = pageIndex * pageSize + data.length;
                        }
                        // Set data as is.
                        this.data = data;
                    }
                }
                this.tableLoading = false;
            }
            catch (err) {
                console.error(err);
            }
        });
    }
    /**
     * Converts a filter object to Mongo filter string.
     * @private
     * @param {?} filters Pair of keys & values to be converted to Mongo filter.
     * @return {?}
     */
    convertFilter(filters) {
        /** @type {?} */
        let str = '';
        for (const filterName in filters) {
            if (filterName === 'limit' || filterName === 'offset') {
                str += `${filterName}:${filters[filterName]},`;
            }
            else {
                str += `${filterName}:"${filters[filterName]}",`;
            }
        }
        return str;
    }
    /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    isType(field, type) {
        return isType(this.schema, field, type);
    }
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplay(field) {
        return getReferenceDisplay(this.models, this.schema, field);
    }
    /**
     * Returns the display expression of an array of references field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplayArray(field) {
        return getReferenceDisplayArray(this.models, this.schema, field);
    }
}
DynamicTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-dynamic-table',
                template: "<div *ngIf=\"tableLoading\" class=\"table-loader\">\r\n  <mat-spinner></mat-spinner>\r\n</div>\r\n\r\n<!-- Table's Search Box -->\r\n<mat-form-field\r\n  *ngIf=\"showSearch\"\r\n  floatLabel=\"never\"\r\n  class=\"keps-table-search\"\r\n>\r\n  <input\r\n    matInput\r\n    (keyup)=\"search = $event.target.value\"\r\n    placeholder=\"Search\"\r\n  >\r\n</mat-form-field>\r\n\r\n<!-- Table -->\r\n<table\r\n  mat-table [dataSource]=\"data\"\r\n  matSort [matSortActive]=\"activeSort\" matSortDirection=\"asc\"\r\n  class=\"keps-table\"\r\n>\r\n  <!-- Table Columns from Schema -->\r\n  <ng-container\r\n    *ngFor=\"let fieldName of schema | keys\"\r\n    [matColumnDef]=\"fieldName\"\r\n  >\r\n    <ng-container *ngIf=\"schema[fieldName]; let field\">\r\n      <th mat-header-cell *matHeaderCellDef mat-sort-header>\r\n        {{ (field.label ? field.label : fieldName) | splitCamel | titlecase }}\r\n      </th>\r\n      <td \r\n        mat-cell *matCellDef=\"let element\" \r\n        class=\"keps-table-cell\"\r\n      >\r\n        <ng-container *ngIf=\"element[fieldName]; let fieldData\">\r\n          <span *ngIf=\"isType(fieldName, 'reference')\">\r\n            {{ fieldData | eval:getReferenceDisplay(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'address')\">\r\n            {{ fieldData | address:4 }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'datetime')\">\r\n            {{ fieldData | moment:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-reference')\">\r\n            {{ fieldData | arrayDisplay:', ':getReferenceDisplayArray(fieldName) }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'array-string')\">\r\n            {{ fieldData | arrayDisplay:', ' }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'number')\">\r\n            {{ fieldData | numberFormat:field.format }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'enum')\">\r\n            {{ field.labels && field.labels[fieldData] ? field.labels[fieldData] : fieldData }}\r\n          </span>\r\n          <span *ngIf=\"isType(fieldName, 'table-normal')\">\r\n            {{ fieldData }}\r\n          </span>\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Table Columns from Custom Columns -->\r\n  <ng-container\r\n    *ngFor=\"let colName of customCols | keys\"\r\n    [matColumnDef]=\"colName\"\r\n  >\r\n    <ng-container *ngIf=\"customCols[colName].data; let colData\">\r\n      <th\r\n        mat-header-cell *matHeaderCellDef\r\n        [width]=\"colData.width\"\r\n        [align]=\"colData.align\"\r\n        mat-sort-header [disabled]=\"!colData.sortable\" [start]=\"colData.sortStart\"\r\n      >\r\n        {{ (colData.label || colName) | splitCamel | titlecase }}\r\n      </th>\r\n\r\n      <td\r\n        mat-cell *matCellDef=\"let element; let i = index;\"\r\n        [align]=\"colData.align\"\r\n        class=\"keps-table-column\"\r\n      >\r\n        <ng-container\r\n          *ngTemplateOutlet=\"customCols[colName].template; context: { $implicit: element, index: i, element: element }\"\r\n        >\r\n        </ng-container>\r\n      </td>\r\n    </ng-container>\r\n  </ng-container>\r\n\r\n  <!-- Rows -->\r\n  <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n  <tr\r\n    mat-row *matRowDef=\"let row; columns: displayedColumns;\"\r\n    (click)=\"rowClick.emit(row)\"\r\n    [class.clickable-row]=\"rowClick.observers.length > 0\"\r\n  >\r\n  </tr>\r\n</table>\r\n\r\n<!-- Empty Table Row -->\r\n<mat-card *ngIf=\"data.length === 0\" class=\"empty-table\">\r\n  No Data.\r\n</mat-card>\r\n\r\n<!-- Paginator -->\r\n<mat-paginator\r\n  [class.hide-paginator]=\"filterFunction\"\r\n  [pageSizeOptions]=\"[10, 25, 50, 100]\"\r\n  [pageSize]=\"pageSize\"\r\n  [length]=\"dataLength\"\r\n>\r\n</mat-paginator>\r\n",
                styles: [":host{display:block;position:relative}.keps-table{width:100%}.empty-table,.keps-paginator,.keps-table{background:0 0!important}.keps-table-cell{white-space:pre-line}.keps-table-cell *{display:block;margin:16px 0}.empty-table{border-radius:0;text-align:center;box-shadow:none!important}.hide-paginator{display:none!important}.keps-table-search{width:95%;margin:0 2.5%}.clickable-row:hover{background:rgba(0,0,0,.05);cursor:pointer}.table-loader{background:rgba(0,0,0,.125);z-index:999;width:100%;height:100%;position:absolute;display:flex;justify-content:center;align-items:center}"]
            }] }
];
/** @nocollapse */
DynamicTableComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] },
    { type: DataService }
];
DynamicTableComponent.propDecorators = {
    model: [{ type: Input }],
    query: [{ type: Input }],
    filterQuery: [{ type: Input }],
    displayedColumns: [{ type: Input }],
    hiddenColumns: [{ type: Input }],
    customColDatas: [{ type: ContentChildren, args: [TableColDirective,] }],
    customColTemplates: [{ type: ContentChildren, args: [TableColDirective, { read: TemplateRef },] }],
    paginator: [{ type: ViewChild, args: [MatPaginator, { static: true },] }],
    savePageSize: [{ type: Input }],
    defaultPageSize: [{ type: Input }],
    sort: [{ type: ViewChild, args: [MatSort, { static: true },] }],
    activeSort: [{ type: Input }],
    showSearch: [{ type: Input }],
    searchFields: [{ type: Input }],
    search: [{ type: Input, args: ['search',] }],
    searchDelay: [{ type: Input }],
    filterFunction: [{ type: Input, args: ['filterFunction',] }],
    rowClick: [{ type: Output }]
};
if (false) {
    /**
     * Name of the model for the table.
     * @type {?}
     */
    DynamicTableComponent.prototype.model;
    /**
     * Schema of the model.
     * \@internal Do not use!
     * @type {?}
     */
    DynamicTableComponent.prototype.schema;
    /**
     * Data to be shown on the table from the GraphQL query.
     * \@readonly
     * @type {?}
     */
    DynamicTableComponent.prototype.data;
    /**
     * Comma-separated list of fields for the GraphQL query.
     * @type {?}
     */
    DynamicTableComponent.prototype.query;
    /**
     * Additional filter to be added to the GraphQL query.
     * @type {?}
     */
    DynamicTableComponent.prototype.filterQuery;
    /**
     * Columns to be displayed on the table.
     * @type {?}
     */
    DynamicTableComponent.prototype.displayedColumns;
    /**
     * Columns to be hidden on the table.
     * @type {?}
     */
    DynamicTableComponent.prototype.hiddenColumns;
    /**
     * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
     * \@internal Do not use!
     * @type {?}
     */
    DynamicTableComponent.prototype.allColumns;
    /**
     * Queries all `TableColDirective` data inside the component.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.customColDatas;
    /**
     * Queries all `TableColDirective` templates inside the component.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.customColTemplates;
    /**
     * A record of custom column data and template keyed by the custom column ID.
     * @type {?}
     */
    DynamicTableComponent.prototype.customCols;
    /**
     * Reference to the MatPaginator.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.paginator;
    /**
     * A key used when saving page size number to the local storage.
     * @type {?}
     */
    DynamicTableComponent.prototype.savePageSize;
    /**
     * The table's default paginator page size.
     * @type {?}
     */
    DynamicTableComponent.prototype.defaultPageSize;
    /**
     * The current page size on paginator.
     * @type {?}
     */
    DynamicTableComponent.prototype.pageSize;
    /**
     * Length of the data to be shown on the paginator.
     * \@readonly
     * @type {?}
     */
    DynamicTableComponent.prototype.dataLength;
    /**
     * Whether the data length has been known (from last query) or not.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.isDataLengthSet;
    /**
     * Reference to the MatSort.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.sort;
    /**
     * The ID of the column that should be sorted.
     * @type {?}
     */
    DynamicTableComponent.prototype.activeSort;
    /**
     * Whether the table should show the default search box or not.
     * @type {?}
     */
    DynamicTableComponent.prototype.showSearch;
    /**
     * Field used for the search.
     * @type {?}
     */
    DynamicTableComponent.prototype.searchFields;
    /**
     * String to be searched based on `searchFields`.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype._search;
    /**
     * A subject that emits every time the search string changes.
     * @type {?}
     */
    DynamicTableComponent.prototype.searchChange;
    /**
     * Delay for the search function to be called in milliseconds.
     * @type {?}
     */
    DynamicTableComponent.prototype.searchDelay;
    /**
     * Function to filter the result after query.
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype._filterFunction;
    /**
     * A subject that emits every time the filter function changes.
     * @type {?}
     */
    DynamicTableComponent.prototype.filterFunctionChange;
    /**
     * An event emitter that emits the object of the row clicked by the user.
     * @type {?}
     */
    DynamicTableComponent.prototype.rowClick;
    /**
     * Whether the table is currently loading or not.
     * \@readonly
     * @type {?}
     */
    DynamicTableComponent.prototype.tableLoading;
    /**
     * Whether the table has done initializing or not.
     * \@readonly
     * @type {?}
     */
    DynamicTableComponent.prototype.initialized;
    /** @type {?} */
    DynamicTableComponent.prototype.models;
    /**
     * @type {?}
     * @private
     */
    DynamicTableComponent.prototype.dataService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy10YWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudC90YWJsZS9keW5hbWljLXRhYmxlL2R5bmFtaWMtdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNMLFNBQVMsRUFJVCxLQUFLLEVBQ0wsTUFBTSxFQUNOLFNBQVMsRUFDVCxlQUFlLEVBQ2YsU0FBUyxFQUNULFdBQVcsRUFDWCxZQUFZLEVBQ1osTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDMUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBRTVELE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxZQUFZLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQWdCLE1BQU0sd0JBQXdCLENBQUM7QUFDekUsT0FBTyxFQUFFLE1BQU0sRUFBRSxtQkFBbUIsRUFBRSx3QkFBd0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3ZGLE9BQU8sQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQWF2QixNQUFNLE9BQU8scUJBQXFCOzs7Ozs7SUFtTGhDLFlBQzJCLE1BQWtCLEVBQ25DLFdBQXdCO1FBRFAsV0FBTSxHQUFOLE1BQU0sQ0FBWTtRQUNuQyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTs7Ozs7UUFyS2xDLFNBQUksR0FBaUIsRUFBRSxDQUFDOzs7O1FBZWYscUJBQWdCLEdBQWEsRUFBRSxDQUFDOzs7O1FBS2hDLGtCQUFhLEdBQWEsRUFBRSxDQUFDOzs7OztRQU10QyxlQUFVLEdBQWEsRUFBRSxDQUFDOzs7O1FBZTFCLGVBQVUsR0FBaUMsRUFBRSxDQUFDOzs7O1FBZXJDLG9CQUFlLEdBQUcsRUFBRSxDQUFDOzs7OztRQVc5QixlQUFVLEdBQUcsUUFBUSxDQUFDOzs7O1FBS2Qsb0JBQWUsR0FBRyxLQUFLLENBQUM7Ozs7UUFldkIsZUFBVSxHQUFHLEtBQUssQ0FBQzs7OztRQWU1QixpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFVLENBQUM7Ozs7UUFpQjVCLGdCQUFXLEdBQUcsSUFBSSxDQUFDOzs7O1FBVTVCLHlCQUFvQixHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7Ozs7UUFpQmpDLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBYyxDQUFDOzs7OztRQU1wRCxpQkFBWSxHQUFHLElBQUksQ0FBQzs7Ozs7UUFNcEIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7SUFRaEIsQ0FBQzs7Ozs7SUEzREwsSUFBSSxNQUFNO1FBQ1IsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBQ0QsSUFDSSxNQUFNLENBQUMsS0FBYTtRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7OztJQW9CRCxJQUFJLGNBQWM7UUFDaEIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQzlCLENBQUM7Ozs7O0lBQ0QsSUFDSSxjQUFjLENBQUMsS0FBcUI7UUFDdEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBOEJELFFBQVE7UUFDTiw2RUFBNkU7UUFDN0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDO0lBQzFGLENBQUM7Ozs7O0lBS0QsZUFBZTtRQUNiLHdDQUF3QztRQUN4QyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7OztjQUdULGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWTthQUN0QyxJQUFJLENBQ0gsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFDOUIsb0JBQW9CLEVBQUUsQ0FDdkI7UUFFRCxpRUFBaUU7UUFDakUsYUFBYSxDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7UUFDN0IsQ0FBQyxFQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztRQUM3QixDQUFDLEVBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBQyxDQUFDO1FBRW5FLHVDQUF1QztRQUN2QyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUzs7O1lBQUMsR0FBRyxFQUFFO2dCQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUM5RSxDQUFDLEVBQUMsQ0FBQztTQUNKO1FBRUQsOERBQThEO1FBQzlELEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDO2FBQ3pGLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBS0Qsa0JBQWtCO1FBQ2hCLHdEQUF3RDtRQUN4RCwrREFBK0Q7UUFDL0Qsd0JBQXdCO1FBRXhCLDBDQUEwQztRQUMxQyxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUM7U0FDOUM7YUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqRCxNQUFNLElBQUksS0FBSyxDQUFDLDRCQUE0QixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztTQUM1RDtRQUVELDRCQUE0QjtRQUM1QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLHFEQUFxRCxDQUFDLENBQUM7U0FDeEU7UUFFRCwyREFBMkQ7UUFDM0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFDLENBQUM7OztjQUd2RSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUU7O2NBQ3hDLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFO1FBQ3RELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztrQkFDbEMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUc7Z0JBQ3ZCLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNqQixRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQzthQUMxQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDN0I7UUFFRCx1RkFBdUY7UUFDdkYsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN0QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUM7U0FDOUY7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztJQUMxQixDQUFDOzs7OztJQUtLLE9BQU87O1lBQ1gsSUFBSTtnQkFDRixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzs7c0JBRW5CLE9BQU8sR0FBd0IsRUFBRTs7c0JBR2pDLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxHQUFHLElBQUksQ0FBQyxTQUFTO2dCQUM5QyxPQUFPLENBQUMsTUFBTSxHQUFHLFNBQVMsR0FBRyxRQUFRLENBQUM7O3NCQUdoQyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsR0FBRyxJQUFJLENBQUMsSUFBSTtnQkFDdkMsSUFBSSxNQUFNLEVBQUU7b0JBQ1YsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLFNBQVMsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDO2lCQUM1RDtnQkFFRCxzQkFBc0I7Z0JBQ3RCLElBQUksSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFOzt3QkFDaEMsV0FBVyxHQUFHLGFBQWE7b0JBQy9CLEtBQUssSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTt3QkFDbkMsV0FBVyxJQUFJLEtBQUssS0FBSzt5QkFDVixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUM7Z0NBQzVDLENBQUE7cUJBQ3ZCO29CQUNELFdBQVcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDMUMsV0FBVyxJQUFJLElBQUksQ0FBQztvQkFDcEIsT0FBTyxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUM7aUJBQzdCO2dCQUVELDZDQUE2QztnQkFDN0MsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ3hCLE9BQU8sQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDO2lCQUMxQjs7O3NCQUdLLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO1VBQzFDLElBQUksQ0FBQyxLQUFLO1lBQ1IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDNUQsSUFBSSxDQUFDLEtBQUs7T0FDZixDQUFDOztzQkFDSSxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztnQkFFMUMsd0RBQXdEO2dCQUN4RCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2lCQUNqRDtxQkFBTTtvQkFDTCxtREFBbUQ7b0JBQ25ELG9DQUFvQztvQkFDcEMsSUFBSSxTQUFTLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO3dCQUN0Qyx5QkFBeUI7d0JBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO3dCQUNoRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxTQUFTLEdBQUcsQ0FBQyxDQUFDO3FCQUMxQzt5QkFBTTt3QkFDTCw0Q0FBNEM7d0JBQzVDLDREQUE0RDt3QkFDNUQsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsRUFBRTs0QkFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLEdBQUcsUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7eUJBQ3REO3dCQUVELGtCQUFrQjt3QkFDbEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7cUJBQ2xCO2lCQUNGO2dCQUVELElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2FBQzNCO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1osT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNwQjtRQUNILENBQUM7S0FBQTs7Ozs7OztJQU1PLGFBQWEsQ0FBQyxPQUErQjs7WUFDL0MsR0FBRyxHQUFHLEVBQUU7UUFDWixLQUFLLE1BQU0sVUFBVSxJQUFJLE9BQU8sRUFBRTtZQUNoQyxJQUFJLFVBQVUsS0FBSyxPQUFPLElBQUksVUFBVSxLQUFLLFFBQVEsRUFBRTtnQkFDckQsR0FBRyxJQUFJLEdBQUcsVUFBVSxJQUFJLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDO2FBQ2hEO2lCQUFNO2dCQUNMLEdBQUcsSUFBSSxHQUFHLFVBQVUsS0FBSyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQzthQUNsRDtTQUNGO1FBQ0QsT0FBTyxHQUFHLENBQUM7SUFDYixDQUFDOzs7Ozs7OztJQVFELE1BQU0sQ0FBQyxLQUFhLEVBQUUsSUFBWTtRQUNoQyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7Ozs7O0lBT0QsbUJBQW1CLENBQUMsS0FBYTtRQUMvQixPQUFPLG1CQUFtQixDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM5RCxDQUFDOzs7Ozs7O0lBT0Qsd0JBQXdCLENBQUMsS0FBYTtRQUNwQyxPQUFPLHdCQUF3QixDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNuRSxDQUFDOzs7WUF4WUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLGc0SEFBNkM7O2FBRTlDOzs7OzRDQXFMSSxNQUFNLFNBQUMsUUFBUTtZQXZNWCxXQUFXOzs7b0JBdUJqQixLQUFLO29CQWlCTCxLQUFLOzBCQUtMLEtBQUs7K0JBS0wsS0FBSzs0QkFLTCxLQUFLOzZCQVdMLGVBQWUsU0FBQyxpQkFBaUI7aUNBS2pDLGVBQWUsU0FBQyxpQkFBaUIsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUU7d0JBVXhELFNBQVMsU0FBQyxZQUFZLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFOzJCQUt4QyxLQUFLOzhCQUtMLEtBQUs7bUJBcUJMLFNBQVMsU0FBQyxPQUFPLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFO3lCQUtuQyxLQUFLO3lCQUtMLEtBQUs7MkJBS0wsS0FBSztxQkFrQkwsS0FBSyxTQUFDLFFBQVE7MEJBU2QsS0FBSzs2QkFrQkwsS0FBSyxTQUFDLGdCQUFnQjt1QkFTdEIsTUFBTTs7Ozs7OztJQTlKUCxzQ0FBdUI7Ozs7OztJQU12Qix1Q0FBbUI7Ozs7OztJQU1uQixxQ0FBd0I7Ozs7O0lBS3hCLHNDQUF1Qjs7Ozs7SUFLdkIsNENBQTZDOzs7OztJQUs3QyxpREFBeUM7Ozs7O0lBS3pDLDhDQUFzQzs7Ozs7O0lBTXRDLDJDQUEwQjs7Ozs7O0lBSzFCLCtDQUF5Rjs7Ozs7O0lBS3pGLG1EQUFtSDs7Ozs7SUFLbkgsMkNBQThDOzs7Ozs7SUFLOUMsMENBQTJFOzs7OztJQUszRSw2Q0FBOEI7Ozs7O0lBSzlCLGdEQUE4Qjs7Ozs7SUFLOUIseUNBQWlCOzs7Ozs7SUFNakIsMkNBQXNCOzs7Ozs7SUFLdEIsZ0RBQWdDOzs7Ozs7SUFLaEMscUNBQTREOzs7OztJQUs1RCwyQ0FBNEI7Ozs7O0lBSzVCLDJDQUE0Qjs7Ozs7SUFLNUIsNkNBQWdDOzs7Ozs7SUFLaEMsd0NBQXdCOzs7OztJQUt4Qiw2Q0FBcUM7Ozs7O0lBaUJyQyw0Q0FBNEI7Ozs7OztJQUs1QixnREFBd0M7Ozs7O0lBS3hDLHFEQUEyQzs7Ozs7SUFpQjNDLHlDQUFvRDs7Ozs7O0lBTXBELDZDQUFvQjs7Ozs7O0lBTXBCLDRDQUFvQjs7SUFNbEIsdUNBQTJDOzs7OztJQUMzQyw0Q0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIE9uSW5pdCxcbiAgQWZ0ZXJWaWV3SW5pdCxcbiAgQWZ0ZXJDb250ZW50SW5pdCxcbiAgSW5wdXQsXG4gIE91dHB1dCxcbiAgVmlld0NoaWxkLFxuICBDb250ZW50Q2hpbGRyZW4sXG4gIFF1ZXJ5TGlzdCxcbiAgVGVtcGxhdGVSZWYsXG4gIEV2ZW50RW1pdHRlcixcbiAgSW5qZWN0XG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0UGFnaW5hdG9yLCBNYXRTb3J0IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgRGF0YVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlL2RhdGEuc2VydmljZSc7XG5pbXBvcnQgeyBLZXBzTW9kZWxzLCBLZXBzU2NoZW1hLCBHcmFwaHFsUmVzdWx0LCBLZXBzT2JqZWN0IH0gZnJvbSAnLi4vLi4vLi4vaW50ZXJmYWNlJztcbmltcG9ydCB7IFN1YmplY3QsIG1lcmdlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBkZWJvdW5jZVRpbWUsIGRpc3RpbmN0VW50aWxDaGFuZ2VkIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgVGFibGVDb2xEaXJlY3RpdmUsIEN1c3RvbUNvbHVtbiB9IGZyb20gJy4uL3RhYmxlLWNvbC5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgaXNUeXBlLCBnZXRSZWZlcmVuY2VEaXNwbGF5LCBnZXRSZWZlcmVuY2VEaXNwbGF5QXJyYXkgfSBmcm9tICcuLi8uLi8uLi91dGlscyc7XG5pbXBvcnQgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IHsgZmlsdGVyIH0gZnJvbSAnbWluaW1hdGNoJztcblxuLyoqXG4gKiBBIGZ1bmN0aW9uIHVzZWQgYnkgdGhlIGR5bmFtaWMgdGFibGUgdG8gZmlsdGVyIHJlc3VsdC5cbiAqL1xudHlwZSBGaWx0ZXJGdW5jdGlvbiA9IChkYXRhOiBLZXBzT2JqZWN0KSA9PiBib29sZWFuO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdrZXBzLWR5bmFtaWMtdGFibGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vZHluYW1pYy10YWJsZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2R5bmFtaWMtdGFibGUuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBEeW5hbWljVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQsIEFmdGVyQ29udGVudEluaXQge1xuICAvKipcbiAgICogTmFtZSBvZiB0aGUgbW9kZWwgZm9yIHRoZSB0YWJsZS5cbiAgICovXG4gIEBJbnB1dCgpIG1vZGVsOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFNjaGVtYSBvZiB0aGUgbW9kZWwuXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxuICAgKi9cbiAgc2NoZW1hOiBLZXBzU2NoZW1hO1xuXG4gIC8qKlxuICAgKiBEYXRhIHRvIGJlIHNob3duIG9uIHRoZSB0YWJsZSBmcm9tIHRoZSBHcmFwaFFMIHF1ZXJ5LlxuICAgKiBAcmVhZG9ubHlcbiAgICovXG4gIGRhdGE6IEtlcHNPYmplY3RbXSA9IFtdO1xuXG4gIC8qKlxuICAgKiBDb21tYS1zZXBhcmF0ZWQgbGlzdCBvZiBmaWVsZHMgZm9yIHRoZSBHcmFwaFFMIHF1ZXJ5LlxuICAgKi9cbiAgQElucHV0KCkgcXVlcnk6IHN0cmluZztcblxuICAvKipcbiAgICogQWRkaXRpb25hbCBmaWx0ZXIgdG8gYmUgYWRkZWQgdG8gdGhlIEdyYXBoUUwgcXVlcnkuXG4gICAqL1xuICBASW5wdXQoKSBmaWx0ZXJRdWVyeTogUmVjb3JkPHN0cmluZywgc3RyaW5nPjtcblxuICAvKipcbiAgICogQ29sdW1ucyB0byBiZSBkaXNwbGF5ZWQgb24gdGhlIHRhYmxlLlxuICAgKi9cbiAgQElucHV0KCkgZGlzcGxheWVkQ29sdW1uczogc3RyaW5nW10gPSBbXTtcblxuICAvKipcbiAgICogQ29sdW1ucyB0byBiZSBoaWRkZW4gb24gdGhlIHRhYmxlLlxuICAgKi9cbiAgQElucHV0KCkgaGlkZGVuQ29sdW1uczogc3RyaW5nW10gPSBbXTtcblxuICAvKipcbiAgICogQ29sdW1ucyB0byBiZSBkaXNwbGF5ZWQgb24gdGhlIHRhYmxlIGJhc2VkIG9uIGRpc3BsYXllZENvbHVtbnMgYW5kIGhpZGRlbkNvbHVtbnMuXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxuICAgKi9cbiAgYWxsQ29sdW1uczogc3RyaW5nW10gPSBbXTtcblxuICAvKipcbiAgICogUXVlcmllcyBhbGwgYFRhYmxlQ29sRGlyZWN0aXZlYCBkYXRhIGluc2lkZSB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgQENvbnRlbnRDaGlsZHJlbihUYWJsZUNvbERpcmVjdGl2ZSkgcHJpdmF0ZSBjdXN0b21Db2xEYXRhczogUXVlcnlMaXN0PFRhYmxlQ29sRGlyZWN0aXZlPjtcblxuICAvKipcbiAgICogUXVlcmllcyBhbGwgYFRhYmxlQ29sRGlyZWN0aXZlYCB0ZW1wbGF0ZXMgaW5zaWRlIHRoZSBjb21wb25lbnQuXG4gICAqL1xuICBAQ29udGVudENoaWxkcmVuKFRhYmxlQ29sRGlyZWN0aXZlLCB7IHJlYWQ6IFRlbXBsYXRlUmVmIH0pIHByaXZhdGUgY3VzdG9tQ29sVGVtcGxhdGVzOiBRdWVyeUxpc3Q8VGVtcGxhdGVSZWY8YW55Pj47XG5cbiAgLyoqXG4gICAqIEEgcmVjb3JkIG9mIGN1c3RvbSBjb2x1bW4gZGF0YSBhbmQgdGVtcGxhdGUga2V5ZWQgYnkgdGhlIGN1c3RvbSBjb2x1bW4gSUQuXG4gICAqL1xuICBjdXN0b21Db2xzOiBSZWNvcmQ8c3RyaW5nLCBDdXN0b21Db2x1bW4+ID0ge307XG5cbiAgLyoqXG4gICAqIFJlZmVyZW5jZSB0byB0aGUgTWF0UGFnaW5hdG9yLlxuICAgKi9cbiAgQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XG5cbiAgLyoqXG4gICAqIEEga2V5IHVzZWQgd2hlbiBzYXZpbmcgcGFnZSBzaXplIG51bWJlciB0byB0aGUgbG9jYWwgc3RvcmFnZS5cbiAgICovXG4gIEBJbnB1dCgpIHNhdmVQYWdlU2l6ZTogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBUaGUgdGFibGUncyBkZWZhdWx0IHBhZ2luYXRvciBwYWdlIHNpemUuXG4gICAqL1xuICBASW5wdXQoKSBkZWZhdWx0UGFnZVNpemUgPSAxMDtcblxuICAvKipcbiAgICogVGhlIGN1cnJlbnQgcGFnZSBzaXplIG9uIHBhZ2luYXRvci5cbiAgICovXG4gIHBhZ2VTaXplOiBudW1iZXI7XG5cbiAgLyoqXG4gICAqIExlbmd0aCBvZiB0aGUgZGF0YSB0byBiZSBzaG93biBvbiB0aGUgcGFnaW5hdG9yLlxuICAgKiBAcmVhZG9ubHlcbiAgICovXG4gIGRhdGFMZW5ndGggPSBJbmZpbml0eTtcblxuICAvKipcbiAgICogV2hldGhlciB0aGUgZGF0YSBsZW5ndGggaGFzIGJlZW4ga25vd24gKGZyb20gbGFzdCBxdWVyeSkgb3Igbm90LlxuICAgKi9cbiAgcHJpdmF0ZSBpc0RhdGFMZW5ndGhTZXQgPSBmYWxzZTtcblxuICAvKipcbiAgICogUmVmZXJlbmNlIHRvIHRoZSBNYXRTb3J0LlxuICAgKi9cbiAgQFZpZXdDaGlsZChNYXRTb3J0LCB7IHN0YXRpYzogdHJ1ZSB9KSBwcml2YXRlIHNvcnQ6IE1hdFNvcnQ7XG5cbiAgLyoqXG4gICAqIFRoZSBJRCBvZiB0aGUgY29sdW1uIHRoYXQgc2hvdWxkIGJlIHNvcnRlZC5cbiAgICovXG4gIEBJbnB1dCgpIGFjdGl2ZVNvcnQ6IHN0cmluZztcblxuICAvKipcbiAgICogV2hldGhlciB0aGUgdGFibGUgc2hvdWxkIHNob3cgdGhlIGRlZmF1bHQgc2VhcmNoIGJveCBvciBub3QuXG4gICAqL1xuICBASW5wdXQoKSBzaG93U2VhcmNoID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIEZpZWxkIHVzZWQgZm9yIHRoZSBzZWFyY2guXG4gICAqL1xuICBASW5wdXQoKSBzZWFyY2hGaWVsZHM6IHN0cmluZ1tdO1xuXG4gIC8qKlxuICAgKiBTdHJpbmcgdG8gYmUgc2VhcmNoZWQgYmFzZWQgb24gYHNlYXJjaEZpZWxkc2AuXG4gICAqL1xuICBwcml2YXRlIF9zZWFyY2g6IHN0cmluZztcblxuICAvKipcbiAgICogQSBzdWJqZWN0IHRoYXQgZW1pdHMgZXZlcnkgdGltZSB0aGUgc2VhcmNoIHN0cmluZyBjaGFuZ2VzLlxuICAgKi9cbiAgc2VhcmNoQ2hhbmdlID0gbmV3IFN1YmplY3Q8c3RyaW5nPigpO1xuXG4gIC8qKlxuICAgKiBTdHJpbmcgdG8gYmUgc2VhcmNoZWQgYmFzZWQgb24gYHNlYXJjaEZpZWxkc2AuXG4gICAqL1xuICBnZXQgc2VhcmNoKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIHRoaXMuX3NlYXJjaDtcbiAgfVxuICBASW5wdXQoJ3NlYXJjaCcpXG4gIHNldCBzZWFyY2godmFsdWU6IHN0cmluZykge1xuICAgIHRoaXMuX3NlYXJjaCA9IHZhbHVlO1xuICAgIHRoaXMuc2VhcmNoQ2hhbmdlLm5leHQodmFsdWUpO1xuICB9XG5cbiAgLyoqXG4gICAqIERlbGF5IGZvciB0aGUgc2VhcmNoIGZ1bmN0aW9uIHRvIGJlIGNhbGxlZCBpbiBtaWxsaXNlY29uZHMuXG4gICAqL1xuICBASW5wdXQoKSBzZWFyY2hEZWxheSA9IDEwMDA7XG5cbiAgLyoqXG4gICAqIEZ1bmN0aW9uIHRvIGZpbHRlciB0aGUgcmVzdWx0IGFmdGVyIHF1ZXJ5LlxuICAgKi9cbiAgcHJpdmF0ZSBfZmlsdGVyRnVuY3Rpb246IEZpbHRlckZ1bmN0aW9uO1xuXG4gIC8qKlxuICAgKiBBIHN1YmplY3QgdGhhdCBlbWl0cyBldmVyeSB0aW1lIHRoZSBmaWx0ZXIgZnVuY3Rpb24gY2hhbmdlcy5cbiAgICovXG4gIGZpbHRlckZ1bmN0aW9uQ2hhbmdlID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcblxuICAvKipcbiAgICogRnVuY3Rpb24gdG8gZmlsdGVyIHRoZSByZXN1bHQgYWZ0ZXIgcXVlcnkuXG4gICAqL1xuICBnZXQgZmlsdGVyRnVuY3Rpb24oKTogRmlsdGVyRnVuY3Rpb24ge1xuICAgIHJldHVybiB0aGlzLl9maWx0ZXJGdW5jdGlvbjtcbiAgfVxuICBASW5wdXQoJ2ZpbHRlckZ1bmN0aW9uJylcbiAgc2V0IGZpbHRlckZ1bmN0aW9uKHZhbHVlOiBGaWx0ZXJGdW5jdGlvbikge1xuICAgIHRoaXMuX2ZpbHRlckZ1bmN0aW9uID0gdmFsdWU7XG4gICAgdGhpcy5maWx0ZXJGdW5jdGlvbkNoYW5nZS5uZXh0KCk7XG4gIH1cblxuICAvKipcbiAgICogQW4gZXZlbnQgZW1pdHRlciB0aGF0IGVtaXRzIHRoZSBvYmplY3Qgb2YgdGhlIHJvdyBjbGlja2VkIGJ5IHRoZSB1c2VyLlxuICAgKi9cbiAgQE91dHB1dCgpIHJvd0NsaWNrID0gbmV3IEV2ZW50RW1pdHRlcjxLZXBzT2JqZWN0PigpO1xuXG4gIC8qKlxuICAgKiBXaGV0aGVyIHRoZSB0YWJsZSBpcyBjdXJyZW50bHkgbG9hZGluZyBvciBub3QuXG4gICAqIEByZWFkb25seVxuICAgKi9cbiAgdGFibGVMb2FkaW5nID0gdHJ1ZTtcblxuICAvKipcbiAgICogV2hldGhlciB0aGUgdGFibGUgaGFzIGRvbmUgaW5pdGlhbGl6aW5nIG9yIG5vdC5cbiAgICogQHJlYWRvbmx5XG4gICAqL1xuICBpbml0aWFsaXplZCA9IGZhbHNlO1xuXG4gIC8qKlxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcbiAgICovXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoJ01PREVMUycpIHB1YmxpYyBtb2RlbHM6IEtlcHNNb2RlbHMsXG4gICAgcHJpdmF0ZSBkYXRhU2VydmljZTogRGF0YVNlcnZpY2VcbiAgKSB7IH1cblxuICAvKipcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXG4gICAqL1xuICBuZ09uSW5pdCgpIHtcbiAgICAvLyBHZXQgdGhlIHByZXZpb3VzIHNlc3Npb24ncyBwYWdlIHNpemUgaWYgYW55LCBvciB1c2UgdGhlIGRlZmF1bHQgcGFnZSBzaXplLlxuICAgIHRoaXMucGFnZVNpemUgPSBOdW1iZXIobG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy5zYXZlUGFnZVNpemUpKSB8fCB0aGlzLmRlZmF1bHRQYWdlU2l6ZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcbiAgICovXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICAvLyBHZXQgZGF0YSBhcyBzb29uIGFzIHZpZXcgaW5pdGlhbGl6ZWQuXG4gICAgdGhpcy5nZXREYXRhKCk7XG5cbiAgICAvLyBTZXR1cCBkZWxheWVkIHNlYXJjaC5cbiAgICBjb25zdCBkZWxheWVkU2VhcmNoID0gdGhpcy5zZWFyY2hDaGFuZ2VcbiAgICAucGlwZShcbiAgICAgIGRlYm91bmNlVGltZSh0aGlzLnNlYXJjaERlbGF5KSxcbiAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKClcbiAgICApO1xuXG4gICAgLy8gSWYgYW55IG9mIHRoZXNlIG9ic2VydmFibGVzIGVtaXQsIHRoZW4gd2UgcmVzZXQgdGhlIHBhZ2luYXRvci5cbiAgICBkZWxheWVkU2VhcmNoLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICB0aGlzLnBhZ2luYXRvci5wYWdlSW5kZXggPSAwO1xuICAgICAgdGhpcy5kYXRhTGVuZ3RoID0gSW5maW5pdHk7XG4gICAgfSk7XG4gICAgdGhpcy5maWx0ZXJGdW5jdGlvbkNoYW5nZS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgdGhpcy5wYWdpbmF0b3IucGFnZUluZGV4ID0gMDtcbiAgICAgIHRoaXMuZGF0YUxlbmd0aCA9IEluZmluaXR5O1xuICAgIH0pO1xuICAgIHRoaXMuc29ydC5zb3J0Q2hhbmdlLnN1YnNjcmliZSgoKSA9PiB0aGlzLnBhZ2luYXRvci5wYWdlSW5kZXggPSAwKTtcblxuICAgIC8vIFNhdmUgcGFnZSBzaXplIGlmIHBhZ2luYXRvciBjaGFuZ2VzLlxuICAgIGlmICh0aGlzLnNhdmVQYWdlU2l6ZSkge1xuICAgICAgdGhpcy5wYWdpbmF0b3IucGFnZS5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnNhdmVQYWdlU2l6ZSwgdGhpcy5wYWdpbmF0b3IucGFnZVNpemUudG9TdHJpbmcoKSk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBJZiBhbnkgb2YgdGhlc2Ugb2JzZXJ2YWJsZXMgZW1pdCwgdGhlbiB3ZSByZWZyZXNoIHRoZSBkYXRhLlxuICAgIG1lcmdlKHRoaXMuc29ydC5zb3J0Q2hhbmdlLCB0aGlzLnBhZ2luYXRvci5wYWdlLCBkZWxheWVkU2VhcmNoLCB0aGlzLmZpbHRlckZ1bmN0aW9uQ2hhbmdlKVxuICAgIC5zdWJzY3JpYmUoKCkgPT4gdGhpcy5nZXREYXRhKCkpO1xuICB9XG5cbiAgLyoqXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxuICAgKi9cbiAgbmdBZnRlckNvbnRlbnRJbml0KCkge1xuICAgIC8vIFdlIGFyZSB1c2luZyBgbmdBZnRlckNvbnRlbnRJbml0YCBoZXJlIGJlY2F1c2Ugd2UgY2FuXG4gICAgLy8gb25seSBnZXQgdGhlIGN1c3RvbSBjb2x1bW5zIGRhdGEgYWZ0ZXIgdGhlIGNvbXBvbmVudCBjb250ZW50XG4gICAgLy8gaGFzIGJlZW4gaW5pdGlhbGl6ZWQuXG5cbiAgICAvLyBHZXQgc2NoZW1hIGZyb20gbW9kZWwgbmFtZSBpZiBzdXBwbGllZC5cbiAgICBpZiAodGhpcy5tb2RlbCAmJiB0aGlzLm1vZGVsc1t0aGlzLm1vZGVsXSkge1xuICAgICAgdGhpcy5zY2hlbWEgPSB0aGlzLm1vZGVsc1t0aGlzLm1vZGVsXS5zY2hlbWE7XG4gICAgfSBlbHNlIGlmICh0aGlzLm1vZGVsICYmICF0aGlzLm1vZGVsc1t0aGlzLm1vZGVsXSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGBDYW4ndCBmaW5kIGEgbW9kZWwgbmFtZWQgJHt0aGlzLm1vZGVsfSFgKTtcbiAgICB9XG5cbiAgICAvLyBBc3NlcnQgc2NoZW1hIG11c3QgZXhpc3QuXG4gICAgaWYgKCF0aGlzLnNjaGVtYSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKGBNdXN0IHN1cHBseSB2YWxpZCBtb2RlbCBuYW1lIHRvIEtlcHMgZHluYW1pYyB0YWJsZSFgKTtcbiAgICB9XG5cbiAgICAvLyBTZXQgYGFsbENvbHVtbnNgIHRvIGJlIGV2ZXJ5IHB1YmxpYyBmaWVsZCBpbiB0aGUgc2NoZW1hLlxuICAgIHRoaXMuYWxsQ29sdW1ucyA9IE9iamVjdC5rZXlzKHRoaXMuc2NoZW1hKS5maWx0ZXIoZmllbGQgPT4gZmllbGRbMF0gIT09ICdfJyk7XG4gICAgXG4gICAgLy8gUHJvY2VzcyBjdXN0b20gY29sdW1ucy5cbiAgICBjb25zdCBjb2xEYXRhcyA9IHRoaXMuY3VzdG9tQ29sRGF0YXMudG9BcnJheSgpO1xuICAgIGNvbnN0IGNvbFRlbXBsYXRlcyA9IHRoaXMuY3VzdG9tQ29sVGVtcGxhdGVzLnRvQXJyYXkoKTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNvbERhdGFzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBjb25zdCBjb2xJZCA9IGNvbERhdGFzW2ldLmlkO1xuICAgICAgdGhpcy5jdXN0b21Db2xzW2NvbElkXSA9IHtcbiAgICAgICAgZGF0YTogY29sRGF0YXNbaV0sXG4gICAgICAgIHRlbXBsYXRlOiBjb2xUZW1wbGF0ZXNbaV1cbiAgICAgIH07XG4gICAgICB0aGlzLmFsbENvbHVtbnMucHVzaChjb2xJZCk7XG4gICAgfVxuICAgIFxuICAgIC8vIElmIGBkaXNwbGF5ZWRDb2x1bW5zYCBpcyBub3QgcHJvdmlkZWQsIHVzZSBgYWxsQ29sdW1uc2AgZmlsdGVyZWQgYnkgYGhpZGRlbkNvbHVtbnNgLlxuICAgIGlmICh0aGlzLmRpc3BsYXllZENvbHVtbnMubGVuZ3RoID09PSAwKSB7XG4gICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSB0aGlzLmFsbENvbHVtbnMuZmlsdGVyKGZpZWxkID0+ICF0aGlzLmhpZGRlbkNvbHVtbnMuaW5jbHVkZXMoZmllbGQpKTtcbiAgICB9XG5cbiAgICB0aGlzLmluaXRpYWxpemVkID0gdHJ1ZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDb21iaW5lcyBwcm9wZXJ0aWVzIGFzIEdyYXBoUUwgcXVlcnkgYW5kIGdldHMgZGF0YSBmcm9tIGRhdGEgc2VydmljZS5cbiAgICovXG4gIGFzeW5jIGdldERhdGEoKSB7XG4gICAgdHJ5IHtcbiAgICAgIHRoaXMudGFibGVMb2FkaW5nID0gdHJ1ZTtcblxuICAgICAgY29uc3QgZmlsdGVyczogUmVjb3JkPHN0cmluZywgYW55PiA9IHt9O1xuXG4gICAgICAvLyBTZXR1cCBwYWdlIHF1ZXJ5LlxuICAgICAgY29uc3QgeyBwYWdlSW5kZXgsIHBhZ2VTaXplIH0gPSB0aGlzLnBhZ2luYXRvcjtcbiAgICAgIGZpbHRlcnMub2Zmc2V0ID0gcGFnZUluZGV4ICogcGFnZVNpemU7XG5cbiAgICAgIC8vIFNldHVwIHNvcnQgcXVlcnkuXG4gICAgICBjb25zdCB7IGFjdGl2ZSwgZGlyZWN0aW9uIH0gPSB0aGlzLnNvcnQ7XG4gICAgICBpZiAoYWN0aXZlKSB7XG4gICAgICAgIGZpbHRlcnMuc29ydCA9IChkaXJlY3Rpb24gPT09ICdkZXNjJyA/ICctJyA6ICcrJykgKyBhY3RpdmU7XG4gICAgICB9XG5cbiAgICAgIC8vIFNldHVwIHNlYXJjaCBxdWVyeS5cbiAgICAgIGlmICh0aGlzLnNlYXJjaEZpZWxkcyAmJiB0aGlzLnNlYXJjaCkge1xuICAgICAgICBsZXQgbW9uZ29GaWx0ZXIgPSBgIHsgJyRvcic6IFtgO1xuICAgICAgICBmb3IgKGxldCBmaWVsZCBvZiB0aGlzLnNlYXJjaEZpZWxkcykge1xuICAgICAgICAgIG1vbmdvRmlsdGVyICs9IGB7JyR7ZmllbGR9Jzoge1xuICAgICAgICAgICAgJyRyZWdleCc6ICcke3RoaXMuc2VhcmNoLnJlcGxhY2UoJygnLCAnJTI4JykucmVwbGFjZSgnKScsICclMjknKX0nLFxuICAgICAgICAgICAgJyRvcHRpb25zJzogJ2knIH19LGBcbiAgICAgICAgfVxuICAgICAgICBtb25nb0ZpbHRlciA9IF8udHJpbUVuZChtb25nb0ZpbHRlciwgJywnKTtcbiAgICAgICAgbW9uZ29GaWx0ZXIgKz0gJ119JztcbiAgICAgICAgZmlsdGVycy5tb25nbyA9IG1vbmdvRmlsdGVyO1xuICAgICAgfVx0XG5cbiAgICAgIC8vIFNldCBsaW1pdCBpZiB0aGVyZSBpcyBub3QgZmlsdGVyIGZ1bmN0aW9uLlxuICAgICAgaWYgKCF0aGlzLmZpbHRlckZ1bmN0aW9uKSB7XG4gICAgICAgIGZpbHRlcnMubGltaXQgPSBwYWdlU2l6ZTtcbiAgICAgIH1cblxuICAgICAgLy8gR2V0IGRhdGEgZnJvbSBkYXRhIHNlcnZpY2UuXG4gICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCB0aGlzLmRhdGFTZXJ2aWNlLmdyYXBocWwoYFxuICAgICAgICAke3RoaXMubW9kZWx9cyhcbiAgICAgICAgICAke3RoaXMuY29udmVydEZpbHRlcihPYmplY3QuYXNzaWduKGZpbHRlcnMsIHRoaXMuZmlsdGVyUXVlcnkpKX1cbiAgICAgICAgKXske3RoaXMucXVlcnl9fVxuICAgICAgYCk7XG4gICAgICBjb25zdCBkYXRhID0gcmVzdWx0LmRhdGFbdGhpcy5tb2RlbCArICdzJ107XG5cbiAgICAgIC8vIElmIGZpbHRlciBmdW5jdGlvbiBleGlzdHMsIGZpbHRlciB3aXRoIHRoYXQgZnVuY3Rpb24uXG4gICAgICBpZiAodGhpcy5maWx0ZXJGdW5jdGlvbikge1xuICAgICAgICB0aGlzLmRhdGEgPSBfLmZpbHRlcihkYXRhLCB0aGlzLmZpbHRlckZ1bmN0aW9uKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIElmIHRoaXMgaXMgbm90IHRoZSBmaXJzdCBwYWdlIGFuZCBkYXRhIGlzIGVtcHR5LFxuICAgICAgICAvLyB0aGVuIHdlIGdvIGJhY2sgdG8gdGhlIGxhc3QgcGFnZS5cbiAgICAgICAgaWYgKHBhZ2VJbmRleCA+IDEgJiYgZGF0YS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAvLyBDYWxjdWxhdGUgZGF0YSBsZW5ndGguXG4gICAgICAgICAgdGhpcy5kYXRhTGVuZ3RoID0gKHBhZ2VJbmRleCAtIDEpICogcGFnZVNpemUgKyB0aGlzLmRhdGEubGVuZ3RoO1xuICAgICAgICAgIHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCA9IHBhZ2VJbmRleCAtIDE7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gSWYgZGF0YSBsZW5ndGggaXMgc21hbGxlciB0aGFuIHBhZ2Ugc2l6ZSxcbiAgICAgICAgICAvLyB3ZSBhc3N1bWUgd2UncmUgb24gbGFzdCBwYWdlIGFuZCByZWNhbGN1bGF0ZSBkYXRhIGxlbmd0aC5cbiAgICAgICAgICBpZiAoZGF0YS5sZW5ndGggPCBwYWdlU2l6ZSkge1xuICAgICAgICAgICAgdGhpcy5kYXRhTGVuZ3RoID0gcGFnZUluZGV4ICogcGFnZVNpemUgKyBkYXRhLmxlbmd0aDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBTZXQgZGF0YSBhcyBpcy5cbiAgICAgICAgICB0aGlzLmRhdGEgPSBkYXRhO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHRoaXMudGFibGVMb2FkaW5nID0gZmFsc2U7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGVycik7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIENvbnZlcnRzIGEgZmlsdGVyIG9iamVjdCB0byBNb25nbyBmaWx0ZXIgc3RyaW5nLlxuICAgKiBAcGFyYW0gZmlsdGVycyBQYWlyIG9mIGtleXMgJiB2YWx1ZXMgdG8gYmUgY29udmVydGVkIHRvIE1vbmdvIGZpbHRlci5cbiAgICovXG4gIHByaXZhdGUgY29udmVydEZpbHRlcihmaWx0ZXJzOiBSZWNvcmQ8c3RyaW5nLCBzdHJpbmc+KTogc3RyaW5nIHtcbiAgICBsZXQgc3RyID0gJyc7XG4gICAgZm9yIChjb25zdCBmaWx0ZXJOYW1lIGluIGZpbHRlcnMpIHtcbiAgICAgIGlmIChmaWx0ZXJOYW1lID09PSAnbGltaXQnIHx8IGZpbHRlck5hbWUgPT09ICdvZmZzZXQnKSB7XG4gICAgICAgIHN0ciArPSBgJHtmaWx0ZXJOYW1lfToke2ZpbHRlcnNbZmlsdGVyTmFtZV19LGA7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzdHIgKz0gYCR7ZmlsdGVyTmFtZX06XCIke2ZpbHRlcnNbZmlsdGVyTmFtZV19XCIsYDtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHN0cjtcbiAgfVxuXG4gIC8qKlxuICAgKiBDaGVja3MgdGhlIHR5cGUgb2YgYSBmaWVsZC5cbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXG4gICAqIEBwYXJhbSBmaWVsZCBGaWVsZCB0byBiZSBjaGVja2VkLlxuICAgKiBAcGFyYW0gdHlwZSBUeXBlIHRvIGJlIGNoZWNrZWQuXG4gICAqL1xuICBpc1R5cGUoZmllbGQ6IHN0cmluZywgdHlwZTogc3RyaW5nKSB7XG4gICAgcmV0dXJuIGlzVHlwZSh0aGlzLnNjaGVtYSwgZmllbGQsIHR5cGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIGRpc3BsYXkgZXhwcmVzc2lvbiBvZiBhIHJlZmVyZW5jZSBmaWVsZC5cbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXG4gICAqIEBwYXJhbSBmaWVsZCBGaWVsZCB0byBnZXQgZnJvbS5cbiAgICovXG4gIGdldFJlZmVyZW5jZURpc3BsYXkoZmllbGQ6IHN0cmluZykge1xuICAgIHJldHVybiBnZXRSZWZlcmVuY2VEaXNwbGF5KHRoaXMubW9kZWxzLCB0aGlzLnNjaGVtYSwgZmllbGQpO1xuICB9XG5cbiAgLyoqXG4gICAqIFJldHVybnMgdGhlIGRpc3BsYXkgZXhwcmVzc2lvbiBvZiBhbiBhcnJheSBvZiByZWZlcmVuY2VzIGZpZWxkLlxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcbiAgICogQHBhcmFtIGZpZWxkIEZpZWxkIHRvIGdldCBmcm9tLlxuICAgKi9cbiAgZ2V0UmVmZXJlbmNlRGlzcGxheUFycmF5KGZpZWxkOiBzdHJpbmcpIHtcbiAgICByZXR1cm4gZ2V0UmVmZXJlbmNlRGlzcGxheUFycmF5KHRoaXMubW9kZWxzLCB0aGlzLnNjaGVtYSwgZmllbGQpO1xuICB9XG5cbn1cbiJdfQ==