/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { FormArray, FormControl } from '@angular/forms';
import _ from 'lodash';
import { KepsForm } from './keps-form';
/**
 * A Keps form for array field.
 *
 * It extends Angular's `FormArray` class
 * and changes its `patchValue` and `reset` methods to
 * deal with arrays appropriately.
 */
export class KepsFormArray extends FormArray {
    /**
     * Set an optional BehaviorSubject counter.
     * @param {?} counter$ A BehaviorSubject from parent component.
     * @return {?}
     */
    setCounter$(counter$) {
        this.counter$ = counter$;
    }
    /**
     * Patches the value of the `KepsFormArray`.
     * It accepts an array that matches the structure of the control,
     * and does its best to match the values to the correct controls in the group.
     *
     * It accepts both super-sets and sub-sets of the array without throwing an error.
     * @param {?} values Array of latest values for the controls
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    patchValue(values, options = {}) {
        values = values.map((/**
         * @param {?} value
         * @return {?}
         */
        value => {
            if (_.isPlainObject(value) && value._id && value._id) {
                return value._id;
            }
            else {
                return value;
            }
        }));
        // Reset the form first.
        this.reset();
        // Add each form control with value and counter.
        for (let i = 0; i < values.length; i++) {
            /** @type {?} */
            const value = values[i];
            // If value is an object, then we assume that we're making a KepsForm.
            if (_.isPlainObject(value)) {
                this.push(new KepsForm({}));
                this.at(this.length - 1).patchValue(value);
            }
            else {
                this.push(new FormControl(value));
            }
        }
        // Let the parent component know that the length changed.
        // The `setTimeout` is here in case the `setCounter$` function is called after `patchValue`.
        setTimeout((/**
         * @return {?}
         */
        () => {
            if (this.counter$) {
                this.counter$.next(this.length);
            }
        }), 5);
        this.updateValueAndValidity(options);
    }
    /**
     * Resets the FormArray and all descendants are marked `pristine` and `untouched`,
     * and the value of all descendants to null or null maps.
     *
     * You reset to a specific form state by passing in an array of states that matches the structure of the control.
     * The state is a standalone value or a form state object with both a value and a disabled status.
     * @param {?=} values Array of latest values for the controls
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    reset(values, options = {}) {
        while (this.length > 0) {
            this.removeAt(0);
        }
        if (this.counter$) {
            this.counter$.next(0);
        }
        this.markAsPristine(options);
        this.markAsUntouched(options);
        this.updateValueAndValidity(options);
    }
}
if (false) {
    /**
     * Let the parent knows if there is update to the controls array length.
     * @type {?}
     * @private
     */
    KepsFormArray.prototype.counter$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2Vwcy1mb3JtLWFycmF5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvZm9ybS9rZXBzLWZvcm0tYXJyYXkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFeEQsT0FBTyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxhQUFhLENBQUM7Ozs7Ozs7O0FBU3ZDLE1BQU0sT0FBTyxhQUFjLFNBQVEsU0FBUzs7Ozs7O0lBVTFDLFdBQVcsQ0FBQyxRQUFpQztRQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztJQUMzQixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFrQkQsVUFBVSxDQUNSLE1BQWEsRUFDYixVQUF3RCxFQUFFO1FBRTFELE1BQU0sR0FBRyxNQUFNLENBQUMsR0FBRzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzFCLElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxHQUFHLEVBQUU7Z0JBQ3BELE9BQU8sS0FBSyxDQUFDLEdBQUcsQ0FBQzthQUNsQjtpQkFBTTtnQkFDTCxPQUFPLEtBQUssQ0FBQzthQUNkO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFFSCx3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRWIsZ0RBQWdEO1FBQ2hELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztrQkFDaEMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDdkIsc0VBQXNFO1lBQ3RFLElBQUksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzVDO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzthQUNuQztTQUNGO1FBRUQseURBQXlEO1FBQ3pELDRGQUE0RjtRQUM1RixVQUFVOzs7UUFBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNqQztRQUNILENBQUMsR0FBRSxDQUFDLENBQUMsQ0FBQztRQUVOLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFrQkQsS0FBSyxDQUNILE1BQWMsRUFDZCxVQUF3RCxFQUFFO1FBRTFELE9BQU8sSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDdEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNsQjtRQUNELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN2QjtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDdkMsQ0FBQztDQUVGOzs7Ozs7O0lBL0ZDLGlDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1BcnJheSwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgeyBLZXBzRm9ybSB9IGZyb20gJy4va2Vwcy1mb3JtJztcblxuLyoqXG4gKiBBIEtlcHMgZm9ybSBmb3IgYXJyYXkgZmllbGQuXG4gKlxuICogSXQgZXh0ZW5kcyBBbmd1bGFyJ3MgYEZvcm1BcnJheWAgY2xhc3NcbiAqIGFuZCBjaGFuZ2VzIGl0cyBgcGF0Y2hWYWx1ZWAgYW5kIGByZXNldGAgbWV0aG9kcyB0b1xuICogZGVhbCB3aXRoIGFycmF5cyBhcHByb3ByaWF0ZWx5LlxuICovXG5leHBvcnQgY2xhc3MgS2Vwc0Zvcm1BcnJheSBleHRlbmRzIEZvcm1BcnJheSB7XG4gIC8qKlxuICAgKiBMZXQgdGhlIHBhcmVudCBrbm93cyBpZiB0aGVyZSBpcyB1cGRhdGUgdG8gdGhlIGNvbnRyb2xzIGFycmF5IGxlbmd0aC5cbiAgICovXG4gIHByaXZhdGUgY291bnRlciQ6IEJlaGF2aW9yU3ViamVjdDxudW1iZXI+O1xuXG4gIC8qKlxuICAgKiBTZXQgYW4gb3B0aW9uYWwgQmVoYXZpb3JTdWJqZWN0IGNvdW50ZXIuXG4gICAqIEBwYXJhbSBjb3VudGVyJCBBIEJlaGF2aW9yU3ViamVjdCBmcm9tIHBhcmVudCBjb21wb25lbnQuXG4gICAqL1xuICBzZXRDb3VudGVyJChjb3VudGVyJDogQmVoYXZpb3JTdWJqZWN0PG51bWJlcj4pIHtcbiAgICB0aGlzLmNvdW50ZXIkID0gY291bnRlciQ7XG4gIH1cblxuICAvKipcbiAgICogUGF0Y2hlcyB0aGUgdmFsdWUgb2YgdGhlIGBLZXBzRm9ybUFycmF5YC5cbiAgICogSXQgYWNjZXB0cyBhbiBhcnJheSB0aGF0IG1hdGNoZXMgdGhlIHN0cnVjdHVyZSBvZiB0aGUgY29udHJvbCxcbiAgICogYW5kIGRvZXMgaXRzIGJlc3QgdG8gbWF0Y2ggdGhlIHZhbHVlcyB0byB0aGUgY29ycmVjdCBjb250cm9scyBpbiB0aGUgZ3JvdXAuXG4gICAqXG4gICAqIEl0IGFjY2VwdHMgYm90aCBzdXBlci1zZXRzIGFuZCBzdWItc2V0cyBvZiB0aGUgYXJyYXkgd2l0aG91dCB0aHJvd2luZyBhbiBlcnJvci5cbiAgICogQHBhcmFtIHZhbHVlcyBBcnJheSBvZiBsYXRlc3QgdmFsdWVzIGZvciB0aGUgY29udHJvbHNcbiAgICogQHBhcmFtIG9wdGlvbnNcbiAgICogQ29uZmlndXJhdGlvbiBvcHRpb25zIHRoYXQgZGV0ZXJtaW5lIGhvdyB0aGUgY29udHJvbCBwcm9wYWdhdGVzIGNoYW5nZXMgYW5kIGVtaXRzIGV2ZW50cyBhZnRlciB0aGUgdmFsdWUgaXMgcGF0Y2hlZC5cbiAgICogLSBgb25seVNlbGZgOiBXaGVuIHRydWUsIGVhY2ggY2hhbmdlIG9ubHkgYWZmZWN0cyB0aGlzIGNvbnRyb2wgYW5kIG5vdCBpdHMgcGFyZW50LiBEZWZhdWx0IGlzIHRydWUuXG4gICAqIC0gYGVtaXRFdmVudGA6XG4gICAqIFdoZW4gdHJ1ZSBvciBub3Qgc3VwcGxpZWQgKHRoZSBkZWZhdWx0KSxcbiAgICogYm90aCB0aGUgYHN0YXR1c0NoYW5nZXNgIGFuZCBgdmFsdWVDaGFuZ2VzYCBvYnNlcnZhYmxlcyBlbWl0IGV2ZW50cyB3aXRoIHRoZSBsYXRlc3Qgc3RhdHVzIGFuZCB2YWx1ZSB3aGVuIHRoZSBjb250cm9sIHZhbHVlIGlzIHVwZGF0ZWQuXG4gICAqIFdoZW4gZmFsc2UsIG5vIGV2ZW50cyBhcmUgZW1pdHRlZC5cbiAgICogVGhlIGNvbmZpZ3VyYXRpb24gb3B0aW9ucyBhcmUgcGFzc2VkIHRvIHRoZSBgdXBkYXRlVmFsdWVBbmRWYWxpZGl0eWAgbWV0aG9kLlxuICAgKi9cbiAgcGF0Y2hWYWx1ZShcbiAgICB2YWx1ZXM6IGFueVtdLFxuICAgIG9wdGlvbnM6IHsgb25seVNlbGY/OiBib29sZWFuOyBlbWl0RXZlbnQ/OiBib29sZWFuOyB9ID0ge31cbiAgKTogdm9pZCB7XG4gICAgdmFsdWVzID0gdmFsdWVzLm1hcCh2YWx1ZSA9PiB7XG4gICAgICBpZiAoXy5pc1BsYWluT2JqZWN0KHZhbHVlKSAmJiB2YWx1ZS5faWQgJiYgdmFsdWUuX2lkKSB7XG4gICAgICAgIHJldHVybiB2YWx1ZS5faWQ7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICAvLyBSZXNldCB0aGUgZm9ybSBmaXJzdC5cbiAgICB0aGlzLnJlc2V0KCk7XG5cbiAgICAvLyBBZGQgZWFjaCBmb3JtIGNvbnRyb2wgd2l0aCB2YWx1ZSBhbmQgY291bnRlci5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHZhbHVlcy5sZW5ndGg7IGkrKykge1xuICAgICAgY29uc3QgdmFsdWUgPSB2YWx1ZXNbaV07XG4gICAgICAvLyBJZiB2YWx1ZSBpcyBhbiBvYmplY3QsIHRoZW4gd2UgYXNzdW1lIHRoYXQgd2UncmUgbWFraW5nIGEgS2Vwc0Zvcm0uXG4gICAgICBpZiAoXy5pc1BsYWluT2JqZWN0KHZhbHVlKSkge1xuICAgICAgICB0aGlzLnB1c2gobmV3IEtlcHNGb3JtKHt9KSk7XG4gICAgICAgIHRoaXMuYXQodGhpcy5sZW5ndGggLSAxKS5wYXRjaFZhbHVlKHZhbHVlKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMucHVzaChuZXcgRm9ybUNvbnRyb2wodmFsdWUpKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBMZXQgdGhlIHBhcmVudCBjb21wb25lbnQga25vdyB0aGF0IHRoZSBsZW5ndGggY2hhbmdlZC5cbiAgICAvLyBUaGUgYHNldFRpbWVvdXRgIGlzIGhlcmUgaW4gY2FzZSB0aGUgYHNldENvdW50ZXIkYCBmdW5jdGlvbiBpcyBjYWxsZWQgYWZ0ZXIgYHBhdGNoVmFsdWVgLlxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgaWYgKHRoaXMuY291bnRlciQpIHtcbiAgICAgICAgdGhpcy5jb3VudGVyJC5uZXh0KHRoaXMubGVuZ3RoKTtcbiAgICAgIH1cbiAgICB9LCA1KTtcblxuICAgIHRoaXMudXBkYXRlVmFsdWVBbmRWYWxpZGl0eShvcHRpb25zKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXNldHMgdGhlIEZvcm1BcnJheSBhbmQgYWxsIGRlc2NlbmRhbnRzIGFyZSBtYXJrZWQgYHByaXN0aW5lYCBhbmQgYHVudG91Y2hlZGAsXG4gICAqIGFuZCB0aGUgdmFsdWUgb2YgYWxsIGRlc2NlbmRhbnRzIHRvIG51bGwgb3IgbnVsbCBtYXBzLlxuICAgKlxuICAgKiBZb3UgcmVzZXQgdG8gYSBzcGVjaWZpYyBmb3JtIHN0YXRlIGJ5IHBhc3NpbmcgaW4gYW4gYXJyYXkgb2Ygc3RhdGVzIHRoYXQgbWF0Y2hlcyB0aGUgc3RydWN0dXJlIG9mIHRoZSBjb250cm9sLlxuICAgKiBUaGUgc3RhdGUgaXMgYSBzdGFuZGFsb25lIHZhbHVlIG9yIGEgZm9ybSBzdGF0ZSBvYmplY3Qgd2l0aCBib3RoIGEgdmFsdWUgYW5kIGEgZGlzYWJsZWQgc3RhdHVzLlxuICAgKiBAcGFyYW0gdmFsdWVzIEFycmF5IG9mIGxhdGVzdCB2YWx1ZXMgZm9yIHRoZSBjb250cm9sc1xuICAgKiBAcGFyYW0gb3B0aW9uc1xuICAgKiBDb25maWd1cmF0aW9uIG9wdGlvbnMgdGhhdCBkZXRlcm1pbmUgaG93IHRoZSBjb250cm9sIHByb3BhZ2F0ZXMgY2hhbmdlcyBhbmQgZW1pdHMgZXZlbnRzIGFmdGVyIHRoZSB2YWx1ZSBpcyBwYXRjaGVkLlxuICAgKiAtIGBvbmx5U2VsZmA6IFdoZW4gdHJ1ZSwgZWFjaCBjaGFuZ2Ugb25seSBhZmZlY3RzIHRoaXMgY29udHJvbCBhbmQgbm90IGl0cyBwYXJlbnQuIERlZmF1bHQgaXMgdHJ1ZS5cbiAgICogLSBgZW1pdEV2ZW50YDpcbiAgICogV2hlbiB0cnVlIG9yIG5vdCBzdXBwbGllZCAodGhlIGRlZmF1bHQpLFxuICAgKiBib3RoIHRoZSBgc3RhdHVzQ2hhbmdlc2AgYW5kIGB2YWx1ZUNoYW5nZXNgIG9ic2VydmFibGVzIGVtaXQgZXZlbnRzIHdpdGggdGhlIGxhdGVzdCBzdGF0dXMgYW5kIHZhbHVlIHdoZW4gdGhlIGNvbnRyb2wgdmFsdWUgaXMgdXBkYXRlZC5cbiAgICogV2hlbiBmYWxzZSwgbm8gZXZlbnRzIGFyZSBlbWl0dGVkLlxuICAgKiBUaGUgY29uZmlndXJhdGlvbiBvcHRpb25zIGFyZSBwYXNzZWQgdG8gdGhlIGB1cGRhdGVWYWx1ZUFuZFZhbGlkaXR5YCBtZXRob2QuXG4gICAqL1xuICByZXNldChcbiAgICB2YWx1ZXM/OiBhbnlbXSxcbiAgICBvcHRpb25zOiB7IG9ubHlTZWxmPzogYm9vbGVhbjsgZW1pdEV2ZW50PzogYm9vbGVhbjsgfSA9IHt9XG4gICk6IHZvaWQge1xuICAgIHdoaWxlICh0aGlzLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMucmVtb3ZlQXQoMCk7XG4gICAgfVxuICAgIGlmICh0aGlzLmNvdW50ZXIkKSB7XG4gICAgICB0aGlzLmNvdW50ZXIkLm5leHQoMCk7XG4gICAgfVxuICAgIHRoaXMubWFya0FzUHJpc3RpbmUob3B0aW9ucyk7XG4gICAgdGhpcy5tYXJrQXNVbnRvdWNoZWQob3B0aW9ucyk7XG4gICAgdGhpcy51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KG9wdGlvbnMpO1xuICB9XG5cbn1cbiJdfQ==