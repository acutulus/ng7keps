/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { KepsForm } from './keps-form';
import { KepsFormArray } from './keps-form-array';
import { isType, getReferenceDisplay } from '../../utils';
/**
 * A component that renders form based on Keps schema.
 */
export class FormComponent {
    /**
     * \@internal Do not use!
     * @param {?} models
     */
    constructor(models) {
        this.models = models;
        /**
         * An instance of Keps form.
         */
        this.form = new KepsForm({});
        /**
         * A map of reference to Keps objects to be shown for reference fields.
         *
         * Notes:-The key of the map is based on the field name of the Keps form model.
         *        Not the name of the model it is referenced to.
         *       -If field type is a string and a refObjs is provided,
         *        instead of a normal text input, it will be a select. see html for more info.
         *
         * **sample usage**
         * given the following models:
         *  PERSON {
         *    _id
         *    name: 'string',
         *    pet: ':ANIMAL'
         *  }
         *
         *  ANIMAL {
         *    _id
         *    type: 'string'
         *  }
         *
         *  if one wish to create a form for
         *  object PERSON where the options for ANIMAL
         *  comes from DB, that dev can create a refObjs:KepsReference
         *  which  will look like:
         *
         *  refObjs = {
         *              'pet': [
         *                <ANIMAL>{
         *                    _id
         *                    type
         *                  },
         *                <ANIMAL>{
         *                    _id
         *                    type
         *                },
         *                ...
         *                ...
         *                ..
         *              ],
         *            }
         *
         *  SIDENOTE: The annotation <TYPE> is used to make this exmaple more explicit.
         */
        this.refObjs = {};
        /**
         * Appearance style of the form.
         */
        this.appearance = 'standard';
        /**
         * Angular Material color of the form field underline and floating label.
         */
        this.color = 'primary';
        /**
         * Fields to be displayed on the form.
         */
        this.displayedFields = [];
        /**
         * Fields to be hidden on the form.
         */
        this.hiddenFields = [];
        /**
         * Placeholders to be displayed on fields.
         */
        this.placeholders = {};
        /**
         * Hints for fields input.
         */
        this.hints = {};
        /**
         * Whether the form has done initializing or not.
         */
        this.initialized = false;
    }
    /**
     * Initializes the form with fields based on the schema.
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        // Get schema from model name if supplied.
        if (this.model && this.models[this.model]) {
            this.schema = this.models[this.model].schema;
        }
        else if (this.model && !this.models[this.model]) {
            throw new Error(`Can't find a model named ${this.model}!`);
        }
        // Assert schema must exist.
        if (!this.schema) {
            throw new Error(`Must supply valid model name or schema to Keps form!`);
        }
        // Set schema fields.
        if (this.displayedFields.length > 0) {
            this.schemaFields = this.displayedFields;
        }
        else {
            this.schemaFields = Object.keys(this.schema)
                .filter((/**
             * @param {?} field
             * @return {?}
             */
            field => field[0] !== '_' && !this.hiddenFields.includes(field)));
        }
        this.form.setSchema(this.schema);
        for (const fieldName of this.schemaFields) {
            /** @type {?} */
            const field = this.schema[fieldName];
            // Create validators if needed.
            /** @type {?} */
            const validators = [];
            if (!field.optional) {
                validators.push(Validators.required);
            }
            if (field.type === 'number') {
            }
            if (field.type === 'email') {
                validators.push(Validators.email);
            }
            // Create the appropriate form control for each field.
            if (field.type === 'address') {
                this.form.addControl(fieldName, new FormGroup({}, validators));
            }
            else if (field.type === 'array') {
                this.form.addControl(fieldName, new KepsFormArray([], validators));
            }
            else if (field.type === 'datetime') {
                this.form.addControl(fieldName, new FormControl(null, validators));
            }
            else {
                this.form.addControl(fieldName, new FormControl(field.default || null, validators));
            }
        }
        this.form.initialize();
        this.initialized = true;
    }
    /**
     * Checks the type of a field.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @param {?} type Type to be checked.
     * @return {?}
     */
    isType(field, type) {
        return isType(this.schema, field, type);
    }
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @param {?} field Field to get from.
     * @return {?}
     */
    getReferenceDisplay(field) {
        return getReferenceDisplay(this.models, this.schema, field);
    }
    /**
     * Check if MatFormField should hide the underline or not.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @return {?}
     */
    isNotUnderlined(field) {
        return this.schema[field].type === 'boolean' ||
            this.schema[field].type === 'address' ||
            this.schema[field].type === 'array';
    }
    /**
     * Check if a field has a reference in refObjs.
     * if has reference in refobjs, use mat-select instead of input.
     * supports only type string at the moment.
     * \@internal Do not use!
     * @param {?} field Field to be checked.
     * @return {?}
     */
    hasReference(field) {
        return this.schema[field].type === 'string' &&
            this.refObjs &&
            this.refObjs[field];
    }
}
FormComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-form',
                template: "<form\r\n  *ngIf=\"initialized\"\r\n  [formGroup]=\"form\" \r\n  class=\"keps-form mat-typography\"\r\n>\r\n  <ng-container *ngFor=\"let fieldName of schemaFields\">\r\n    <ng-container *ngIf=\"schema[fieldName]; let field\">\r\n      <mat-form-field\r\n        floatLabel=\"always\"\r\n        [appearance]=\"appearance\"\r\n        [color]=\"color\"\r\n        [class.hide-underline]=\"isNotUnderlined(fieldName)\"\r\n      >\r\n        <mat-label>\r\n          {{ field.label || fieldName | splitCamel | titlecase }}\r\n        </mat-label>\r\n  \r\n        <!-- Type: String -->\r\n        <!-- reference is not provided, use input-->\r\n        <input\r\n          *ngIf=\"isType(fieldName, 'string') && !hasReference(fieldName)\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          type=\"text\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n          matInput\r\n        >\r\n        <!-- Type: String -->\r\n        <!-- If reference is provided, use select -->\r\n        <mat-select \r\n          *ngIf=\"isType(fieldName, 'string') && hasReference(fieldName)\" \r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\" >\r\n          <mat-option *ngIf=\"field.optional\">None</mat-option>\r\n          <mat-option *ngFor=\"let option of refObjs[fieldName]\" [value]=\"option\">{{option}}</mat-option>\r\n        </mat-select>\r\n  \r\n        <!-- Type: Number -->\r\n        <input\r\n          *ngIf=\"isType(fieldName, 'number')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          type=\"number\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n          [min]=\"field.min\"\r\n          [max]=\"field.max\"\r\n          [step]=\"field.step\"\r\n          matInput\r\n        >\r\n\r\n        <!-- Type: Email -->\r\n        <input\r\n          *ngIf=\"isType(fieldName, 'email')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          type=\"email\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n          matInput\r\n        >\r\n\r\n        <!-- Type: Boolean -->\r\n        <keps-field-boolean\r\n          *ngIf=\"isType(fieldName, 'boolean')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n        >\r\n        </keps-field-boolean>\r\n\r\n        <!-- Type: Enum & Multi -->\r\n        <mat-select\r\n          *ngIf=\"isType(fieldName, 'enum') || isType(fieldName, 'multi')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          [multiple]=\"isType(fieldName, 'multi')\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n        >\r\n          <mat-option\r\n            *ngFor=\"let option of field.options\"\r\n            [value]=\"option\"\r\n          >\r\n            {{ field.labels && field.labels[option] ? field.labels[option] : option }}\r\n          </mat-option>\r\n        </mat-select>\r\n\r\n        <!-- Type: Address -->\r\n        <keps-field-address\r\n          *ngIf=\"isType(fieldName, 'address')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          [appearance]=\"appearance\"\r\n          [color]=\"color\"\r\n        >\r\n        </keps-field-address>\r\n\r\n        <!-- Type: Array -->\r\n        <keps-field-array\r\n          *ngIf=\"isType(fieldName, 'array')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [subSchema]=\"field.subSchema\"\r\n          [refObjs]=\"refObjs[field]\"\r\n          [required]=\"!field.optional\"\r\n          [appearance]=\"appearance\"\r\n          [color]=\"color\"\r\n        >\r\n        </keps-field-array>\r\n\r\n        <!-- Type: Reference -->\r\n        <mat-select\r\n          *ngIf=\"isType(fieldName, 'reference')\"\r\n          [formControl]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n          [placeholder]=\"placeholders[fieldName]\"\r\n        >\r\n          <mat-option\r\n            *ngFor=\"let option of refObjs[fieldName]\"\r\n            [value]=\"option._id\"\r\n          >\r\n            {{ option | eval:getReferenceDisplay(fieldName) }}\r\n          </mat-option>\r\n        </mat-select>\r\n\r\n        <!-- Type: Datetime -->\r\n        <keps-field-datetime\r\n          *ngIf=\"isType(fieldName, 'datetime')\"\r\n          [form]=\"form.get(fieldName)\"\r\n          [required]=\"!field.optional\"\r\n        >\r\n        </keps-field-datetime>\r\n\r\n        <!-- Hints -->\r\n        <mat-hint *ngIf=\"hints\">{{hints[fieldName]}}</mat-hint>\r\n\r\n      </mat-form-field>\r\n    </ng-container>\r\n  </ng-container>\r\n</form>\r\n",
                styles: [".keps-form{display:flex;flex-direction:column}.keps-form>*{margin-bottom:1em}.keps-form-radio-group{display:inline-flex;flex-direction:column}.keps-form-radio-group>*{margin-bottom:.5em}.hide-underline::ng-deep .mat-form-field-underline{display:none!important}"]
            }] }
];
/** @nocollapse */
FormComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] }
];
FormComponent.propDecorators = {
    form: [{ type: Input }],
    model: [{ type: Input }],
    schema: [{ type: Input }],
    refObjs: [{ type: Input }],
    appearance: [{ type: Input }],
    color: [{ type: Input }],
    displayedFields: [{ type: Input }],
    hiddenFields: [{ type: Input }],
    placeholders: [{ type: Input }],
    hints: [{ type: Input }]
};
if (false) {
    /**
     * An instance of Keps form.
     * @type {?}
     */
    FormComponent.prototype.form;
    /**
     * Name of the model for the form.
     * @type {?}
     */
    FormComponent.prototype.model;
    /**
     * Schema for the form.
     *
     * If the model name is supplied,
     * then the form will get the schema from that model.
     * @type {?}
     */
    FormComponent.prototype.schema;
    /**
     * A map of reference to Keps objects to be shown for reference fields.
     *
     * Notes:-The key of the map is based on the field name of the Keps form model.
     *        Not the name of the model it is referenced to.
     *       -If field type is a string and a refObjs is provided,
     *        instead of a normal text input, it will be a select. see html for more info.
     *
     * **sample usage**
     * given the following models:
     *  PERSON {
     *    _id
     *    name: 'string',
     *    pet: ':ANIMAL'
     *  }
     *
     *  ANIMAL {
     *    _id
     *    type: 'string'
     *  }
     *
     *  if one wish to create a form for
     *  object PERSON where the options for ANIMAL
     *  comes from DB, that dev can create a refObjs:KepsReference
     *  which  will look like:
     *
     *  refObjs = {
     *              'pet': [
     *                <ANIMAL>{
     *                    _id
     *                    type
     *                  },
     *                <ANIMAL>{
     *                    _id
     *                    type
     *                },
     *                ...
     *                ...
     *                ..
     *              ],
     *            }
     *
     *  SIDENOTE: The annotation <TYPE> is used to make this exmaple more explicit.
     * @type {?}
     */
    FormComponent.prototype.refObjs;
    /**
     * Appearance style of the form.
     * @type {?}
     */
    FormComponent.prototype.appearance;
    /**
     * Angular Material color of the form field underline and floating label.
     * @type {?}
     */
    FormComponent.prototype.color;
    /**
     * Fields to be displayed on the form.
     * @type {?}
     */
    FormComponent.prototype.displayedFields;
    /**
     * Fields to be hidden on the form.
     * @type {?}
     */
    FormComponent.prototype.hiddenFields;
    /**
     * Placeholders to be displayed on fields.
     * @type {?}
     */
    FormComponent.prototype.placeholders;
    /**
     * Hints for fields input.
     * @type {?}
     */
    FormComponent.prototype.hints;
    /**
     * Fields to be displayed on the form based on displayedFields and hiddenFields.
     * \@internal Do not use!
     * @type {?}
     */
    FormComponent.prototype.schemaFields;
    /**
     * Whether the form has done initializing or not.
     * @type {?}
     */
    FormComponent.prototype.initialized;
    /**
     * @type {?}
     * @private
     */
    FormComponent.prototype.models;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudC9mb3JtL2Zvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakUsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFcEUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUN2QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFFbEQsT0FBTyxFQUFFLE1BQU0sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGFBQWEsQ0FBQzs7OztBQVUxRCxNQUFNLE9BQU8sYUFBYTs7Ozs7SUE2R3hCLFlBQzRCLE1BQWtCO1FBQWxCLFdBQU0sR0FBTixNQUFNLENBQVk7Ozs7UUExR3JDLFNBQUksR0FBRyxJQUFJLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBMkR4QixZQUFPLEdBQWtCLEVBQUUsQ0FBQzs7OztRQUs1QixlQUFVLEdBQTJCLFVBQVUsQ0FBQzs7OztRQUtoRCxVQUFLLEdBQWlCLFNBQVMsQ0FBQzs7OztRQUtoQyxvQkFBZSxHQUFhLEVBQUUsQ0FBQzs7OztRQUsvQixpQkFBWSxHQUFhLEVBQUUsQ0FBQzs7OztRQUs1QixpQkFBWSxHQUF3QixFQUFFLENBQUM7Ozs7UUFLdkMsVUFBSyxHQUEyQixFQUFFLENBQUM7Ozs7UUFXNUMsZ0JBQVcsR0FBRyxLQUFLLENBQUM7SUFPaEIsQ0FBQzs7Ozs7O0lBTUwsUUFBUTtRQUNOLDBDQUEwQztRQUMxQyxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDekMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUM7U0FDOUM7YUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqRCxNQUFNLElBQUksS0FBSyxDQUFDLDRCQUE0QixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztTQUM1RDtRQUVELDRCQUE0QjtRQUM1QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLHNEQUFzRCxDQUFDLENBQUM7U0FDekU7UUFFRCxxQkFBcUI7UUFDckIsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDbkMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1NBQzFDO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztpQkFDM0MsTUFBTTs7OztZQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUM7U0FDMUU7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFakMsS0FBSyxNQUFNLFNBQVMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFOztrQkFDbkMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDOzs7a0JBRzlCLFVBQVUsR0FBRyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO2dCQUNuQixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN0QztZQUNELElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxRQUFRLEVBQUU7YUFDNUI7WUFDRCxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssT0FBTyxFQUFFO2dCQUMxQixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNuQztZQUVELHNEQUFzRDtZQUN0RCxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUM1QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxTQUFTLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUM7YUFDaEU7aUJBQU0sSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLE9BQU8sRUFBRTtnQkFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLElBQUksYUFBYSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO2FBQ3BFO2lCQUFNLElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxVQUFVLEVBQUU7Z0JBQ3BDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQzthQUNwRTtpQkFBTTtnQkFDTCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FDbEIsU0FBUyxFQUNULElBQUksV0FBVyxDQUNiLEtBQUssQ0FBQyxPQUFPLElBQUksSUFBSSxFQUNyQixVQUFVLENBQ1gsQ0FDRixDQUFDO2FBQ0g7U0FDRjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7SUFDMUIsQ0FBQzs7Ozs7Ozs7SUFRRCxNQUFNLENBQUMsS0FBYSxFQUFFLElBQVk7UUFDaEMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7OztJQU9ELG1CQUFtQixDQUFDLEtBQWE7UUFDL0IsT0FBTyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7OztJQU9ELGVBQWUsQ0FBQyxLQUFhO1FBQzNCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLEtBQUssU0FBUztZQUM1QyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTO1lBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQztJQUN0QyxDQUFDOzs7Ozs7Ozs7SUFTRCxZQUFZLENBQUMsS0FBYTtRQUN4QixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxLQUFLLFFBQVE7WUFDM0MsSUFBSSxDQUFDLE9BQU87WUFDWixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RCLENBQUM7OztZQTVORixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLCtySkFBb0M7O2FBRXJDOzs7OzRDQStHSSxNQUFNLFNBQUMsUUFBUTs7O21CQTFHakIsS0FBSztvQkFLTCxLQUFLO3FCQVFMLEtBQUs7c0JBOENMLEtBQUs7eUJBS0wsS0FBSztvQkFLTCxLQUFLOzhCQUtMLEtBQUs7MkJBS0wsS0FBSzsyQkFLTCxLQUFLO29CQUtMLEtBQUs7Ozs7Ozs7SUF6Rk4sNkJBQWlDOzs7OztJQUtqQyw4QkFBdUI7Ozs7Ozs7O0lBUXZCLCtCQUE0Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQThDNUIsZ0NBQXFDOzs7OztJQUtyQyxtQ0FBeUQ7Ozs7O0lBS3pELDhCQUF5Qzs7Ozs7SUFLekMsd0NBQXdDOzs7OztJQUt4QyxxQ0FBcUM7Ozs7O0lBS3JDLHFDQUFnRDs7Ozs7SUFLaEQsOEJBQTRDOzs7Ozs7SUFNNUMscUNBQXVCOzs7OztJQUt2QixvQ0FBb0I7Ozs7O0lBTWxCLCtCQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbmplY3QsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFRoZW1lUGFsZXR0ZSwgTWF0Rm9ybUZpZWxkQXBwZWFyYW5jZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgS2Vwc0Zvcm0gfSBmcm9tICcuL2tlcHMtZm9ybSc7XHJcbmltcG9ydCB7IEtlcHNGb3JtQXJyYXkgfSBmcm9tICcuL2tlcHMtZm9ybS1hcnJheSc7XHJcbmltcG9ydCB7IEtlcHNTY2hlbWEsIEtlcHNNb2RlbHMsIEtlcHNSZWZlcmVuY2UgfSBmcm9tICcuLi8uLi9pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBpc1R5cGUsIGdldFJlZmVyZW5jZURpc3BsYXkgfSBmcm9tICcuLi8uLi91dGlscyc7XHJcblxyXG4vKipcclxuICogQSBjb21wb25lbnQgdGhhdCByZW5kZXJzIGZvcm0gYmFzZWQgb24gS2VwcyBzY2hlbWEuXHJcbiAqL1xyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2tlcHMtZm9ybScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2Zvcm0uY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2Zvcm0uY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgLyoqXHJcbiAgICogQW4gaW5zdGFuY2Ugb2YgS2VwcyBmb3JtLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGZvcm0gPSBuZXcgS2Vwc0Zvcm0oe30pO1xyXG5cclxuICAvKipcclxuICAgKiBOYW1lIG9mIHRoZSBtb2RlbCBmb3IgdGhlIGZvcm0uXHJcbiAgICovXHJcbiAgQElucHV0KCkgbW9kZWw6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogU2NoZW1hIGZvciB0aGUgZm9ybS5cclxuICAgKlxyXG4gICAqIElmIHRoZSBtb2RlbCBuYW1lIGlzIHN1cHBsaWVkLFxyXG4gICAqIHRoZW4gdGhlIGZvcm0gd2lsbCBnZXQgdGhlIHNjaGVtYSBmcm9tIHRoYXQgbW9kZWwuXHJcbiAgICovXHJcbiAgQElucHV0KCkgc2NoZW1hOiBLZXBzU2NoZW1hO1xyXG5cclxuICAvKipcclxuICAgKiBBIG1hcCBvZiByZWZlcmVuY2UgdG8gS2VwcyBvYmplY3RzIHRvIGJlIHNob3duIGZvciByZWZlcmVuY2UgZmllbGRzLlxyXG4gICAqXHJcbiAgICogTm90ZXM6LVRoZSBrZXkgb2YgdGhlIG1hcCBpcyBiYXNlZCBvbiB0aGUgZmllbGQgbmFtZSBvZiB0aGUgS2VwcyBmb3JtIG1vZGVsLlxyXG4gICAqICAgICAgICBOb3QgdGhlIG5hbWUgb2YgdGhlIG1vZGVsIGl0IGlzIHJlZmVyZW5jZWQgdG8uXHJcbiAgICogICAgICAgLUlmIGZpZWxkIHR5cGUgaXMgYSBzdHJpbmcgYW5kIGEgcmVmT2JqcyBpcyBwcm92aWRlZCxcclxuICAgKiAgICAgICAgaW5zdGVhZCBvZiBhIG5vcm1hbCB0ZXh0IGlucHV0LCBpdCB3aWxsIGJlIGEgc2VsZWN0LiBzZWUgaHRtbCBmb3IgbW9yZSBpbmZvLlxyXG4gICAqIFxyXG4gICAqICoqc2FtcGxlIHVzYWdlKipcclxuICAgKiBnaXZlbiB0aGUgZm9sbG93aW5nIG1vZGVsczogXHJcbiAgICogIFBFUlNPTiB7XHJcbiAgICogICAgX2lkXHJcbiAgICogICAgbmFtZTogJ3N0cmluZycsXHJcbiAgICogICAgcGV0OiAnOkFOSU1BTCdcclxuICAgKiAgfVxyXG4gICAqIFxyXG4gICAqICBBTklNQUwge1xyXG4gICAqICAgIF9pZFxyXG4gICAqICAgIHR5cGU6ICdzdHJpbmcnXHJcbiAgICogIH1cclxuICAgKiBcclxuICAgKiAgaWYgb25lIHdpc2ggdG8gY3JlYXRlIGEgZm9ybSBmb3IgXHJcbiAgICogIG9iamVjdCBQRVJTT04gd2hlcmUgdGhlIG9wdGlvbnMgZm9yIEFOSU1BTCBcclxuICAgKiAgY29tZXMgZnJvbSBEQiwgdGhhdCBkZXYgY2FuIGNyZWF0ZSBhIHJlZk9ianM6S2Vwc1JlZmVyZW5jZVxyXG4gICAqICB3aGljaCAgd2lsbCBsb29rIGxpa2U6XHJcbiAgICogIFxyXG4gICAqICByZWZPYmpzID0ge1xyXG4gICAqICAgICAgICAgICAgICAncGV0JzogW1xyXG4gICAqICAgICAgICAgICAgICAgIDxBTklNQUw+e1xyXG4gICAqICAgICAgICAgICAgICAgICAgICBfaWRcclxuICAgKiAgICAgICAgICAgICAgICAgICAgdHlwZVxyXG4gICAqICAgICAgICAgICAgICAgICAgfSxcclxuICAgKiAgICAgICAgICAgICAgICA8QU5JTUFMPntcclxuICAgKiAgICAgICAgICAgICAgICAgICAgX2lkXHJcbiAgICogICAgICAgICAgICAgICAgICAgIHR5cGVcclxuICAgKiAgICAgICAgICAgICAgICB9LFxyXG4gICAqICAgICAgICAgICAgICAgIC4uLlxyXG4gICAqICAgICAgICAgICAgICAgIC4uLlxyXG4gICAqICAgICAgICAgICAgICAgIC4uXHJcbiAgICogICAgICAgICAgICAgIF0sXHJcbiAgICogICAgICAgICAgICB9XHJcbiAgICogIFxyXG4gICAqICBTSURFTk9URTogVGhlIGFubm90YXRpb24gPFRZUEU+IGlzIHVzZWQgdG8gbWFrZSB0aGlzIGV4bWFwbGUgbW9yZSBleHBsaWNpdC5cclxuICAgKi9cclxuICBASW5wdXQoKSByZWZPYmpzOiBLZXBzUmVmZXJlbmNlID0ge307XHJcblxyXG4gIC8qKlxyXG4gICAqIEFwcGVhcmFuY2Ugc3R5bGUgb2YgdGhlIGZvcm0uXHJcbiAgICovXHJcbiAgQElucHV0KCkgYXBwZWFyYW5jZTogTWF0Rm9ybUZpZWxkQXBwZWFyYW5jZSA9ICdzdGFuZGFyZCc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFuZ3VsYXIgTWF0ZXJpYWwgY29sb3Igb2YgdGhlIGZvcm0gZmllbGQgdW5kZXJsaW5lIGFuZCBmbG9hdGluZyBsYWJlbC5cclxuICAgKi9cclxuICBASW5wdXQoKSBjb2xvcjogVGhlbWVQYWxldHRlID0gJ3ByaW1hcnknO1xyXG5cclxuICAvKipcclxuICAgKiBGaWVsZHMgdG8gYmUgZGlzcGxheWVkIG9uIHRoZSBmb3JtLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGRpc3BsYXllZEZpZWxkczogc3RyaW5nW10gPSBbXTtcclxuXHJcbiAgLyoqXHJcbiAgICogRmllbGRzIHRvIGJlIGhpZGRlbiBvbiB0aGUgZm9ybS5cclxuICAgKi9cclxuICBASW5wdXQoKSBoaWRkZW5GaWVsZHM6IHN0cmluZ1tdID0gW107XHJcblxyXG4gIC8qKlxyXG4gICAqIFBsYWNlaG9sZGVycyB0byBiZSBkaXNwbGF5ZWQgb24gZmllbGRzLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyczogUmVjb3JkPHN0cmluZywgYW55PiA9IHt9O1xyXG5cclxuICAvKipcclxuICAgKiBIaW50cyBmb3IgZmllbGRzIGlucHV0LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGhpbnRzOiBSZWNvcmQ8c3RyaW5nLCBzdHJpbmc+ID0ge307XHJcblxyXG4gIC8qKlxyXG4gICAqIEZpZWxkcyB0byBiZSBkaXNwbGF5ZWQgb24gdGhlIGZvcm0gYmFzZWQgb24gZGlzcGxheWVkRmllbGRzIGFuZCBoaWRkZW5GaWVsZHMuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2NoZW1hRmllbGRzOiBzdHJpbmdbXTtcclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgZm9ybSBoYXMgZG9uZSBpbml0aWFsaXppbmcgb3Igbm90LlxyXG4gICAqL1xyXG4gIGluaXRpYWxpemVkID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgQEluamVjdCgnTU9ERUxTJykgcHJpdmF0ZSBtb2RlbHM6IEtlcHNNb2RlbHNcclxuICApIHsgfVxyXG5cclxuICAvKipcclxuICAgKiBJbml0aWFsaXplcyB0aGUgZm9ybSB3aXRoIGZpZWxkcyBiYXNlZCBvbiB0aGUgc2NoZW1hLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgLy8gR2V0IHNjaGVtYSBmcm9tIG1vZGVsIG5hbWUgaWYgc3VwcGxpZWQuXHJcbiAgICBpZiAodGhpcy5tb2RlbCAmJiB0aGlzLm1vZGVsc1t0aGlzLm1vZGVsXSkge1xyXG4gICAgICB0aGlzLnNjaGVtYSA9IHRoaXMubW9kZWxzW3RoaXMubW9kZWxdLnNjaGVtYTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5tb2RlbCAmJiAhdGhpcy5tb2RlbHNbdGhpcy5tb2RlbF0pIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKGBDYW4ndCBmaW5kIGEgbW9kZWwgbmFtZWQgJHt0aGlzLm1vZGVsfSFgKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBBc3NlcnQgc2NoZW1hIG11c3QgZXhpc3QuXHJcbiAgICBpZiAoIXRoaXMuc2NoZW1hKSB7XHJcbiAgICAgIHRocm93IG5ldyBFcnJvcihgTXVzdCBzdXBwbHkgdmFsaWQgbW9kZWwgbmFtZSBvciBzY2hlbWEgdG8gS2VwcyBmb3JtIWApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFNldCBzY2hlbWEgZmllbGRzLlxyXG4gICAgaWYgKHRoaXMuZGlzcGxheWVkRmllbGRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5zY2hlbWFGaWVsZHMgPSB0aGlzLmRpc3BsYXllZEZpZWxkcztcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2NoZW1hRmllbGRzID0gT2JqZWN0LmtleXModGhpcy5zY2hlbWEpXHJcbiAgICAgIC5maWx0ZXIoZmllbGQgPT4gZmllbGRbMF0gIT09ICdfJyAmJiAhdGhpcy5oaWRkZW5GaWVsZHMuaW5jbHVkZXMoZmllbGQpKTtcclxuICAgIH1cclxuICAgIHRoaXMuZm9ybS5zZXRTY2hlbWEodGhpcy5zY2hlbWEpO1xyXG5cclxuICAgIGZvciAoY29uc3QgZmllbGROYW1lIG9mIHRoaXMuc2NoZW1hRmllbGRzKSB7XHJcbiAgICAgIGNvbnN0IGZpZWxkID0gdGhpcy5zY2hlbWFbZmllbGROYW1lXTtcclxuXHJcbiAgICAgIC8vIENyZWF0ZSB2YWxpZGF0b3JzIGlmIG5lZWRlZC5cclxuICAgICAgY29uc3QgdmFsaWRhdG9ycyA9IFtdO1xyXG4gICAgICBpZiAoIWZpZWxkLm9wdGlvbmFsKSB7XHJcbiAgICAgICAgdmFsaWRhdG9ycy5wdXNoKFZhbGlkYXRvcnMucmVxdWlyZWQpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChmaWVsZC50eXBlID09PSAnbnVtYmVyJykge1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChmaWVsZC50eXBlID09PSAnZW1haWwnKSB7XHJcbiAgICAgICAgdmFsaWRhdG9ycy5wdXNoKFZhbGlkYXRvcnMuZW1haWwpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBDcmVhdGUgdGhlIGFwcHJvcHJpYXRlIGZvcm0gY29udHJvbCBmb3IgZWFjaCBmaWVsZC5cclxuICAgICAgaWYgKGZpZWxkLnR5cGUgPT09ICdhZGRyZXNzJykge1xyXG4gICAgICAgIHRoaXMuZm9ybS5hZGRDb250cm9sKGZpZWxkTmFtZSwgbmV3IEZvcm1Hcm91cCh7fSwgdmFsaWRhdG9ycykpO1xyXG4gICAgICB9IGVsc2UgaWYgKGZpZWxkLnR5cGUgPT09ICdhcnJheScpIHtcclxuICAgICAgICB0aGlzLmZvcm0uYWRkQ29udHJvbChmaWVsZE5hbWUsIG5ldyBLZXBzRm9ybUFycmF5KFtdLCB2YWxpZGF0b3JzKSk7XHJcbiAgICAgIH0gZWxzZSBpZiAoZmllbGQudHlwZSA9PT0gJ2RhdGV0aW1lJykge1xyXG4gICAgICAgIHRoaXMuZm9ybS5hZGRDb250cm9sKGZpZWxkTmFtZSwgbmV3IEZvcm1Db250cm9sKG51bGwsIHZhbGlkYXRvcnMpKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLmZvcm0uYWRkQ29udHJvbChcclxuICAgICAgICAgIGZpZWxkTmFtZSxcclxuICAgICAgICAgIG5ldyBGb3JtQ29udHJvbChcclxuICAgICAgICAgICAgZmllbGQuZGVmYXVsdCB8fCBudWxsLFxyXG4gICAgICAgICAgICB2YWxpZGF0b3JzXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgdGhpcy5mb3JtLmluaXRpYWxpemUoKTtcclxuICAgIHRoaXMuaW5pdGlhbGl6ZWQgPSB0cnVlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ2hlY2tzIHRoZSB0eXBlIG9mIGEgZmllbGQuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICogQHBhcmFtIGZpZWxkIEZpZWxkIHRvIGJlIGNoZWNrZWQuXHJcbiAgICogQHBhcmFtIHR5cGUgVHlwZSB0byBiZSBjaGVja2VkLlxyXG4gICAqL1xyXG4gIGlzVHlwZShmaWVsZDogc3RyaW5nLCB0eXBlOiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBpc1R5cGUodGhpcy5zY2hlbWEsIGZpZWxkLCB0eXBlKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJldHVybnMgdGhlIGRpc3BsYXkgZXhwcmVzc2lvbiBvZiBhIHJlZmVyZW5jZSBmaWVsZC5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKiBAcGFyYW0gZmllbGQgRmllbGQgdG8gZ2V0IGZyb20uXHJcbiAgICovXHJcbiAgZ2V0UmVmZXJlbmNlRGlzcGxheShmaWVsZDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gZ2V0UmVmZXJlbmNlRGlzcGxheSh0aGlzLm1vZGVscywgdGhpcy5zY2hlbWEsIGZpZWxkKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENoZWNrIGlmIE1hdEZvcm1GaWVsZCBzaG91bGQgaGlkZSB0aGUgdW5kZXJsaW5lIG9yIG5vdC5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKiBAcGFyYW0gZmllbGQgRmllbGQgdG8gYmUgY2hlY2tlZC5cclxuICAgKi9cclxuICBpc05vdFVuZGVybGluZWQoZmllbGQ6IHN0cmluZykge1xyXG4gICAgcmV0dXJuIHRoaXMuc2NoZW1hW2ZpZWxkXS50eXBlID09PSAnYm9vbGVhbicgfHxcclxuICAgIHRoaXMuc2NoZW1hW2ZpZWxkXS50eXBlID09PSAnYWRkcmVzcycgfHxcclxuICAgIHRoaXMuc2NoZW1hW2ZpZWxkXS50eXBlID09PSAnYXJyYXknO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgKiBDaGVjayBpZiBhIGZpZWxkIGhhcyBhIHJlZmVyZW5jZSBpbiByZWZPYmpzLlxyXG4gICogaWYgaGFzIHJlZmVyZW5jZSBpbiByZWZvYmpzLCB1c2UgbWF0LXNlbGVjdCBpbnN0ZWFkIG9mIGlucHV0LlxyXG4gICogc3VwcG9ydHMgb25seSB0eXBlIHN0cmluZyBhdCB0aGUgbW9tZW50LlxyXG4gICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgKiBAcGFyYW0gZmllbGQgRmllbGQgdG8gYmUgY2hlY2tlZC5cclxuICAqL1xyXG4gIGhhc1JlZmVyZW5jZShmaWVsZDogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gdGhpcy5zY2hlbWFbZmllbGRdLnR5cGUgPT09ICdzdHJpbmcnICYmIFxyXG4gICAgdGhpcy5yZWZPYmpzICYmIFxyXG4gICAgdGhpcy5yZWZPYmpzW2ZpZWxkXTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==