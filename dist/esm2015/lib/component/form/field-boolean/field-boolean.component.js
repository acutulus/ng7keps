/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, Input, Optional, Self } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';
import { Subject } from 'rxjs';
/**
 * A custom field that displays two radio buttons: True, False.
 */
export class FieldBooleanComponent {
    /**
     * \@internal Do not use!
     * @param {?} fm
     * @param {?} elRef
     * @param {?} ngControl
     */
    constructor(fm, elRef, ngControl) {
        this.fm = fm;
        this.elRef = elRef;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-boolean';
        /**
         * \@internal Do not use!
         */
        this.id = `keps-field-boolean-${FieldBooleanComponent.nextId++}`;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new FormControl(null);
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        }));
    }
    /**
     * \@internal Do not use!
     * @param {?} change
     * @return {?}
     */
    onRadioChange(change) {
        this.value = change.value;
    }
    /**
     * The value of the control.
     * @return {?}
     */
    get value() {
        return this.form.value;
    }
    /**
     * \@internal Do not use!
     * @param {?} b
     * @return {?}
     */
    set value(b) {
        this.form.patchValue(b);
        this.stateChanges.next();
    }
    /**
     * Whether the control is required.
     * @return {?}
     */
    get required() {
        return this._required;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set required(value) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * Whether the control is disabled.
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * The placeholder for this control.
     * @return {?}
     */
    get placeholder() {
        return this._placeholder;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }
    /**
     * Whether the control is empty.
     * @return {?}
     */
    get empty() {
        return this.form.value == null;
    }
    /**
     * Whether the MatFormField label should try to float.
     * @return {?}
     */
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    setDescribedByIds(ids) {
        this.describedBy = ids.join(' ');
    }
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    onContainerClick(event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    }
}
/**
 * \@internal Do not use!
 */
FieldBooleanComponent.nextId = 0;
FieldBooleanComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-field-boolean',
                template: "<mat-radio-button\r\n  [checked]=\"form.value === true\"\r\n  [disabled]=\"disabled\"\r\n  (change)=\"onRadioChange($event)\"\r\n  [value]=\"true\"\r\n>\r\n  True\r\n</mat-radio-button>\r\n<mat-radio-button\r\n  [checked]=\"form.value === false\"\r\n  [disabled]=\"disabled\"\r\n  (change)=\"onRadioChange($event)\"\r\n  [value]=\"false\"\r\n>\r\n  False\r\n</mat-radio-button>\r\n",
                providers: [
                    {
                        provide: MatFormFieldControl,
                        useExisting: FieldBooleanComponent
                    }
                ],
                styles: [":host{display:flex;flex-direction:column}mat-radio-button{margin-bottom:8px}"]
            }] }
];
/** @nocollapse */
FieldBooleanComponent.ctorParameters = () => [
    { type: FocusMonitor },
    { type: ElementRef },
    { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
];
FieldBooleanComponent.propDecorators = {
    form: [{ type: Input }],
    value: [{ type: Input }],
    required: [{ type: Input }],
    disabled: [{ type: Input }],
    placeholder: [{ type: Input }]
};
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.nextId;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype._required;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype._disabled;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype._placeholder;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.stateChanges;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.focused;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.errorState;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.controlType;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.id;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldBooleanComponent.prototype.describedBy;
    /**
     * Form control of the field.
     * @type {?}
     */
    FieldBooleanComponent.prototype.form;
    /**
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype.fm;
    /**
     * @type {?}
     * @private
     */
    FieldBooleanComponent.prototype.elRef;
    /** @type {?} */
    FieldBooleanComponent.prototype.ngControl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtYm9vbGVhbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudC9mb3JtL2ZpZWxkLWJvb2xlYW4vZmllbGQtYm9vbGVhbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBRVQsVUFBVSxFQUNWLEtBQUssRUFDTCxRQUFRLEVBQ1IsSUFBSSxFQUNMLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxtQkFBbUIsRUFBa0IsTUFBTSxtQkFBbUIsQ0FBQztBQUN4RSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDOzs7O0FBZ0IvQixNQUFNLE9BQU8scUJBQXFCOzs7Ozs7O0lBMkRoQyxZQUNVLEVBQWdCLEVBQ2hCLEtBQThCLEVBQ1gsU0FBb0I7UUFGdkMsT0FBRSxHQUFGLEVBQUUsQ0FBYztRQUNoQixVQUFLLEdBQUwsS0FBSyxDQUF5QjtRQUNYLGNBQVMsR0FBVCxTQUFTLENBQVc7Ozs7UUFyRHpDLGNBQVMsR0FBRyxLQUFLLENBQUM7Ozs7UUFLbEIsY0FBUyxHQUFHLEtBQUssQ0FBQzs7OztRQVUxQixpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7Ozs7UUFLbkMsWUFBTyxHQUFHLEtBQUssQ0FBQzs7OztRQUtoQixlQUFVLEdBQUcsS0FBSyxDQUFDOzs7O1FBS25CLGdCQUFXLEdBQUcsb0JBQW9CLENBQUM7Ozs7UUFLbkMsT0FBRSxHQUFHLHNCQUFzQixxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDOzs7O1FBSzVELGdCQUFXLEdBQUcsRUFBRSxDQUFDOzs7O1FBS1IsU0FBSSxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBVXBDLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUN6QyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDeEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUtELGFBQWEsQ0FBQyxNQUFzQjtRQUNsQyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFLRCxJQUNJLEtBQUs7UUFDUCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUtELElBQUksS0FBSyxDQUFDLENBQWlCO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFLRCxJQUNJLFFBQVE7UUFDVixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQzs7Ozs7O0lBS0QsSUFBSSxRQUFRLENBQUMsS0FBYztRQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFLRCxJQUNJLFFBQVE7UUFDVixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQzs7Ozs7O0lBS0QsSUFBSSxRQUFRLENBQUMsS0FBYztRQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFLRCxJQUNJLFdBQVc7UUFDYixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDM0IsQ0FBQzs7Ozs7O0lBS0QsSUFBSSxXQUFXLENBQUMsS0FBYTtRQUMzQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBS0QsSUFBSSxLQUFLO1FBQ1AsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFLRCxJQUFJLGdCQUFnQjtRQUNsQixPQUFPLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3JDLENBQUM7Ozs7OztJQUtELGlCQUFpQixDQUFDLEdBQWE7UUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7OztJQUtELGdCQUFnQixDQUFDLEtBQWlCO1FBQ2hDLElBQUksQ0FBQyxtQkFBQSxLQUFLLENBQUMsTUFBTSxFQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEtBQUssT0FBTyxFQUFFO1lBQy9ELElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN6RDtJQUNILENBQUM7Ozs7O0lBS0QsV0FBVztRQUNULElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7Ozs7O0FBN0tNLDRCQUFNLEdBQUcsQ0FBQyxDQUFDOztZQWZuQixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIseVlBQTZDO2dCQUU3QyxTQUFTLEVBQUU7b0JBQ1Q7d0JBQ0UsT0FBTyxFQUFFLG1CQUFtQjt3QkFDNUIsV0FBVyxFQUFFLHFCQUFxQjtxQkFDbkM7aUJBQ0Y7O2FBQ0Y7Ozs7WUFuQlEsWUFBWTtZQUxuQixVQUFVO1lBT1UsU0FBUyx1QkFnRjFCLFFBQVEsWUFBSSxJQUFJOzs7bUJBUmxCLEtBQUs7b0JBMEJMLEtBQUs7dUJBZ0JMLEtBQUs7dUJBZ0JMLEtBQUs7MEJBZ0JMLEtBQUs7Ozs7Ozs7SUE1SE4sNkJBQWtCOzs7Ozs7SUFLbEIsMENBQTBCOzs7Ozs7SUFLMUIsMENBQTBCOzs7Ozs7SUFLMUIsNkNBQTZCOzs7OztJQUs3Qiw2Q0FBbUM7Ozs7O0lBS25DLHdDQUFnQjs7Ozs7SUFLaEIsMkNBQW1COzs7OztJQUtuQiw0Q0FBbUM7Ozs7O0lBS25DLG1DQUE0RDs7Ozs7SUFLNUQsNENBQWlCOzs7OztJQUtqQixxQ0FBc0M7Ozs7O0lBTXBDLG1DQUF3Qjs7Ozs7SUFDeEIsc0NBQXNDOztJQUN0QywwQ0FBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkRlc3Ryb3ksXHJcbiAgRWxlbWVudFJlZixcclxuICBJbnB1dCxcclxuICBPcHRpb25hbCxcclxuICBTZWxmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvY3VzTW9uaXRvciB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9hMTF5JztcclxuaW1wb3J0IHsgY29lcmNlQm9vbGVhblByb3BlcnR5IH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvZXJjaW9uJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIE5nQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTWF0Rm9ybUZpZWxkQ29udHJvbCwgTWF0UmFkaW9DaGFuZ2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuXHJcbi8qKlxyXG4gKiBBIGN1c3RvbSBmaWVsZCB0aGF0IGRpc3BsYXlzIHR3byByYWRpbyBidXR0b25zOiBUcnVlLCBGYWxzZS5cclxuICovXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy1maWVsZC1ib29sZWFuJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZmllbGQtYm9vbGVhbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZmllbGQtYm9vbGVhbi5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBNYXRGb3JtRmllbGRDb250cm9sLFxyXG4gICAgICB1c2VFeGlzdGluZzogRmllbGRCb29sZWFuQ29tcG9uZW50XHJcbiAgICB9XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmllbGRCb29sZWFuQ29tcG9uZW50IGltcGxlbWVudHMgTWF0Rm9ybUZpZWxkQ29udHJvbDxib29sZWFuPiwgT25EZXN0cm95IHtcclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzdGF0aWMgbmV4dElkID0gMDtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfcmVxdWlyZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfZGlzYWJsZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfcGxhY2Vob2xkZXI6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc3RhdGVDaGFuZ2VzID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZm9jdXNlZCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBlcnJvclN0YXRlID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnRyb2xUeXBlID0gJ2tlcHMtZmllbGQtYm9vbGVhbic7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGlkID0gYGtlcHMtZmllbGQtYm9vbGVhbi0ke0ZpZWxkQm9vbGVhbkNvbXBvbmVudC5uZXh0SWQrK31gO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBkZXNjcmliZWRCeSA9ICcnO1xyXG5cclxuICAvKipcclxuICAgKiBGb3JtIGNvbnRyb2wgb2YgdGhlIGZpZWxkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGZvcm0gPSBuZXcgRm9ybUNvbnRyb2wobnVsbCk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmbTogRm9jdXNNb25pdG9yLFxyXG4gICAgcHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4sXHJcbiAgICBAT3B0aW9uYWwoKSBAU2VsZigpIHB1YmxpYyBuZ0NvbnRyb2w6IE5nQ29udHJvbFxyXG4gICkge1xyXG4gICAgZm0ubW9uaXRvcihlbFJlZiwgdHJ1ZSkuc3Vic2NyaWJlKG9yaWdpbiA9PiB7XHJcbiAgICAgIHRoaXMuZm9jdXNlZCA9ICEhb3JpZ2luO1xyXG4gICAgICB0aGlzLnN0YXRlQ2hhbmdlcy5uZXh0KCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG9uUmFkaW9DaGFuZ2UoY2hhbmdlOiBNYXRSYWRpb0NoYW5nZSkge1xyXG4gICAgdGhpcy52YWx1ZSA9IGNoYW5nZS52YWx1ZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSB2YWx1ZSBvZiB0aGUgY29udHJvbC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCB2YWx1ZSgpOiBib29sZWFuIHwgbnVsbCB7XHJcbiAgICByZXR1cm4gdGhpcy5mb3JtLnZhbHVlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2V0IHZhbHVlKGI6IGJvb2xlYW4gfCBudWxsKSB7XHJcbiAgICB0aGlzLmZvcm0ucGF0Y2hWYWx1ZShiKTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbnRyb2wgaXMgcmVxdWlyZWQuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgcmVxdWlyZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5fcmVxdWlyZWQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgcmVxdWlyZWQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuX3JlcXVpcmVkID0gY29lcmNlQm9vbGVhblByb3BlcnR5KHZhbHVlKTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbnRyb2wgaXMgZGlzYWJsZWQuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgZGlzYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gdGhpcy5fZGlzYWJsZWQ7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgZGlzYWJsZWQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuX2Rpc2FibGVkID0gY29lcmNlQm9vbGVhblByb3BlcnR5KHZhbHVlKTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBwbGFjZWhvbGRlciBmb3IgdGhpcyBjb250cm9sLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IHBsYWNlaG9sZGVyKCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGhpcy5fcGxhY2Vob2xkZXI7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXQgcGxhY2Vob2xkZXIodmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5fcGxhY2Vob2xkZXIgPSB2YWx1ZTtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbnRyb2wgaXMgZW1wdHkuXHJcbiAgICovXHJcbiAgZ2V0IGVtcHR5KCkge1xyXG4gICAgcmV0dXJuIHRoaXMuZm9ybS52YWx1ZSA9PSBudWxsO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgTWF0Rm9ybUZpZWxkIGxhYmVsIHNob3VsZCB0cnkgdG8gZmxvYXQuXHJcbiAgICovXHJcbiAgZ2V0IHNob3VsZExhYmVsRmxvYXQoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5mb2N1c2VkIHx8ICF0aGlzLmVtcHR5O1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2V0RGVzY3JpYmVkQnlJZHMoaWRzOiBzdHJpbmdbXSkge1xyXG4gICAgdGhpcy5kZXNjcmliZWRCeSA9IGlkcy5qb2luKCcgJyk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBvbkNvbnRhaW5lckNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICBpZiAoKGV2ZW50LnRhcmdldCBhcyBFbGVtZW50KS50YWdOYW1lLnRvTG93ZXJDYXNlKCkgIT09ICdpbnB1dCcpIHtcclxuICAgICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2lucHV0JykuZm9jdXMoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMuY29tcGxldGUoKTtcclxuICAgIHRoaXMuZm0uc3RvcE1vbml0b3JpbmcodGhpcy5lbFJlZik7XHJcbiAgfVxyXG59XHJcbiJdfQ==