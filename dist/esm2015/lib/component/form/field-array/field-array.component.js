/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject, ElementRef, Input, Optional, Self } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';
import { KepsFormArray } from '../keps-form-array';
import { Subject, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
export class FieldArrayComponent {
    /**
     * \@internal Do not use!
     * @param {?} fm
     * @param {?} elRef
     * @param {?} models
     * @param {?} ngControl
     */
    constructor(fm, elRef, models, ngControl) {
        this.fm = fm;
        this.elRef = elRef;
        this.models = models;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-array';
        /**
         * \@internal Do not use!
         */
        this.id = `keps-field-array-${FieldArrayComponent.nextId++}`;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new KepsFormArray([]);
        /**
         * Subschema of the form array.
         */
        this.subSchema = 'string';
        /**
         * Objects to be shown for reference field.
         */
        this.refObjs = [];
        /**
         * Appearance style of the form.
         */
        this.appearance = 'standard';
        /**
         * Angular Material color of the form field underline and floating label.
         */
        this.color = 'primary';
        /**
         * A behavior subject that emits a number which is the length of the form array.
         *
         * It should emit whenever a form control is added/removed from the array.
         * \@internal Do not use!
         */
        this.counter$ = new BehaviorSubject(0);
        /**
         * An observable that emits an empty array with the same length as the form array.
         *
         * It is used for *ngFor in this component's HTML.
         * \@internal Do not use!
         */
        this.counterArr$ = this.counter$.pipe(map((/**
         * @param {?} value
         * @return {?}
         */
        value => {
            return new Array(value);
        })));
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        }));
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        this.form.setCounter$(this.counter$);
    }
    /**
     * Adds a form control to the form array.
     * @return {?}
     */
    add() {
        this.form.push(new FormControl(''));
        this.counter$.next(this.form.length);
    }
    /**
     * Removes a form control from the form array.
     * @param {?} index Index of the form control to be removed.
     * @return {?}
     */
    remove(index) {
        this.form.removeAt(index);
        this.focused = false;
        this.counter$.next(this.form.length);
    }
    /**
     * Rearranges form controls inside the form array.
     * \@internal Do not use!
     * @param {?} event The drag event containing the form control.
     * @return {?}
     */
    drop(event) {
        /** @type {?} */
        const prevIndex = event.previousIndex;
        /** @type {?} */
        const nextIndex = event.currentIndex;
        /** @type {?} */
        const movedForm = this.form.at(prevIndex);
        this.form.removeAt(prevIndex);
        this.form.insert(nextIndex, movedForm);
    }
    /**
     * Returns the display expression of a reference field.
     * \@internal Do not use!
     * @return {?}
     */
    getReferenceDisplay() {
        /** @type {?} */
        const referenceTo = this.subSchema.slice(1);
        if (this.models && this.models[referenceTo]) {
            return this.models[referenceTo].properties.displayExpression;
        }
        return null;
    }
    /**
     * The value of the control.
     * @return {?}
     */
    get value() {
        return this.form.value;
    }
    /**
     * \@internal Do not use!
     * @param {?} v
     * @return {?}
     */
    set value(v) {
        this.form.patchValue(v);
        this.stateChanges.next();
    }
    /**
     * Whether the control is required.
     * @return {?}
     */
    get required() {
        return this._required;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set required(value) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * Whether the control is disabled.
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * The placeholder for this control.
     * @return {?}
     */
    get placeholder() {
        return this._placeholder;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }
    /**
     * Whether the control is empty.
     * @return {?}
     */
    get empty() {
        return !this.form.value || this.form.value.length === 0;
    }
    /**
     * Whether the MatFormField label should try to float.
     * @return {?}
     */
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    setDescribedByIds(ids) {
        this.describedBy = ids.join(' ');
    }
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    onContainerClick(event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    }
}
/**
 * \@internal Do not use!
 */
FieldArrayComponent.nextId = 0;
FieldArrayComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-field-array',
                template: "<div cdkDropList (cdkDropListDropped)=\"drop($event)\">\n  <div *ngFor=\"let c of counterArr$ | async; let i = index\" class=\"field-array-container\" cdkDrag>\n    <button cdkDragHandle mat-icon-button color=\"accent\" class=\"field-array-reorder\">\n      <mat-icon>reorder</mat-icon>\n    </button>\n    <mat-form-field\n      [appearance]=\"appearance\"\n      [color]=\"color\"\n      floatLabel=\"always\"\n      class=\"field-array\"\n    >\n      <!-- Type: String -->\n      <input\n        *ngIf=\"subSchema === 'string'\"\n        [formControl]=\"form.at(i)\"\n        [required]=\"required\"\n        type=\"text\"\n        matInput\n      >\n      <!-- Type: number -->\n      <input\n        *ngIf=\"subSchema === 'number'\"\n        [formControl]=\"form.at(i)\"\n        [required]=\"required\"\n        type=\"number\"\n        matInput\n      >\n      <!-- Type: Reference -->\n      <mat-select\n        *ngIf=\"subSchema[0] === ':'\"\n        [formControl]=\"form.at(i)\"\n        [required]=\"required\"\n      >\n        <mat-option\n          *ngFor=\"let option of refObjs\"\n          [value]=\"option._id\"\n        >\n          {{ option | eval:getReferenceDisplay() }}\n        </mat-option>\n      </mat-select>\n\n      <!-- TODO: If subschema is another schema object, we should make something here. -->\n    </mat-form-field>\n    <button (click)=\"remove(i)\" mat-icon-button color=\"warn\" class=\"field-array-delete\">\n      <mat-icon>delete</mat-icon>\n    </button>\n  </div>\n</div>\n\n<button (click)=\"add()\" mat-icon-button color=\"accent\">\n  <mat-icon>add</mat-icon>\n</button>\n  ",
                providers: [
                    {
                        provide: MatFormFieldControl,
                        useExisting: FieldArrayComponent
                    }
                ],
                styles: [".field-array-container{width:100%;display:flex;flex-direction:row;justify-content:center}.field-array-reorder{max-width:30px}.field-array-delete{max-width:20px}.field-array-container::ng-deep mat-form-field{flex:1 1 auto;margin-top:5px}.field-array::ng-deep .mat-form-field-underline{display:block!important}.field-array::ng-deep .mat-form-field-flex{padding-top:0!important}.field-array::ng-deep .mat-form-field-infix{border-top:0!important;width:100%}"]
            }] }
];
/** @nocollapse */
FieldArrayComponent.ctorParameters = () => [
    { type: FocusMonitor },
    { type: ElementRef },
    { type: undefined, decorators: [{ type: Inject, args: ['MODELS',] }] },
    { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
];
FieldArrayComponent.propDecorators = {
    form: [{ type: Input }],
    subSchema: [{ type: Input }],
    refObjs: [{ type: Input }],
    appearance: [{ type: Input }],
    color: [{ type: Input }],
    value: [{ type: Input }],
    required: [{ type: Input }],
    disabled: [{ type: Input }],
    placeholder: [{ type: Input }]
};
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.nextId;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype._required;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype._disabled;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype._placeholder;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.stateChanges;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.focused;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.errorState;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.controlType;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.id;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.describedBy;
    /**
     * Form control of the field.
     * @type {?}
     */
    FieldArrayComponent.prototype.form;
    /**
     * Subschema of the form array.
     * @type {?}
     */
    FieldArrayComponent.prototype.subSchema;
    /**
     * Objects to be shown for reference field.
     * @type {?}
     */
    FieldArrayComponent.prototype.refObjs;
    /**
     * Appearance style of the form.
     * @type {?}
     */
    FieldArrayComponent.prototype.appearance;
    /**
     * Angular Material color of the form field underline and floating label.
     * @type {?}
     */
    FieldArrayComponent.prototype.color;
    /**
     * A behavior subject that emits a number which is the length of the form array.
     *
     * It should emit whenever a form control is added/removed from the array.
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.counter$;
    /**
     * An observable that emits an empty array with the same length as the form array.
     *
     * It is used for *ngFor in this component's HTML.
     * \@internal Do not use!
     * @type {?}
     */
    FieldArrayComponent.prototype.counterArr$;
    /**
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype.fm;
    /**
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype.elRef;
    /**
     * @type {?}
     * @private
     */
    FieldArrayComponent.prototype.models;
    /** @type {?} */
    FieldArrayComponent.prototype.ngControl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtYXJyYXkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvZm9ybS9maWVsZC1hcnJheS9maWVsZC1hcnJheS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBR1QsTUFBTSxFQUNOLFVBQVUsRUFDVixLQUFLLEVBQ0wsUUFBUSxFQUNSLElBQUksRUFDTCxNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDakQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsbUJBQW1CLEVBQXdDLE1BQU0sbUJBQW1CLENBQUM7QUFFOUYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRW5ELE9BQU8sRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2hELE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQWFyQyxNQUFNLE9BQU8sbUJBQW1COzs7Ozs7OztJQW1HOUIsWUFDVSxFQUFnQixFQUNoQixLQUE4QixFQUNaLE1BQWtCLEVBQ2pCLFNBQW9CO1FBSHZDLE9BQUUsR0FBRixFQUFFLENBQWM7UUFDaEIsVUFBSyxHQUFMLEtBQUssQ0FBeUI7UUFDWixXQUFNLEdBQU4sTUFBTSxDQUFZO1FBQ2pCLGNBQVMsR0FBVCxTQUFTLENBQVc7Ozs7UUE5RnpDLGNBQVMsR0FBRyxLQUFLLENBQUM7Ozs7UUFLbEIsY0FBUyxHQUFHLEtBQUssQ0FBQzs7OztRQVUxQixpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7Ozs7UUFLbkMsWUFBTyxHQUFHLEtBQUssQ0FBQzs7OztRQUtoQixlQUFVLEdBQUcsS0FBSyxDQUFDOzs7O1FBS25CLGdCQUFXLEdBQUcsa0JBQWtCLENBQUM7Ozs7UUFLakMsT0FBRSxHQUFHLG9CQUFvQixtQkFBbUIsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDOzs7O1FBS3hELGdCQUFXLEdBQUcsRUFBRSxDQUFDOzs7O1FBS1IsU0FBSSxHQUFHLElBQUksYUFBYSxDQUFDLEVBQUUsQ0FBQyxDQUFDOzs7O1FBSzdCLGNBQVMsR0FBRyxRQUFRLENBQUM7Ozs7UUFLckIsWUFBTyxHQUFVLEVBQUUsQ0FBQzs7OztRQUtwQixlQUFVLEdBQTJCLFVBQVUsQ0FBQzs7OztRQUtoRCxVQUFLLEdBQWlCLFNBQVMsQ0FBQzs7Ozs7OztRQVF6QyxhQUFRLEdBQUcsSUFBSSxlQUFlLENBQVMsQ0FBQyxDQUFDLENBQUM7Ozs7Ozs7UUFRMUMsZ0JBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FDOUIsR0FBRzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ1YsT0FBTyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixDQUFDLEVBQUMsQ0FDSCxDQUFDO1FBV0EsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3pDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFLRCxRQUFRO1FBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBS0QsR0FBRztRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7Ozs7SUFNRCxNQUFNLENBQUMsS0FBYTtRQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7Ozs7SUFPRCxJQUFJLENBQUMsS0FBaUM7O2NBQzlCLFNBQVMsR0FBRyxLQUFLLENBQUMsYUFBYTs7Y0FDL0IsU0FBUyxHQUFHLEtBQUssQ0FBQyxZQUFZOztjQUM5QixTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7Ozs7SUFPRCxtQkFBbUI7O2NBQ1gsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUMzQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUMzQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDO1NBQzlEO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs7OztJQUtELElBQ0ksS0FBSztRQUNQLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDekIsQ0FBQzs7Ozs7O0lBS0QsSUFBSSxLQUFLLENBQUMsQ0FBZTtRQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBS0QsSUFDSSxRQUFRO1FBQ1YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7Ozs7OztJQUtELElBQUksUUFBUSxDQUFDLEtBQWM7UUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBS0QsSUFDSSxRQUFRO1FBQ1YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7Ozs7OztJQUtELElBQUksUUFBUSxDQUFDLEtBQWM7UUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBS0QsSUFDSSxXQUFXO1FBQ2IsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzNCLENBQUM7Ozs7OztJQUtELElBQUksV0FBVyxDQUFDLEtBQWE7UUFDM0IsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7OztJQUtELElBQUksS0FBSztRQUNQLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7O0lBS0QsSUFBSSxnQkFBZ0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNyQyxDQUFDOzs7Ozs7SUFLRCxpQkFBaUIsQ0FBQyxHQUFhO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7Ozs7SUFLRCxnQkFBZ0IsQ0FBQyxLQUFpQjtRQUNoQyxJQUFJLENBQUMsbUJBQUEsS0FBSyxDQUFDLE1BQU0sRUFBVyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxLQUFLLE9BQU8sRUFBRTtZQUMvRCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDekQ7SUFDSCxDQUFDOzs7OztJQUtELFdBQVc7UUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7OztBQWxRTSwwQkFBTSxHQUFHLENBQUMsQ0FBQzs7WUFmbkIsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLHNtREFBMkM7Z0JBRTNDLFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsbUJBQW1CO3dCQUM1QixXQUFXLEVBQUUsbUJBQW1CO3FCQUNqQztpQkFDRjs7YUFDRjs7OztZQXBCUSxZQUFZO1lBTG5CLFVBQVU7NENBZ0lQLE1BQU0sU0FBQyxRQUFRO1lBekhFLFNBQVMsdUJBMEgxQixRQUFRLFlBQUksSUFBSTs7O21CQWpEbEIsS0FBSzt3QkFLTCxLQUFLO3NCQUtMLEtBQUs7eUJBS0wsS0FBSztvQkFLTCxLQUFLO29CQTJGTCxLQUFLO3VCQWdCTCxLQUFLO3VCQWdCTCxLQUFLOzBCQWdCTCxLQUFLOzs7Ozs7O0lBak5OLDJCQUFrQjs7Ozs7O0lBS2xCLHdDQUEwQjs7Ozs7O0lBSzFCLHdDQUEwQjs7Ozs7O0lBSzFCLDJDQUE2Qjs7Ozs7SUFLN0IsMkNBQW1DOzs7OztJQUtuQyxzQ0FBZ0I7Ozs7O0lBS2hCLHlDQUFtQjs7Ozs7SUFLbkIsMENBQWlDOzs7OztJQUtqQyxpQ0FBd0Q7Ozs7O0lBS3hELDBDQUFpQjs7Ozs7SUFLakIsbUNBQXNDOzs7OztJQUt0Qyx3Q0FBOEI7Ozs7O0lBSzlCLHNDQUE2Qjs7Ozs7SUFLN0IseUNBQXlEOzs7OztJQUt6RCxvQ0FBeUM7Ozs7Ozs7O0lBUXpDLHVDQUEwQzs7Ozs7Ozs7SUFRMUMsMENBSUU7Ozs7O0lBTUEsaUNBQXdCOzs7OztJQUN4QixvQ0FBc0M7Ozs7O0lBQ3RDLHFDQUE0Qzs7SUFDNUMsd0NBQStDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25Jbml0LFxyXG4gIE9uRGVzdHJveSxcclxuICBJbmplY3QsXHJcbiAgRWxlbWVudFJlZixcclxuICBJbnB1dCxcclxuICBPcHRpb25hbCxcclxuICBTZWxmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvY3VzTW9uaXRvciB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9hMTF5JztcclxuaW1wb3J0IHsgY29lcmNlQm9vbGVhblByb3BlcnR5IH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvZXJjaW9uJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIE5nQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTWF0Rm9ybUZpZWxkQ29udHJvbCwgTWF0Rm9ybUZpZWxkQXBwZWFyYW5jZSwgVGhlbWVQYWxldHRlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBDZGtEcmFnRHJvcCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9kcmFnLWRyb3AnO1xyXG5pbXBvcnQgeyBLZXBzRm9ybUFycmF5IH0gZnJvbSAnLi4va2Vwcy1mb3JtLWFycmF5JztcclxuaW1wb3J0IHsgS2Vwc01vZGVscyB9IGZyb20gJy4uLy4uLy4uL2ludGVyZmFjZSc7XHJcbmltcG9ydCB7IFN1YmplY3QsIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2tlcHMtZmllbGQtYXJyYXknLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9maWVsZC1hcnJheS5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZmllbGQtYXJyYXkuY29tcG9uZW50LnNjc3MnXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogTWF0Rm9ybUZpZWxkQ29udHJvbCxcclxuICAgICAgdXNlRXhpc3Rpbmc6IEZpZWxkQXJyYXlDb21wb25lbnRcclxuICAgIH1cclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGaWVsZEFycmF5Q29tcG9uZW50IGltcGxlbWVudHMgTWF0Rm9ybUZpZWxkQ29udHJvbDxhbnlbXT4sIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzdGF0aWMgbmV4dElkID0gMDtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfcmVxdWlyZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfZGlzYWJsZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfcGxhY2Vob2xkZXI6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc3RhdGVDaGFuZ2VzID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZm9jdXNlZCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBlcnJvclN0YXRlID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnRyb2xUeXBlID0gJ2tlcHMtZmllbGQtYXJyYXknO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBpZCA9IGBrZXBzLWZpZWxkLWFycmF5LSR7RmllbGRBcnJheUNvbXBvbmVudC5uZXh0SWQrK31gO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBkZXNjcmliZWRCeSA9ICcnO1xyXG5cclxuICAvKipcclxuICAgKiBGb3JtIGNvbnRyb2wgb2YgdGhlIGZpZWxkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGZvcm0gPSBuZXcgS2Vwc0Zvcm1BcnJheShbXSk7XHJcblxyXG4gIC8qKlxyXG4gICAqIFN1YnNjaGVtYSBvZiB0aGUgZm9ybSBhcnJheS5cclxuICAgKi9cclxuICBASW5wdXQoKSBzdWJTY2hlbWEgPSAnc3RyaW5nJztcclxuXHJcbiAgLyoqXHJcbiAgICogT2JqZWN0cyB0byBiZSBzaG93biBmb3IgcmVmZXJlbmNlIGZpZWxkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHJlZk9ianM6IGFueVtdID0gW107XHJcblxyXG4gIC8qKlxyXG4gICAqIEFwcGVhcmFuY2Ugc3R5bGUgb2YgdGhlIGZvcm0uXHJcbiAgICovXHJcbiAgQElucHV0KCkgYXBwZWFyYW5jZTogTWF0Rm9ybUZpZWxkQXBwZWFyYW5jZSA9ICdzdGFuZGFyZCc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFuZ3VsYXIgTWF0ZXJpYWwgY29sb3Igb2YgdGhlIGZvcm0gZmllbGQgdW5kZXJsaW5lIGFuZCBmbG9hdGluZyBsYWJlbC5cclxuICAgKi9cclxuICBASW5wdXQoKSBjb2xvcjogVGhlbWVQYWxldHRlID0gJ3ByaW1hcnknO1xyXG5cclxuICAvKipcclxuICAgKiBBIGJlaGF2aW9yIHN1YmplY3QgdGhhdCBlbWl0cyBhIG51bWJlciB3aGljaCBpcyB0aGUgbGVuZ3RoIG9mIHRoZSBmb3JtIGFycmF5LlxyXG4gICAqXHJcbiAgICogSXQgc2hvdWxkIGVtaXQgd2hlbmV2ZXIgYSBmb3JtIGNvbnRyb2wgaXMgYWRkZWQvcmVtb3ZlZCBmcm9tIHRoZSBhcnJheS5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb3VudGVyJCA9IG5ldyBCZWhhdmlvclN1YmplY3Q8bnVtYmVyPigwKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQW4gb2JzZXJ2YWJsZSB0aGF0IGVtaXRzIGFuIGVtcHR5IGFycmF5IHdpdGggdGhlIHNhbWUgbGVuZ3RoIGFzIHRoZSBmb3JtIGFycmF5LlxyXG4gICAqXHJcbiAgICogSXQgaXMgdXNlZCBmb3IgKm5nRm9yIGluIHRoaXMgY29tcG9uZW50J3MgSFRNTC5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb3VudGVyQXJyJCA9IHRoaXMuY291bnRlciQucGlwZShcclxuICAgIG1hcCh2YWx1ZSA9PiB7XHJcbiAgICAgIHJldHVybiBuZXcgQXJyYXkodmFsdWUpO1xyXG4gICAgfSlcclxuICApO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm06IEZvY3VzTW9uaXRvcixcclxuICAgIHByaXZhdGUgZWxSZWY6IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+LFxyXG4gICAgQEluamVjdCgnTU9ERUxTJykgcHJpdmF0ZSBtb2RlbHM6IEtlcHNNb2RlbHMsXHJcbiAgICBAT3B0aW9uYWwoKSBAU2VsZigpIHB1YmxpYyBuZ0NvbnRyb2w6IE5nQ29udHJvbFxyXG4gICkge1xyXG4gICAgZm0ubW9uaXRvcihlbFJlZiwgdHJ1ZSkuc3Vic2NyaWJlKG9yaWdpbiA9PiB7XHJcbiAgICAgIHRoaXMuZm9jdXNlZCA9ICEhb3JpZ2luO1xyXG4gICAgICB0aGlzLnN0YXRlQ2hhbmdlcy5uZXh0KCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5mb3JtLnNldENvdW50ZXIkKHRoaXMuY291bnRlciQpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQWRkcyBhIGZvcm0gY29udHJvbCB0byB0aGUgZm9ybSBhcnJheS5cclxuICAgKi9cclxuICBhZGQoKSB7XHJcbiAgICB0aGlzLmZvcm0ucHVzaChuZXcgRm9ybUNvbnRyb2woJycpKTtcclxuICAgIHRoaXMuY291bnRlciQubmV4dCh0aGlzLmZvcm0ubGVuZ3RoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJlbW92ZXMgYSBmb3JtIGNvbnRyb2wgZnJvbSB0aGUgZm9ybSBhcnJheS5cclxuICAgKiBAcGFyYW0gaW5kZXggSW5kZXggb2YgdGhlIGZvcm0gY29udHJvbCB0byBiZSByZW1vdmVkLlxyXG4gICAqL1xyXG4gIHJlbW92ZShpbmRleDogbnVtYmVyKSB7XHJcbiAgICB0aGlzLmZvcm0ucmVtb3ZlQXQoaW5kZXgpO1xyXG4gICAgdGhpcy5mb2N1c2VkID0gZmFsc2U7XHJcbiAgICB0aGlzLmNvdW50ZXIkLm5leHQodGhpcy5mb3JtLmxlbmd0aCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZWFycmFuZ2VzIGZvcm0gY29udHJvbHMgaW5zaWRlIHRoZSBmb3JtIGFycmF5LlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqIEBwYXJhbSBldmVudCBUaGUgZHJhZyBldmVudCBjb250YWluaW5nIHRoZSBmb3JtIGNvbnRyb2wuXHJcbiAgICovXHJcbiAgZHJvcChldmVudDogQ2RrRHJhZ0Ryb3A8Rm9ybUNvbnRyb2xbXT4pIHtcclxuICAgIGNvbnN0IHByZXZJbmRleCA9IGV2ZW50LnByZXZpb3VzSW5kZXg7XHJcbiAgICBjb25zdCBuZXh0SW5kZXggPSBldmVudC5jdXJyZW50SW5kZXg7XHJcbiAgICBjb25zdCBtb3ZlZEZvcm0gPSB0aGlzLmZvcm0uYXQocHJldkluZGV4KTtcclxuICAgIHRoaXMuZm9ybS5yZW1vdmVBdChwcmV2SW5kZXgpO1xyXG4gICAgdGhpcy5mb3JtLmluc2VydChuZXh0SW5kZXgsIG1vdmVkRm9ybSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXR1cm5zIHRoZSBkaXNwbGF5IGV4cHJlc3Npb24gb2YgYSByZWZlcmVuY2UgZmllbGQuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICogQHBhcmFtIGZpZWxkIEZpZWxkIHRvIGdldCBmcm9tLlxyXG4gICAqL1xyXG4gIGdldFJlZmVyZW5jZURpc3BsYXkoKSB7XHJcbiAgICBjb25zdCByZWZlcmVuY2VUbyA9IHRoaXMuc3ViU2NoZW1hLnNsaWNlKDEpO1xyXG4gICAgaWYgKHRoaXMubW9kZWxzICYmIHRoaXMubW9kZWxzW3JlZmVyZW5jZVRvXSkge1xyXG4gICAgICByZXR1cm4gdGhpcy5tb2RlbHNbcmVmZXJlbmNlVG9dLnByb3BlcnRpZXMuZGlzcGxheUV4cHJlc3Npb247XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbnVsbDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSB2YWx1ZSBvZiB0aGUgY29udHJvbC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCB2YWx1ZSgpOiBhbnlbXSB8IG51bGwge1xyXG4gICAgcmV0dXJuIHRoaXMuZm9ybS52YWx1ZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCB2YWx1ZSh2OiBhbnlbXSB8IG51bGwpIHtcclxuICAgIHRoaXMuZm9ybS5wYXRjaFZhbHVlKHYpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyByZXF1aXJlZC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCByZXF1aXJlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9yZXF1aXJlZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCByZXF1aXJlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fcmVxdWlyZWQgPSBjb2VyY2VCb29sZWFuUHJvcGVydHkodmFsdWUpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyBkaXNhYmxlZC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCBkaXNhYmxlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9kaXNhYmxlZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCBkaXNhYmxlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fZGlzYWJsZWQgPSBjb2VyY2VCb29sZWFuUHJvcGVydHkodmFsdWUpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIHBsYWNlaG9sZGVyIGZvciB0aGlzIGNvbnRyb2wuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgcGxhY2Vob2xkZXIoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLl9wbGFjZWhvbGRlcjtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCBwbGFjZWhvbGRlcih2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLl9wbGFjZWhvbGRlciA9IHZhbHVlO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyBlbXB0eS5cclxuICAgKi9cclxuICBnZXQgZW1wdHkoKSB7XHJcbiAgICByZXR1cm4gIXRoaXMuZm9ybS52YWx1ZSB8fCB0aGlzLmZvcm0udmFsdWUubGVuZ3RoID09PSAwO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgTWF0Rm9ybUZpZWxkIGxhYmVsIHNob3VsZCB0cnkgdG8gZmxvYXQuXHJcbiAgICovXHJcbiAgZ2V0IHNob3VsZExhYmVsRmxvYXQoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5mb2N1c2VkIHx8ICF0aGlzLmVtcHR5O1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc2V0RGVzY3JpYmVkQnlJZHMoaWRzOiBzdHJpbmdbXSkge1xyXG4gICAgdGhpcy5kZXNjcmliZWRCeSA9IGlkcy5qb2luKCcgJyk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBvbkNvbnRhaW5lckNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICBpZiAoKGV2ZW50LnRhcmdldCBhcyBFbGVtZW50KS50YWdOYW1lLnRvTG93ZXJDYXNlKCkgIT09ICdpbnB1dCcpIHtcclxuICAgICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ2lucHV0JykuZm9jdXMoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMuY29tcGxldGUoKTtcclxuICAgIHRoaXMuZm0uc3RvcE1vbml0b3JpbmcodGhpcy5lbFJlZik7XHJcbiAgfVxyXG59XHJcbiJdfQ==