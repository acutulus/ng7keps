/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { first } from 'rxjs/operators';
import _ from 'lodash';
/**
 * A Keps form.
 *
 * It extends Angular's `FormGroup` class
 * and changes its `patchValue` and `reset` methods to
 * wait until the form is initialized.
 */
export class KepsForm extends FormGroup {
    constructor() {
        super(...arguments);
        this.initialized = false;
        this.initialized$ = new BehaviorSubject(false);
    }
    /**
     * Lets the Keps form know that is has been initialized.
     * \@internal Do not use!
     * @return {?}
     */
    initialize() {
        this.initialized = true;
        this.initialized$.next(true);
    }
    /**
     * Sets the schema for this form.
     * \@internal Do not use!
     * @param {?} schema Schema to be used.
     * @return {?}
     */
    setSchema(schema) {
        this.schema = schema;
    }
    /**
     * Patches the value of the Keps form.
     * It accepts an object with control names as keys, and does its best to match the values to the correct controls in the group.
     *
     * It accepts both super-sets and sub-sets of the group without throwing an error.
     * @param {?} values Values to be passed to the fields.
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    patchValue(values, options = {}) {
        // Check if form is initialized or not.
        if (this.initialized) {
            if (options === void 0) {
                options = {};
            }
            for (const field of Object.keys(values)) {
                /** @type {?} */
                let value = values[field];
                // If value is an object with an ID, convert to ID reference.
                if (_.isPlainObject(value) && value._id) {
                    value = value._id;
                }
                // Pass in value to child controls.
                if (this.get(field)) {
                    this.get(field).patchValue(value, { onlySelf: true, emitEvent: options.emitEvent });
                }
            }
            this.updateValueAndValidity(options);
        }
        else {
            // If form is not initialized, wait until it is before patching.
            this.initialized$
                .pipe(first((/**
             * @param {?} value
             * @return {?}
             */
            value => value)))
                .subscribe((/**
             * @param {?} value
             * @return {?}
             */
            value => {
                this.patchValue(values, options);
            }));
        }
    }
    /**
     * Resets the Keps form, marks all descendants are marked pristine and untouched, and the value of all descendants to null.
     *
     * You reset to a specific form state by passing in a map of states that matches the structure of your form, with control names as keys.
     * The state is a standalone value or a form state object with both a value and a disabled status.
     * @param {?=} values Resets the control with an initial value, or an object that defines the initial value and disabled state.
     * @param {?=} options
     * Configuration options that determine how the control propagates changes and emits events after the value is patched.
     * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
     * - `emitEvent`:
     * When true or not supplied (the default),
     * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
     * When false, no events are emitted.
     * The configuration options are passed to the `updateValueAndValidity` method.
     * @return {?}
     */
    reset(values, options = {}) {
        if (this.initialized) {
            if (values === void 0) {
                values = {};
            }
            // Reset each fields in the control.
            for (const field in this.controls) {
                // If no value supplied for this field, try using the schema's default value.
                if (!values[field] && this.schema[field] && this.schema[field].default) {
                    values[field] = this.schema[field].default;
                }
                this.get(field).reset(values[field], { onlySelf: true, emitEvent: options.emitEvent });
            }
            this.markAsPristine(options);
            this.markAsUntouched(options);
            this.updateValueAndValidity(options);
        }
        else {
            // If form is not initialized, wait until it is before reseting.
            this.initialized$
                .pipe(first((/**
             * @param {?} value
             * @return {?}
             */
            value => value)))
                .subscribe((/**
             * @param {?} value
             * @return {?}
             */
            value => {
                this.reset(values, options);
            }));
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    KepsForm.prototype.schema;
    /**
     * @type {?}
     * @private
     */
    KepsForm.prototype.initialized;
    /**
     * @type {?}
     * @private
     */
    KepsForm.prototype.initialized$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2Vwcy1mb3JtLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvZm9ybS9rZXBzLWZvcm0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3ZDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN2QyxPQUFPLENBQUMsTUFBTSxRQUFRLENBQUM7Ozs7Ozs7O0FBU3ZCLE1BQU0sT0FBTyxRQUFTLFNBQVEsU0FBUztJQUF2Qzs7UUFFVSxnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUNwQixpQkFBWSxHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO0lBb0g3RCxDQUFDOzs7Ozs7SUE5R0MsVUFBVTtRQUNSLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7Ozs7Ozs7SUFPRCxTQUFTLENBQUMsTUFBa0I7UUFDMUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDdkIsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFpQkQsVUFBVSxDQUNSLE1BQStCLEVBQy9CLFVBQXdELEVBQUU7UUFFMUQsdUNBQXVDO1FBQ3ZDLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNwQixJQUFJLE9BQU8sS0FBSyxLQUFLLENBQUMsRUFBRTtnQkFBRSxPQUFPLEdBQUcsRUFBRSxDQUFDO2FBQUU7WUFFekMsS0FBSyxNQUFNLEtBQUssSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFOztvQkFDbkMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7Z0JBRXpCLDZEQUE2RDtnQkFDN0QsSUFBSSxDQUFDLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxHQUFHLEVBQUU7b0JBQ3ZDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDO2lCQUNuQjtnQkFFRCxtQ0FBbUM7Z0JBQ25DLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDbkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7aUJBQ3JGO2FBQ0Y7WUFFRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDdEM7YUFBTTtZQUNMLGdFQUFnRTtZQUNoRSxJQUFJLENBQUMsWUFBWTtpQkFDaEIsSUFBSSxDQUNILEtBQUs7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBQyxDQUN0QjtpQkFDQSxTQUFTOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ25DLENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7OztJQWlCRCxLQUFLLENBQ0gsTUFBWSxFQUNaLFVBQXdELEVBQUU7UUFFMUQsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3BCLElBQUksTUFBTSxLQUFLLEtBQUssQ0FBQyxFQUFFO2dCQUFFLE1BQU0sR0FBRyxFQUFFLENBQUM7YUFBRTtZQUV2QyxvQ0FBb0M7WUFDcEMsS0FBSyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNqQyw2RUFBNkU7Z0JBQzdFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sRUFBRTtvQkFDdEUsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDO2lCQUM1QztnQkFDRCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQzthQUN4RjtZQUVELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM5QixJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDdEM7YUFBTTtZQUNMLGdFQUFnRTtZQUNoRSxJQUFJLENBQUMsWUFBWTtpQkFDaEIsSUFBSSxDQUNILEtBQUs7Ozs7WUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssRUFBQyxDQUN0QjtpQkFDQSxTQUFTOzs7O1lBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQzlCLENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0NBRUY7Ozs7OztJQXRIQywwQkFBMkI7Ozs7O0lBQzNCLCtCQUE0Qjs7Ozs7SUFDNUIsZ0NBQTJEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybUdyb3VwIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBLZXBzU2NoZW1hIH0gZnJvbSAnLi4vLi4vaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IGZpcnN0IH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgXyBmcm9tICdsb2Rhc2gnO1xyXG5cclxuLyoqXHJcbiAqIEEgS2VwcyBmb3JtLlxyXG4gKlxyXG4gKiBJdCBleHRlbmRzIEFuZ3VsYXIncyBgRm9ybUdyb3VwYCBjbGFzc1xyXG4gKiBhbmQgY2hhbmdlcyBpdHMgYHBhdGNoVmFsdWVgIGFuZCBgcmVzZXRgIG1ldGhvZHMgdG9cclxuICogd2FpdCB1bnRpbCB0aGUgZm9ybSBpcyBpbml0aWFsaXplZC5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBLZXBzRm9ybSBleHRlbmRzIEZvcm1Hcm91cCB7XHJcbiAgcHJpdmF0ZSBzY2hlbWE6IEtlcHNTY2hlbWE7XHJcbiAgcHJpdmF0ZSBpbml0aWFsaXplZCA9IGZhbHNlO1xyXG4gIHByaXZhdGUgaW5pdGlhbGl6ZWQkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPihmYWxzZSk7XHJcblxyXG4gIC8qKlxyXG4gICAqIExldHMgdGhlIEtlcHMgZm9ybSBrbm93IHRoYXQgaXMgaGFzIGJlZW4gaW5pdGlhbGl6ZWQuXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgaW5pdGlhbGl6ZSgpIHtcclxuICAgIHRoaXMuaW5pdGlhbGl6ZWQgPSB0cnVlO1xyXG4gICAgdGhpcy5pbml0aWFsaXplZCQubmV4dCh0cnVlKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNldHMgdGhlIHNjaGVtYSBmb3IgdGhpcyBmb3JtLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqIEBwYXJhbSBzY2hlbWEgU2NoZW1hIHRvIGJlIHVzZWQuXHJcbiAgICovXHJcbiAgc2V0U2NoZW1hKHNjaGVtYTogS2Vwc1NjaGVtYSkge1xyXG4gICAgdGhpcy5zY2hlbWEgPSBzY2hlbWE7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBQYXRjaGVzIHRoZSB2YWx1ZSBvZiB0aGUgS2VwcyBmb3JtLlxyXG4gICAqIEl0IGFjY2VwdHMgYW4gb2JqZWN0IHdpdGggY29udHJvbCBuYW1lcyBhcyBrZXlzLCBhbmQgZG9lcyBpdHMgYmVzdCB0byBtYXRjaCB0aGUgdmFsdWVzIHRvIHRoZSBjb3JyZWN0IGNvbnRyb2xzIGluIHRoZSBncm91cC5cclxuICAgKlxyXG4gICAqIEl0IGFjY2VwdHMgYm90aCBzdXBlci1zZXRzIGFuZCBzdWItc2V0cyBvZiB0aGUgZ3JvdXAgd2l0aG91dCB0aHJvd2luZyBhbiBlcnJvci5cclxuICAgKiBAcGFyYW0gdmFsdWVzIFZhbHVlcyB0byBiZSBwYXNzZWQgdG8gdGhlIGZpZWxkcy5cclxuICAgKiBAcGFyYW0gb3B0aW9uc1xyXG4gICAqIENvbmZpZ3VyYXRpb24gb3B0aW9ucyB0aGF0IGRldGVybWluZSBob3cgdGhlIGNvbnRyb2wgcHJvcGFnYXRlcyBjaGFuZ2VzIGFuZCBlbWl0cyBldmVudHMgYWZ0ZXIgdGhlIHZhbHVlIGlzIHBhdGNoZWQuXHJcbiAgICogLSBgb25seVNlbGZgOiBXaGVuIHRydWUsIGVhY2ggY2hhbmdlIG9ubHkgYWZmZWN0cyB0aGlzIGNvbnRyb2wgYW5kIG5vdCBpdHMgcGFyZW50LiBEZWZhdWx0IGlzIHRydWUuXHJcbiAgICogLSBgZW1pdEV2ZW50YDpcclxuICAgKiBXaGVuIHRydWUgb3Igbm90IHN1cHBsaWVkICh0aGUgZGVmYXVsdCksXHJcbiAgICogYm90aCB0aGUgYHN0YXR1c0NoYW5nZXNgIGFuZCBgdmFsdWVDaGFuZ2VzYCBvYnNlcnZhYmxlcyBlbWl0IGV2ZW50cyB3aXRoIHRoZSBsYXRlc3Qgc3RhdHVzIGFuZCB2YWx1ZSB3aGVuIHRoZSBjb250cm9sIHZhbHVlIGlzIHVwZGF0ZWQuXHJcbiAgICogV2hlbiBmYWxzZSwgbm8gZXZlbnRzIGFyZSBlbWl0dGVkLlxyXG4gICAqIFRoZSBjb25maWd1cmF0aW9uIG9wdGlvbnMgYXJlIHBhc3NlZCB0byB0aGUgYHVwZGF0ZVZhbHVlQW5kVmFsaWRpdHlgIG1ldGhvZC5cclxuICAgKi9cclxuICBwYXRjaFZhbHVlKFxyXG4gICAgdmFsdWVzOiB7IFtrZXk6IHN0cmluZ106IGFueTsgfSxcclxuICAgIG9wdGlvbnM6IHsgb25seVNlbGY/OiBib29sZWFuOyBlbWl0RXZlbnQ/OiBib29sZWFuOyB9ID0ge31cclxuICApOiB2b2lkIHtcclxuICAgIC8vIENoZWNrIGlmIGZvcm0gaXMgaW5pdGlhbGl6ZWQgb3Igbm90LlxyXG4gICAgaWYgKHRoaXMuaW5pdGlhbGl6ZWQpIHtcclxuICAgICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkgeyBvcHRpb25zID0ge307IH1cclxuXHJcbiAgICAgIGZvciAoY29uc3QgZmllbGQgb2YgT2JqZWN0LmtleXModmFsdWVzKSkge1xyXG4gICAgICAgIGxldCB2YWx1ZSA9IHZhbHVlc1tmaWVsZF07XHJcblxyXG4gICAgICAgIC8vIElmIHZhbHVlIGlzIGFuIG9iamVjdCB3aXRoIGFuIElELCBjb252ZXJ0IHRvIElEIHJlZmVyZW5jZS5cclxuICAgICAgICBpZiAoXy5pc1BsYWluT2JqZWN0KHZhbHVlKSAmJiB2YWx1ZS5faWQpIHtcclxuICAgICAgICAgIHZhbHVlID0gdmFsdWUuX2lkO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gUGFzcyBpbiB2YWx1ZSB0byBjaGlsZCBjb250cm9scy5cclxuICAgICAgICBpZiAodGhpcy5nZXQoZmllbGQpKSB7XHJcbiAgICAgICAgICB0aGlzLmdldChmaWVsZCkucGF0Y2hWYWx1ZSh2YWx1ZSwgeyBvbmx5U2VsZjogdHJ1ZSwgZW1pdEV2ZW50OiBvcHRpb25zLmVtaXRFdmVudCB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMudXBkYXRlVmFsdWVBbmRWYWxpZGl0eShvcHRpb25zKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIElmIGZvcm0gaXMgbm90IGluaXRpYWxpemVkLCB3YWl0IHVudGlsIGl0IGlzIGJlZm9yZSBwYXRjaGluZy5cclxuICAgICAgdGhpcy5pbml0aWFsaXplZCRcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgZmlyc3QodmFsdWUgPT4gdmFsdWUpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZSh2YWx1ZSA9PiB7XHJcbiAgICAgICAgdGhpcy5wYXRjaFZhbHVlKHZhbHVlcywgb3B0aW9ucyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUmVzZXRzIHRoZSBLZXBzIGZvcm0sIG1hcmtzIGFsbCBkZXNjZW5kYW50cyBhcmUgbWFya2VkIHByaXN0aW5lIGFuZCB1bnRvdWNoZWQsIGFuZCB0aGUgdmFsdWUgb2YgYWxsIGRlc2NlbmRhbnRzIHRvIG51bGwuXHJcbiAgICpcclxuICAgKiBZb3UgcmVzZXQgdG8gYSBzcGVjaWZpYyBmb3JtIHN0YXRlIGJ5IHBhc3NpbmcgaW4gYSBtYXAgb2Ygc3RhdGVzIHRoYXQgbWF0Y2hlcyB0aGUgc3RydWN0dXJlIG9mIHlvdXIgZm9ybSwgd2l0aCBjb250cm9sIG5hbWVzIGFzIGtleXMuXHJcbiAgICogVGhlIHN0YXRlIGlzIGEgc3RhbmRhbG9uZSB2YWx1ZSBvciBhIGZvcm0gc3RhdGUgb2JqZWN0IHdpdGggYm90aCBhIHZhbHVlIGFuZCBhIGRpc2FibGVkIHN0YXR1cy5cclxuICAgKiBAcGFyYW0gdmFsdWVzIFJlc2V0cyB0aGUgY29udHJvbCB3aXRoIGFuIGluaXRpYWwgdmFsdWUsIG9yIGFuIG9iamVjdCB0aGF0IGRlZmluZXMgdGhlIGluaXRpYWwgdmFsdWUgYW5kIGRpc2FibGVkIHN0YXRlLlxyXG4gICAqIEBwYXJhbSBvcHRpb25zXHJcbiAgICogQ29uZmlndXJhdGlvbiBvcHRpb25zIHRoYXQgZGV0ZXJtaW5lIGhvdyB0aGUgY29udHJvbCBwcm9wYWdhdGVzIGNoYW5nZXMgYW5kIGVtaXRzIGV2ZW50cyBhZnRlciB0aGUgdmFsdWUgaXMgcGF0Y2hlZC5cclxuICAgKiAtIGBvbmx5U2VsZmA6IFdoZW4gdHJ1ZSwgZWFjaCBjaGFuZ2Ugb25seSBhZmZlY3RzIHRoaXMgY29udHJvbCBhbmQgbm90IGl0cyBwYXJlbnQuIERlZmF1bHQgaXMgdHJ1ZS5cclxuICAgKiAtIGBlbWl0RXZlbnRgOlxyXG4gICAqIFdoZW4gdHJ1ZSBvciBub3Qgc3VwcGxpZWQgKHRoZSBkZWZhdWx0KSxcclxuICAgKiBib3RoIHRoZSBgc3RhdHVzQ2hhbmdlc2AgYW5kIGB2YWx1ZUNoYW5nZXNgIG9ic2VydmFibGVzIGVtaXQgZXZlbnRzIHdpdGggdGhlIGxhdGVzdCBzdGF0dXMgYW5kIHZhbHVlIHdoZW4gdGhlIGNvbnRyb2wgdmFsdWUgaXMgdXBkYXRlZC5cclxuICAgKiBXaGVuIGZhbHNlLCBubyBldmVudHMgYXJlIGVtaXR0ZWQuXHJcbiAgICogVGhlIGNvbmZpZ3VyYXRpb24gb3B0aW9ucyBhcmUgcGFzc2VkIHRvIHRoZSBgdXBkYXRlVmFsdWVBbmRWYWxpZGl0eWAgbWV0aG9kLlxyXG4gICAqL1xyXG4gIHJlc2V0KFxyXG4gICAgdmFsdWVzPzogYW55LFxyXG4gICAgb3B0aW9uczogeyBvbmx5U2VsZj86IGJvb2xlYW47IGVtaXRFdmVudD86IGJvb2xlYW47IH0gPSB7fVxyXG4gICk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuaW5pdGlhbGl6ZWQpIHtcclxuICAgICAgaWYgKHZhbHVlcyA9PT0gdm9pZCAwKSB7IHZhbHVlcyA9IHt9OyB9XHJcblxyXG4gICAgICAvLyBSZXNldCBlYWNoIGZpZWxkcyBpbiB0aGUgY29udHJvbC5cclxuICAgICAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmNvbnRyb2xzKSB7XHJcbiAgICAgICAgLy8gSWYgbm8gdmFsdWUgc3VwcGxpZWQgZm9yIHRoaXMgZmllbGQsIHRyeSB1c2luZyB0aGUgc2NoZW1hJ3MgZGVmYXVsdCB2YWx1ZS5cclxuICAgICAgICBpZiAoIXZhbHVlc1tmaWVsZF0gJiYgdGhpcy5zY2hlbWFbZmllbGRdICYmIHRoaXMuc2NoZW1hW2ZpZWxkXS5kZWZhdWx0KSB7XHJcbiAgICAgICAgICB2YWx1ZXNbZmllbGRdID0gdGhpcy5zY2hlbWFbZmllbGRdLmRlZmF1bHQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZ2V0KGZpZWxkKS5yZXNldCh2YWx1ZXNbZmllbGRdLCB7IG9ubHlTZWxmOiB0cnVlLCBlbWl0RXZlbnQ6IG9wdGlvbnMuZW1pdEV2ZW50IH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLm1hcmtBc1ByaXN0aW5lKG9wdGlvbnMpO1xyXG4gICAgICB0aGlzLm1hcmtBc1VudG91Y2hlZChvcHRpb25zKTtcclxuICAgICAgdGhpcy51cGRhdGVWYWx1ZUFuZFZhbGlkaXR5KG9wdGlvbnMpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gSWYgZm9ybSBpcyBub3QgaW5pdGlhbGl6ZWQsIHdhaXQgdW50aWwgaXQgaXMgYmVmb3JlIHJlc2V0aW5nLlxyXG4gICAgICB0aGlzLmluaXRpYWxpemVkJFxyXG4gICAgICAucGlwZShcclxuICAgICAgICBmaXJzdCh2YWx1ZSA9PiB2YWx1ZSlcclxuICAgICAgKVxyXG4gICAgICAuc3Vic2NyaWJlKHZhbHVlID0+IHtcclxuICAgICAgICB0aGlzLnJlc2V0KHZhbHVlcywgb3B0aW9ucyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19