/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ElementRef, Optional, Self } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormGroup, FormControl, NgControl, Validators } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material';
import { Subject } from 'rxjs';
export class FieldAddressComponent {
    /**
     * \@internal Do not use!
     * @param {?} fm
     * @param {?} elRef
     * @param {?} ngControl
     */
    constructor(fm, elRef, ngControl) {
        this.fm = fm;
        this.elRef = elRef;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-address';
        /**
         * \@internal Do not use!
         */
        this.id = `keps-field-address-${FieldAddressComponent.nextId++}`;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new FormGroup({});
        /**
         * Appearance style of the form.
         */
        this.appearance = 'standard';
        /**
         * Angular Material color of the form field underline and floating label.
         */
        this.color = 'primary';
        /**
         * Whether the field has been initialized or not.
         * \@internal Do not use!
         */
        this.initialized = false;
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        }));
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const validators = [];
        if (this.required) {
            validators.push(Validators.required);
        }
        for (const field of ['address1', 'address2', 'city', 'region', 'postal', 'country']) {
            this.form.addControl(field, new FormControl(null, validators));
        }
        this.initialized = true;
    }
    /**
     * The value of the control.
     * @return {?}
     */
    get value() {
        return this.form.value;
    }
    /**
     * \@internal Do not use!
     * @param {?} v
     * @return {?}
     */
    set value(v) {
        this.form.patchValue(v);
        this.stateChanges.next();
    }
    /**
     * Whether the control is required.
     * @return {?}
     */
    get required() {
        return this._required;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set required(value) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * Whether the control is disabled.
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * The placeholder for this control.
     * @return {?}
     */
    get placeholder() {
        return this._placeholder;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }
    /**
     * Whether the control is empty.
     * @return {?}
     */
    get empty() {
        const { address1, address2, city, region, postal, country } = this.form.value;
        return !address1 && !address2 && !city && !region && !postal && !country;
    }
    /**
     * Whether the MatFormField label should try to float.
     * @return {?}
     */
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    setDescribedByIds(ids) {
        this.describedBy = ids.join(' ');
    }
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    onContainerClick(event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    }
}
/**
 * \@internal Do not use!
 */
FieldAddressComponent.nextId = 0;
FieldAddressComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-field-address',
                template: "<ng-container *ngIf=\"initialized\">\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Address 1\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('address1')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Address 2\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('address2')\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      City\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('city')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      State\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('region')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Zipcode\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('postal')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n  <mat-form-field\r\n    [appearance]=\"appearance\"\r\n    [color]=\"color\"\r\n    floatLabel=\"always\"\r\n    class=\"field-address\"\r\n  >\r\n    <mat-label>\r\n      Country\r\n    </mat-label>\r\n    <input\r\n      [formControl]=\"form.get('country')\"\r\n      [required]=\"required\"\r\n      type=\"text\"\r\n      matInput\r\n    >\r\n  </mat-form-field>\r\n</ng-container>\r\n",
                providers: [
                    {
                        provide: MatFormFieldControl,
                        useExisting: FieldAddressComponent
                    }
                ],
                styles: [".field-address{width:100%}.field-address::ng-deep .mat-form-field-underline{display:block!important}"]
            }] }
];
/** @nocollapse */
FieldAddressComponent.ctorParameters = () => [
    { type: FocusMonitor },
    { type: ElementRef },
    { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
];
FieldAddressComponent.propDecorators = {
    form: [{ type: Input }],
    appearance: [{ type: Input }],
    color: [{ type: Input }],
    value: [{ type: Input }],
    required: [{ type: Input }],
    disabled: [{ type: Input }],
    placeholder: [{ type: Input }]
};
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.nextId;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype._required;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype._disabled;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype._placeholder;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.stateChanges;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.focused;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.errorState;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.controlType;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.id;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.describedBy;
    /**
     * Form control of the field.
     * @type {?}
     */
    FieldAddressComponent.prototype.form;
    /**
     * Appearance style of the form.
     * @type {?}
     */
    FieldAddressComponent.prototype.appearance;
    /**
     * Angular Material color of the form field underline and floating label.
     * @type {?}
     */
    FieldAddressComponent.prototype.color;
    /**
     * Whether the field has been initialized or not.
     * \@internal Do not use!
     * @type {?}
     */
    FieldAddressComponent.prototype.initialized;
    /**
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype.fm;
    /**
     * @type {?}
     * @private
     */
    FieldAddressComponent.prototype.elRef;
    /** @type {?} */
    FieldAddressComponent.prototype.ngControl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtYWRkcmVzcy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudC9mb3JtL2ZpZWxkLWFkZHJlc3MvZmllbGQtYWRkcmVzcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBR1QsS0FBSyxFQUNMLFVBQVUsRUFDVixRQUFRLEVBQ1IsSUFBSSxFQUNMLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDL0UsT0FBTyxFQUFFLG1CQUFtQixFQUF3QyxNQUFNLG1CQUFtQixDQUFDO0FBRTlGLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFhL0IsTUFBTSxPQUFPLHFCQUFxQjs7Ozs7OztJQTJFaEMsWUFDVSxFQUFnQixFQUNoQixLQUFpQixFQUNFLFNBQW9CO1FBRnZDLE9BQUUsR0FBRixFQUFFLENBQWM7UUFDaEIsVUFBSyxHQUFMLEtBQUssQ0FBWTtRQUNFLGNBQVMsR0FBVCxTQUFTLENBQVc7Ozs7UUFyRXpDLGNBQVMsR0FBRyxLQUFLLENBQUM7Ozs7UUFLbEIsY0FBUyxHQUFHLEtBQUssQ0FBQzs7OztRQVUxQixpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7Ozs7UUFLbkMsWUFBTyxHQUFHLEtBQUssQ0FBQzs7OztRQUtoQixlQUFVLEdBQUcsS0FBSyxDQUFDOzs7O1FBS25CLGdCQUFXLEdBQUcsb0JBQW9CLENBQUM7Ozs7UUFLbkMsT0FBRSxHQUFHLHNCQUFzQixxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDOzs7O1FBSzVELGdCQUFXLEdBQUcsRUFBRSxDQUFDOzs7O1FBS1IsU0FBSSxHQUFHLElBQUksU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDOzs7O1FBS3pCLGVBQVUsR0FBMkIsVUFBVSxDQUFDOzs7O1FBS2hELFVBQUssR0FBaUIsU0FBUyxDQUFDOzs7OztRQU16QyxnQkFBVyxHQUFHLEtBQUssQ0FBQztRQVVsQixFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDekMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUtELFFBQVE7O2NBQ0EsVUFBVSxHQUFHLEVBQUU7UUFDckIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3RDO1FBQ0QsS0FBSyxNQUFNLEtBQUssSUFBSSxDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsU0FBUyxDQUFDLEVBQUU7WUFDbkYsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO1NBQ2hFO1FBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFLRCxJQUNJLEtBQUs7UUFDUCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUtELElBQUksS0FBSyxDQUFDLENBQXFCO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFLRCxJQUNJLFFBQVE7UUFDVixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQzs7Ozs7O0lBS0QsSUFBSSxRQUFRLENBQUMsS0FBYztRQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFLRCxJQUNJLFFBQVE7UUFDVixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDeEIsQ0FBQzs7Ozs7O0lBS0QsSUFBSSxRQUFRLENBQUMsS0FBYztRQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFLRCxJQUNJLFdBQVc7UUFDYixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDM0IsQ0FBQzs7Ozs7O0lBS0QsSUFBSSxXQUFXLENBQUMsS0FBYTtRQUMzQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBS0QsSUFBSSxLQUFLO2NBQ0QsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSztRQUM3RSxPQUFPLENBQUMsUUFBUSxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQzNFLENBQUM7Ozs7O0lBS0QsSUFBSSxnQkFBZ0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNyQyxDQUFDOzs7Ozs7SUFLRCxpQkFBaUIsQ0FBQyxHQUFhO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7Ozs7SUFLRCxnQkFBZ0IsQ0FBQyxLQUFpQjtRQUNoQyxJQUFJLENBQUMsbUJBQUEsS0FBSyxDQUFDLE1BQU0sRUFBVyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxLQUFLLE9BQU8sRUFBRTtZQUMvRCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDekQ7SUFDSCxDQUFDOzs7OztJQUtELFdBQVc7UUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7OztBQXJNTSw0QkFBTSxHQUFHLENBQUMsQ0FBQzs7WUFmbkIsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLG90RUFBNkM7Z0JBRTdDLFNBQVMsRUFBRTtvQkFDVDt3QkFDRSxPQUFPLEVBQUUsbUJBQW1CO3dCQUM1QixXQUFXLEVBQUUscUJBQXFCO3FCQUNuQztpQkFDRjs7YUFDRjs7OztZQWpCUSxZQUFZO1lBSm5CLFVBQVU7WUFNcUIsU0FBUyx1QkE4RnJDLFFBQVEsWUFBSSxJQUFJOzs7bUJBeEJsQixLQUFLO3lCQUtMLEtBQUs7b0JBS0wsS0FBSztvQkF1Q0wsS0FBSzt1QkFnQkwsS0FBSzt1QkFnQkwsS0FBSzswQkFnQkwsS0FBSzs7Ozs7OztJQW5KTiw2QkFBa0I7Ozs7OztJQUtsQiwwQ0FBMEI7Ozs7OztJQUsxQiwwQ0FBMEI7Ozs7OztJQUsxQiw2Q0FBNkI7Ozs7O0lBSzdCLDZDQUFtQzs7Ozs7SUFLbkMsd0NBQWdCOzs7OztJQUtoQiwyQ0FBbUI7Ozs7O0lBS25CLDRDQUFtQzs7Ozs7SUFLbkMsbUNBQTREOzs7OztJQUs1RCw0Q0FBaUI7Ozs7O0lBS2pCLHFDQUFrQzs7Ozs7SUFLbEMsMkNBQXlEOzs7OztJQUt6RCxzQ0FBeUM7Ozs7OztJQU16Qyw0Q0FBb0I7Ozs7O0lBTWxCLG1DQUF3Qjs7Ozs7SUFDeEIsc0NBQXlCOztJQUN6QiwwQ0FBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkluaXQsXHJcbiAgT25EZXN0cm95LFxyXG4gIElucHV0LFxyXG4gIEVsZW1lbnRSZWYsXHJcbiAgT3B0aW9uYWwsXHJcbiAgU2VsZlxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb2N1c01vbml0b3IgfSBmcm9tICdAYW5ndWxhci9jZGsvYTExeSc7XHJcbmltcG9ydCB7IGNvZXJjZUJvb2xlYW5Qcm9wZXJ0eSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2VyY2lvbic7XHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wsIE5nQ29udHJvbCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTWF0Rm9ybUZpZWxkQ29udHJvbCwgTWF0Rm9ybUZpZWxkQXBwZWFyYW5jZSwgVGhlbWVQYWxldHRlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBLZXBzQWRkcmVzcyB9IGZyb20gJy4uLy4uLy4uL2ludGVyZmFjZSc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy1maWVsZC1hZGRyZXNzJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZmllbGQtYWRkcmVzcy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZmllbGQtYWRkcmVzcy5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBNYXRGb3JtRmllbGRDb250cm9sLFxyXG4gICAgICB1c2VFeGlzdGluZzogRmllbGRBZGRyZXNzQ29tcG9uZW50XHJcbiAgICB9XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmllbGRBZGRyZXNzQ29tcG9uZW50IGltcGxlbWVudHMgTWF0Rm9ybUZpZWxkQ29udHJvbDxLZXBzQWRkcmVzcz4sIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzdGF0aWMgbmV4dElkID0gMDtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfcmVxdWlyZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfZGlzYWJsZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfcGxhY2Vob2xkZXI6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc3RhdGVDaGFuZ2VzID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZm9jdXNlZCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBlcnJvclN0YXRlID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnRyb2xUeXBlID0gJ2tlcHMtZmllbGQtYWRkcmVzcyc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGlkID0gYGtlcHMtZmllbGQtYWRkcmVzcy0ke0ZpZWxkQWRkcmVzc0NvbXBvbmVudC5uZXh0SWQrK31gO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBkZXNjcmliZWRCeSA9ICcnO1xyXG5cclxuICAvKipcclxuICAgKiBGb3JtIGNvbnRyb2wgb2YgdGhlIGZpZWxkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGZvcm0gPSBuZXcgRm9ybUdyb3VwKHt9KTtcclxuXHJcbiAgLyoqXHJcbiAgICogQXBwZWFyYW5jZSBzdHlsZSBvZiB0aGUgZm9ybS5cclxuICAgKi9cclxuICBASW5wdXQoKSBhcHBlYXJhbmNlOiBNYXRGb3JtRmllbGRBcHBlYXJhbmNlID0gJ3N0YW5kYXJkJztcclxuXHJcbiAgLyoqXHJcbiAgICogQW5ndWxhciBNYXRlcmlhbCBjb2xvciBvZiB0aGUgZm9ybSBmaWVsZCB1bmRlcmxpbmUgYW5kIGZsb2F0aW5nIGxhYmVsLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGNvbG9yOiBUaGVtZVBhbGV0dGUgPSAncHJpbWFyeSc7XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGZpZWxkIGhhcyBiZWVuIGluaXRpYWxpemVkIG9yIG5vdC5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBpbml0aWFsaXplZCA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm06IEZvY3VzTW9uaXRvcixcclxuICAgIHByaXZhdGUgZWxSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICBAT3B0aW9uYWwoKSBAU2VsZigpIHB1YmxpYyBuZ0NvbnRyb2w6IE5nQ29udHJvbFxyXG4gICkge1xyXG4gICAgZm0ubW9uaXRvcihlbFJlZiwgdHJ1ZSkuc3Vic2NyaWJlKG9yaWdpbiA9PiB7XHJcbiAgICAgIHRoaXMuZm9jdXNlZCA9ICEhb3JpZ2luO1xyXG4gICAgICB0aGlzLnN0YXRlQ2hhbmdlcy5uZXh0KCk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgY29uc3QgdmFsaWRhdG9ycyA9IFtdO1xyXG4gICAgaWYgKHRoaXMucmVxdWlyZWQpIHtcclxuICAgICAgdmFsaWRhdG9ycy5wdXNoKFZhbGlkYXRvcnMucmVxdWlyZWQpO1xyXG4gICAgfVxyXG4gICAgZm9yIChjb25zdCBmaWVsZCBvZiBbJ2FkZHJlc3MxJywgJ2FkZHJlc3MyJywgJ2NpdHknLCAncmVnaW9uJywgJ3Bvc3RhbCcsICdjb3VudHJ5J10pIHtcclxuICAgICAgdGhpcy5mb3JtLmFkZENvbnRyb2woZmllbGQsIG5ldyBGb3JtQ29udHJvbChudWxsLCB2YWxpZGF0b3JzKSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmluaXRpYWxpemVkID0gdHJ1ZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSB2YWx1ZSBvZiB0aGUgY29udHJvbC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCB2YWx1ZSgpOiBLZXBzQWRkcmVzcyB8IG51bGwge1xyXG4gICAgcmV0dXJuIHRoaXMuZm9ybS52YWx1ZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCB2YWx1ZSh2OiBLZXBzQWRkcmVzcyB8IG51bGwpIHtcclxuICAgIHRoaXMuZm9ybS5wYXRjaFZhbHVlKHYpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyByZXF1aXJlZC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCByZXF1aXJlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9yZXF1aXJlZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCByZXF1aXJlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fcmVxdWlyZWQgPSBjb2VyY2VCb29sZWFuUHJvcGVydHkodmFsdWUpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyBkaXNhYmxlZC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCBkaXNhYmxlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9kaXNhYmxlZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCBkaXNhYmxlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fZGlzYWJsZWQgPSBjb2VyY2VCb29sZWFuUHJvcGVydHkodmFsdWUpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIHBsYWNlaG9sZGVyIGZvciB0aGlzIGNvbnRyb2wuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgcGxhY2Vob2xkZXIoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLl9wbGFjZWhvbGRlcjtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCBwbGFjZWhvbGRlcih2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLl9wbGFjZWhvbGRlciA9IHZhbHVlO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyBlbXB0eS5cclxuICAgKi9cclxuICBnZXQgZW1wdHkoKSB7XHJcbiAgICBjb25zdCB7IGFkZHJlc3MxLCBhZGRyZXNzMiwgY2l0eSwgcmVnaW9uLCBwb3N0YWwsIGNvdW50cnkgfSA9IHRoaXMuZm9ybS52YWx1ZTtcclxuICAgIHJldHVybiAhYWRkcmVzczEgJiYgIWFkZHJlc3MyICYmICFjaXR5ICYmICFyZWdpb24gJiYgIXBvc3RhbCAmJiAhY291bnRyeTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIE1hdEZvcm1GaWVsZCBsYWJlbCBzaG91bGQgdHJ5IHRvIGZsb2F0LlxyXG4gICAqL1xyXG4gIGdldCBzaG91bGRMYWJlbEZsb2F0KCkge1xyXG4gICAgcmV0dXJuIHRoaXMuZm9jdXNlZCB8fCAhdGhpcy5lbXB0eTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldERlc2NyaWJlZEJ5SWRzKGlkczogc3RyaW5nW10pIHtcclxuICAgIHRoaXMuZGVzY3JpYmVkQnkgPSBpZHMuam9pbignICcpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgb25Db250YWluZXJDbGljayhldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgaWYgKChldmVudC50YXJnZXQgYXMgRWxlbWVudCkudGFnTmFtZS50b0xvd2VyQ2FzZSgpICE9PSAnaW5wdXQnKSB7XHJcbiAgICAgIHRoaXMuZWxSZWYubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCdpbnB1dCcpLmZvY3VzKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMuc3RhdGVDaGFuZ2VzLmNvbXBsZXRlKCk7XHJcbiAgICB0aGlzLmZtLnN0b3BNb25pdG9yaW5nKHRoaXMuZWxSZWYpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19