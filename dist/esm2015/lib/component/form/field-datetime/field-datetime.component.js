/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ElementRef, Optional, Self } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { Subject } from 'rxjs';
import moment from 'moment';
const ɵ0 = MAT_MOMENT_DATE_FORMATS;
export class FieldDatetimeComponent {
    /**
     * \@internal Do not use!
     * @param {?} fm
     * @param {?} elRef
     * @param {?} ngControl
     */
    constructor(fm, elRef, ngControl) {
        this.fm = fm;
        this.elRef = elRef;
        this.ngControl = ngControl;
        /**
         * \@internal Do not use!
         */
        this._required = false;
        /**
         * \@internal Do not use!
         */
        this._disabled = false;
        /**
         * \@internal Do not use!
         */
        this.stateChanges = new Subject();
        /**
         * \@internal Do not use!
         */
        this.focused = false;
        /**
         * \@internal Do not use!
         */
        this.errorState = false;
        /**
         * \@internal Do not use!
         */
        this.controlType = 'keps-field-datetime';
        /**
         * \@internal Do not use!
         */
        this.id = `keps-field-datetime-${FieldDatetimeComponent.nextId++}`;
        /**
         * \@internal Do not use!
         */
        this.describedBy = '';
        /**
         * Form control of the field.
         */
        this.form = new FormControl(null);
        /**
         * Form control for the date.
         * \@internal Do not use!
         */
        this.dateForm = new FormControl(null);
        /**
         * The current date for placeholder.
         * \@internal Do not use!
         */
        this.startDate = moment();
        fm.monitor(elRef, true).subscribe((/**
         * @param {?} origin
         * @return {?}
         */
        origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        }));
    }
    /**
     * The value of the control.
     * @return {?}
     */
    get value() {
        return this.form.value;
    }
    /**
     * \@internal Do not use!
     * @param {?} d
     * @return {?}
     */
    set value(d) {
        this.form.setValue(d);
        if (d) {
            this.dateForm.setValue(moment(d));
        }
        else {
            this.dateForm.setValue(null);
        }
        this.stateChanges.next();
    }
    /**
     * Whether the control is required.
     * @return {?}
     */
    get required() {
        return this._required;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set required(value) {
        this._required = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * Whether the control is disabled.
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = coerceBooleanProperty(value);
        this.stateChanges.next();
    }
    /**
     * The placeholder for this control.
     * @return {?}
     */
    get placeholder() {
        return this._placeholder;
    }
    /**
     * \@internal Do not use!
     * @param {?} value
     * @return {?}
     */
    set placeholder(value) {
        this._placeholder = value;
        this.stateChanges.next();
    }
    /**
     * Whether the control is empty.
     * @return {?}
     */
    get empty() {
        return this.form.value == null;
    }
    /**
     * Whether the MatFormField label should try to float.
     * @return {?}
     */
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }
    /**
     * \@internal Do not use!
     * @param {?} ids
     * @return {?}
     */
    setDescribedByIds(ids) {
        this.describedBy = ids.join(' ');
    }
    /**
     * \@internal Do not use!
     * @param {?} event
     * @return {?}
     */
    onContainerClick(event) {
        if (((/** @type {?} */ (event.target))).tagName.toLowerCase() !== 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    }
    /**
     * Sets the forms value after selecting a date on datepicker.
     * \@internal Do not use!
     * @param {?} event Changes to the date picker.
     * @return {?}
     */
    onDateChange(event) {
        if (event.value) {
            this.value = event.value.valueOf();
        }
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef);
    }
}
/**
 * \@internal Do not use!
 */
FieldDatetimeComponent.nextId = 0;
FieldDatetimeComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-field-datetime',
                template: "<div class=\"field-datetime\">\r\n  <input\r\n    matInput\r\n    [matDatepicker]=\"dp\"\r\n    [formControl]=\"dateForm\"\r\n    (dateInput)=\"onDateChange($event)\"\r\n    placeholder=\"Choose a date\"\r\n  >\r\n  <mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n  <mat-datepicker #dp [startAt]=\"startDate\"></mat-datepicker>\r\n</div>  \r\n",
                providers: [
                    {
                        provide: MatFormFieldControl,
                        useExisting: FieldDatetimeComponent
                    },
                    {
                        provide: DateAdapter,
                        useClass: MomentDateAdapter,
                        deps: [MAT_DATE_LOCALE]
                    },
                    {
                        provide: MAT_DATE_FORMATS,
                        useValue: ɵ0
                    }
                ],
                styles: [".field-datetime{width:100%;display:flex;height:13.5px}::ng-deep mat-datepicker-toggle>button{bottom:16px}"]
            }] }
];
/** @nocollapse */
FieldDatetimeComponent.ctorParameters = () => [
    { type: FocusMonitor },
    { type: ElementRef },
    { type: NgControl, decorators: [{ type: Optional }, { type: Self }] }
];
FieldDatetimeComponent.propDecorators = {
    form: [{ type: Input }],
    value: [{ type: Input }],
    required: [{ type: Input }],
    disabled: [{ type: Input }],
    placeholder: [{ type: Input }]
};
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.nextId;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype._required;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype._disabled;
    /**
     * \@internal Do not use!
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype._placeholder;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.stateChanges;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.focused;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.errorState;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.controlType;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.id;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.describedBy;
    /**
     * Form control of the field.
     * @type {?}
     */
    FieldDatetimeComponent.prototype.form;
    /**
     * Form control for the date.
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.dateForm;
    /**
     * The current date for placeholder.
     * \@internal Do not use!
     * @type {?}
     */
    FieldDatetimeComponent.prototype.startDate;
    /**
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype.fm;
    /**
     * @type {?}
     * @private
     */
    FieldDatetimeComponent.prototype.elRef;
    /** @type {?} */
    FieldDatetimeComponent.prototype.ngControl;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQtZGF0ZXRpbWUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvZm9ybS9maWVsZC1kYXRldGltZS9maWVsZC1kYXRldGltZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDTCxTQUFTLEVBRVQsS0FBSyxFQUNMLFVBQVUsRUFDVixRQUFRLEVBQ1IsSUFBSSxFQUNMLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUEyQixNQUFNLG1CQUFtQixDQUFDO0FBQ2pJLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlGLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxNQUFNLE1BQU0sUUFBUSxDQUFDO1dBa0JaLHVCQUF1QjtBQUl2QyxNQUFNLE9BQU8sc0JBQXNCOzs7Ozs7O0lBdUVqQyxZQUNVLEVBQWdCLEVBQ2hCLEtBQThCLEVBQ1gsU0FBb0I7UUFGdkMsT0FBRSxHQUFGLEVBQUUsQ0FBYztRQUNoQixVQUFLLEdBQUwsS0FBSyxDQUF5QjtRQUNYLGNBQVMsR0FBVCxTQUFTLENBQVc7Ozs7UUFqRXpDLGNBQVMsR0FBRyxLQUFLLENBQUM7Ozs7UUFLbEIsY0FBUyxHQUFHLEtBQUssQ0FBQzs7OztRQVUxQixpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7Ozs7UUFLbkMsWUFBTyxHQUFHLEtBQUssQ0FBQzs7OztRQUtoQixlQUFVLEdBQUcsS0FBSyxDQUFDOzs7O1FBS25CLGdCQUFXLEdBQUcscUJBQXFCLENBQUM7Ozs7UUFLcEMsT0FBRSxHQUFHLHVCQUF1QixzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDOzs7O1FBSzlELGdCQUFXLEdBQUcsRUFBRSxDQUFDOzs7O1FBS1IsU0FBSSxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDOzs7OztRQU10QyxhQUFRLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7Ozs7O1FBTWpDLGNBQVMsR0FBRyxNQUFNLEVBQUUsQ0FBQztRQVVuQixFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDekMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUtELElBQ0ksS0FBSztRQUNQLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDekIsQ0FBQzs7Ozs7O0lBS0QsSUFBSSxLQUFLLENBQUMsQ0FBZ0I7UUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLEVBQUU7WUFDTCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNuQzthQUFNO1lBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDOUI7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBS0QsSUFDSSxRQUFRO1FBQ1YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7Ozs7OztJQUtELElBQUksUUFBUSxDQUFDLEtBQWM7UUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBS0QsSUFDSSxRQUFRO1FBQ1YsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3hCLENBQUM7Ozs7OztJQUtELElBQUksUUFBUSxDQUFDLEtBQWM7UUFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBS0QsSUFDSSxXQUFXO1FBQ2IsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzNCLENBQUM7Ozs7OztJQUtELElBQUksV0FBVyxDQUFDLEtBQWE7UUFDM0IsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7OztJQUtELElBQUksS0FBSztRQUNQLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBS0QsSUFBSSxnQkFBZ0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNyQyxDQUFDOzs7Ozs7SUFLRCxpQkFBaUIsQ0FBQyxHQUFhO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7Ozs7SUFLRCxnQkFBZ0IsQ0FBQyxLQUFpQjtRQUNoQyxJQUFJLENBQUMsbUJBQUEsS0FBSyxDQUFDLE1BQU0sRUFBVyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxLQUFLLE9BQU8sRUFBRTtZQUMvRCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDekQ7SUFDSCxDQUFDOzs7Ozs7O0lBT0QsWUFBWSxDQUFDLEtBQTZDO1FBQ3hELElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtZQUNmLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUNwQztJQUNILENBQUM7Ozs7O0lBS0QsV0FBVztRQUNULElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLEVBQUUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JDLENBQUM7Ozs7O0FBbE1NLDZCQUFNLEdBQUcsQ0FBQyxDQUFDOztZQXhCbkIsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxxQkFBcUI7Z0JBQy9CLDZYQUE4QztnQkFFOUMsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxtQkFBbUI7d0JBQzVCLFdBQVcsRUFBRSxzQkFBc0I7cUJBQ3BDO29CQUNEO3dCQUNFLE9BQU8sRUFBRSxXQUFXO3dCQUNwQixRQUFRLEVBQUUsaUJBQWlCO3dCQUMzQixJQUFJLEVBQUUsQ0FBQyxlQUFlLENBQUM7cUJBQ3hCO29CQUNEO3dCQUNFLE9BQU8sRUFBRSxnQkFBZ0I7d0JBQ3pCLFFBQVEsSUFBeUI7cUJBQ2xDO2lCQUNGOzthQUNGOzs7O1lBM0JRLFlBQVk7WUFKbkIsVUFBVTtZQU1VLFNBQVMsdUJBb0cxQixRQUFRLFlBQUksSUFBSTs7O21CQXBCbEIsS0FBSztvQkErQkwsS0FBSzt1QkFxQkwsS0FBSzt1QkFnQkwsS0FBSzswQkFnQkwsS0FBSzs7Ozs7OztJQXRJTiw4QkFBa0I7Ozs7OztJQUtsQiwyQ0FBMEI7Ozs7OztJQUsxQiwyQ0FBMEI7Ozs7OztJQUsxQiw4Q0FBNkI7Ozs7O0lBSzdCLDhDQUFtQzs7Ozs7SUFLbkMseUNBQWdCOzs7OztJQUtoQiw0Q0FBbUI7Ozs7O0lBS25CLDZDQUFvQzs7Ozs7SUFLcEMsb0NBQThEOzs7OztJQUs5RCw2Q0FBaUI7Ozs7O0lBS2pCLHNDQUFzQzs7Ozs7O0lBTXRDLDBDQUFpQzs7Ozs7O0lBTWpDLDJDQUFxQjs7Ozs7SUFNbkIsb0NBQXdCOzs7OztJQUN4Qix1Q0FBc0M7O0lBQ3RDLDJDQUErQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIE9uRGVzdHJveSxcclxuICBJbnB1dCxcclxuICBFbGVtZW50UmVmLFxyXG4gIE9wdGlvbmFsLFxyXG4gIFNlbGZcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9jdXNNb25pdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2ExMXknO1xyXG5pbXBvcnQgeyBjb2VyY2VCb29sZWFuUHJvcGVydHkgfSBmcm9tICdAYW5ndWxhci9jZGsvY29lcmNpb24nO1xyXG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgTmdDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBNYXRGb3JtRmllbGRDb250cm9sLCBEYXRlQWRhcHRlciwgTUFUX0RBVEVfRk9STUFUUywgTUFUX0RBVEVfTE9DQUxFLCBNYXREYXRlcGlja2VySW5wdXRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgTUFUX01PTUVOVF9EQVRFX0ZPUk1BVFMsIE1vbWVudERhdGVBZGFwdGVyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwtbW9tZW50LWFkYXB0ZXInO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy1maWVsZC1kYXRldGltZScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZpZWxkLWRhdGV0aW1lLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9maWVsZC1kYXRldGltZS5jb21wb25lbnQuc2NzcyddLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBNYXRGb3JtRmllbGRDb250cm9sLFxyXG4gICAgICB1c2VFeGlzdGluZzogRmllbGREYXRldGltZUNvbXBvbmVudFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgcHJvdmlkZTogRGF0ZUFkYXB0ZXIsXHJcbiAgICAgIHVzZUNsYXNzOiBNb21lbnREYXRlQWRhcHRlcixcclxuICAgICAgZGVwczogW01BVF9EQVRFX0xPQ0FMRV1cclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IE1BVF9EQVRFX0ZPUk1BVFMsXHJcbiAgICAgIHVzZVZhbHVlOiBNQVRfTU9NRU5UX0RBVEVfRk9STUFUU1xyXG4gICAgfVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZpZWxkRGF0ZXRpbWVDb21wb25lbnQgaW1wbGVtZW50cyBNYXRGb3JtRmllbGRDb250cm9sPG51bWJlcj4sIE9uRGVzdHJveSB7XHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgc3RhdGljIG5leHRJZCA9IDA7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX3JlcXVpcmVkID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX2Rpc2FibGVkID0gZmFsc2U7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX3BsYWNlaG9sZGVyOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHN0YXRlQ2hhbmdlcyA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGZvY3VzZWQgPSBmYWxzZTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZXJyb3JTdGF0ZSA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb250cm9sVHlwZSA9ICdrZXBzLWZpZWxkLWRhdGV0aW1lJztcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgaWQgPSBga2Vwcy1maWVsZC1kYXRldGltZS0ke0ZpZWxkRGF0ZXRpbWVDb21wb25lbnQubmV4dElkKyt9YDtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgZGVzY3JpYmVkQnkgPSAnJztcclxuXHJcbiAgLyoqXHJcbiAgICogRm9ybSBjb250cm9sIG9mIHRoZSBmaWVsZC5cclxuICAgKi9cclxuICBASW5wdXQoKSBmb3JtID0gbmV3IEZvcm1Db250cm9sKG51bGwpO1xyXG5cclxuICAvKipcclxuICAgKiBGb3JtIGNvbnRyb2wgZm9yIHRoZSBkYXRlLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGRhdGVGb3JtID0gbmV3IEZvcm1Db250cm9sKG51bGwpO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgY3VycmVudCBkYXRlIGZvciBwbGFjZWhvbGRlci5cclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzdGFydERhdGUgPSBtb21lbnQoKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsIERvIG5vdCB1c2UhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZtOiBGb2N1c01vbml0b3IsXHJcbiAgICBwcml2YXRlIGVsUmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PixcclxuICAgIEBPcHRpb25hbCgpIEBTZWxmKCkgcHVibGljIG5nQ29udHJvbDogTmdDb250cm9sXHJcbiAgKSB7XHJcbiAgICBmbS5tb25pdG9yKGVsUmVmLCB0cnVlKS5zdWJzY3JpYmUob3JpZ2luID0+IHtcclxuICAgICAgdGhpcy5mb2N1c2VkID0gISFvcmlnaW47XHJcbiAgICAgIHRoaXMuc3RhdGVDaGFuZ2VzLm5leHQoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIHZhbHVlIG9mIHRoZSBjb250cm9sLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgZ2V0IHZhbHVlKCk6IG51bWJlciB8IG51bGwge1xyXG4gICAgcmV0dXJuIHRoaXMuZm9ybS52YWx1ZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCB2YWx1ZShkOiBudW1iZXIgfCBudWxsKSB7XHJcbiAgICB0aGlzLmZvcm0uc2V0VmFsdWUoZCk7XHJcbiAgICBpZiAoZCkge1xyXG4gICAgICB0aGlzLmRhdGVGb3JtLnNldFZhbHVlKG1vbWVudChkKSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmRhdGVGb3JtLnNldFZhbHVlKG51bGwpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyByZXF1aXJlZC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCByZXF1aXJlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9yZXF1aXJlZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCByZXF1aXJlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fcmVxdWlyZWQgPSBjb2VyY2VCb29sZWFuUHJvcGVydHkodmFsdWUpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyBkaXNhYmxlZC5cclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIGdldCBkaXNhYmxlZCgpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9kaXNhYmxlZDtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCBkaXNhYmxlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5fZGlzYWJsZWQgPSBjb2VyY2VCb29sZWFuUHJvcGVydHkodmFsdWUpO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIHBsYWNlaG9sZGVyIGZvciB0aGlzIGNvbnRyb2wuXHJcbiAgICovXHJcbiAgQElucHV0KClcclxuICBnZXQgcGxhY2Vob2xkZXIoKTogc3RyaW5nIHtcclxuICAgIHJldHVybiB0aGlzLl9wbGFjZWhvbGRlcjtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIHNldCBwbGFjZWhvbGRlcih2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLl9wbGFjZWhvbGRlciA9IHZhbHVlO1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgY29udHJvbCBpcyBlbXB0eS5cclxuICAgKi9cclxuICBnZXQgZW1wdHkoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5mb3JtLnZhbHVlID09IG51bGw7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBNYXRGb3JtRmllbGQgbGFiZWwgc2hvdWxkIHRyeSB0byBmbG9hdC5cclxuICAgKi9cclxuICBnZXQgc2hvdWxkTGFiZWxGbG9hdCgpIHtcclxuICAgIHJldHVybiB0aGlzLmZvY3VzZWQgfHwgIXRoaXMuZW1wdHk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzZXREZXNjcmliZWRCeUlkcyhpZHM6IHN0cmluZ1tdKSB7XHJcbiAgICB0aGlzLmRlc2NyaWJlZEJ5ID0gaWRzLmpvaW4oJyAnKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG9uQ29udGFpbmVyQ2xpY2soZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgIGlmICgoZXZlbnQudGFyZ2V0IGFzIEVsZW1lbnQpLnRhZ05hbWUudG9Mb3dlckNhc2UoKSAhPT0gJ2lucHV0Jykge1xyXG4gICAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignaW5wdXQnKS5mb2N1cygpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2V0cyB0aGUgZm9ybXMgdmFsdWUgYWZ0ZXIgc2VsZWN0aW5nIGEgZGF0ZSBvbiBkYXRlcGlja2VyLlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqIEBwYXJhbSBldmVudCBDaGFuZ2VzIHRvIHRoZSBkYXRlIHBpY2tlci5cclxuICAgKi9cclxuICBvbkRhdGVDaGFuZ2UoZXZlbnQ6IE1hdERhdGVwaWNrZXJJbnB1dEV2ZW50PG1vbWVudC5Nb21lbnQ+KSB7XHJcbiAgICBpZiAoZXZlbnQudmFsdWUpIHtcclxuICAgICAgdGhpcy52YWx1ZSA9IGV2ZW50LnZhbHVlLnZhbHVlT2YoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy5zdGF0ZUNoYW5nZXMuY29tcGxldGUoKTtcclxuICAgIHRoaXMuZm0uc3RvcE1vbml0b3JpbmcodGhpcy5lbFJlZik7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=