/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Represents a button for the dialog.
 * @record
 */
export function DialogButton() { }
if (false) {
    /** @type {?} */
    DialogButton.prototype.label;
    /**
     * Value to be returned when the user clicks this button.
     * @type {?}
     */
    DialogButton.prototype.result;
    /**
     * Angular Material color for the dialog button.
     * @type {?|undefined}
     */
    DialogButton.prototype.color;
}
/**
 * Represents data to be shown by the dialog.
 * @record
 */
export function DialogData() { }
if (false) {
    /** @type {?} */
    DialogData.prototype.title;
    /** @type {?} */
    DialogData.prototype.text;
    /**
     * Optional buttons to be shown by the dialog.
     *
     * If no button is passed, the 'Close' button will be shown by default.
     *
     * See {\@link DialogButton}.
     * @type {?|undefined}
     */
    DialogData.prototype.buttons;
    /**
     * Whether the dialog should show 'Close' button or not.
     * @type {?|undefined}
     */
    DialogData.prototype.showClose;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLWludGVyZmFjZXMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZzdrZXBzLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudC9kaWFsb2cvZGlhbG9nLWludGVyZmFjZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFLQSxrQ0FZQzs7O0lBWEMsNkJBQWM7Ozs7O0lBS2QsOEJBQVk7Ozs7O0lBS1osNkJBQXFCOzs7Ozs7QUFNdkIsZ0NBaUJDOzs7SUFoQkMsMkJBQWM7O0lBQ2QsMEJBQWE7Ozs7Ozs7OztJQVNiLDZCQUF5Qjs7Ozs7SUFLekIsK0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVGhlbWVQYWxldHRlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuLyoqXHJcbiAqIFJlcHJlc2VudHMgYSBidXR0b24gZm9yIHRoZSBkaWFsb2cuXHJcbiAqL1xyXG5leHBvcnQgaW50ZXJmYWNlIERpYWxvZ0J1dHRvbiB7XHJcbiAgbGFiZWw6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogVmFsdWUgdG8gYmUgcmV0dXJuZWQgd2hlbiB0aGUgdXNlciBjbGlja3MgdGhpcyBidXR0b24uXHJcbiAgICovXHJcbiAgcmVzdWx0OiBhbnk7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFuZ3VsYXIgTWF0ZXJpYWwgY29sb3IgZm9yIHRoZSBkaWFsb2cgYnV0dG9uLlxyXG4gICAqL1xyXG4gIGNvbG9yPzogVGhlbWVQYWxldHRlO1xyXG59XHJcblxyXG4vKipcclxuICogUmVwcmVzZW50cyBkYXRhIHRvIGJlIHNob3duIGJ5IHRoZSBkaWFsb2cuXHJcbiAqL1xyXG5leHBvcnQgaW50ZXJmYWNlIERpYWxvZ0RhdGEge1xyXG4gIHRpdGxlOiBzdHJpbmc7XHJcbiAgdGV4dDogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBPcHRpb25hbCBidXR0b25zIHRvIGJlIHNob3duIGJ5IHRoZSBkaWFsb2cuXHJcbiAgICpcclxuICAgKiBJZiBubyBidXR0b24gaXMgcGFzc2VkLCB0aGUgJ0Nsb3NlJyBidXR0b24gd2lsbCBiZSBzaG93biBieSBkZWZhdWx0LlxyXG4gICAqXHJcbiAgICogU2VlIHtAbGluayBEaWFsb2dCdXR0b259LlxyXG4gICAqL1xyXG4gIGJ1dHRvbnM/OiBEaWFsb2dCdXR0b25bXTtcclxuXHJcbiAgLyoqXHJcbiAgICogV2hldGhlciB0aGUgZGlhbG9nIHNob3VsZCBzaG93ICdDbG9zZScgYnV0dG9uIG9yIG5vdC5cclxuICAgKi9cclxuICBzaG93Q2xvc2U/OiBib29sZWFuO1xyXG59XHJcbiJdfQ==