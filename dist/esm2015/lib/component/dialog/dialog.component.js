/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
/**
 * A component that wraps around an Angular Material dialog.
 *
 * Use the {\@link PopupService} to create this dialog.
 */
export class DialogComponent {
    /**
     * \@internal Do not use!
     * @param {?} dialogRef
     * @param {?} data
     */
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.buttons = this.data.buttons || [];
        if (this.data.showClose !== undefined) {
            this.showClose = this.data.showClose;
        }
        else {
            this.showClose = !(this.buttons.length > 0);
        }
    }
}
DialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-dialog',
                template: "<section class=\"mat-typography\">\r\n  <h1 mat-dialog-title>{{ data.title }}</h1>\r\n  <div mat-dialog-content>\r\n    <p>{{ data.text }}</p>\r\n  </div>\r\n  <div mat-dialog-actions class=\"dialog-actions\">\r\n    <button\r\n      *ngFor=\"let button of buttons\"\r\n      [mat-dialog-close]=\"button.result\"\r\n      mat-button [color]=\"button.color\"\r\n    >\r\n     {{ button.label }}\r\n    </button>\r\n    <button\r\n      *ngIf=\"showClose\"\r\n      [mat-dialog-close]=\"undefined\"\r\n      mat-button\r\n    >\r\n      Close\r\n    </button>\r\n  </div>\r\n</section>\r\n",
                styles: [""]
            }] }
];
/** @nocollapse */
DialogComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
if (false) {
    /**
     * \@internal Do not use!
     * @type {?}
     */
    DialogComponent.prototype.buttons;
    /**
     * \@internal Do not use!
     * @type {?}
     */
    DialogComponent.prototype.showClose;
    /** @type {?} */
    DialogComponent.prototype.dialogRef;
    /** @type {?} */
    DialogComponent.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlhbG9nLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nN2tlcHMvIiwic291cmNlcyI6WyJsaWIvY29tcG9uZW50L2RpYWxvZy9kaWFsb2cuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsRCxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLG1CQUFtQixDQUFDOzs7Ozs7QUFhbEUsTUFBTSxPQUFPLGVBQWU7Ozs7OztJQWMxQixZQUNTLFNBQXdDLEVBQ2YsSUFBZ0I7UUFEekMsY0FBUyxHQUFULFNBQVMsQ0FBK0I7UUFDZixTQUFJLEdBQUosSUFBSSxDQUFZO1FBRWhELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDO1FBQ3ZDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFO1lBQ3JDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDdEM7YUFBTTtZQUNMLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQzdDO0lBQ0gsQ0FBQzs7O1lBN0JGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsYUFBYTtnQkFDdkIsdWxCQUFzQzs7YUFFdkM7Ozs7WUFaUSxZQUFZOzRDQTZCaEIsTUFBTSxTQUFDLGVBQWU7Ozs7Ozs7SUFaekIsa0NBQXdCOzs7OztJQUt4QixvQ0FBbUI7O0lBTWpCLG9DQUErQzs7SUFDL0MsK0JBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IERpYWxvZ0RhdGEsIERpYWxvZ0J1dHRvbiB9IGZyb20gJy4vZGlhbG9nLWludGVyZmFjZXMnO1xyXG5cclxuLyoqXHJcbiAqIEEgY29tcG9uZW50IHRoYXQgd3JhcHMgYXJvdW5kIGFuIEFuZ3VsYXIgTWF0ZXJpYWwgZGlhbG9nLlxyXG4gKlxyXG4gKiBVc2UgdGhlIHtAbGluayBQb3B1cFNlcnZpY2V9IHRvIGNyZWF0ZSB0aGlzIGRpYWxvZy5cclxuICovXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy1kaWFsb2cnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9kaWFsb2cuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2RpYWxvZy5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEaWFsb2dDb21wb25lbnQge1xyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGJ1dHRvbnM6IERpYWxvZ0J1dHRvbltdO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBzaG93Q2xvc2U6IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIEBpbnRlcm5hbCBEbyBub3QgdXNlIVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPERpYWxvZ0NvbXBvbmVudD4sXHJcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IERpYWxvZ0RhdGFcclxuICApIHtcclxuICAgIHRoaXMuYnV0dG9ucyA9IHRoaXMuZGF0YS5idXR0b25zIHx8IFtdO1xyXG4gICAgaWYgKHRoaXMuZGF0YS5zaG93Q2xvc2UgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICB0aGlzLnNob3dDbG9zZSA9IHRoaXMuZGF0YS5zaG93Q2xvc2U7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNob3dDbG9zZSA9ICEodGhpcy5idXR0b25zLmxlbmd0aCA+IDApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbn1cclxuIl19