/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
/**
 * A component that wraps around the default HTML file select button.
 */
export class FileSelectComponent {
    /**
     * \@internal Do not use!
     */
    constructor() {
        /**
         * File to be selected.
         */
        this.selectedFile = null;
        /**
         * An event emitter that emits the selected file.
         */
        this.selectedFileChange = new EventEmitter();
        /**
         * Label for the file select button.
         */
        this.label = 'Choose File';
        /**
         * Whether the component should display the file name.
         */
        this.showFilename = true;
        /**
         * The type(s) of file that the file select dialog can accept.
         *
         * See: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#Unique_file_type_specifiers
         *
         * If the value is "image", "video", or "audio", it will accept every possible file types
         * of that media type.
         */
        this.type = '';
        /**
         * Angular Material color for the file select button.
         */
        this.color = 'primary';
    }
    /**
     * \@internal Do not use!
     * @return {?}
     */
    ngOnInit() {
        if (this.type === 'image' ||
            this.type === 'video' ||
            this.type === 'audio') {
            this.type += '/*';
        }
    }
    /**
     * Open the file select dialog manually.
     * @return {?}
     */
    openFileSelect() {
        this.fileSelector.nativeElement.click();
    }
    /**
     * Select a file.
     * @param {?} files Files to be passed. Only the first one will be selected.
     * @return {?}
     */
    selectFile(files) {
        if (files && files.length > 0) {
            this.selectedFile = files[0];
            this.selectedFileChange.emit(this.selectedFile);
        }
    }
}
FileSelectComponent.decorators = [
    { type: Component, args: [{
                selector: 'keps-file-select',
                template: "<button\r\n  (click)=\"openFileSelect()\"\r\n  mat-raised-button [color]=\"color\"\r\n>\r\n  {{ label }}\r\n</button>\r\n<span *ngIf=\"showFilename\" class=\"file-name\">\r\n  {{ selectedFile ? selectedFile.name : 'No file chosen.' }}\r\n</span>\r\n<input\r\n  #fileSelector\r\n  (change)=\"selectFile($event.target.files)\"\r\n  type=\"file\"\r\n  [attr.accept]=\"type\"\r\n  hidden\r\n/>\r\n",
                styles: [""]
            }] }
];
/** @nocollapse */
FileSelectComponent.ctorParameters = () => [];
FileSelectComponent.propDecorators = {
    selectedFile: [{ type: Input }],
    selectedFileChange: [{ type: Output }],
    label: [{ type: Input }],
    showFilename: [{ type: Input }],
    type: [{ type: Input }],
    color: [{ type: Input }],
    fileSelector: [{ type: ViewChild, args: ['fileSelector', { static: true },] }]
};
if (false) {
    /**
     * File to be selected.
     * @type {?}
     */
    FileSelectComponent.prototype.selectedFile;
    /**
     * An event emitter that emits the selected file.
     * @type {?}
     */
    FileSelectComponent.prototype.selectedFileChange;
    /**
     * Label for the file select button.
     * @type {?}
     */
    FileSelectComponent.prototype.label;
    /**
     * Whether the component should display the file name.
     * @type {?}
     */
    FileSelectComponent.prototype.showFilename;
    /**
     * The type(s) of file that the file select dialog can accept.
     *
     * See: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#Unique_file_type_specifiers
     *
     * If the value is "image", "video", or "audio", it will accept every possible file types
     * of that media type.
     * @type {?}
     */
    FileSelectComponent.prototype.type;
    /**
     * Angular Material color for the file select button.
     * @type {?}
     */
    FileSelectComponent.prototype.color;
    /**
     * The `input` element itself.
     * @type {?}
     * @private
     */
    FileSelectComponent.prototype.fileSelector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS1zZWxlY3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmc3a2Vwcy8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnQvZmlsZS1zZWxlY3QvZmlsZS1zZWxlY3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULEtBQUssRUFDTCxNQUFNLEVBQ04sWUFBWSxFQUNaLFNBQVMsRUFDVCxVQUFVLEVBQ1gsTUFBTSxlQUFlLENBQUM7Ozs7QUFXdkIsTUFBTSxPQUFPLG1CQUFtQjs7OztJQTRDOUI7Ozs7UUF4Q1MsaUJBQVksR0FBUyxJQUFJLENBQUM7Ozs7UUFLekIsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQVEsQ0FBQzs7OztRQUsvQyxVQUFLLEdBQUcsYUFBYSxDQUFDOzs7O1FBS3RCLGlCQUFZLEdBQUcsSUFBSSxDQUFDOzs7Ozs7Ozs7UUFVcEIsU0FBSSxHQUFHLEVBQUUsQ0FBQzs7OztRQUtWLFVBQUssR0FBaUIsU0FBUyxDQUFDO0lBVXpCLENBQUM7Ozs7O0lBS2pCLFFBQVE7UUFDTixJQUNFLElBQUksQ0FBQyxJQUFJLEtBQUssT0FBTztZQUNyQixJQUFJLENBQUMsSUFBSSxLQUFLLE9BQU87WUFDckIsSUFBSSxDQUFDLElBQUksS0FBSyxPQUFPLEVBQ3JCO1lBQ0EsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUM7U0FDbkI7SUFDSCxDQUFDOzs7OztJQUtELGNBQWM7UUFDWixJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUMxQyxDQUFDOzs7Ozs7SUFNRCxVQUFVLENBQUMsS0FBYTtRQUN0QixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUNqRDtJQUNILENBQUM7OztZQWhGRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIscVpBQTJDOzthQUU1Qzs7Ozs7MkJBS0UsS0FBSztpQ0FLTCxNQUFNO29CQUtOLEtBQUs7MkJBS0wsS0FBSzttQkFVTCxLQUFLO29CQUtMLEtBQUs7MkJBS0wsU0FBUyxTQUFDLGNBQWMsRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7Ozs7Ozs7SUFuQzNDLDJDQUFtQzs7Ozs7SUFLbkMsaURBQXdEOzs7OztJQUt4RCxvQ0FBK0I7Ozs7O0lBSy9CLDJDQUE2Qjs7Ozs7Ozs7OztJQVU3QixtQ0FBbUI7Ozs7O0lBS25CLG9DQUF5Qzs7Ozs7O0lBS3pDLDJDQUE4RSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIE9uSW5pdCxcclxuICBJbnB1dCxcclxuICBPdXRwdXQsXHJcbiAgRXZlbnRFbWl0dGVyLFxyXG4gIFZpZXdDaGlsZCxcclxuICBFbGVtZW50UmVmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRoZW1lUGFsZXR0ZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcbi8qKlxyXG4gKiBBIGNvbXBvbmVudCB0aGF0IHdyYXBzIGFyb3VuZCB0aGUgZGVmYXVsdCBIVE1MIGZpbGUgc2VsZWN0IGJ1dHRvbi5cclxuICovXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAna2Vwcy1maWxlLXNlbGVjdCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZpbGUtc2VsZWN0LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9maWxlLXNlbGVjdC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGaWxlU2VsZWN0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAvKipcclxuICAgKiBGaWxlIHRvIGJlIHNlbGVjdGVkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHNlbGVjdGVkRmlsZTogRmlsZSA9IG51bGw7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFuIGV2ZW50IGVtaXR0ZXIgdGhhdCBlbWl0cyB0aGUgc2VsZWN0ZWQgZmlsZS5cclxuICAgKi9cclxuICBAT3V0cHV0KCkgc2VsZWN0ZWRGaWxlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxGaWxlPigpO1xyXG5cclxuICAvKipcclxuICAgKiBMYWJlbCBmb3IgdGhlIGZpbGUgc2VsZWN0IGJ1dHRvbi5cclxuICAgKi9cclxuICBASW5wdXQoKSBsYWJlbCA9ICdDaG9vc2UgRmlsZSc7XHJcblxyXG4gIC8qKlxyXG4gICAqIFdoZXRoZXIgdGhlIGNvbXBvbmVudCBzaG91bGQgZGlzcGxheSB0aGUgZmlsZSBuYW1lLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHNob3dGaWxlbmFtZSA9IHRydWU7XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSB0eXBlKHMpIG9mIGZpbGUgdGhhdCB0aGUgZmlsZSBzZWxlY3QgZGlhbG9nIGNhbiBhY2NlcHQuXHJcbiAgICpcclxuICAgKiBTZWU6IGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuLVVTL2RvY3MvV2ViL0hUTUwvRWxlbWVudC9pbnB1dC9maWxlI1VuaXF1ZV9maWxlX3R5cGVfc3BlY2lmaWVyc1xyXG4gICAqXHJcbiAgICogSWYgdGhlIHZhbHVlIGlzIFwiaW1hZ2VcIiwgXCJ2aWRlb1wiLCBvciBcImF1ZGlvXCIsIGl0IHdpbGwgYWNjZXB0IGV2ZXJ5IHBvc3NpYmxlIGZpbGUgdHlwZXNcclxuICAgKiBvZiB0aGF0IG1lZGlhIHR5cGUuXHJcbiAgICovXHJcbiAgQElucHV0KCkgdHlwZSA9ICcnO1xyXG5cclxuICAvKipcclxuICAgKiBBbmd1bGFyIE1hdGVyaWFsIGNvbG9yIGZvciB0aGUgZmlsZSBzZWxlY3QgYnV0dG9uLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGNvbG9yOiBUaGVtZVBhbGV0dGUgPSAncHJpbWFyeSc7XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBgaW5wdXRgIGVsZW1lbnQgaXRzZWxmLlxyXG4gICAqL1xyXG4gIEBWaWV3Q2hpbGQoJ2ZpbGVTZWxlY3RvcicsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgZmlsZVNlbGVjdG9yOiBFbGVtZW50UmVmO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWwgRG8gbm90IHVzZSFcclxuICAgKi9cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIGlmIChcclxuICAgICAgdGhpcy50eXBlID09PSAnaW1hZ2UnIHx8XHJcbiAgICAgIHRoaXMudHlwZSA9PT0gJ3ZpZGVvJyB8fFxyXG4gICAgICB0aGlzLnR5cGUgPT09ICdhdWRpbydcclxuICAgICkge1xyXG4gICAgICB0aGlzLnR5cGUgKz0gJy8qJztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIE9wZW4gdGhlIGZpbGUgc2VsZWN0IGRpYWxvZyBtYW51YWxseS5cclxuICAgKi9cclxuICBvcGVuRmlsZVNlbGVjdCgpIHtcclxuICAgIHRoaXMuZmlsZVNlbGVjdG9yLm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNlbGVjdCBhIGZpbGUuXHJcbiAgICogQHBhcmFtIGZpbGVzIEZpbGVzIHRvIGJlIHBhc3NlZC4gT25seSB0aGUgZmlyc3Qgb25lIHdpbGwgYmUgc2VsZWN0ZWQuXHJcbiAgICovXHJcbiAgc2VsZWN0RmlsZShmaWxlczogRmlsZVtdKSB7XHJcbiAgICBpZiAoZmlsZXMgJiYgZmlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRmlsZSA9IGZpbGVzWzBdO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRmlsZUNoYW5nZS5lbWl0KHRoaXMuc2VsZWN0ZWRGaWxlKTtcclxuICAgIH1cclxuICB9XHJcblxyXG59XHJcbiJdfQ==