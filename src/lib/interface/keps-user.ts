/**
 * Represents basic Keps user.
 *
 * A user on a project may have more fields.
 */
export interface KepsUser {
  _id:                  string;
  _createdAt:           number;
  _updatedAt:           number;
  email:                string;
  firstName:            string;
  lastName:             string;
  displayName:          string;
  roles:                string[];
  token:                string;
  tokenExpires:         number;
}
