/**
 * Represents basic Keps object.
 */
export interface KepsObject {
  _id:                   string;
  _createdAt?:           number;
  _updatedAt?:           number;
  [otherFields: string]: any;
};

/**
 * Represents a map where the keys are names of models and the values
 * are arrays of `KepsObject`s.
 */
export type KepsReference = Record<string, KepsObject[]>;
