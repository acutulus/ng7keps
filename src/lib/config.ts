export const webAlias = location.host;
export const apiPrefix = '/api/v1/';
export const apiRoute = new URL(apiPrefix, location.origin).href;
