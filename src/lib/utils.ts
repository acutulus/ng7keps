import { KepsModels, KepsSchema } from './interface';

/**
 * A helper function that checks for a schema's field type.
 * @param schema A Keps schema.
 * @param field A field name.
 * @param type A type name to be checked.
 */
export const isType = (schema: KepsSchema, field: string, type: string) => {
  const fieldType = schema[field].type;
  if (type === 'reference') {
    return fieldType[0] === ':';
  }
  if (type === 'array-reference') {
    return fieldType === 'array' && schema[field].subSchema[0] === ':';
  }
  if (type === 'array-string') {
    return fieldType === 'array' && schema[field].subSchema === 'string';
  }
  // This is to check if table should render the object as is with no additional processing.
  if (type === 'table-normal') {
    return !(
      isType(schema, field, 'reference') ||
      isType(schema, field, 'array-reference') ||
      isType(schema, field, 'array-string') ||
      fieldType === 'address' ||
      fieldType === 'datetime' ||
      fieldType === 'enum' ||
      fieldType === 'number'
    );
  }

  return fieldType === type;
};

/**
 * A helper function to get the display expression of a field in schema.
 * @param models Models from the project to get the reference from.
 * @param schema A Keps schema.
 * @param field A field name.
 */
export const getReferenceDisplay = (models: KepsModels, schema: KepsSchema, field: string) => {
  const referenceTo = schema[field].type.slice(1);
  if (models[referenceTo]) {
    return models[referenceTo].properties.displayExpression;
  }
  return null;
};

/**
 * A helper function to get the display expression of an array field in schema.
 * @param models Models from the project to get the reference from.
 * @param schema A Keps schema.
 * @param field A field name.
 */
export const getReferenceDisplayArray = (models: KepsModels, schema: KepsSchema, field: string) => {
  if (
    schema[field].type === 'array' &&
    typeof schema[field].subSchema === 'string' &&
    schema[field].subSchema[0] === ':'
  ) {
    const referenceTo = (<string>schema[field].subSchema).slice(1);
    if (models[referenceTo]) {
      return models[referenceTo].properties.displayExpression;
    }
  }
  return null;
};
