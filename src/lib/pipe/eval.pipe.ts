import { Pipe, PipeTransform } from '@angular/core';

/**
 * A pipe to evaluate an expression from supplied context.
 */
@Pipe({
  name: 'eval'
})
export class EvalPipe implements PipeTransform {

  transform(context: any, expression: string): any {
    // If expression is not a string, return the context.
    if (typeof expression !== 'string') {
      return context;
    }

    // Convert expression to tokens.
    let tokens = this.getTokens(expression);

    // Parse each token based on the context.
    tokens = tokens.map(token => this.parseToken(token, context));

    // Combine tokens back as a string.
    return this.combineTokens(tokens);
  }

  private getTokens(str: string): string[] {
    return str.match(/[^\s"']+|"([^"]*)"|'([^']*)'/gm);
  }

  private parseToken(token: string, context: object): any {
    if (
      (token[0] === '"' && token[token.length - 1] === '"') ||
      (token[0] === `'` && token[token.length - 1] === `'`)
    ) {
      // If token is a quoted string, return the text inside the quotes.
      return token.substring(1, token.length - 1);
    } else if (token === '+') {
      // Return '+' as it is.
      return token;
    } else if (token[0] === '.') {
      // If the first character is a dot, continue parsing after the dot.
      return this.parseToken(token.substring(1), context);
    } else {
      // If context is missing, log the error and return null.
      if (context === null) {
        this.parseError('No context supplied.');
        return null;
      }

      // Check for special characters.
      const specials = token.match(/(\[|\.)/gm);
      if (specials && specials.length > 0) {
        if (specials[0] === '[') {
          const inbetween = token.match(/\[(\S)\]/);
          if (inbetween && inbetween.length > 0) {
            const num = parseInt(inbetween[1], 10);
            const before = token.substring(0, token.indexOf('['));
            if (!context[before]) {
              this.parseError(`${before} not found`);
              return null;
            }
            const after = token.substring(token.indexOf(']') + 1);
            let parsed: any;
            if (isNaN(num)) {
              parsed = context[before][inbetween[1]];
            } else {
              parsed = context[before][num];
            }
            if (after.length > 0) {
              return this.parseToken(after, parsed);
            } else {
              return parsed;
            }
          } else {
            this.parseError('No closing bracket.');
            return null;
          }
        } else {
          const i = token.indexOf('.');
          return this.parseToken(token.substring(i + 1), context[token.substring(0, i)]);
        }
      } else {
        return context[token];
      }
    }
  }

  private combineTokens(tokens: any[]) {
    let result: any;
    for (let i = 0; i < tokens.length; i++) {
      if (i % 2 === 1) {
        if (tokens[i] !== '+') {
          this.parseError('error');
          return null;
        }
      } else {
        if (!result) {
          result = tokens[i];
        } else {
          result += tokens[i];
        }
      }
    }
    return result;
  }

  private parseError(message: string) {
    console.error('EvalPipeError: ' + message);
  }

}
