import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

/**
 * A pipe that transforms number to formatted number.
 */
@Pipe({
  name: 'numberFormat'
})
export class NumberFormatPipe implements PipeTransform {

  transform(value: number, format = ''): any {
    if (typeof value !== 'number' || !format) {
      return value;
    } else {
      // Get prefix.
      let prefix = '';
      while (!format.startsWith('0') && format.length > 1) {
        prefix += format[0];
        format = format.slice(1);
      }

      // Get number of 0s.
      const re = /[0]+/g;
      const zeroes = format.match(re);
      const decimalPipe = new DecimalPipe('en-US');
      let number = '';
      if (zeroes.length === 1) {
        number = decimalPipe.transform(value, `${0}.${zeroes[0].length}-${zeroes[0].length}`);
      } else if (zeroes.length === 2) {
        number = decimalPipe.transform(value, `${zeroes[0].length}.${zeroes[1].length}-${zeroes[1].length}`);
      } else if (zeroes.length === 3) {
        number = decimalPipe.transform(value, `${zeroes[1].length}.${zeroes[2].length}-${zeroes[2].length}`);
      } else {
        number = value.toString();
      }

      // Get positions after zeroes.
      const pos = [];
      let x: any;
      while ((x = re.exec(format)) !== null) {
        pos.push(re.lastIndex);
      }

      // Get symbols.
      const symbols = [];
      for (let i = 1; i <= pos.length; i++) {
        const symbol = format.substring(pos[i - 1], i === pos.length ? format.length : pos[i] - 1);
        symbols.push(symbol.replace(/[0]/g, ''));
      }

      let suffix = '';
      if (zeroes.length === 2) {
        number = number.replace(/\./g, symbols.shift());
        suffix = symbols.shift() || '';
      } else if (zeroes.length === 3) {
        number = number.replace(/\,/g, '$');
        number = number.replace(/\./g, '^');
        number = number.replace(/\$/g, symbols.shift());
        number = number.replace(/\^/g, symbols.shift());
        suffix = symbols.shift() || '';
      }

      return prefix + number + suffix;
    }
  }

}
