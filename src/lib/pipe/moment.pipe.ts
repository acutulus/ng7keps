import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

/**
 * A pipe that transforms UNIX timestamp to a nice moment-formatted string.
 */
@Pipe({
  name: 'moment'
})
export class MomentPipe implements PipeTransform {

  transform(timestamp: number, format: string = 'MM/DD/YYYY'): string {
    return moment(timestamp).format(format);
  }

}
