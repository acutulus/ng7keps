import { Pipe, PipeTransform } from '@angular/core';

/**
 * A pipe that transforms the keys of an object to an array of strings.
 */
@Pipe({
  name: 'keys'
})
export class KeysPipe implements PipeTransform {

  transform(obj: object): string[] {
    return Object.keys(obj);
  }

}
