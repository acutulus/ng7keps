import { Pipe, PipeTransform } from '@angular/core';
import { EvalPipe } from './eval.pipe';
import _ from 'lodash';

/**
 * A pipe that transforms an array to a nicely formatted list.
 */
@Pipe({
  name: 'arrayDisplay'
})
export class ArrayDisplayPipe implements PipeTransform {

  transform(value: any, delimiter = ', ', displayExpression = ''): string {
    if (Array.isArray(value)) {
      // Assume that array contains object of the same type.
      if (_.isPlainObject(value[0])) {
        // If it's an array of objects, map the objects using display expression.
        const evalPipe = new EvalPipe();
        value = value.map(obj => evalPipe.transform(obj, displayExpression));
      }
      return value.join(delimiter);
    } else {
      return value;
    }
  }

}
