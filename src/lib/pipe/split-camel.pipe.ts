import { Pipe, PipeTransform } from '@angular/core';

/**
 * A pipe that transforms a camel-cased string to words separated by space.
 */
@Pipe({
  name: 'splitCamel'
})
export class SplitCamelPipe implements PipeTransform {

  transform(str: string): string {
    const re = /([A-Z])([A-Z])([a-z])|([a-z])([A-Z])|([a-z])([0-9])/g;
    return str.replace(re, '$1$4$6 $2$3$5$7');
  }

}
