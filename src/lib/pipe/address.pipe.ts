import { Pipe, PipeTransform } from '@angular/core';
import { KepsAddress } from '../interface';

/**
 * A pipe that transforms a Keps address object to a nicely formatted string.
 */
@Pipe({
  name: 'address'
})
export class AddressPipe implements PipeTransform {

  transform(address: KepsAddress, lines: number = 4): string {
    let str = '';

    if (!address) {
      return str;
    }

    str += address.address1;
    if (lines === 4 && address.address2) {
      str += `\n${address.address2}`;
    } else if (lines === 3 && address.address2) {
      str += `, ${address.address2}`;
    }
    str += `\n${address.city}, ${address.region} ${address.postal}`;
    str += `\n${address.country}`;

    return str;
  }

}
