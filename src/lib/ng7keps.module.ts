// Import Angular modules.
import { NgModule, ModuleWithProviders } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Import other external modules.
import { MaterialModule } from './material-module';

// Import model.
import { KepsModels } from './interface';

// Import services.
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { DataService } from './service/data.service';
import { ErrorService } from './service/error.service';
import { PopupService } from './service/popup.service';

// Import pipes.
import { AddressPipe } from './pipe/address.pipe';
import { EvalPipe } from './pipe/eval.pipe';
import { KeysPipe } from './pipe/keys.pipe';
import { MomentPipe } from './pipe/moment.pipe';
import { SplitCamelPipe } from './pipe/split-camel.pipe';

// Import components.
import { FileSelectComponent } from './component/file-select/file-select.component';
import { DialogComponent } from './component/dialog/dialog.component';
import { FormComponent } from './component/form/form.component';
import { FieldAddressComponent } from './component/form/field-address/field-address.component';
import { FieldArrayComponent } from './component/form/field-array/field-array.component';
import { FieldBooleanComponent } from './component/form/field-boolean/field-boolean.component';
import { FieldDatetimeComponent } from './component/form/field-datetime/field-datetime.component';
import { ArrayDisplayPipe } from './pipe/array-display.pipe';
import { NumberFormatPipe } from './pipe/number-format.pipe';
import { TableComponent } from './component/table/table/table.component';
import { TableColDirective } from './component/table/table-col.directive';
import { DynamicTableComponent } from './component/table/dynamic-table/dynamic-table.component';

/**
 * Ng7Keps main module.
 */
@NgModule({
  declarations: [
    AddressPipe,
    ArrayDisplayPipe,
    EvalPipe,
    KeysPipe,
    MomentPipe,
    NumberFormatPipe,
    SplitCamelPipe,
    FileSelectComponent,
    DialogComponent,
    FormComponent,
    FieldAddressComponent,
    FieldArrayComponent,
    FieldBooleanComponent,
    FieldDatetimeComponent,
    TableComponent,
    TableColDirective,
    DynamicTableComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  exports: [
    AddressPipe,
    ArrayDisplayPipe,
    EvalPipe,
    KeysPipe,
    MomentPipe,
    NumberFormatPipe,
    SplitCamelPipe,
    FileSelectComponent,
    FormComponent,
    FieldAddressComponent,
    FieldArrayComponent,
    FieldBooleanComponent,
    FieldDatetimeComponent,
    TableComponent,
    DynamicTableComponent,
    TableColDirective
  ],
  entryComponents: [
    DialogComponent
  ]
})
export class Ng7KepsModule {
  /**
   * Initializes Ng7Keps with the project models.
   * @param models Generated models of the project.
   */
  static forRoot(models: KepsModels): ModuleWithProviders {
    return {
      ngModule: Ng7KepsModule,
      providers: [
        AuthService,
        AuthGuard,
        AuthInterceptor,
        DataService,
        ErrorService,
        PopupService,
        {
          provide: 'MODELS',
          useValue: models
        }
      ]
    };
  }
}
