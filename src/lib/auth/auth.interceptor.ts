import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
  HttpEvent
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { apiRoute } from '../config';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

/**
 * Intercepts outgoing requests and adds token if needed.
 * Intercepts incoming responses and sets/removes user if needed.
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  /**
   * @internal Do not use!
   */
  constructor(private auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Intercept request and add token.
    let newUrl = req.url;
    let newHeaders = req.headers;
    if (req.url.startsWith(apiRoute)) {
      const token = this.auth.getToken();
      if (token) {
        newHeaders = newHeaders.set('Authorization', `Bearer ${token}`);
      }
      if (newUrl.indexOf('?') > -1) {
        newUrl = `${newUrl}&cache_bust=${(new Date()).getTime()}`;
      } else {
        newUrl = `${newUrl}?cache_bust=${(new Date()).getTime()}`;
      }
    }

    // Modify request and pass it to the next handler.
    req = req.clone({
      url: newUrl,
      headers: newHeaders
    });
    return next.handle(req)
    .pipe(
      tap(res => {
        if (res instanceof HttpResponse) {
          // Intercept response and refresh user data if needed.
          if (req.url.startsWith(apiRoute)) {
            if (res.headers.has('x-user-token-refresh') && res.headers.get('x-user-token-refresh') !== '') {
              const refreshedUser = JSON.parse(decodeURIComponent(res.headers.get('x-user-token-refresh')));
              this.auth.setUser(refreshedUser);
            }
          }
        }
      }, err => {
        // Intercept error response and logout user if session timed out.
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            if (err.headers.has('x-user-logout')) {
              alert('Your Session has timed out please login again.');
              this.auth.logout();
              window.location.reload();
            }
          }
        }
      })
    );
  }

}
