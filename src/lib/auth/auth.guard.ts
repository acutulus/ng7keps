import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AuthService } from './auth.service';
import { AuthGuardOptions } from './auth-interfaces';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';

/**
 * A route guard that rejects user from accessing the route if they're not logged in.
 *
 * Can also allows only specific user role from accessing the route based on the route
 * data's `guardOpt` field. See {@link AuthGuardOptions}.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  /**
   * @internal Do not use!
   */
  constructor(
    private router: Router,
    private auth: AuthService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.auth.isLoggedIn$()
    .pipe(
      take(1),
      map((isLoggedIn: boolean) => {
        if (next.data && next.data.guardOpt) {
          const opts = <AuthGuardOptions>next.data.guardOpt;
          if (isLoggedIn) {
              const user = this.auth.getUser();
              if (opts.role) {
                if (user.roles.includes(opts.role)) {
                  return true;
                } else {
                  alert('Access Denied');
                  if (opts.logout) {
                    this.auth.logout();
                    if(opts.logoutUrl){
                      window.location.href = opts.logoutUrl;
                    } else{
                      window.location.href = window.location.origin;
                    }
                  } else {
                    this.router.navigate([this.router.url]);
                  }
                  return false;
                }
              
            }
            return true;
          } else {
            if(opts.logoutUrl){
              window.location.href = opts.logoutUrl;
            } else{
              window.location.href = window.location.origin;
            }
            return false;
          }
      }
      })
    );
  }

}
