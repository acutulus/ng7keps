import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ReplaySubject, Observable } from 'rxjs';
import { webAlias, apiRoute } from '../config';
import { KepsUser } from '../interface';
import { AuthLoginData, AuthSignupData } from './auth-interfaces';

const storageKey = webAlias + '-user';

/**
 * A service that handles authentication with a Keps server.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: KepsUser;
  private user$ = new ReplaySubject<KepsUser>(1);
  private loggedIn$ = new ReplaySubject<boolean>(1);

  /**
   * @internal Do not use!
   */
  constructor(private http: HttpClient) { }

  /**
   * Starts the auth service.
   * Should be called only when the app is bootstrapping.
   */
  async start(): Promise<void> {
    // Try getting user from storage.
    let storageUser: KepsUser;
    try {
      storageUser = JSON.parse(localStorage.getItem(storageKey));
    } catch (err) {
      console.error('Error getting user from local storage');
      this.setUser(null);
    }

    // If user exist, then get user data from server.
    if (storageUser) {
      try {
        const serverUser = await this.http.get<KepsUser>(apiRoute + 'users/me').toPromise();
        if (serverUser.token) {
          this.setUser(serverUser);
        } else if (storageUser.tokenExpires > new Date().getTime()) {
          this.setUser(storageUser);
        } else {
          // Token expired.
          this.user$.error(serverUser);
        }
      } catch (err) {
        // Token error.
        this.user$.error(err);
      }
    } else {
      this.setUser(null);
    }
  }

  /**
   * Saves/removes user and emits the user and logged in status to observables.
   * @internal Do not use!
   * @param user A user or null
   */
  setUser(user?: KepsUser) {
    if (user) {
      localStorage.setItem(storageKey, JSON.stringify(user));
      this.user = user;
      this.user$.next(user);
      this.loggedIn$.next(true);
    } else {
      localStorage.removeItem(storageKey);
      this.user = null;
      this.user$.next(null);
      this.loggedIn$.next(false);
    }
  }

  /**
   * Returns the current user.
   */
  getUser(): KepsUser {
    return this.user;
  }

  /**
   * Returns an observable that will emit the current user.
   */
  getUser$(): Observable<KepsUser> {
    return this.user$.asObservable();
  }

  /**
   * Returns token from the user.
   */
  getToken(): string | null {
    // If user is stored in auth service, use that user's token.
    if (this.user) {
      return this.user.token;
    } else {
      // Otherwise, try getting from the storage.
      try {
        const storageUser: KepsUser = JSON.parse(localStorage.getItem(storageKey));
        return storageUser.token;
      } catch (err) {
        console.error('Error getting user from local storage');
        return null;
      }
    }
  }

  /**
   * Returns an observable that emits logged in status.
   */
  isLoggedIn$(): Observable<boolean> {
    return this.loggedIn$.asObservable();
  }

  /**
   * Logs in using local provider, sets current user, and returns the user object.
   * @param provider Local provider.
   * @param data An object with username and password fields.
   */
  async loginWithProvider(provider: 'local', data: AuthLoginData): Promise<KepsUser>;
  /**
   * Logs in using the passed in provider name.
   * @param provider Provider to use to login.
   */
  async loginWithProvider(provider: string): Promise<void>;
  /**
   * Logs in using the passed in provider name.
   * @param provider Provider to use to login.
   * @param data Data to be passed on to the provider.
   */
  async loginWithProvider(provider: string, data?: AuthLoginData): Promise<KepsUser | void> {
    if (provider === 'local') {
      const serverUser = await this.http.post<KepsUser>(apiRoute + 'users/signin', data).toPromise();
      this.setUser(serverUser);
      return this.user;
    } else {
      window.location.href = apiRoute + 'oauths/' + provider;
    }
  }

  /**
   * Signs up using local provider, sets current user, and returns the user object.
   * @param provider Local provider.
   * @param data An object with use data.
   * @param customRoute Custom route to use instead of the default 'users/signup'.
   */
  async signupWithProvider(provider: 'local', data: AuthSignupData, customRoute?: string): Promise<KepsUser>;
  /**
   * Signs up user using the passed in provider name.
   * @param provider Provider to use to sign up.
   */
  async signupWithProvider(provider: string): Promise<void>;
  /**
   * Signs up user using the passed in provider name.
   * @param provider Provider to use to sign up.
   * @param data Data to be passed on to the provider.
   * @param customRoute Custom route to use instead of the default 'users/signup'.
   */
  async signupWithProvider(provider: string, data?: AuthSignupData, customRoute?: string): Promise<KepsUser | void> {
    if (provider === 'local') {
      const route = apiRoute + customRoute || 'users/signup';
      const serverUser = await this.http.post<KepsUser>(route, data).toPromise();
      if (data.noLogin) {
        return serverUser;
      } else {
        this.setUser(serverUser);
        return this.user;
      }
    } else {
      window.location.href = webAlias + '/auth/' + provider;
    }
  }

  /**
   * Switch current user.
   */
  switchUser(newUser: KepsUser) {
    this.setUser(newUser);
  }

  /**
   * Logs out the current user.
   */
  logout() {
    this.setUser(null);
  }
}
