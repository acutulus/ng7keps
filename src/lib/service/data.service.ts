import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from './error.service';
import { apiRoute } from '../config';
import { KepsObject, GraphqlResult } from '../interface';

/**
 * A service to send requests to Keps server.
 */
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private cache: Record<string, any> = {};

  /**
   * @internal Do not use!
   */
  constructor(
    private http: HttpClient,
    private errorService: ErrorService
  ) { }

  /**
   * Sends a GET request to a route of Keps server.
   * @param route Route to get from.
   * @param id ID of an object.
   * @param useCache Whether we should use cache or not.
   */
  async get<T = any>(route: string, id?: string, useCache?: boolean): Promise<T> {
    try {
      if (id) {
        route += '/' + id;
      }

      const getFromCache: Promise<T> = new Promise((resolve, reject) => {
        if (useCache && this.cache[route]) {
          setTimeout(() => {
            resolve(this.cache[route]);
          }, 3000);
        }
      });

      const getFromKeps = this.http.get<T>(apiRoute + route).toPromise()
      .then(result => {
        this.cache[route] = result;
        return result;
      });

      return Promise.race([getFromCache, getFromKeps]);
    } catch (err) {
      throw this.errorService.normalizeError(err);
    }
  }

  /**
   * Sends a POST request to a route of Keps server.
   * @param route Route to post to.
   * @param data Data to be passed on to the route.
   */
  async post<T = any>(route: string, data: any): Promise<T> {
    try {
      data = this.normalizeData(data);
      return this.http.post<T>(apiRoute + route, data).toPromise();
    } catch (err) {
      throw this.errorService.normalizeError(err);
    }
  }

  /**
   * Sends a PUT request to a route of Keps server.
   * @param route Route to put to.
   * @param data Data to be passed on to the route.
   */
  async put<T = any>(route: string, data: any): Promise<T> {
    try {
      data = this.normalizeData(data);
      return this.http.put<T>(apiRoute + route, data).toPromise();
    } catch (err) {
      throw this.errorService.normalizeError(err);
    }
  }

  /**
   * Sends a DELETE request to a route of Keps server.
   * @param type Type of the object to delete (can also be just a route).
   * @param data An ID string or a Keps object to be deleted.
   */
  async delete<T = void>(type: string, data: string | KepsObject): Promise<T> {
    try {
      let route = apiRoute;
      if (typeof data === 'string') {
        route += type + '/' + data;
      } else if (typeof data === 'object' && data._id) {
        route += type + '/' + data._id;
      } else {
        route += type;
      }
      return this.http.delete<T>(route).toPromise();
    } catch (err) {
      throw this.errorService.normalizeError(err);
    }
  }

  /**
   * Sends a request depending on the command to Keps server.
   * @param command A Keps command (e.g. 'put.users.update').
   * @param data Data to be passed on to the server.
   */
  async call<T = any>(command: string, data: FormData | any): Promise<T> {
    try {
      // Parse the command.
      const [method, type, route] = command.split('.');
      if (!method || !type || !route) {
        throw new Error('Invalid Keps command.');
      }

      // Build URL based on data.
      let url = apiRoute + type + 's/';
      if (data instanceof FormData) {
        const id = data.get(type);
        if (id) {
          data.delete(type);
          url += id + '/' + route;
        } else {
          url += route;
        }
      } else if (typeof data === 'object') {
        if (data[type]) {
          const typeData = data[type];
          if (typeof typeData === 'object') {
            url += typeData._id + '/' + route;
            delete data[type];
          } else if (typeof typeData === 'string') {
            url += typeData + '/' + route;
            delete data[type];
          } else {
            url += route;
          }
        } else {
          url += route;
        }
      } else {
        url += route;
      }

      // Truncate URL for specific routes.
      if (['query', 'read', 'delete', 'update', 'create'].includes(route)) {
        url = url.substr(0, url.length - route.length - 1);
      }

      // Send out requests.
      if (method === 'get' || method === 'delete') {
        url += '?' + this.serializeData(data);
        return this.http.request<T>(method.toUpperCase(), url).toPromise();
      } else if (method === 'post' || method === 'put') {
        return this.http.request<T>(method.toUpperCase(), url, { body: data }).toPromise();
      } else {
        return this.http.get<T>(url).toPromise();
      }
    } catch (err) {
      throw this.errorService.normalizeError(err);
    }
  }

  /**
   * Sends a GraphQL query request to the Keps server and returns the result back.
   *
   * Will throw an error if the result contains any error.
   * @param query A GraphQL query.
   */
  async graphql(query: string): Promise<GraphqlResult> {
    try {
      // Clean up whitespaces in query.
      const cleanQuery = query.replace(/([^"]+)|("[^"]+")/g, ($0, $1, $2) => {
        if ($1) {
          return $1.replace(/\s/g, '');
        } else {
          return $2;
        }
      });

      // Send a GraphQL request and throw error if any.
      const result = await this.http.get<GraphqlResult>(
        apiRoute + `graphqls?q={${cleanQuery}}`
      ).toPromise();
      if (result.errors) {
        throw new Error(result.errors[0]);
      }
      return result;
    } catch (err) {
      throw this.errorService.normalizeError(err);
    }
  }

  /**
   * Clears all stored cache.
   */
  clearCache() {
    this.cache = {};
  }

  /**
   * Converts data to FormData if it contains a File or Blob.
   * @param data Data to be normalized.
   */
  private normalizeData(data: object): object | FormData {
    if (!(data instanceof FormData)) {
      // Check if data contains a File or Blob.
      if (
        Object.values(data).some(value => value instanceof File || value instanceof Blob)
      ) {
        // Convert to FormData and return it.
        const formData = new FormData();
        for (const [key, value] of Object.entries(data)) {
          formData.set(key, value);
        }
        return formData;
      }
    }
    return data;
  }

  /**
   * Serializes data and returns it as a query string.
   * @param data Data to be serialized.
   * @param prefix Prefix for the passed data.
   */
  private serializeData(data: object, prefix?: string): string {
    const str = [];
    for (const p in data) {
      const k = prefix ? prefix + '[' + p + ']' : p, v = data[p];
      str.push(
        typeof v === 'object' ?
        this.serializeData(v, k) :
        encodeURIComponent(k) + '=' + encodeURIComponent(v)
      );
    }
    return str.join('&');
  }
}
