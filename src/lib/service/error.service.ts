import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { KepsError } from '../interface';

/**
 * A service to do stuffs related to errors.
 */
@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  /**
   * @internal Do not use!
   */
  constructor() { }

  /**
   * Normalizes error.
   *
   * Will also log HttpErrorResponse for debugging purpose before normalizing it.
   * @param err Error to be normalized.
   */
  normalizeError(err: Error): Error {
    if (err instanceof HttpErrorResponse) {
      console.error(err);
      // Check if error from Keps.
      if (err.error && err.error.errors && err.error.errors[0]) {
        const kepsErr: KepsError = err.error.errors[0];
        err = new Error(kepsErr.friendly || kepsErr.message);
      } else {
        err = new Error(err.message);
      }
    }
    return err;
  }
}
