import { Injectable } from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatSnackBar
} from '@angular/material';
import { DialogComponent, DialogData } from '../component/dialog';

/**
 * A service to show custom dialogs and toasts.
 */
@Injectable({
  providedIn: 'root'
})
export class PopupService {

  /**
   * @internal Do not use!
   */
  constructor(
    private dialog: MatDialog,
    private toast: MatSnackBar
  ) { }

  /**
   * Shows a custom dialog.
   *
   * @param data
   * Data to be shown on the dialog.
   * See {@link DialogData}.
   *
   * @param options
   * Additional options for the dialog.
   * See [MatDialogConfig](https://material.angular.io/components/dialog/api#MatDialogConfig).
   *
   * @returns
   * The value set on the dialog buttons.
   * If the user clicks the 'Close' button or outside the dialog, it will return undefined.
   */
  showDialog(data: DialogData, options: MatDialogConfig = {}) {
    options.data = data;

    return this.dialog
    .open(DialogComponent, options)
    .afterClosed()
    .toPromise();
  }

  /**
   * Show a custom toast.
   * @param message Text to be shown on the toast.
   * @param duration How long the toast should be shown.
   */
  showToast(message: string, duration = 3000) {
    this.toast.open(message, null, { duration });
  }
}
