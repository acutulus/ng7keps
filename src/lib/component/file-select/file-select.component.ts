import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';
import { ThemePalette } from '@angular/material';

/**
 * A component that wraps around the default HTML file select button.
 */
@Component({
  selector: 'keps-file-select',
  templateUrl: './file-select.component.html',
  styleUrls: ['./file-select.component.scss']
})
export class FileSelectComponent implements OnInit {
  /**
   * File to be selected.
   */
  @Input() selectedFile: File = null;

  /**
   * An event emitter that emits the selected file.
   */
  @Output() selectedFileChange = new EventEmitter<File>();

  /**
   * Label for the file select button.
   */
  @Input() label = 'Choose File';

  /**
   * Whether the component should display the file name.
   */
  @Input() showFilename = true;

  /**
   * The type(s) of file that the file select dialog can accept.
   *
   * See: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#Unique_file_type_specifiers
   *
   * If the value is "image", "video", or "audio", it will accept every possible file types
   * of that media type.
   */
  @Input() type = '';

  /**
   * Angular Material color for the file select button.
   */
  @Input() color: ThemePalette = 'primary';

  /**
   * The `input` element itself.
   */
  @ViewChild('fileSelector', { static: true }) private fileSelector: ElementRef;

  /**
   * @internal Do not use!
   */
  constructor() { }

  /**
   * @internal Do not use!
   */
  ngOnInit() {
    if (
      this.type === 'image' ||
      this.type === 'video' ||
      this.type === 'audio'
    ) {
      this.type += '/*';
    }
  }

  /**
   * Open the file select dialog manually.
   */
  openFileSelect() {
    this.fileSelector.nativeElement.click();
  }

  /**
   * Select a file.
   * @param files Files to be passed. Only the first one will be selected.
   */
  selectFile(files: File[]) {
    if (files && files.length > 0) {
      this.selectedFile = files[0];
      this.selectedFileChange.emit(this.selectedFile);
    }
  }

}
