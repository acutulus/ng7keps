import { ThemePalette } from '@angular/material';

/**
 * Represents a button for the dialog.
 */
export interface DialogButton {
  label: string;

  /**
   * Value to be returned when the user clicks this button.
   */
  result: any;

  /**
   * Angular Material color for the dialog button.
   */
  color?: ThemePalette;
}

/**
 * Represents data to be shown by the dialog.
 */
export interface DialogData {
  title: string;
  text: string;

  /**
   * Optional buttons to be shown by the dialog.
   *
   * If no button is passed, the 'Close' button will be shown by default.
   *
   * See {@link DialogButton}.
   */
  buttons?: DialogButton[];

  /**
   * Whether the dialog should show 'Close' button or not.
   */
  showClose?: boolean;
}
