import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData, DialogButton } from './dialog-interfaces';

/**
 * A component that wraps around an Angular Material dialog.
 *
 * Use the {@link PopupService} to create this dialog.
 */
@Component({
  selector: 'keps-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {
  /**
   * @internal Do not use!
   */
  buttons: DialogButton[];

  /**
   * @internal Do not use!
   */
  showClose: boolean;

  /**
   * @internal Do not use!
   */
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.buttons = this.data.buttons || [];
    if (this.data.showClose !== undefined) {
      this.showClose = this.data.showClose;
    } else {
      this.showClose = !(this.buttons.length > 0);
    }
  }

}
