import {
  Component,
  OnInit,
  AfterViewInit,
  AfterContentInit,
  Input,
  Output,
  ViewChild,
  ContentChildren,
  QueryList,
  TemplateRef,
  EventEmitter,
  Inject
} from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { DataService } from '../../../service/data.service';
import { KepsModels, KepsSchema, GraphqlResult, KepsObject } from '../../../interface';
import { Subject, merge } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { TableColDirective, CustomColumn } from '../table-col.directive';
import { isType, getReferenceDisplay, getReferenceDisplayArray } from '../../../utils';
import _ from 'lodash';
import { filter } from 'minimatch';

/**
 * A function used by the dynamic table to filter result.
 */
type FilterFunction = (data: KepsObject) => boolean;

@Component({
  selector: 'keps-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.scss']
})
export class DynamicTableComponent implements OnInit, AfterViewInit, AfterContentInit {
  /**
   * Name of the model for the table.
   */
  @Input() model: string;

  /**
   * Schema of the model.
   * @internal Do not use!
   */
  schema: KepsSchema;

  /**
   * Data to be shown on the table from the GraphQL query.
   * @readonly
   */
  data: KepsObject[] = [];

  /**
   * Comma-separated list of fields for the GraphQL query.
   */
  @Input() query: string;

  /**
   * Additional filter to be added to the GraphQL query.
   */
  @Input() filterQuery: Record<string, string>;

  /**
   * Columns to be displayed on the table.
   */
  @Input() displayedColumns: string[] = [];

  /**
   * Columns to be hidden on the table.
   */
  @Input() hiddenColumns: string[] = [];

  /**
   * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
   * @internal Do not use!
   */
  allColumns: string[] = [];

  /**
   * Queries all `TableColDirective` data inside the component.
   */
  @ContentChildren(TableColDirective) private customColDatas: QueryList<TableColDirective>;

  /**
   * Queries all `TableColDirective` templates inside the component.
   */
  @ContentChildren(TableColDirective, { read: TemplateRef }) private customColTemplates: QueryList<TemplateRef<any>>;

  /**
   * A record of custom column data and template keyed by the custom column ID.
   */
  customCols: Record<string, CustomColumn> = {};

  /**
   * Reference to the MatPaginator.
   */
  @ViewChild(MatPaginator, { static: true }) private paginator: MatPaginator;

  /**
   * A key used when saving page size number to the local storage.
   */
  @Input() savePageSize: string;

  /**
   * The table's default paginator page size.
   */
  @Input() defaultPageSize = 10;

  /**
   * The current page size on paginator.
   */
  pageSize: number;

  /**
   * Length of the data to be shown on the paginator.
   * @readonly
   */
  dataLength = Infinity;

  /**
   * Whether the data length has been known (from last query) or not.
   */
  private isDataLengthSet = false;

  /**
   * Reference to the MatSort.
   */
  @ViewChild(MatSort, { static: true }) private sort: MatSort;

  /**
   * The ID of the column that should be sorted.
   */
  @Input() activeSort: string;

  /**
   * Whether the table should show the default search box or not.
   */
  @Input() showSearch = false;

  /**
   * Field used for the search.
   */
  @Input() searchFields: string[];

  /**
   * String to be searched based on `searchFields`.
   */
  private _search: string;

  /**
   * A subject that emits every time the search string changes.
   */
  searchChange = new Subject<string>();

  /**
   * String to be searched based on `searchFields`.
   */
  get search(): string {
    return this._search;
  }
  @Input('search')
  set search(value: string) {
    this._search = value;
    this.searchChange.next(value);
  }

  /**
   * Delay for the search function to be called in milliseconds.
   */
  @Input() searchDelay = 1000;

  /**
   * Function to filter the result after query.
   */
  private _filterFunction: FilterFunction;

  /**
   * A subject that emits every time the filter function changes.
   */
  filterFunctionChange = new Subject<void>();

  /**
   * Function to filter the result after query.
   */
  get filterFunction(): FilterFunction {
    return this._filterFunction;
  }
  @Input('filterFunction')
  set filterFunction(value: FilterFunction) {
    this._filterFunction = value;
    this.filterFunctionChange.next();
  }

  /**
   * An event emitter that emits the object of the row clicked by the user.
   */
  @Output() rowClick = new EventEmitter<KepsObject>();

  /**
   * Whether the table is currently loading or not.
   * @readonly
   */
  tableLoading = true;

  /**
   * Whether the table has done initializing or not.
   * @readonly
   */
  initialized = false;

  /**
   * @internal Do not use!
   */
  constructor(
    @Inject('MODELS') public models: KepsModels,
    private dataService: DataService
  ) { }

  /**
   * @internal Do not use!
   */
  ngOnInit() {
    // Get the previous session's page size if any, or use the default page size.
    this.pageSize = Number(localStorage.getItem(this.savePageSize)) || this.defaultPageSize;
  }

  /**
   * @internal Do not use!
   */
  ngAfterViewInit() {
    // Get data as soon as view initialized.
    this.getData();

    // Setup delayed search.
    const delayedSearch = this.searchChange
    .pipe(
      debounceTime(this.searchDelay),
      distinctUntilChanged()
    );

    // If any of these observables emit, then we reset the paginator.
    delayedSearch.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.dataLength = Infinity;
    });
    this.filterFunctionChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.dataLength = Infinity;
    });
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // Save page size if paginator changes.
    if (this.savePageSize) {
      this.paginator.page.subscribe(() => {
        localStorage.setItem(this.savePageSize, this.paginator.pageSize.toString());
      });
    }

    // If any of these observables emit, then we refresh the data.
    merge(this.sort.sortChange, this.paginator.page, delayedSearch, this.filterFunctionChange)
    .subscribe(() => this.getData());
  }

  /**
   * @internal Do not use!
   */
  ngAfterContentInit() {
    // We are using `ngAfterContentInit` here because we can
    // only get the custom columns data after the component content
    // has been initialized.

    // Get schema from model name if supplied.
    if (this.model && this.models[this.model]) {
      this.schema = this.models[this.model].schema;
    } else if (this.model && !this.models[this.model]) {
      throw new Error(`Can't find a model named ${this.model}!`);
    }

    // Assert schema must exist.
    if (!this.schema) {
      throw new Error(`Must supply valid model name to Keps dynamic table!`);
    }

    // Set `allColumns` to be every public field in the schema.
    this.allColumns = Object.keys(this.schema).filter(field => field[0] !== '_');
    
    // Process custom columns.
    const colDatas = this.customColDatas.toArray();
    const colTemplates = this.customColTemplates.toArray();
    for (let i = 0; i < colDatas.length; i++) {
      const colId = colDatas[i].id;
      this.customCols[colId] = {
        data: colDatas[i],
        template: colTemplates[i]
      };
      this.allColumns.push(colId);
    }
    
    // If `displayedColumns` is not provided, use `allColumns` filtered by `hiddenColumns`.
    if (this.displayedColumns.length === 0) {
      this.displayedColumns = this.allColumns.filter(field => !this.hiddenColumns.includes(field));
    }

    this.initialized = true;
  }

  /**
   * Combines properties as GraphQL query and gets data from data service.
   */
  async getData() {
    try {
      this.tableLoading = true;

      const filters: Record<string, any> = {};

      // Setup page query.
      const { pageIndex, pageSize } = this.paginator;
      filters.offset = pageIndex * pageSize;

      // Setup sort query.
      const { active, direction } = this.sort;
      if (active) {
        filters.sort = (direction === 'desc' ? '-' : '+') + active;
      }

      // Setup search query.
      if (this.searchFields && this.search) {
        let mongoFilter = ` { '$or': [`;
        for (let field of this.searchFields) {
          mongoFilter += `{'${field}': {
            '$regex': '${this.search.replace('(', '%28').replace(')', '%29')}',
            '$options': 'i' }},`
        }
        mongoFilter = _.trimEnd(mongoFilter, ',');
        mongoFilter += ']}';
        filters.mongo = mongoFilter;
      }	

      // Set limit if there is not filter function.
      if (!this.filterFunction) {
        filters.limit = pageSize;
      }

      // Get data from data service.
      const result = await this.dataService.graphql(`
        ${this.model}s(
          ${this.convertFilter(Object.assign(filters, this.filterQuery))}
        ){${this.query}}
      `);
      const data = result.data[this.model + 's'];

      // If filter function exists, filter with that function.
      if (this.filterFunction) {
        this.data = _.filter(data, this.filterFunction);
      } else {
        // If this is not the first page and data is empty,
        // then we go back to the last page.
        if (pageIndex > 1 && data.length === 0) {
          // Calculate data length.
          this.dataLength = (pageIndex - 1) * pageSize + this.data.length;
          this.paginator.pageIndex = pageIndex - 1;
        } else {
          // If data length is smaller than page size,
          // we assume we're on last page and recalculate data length.
          if (data.length < pageSize) {
            this.dataLength = pageIndex * pageSize + data.length;
          }

          // Set data as is.
          this.data = data;
        }
      }

      this.tableLoading = false;
    } catch (err) {
      console.error(err);
    }
  }

  /**
   * Converts a filter object to Mongo filter string.
   * @param filters Pair of keys & values to be converted to Mongo filter.
   */
  private convertFilter(filters: Record<string, string>): string {
    let str = '';
    for (const filterName in filters) {
      if (filterName === 'limit' || filterName === 'offset') {
        str += `${filterName}:${filters[filterName]},`;
      } else {
        str += `${filterName}:"${filters[filterName]}",`;
      }
    }
    return str;
  }

  /**
   * Checks the type of a field.
   * @internal Do not use!
   * @param field Field to be checked.
   * @param type Type to be checked.
   */
  isType(field: string, type: string) {
    return isType(this.schema, field, type);
  }

  /**
   * Returns the display expression of a reference field.
   * @internal Do not use!
   * @param field Field to get from.
   */
  getReferenceDisplay(field: string) {
    return getReferenceDisplay(this.models, this.schema, field);
  }

  /**
   * Returns the display expression of an array of references field.
   * @internal Do not use!
   * @param field Field to get from.
   */
  getReferenceDisplayArray(field: string) {
    return getReferenceDisplayArray(this.models, this.schema, field);
  }

}
