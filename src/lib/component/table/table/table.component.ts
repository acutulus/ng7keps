import {
  Component,
  OnInit,
  AfterContentInit,
  DoCheck,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ContentChildren,
  QueryList,
  TemplateRef,
  IterableDiffer,
  IterableDiffers,
  Inject
} from '@angular/core';
import {
  MatTable,
  MatTableDataSource,
  MatPaginator,
  PageEvent,
  MatSort
} from '@angular/material';
import { TableColDirective, CustomColumn } from '../table-col.directive';
import { KepsSchema, KepsModels, KepsObject } from '../../../interface';
import { EvalPipe } from '../../../pipe/eval.pipe';
import { isType, getReferenceDisplay, getReferenceDisplayArray } from '../../../utils';

@Component({
  selector: 'keps-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterContentInit, DoCheck, OnChanges {
  /**
   * Name of the model for the table.
   */
  @Input() model: string;

  /**
   * Schema of the model for the table.
   *
   * If the model name is supplied,
   * then the table will get the schema from that model.
   */
  @Input() schema: KepsSchema;

  /**
   * Reference to the MatTable.
   */
  @ViewChild(MatTable, { static: true }) private table: MatTable<any>;

  /**
   * Data to be shown on the table.
   */
  @Input() data: KepsObject[] = [];

  /**
   * Wrapper for the data using MatTableDataSource.
   *
   * Do not change this. Use the `data` property instead to change the data.
   *
   * @internal Do not use!
   */
  dataSource: MatTableDataSource<KepsObject>;

  /**
   * Columns to be displayed on the table.
   */
  @Input() displayedColumns: string[] = [];

  /**
   * Columns to be hidden on the table.
   */
  @Input() hiddenColumns: string[] = [];


  /**
   * Custom function to filter the data source
   * See example below:
   * <pre>
   *  customFilter(Data: T, Filter: string): boolean {
   *     return <true if Data matches filter>
   *  }
   * <pre>
   * 
   * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
   */
  @Input() filterPredicate: Function = null;

  /**
   * Columns to be displayed on the table based on displayedColumns and hiddenColumns.
   * @internal Do not use!
   */
  allColumns: string[] = [];

  /**
   * Queries all `TableColDirective` data inside the component.
   */
  @ContentChildren(TableColDirective) private customColDatas: QueryList<TableColDirective>;

  /**
   * Queries all `TableColDirective` templates inside the component.
   */
  @ContentChildren(TableColDirective, { read: TemplateRef }) private customColTemplates: QueryList<TemplateRef<any>>;

  /**
   * A record of custom column data and template keyed by the custom column ID.
   */
  customCols: Record<string, CustomColumn> = {};

  /**
   * Reference to the MatPaginator.
   */
  @ViewChild(MatPaginator, { static: true }) private paginator: MatPaginator;

  /**
   * Whether the table should display the paginator or not.
   */
  @Input() showPaginator: false;

  /**
   * A key used when saving page size number to the local storage.
   */
  @Input() savePageSize: string;

  /**
   * The table's default paginator page size.
   */
  @Input() defaultPageSize = 10;

  /**
   * The current page size on paginator.
   */
  pageSize: number;

  /**
   * Reference to the MatSort.
   */
  @ViewChild(MatSort, { static: true }) private sort: MatSort;

  /**
   * Whether the table should display sort on headers or not.
   */
  @Input() showSort = false;

  /**
   * The ID of the column that should be sorted.
   */
  @Input() activeSort: string;

  /**
   * Whether the table should show the default search box or not.
   */
  @Input() showSearch = false;

  /**
   * The search query to be used to filter the data.
   */
  @Input() search = '';

  /**
   * An event emitter that emits the object of the row clicked by the user.
   */
  @Output() rowClick = new EventEmitter<KepsObject>();

  /**
   * An iterable differ instance to track changes to the data.
   */
  private diff: IterableDiffer<any>;

  /**
   * Instance of the `EvalPipe` to be used for custom columns sorting.
   */
  private evalPipe: EvalPipe;

  /**
   * Whether the table has done initializing or not.
   * @readonly
   */
  initialized = false;

  /**
   * @internal Do not use!
   */
  constructor(
    @Inject('MODELS') private models: KepsModels,
    private iterableDiffers: IterableDiffers
  ) {
    this.diff = this.iterableDiffers.find([]).create(null);
  }

  /**
   * @internal Do not use!
   */
  ngOnInit() {
    // Initialize the data source.
    this.dataSource = new MatTableDataSource<KepsObject>(this.data);

    // Initialize the paginator.
    if (this.showPaginator) {
      this.dataSource.paginator = this.paginator;

      // Get the previous session's page size if any, or use the default page size.
      this.pageSize = Number(localStorage.getItem(this.savePageSize)) || this.defaultPageSize;
    }

    // Initialize the sort.
    if (this.showSort) {
      this.dataSource.sort = this.sort;

      // Modify the data source's `sortingDataAccessor` to use `EvalPipe` for custom columns.
      this.evalPipe = new EvalPipe();
      this.dataSource.sortingDataAccessor = (data: KepsObject, sortHeaderId: string) => {
        const customCol = this.customCols[sortHeaderId];
        if (customCol && customCol.data.sortAccessor) {
          return this.evalPipe.transform(data, customCol.data.sortAccessor);
        }
        return data[sortHeaderId];
      };
    }
    this.setFilterPredicate(this.filterPredicate);    
  }

  /**
   * @internal Do not use!
   */
  ngAfterContentInit() {
    // We are using `ngAfterContentInit` here because we can
    // only get the custom columns data after the component content
    // has been initialized.

    // Get schema from model name if supplied.
    if (this.model && this.models[this.model]) {
      this.schema = this.models[this.model].schema;
    } else if (this.model && !this.models[this.model]) {
      throw new Error(`Can't find a model named ${this.model}!`);
    }

    // Assert schema must exist.
    if (!this.schema) {
      throw new Error(`Must supply valid model name or schema to Keps table!`);
    }

    // Set `allColumns` to be every public field in the schema.
    this.allColumns = Object.keys(this.schema).filter(field => field[0] !== '_');
    
    // Process custom columns.
    const colDatas = this.customColDatas.toArray();
    const colTemplates = this.customColTemplates.toArray();
    for (let i = 0; i < colDatas.length; i++) {
      const colId = colDatas[i].id;
      this.customCols[colId] = {
        data: colDatas[i],
        template: colTemplates[i]
      };
      this.allColumns.push(colId);
    }
    
    // If `displayedColumns` is not provided, use `allColumns` filtered by `hiddenColumns`.
    if (this.displayedColumns.length === 0) {
      this.displayedColumns = this.allColumns.filter(field => !this.hiddenColumns.includes(field));
    }

    this.initialized = true;
  }

  /**
   * @internal Do not use!
   */
  ngDoCheck() {
    // This will refresh the table data source and rerender
    // the table if there is any change to the data.
    if (this.initialized) {
      const changes = this.diff.diff(this.data);
      if (changes) {
        this.dataSource.data = this.data;
        this.table.renderRows();
      }
    }
  }

  /**
   * @internal Do not use!
   */
  ngOnChanges(changes: SimpleChanges) {
    // This check if the `search` property has changed and apply search if it does.
    if (this.initialized && changes.search) {
      this.applySearch(this.search);
    }
  }

  /**
   * Saves current page size to local storage if property `savePageSize` is supplied.
   * @internal Do not use!
   * @param event Event from paginator.
   */
  onPageChange(event: PageEvent) {
    if (this.savePageSize) {
      localStorage.setItem(this.savePageSize, event.pageSize.toString());
    }
  }

  /**
   * Applies a search query to filter the data.
   * @param search Query for the search.
   */
  applySearch(search: string) {
    this.dataSource.filter = search.trim().toLowerCase();
  }

  /**
   * Checks the type of a field.
   * @internal Do not use!
   * @param field Field to be checked.
   * @param type Type to be checked.
   */
  isType(field: string, type: string) {
    return isType(this.schema, field, type);
  }

  /**
   * Returns the display expression of a reference field.
   * @internal Do not use!
   * @param field Field to get from.
   */
  getReferenceDisplay(field: string) {
    return getReferenceDisplay(this.models, this.schema, field);
  }

  /**
   * Returns the display expression of an array of references field.
   * @internal Do not use!
   * @param field Field to get from.
   */
  getReferenceDisplayArray(field: string) {
    return getReferenceDisplayArray(this.models, this.schema, field);
  }


  /**
   * Set the filter predicate of data source. 
   * this function accepts a function as a parameter. 
   * See example below:
   * <pre> 
   *  customFilter(Data: T, Filter: string): boolean { 
   *     return <true if Data matches filter> 
   *  }
   * <pre>
   * If no funtion is provided, this function will use the default 
   * @see <a href="https://material.angular.io/components/table/api">Angular Table</a> for more info.
   * @param filterPredicateFunction custom filterPredicateFunction that follows the above format
   */
  setFilterPredicate(filterPredicateFunction= null) {
    if (filterPredicateFunction) {
      this.dataSource.filterPredicate = filterPredicateFunction;
    }
  }

}
