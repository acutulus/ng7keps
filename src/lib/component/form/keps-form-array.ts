import { FormArray, FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import _ from 'lodash';
import { KepsForm } from './keps-form';

/**
 * A Keps form for array field.
 *
 * It extends Angular's `FormArray` class
 * and changes its `patchValue` and `reset` methods to
 * deal with arrays appropriately.
 */
export class KepsFormArray extends FormArray {
  /**
   * Let the parent knows if there is update to the controls array length.
   */
  private counter$: BehaviorSubject<number>;

  /**
   * Set an optional BehaviorSubject counter.
   * @param counter$ A BehaviorSubject from parent component.
   */
  setCounter$(counter$: BehaviorSubject<number>) {
    this.counter$ = counter$;
  }

  /**
   * Patches the value of the `KepsFormArray`.
   * It accepts an array that matches the structure of the control,
   * and does its best to match the values to the correct controls in the group.
   *
   * It accepts both super-sets and sub-sets of the array without throwing an error.
   * @param values Array of latest values for the controls
   * @param options
   * Configuration options that determine how the control propagates changes and emits events after the value is patched.
   * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
   * - `emitEvent`:
   * When true or not supplied (the default),
   * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
   * When false, no events are emitted.
   * The configuration options are passed to the `updateValueAndValidity` method.
   */
  patchValue(
    values: any[],
    options: { onlySelf?: boolean; emitEvent?: boolean; } = {}
  ): void {
    values = values.map(value => {
      if (_.isPlainObject(value) && value._id && value._id) {
        return value._id;
      } else {
        return value;
      }
    });

    // Reset the form first.
    this.reset();

    // Add each form control with value and counter.
    for (let i = 0; i < values.length; i++) {
      const value = values[i];
      // If value is an object, then we assume that we're making a KepsForm.
      if (_.isPlainObject(value)) {
        this.push(new KepsForm({}));
        this.at(this.length - 1).patchValue(value);
      } else {
        this.push(new FormControl(value));
      }
    }

    // Let the parent component know that the length changed.
    // The `setTimeout` is here in case the `setCounter$` function is called after `patchValue`.
    setTimeout(() => {
      if (this.counter$) {
        this.counter$.next(this.length);
      }
    }, 5);

    this.updateValueAndValidity(options);
  }

  /**
   * Resets the FormArray and all descendants are marked `pristine` and `untouched`,
   * and the value of all descendants to null or null maps.
   *
   * You reset to a specific form state by passing in an array of states that matches the structure of the control.
   * The state is a standalone value or a form state object with both a value and a disabled status.
   * @param values Array of latest values for the controls
   * @param options
   * Configuration options that determine how the control propagates changes and emits events after the value is patched.
   * - `onlySelf`: When true, each change only affects this control and not its parent. Default is true.
   * - `emitEvent`:
   * When true or not supplied (the default),
   * both the `statusChanges` and `valueChanges` observables emit events with the latest status and value when the control value is updated.
   * When false, no events are emitted.
   * The configuration options are passed to the `updateValueAndValidity` method.
   */
  reset(
    values?: any[],
    options: { onlySelf?: boolean; emitEvent?: boolean; } = {}
  ): void {
    while (this.length > 0) {
      this.removeAt(0);
    }
    if (this.counter$) {
      this.counter$.next(0);
    }
    this.markAsPristine(options);
    this.markAsUntouched(options);
    this.updateValueAndValidity(options);
  }

}
