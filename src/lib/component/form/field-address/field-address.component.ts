import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ElementRef,
  Optional,
  Self
} from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormGroup, FormControl, NgControl, Validators } from '@angular/forms';
import { MatFormFieldControl, MatFormFieldAppearance, ThemePalette } from '@angular/material';
import { KepsAddress } from '../../../interface';
import { Subject } from 'rxjs';

@Component({
  selector: 'keps-field-address',
  templateUrl: './field-address.component.html',
  styleUrls: ['./field-address.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: FieldAddressComponent
    }
  ]
})
export class FieldAddressComponent implements MatFormFieldControl<KepsAddress>, OnInit, OnDestroy {
  /**
   * @internal Do not use!
   */
  static nextId = 0;

  /**
   * @internal Do not use!
   */
  private _required = false;

  /**
   * @internal Do not use!
   */
  private _disabled = false;

  /**
   * @internal Do not use!
   */
  private _placeholder: string;

  /**
   * @internal Do not use!
   */
  stateChanges = new Subject<void>();

  /**
   * @internal Do not use!
   */
  focused = false;

  /**
   * @internal Do not use!
   */
  errorState = false;

  /**
   * @internal Do not use!
   */
  controlType = 'keps-field-address';

  /**
   * @internal Do not use!
   */
  id = `keps-field-address-${FieldAddressComponent.nextId++}`;

  /**
   * @internal Do not use!
   */
  describedBy = '';

  /**
   * Form control of the field.
   */
  @Input() form = new FormGroup({});

  /**
   * Appearance style of the form.
   */
  @Input() appearance: MatFormFieldAppearance = 'standard';

  /**
   * Angular Material color of the form field underline and floating label.
   */
  @Input() color: ThemePalette = 'primary';

  /**
   * Whether the field has been initialized or not.
   * @internal Do not use!
   */
  initialized = false;

  /**
   * @internal Do not use!
   */
  constructor(
    private fm: FocusMonitor,
    private elRef: ElementRef,
    @Optional() @Self() public ngControl: NgControl
  ) {
    fm.monitor(elRef, true).subscribe(origin => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  /**
   * @internal Do not use!
   */
  ngOnInit() {
    const validators = [];
    if (this.required) {
      validators.push(Validators.required);
    }
    for (const field of ['address1', 'address2', 'city', 'region', 'postal', 'country']) {
      this.form.addControl(field, new FormControl(null, validators));
    }
    this.initialized = true;
  }

  /**
   * The value of the control.
   */
  @Input()
  get value(): KepsAddress | null {
    return this.form.value;
  }

  /**
   * @internal Do not use!
   */
  set value(v: KepsAddress | null) {
    this.form.patchValue(v);
    this.stateChanges.next();
  }

  /**
   * Whether the control is required.
   */
  @Input()
  get required(): boolean {
    return this._required;
  }

  /**
   * @internal Do not use!
   */
  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /**
   * Whether the control is disabled.
   */
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  /**
   * @internal Do not use!
   */
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /**
   * The placeholder for this control.
   */
  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  /**
   * @internal Do not use!
   */
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  /**
   * Whether the control is empty.
   */
  get empty() {
    const { address1, address2, city, region, postal, country } = this.form.value;
    return !address1 && !address2 && !city && !region && !postal && !country;
  }

  /**
   * Whether the MatFormField label should try to float.
   */
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  /**
   * @internal Do not use!
   */
  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  /**
   * @internal Do not use!
   */
  onContainerClick(event: MouseEvent) {
    if ((event.target as Element).tagName.toLowerCase() !== 'input') {
      this.elRef.nativeElement.querySelector('input').focus();
    }
  }

  /**
   * @internal Do not use!
   */
  ngOnDestroy() {
    this.stateChanges.complete();
    this.fm.stopMonitoring(this.elRef);
  }

}
