import {
  Component,
  OnDestroy,
  ElementRef,
  Input,
  Optional,
  Self
} from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl, MatRadioChange } from '@angular/material';
import { Subject } from 'rxjs';

/**
 * A custom field that displays two radio buttons: True, False.
 */
@Component({
  selector: 'keps-field-boolean',
  templateUrl: './field-boolean.component.html',
  styleUrls: ['./field-boolean.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: FieldBooleanComponent
    }
  ]
})
export class FieldBooleanComponent implements MatFormFieldControl<boolean>, OnDestroy {
  /**
   * @internal Do not use!
   */
  static nextId = 0;

  /**
   * @internal Do not use!
   */
  private _required = false;

  /**
   * @internal Do not use!
   */
  private _disabled = false;

  /**
   * @internal Do not use!
   */
  private _placeholder: string;

  /**
   * @internal Do not use!
   */
  stateChanges = new Subject<void>();

  /**
   * @internal Do not use!
   */
  focused = false;

  /**
   * @internal Do not use!
   */
  errorState = false;

  /**
   * @internal Do not use!
   */
  controlType = 'keps-field-boolean';

  /**
   * @internal Do not use!
   */
  id = `keps-field-boolean-${FieldBooleanComponent.nextId++}`;

  /**
   * @internal Do not use!
   */
  describedBy = '';

  /**
   * Form control of the field.
   */
  @Input() form = new FormControl(null);

  /**
   * @internal Do not use!
   */
  constructor(
    private fm: FocusMonitor,
    private elRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl
  ) {
    fm.monitor(elRef, true).subscribe(origin => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  /**
   * @internal Do not use!
   */
  onRadioChange(change: MatRadioChange) {
    this.value = change.value;
  }

  /**
   * The value of the control.
   */
  @Input()
  get value(): boolean | null {
    return this.form.value;
  }

  /**
   * @internal Do not use!
   */
  set value(b: boolean | null) {
    this.form.patchValue(b);
    this.stateChanges.next();
  }

  /**
   * Whether the control is required.
   */
  @Input()
  get required(): boolean {
    return this._required;
  }

  /**
   * @internal Do not use!
   */
  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /**
   * Whether the control is disabled.
   */
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  /**
   * @internal Do not use!
   */
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /**
   * The placeholder for this control.
   */
  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  /**
   * @internal Do not use!
   */
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  /**
   * Whether the control is empty.
   */
  get empty() {
    return this.form.value == null;
  }

  /**
   * Whether the MatFormField label should try to float.
   */
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  /**
   * @internal Do not use!
   */
  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  /**
   * @internal Do not use!
   */
  onContainerClick(event: MouseEvent) {
    if ((event.target as Element).tagName.toLowerCase() !== 'input') {
      this.elRef.nativeElement.querySelector('input').focus();
    }
  }

  /**
   * @internal Do not use!
   */
  ngOnDestroy() {
    this.stateChanges.complete();
    this.fm.stopMonitoring(this.elRef);
  }
}
