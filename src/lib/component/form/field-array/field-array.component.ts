import {
  Component,
  OnInit,
  OnDestroy,
  Inject,
  ElementRef,
  Input,
  Optional,
  Self
} from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl, MatFormFieldAppearance, ThemePalette } from '@angular/material';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { KepsFormArray } from '../keps-form-array';
import { KepsModels } from '../../../interface';
import { Subject, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'keps-field-array',
  templateUrl: './field-array.component.html',
  styleUrls: ['./field-array.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: FieldArrayComponent
    }
  ]
})
export class FieldArrayComponent implements MatFormFieldControl<any[]>, OnInit, OnDestroy {
  /**
   * @internal Do not use!
   */
  static nextId = 0;

  /**
   * @internal Do not use!
   */
  private _required = false;

  /**
   * @internal Do not use!
   */
  private _disabled = false;

  /**
   * @internal Do not use!
   */
  private _placeholder: string;

  /**
   * @internal Do not use!
   */
  stateChanges = new Subject<void>();

  /**
   * @internal Do not use!
   */
  focused = false;

  /**
   * @internal Do not use!
   */
  errorState = false;

  /**
   * @internal Do not use!
   */
  controlType = 'keps-field-array';

  /**
   * @internal Do not use!
   */
  id = `keps-field-array-${FieldArrayComponent.nextId++}`;

  /**
   * @internal Do not use!
   */
  describedBy = '';

  /**
   * Form control of the field.
   */
  @Input() form = new KepsFormArray([]);

  /**
   * Subschema of the form array.
   */
  @Input() subSchema = 'string';

  /**
   * Objects to be shown for reference field.
   */
  @Input() refObjs: any[] = [];

  /**
   * Appearance style of the form.
   */
  @Input() appearance: MatFormFieldAppearance = 'standard';

  /**
   * Angular Material color of the form field underline and floating label.
   */
  @Input() color: ThemePalette = 'primary';

  /**
   * A behavior subject that emits a number which is the length of the form array.
   *
   * It should emit whenever a form control is added/removed from the array.
   * @internal Do not use!
   */
  counter$ = new BehaviorSubject<number>(0);

  /**
   * An observable that emits an empty array with the same length as the form array.
   *
   * It is used for *ngFor in this component's HTML.
   * @internal Do not use!
   */
  counterArr$ = this.counter$.pipe(
    map(value => {
      return new Array(value);
    })
  );

  /**
   * @internal Do not use!
   */
  constructor(
    private fm: FocusMonitor,
    private elRef: ElementRef<HTMLElement>,
    @Inject('MODELS') private models: KepsModels,
    @Optional() @Self() public ngControl: NgControl
  ) {
    fm.monitor(elRef, true).subscribe(origin => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  /**
   * @internal Do not use!
   */
  ngOnInit() {
    this.form.setCounter$(this.counter$);
  }

  /**
   * Adds a form control to the form array.
   */
  add() {
    this.form.push(new FormControl(''));
    this.counter$.next(this.form.length);
  }

  /**
   * Removes a form control from the form array.
   * @param index Index of the form control to be removed.
   */
  remove(index: number) {
    this.form.removeAt(index);
    this.focused = false;
    this.counter$.next(this.form.length);
  }

  /**
   * Rearranges form controls inside the form array.
   * @internal Do not use!
   * @param event The drag event containing the form control.
   */
  drop(event: CdkDragDrop<FormControl[]>) {
    const prevIndex = event.previousIndex;
    const nextIndex = event.currentIndex;
    const movedForm = this.form.at(prevIndex);
    this.form.removeAt(prevIndex);
    this.form.insert(nextIndex, movedForm);
  }

  /**
   * Returns the display expression of a reference field.
   * @internal Do not use!
   * @param field Field to get from.
   */
  getReferenceDisplay() {
    const referenceTo = this.subSchema.slice(1);
    if (this.models && this.models[referenceTo]) {
      return this.models[referenceTo].properties.displayExpression;
    }
    return null;
  }

  /**
   * The value of the control.
   */
  @Input()
  get value(): any[] | null {
    return this.form.value;
  }

  /**
   * @internal Do not use!
   */
  set value(v: any[] | null) {
    this.form.patchValue(v);
    this.stateChanges.next();
  }

  /**
   * Whether the control is required.
   */
  @Input()
  get required(): boolean {
    return this._required;
  }

  /**
   * @internal Do not use!
   */
  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /**
   * Whether the control is disabled.
   */
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  /**
   * @internal Do not use!
   */
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /**
   * The placeholder for this control.
   */
  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  /**
   * @internal Do not use!
   */
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  /**
   * Whether the control is empty.
   */
  get empty() {
    return !this.form.value || this.form.value.length === 0;
  }

  /**
   * Whether the MatFormField label should try to float.
   */
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  /**
   * @internal Do not use!
   */
  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  /**
   * @internal Do not use!
   */
  onContainerClick(event: MouseEvent) {
    if ((event.target as Element).tagName.toLowerCase() !== 'input') {
      this.elRef.nativeElement.querySelector('input').focus();
    }
  }

  /**
   * @internal Do not use!
   */
  ngOnDestroy() {
    this.stateChanges.complete();
    this.fm.stopMonitoring(this.elRef);
  }
}
