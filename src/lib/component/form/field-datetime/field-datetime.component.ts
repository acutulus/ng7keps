import {
  Component,
  OnDestroy,
  Input,
  ElementRef,
  Optional,
  Self
} from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { FormControl, NgControl } from '@angular/forms';
import { MatFormFieldControl, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDatepickerInputEvent } from '@angular/material';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { Subject } from 'rxjs';
import moment from 'moment';

@Component({
  selector: 'keps-field-datetime',
  templateUrl: './field-datetime.component.html',
  styleUrls: ['./field-datetime.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: FieldDatetimeComponent
    },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: MAT_MOMENT_DATE_FORMATS
    }
  ]
})
export class FieldDatetimeComponent implements MatFormFieldControl<number>, OnDestroy {
  /**
   * @internal Do not use!
   */
  static nextId = 0;

  /**
   * @internal Do not use!
   */
  private _required = false;

  /**
   * @internal Do not use!
   */
  private _disabled = false;

  /**
   * @internal Do not use!
   */
  private _placeholder: string;

  /**
   * @internal Do not use!
   */
  stateChanges = new Subject<void>();

  /**
   * @internal Do not use!
   */
  focused = false;

  /**
   * @internal Do not use!
   */
  errorState = false;

  /**
   * @internal Do not use!
   */
  controlType = 'keps-field-datetime';

  /**
   * @internal Do not use!
   */
  id = `keps-field-datetime-${FieldDatetimeComponent.nextId++}`;

  /**
   * @internal Do not use!
   */
  describedBy = '';

  /**
   * Form control of the field.
   */
  @Input() form = new FormControl(null);

  /**
   * Form control for the date.
   * @internal Do not use!
   */
  dateForm = new FormControl(null);

  /**
   * The current date for placeholder.
   * @internal Do not use!
   */
  startDate = moment();

  /**
   * @internal Do not use!
   */
  constructor(
    private fm: FocusMonitor,
    private elRef: ElementRef<HTMLElement>,
    @Optional() @Self() public ngControl: NgControl
  ) {
    fm.monitor(elRef, true).subscribe(origin => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  /**
   * The value of the control.
   */
  @Input()
  get value(): number | null {
    return this.form.value;
  }

  /**
   * @internal Do not use!
   */
  set value(d: number | null) {
    this.form.setValue(d);
    if (d) {
      this.dateForm.setValue(moment(d));
    } else {
      this.dateForm.setValue(null);
    }
    this.stateChanges.next();
  }

  /**
   * Whether the control is required.
   */
  @Input()
  get required(): boolean {
    return this._required;
  }

  /**
   * @internal Do not use!
   */
  set required(value: boolean) {
    this._required = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /**
   * Whether the control is disabled.
   */
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  /**
   * @internal Do not use!
   */
  set disabled(value: boolean) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  /**
   * The placeholder for this control.
   */
  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  /**
   * @internal Do not use!
   */
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  /**
   * Whether the control is empty.
   */
  get empty() {
    return this.form.value == null;
  }

  /**
   * Whether the MatFormField label should try to float.
   */
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  /**
   * @internal Do not use!
   */
  setDescribedByIds(ids: string[]) {
    this.describedBy = ids.join(' ');
  }

  /**
   * @internal Do not use!
   */
  onContainerClick(event: MouseEvent) {
    if ((event.target as Element).tagName.toLowerCase() !== 'input') {
      this.elRef.nativeElement.querySelector('input').focus();
    }
  }

  /**
   * Sets the forms value after selecting a date on datepicker.
   * @internal Do not use!
   * @param event Changes to the date picker.
   */
  onDateChange(event: MatDatepickerInputEvent<moment.Moment>) {
    if (event.value) {
      this.value = event.value.valueOf();
    }
  }

  /**
   * @internal Do not use!
   */
  ngOnDestroy() {
    this.stateChanges.complete();
    this.fm.stopMonitoring(this.elRef);
  }

}
