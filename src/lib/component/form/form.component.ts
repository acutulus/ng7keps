import { Component, OnInit, Inject, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ThemePalette, MatFormFieldAppearance } from '@angular/material';
import { KepsForm } from './keps-form';
import { KepsFormArray } from './keps-form-array';
import { KepsSchema, KepsModels, KepsReference } from '../../interface';
import { isType, getReferenceDisplay } from '../../utils';

/**
 * A component that renders form based on Keps schema.
 */
@Component({
  selector: 'keps-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  /**
   * An instance of Keps form.
   */
  @Input() form = new KepsForm({});

  /**
   * Name of the model for the form.
   */
  @Input() model: string;

  /**
   * Schema for the form.
   *
   * If the model name is supplied,
   * then the form will get the schema from that model.
   */
  @Input() schema: KepsSchema;

  /**
   * A map of reference to Keps objects to be shown for reference fields.
   *
   * Notes:-The key of the map is based on the field name of the Keps form model.
   *        Not the name of the model it is referenced to.
   *       -If field type is a string and a refObjs is provided,
   *        instead of a normal text input, it will be a select. see html for more info.
   * 
   * **sample usage**
   * given the following models: 
   *  PERSON {
   *    _id
   *    name: 'string',
   *    pet: ':ANIMAL'
   *  }
   * 
   *  ANIMAL {
   *    _id
   *    type: 'string'
   *  }
   * 
   *  if one wish to create a form for 
   *  object PERSON where the options for ANIMAL 
   *  comes from DB, that dev can create a refObjs:KepsReference
   *  which  will look like:
   *  
   *  refObjs = {
   *              'pet': [
   *                <ANIMAL>{
   *                    _id
   *                    type
   *                  },
   *                <ANIMAL>{
   *                    _id
   *                    type
   *                },
   *                ...
   *                ...
   *                ..
   *              ],
   *            }
   *  
   *  SIDENOTE: The annotation <TYPE> is used to make this exmaple more explicit.
   */
  @Input() refObjs: KepsReference = {};

  /**
   * Appearance style of the form.
   */
  @Input() appearance: MatFormFieldAppearance = 'standard';

  /**
   * Angular Material color of the form field underline and floating label.
   */
  @Input() color: ThemePalette = 'primary';

  /**
   * Fields to be displayed on the form.
   */
  @Input() displayedFields: string[] = [];

  /**
   * Fields to be hidden on the form.
   */
  @Input() hiddenFields: string[] = [];

  /**
   * Placeholders to be displayed on fields.
   */
  @Input() placeholders: Record<string, any> = {};

  /**
   * Hints for fields input.
   */
  @Input() hints: Record<string, string> = {};

  /**
   * Fields to be displayed on the form based on displayedFields and hiddenFields.
   * @internal Do not use!
   */
  schemaFields: string[];

  /**
   * Whether the form has done initializing or not.
   */
  initialized = false;

  /**
   * @internal Do not use!
   */
  constructor(
    @Inject('MODELS') private models: KepsModels
  ) { }

  /**
   * Initializes the form with fields based on the schema.
   * @internal Do not use!
   */
  ngOnInit() {
    // Get schema from model name if supplied.
    if (this.model && this.models[this.model]) {
      this.schema = this.models[this.model].schema;
    } else if (this.model && !this.models[this.model]) {
      throw new Error(`Can't find a model named ${this.model}!`);
    }

    // Assert schema must exist.
    if (!this.schema) {
      throw new Error(`Must supply valid model name or schema to Keps form!`);
    }

    // Set schema fields.
    if (this.displayedFields.length > 0) {
      this.schemaFields = this.displayedFields;
    } else {
      this.schemaFields = Object.keys(this.schema)
      .filter(field => field[0] !== '_' && !this.hiddenFields.includes(field));
    }
    this.form.setSchema(this.schema);

    for (const fieldName of this.schemaFields) {
      const field = this.schema[fieldName];

      // Create validators if needed.
      const validators = [];
      if (!field.optional) {
        validators.push(Validators.required);
      }
      if (field.type === 'number') {
      }
      if (field.type === 'email') {
        validators.push(Validators.email);
      }

      // Create the appropriate form control for each field.
      if (field.type === 'address') {
        this.form.addControl(fieldName, new FormGroup({}, validators));
      } else if (field.type === 'array') {
        this.form.addControl(fieldName, new KepsFormArray([], validators));
      } else if (field.type === 'datetime') {
        this.form.addControl(fieldName, new FormControl(null, validators));
      } else {
        this.form.addControl(
          fieldName,
          new FormControl(
            field.default || null,
            validators
          )
        );
      }
    }
    this.form.initialize();
    this.initialized = true;
  }

  /**
   * Checks the type of a field.
   * @internal Do not use!
   * @param field Field to be checked.
   * @param type Type to be checked.
   */
  isType(field: string, type: string) {
    return isType(this.schema, field, type);
  }

  /**
   * Returns the display expression of a reference field.
   * @internal Do not use!
   * @param field Field to get from.
   */
  getReferenceDisplay(field: string) {
    return getReferenceDisplay(this.models, this.schema, field);
  }

  /**
   * Check if MatFormField should hide the underline or not.
   * @internal Do not use!
   * @param field Field to be checked.
   */
  isNotUnderlined(field: string) {
    return this.schema[field].type === 'boolean' ||
    this.schema[field].type === 'address' ||
    this.schema[field].type === 'array';
  }

  /**
  * Check if a field has a reference in refObjs.
  * if has reference in refobjs, use mat-select instead of input.
  * supports only type string at the moment.
  * @internal Do not use!
  * @param field Field to be checked.
  */
  hasReference(field: string) {
    return this.schema[field].type === 'string' && 
    this.refObjs && 
    this.refObjs[field];
  }

}
