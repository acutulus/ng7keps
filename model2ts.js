/*
 * This file should:
 * - Get models from the Keps server.
 * - Create interface files for the models.
 * - Create a MODELS.ts that export all the models (only properties and schema) as one object.
 * - Create an index.ts (if it doesn't exist) that exports everything.
 */

/*
  Modules
*/
const fs = require('fs');
const path = require('path');
const request = require('request');
const { promisify } = require('util');
const _ = require('lodash');

/*
  Default Settings
*/
let baseHref  = 'http://localhost:8000/';
let outFolder = '../../models';

/*
  Start
*/
if (process.argv.length > 2) {
  baseHref = process.argv[2];
  if (!baseHref.endsWith('/')) {
    baseHref += '/';
  }
}
// if provided, outFolder will be specified
if (process.argv.length > 3) {
  outFolder = process.argv[3];
}

const main = async () => {
  // Get models from Keps server.
  let models;
  try {
    const result = await request.get(`${baseHref}api/models?full=true`);
    models = JSON.parse(result.body);
  } catch (err) {
    console.error('Error Getting Models');
    console.error('- Make sure server is running');
    console.error('- Pass the correct server path to this script');
    return;
  }

  // Create output folder if it doesn't exist.
  if (!fs.existsSync(outFolder)) {
    fs.mkdirSync(outFolder);
  }

  // create keps folder inside model if not exists
  if (!fs.existsSync(outFolder + '/keps')) {
    fs.mkdirSync(outFolder + '/keps');
  }
  
  // Generate interface files for models.
  const processedModels = [];
  for (const model in models) {
    // Get name and schema.
    const name = capitalize(model);
    const schema = models[model].schema;

    // Create file for the interface.
    const file = fs.createWriteStream(path.join(outFolder + '/keps', name + '.ts'));
    const references = new Set([]);

    // Convert necessary model data to object.
    const obj = {};
    obj.schema = models[model].schema;
    obj.properties = {};
    obj.properties.description = models[model].properties.description;
    obj.properties.displayExpression = models[model].properties.displayExpression;

    // Export the interface.
    file.write(`export interface ${name} {\n`);
    for (const field in schema) {
      // Check if optional or not.
      if (schema[field].optional) {
        file.write(`  ${field}?: `);
      } else {
        file.write(`  ${field}: `);
      }

      // Write the type of field.
      const type = field2type(schema[field]);
      if (!type.endsWith(';')) {
        file.write(`${type};\n`);
      } else {
        file.write(`${type}\n`);
      }

      // Add to references if needed.
      if (schema[field].type[0] === ':' || schema[field].type === 'address') {
        references.add(type);
      }
      if (schema[field].type[0] === ':') {
        file.write(`  ${field}Id?: string;\n`);
      }
      if (schema[field].type === 'array' && schema[field].subSchema && schema[field].subSchema[0] === ':') {
        const subType = camel2pascal(schema[field].subSchema.slice(1));
        references.add(subType);
      }
    }
    file.write(`}\n`);

    // Import any references.
    for (const reference of references) {
      if (reference === 'KepsAddress') {
        file.write(`\nimport { KepsAddress } from 'ng7keps';`)
      } else {
        file.write(`\nimport { ${reference} } from './${reference}';`);
      }
    }

    // End file.
    file.end();
    processedModels.push(name);
    console.log(`Generated file for ${name}`);
  }

  // Generate MODELS.ts file.
  const cleanedModels = {};
  for (const model in models) {
    cleanedModels[model] = {
      properties: _.pick(models[model].properties, ['displayExpression']),
      schema: models[model].schema
    }
  }
  const modelFile = fs.createWriteStream(path.join(outFolder + '/keps', 'MODELS.ts'));
  modelFile.write(`import { KepsModels } from 'ng7keps'\n\n`);
  modelFile.write(`export const MODELS: KepsModels = `)
  modelFile.write(JSON.stringify(cleanedModels, null, 2));
  modelFile.end();
  console.log(`Generated MODEL file`);

  // Create file to export everything.
  const indexFile = fs.createWriteStream(path.join(outFolder + '/keps', 'index.ts'));
  for (const model of processedModels) {
    indexFile.write(`export * from './${model}';\n`);
  }
  indexFile.write(`\n`);
  indexFile.end();

  // add index file on init
  if (!fs.existsSync(path.join(outFolder, 'index.ts'))) {
    const mergedModelFile = fs.createWriteStream(path.join(outFolder, 'index.ts'));
    mergedModelFile.write(`/*\n`);
    mergedModelFile.write(' This is were to export all front-end interfaces.\n')
    mergedModelFile.write(` And all front-end related models\n`);
    mergedModelFile.write(`*/\n\n`);
    mergedModelFile.write(`// DO NOT modify - auto generated.\n`)
    mergedModelFile.write(`export * from './keps';\n\n`);
    mergedModelFile.write(`// merged backend and front-end KepsModels\n`);
    mergedModelFile.write(`// to Add KepsModel used for front-end ONLY, see ./MODELS\n`);
    mergedModelFile.write(`export * from './MODELS';\n\n`);
    mergedModelFile.write(`/*-----------------------*/\n`);
    mergedModelFile.write(`/*---ADD exports below---*/\n`);
    mergedModelFile.write(`/*-----------------------*/\n\n`);
  }

  // add starting MODELS.ts file on init
  if (!fs.existsSync(path.join(outFolder, 'MODELS.ts'))) {
    const mergedModelFile = fs.createWriteStream(path.join(outFolder, 'MODELS.ts'));
    mergedModelFile.write(`import * as models from './keps/MODELS';\n`);
    mergedModelFile.write(`import { KepsModels } from 'ng7keps';\n\n`);
    mergedModelFile.write(`/*\n`);
    mergedModelFile.write(' Merge backend KepsModels and Front-end KepsModels.\n');
    mergedModelFile.write(`*/\n`);
    mergedModelFile.write(`export const MODELS: KepsModels = Object.assign(models, <KepsModels>{\n`)
    mergedModelFile.write(`    // Enter front-end only KepsModels here.\n`);
    mergedModelFile.write(`});\n`);
  }
  
  

  console.log(`Generated index file`);
};

/*
  Utils
*/
const field2type = field => {
  const getType = type => {
    if (type[0] === ':') {
      return camel2pascal(type.slice(1));
    } else if (type === 'enum') {
      return 'string;'
    } else if (type === 'enum' || type === 'multienum') {
      return 'string[]';
    } else if (type === 'address') {
      return 'KepsAddress';
    } else if (type === 'email' || type === 'image' || type === 'url') {
      return 'string';
    } else if (type === 'datetime' || type === 'phone') {
      return 'number';
    } else if (type === 'string' || type === 'number' || type === 'boolean') {
      return type;
    } else {
      return 'any';
    }
  };

  const type = field.type;
  if (type === 'array') {
    const subSchema = field.subSchema;
    if (!subSchema || typeof subSchema === 'object') {
      return 'any[]';
    } else {
      return getType(subSchema) + '[]';
    }
  } else {
    return getType(type);
  }
};

const camel2pascal = str => {
  const re = /([A-Z])([A-Z])([a-z])|([a-z])([A-Z])|([a-z])([0-9])/g;
  str = str
    .replace(re, '$1$4$6 $2$3$5$7')
    .replace(/\s/g, '');
  return capitalize(str);
};

const capitalize = str => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

request.get = promisify(request.get);

return main();
